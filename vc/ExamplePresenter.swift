import Foundation

protocol ExampleDelegate: BaseDelegate {
    func updateData()
}

class ExamplePresenter {
    weak fileprivate var view: ExampleDelegate?
    fileprivate let service: ExampleService
    fileprivate var dataList = [ExampleModel]()
    
    init(service: ExampleService){
        self.service = service
    }
    
    func attachView(view: ExampleDelegate){
        self.view = view
    }
    
    func viewDidAppear(){
        
    }
    
    func viewDidShow(){
        actualizarDatos()
    }
    
    func detachView() {
        view = nil
    }
    
    func willHideView(){
        
    }
    
    func actualizarDatos(){
        self.view?.startLoading()
        service.getData(){[weak self] data in
            self?.onFinishGetData(data: data)
        }
    }
    
    func onFinishGetData( data: [ExampleModel]){
        view?.finishLoading()
        if (data.count == 0){
            dataList = [ExampleModel]()
        }else{
            dataList = data
        }
        view?.updateData()
    }
    
}
