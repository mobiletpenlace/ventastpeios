import RealmSwift

class ExampleService: BaseDBManagerDelegate {
    var realm: Realm!
    var serverManager: ServerDataManager?
    var dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()
        realm = getRealm()
        serverManager = ServerDataManager()
    }
    
    func getData(_ callBack:@escaping ([ExampleModel]) -> Void){
        getDataDummy(callBack)
    }
    
    func getDataFromDB(_ callBack:@escaping ([ExampleModel]) -> Void) {
        let res = [ExampleModel]()//crear lista de datos
//        let data = dbManager.getData()
//        for value in data{
//            res.append(ExampleModel(text: value))
//        }
        callBack(res)
    }
    
    func getDataFromService(_ callBack:@escaping ([ExampleModel]) -> Void){
        let res = [ExampleModel]()
        callBack(res)
        
//        let url = Constantes.URL_FAMILIAS_ID
//        let tipo = ResponseFamilias.self
//        serverManager.post(url: url, tipo: .sf){responseData in
//            guard let data = responseData.data else{
//                Logger.println("error en el servicio post( \(url) )")
//                return
//            }
//            do{
////                let decoder = JSONDecoder()
////                let responseOBJ = try decoder.decode(tipo, from: data)
//                
//                //procesar responseOBJ
//                
//            }catch let err{
//                Logger.println("error: --> \(err)")
//            }
//            callBack(res)
//        }
    }
    
    func getDataDummy(_ callBack:@escaping ([ExampleModel]) -> Void){
        let data = [
            ExampleModel(text: "test"),
            ExampleModel(text: "test"),
            ]
        let delayTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            callBack(data)
        }
    }
}
