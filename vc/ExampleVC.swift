
class ExampleVC: BaseItemVC  {
    
    fileprivate let presenter = ExamplePresenter(service: ExampleService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow(){
        super.viewDidShow()
        presenter.viewDidShow()
    }
    
    private func setUpView() {
        
    }
    
    @IBAction func clickAceptar(_ sender: UIButton) {
        sender.animateBound(view: sender){ _ in
            //presenter.clickAceptar()
        }
    }
    
    override func willHideView(){
        presenter.willHideView()
    }
}


extension ExampleVC: ExampleDelegate{
    
    func updateData(){
        
    }
    
    func updateTitleHeader() {
        
    }
    
    func startLoading() {
        super.showLoading(view: view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
    
}

