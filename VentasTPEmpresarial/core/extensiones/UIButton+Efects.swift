//
//  AnimationView.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 05/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
extension UIView {

    func animateScale(view: UIView) {
        let time = 0.4
        let scaleX = CGFloat(1.15)
        let scaleY = CGFloat(1.1)
        UIView.animate(withDuration: time / 2,
            animations: {
                view.transform = CGAffineTransform(scaleX: scaleX, y: scaleY)
            },
            completion: { _ in
                UIView.animate(withDuration: time / 2) {
                    view.transform = CGAffineTransform.identity
                }
            })
    }

    func animateBound(view: UIView) {
        view.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        UIView.animate(withDuration: 0.4,
            delay: 0,
            usingSpringWithDamping: CGFloat(0.20),
            initialSpringVelocity: CGFloat(6.0),
            options: UIViewAnimationOptions.allowUserInteraction,
            animations: {
                view.transform = CGAffineTransform.identity
            },
            completion: nil
        )
    }

    func animateBound(view: UIView, closure: ((Bool) -> Void)?) {
        view.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        UIView.animate(withDuration: 0.4,
            delay: 0,
            usingSpringWithDamping: CGFloat(0.20),
            initialSpringVelocity: CGFloat(6.0),
            options: UIViewAnimationOptions.allowUserInteraction,
            animations: {
                view.transform = CGAffineTransform.identity
            },
            completion: closure
        )
    }

    func animateHidden(view: UIView, vIni: Float, vFin: Float) {
        view.alpha = CGFloat(vIni)
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 5, options: .curveEaseInOut, animations: {
            view.alpha = CGFloat(vFin)
        })
    }

    func animateShake(view: UIView) {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: view.center.x - 10, y: view.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: view.center.x + 10, y: view.center.y))

        view.layer.add(animation, forKey: "position")
    }
}
