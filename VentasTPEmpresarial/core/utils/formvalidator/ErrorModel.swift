//
//  errorModel.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 09/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

struct ErrorModel {
    let message: String
    let textfield: UITextField
}
