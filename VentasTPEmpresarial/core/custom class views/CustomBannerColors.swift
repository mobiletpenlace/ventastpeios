//
//  CustomBannerColors.swift
//  VentasTPEmpresarial
//
//  Created by eduardo mancilla on 2/14/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation
import NotificationBannerSwift

class CustomBannerColors: BannerColorsProtocol {
    
    internal func color(for style: BannerStyle) -> UIColor {
        switch style {
        case .danger:   return color(for: style)
        case .info:     return color(for: style)
        case .none:     return color(for: style)
        case .success:  return Colores.azulOscuro
        case .warning:  return color(for: style)
        }
    }
    
}
