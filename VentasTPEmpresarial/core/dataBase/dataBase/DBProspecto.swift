//
//  DBProspecto.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 29/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

import RealmSwift
import Realm

class DBProspecto: Object {
    @objc dynamic var tipoPersona: String = ""
    @objc dynamic var razonSocial: String = ""
    @objc dynamic var nombre: String = ""
    @objc dynamic var apellidoPaterno: String = ""
    @objc dynamic var apellidoMaterno: String = ""
    @objc dynamic var fechaNacimiento: String = ""
    @objc dynamic var rfc: String = ""
    @objc dynamic var medioContacto: String = ""

    @objc dynamic var tipoIdentificacion: String = ""
    @objc dynamic var celular: String = ""
    @objc dynamic var telefonoPrincipal: String = ""
    @objc dynamic var segundoTelefono: String = ""
    @objc dynamic var correoElectronico: String = ""
    @objc dynamic var segundoCorreo: String = ""
    @objc dynamic var segmento: String = ""
    @objc dynamic var montoFacturacion: String = ""
    @objc dynamic var numeroIdentificacion: String = ""

    @objc dynamic var nombreDeLaCompania: String = ""
    @objc dynamic var calle: String = ""
    @objc dynamic var referenciaCalle: String = ""
    @objc dynamic var numeroInterior: String = ""
    @objc dynamic var numeroExterior: String = ""
    @objc dynamic var referenciaUrbana: String = ""

    @objc dynamic var estado: String = ""
    @objc dynamic var ciudad: String = ""
    @objc dynamic var delegacionMuni: String = ""
    @objc dynamic var cp: String = ""
    @objc dynamic var colonia: String = ""
}
