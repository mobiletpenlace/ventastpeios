//
//  DBProductoAdicional.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 15/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import RealmSwift

class DBProductoAdicional: Object {
    @objc dynamic var id: String?
    @objc dynamic var nombre: String = ""
    @objc dynamic var precio: Float = 0
    @objc dynamic var cantidad: Int = 0
    let plan = LinkingObjects(fromType: DBPlan.self, property: "productosAdicionales")
    let addons = List<DBAddonServicio>()
    @objc dynamic var limiteInf: Int = 0
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(nombre: String, precio: Float, id: String) {
        self.init()
        self.nombre = nombre
        self.precio = precio
        self.id = id
    }
}
