//
//  DBProductoAdicional.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 03/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import RealmSwift

class DBServicioAdicional: Object {
    @objc dynamic var id: String?
    @objc dynamic var nombre: String = ""
    @objc dynamic var precio: Float = 0
    @objc dynamic var cantidad: Int = 0
    let plan = LinkingObjects(fromType: DBPlan.self, property: "serviciosAdicionales")
    @objc dynamic var maximoAgregar: Int = 0
    @objc dynamic var esSDWAN: Bool = false
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(nombre: String, precio: Float, id: String) {
        self.init()
        self.nombre = nombre
        self.precio = precio
        self.id = id
    }
}
