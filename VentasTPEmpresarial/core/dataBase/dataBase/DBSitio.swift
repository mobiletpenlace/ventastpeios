//
//  DBSitio.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 03/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import RealmSwift

class DBSitio: Object {
    @objc dynamic var id = 0
    @objc dynamic var nombre: String?
    @objc dynamic var cobertura: DbCobertura?
    //let planes = List<DBPlan>()
    let planesConfig = List<DBConfigPlan>()
    let resumenCotizacion = LinkingObjects(fromType: DBResumenCotizacionSitio.self, property: "sitios")
    @objc private dynamic var _costoRentaMensual: Float = 0
    @objc private dynamic var _costoInstalacion: Float = 0
    @objc private dynamic var _costoAdicionales: Float = 0
    @objc private dynamic var _descuento: Float = 0
    @objc private dynamic var _subTotal: Float = 0
    @objc private dynamic var _impuestos: Float = 0
    @objc private dynamic var _cargosUnicos: Float = 0
    @objc private dynamic var _total: Float = 0

    var costoRentaMensual: Float {
        get { return _costoRentaMensual }
        set {
            _costoRentaMensual = newValue
            calcularTotal()
        }
    }

    var costoInstalacion: Float {
        get { return _costoInstalacion }
        set {
            _costoInstalacion = newValue
            calcularTotal()
        }
    }

    var costoAdicionales: Float {
        get { return _costoAdicionales }
        set {
            _costoAdicionales = newValue
            calcularTotal()
        }
    }

    var descuento: Float {
        get { return _descuento }
        set {
            _descuento = newValue
            calcularTotal()
        }
    }

    var impuestos: Float {
        get { return _impuestos }
        set {
            _impuestos = newValue
            calcularTotal()
        }
    }

    var cargosUnicos: Float {
        get { return _cargosUnicos }
        set {
            _cargosUnicos = newValue
            calcularTotal()
        }
    }

    var subTotal: Float {
        get { return _subTotal }
    }

    var total: Float {
        get { return _total }
    }

    override static func primaryKey() -> String? {
        return "id"
    }

    convenience init(id: Int) {
        self.init()
        self.id = id
    }

    convenience init(nombre: String, id: Int) {
        self.init()
        self.nombre = nombre
        self.id = id
    }

    private func calcularTotal() {
        _subTotal = _costoRentaMensual + _costoInstalacion + _costoAdicionales - _descuento
        _total = _subTotal + _impuestos + _cargosUnicos
    }

    func actualizarCostos() {
        costoRentaMensual = 0
        costoInstalacion = 0
        costoAdicionales = 0
        descuento = 0
        impuestos = 0
        cargosUnicos = 0
        for plan in planesConfig { //------->------->plan repetido
            if let resumenCotPlan = plan.resumenCotizacion {
                costoRentaMensual += resumenCotPlan.costoRentaMensual
                costoInstalacion += resumenCotPlan.costoInstalacion
                costoAdicionales += resumenCotPlan.costoAdicionales
                descuento += resumenCotPlan.descuento
                impuestos += resumenCotPlan.impuestos
                cargosUnicos += resumenCotPlan.cargosUnicos
            }
        }
    }
}
