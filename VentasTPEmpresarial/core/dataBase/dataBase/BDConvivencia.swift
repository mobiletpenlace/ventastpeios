//
//  BDConvivencia.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 17/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import RealmSwift
import Realm

class BDConvivencia: Object {
    @objc dynamic var id: String = ""

    convenience init(id: String) {
        self.init()
        self.id = id
    }
}
