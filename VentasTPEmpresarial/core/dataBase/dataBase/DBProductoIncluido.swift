//
//  DBProductoIncluido.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 03/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import RealmSwift

class DBProductoIncluido: Object {
    @objc dynamic var nombre: String = ""
    @objc dynamic var descripcion: String = ""
    let plan = LinkingObjects(fromType: DBPlan.self, property: "productosIncluidos")
    
    convenience init(nombre: String, descripcion: String) {
        self.init()
        self.nombre = nombre
        self.descripcion = descripcion
    }
}
