//
//  DBFamilia.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 03/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import RealmSwift
import Realm

class DBFamilia: Object {
    @objc dynamic var nombre: String?
    @objc dynamic var id: String?
    @objc dynamic var tipo: DBTipoFamilia?
    let planes = List<DBPlan>()

    override static func primaryKey() -> String? {
        return "id"
    }

    convenience init(nombre: String, id: String) {
        self.init()
        self.nombre = nombre
        self.id = id
    }
}
