//
//  DBAddonServicio.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 15/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import RealmSwift

class DBAddonServicio: Object {
    @objc dynamic var id: String?
    @objc dynamic var nombre: String = ""
    @objc dynamic var precio: Float = 0
    @objc dynamic var maximo: Int = 0
    @objc dynamic var impuesto: Float = 0
    @objc dynamic var cantidad: Int = 0
    @objc dynamic var isActivo: Bool = true
    let convivencia = List<String>()
    let productoAdicional = LinkingObjects(fromType: DBProductoAdicional.self, property: "addons")
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(nombre: String, precio: Float, id: String) {
        self.init()
        self.nombre = nombre
        self.precio = precio
        self.id = id
    }
}
