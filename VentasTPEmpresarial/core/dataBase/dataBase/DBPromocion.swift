//
//  DBPromocion.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 15/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import RealmSwift

class DBPromocion: Object {
    @objc dynamic var id: String?
    @objc dynamic var nombre: String = ""
    @objc dynamic var fechaInicio: String = ""
    @objc dynamic var fechaFin: String = ""
    let convivencia = List<BDConvivencia>()
    @objc dynamic var activo: Bool = true
    @objc dynamic var seleccionado: Bool = false
    @objc dynamic var descuento: Float = 0
    let plan = LinkingObjects(fromType: DBPlan.self, property: "promociones")
    @objc dynamic var idServicio: String = ""
    @objc dynamic var idProducto: String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(nombre: String, id: String) {
        self.init()
        self.nombre = nombre
        self.id = id
    }
}
