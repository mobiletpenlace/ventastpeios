//
//  DBPlazo.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 29/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import RealmSwift
import Realm

class DBPlazo: Object {
    @objc dynamic var id = 0
    @objc dynamic var nombre: String = ""
    let planes = LinkingObjects(fromType: DBPlan.self, property: "plazos")
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(nombre: String, id: Int) {
        self.init()
        self.nombre = nombre
        self.id = id
    }
    
}
