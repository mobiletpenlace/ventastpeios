//
//  DBConfigServicioAdicional.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 23/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import RealmSwift
import Realm

class DBConfigServicioAdicional: Object {
    @objc dynamic var cantidad: Int = 0
    @objc dynamic var servicioAdicional: DBServicioAdicional!
    let plan = LinkingObjects(fromType: DBConfigPlan.self, property: "serviciosAdicionales")

    @objc dynamic var configFlexnet: DBFlexnetConfig?

    convenience init(servicioAdicional: DBServicioAdicional) {
        self.init()
        self.servicioAdicional = servicioAdicional
    }
}
