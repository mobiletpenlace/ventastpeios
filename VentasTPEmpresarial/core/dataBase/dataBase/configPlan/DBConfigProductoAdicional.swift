//
//  DBConfigProductoAdicional.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 23/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import RealmSwift
import Realm

class DBConfigProductoAdicional: Object {
    @objc dynamic var productoAdicional: DBProductoAdicional!
    let plan = LinkingObjects(fromType: DBConfigPlan.self, property: "productosAdicionales")
    var addons = List<DBConfigProductoAdicionalAddon>()

    convenience init(productoAdicional: DBProductoAdicional) {
        self.init()
        self.productoAdicional = productoAdicional
    }
}
