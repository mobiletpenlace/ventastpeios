//
//  DBConfigProductoAdicionalAddon.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 22/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import RealmSwift
import Realm

class DBConfigProductoAdicionalAddon: Object {
    @objc dynamic var cantidad: Int = 0
    @objc dynamic var addon: DBAddonServicio!
    @objc dynamic var precio: Float = 0
    let productoAdic = LinkingObjects(fromType: DBConfigProductoAdicional.self, property: "addons")
    @objc dynamic var isActivo: Bool = true

    convenience init(addon: DBAddonServicio) {
        self.init()
        self.addon = addon
        self.cantidad = addon.cantidad
        self.precio = addon.precio
    }
}
