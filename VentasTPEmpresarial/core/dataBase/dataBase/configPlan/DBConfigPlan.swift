//
//  DBConfigPlan.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 23/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import RealmSwift
import Realm

class DBConfigPlan: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var plan: DBPlan!
    var serviciosAdicionales = List<DBConfigServicioAdicional>()
    var productosAdicionales = List<DBConfigProductoAdicional>()
    @objc dynamic var resumenCotizacion: DBResumenCotizacionPlan?
    let sitio = LinkingObjects(fromType: DBSitio.self, property: "planesConfig")

    var costoInstalacion: Float {
        get {
            return resumenCotizacion!.costoInstalacion
        }
        set {
            resumenCotizacion?.costoInstalacion = newValue
        }
    }
    var costoRentaPrecioLista: Float {
        get {
            return resumenCotizacion!.costoRentaMensual
        }
        set {
            resumenCotizacion?.costoRentaMensual = newValue
        }
    }

    override static func primaryKey() -> String? {
        return "id"
    }

    convenience init(plan: DBPlan, id: Int) {
        self.init()
        self.id = id
        self.plan = plan
        for serv in plan.serviciosAdicionales {
            serviciosAdicionales.append(
                DBConfigServicioAdicional(servicioAdicional: serv)
            )
        }
        for prod in plan.productosAdicionales {
            let prodConfig = DBConfigProductoAdicional(productoAdicional: prod)
            for addon in prod.addons {
                prodConfig.addons.append(
                    DBConfigProductoAdicionalAddon(addon: addon)
                )
            }
            productosAdicionales.append(prodConfig)
        }
        resumenCotizacion = DBResumenCotizacionPlan()
        costoInstalacion = plan.costoInstalacion
        costoRentaPrecioLista = plan.costoRentaPrecioLista

    }
}
