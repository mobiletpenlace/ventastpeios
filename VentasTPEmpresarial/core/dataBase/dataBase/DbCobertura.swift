//
//  DbCobertura.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 03/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import RealmSwift

class DbCobertura: Object {
    @objc dynamic var cobertura: Bool = false
    let sitio = LinkingObjects(fromType: DBSitio.self, property: "cobertura")

    @objc dynamic var categoryService: String = ""
    @objc dynamic var cluster: String = ""
    @objc dynamic var distrito: String = ""
    @objc dynamic var factibilidad: String = ""
    @objc dynamic var plaza: String = ""
    @objc dynamic var region: String = ""
    @objc dynamic var regionId: String = ""
    @objc dynamic var tipoCobertura: String = ""
    @objc dynamic var zona: String = ""

    @objc dynamic var codigoPostal: String = ""
    @objc dynamic var latitud: Double = 0
    @objc dynamic var longitud: Double = 0
    @objc dynamic var calle: String = ""
    @objc dynamic var numExt: String = ""
    @objc dynamic var numInt: String = ""
    @objc dynamic var colonia: String = ""
    @objc dynamic var estado: String = ""
    @objc dynamic var ciudad: String = ""
    @objc dynamic var municipio: String = ""
    @objc dynamic var entreCalle: String = ""
    @objc dynamic var yCalle: String = ""
    @objc dynamic var observaciones: String = ""
}
