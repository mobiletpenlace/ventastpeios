//
//  DBDireccionFacturacion.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 01/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import RealmSwift

class DBDireccionFacturacion: Object {
    @objc dynamic var calle: String = ""
    @objc dynamic var ciudad: String = ""
    @objc dynamic var codigoPostalFacturacion: String = ""
    @objc dynamic var colonia: String = ""
    @objc dynamic var mes: String = ""
    @objc dynamic var delegacionMunicipio: String = ""
    @objc dynamic var estado: String = ""
    @objc dynamic var mismaDirInst: Bool = false
    @objc dynamic var numExterior: String = ""
    @objc dynamic var numInterior: String = ""
    
    let resumenCotizacionSitio = LinkingObjects(fromType: DBResumenCotizacionSitio.self, property: "direccionFacturacion")
    
    convenience init(calle: String) {
        self.init()
        self.calle = calle
    }
}
