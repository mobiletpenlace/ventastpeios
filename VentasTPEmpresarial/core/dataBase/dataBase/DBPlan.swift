//
//  DBPlan.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 03/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import RealmSwift
import Realm

class DBPlan: Object {
    @objc dynamic var nombre: String?
    @objc dynamic var id: String?
    let productosIncluidos = List<DBProductoIncluido>()
    let serviciosAdicionales = List<DBServicioAdicional>()
    let productosAdicionales = List<DBProductoAdicional>()
    let promociones = List<DBPromocion>()
    @objc dynamic var costoInstalacion: Float = 0
    @objc dynamic var costoRentaPrecioLista: Float = 0
    @objc dynamic var costoRentaProntoPago: Float = 0
    @objc dynamic var costoRentaProntoPagoIVA: Float = 0
    @objc dynamic var costoRentaPrecioListaIVA: Float = 0
    let plazos = List<DBPlazo>()
    let planesConfig = LinkingObjects(fromType: DBConfigPlan.self, property: "plan")
    let familia = LinkingObjects(fromType: DBFamilia.self, property: "planes")
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(nombre: String, id: String) {
        self.init()
        self.nombre = nombre
        self.id = id
    }
}
