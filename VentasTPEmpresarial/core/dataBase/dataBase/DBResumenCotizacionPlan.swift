//
//  DBResumenCotizacionPlan.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 25/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import RealmSwift

class DBResumenCotizacionPlan: Object {
    @objc private dynamic var _costoRentaMensual: Float = 0
    @objc private dynamic var _costoInstalacion: Float = 0
    @objc private dynamic var _costoAdicionales: Float = 0
    @objc private dynamic var _descuento: Float = 0
    @objc private dynamic var _subTotal: Float = 0
    @objc private dynamic var _impuestos: Float = 0
    @objc private dynamic var _porcentajeImpuestos: Float = 0.16
    @objc private dynamic var _cargosUnicos: Float = 0
    @objc private dynamic var _total: Float = 0
    @objc dynamic var porcentajeDescuento: Float = 0
    let planConfig = LinkingObjects(fromType: DBConfigPlan.self, property: "resumenCotizacion")

    var costoRentaMensual: Float {
        get { return _costoRentaMensual }
        set {
            _costoRentaMensual = newValue
            calcularTotal()
        }
    }

    var costoInstalacion: Float {
        get { return _costoInstalacion }
        set {
            _costoInstalacion = newValue
            calcularTotal()
        }
    }

    var costoAdicionales: Float {
        get { return _costoAdicionales }
        set {
            _costoAdicionales = newValue
            calcularTotal()
        }
    }

    var descuento: Float {
        get { return _descuento }
        set {
            _descuento = newValue
            calcularTotal()
        }
    }

    var impuestos: Float {
        get { return _impuestos }
        set {
            _impuestos = newValue
            calcularTotal()
        }
    }

    var porcentajeImpuestos: Float {
        get { return _porcentajeImpuestos }
        set {
            _porcentajeImpuestos = newValue
            calcularTotal()
        }
    }

    var cargosUnicos: Float {
        get { return _cargosUnicos }
        set {
            _cargosUnicos = newValue
            calcularTotal()
        }
    }

    var subTotal: Float {
        get { return _subTotal }
    }

    var total: Float {
        get { return _total }
    }

    private func calcularTotal() {
        _subTotal = _costoRentaMensual + _costoInstalacion + _costoAdicionales - _descuento
        _impuestos = _porcentajeImpuestos * _subTotal
        _subTotal -= _impuestos
        _total = _subTotal + _impuestos + _cargosUnicos
    }

    func actualizarCostoAdicionales() {
        var suma: Float = 0
        if let sAdicionales = planConfig.first?.serviciosAdicionales {
            for adicional in sAdicionales {
                suma += Float(adicional.cantidad) * adicional.servicioAdicional.precio
            }
        }
        var suma2: Float = 0
        if let pAdicionales = planConfig.first?.productosAdicionales {
            for adicional in pAdicionales {
                for addon in adicional.addons {
                    suma2 += Float(addon.cantidad) * addon.precio
                }
            }
        }
        costoAdicionales = suma + suma2
    }
}
