//
//  DBMetodoPago.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 01/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import RealmSwift

class DBMetodoPago: Object {
    @objc dynamic var anio: String = ""
    @objc dynamic var apellidoMaternoTitular: String = ""
    @objc dynamic var apellidoPaternoTitular: String = ""
    @objc dynamic var digitosTarjeta: String = ""
    @objc dynamic var mes: String = ""
    @objc dynamic var metodoPago: String = ""
    @objc dynamic var nombreTitularTarjeta: String = ""
    @objc dynamic var tipoTarjeta: String = ""
    let resumenCotizacionSitio = LinkingObjects(fromType: DBResumenCotizacionSitio.self, property: "metodoPago")
    
    convenience init(anio: String, digitosTarjeta: String, mes: String, metodoPago: String, tipoTarjeta: String) {
        self.init()
        self.anio = anio
        self.digitosTarjeta = digitosTarjeta
        self.mes = mes
        self.metodoPago = metodoPago
        self.tipoTarjeta = tipoTarjeta
    }
}
