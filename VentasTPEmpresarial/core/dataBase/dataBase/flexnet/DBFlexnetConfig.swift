//
//  DBFlexnetConfig.swift
//  VentasTPEmpresarial
//
//  Created by eduardo mancilla on 2/1/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//
import RealmSwift
import Realm

class DBFlexnetConfig: Object {
    @objc dynamic var ipOrigenDatos: String = ""
    @objc dynamic var ipDestinoDatos: String = ""
    @objc dynamic var listaDominiosBloqueo: String = ""
    @objc dynamic var determinarVLANs: String = ""
    @objc dynamic var salidaInternet: String = ""
    @objc dynamic var porcentajeAnchoBanda: String = ""
    @objc dynamic var determinarSegmentoOrigen: String = ""
    @objc dynamic var segmentosDestinos: String = ""
    @objc dynamic var tipoRouteo: String = ""
    @objc dynamic var areaId: String = ""
    
    convenience init(ipOrigenDatos: String, ipDestinoDatos: String, listaDominiosBloqueo: String, determinarVLANs: String, salidaInternet: String, porcentajeAnchoBanda: String, determinarSegmentoOrigen: String, segmentosDestinos: String, tipoRouteo: String, areaId: String) {
        self.init()
        self.ipOrigenDatos = ipOrigenDatos
        self.ipDestinoDatos = ipDestinoDatos
        self.listaDominiosBloqueo = listaDominiosBloqueo
        self.determinarVLANs = determinarVLANs
        self.salidaInternet = salidaInternet
        self.porcentajeAnchoBanda = porcentajeAnchoBanda
        self.determinarSegmentoOrigen = determinarSegmentoOrigen
        self.segmentosDestinos = segmentosDestinos
        self.tipoRouteo = tipoRouteo
        self.areaId = areaId
    }
}
