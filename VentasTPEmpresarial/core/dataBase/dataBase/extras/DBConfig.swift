//
//  DBConfig.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 26/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import RealmSwift
import Realm

class DBConfig: Object {
    @objc dynamic var id = 0
    @objc dynamic var idEmpleado: String = ""
    @objc dynamic var nombreEmpleado: String = "Defauilt"
    @objc dynamic var posResumenActivo = 0
    @objc dynamic var ultimaHoraDescarga: Date?
    let resumenCotizacionSitios = LinkingObjects(fromType: DBResumenCotizacionSitio.self, property: "config")

    override static func primaryKey() -> String? {
        return "id"
    }

    convenience init(id: Int) {
        self.init()
        self.id = id
    }
}
