//
//  DBTipoFamilia.swift
//  VentasTPEmpresarial
//
//  Created by eduardo mancilla on 11/30/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import RealmSwift
import Realm

class DBTipoFamilia: Object {
    @objc dynamic var id = 0
    @objc dynamic var nombre: String = ""
    let familias = LinkingObjects(fromType: DBFamilia.self, property: "tipo")
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(nombre: String, id: Int) {
        self.init()
        self.nombre = nombre
        self.id = id
    }
    
}
