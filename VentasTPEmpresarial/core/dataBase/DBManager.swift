//
//  DBManager.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 03/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import RealmSwift

class DBManager {
    private var realm: Realm
    static var instancia: DBManager?

    private init() {
        let config = Realm.Configuration(
            schemaVersion: 1,
            migrationBlock: { _, oldSchemaVersion in
                if oldSchemaVersion < 1 {
                    // Apply any necessary migration logic here.
                }
            }, deleteRealmIfMigrationNeeded: true
        )
        Realm.Configuration.defaultConfiguration = config
        realm = try! Realm()
    }

    func getRealm() -> Realm {
        return realm
    }

    func reloadRealm() {
        realm = try! Realm()
    }

    public static func getInstance() -> DBManager {
        if instancia == nil {
            instancia = DBManager()
        }
        return instancia!
    }

    func incrementID<T: Object>(tipo: T.Type) -> Int {
        reloadRealm()
        return (realm.objects(tipo).max(ofProperty: "id") as Int? ?? 0) + 1
    }

    func find<T: Object>(_ tipo: T.Type, _ id: String) -> T? {
        return realm.object(ofType: tipo, forPrimaryKey: id)
    }

    func find<T: Object>(_ tipo: T.Type, _ id: Int) -> T? {
        return realm.object(ofType: tipo, forPrimaryKey: id)
    }

    func find<T: Object>(_ tipo: T.Type, _ field: String, _ value: Any) -> T? {
        return realm.objects(tipo).filter("\(field) == '\(value)'").first
    }

    func all<T: Object>(_ tipo: T.Type) -> Results<T> {
        return realm.objects(tipo)
    }

    func all<T: Object>(_ tipo: T.Type, _ ascending: Bool) -> Results<T> {
        return realm.objects(tipo).sorted(byKeyPath: "id", ascending: ascending)
    }

    func all<T: Object>(_ tipo: T.Type, _ byKeyPath: String, _ ascending: Bool) -> Results<T> {
        return realm.objects(tipo).sorted(byKeyPath: byKeyPath, ascending: ascending)
    }

    func add<T: Object>(_ obj: T) {
        reloadRealm()
        try! realm.write {
            realm.add(obj, update: true)
        }
    }

    func add<T: Object>(_ obj: T, update: Bool) {
        reloadRealm()
        try! realm.write {
            realm.add(obj, update: update)
        }
    }

    func append<T: Object, O: List<T>>(_ obj: O, _ append: T) {
        reloadRealm()
        try! realm.write {
            obj.append(append)
        }
    }

    func deleteAllOf<T: Object>(_ tipo: T.Type) {
        reloadRealm()
        try! realm.write {
            let datos = all(tipo)
            for dato in datos {
                realm.delete(dato)
            }
        }
    }

    func eliminarAllData() {
        reloadRealm()
        try! realm.write {
            realm.deleteAll()
        }
    }

    func borraDatosVenta() {
        reloadRealm()
        try! realm.write {
            let resumenesSitios = all(DBResumenCotizacionSitio.self)
            for resumen in resumenesSitios {
                realm.delete(resumen)
            }
        }
    }

    func borraFamilias(set nuevoSet: Set<String>) {
//        let tipo = DBFamilia.self
//        let todos = all(tipo)
//
//        var antiguoSet = Set<String>()
//        for v in todos{
//            if let id = v.id{
//                antiguoSet.insert(id)
//            }
//        }
//
//        antiguoSet.subtract(nuevoSet)
//        Logger.w("se van a eliminar los ids: \(antiguoSet), pero existen los ids: \(nuevoSet)")
//
//        for familiaId in antiguoSet{
//            if let familia = find(tipo, familiaId){
//                try! realm.write {
//                    realm.delete(familia)
//                }
//            }
//        }
    }

    func borraPlanes(set nuevoSet: Set<String>) {
//        var antiguoSet = Set<String>()
//
//        let tipo = DBPlan.self
//        let todos = all(tipo)
//        for v in todos{
//            if let id = v.id{
//                antiguoSet.insert(id)
//            }
//        }
//
//        antiguoSet.subtract(nuevoSet)
//        Logger.w("se van a eliminar los ids: \(antiguoSet)")
//
//        for planId in antiguoSet{
//            if let plan = find(tipo, planId){
//                //eliminando planes config primero
//                let allConfigPlan = all(DBConfigPlan.self)
//                for planConfig in allConfigPlan{
//                    if let idConfigPlan = planConfig.plan.id{
//                        if idConfigPlan == planId{
//                            try! realm.write {
//                                realm.delete(planConfig)
//                            }
//                        }
//                    }
//                }
//                //eliminando los planes
//                try! realm.write {
//                    realm.delete(plan)
//                }
//            }
//        }
    }

    func borraProductoAdicional(set nuevoSet: Set<String>) {
//        var antiguoSet = Set<String>()
//
//        let tipo = DBProductoAdicional.self
//        let todos = all(tipo)
//        for v in todos{
//            if let id = v.id{
//                antiguoSet.insert(id)
//            }
//        }
//
//        antiguoSet.subtract(nuevoSet)
//        Logger.w("se van a eliminar los ids: \(antiguoSet)")
//
//        for prodAdicId in antiguoSet{
//            if let prodAdic = find(tipo, prodAdicId){
//                //eliminando prod adic config primero
//                let allConfigprodAdic = all(DBConfigProductoAdicional.self)
//                for prodAdicConfig in allConfigprodAdic{
//                    if let idConfigprodAdic = prodAdicConfig.productoAdicional.id{
//                        if idConfigprodAdic == prodAdicId{
//                            try! realm.write {
//                                realm.delete(prodAdicConfig)
//                            }
//                        }
//                    }
//                }
//                //eliminando los prod adic
//                try! realm.write {
//                    realm.delete(prodAdic)
//                }
//            }
//        }
    }

    func borraServicioAdicional(set nuevoSet: Set<String>) {
//        var antiguoSet = Set<String>()
//
//        let tipo = DBServicioAdicional.self
//        let todos = all(tipo)
//        for v in todos{
//            if let id = v.id{
//                antiguoSet.insert(id)
//            }
//        }
//
//        antiguoSet.subtract(nuevoSet)
//        Logger.w("se van a eliminar los ids: \(antiguoSet)")
//
//        for prodAdicId in antiguoSet{
//            if let prodAdic = find(tipo, prodAdicId){
//                //eliminando prod adic config primero
//                let allConfigprodAdic = all(DBConfigServicioAdicional.self)
//                for prodAdicConfig in allConfigprodAdic{
//                    if let idConfigprodAdic = prodAdicConfig.servicioAdicional.id{
//                        if idConfigprodAdic == prodAdicId{
//                            try! realm.write {
//                                realm.delete(prodAdicConfig)
//                            }
//                        }
//                    }
//                }
//                //eliminando los prod adic
//                try! realm.write {
//                    realm.delete(prodAdic)
//                }
//            }
//        }
    }

    func borraProductoAdicionalAddon(set nuevoSet: Set<String>) {
//        var antiguoSet = Set<String>()
//
//        let tipo = DBAddonServicio.self
//        let todos = all(tipo)
//        for v in todos{
//            if let id = v.id{
//                antiguoSet.insert(id)
//            }
//        }
//
//        antiguoSet.subtract(nuevoSet)
//        Logger.w("se van a eliminar los ids: \(antiguoSet)")
//
//        for prodAdicId in antiguoSet{
//            if let prodAdic = find(tipo, prodAdicId){
//                //eliminando prod adic config primero
//                let allConfigprodAdic = all(DBConfigProductoAdicionalAddon.self)
//                for prodAdicConfig in allConfigprodAdic{
//                    if let idConfigprodAdic = prodAdicConfig.addon.id{
//                        if idConfigprodAdic == prodAdicId{
//                            try! realm.write {
//                                realm.delete(prodAdicConfig)
//                            }
//                        }
//                    }
//                }
//                //eliminando los prod adic
//                try! realm.write {
//                    realm.delete(prodAdic)
//                }
//            }
//        }
    }

    func borraFamiliasYPlanes() {
        deleteAllOf(DBFamilia.self) // ya
        deleteAllOf(DBTipoFamilia.self)
        deleteAllOf(DBPlazo.self)

        deleteAllOf(DBPlan.self) // ya
        deleteAllOf(DBServicioAdicional.self) // ya
        deleteAllOf(DBProductoAdicional.self) // ya
        deleteAllOf(DBAddonServicio.self) // ya
        deleteAllOf(DBProductoIncluido.self)

        deleteAllOf(DBConfigPlan.self) // ya
        deleteAllOf(DBConfigProductoAdicional.self) // ya
        deleteAllOf(DBConfigProductoAdicionalAddon.self) // ya
        deleteAllOf(DBConfigServicioAdicional.self) // ya
    }

    func borraDatosCotizacion() {
        reloadRealm()
        try! realm.write {

            let coberturas = all(DbCobertura.self)
            for cobertura in coberturas {
                realm.delete(cobertura)
            }

            let sitios = all(DBSitio.self)
            for sitio in sitios {
                realm.delete(sitio)
            }

            let configPlanes = all(DBConfigPlan.self)
            for configPlan in configPlanes {
                realm.delete(configPlan)
            }

            let resumenesCotPlanes = all(DBResumenCotizacionPlan.self)
            for resumenesCotPlan in resumenesCotPlanes {
                realm.delete(resumenesCotPlan)
            }

            let configServAdicionales = all(DBConfigServicioAdicional.self)
            for configServAdicional in configServAdicionales {
                realm.delete(configServAdicional)
            }

            let configProdAdicionales = all(DBConfigProductoAdicional.self)
            for configProdAdicional in configProdAdicionales {
                realm.delete(configProdAdicional)
            }

            let configProdAdicionalesAddons = all(DBConfigProductoAdicionalAddon.self)
            for configProdAdicionalesAddon in configProdAdicionalesAddons {
                realm.delete(configProdAdicionalesAddon)
            }
        }

        _ = getResumenCotizacionSitio()//crear un nuevo resumen de cotizacion
    }
}

extension DBManager {

    func addFamilia(nombre: String, id: String, tipo: String) {
        reloadRealm()
        let familia = DBFamilia(nombre: nombre, id: id)
        if let plazoObj = all(DBTipoFamilia.self).filter("nombre == '\(tipo)'").first {
            try! realm.write {
                familia.tipo = plazoObj
            }
        } else {
            try! realm.write {
                let id = incrementID(tipo: DBTipoFamilia.self)
                let tipoFam = DBTipoFamilia(nombre: tipo, id: id)
                realm.add(tipoFam, update: false)
                familia.tipo = tipoFam
            }
        }
        add(familia)
    }

    func addPlan(nombrePlan: String, idPlan: String, precioLista: Float, precioListaIVA: Float, precioProntoPago: Float, precioProntoPagoIVA: Float, costoInstalacion: Float, idFamilia: String) {
        reloadRealm()
        let planDB = DBPlan(nombre: nombrePlan, id: idPlan)
        planDB.costoRentaPrecioLista = precioLista
        planDB.costoRentaPrecioListaIVA = precioListaIVA
        planDB.costoRentaProntoPago = precioProntoPago
        planDB.costoRentaProntoPagoIVA = precioProntoPagoIVA
        planDB.costoInstalacion = costoInstalacion
        reloadRealm()
        if let familia = buscarFamiliaPorID(id: idFamilia) {
            try! realm.write {
                realm.add(planDB, update: true)
                familia.planes.append(planDB)
            }
        }
    }

    func agregarResumenCotizacionSitio(id: Int) {
        reloadRealm()
        let resumenCotizacionSitio = realm.object(ofType: DBResumenCotizacionSitio.self, forPrimaryKey: id)
        if resumenCotizacionSitio == nil {
            try! realm.write {
                realm.add(DBResumenCotizacionSitio(id: id), update: false)
            }
        }
    }

    func agregarPlazosPlan(planId: String, plazos: [String]) {
        reloadRealm()
        if let planDB = buscarPlanPorID(id: planId) {
            try! realm.write {
                for plazo in plazos {
                    if let plazoObj = all(DBPlazo.self).filter("nombre == '\(plazo)'").first {
                        planDB.plazos.append(plazoObj)
                    } else {
                        let id = incrementID(tipo: DBPlazo.self)
                        let plazoObj = DBPlazo(nombre: plazo, id: id)
                        realm.add(plazoObj, update: false)
                        planDB.plazos.append(plazoObj)
                    }
                }
            }
        }
    }

    func agregarServicioAdicionalPlan(_ idPlan: String, nombre: String, precio: Float, idAddonServicio: String, maximoAgregar: Int, esSDWAN: Bool) {
        reloadRealm()
        if let plan = buscarPlanPorID(id: idPlan) {
            try! realm.write {
                let obj = DBServicioAdicional(
                    nombre: nombre,
                    precio: precio,
                    id: idAddonServicio
                )
                obj.maximoAgregar = maximoAgregar
                obj.esSDWAN = esSDWAN
                realm.add(obj, update: true)
                plan.serviciosAdicionales.append(obj)
            }
        }
    }

    func agregarAddonProductoAdicional(nombre: String, precio: Float, id: String, maximoAgregar: Int, impuesto: Float, convivenciaIds: [String], idServicio: String) {
        reloadRealm()
        try! realm.write {
            let obj = DBAddonServicio(
                nombre: nombre,
                precio: precio,
                id: id
            )
            realm.add(obj, update: true)
            obj.maximo = maximoAgregar
            obj.impuesto = impuesto
            for convivenciaId in convivenciaIds {
                obj.convivencia.append(convivenciaId)
            }
            if let servicio = realm.object(ofType: DBProductoAdicional.self, forPrimaryKey: idServicio) {
                return servicio.addons.append(obj)
            }
        }
    }

    func agregarPromocion(nombre: String, id: String, descuento: Float, fechaInicio: String, fechaFin: String, convivenciaIds: [String], idPlan: String, idProducto: String, idServicio: String) {
        reloadRealm()
        try! realm.write {
            let obj = DBPromocion(
                nombre: nombre,
                id: id
            )
            realm.add(obj, update: true)
            obj.descuento = descuento
            obj.fechaInicio = fechaInicio
            obj.fechaFin = fechaFin
            obj.idProducto = idProducto
            obj.idServicio = idServicio
            for convivenciaId in convivenciaIds {
                obj.convivencia.append(BDConvivencia(id: convivenciaId))
            }
            if let plan = realm.object(ofType: DBPlan.self, forPrimaryKey: idPlan) {
                plan.promociones.append(obj)
            }
        }
    }
}

extension DBManager {

    func buscarSitioPorID(id: Int) -> DBSitio? {
        reloadRealm()
        return find(DBSitio.self, id)
    }

    func buscarPlanPorID(id: String) -> DBPlan? {
        reloadRealm()
        return find(DBPlan.self, id)
    }

    func buscarFamiliaPorID(id: String) -> DBFamilia? {
        reloadRealm()
        return find(DBFamilia.self, id)
    }

    func buscarPromocionPorID(id idPromocion: String) -> DBPromocion? {
        let promocion = realm.object(ofType: DBPromocion.self, forPrimaryKey: idPromocion)
        return promocion
    }

    func buscarPromociones(idPlan: String) -> List<DBPromocion> {
        if let plan = buscarPlanPorID(id: idPlan) {
            return plan.promociones
        } else {
            return List<DBPromocion>()
        }
    }

    func obtenerPlanesPorFamilia(id: String) -> Results<DBPlan>? {
        reloadRealm()
        guard let familia = find(DBFamilia.self, id) else { return nil }
        var planes = familia.planes.sorted(byKeyPath: "id")

        if let resumenCot = getResumenCotizacionSitio() {
            let plazo = resumenCot.plazo
            planes = planes.filter("ANY plazos.nombre == '\(plazo)'")
        }
        return planes
    }

    func obtenerPlanes() -> Results<DBPlan> {
        reloadRealm()
        return all(DBPlan.self)
    }

    func obtenerFamilias() -> Results<DBFamilia> {
        reloadRealm()
        return all(DBFamilia.self)
    }

    func obtenerFamilias(tipo: String) -> Results<DBFamilia>? {
        reloadRealm()
        let tiposFamilias = all(DBTipoFamilia.self)
        for tipoFam in tiposFamilias {
            if tipoFam.nombre == tipo {
                return tipoFam.familias.sorted(byKeyPath: "id")
            }
        }
        return nil
    }

//    func obtenerFamilias(tipo: String) -> Results<DBFamilia>{
//        reloadRealm()
//
//        let tipoBD = find(DBTipoFamilia.self, "nombre", tipo)!
//        return tipoBD.familias.sorted(byKeyPath: "id")
//    }

    func obtenerProductosAdicionales(_ idPlan: String) -> List<DBProductoAdicional> {
        reloadRealm()
        if let planDB = buscarPlanPorID(id: idPlan) {
            return planDB.productosAdicionales
        }
        return List<DBProductoAdicional>()
    }

    func obtenerProductosAdicionales() -> Results<DBProductoAdicional> {
        reloadRealm()
        return all(DBProductoAdicional.self)
    }

    func obtenerProductosAdicionalesAddons(idServicio: String) -> List<DBAddonServicio> {
        if let promocion = find(DBProductoAdicional.self, idServicio) {
            return promocion.addons
        } else {
            return List<DBAddonServicio>()
        }
    }

    func getResumenCotizacionSitio() -> DBResumenCotizacionSitio? {
        reloadRealm()
//        let config = getConfig()
//        let nSitios = config.resumenCotizacionSitios.count
//        if nSitios > 0{
//            let pos = config.posResumenActivo
//            return config.resumenCotizacionSitios[pos]
//        }else{
//            return agregarResumenCotizacionSitio()
//        }
        let resumenCotizacionSitio = all(DBResumenCotizacionSitio.self, true).last
        if resumenCotizacionSitio == nil {
            Logger.d("no existe un resumen de cotizacion")
            let resumenCot = DBResumenCotizacionSitio(id: 0)
            add(resumenCot)
            Logger.d("se agregó el resumen con id: \(0)")
        }
        return resumenCotizacionSitio
    }

    func agregarResumenCotizacionSitio() -> DBResumenCotizacionSitio? {
        reloadRealm()
        let config = getConfig()
        let id = incrementID(tipo: DBResumenCotizacionSitio.self)
        let resumenCot = DBResumenCotizacionSitio(id: id)
        resumenCot.config = config
        add(resumenCot)
        Logger.d("se agregó el resumen con id: \(id)")
        return resumenCot
    }

    func cambiarResumenCotizacion(posicion: Int) {
        reloadRealm()
        let config = getConfig()
        let nSitios = config.resumenCotizacionSitios.count
        guard posicion >= 0 && posicion < nSitios else {
            return
        }
        try! realm.write {
            config.posResumenActivo = posicion
        }
    }

    func buscarConfigPlan(sitioId: Int, planId: String) -> DBConfigPlan? {
        guard let plan = find(DBPlan.self, planId) else { return nil }
        return plan.planesConfig.filter("ANY sitio.id == \(sitioId)").first
    }
}

extension DBManager {

    func obtenerSitios() -> List<DBSitio> {
        reloadRealm()
        if let resumenCot = getResumenCotizacionSitio() {
            return resumenCot.sitios
        }
        return List<DBSitio>()
        //return all(DBSitio.self, true)
    }

    func restarProductoAddon(idAddon: String, configPlanId: Int, prodAdicId: String) {
        updateAddons(valor: -1, idAddon: idAddon, configPlanId: configPlanId, prodAdicId: prodAdicId)
    }

    func sumarProductoAddon(idAddon: String, configPlanId: Int, prodAdicId: String) {
        updateAddons(valor: 1, idAddon: idAddon, configPlanId: configPlanId, prodAdicId: prodAdicId)
    }

    func updateAddons(valor: Int, idAddon: String, configPlanId: Int, prodAdicId: String) {
        reloadRealm()
        guard let configPlan = find(DBConfigPlan.self, configPlanId) else { return }
        guard let prodAdic = configPlan.productosAdicionales.filter("productoAdicional.id == '\(prodAdicId)'").first else { return }
        guard let configProdAdicAddon = prodAdic.addons.filter("addon.id == '\(idAddon)'").first else { return }
        let suma = configProdAdicAddon.cantidad + valor
        if suma >= 0 && suma <= configProdAdicAddon.addon.maximo {
            try! realm.write {
                configProdAdicAddon.cantidad = suma
                configPlan.resumenCotizacion?.actualizarCostoAdicionales()
                let activo = (suma > 0) ? false : true
                for convivenciaId in configProdAdicAddon.addon.convivencia {
                    if let prodAdic = configProdAdicAddon.productoAdic.first {
                        if let configaddonEdit = prodAdic.addons.filter("addon.id == '\(convivenciaId)'").first {
                            configaddonEdit.isActivo = activo
                        }
                    }
                }
            }
        }
    }

    func obtenerProductosAdicionalesAddons(configPlanId: Int, servicioId: String) -> List<DBConfigProductoAdicionalAddon> {

        if let configPlan = find(DBConfigPlan.self, configPlanId) {
            if let prodAdic = configPlan.productosAdicionales
                .filter("productoAdicional.id == '\(servicioId)'").first {
                return prodAdic.addons
            }
        }
        return List<DBConfigProductoAdicionalAddon>()
    }

    func buscarProductoAdicionalPorId(_ configProdAdicId: Int) -> DBConfigProductoAdicional? {
        return find(DBConfigProductoAdicional.self, configProdAdicId)
    }

    func restarServicioPlan(_ configPlanId: Int, _ idServAdici: String) {
        updateServicioPlan(-1, configPlanId, idServAdici)
    }

    func sumarServicioPlan(_ configPlanId: Int, _ idServAdici: String) {
        updateServicioPlan(1, configPlanId, idServAdici)
    }

    func updateServicioPlan(_ valor: Int, _ configPlanId: Int, _ idServAdici: String) {
        if let configPlan = find(DBConfigPlan.self, configPlanId) {
            if let servAdicConf = configPlan.serviciosAdicionales
                .filter("servicioAdicional.id == '\(idServAdici)'").first {
                let suma = servAdicConf.cantidad + valor
                let maximoValor = servAdicConf.servicioAdicional.maximoAgregar
                if suma >= 0 && suma <= maximoValor {
                    try! realm.write {
                        servAdicConf.cantidad = suma
                        configPlan.resumenCotizacion?.actualizarCostoAdicionales()
                    }
                }
            }
        }
    }

    func obtenerServiciosAdicionales(_ configPlanId: Int) -> List<DBConfigServicioAdicional> {
        if let configPlan = find(DBConfigPlan.self, configPlanId) {
            return configPlan.serviciosAdicionales
        }
        return List<DBConfigServicioAdicional>()
    }

    func buscarConfigPlanPorId(_ configPlanId: Int) -> DBConfigPlan? {
        return find(DBConfigPlan.self, configPlanId)
    }

}

extension DBManager {

    func crearConfigPlan(idConfigPlan: Int) -> DBConfigPlan {
        let idPlanConfig = incrementID(tipo: DBConfigPlan.self)
        if let configPlan = find(DBConfigPlan.self, idConfigPlan) {
            let configPlanNew = DBConfigPlan(plan: configPlan.plan, id: idPlanConfig)
            configPlanNew.serviciosAdicionales = configPlan.serviciosAdicionales
            configPlanNew.productosAdicionales = configPlan.productosAdicionales
            configPlanNew.resumenCotizacion = configPlan.resumenCotizacion
            add(configPlanNew)
            return configPlanNew
        }
        return DBConfigPlan()
    }

    func crearConfigPlan(idPlan: String) -> DBConfigPlan? {
        guard let plan = find(DBPlan.self, idPlan) else { return nil }
        let planConfig = DBConfigPlan(plan: plan, id: 0)//el id cero es para personalizar
        add(planConfig)
        return planConfig
    }

    func crearConfigPlanSolucionesMedida(idPlan: String) -> DBConfigPlan? {
        guard let plan = find(DBPlan.self, idPlan) else { return nil }
        let configPlan = DBConfigPlan(plan: plan, id: 0)//el id cero es para personalizar
        configPlan.productosAdicionales = List<DBConfigProductoAdicional>()
        configPlan.serviciosAdicionales = List<DBConfigServicioAdicional>()
        add(configPlan)
        return configPlan
    }

    func agregarSitioNuevo() {
        reloadRealm()
        let idSitio = incrementID(tipo: DBSitio.self)
        let nombre = " nuevo sitio ( \(idSitio) )"
        let sitioNuevo = DBSitio(nombre: nombre, id: idSitio)
        add(sitioNuevo)

        if let resumenCotizacion = getResumenCotizacionSitio() {
            try! realm.write {
                resumenCotizacion.sitios.append(sitioNuevo)
                resumenCotizacion.actualizarCostos()
            }
        }
    }

    func agregarSitio(id: Int?, nombre: String) {
        reloadRealm()
        let idSitio = (id == nil) ? incrementID(tipo: DBSitio.self) : id!
        let sitioNuevo = DBSitio(nombre: nombre, id: idSitio)
        add(sitioNuevo)

        let configPlan = crearConfigPlan(idConfigPlan: 0)
        try! realm.write {
            sitioNuevo.planesConfig.append(configPlan)
            sitioNuevo.actualizarCostos()
        }
        if let resumenCotizacion = getResumenCotizacionSitio() {
            try! realm.write {
                resumenCotizacion.sitios.append(sitioNuevo)
                resumenCotizacion.actualizarCostos()
            }
        }
    }

    func agregarPlanASitio(idSitio: Int?) {
        reloadRealm()
        guard let idSitio = idSitio else { return }
        guard let sitio = find(DBSitio.self, idSitio) else { return }
        let configPlan = crearConfigPlan(idConfigPlan: 0)
        try! realm.write {
            sitio.planesConfig.append(configPlan)
            sitio.actualizarCostos()
            if let resumenCotizacion = getResumenCotizacionSitio() {
                resumenCotizacion.actualizarCostos()
            }
        }
    }

    func modificarPlanSitio(idConfigPlan: Int, idSitio: Int?) {
        reloadRealm()
        guard let idSitio = idSitio else { return }
        guard let sitio = find(DBSitio.self, idSitio) else { return }
        if let configPlan = find(DBConfigPlan.self, idConfigPlan) {
            if let posicionPlan = sitio.planesConfig.index(of: configPlan) {
                let configPlanNuevo = crearConfigPlan(idConfigPlan: 0)
                try! realm.write {
                    sitio.planesConfig.replace(
                        index: posicionPlan,
                        object: configPlanNuevo
                    )
                    sitio.actualizarCostos()
                    if let resumenCotizacion = getResumenCotizacionSitio() {
                        resumenCotizacion.actualizarCostos()
                    }
                }
            }
        }
    }

    func eliminarPlanSitio(idConfigPlan: Int, idSitio: Int?) {
        reloadRealm()
        guard let idSitio = idSitio else { return }
        guard let sitio = find(DBSitio.self, idSitio) else { return }
        if let configPlan = find(DBConfigPlan.self, idConfigPlan) {
            if let posicionPlan = sitio.planesConfig.index(of: configPlan) {
                try! realm.write {
                    sitio.planesConfig.remove(at: posicionPlan)
                    sitio.actualizarCostos()
                    if let resumenCotizacion = getResumenCotizacionSitio() {
                        resumenCotizacion.actualizarCostos()
                    }
                }
            }
        }
    }

    func eliminarSitio(idSitio: Int?) {
        reloadRealm()
        guard let idSitio = idSitio else { return }
        guard let sitio = find(DBSitio.self, idSitio) else { return }
        try! realm.write {
            realm.delete(sitio)
            if let resumenCotizacion = getResumenCotizacionSitio() {
                resumenCotizacion.actualizarCostos()
            }
        }
    }

    func addSitioCobertura(_ idSitio: Int, _ nombreSitio: String, _ cobertura: Bool, _ codigoPostal: String, _ latitud: Double, _ longitud: Double, _ calle: String, _ numExt: String, _ numInt: String, _ colonia: String, _ estado: String, _ ciudad: String, _ municipio: String, _ entreCalle: String, _ yCalle: String, _ observaciones: String, _ categoryService: String, _ cluster: String, _ distrito: String, _ factibilidad: String, _ plaza: String, _ region: String, _ regionId: String, _ tipoCobertura: String, _ zona: String) {
        reloadRealm()
        let coberturaObj = DbCobertura()
        coberturaObj.cobertura = cobertura
        coberturaObj.codigoPostal = codigoPostal
        coberturaObj.latitud = latitud
        coberturaObj.longitud = longitud
        coberturaObj.calle = calle
        coberturaObj.numExt = numExt
        coberturaObj.numInt = numInt
        coberturaObj.colonia = colonia
        coberturaObj.estado = estado
        coberturaObj.ciudad = ciudad
        coberturaObj.municipio = municipio
        coberturaObj.entreCalle = entreCalle
        coberturaObj.yCalle = yCalle
        coberturaObj.observaciones = observaciones

        coberturaObj.categoryService = categoryService
        coberturaObj.cluster = cluster
        coberturaObj.distrito = distrito
        coberturaObj.factibilidad = factibilidad
        coberturaObj.plaza = plaza
        coberturaObj.region = region
        coberturaObj.regionId = regionId
        coberturaObj.tipoCobertura = tipoCobertura
        coberturaObj.zona = zona
        var nombreSitioP = nombreSitio
        if nombreSitio.isEmpty {
            nombreSitioP = "Sitio \(ciudad) \(calle)  \(numExt) \(numInt)"
        }
        add(coberturaObj, update: false)
        if let sitio = find(DBSitio.self, idSitio) {
            reloadRealm()
            try! realm.write {
                sitio.cobertura = coberturaObj
                sitio.nombre = nombreSitioP
            }
        }
    }

    func addSitioCobertura(_ nombreSitio: String, _ cobertura: Bool, _ codigoPostal: String, _ latitud: Double, _ longitud: Double, _ calle: String, _ numExt: String, _ numInt: String, _ colonia: String, _ estado: String, _ ciudad: String, _ municipio: String, _ entreCalle: String, _ yCalle: String, _ observaciones: String, _ categoryService: String, _ cluster: String, _ distrito: String, _ factibilidad: String, _ plaza: String, _ region: String, _ regionId: String, _ tipoCobertura: String, _ zona: String) {
        reloadRealm()
        let coberturaObj = DbCobertura()
        coberturaObj.cobertura = cobertura
        coberturaObj.codigoPostal = codigoPostal
        coberturaObj.latitud = latitud
        coberturaObj.longitud = longitud
        coberturaObj.calle = calle
        coberturaObj.numExt = numExt
        coberturaObj.numInt = numInt
        coberturaObj.colonia = colonia
        coberturaObj.estado = estado
        coberturaObj.ciudad = ciudad
        coberturaObj.municipio = municipio
        coberturaObj.entreCalle = entreCalle
        coberturaObj.yCalle = yCalle
        coberturaObj.observaciones = observaciones
        coberturaObj.categoryService = categoryService
        coberturaObj.cluster = cluster
        coberturaObj.distrito = distrito
        coberturaObj.factibilidad = factibilidad
        coberturaObj.plaza = plaza
        coberturaObj.region = region
        coberturaObj.regionId = regionId
        coberturaObj.tipoCobertura = tipoCobertura
        coberturaObj.zona = zona
        add(coberturaObj, update: false)
        var nombreSitioP = nombreSitio
        if nombreSitio.isEmpty {
            nombreSitioP = "Sitio \(ciudad) \(calle)  \(numExt) \(numInt)"
        }

        if let resumenCotizacion = getResumenCotizacionSitio() {
            try! realm.write {
                let id = incrementID(tipo: DBSitio.self)
                let sitio = DBSitio(nombre: nombreSitioP, id: id)

                resumenCotizacion.sitios.append(sitio)
                resumenCotizacion.actualizarCostos()
                sitio.cobertura = coberturaObj
            }
        }
    }

    func actualizarPlazoSeleccionado(plazo: String) {
        reloadRealm()
        if let resumenCot = getResumenCotizacionSitio() {
            try! realm.write {
                resumenCot.plazo = plazo
            }
        }
    }

    func getPlazo() -> String {
        reloadRealm()
        if let resumenCot = getResumenCotizacionSitio() {
            return resumenCot.plazo
        }
        return ""
    }

    func hayUnSitioCreado() -> Bool {
        reloadRealm()
        if let resumenCotizacionSitios = getResumenCotizacionSitio() {
            let nSitios = resumenCotizacionSitios.sitios.count
            Logger.d("Hay \(nSitios) sitios creados.")
            if nSitios > 0 {
                Logger.d("hay mas de 1 sitios creado.")
                var hayModelado = false
                for sitio in resumenCotizacionSitios.sitios {
                    if sitio.planesConfig.count > 0 {
                        hayModelado = true
                    }
                }
                return hayModelado
            } else {
                Logger.d("No hay sitios creados.")
            }
        } else {
            Logger.w("No hay un resumen de cotización.")
        }
        return false
//        if let resumenCotizacionSitios = getResumenCotizacionSitio(){
//            let nSitios = resumenCotizacionSitios.sitios.count
//            Logger.d("Hay \(nSitios) sitios creados.")
//            if nSitios == 1 {
//                Logger.d("hay 1 sitio creado.")
//                let sitio = resumenCotizacionSitios.sitios[0]
//                if sitio.planesConfig.count > 0{
//                    Logger.d("hay mas de 0 configplanes.")
//                    return true
//                }else{
//                    Logger.d("hay 0 o menos")
//                    return false
//                }
//            }else if nSitios > 1{
//                Logger.d("hay mas de 1 sitios creado.")
//                res = true
//            }else{
//                Logger.d("hay menos de 1 sitio creado.")
//            }
//        }
//        return false
    }

    func eliminarPlanesEnSitioDiferentePlazo() {
        reloadRealm()
        if let resumenCotizacionSitios = getResumenCotizacionSitio() {
            let plazo = resumenCotizacionSitios.plazo
            for sitio in resumenCotizacionSitios.sitios {
                for plan in sitio.planesConfig {
                    var encontrado = false
                    for plazoPlan in plan.plan.plazos {
                        if plazoPlan.nombre == plazo {
                            encontrado = true
                        }
                    }
                    if encontrado == false {
                        try! realm.write {
                            realm.delete(plan)
                        }
                    }
                }
            }
        }
    }

    func agregarDatosRepresentanteLegal(tipoPersona: String, razonSocial: String, nombre: String, apellidoPaterno: String, apellidoMaterno: String, fechaNacimiento: String, rfc: String, medioContacto: String, tipoIdentificacion: String, numeroIdentificacion: String, celular: String, telefonoPrincipal: String, segundoTelefono: String, correoElectronico: String, segundoCorreo: String, segmentoFacturacion: String, montoFacturacion: String) {
        reloadRealm()
        if let resumenCotizacionSitios = getResumenCotizacionSitio() {
            try! realm.write {
                var prospecto = DBProspecto()
                if resumenCotizacionSitios.prospecto != nil {
                    prospecto = resumenCotizacionSitios.prospecto!
                }
                realm.add(prospecto)
                resumenCotizacionSitios.prospecto = prospecto
                prospecto.tipoPersona = tipoPersona
                prospecto.razonSocial = razonSocial
                prospecto.nombre = nombre
                prospecto.apellidoPaterno = apellidoPaterno
                prospecto.apellidoMaterno = apellidoMaterno
                prospecto.fechaNacimiento = fechaNacimiento
                prospecto.rfc = rfc
                prospecto.medioContacto = medioContacto

                prospecto.tipoIdentificacion = tipoIdentificacion
                prospecto.numeroIdentificacion = numeroIdentificacion
                prospecto.celular = celular
                prospecto.telefonoPrincipal = telefonoPrincipal
                prospecto.segundoTelefono = segundoTelefono
                prospecto.correoElectronico = correoElectronico
                prospecto.segundoCorreo = segundoCorreo
                prospecto.segmento = segmentoFacturacion
                prospecto.montoFacturacion = montoFacturacion
            }
        }
    }

    func agregarDatosEmpresa(nombreDeLaCompania: String, calle: String, referenciaCalle: String, numeroInterior: String, numeroExterior: String, referenciaUrbana: String, estado: String, ciudad: String, delegacionMuni: String, cp: String, colonia: String) {
        reloadRealm()
        if let resumenCotizacionSitios = getResumenCotizacionSitio() {
            try! realm.write {
                var prospecto = DBProspecto()
                if resumenCotizacionSitios.prospecto != nil {
                    prospecto = resumenCotizacionSitios.prospecto!
                    realm.add(prospecto)
                    resumenCotizacionSitios.prospecto = prospecto
                }
                prospecto.nombreDeLaCompania = nombreDeLaCompania
                prospecto.calle = calle
                prospecto.referenciaCalle = referenciaCalle
                prospecto.numeroInterior = numeroInterior
                prospecto.numeroExterior = numeroExterior
                prospecto.referenciaUrbana = referenciaUrbana
                prospecto.estado = estado
                prospecto.ciudad = ciudad
                prospecto.delegacionMuni = delegacionMuni
                prospecto.cp = cp
                prospecto.colonia = colonia
            }
        }
    }

    func agregarDireccionFacturacion(calle: String, ciudad: String, codigoPostalFacturacion: String, colonia: String, delegacionMunicipio: String, estado: String, mismaDirInst: Bool, numExterior: String, numInterior: String) {
        reloadRealm()
        if let resumenCotizacionSitios = getResumenCotizacionSitio() {
            try! realm.write {
                let direccionFact = DBDireccionFacturacion(calle: calle)
                direccionFact.calle = calle
                direccionFact.ciudad = ciudad
                direccionFact.codigoPostalFacturacion = codigoPostalFacturacion
                direccionFact.colonia = colonia
                direccionFact.delegacionMunicipio = delegacionMunicipio
                direccionFact.estado = estado
                direccionFact.mismaDirInst = mismaDirInst
                direccionFact.numExterior = numExterior
                direccionFact.numInterior = numInterior
                realm.add(direccionFact)
                resumenCotizacionSitios.direccionFacturacion = direccionFact
            }
        }
    }

    func agregarMetodoPago(anio: String, digitosTarjeta: String, mes: String, metodoPago: String, tipoTarjeta: String) {
        reloadRealm()
        if let resumenCotizacionSitios = getResumenCotizacionSitio() {
            try! realm.write {
                let metodoPago = DBMetodoPago(
                    anio: anio,
                    digitosTarjeta: digitosTarjeta,
                    mes: mes,
                    metodoPago: metodoPago,
                    tipoTarjeta: tipoTarjeta
                )
                realm.add(metodoPago)
                resumenCotizacionSitios.metodoPago = metodoPago
            }
        }
    }

    func actualizarTitularTarjeta(_ nombre: String, _ apellidoPaterno: String, _ apellidoMaterno: String) {
        reloadRealm()
        if let metodoPago = getResumenCotizacionSitio()?.metodoPago {
            try! realm.write {
                metodoPago.apellidoMaternoTitular = apellidoMaterno
                metodoPago.apellidoPaternoTitular = apellidoPaterno
                metodoPago.nombreTitularTarjeta = nombre
            }
        }
    }

    func obtenerProspecto() -> DBProspecto? {
        reloadRealm()
        if let resumenCotizacionSitios = getResumenCotizacionSitio() {
            return resumenCotizacionSitios.prospecto
        }
        return nil
    }

    func getConfig() -> DBConfig {
        reloadRealm()
        if let config = find(DBConfig.self, 0) {
            return config
        } else {
            let dbConfig = DBConfig(id: 0)
            add(dbConfig, update: false)
            return dbConfig
        }
    }

    func actualizarIdEmpleado(_ idEmpleado: String) {
        reloadRealm()
        let config = getConfig()
        try! realm.write {
            config.idEmpleado = idEmpleado
        }
    }

    func actualizarNombreEmpleado(_ nombreEmpleado: String) {
        reloadRealm()
        let config = getConfig()
        try! realm.write {
            config.nombreEmpleado = nombreEmpleado
        }
    }

    func getNombreEmpleado() -> String {
        reloadRealm()
        let config = getConfig()
        return config.nombreEmpleado
    }

    func validarSitiosConcobertura() -> Int {
        reloadRealm()
        var posicion = -1
        if let sitios = getResumenCotizacionSitio()?.sitios {
            for (pos, sitio) in sitios.enumerated() {
                if sitio.cobertura == nil {
                    posicion = pos
                    break
                }
            }
        }
        return posicion
    }

    func validarSitiosConPlanes() -> Int {
        reloadRealm()
        var posicion = -1
        if let sitios = getResumenCotizacionSitio()?.sitios {
            for (pos, sitio) in sitios.enumerated() {
                if sitio.planesConfig.count <= 0 {
                    posicion = pos
                    break
                }
            }
        }
        return posicion
    }

    func noHaysitios() -> Bool {
        reloadRealm()
        if let resumenCotizacionSitios = getResumenCotizacionSitio() {
            return resumenCotizacionSitios.sitios.count <= 0
        }
        return true
    }

    func insertarUltimaHoraDescarga(_ hora: Date) {
        reloadRealm()
        let config = getConfig()
        try! realm.write {
            config.ultimaHoraDescarga = hora
        }
    }

    func getUltimaHoraDescarga() -> Date {
        reloadRealm()

        if let ultimaHoraDescarga = getConfig().ultimaHoraDescarga {
            return ultimaHoraDescarga
        } else {
            let currentDate = Date()
            let calendar = Calendar.current
            let futureDate = calendar.date(byAdding: .hour, value: -24, to: currentDate)
            return futureDate!
        }
    }

    func obtenerProductosAdicionales(configPlanId: Int) -> List<DBProductoAdicional> {
        reloadRealm()
        if let configPlan = find(DBConfigPlan.self, configPlanId) {
            return configPlan.plan.productosAdicionales
        }
        return List<DBProductoAdicional>()
    }

    func obtenerConfigProductosAdicionales(configPlanId: Int) -> List<DBConfigProductoAdicional> {
        reloadRealm()
        if let configPlan = find(DBConfigPlan.self, configPlanId) {
            return configPlan.productosAdicionales
        }
        return List<DBConfigProductoAdicional>()
    }

    func obtenerConfigProductosAdicionalesAddons(configPlanId: Int, posicion: Int) -> List<DBConfigProductoAdicionalAddon> {
        reloadRealm()
        if let configPlan = find(DBConfigPlan.self, configPlanId) {
            return configPlan.productosAdicionales[posicion].addons
        }
        return List<DBConfigProductoAdicionalAddon>()
    }

    func aumentarConfigProductosAdicionalesAddons(configPlanId: Int, posicion: Int, posicionAddon: Int) {
        reloadRealm()
        if let configPlan = find(DBConfigPlan.self, configPlanId) {
            let configAddon = configPlan.productosAdicionales[posicion].addons[posicionAddon]
            try! realm.write {
                configAddon.cantidad += 1
                configPlan.resumenCotizacion?.actualizarCostoAdicionales()
            }
        }
    }

    func disminuirConfigProductosAdicionalesAddons(configPlanId: Int, posicion: Int, posicionAddon: Int) {
        reloadRealm()
        if let configPlan = find(DBConfigPlan.self, configPlanId) {
            let configAddon = configPlan.productosAdicionales[posicion].addons[posicionAddon]
            if configAddon.cantidad - 1 >= 0 {
                try! realm.write {
                    configAddon.cantidad -= 1
                    configPlan.resumenCotizacion?.actualizarCostoAdicionales()
                }
            }
        }
    }

    func agregarProductosAdicionalConfig(configPlanId: Int, servicioId: String) {
        reloadRealm()
        if let configPlan = find(DBConfigPlan.self, configPlanId) {
            if let productoAdic = find(DBProductoAdicional.self, servicioId) {
                try! realm.write {
                    let prodAdicConfig = DBConfigProductoAdicional(productoAdicional: productoAdic)
                    for addon in productoAdic.addons {
                        prodAdicConfig.addons.append(
                            DBConfigProductoAdicionalAddon(addon: addon)
                        )
                    }
                    configPlan.productosAdicionales.append(prodAdicConfig)
                    configPlan.resumenCotizacion?.actualizarCostoAdicionales()
                }
            }
        }
    }

    func eliminarProductosAdicionalConfig(configPlanId: Int, posicion: Int) {
        reloadRealm()
        if let configPlan = find(DBConfigPlan.self, configPlanId) {
            let productoAdic = configPlan.productosAdicionales[posicion]
            try! realm.write {
                realm.delete(productoAdic)
                configPlan.resumenCotizacion?.actualizarCostoAdicionales()
            }
        }
    }

    func agregarProductoAdicional(nombre: String, precio: Float, id: String, idPlan: String) {
        reloadRealm()
        let servicioDB = DBProductoAdicional(
            nombre: nombre,
            precio: precio,
            id: id
        )
        try! realm.write {
            realm.add(servicioDB, update: true)
            if let plan = find(DBPlan.self, idPlan) {
                plan.productosAdicionales.append(servicioDB)
            }
        }
    }

    func agregarAddonProducto(nombre: String, precio: Float, id: String, idProdAdic: String) {
        reloadRealm()
        let addonDB = DBAddonServicio(
            nombre: nombre,
            precio: precio,
            id: id
        )
        try! realm.write {
            realm.add(addonDB, update: true)
            if let productoAdic = find(DBProductoAdicional.self, idProdAdic) {
                productoAdic.addons.append(addonDB)
            }
        }
    }

    func getConfigProductosAdicionalesAddons(configPlanId: Int, posicion: Int, posicionAddon: Int) -> DBConfigProductoAdicionalAddon? {
        reloadRealm()
        if let configPlan = find(DBConfigPlan.self, configPlanId) {
            let configAddon = configPlan.productosAdicionales[posicion].addons[posicionAddon]
            return configAddon
        }
        return nil
    }

    func editarPrecio(configPlanId: Int, posicion: Int, posicionAddon: Int, precio: Float) {
        reloadRealm()
        if let configPlan = find(DBConfigPlan.self, configPlanId) {
            let configAddon = configPlan.productosAdicionales[posicion].addons[posicionAddon]
            try! realm.write {
                configAddon.precio = precio
                configPlan.resumenCotizacion?.actualizarCostoAdicionales()
            }
        }
    }

    func agregarDescuentoConfigPlan(_ configPlanId: Int, descuento: Float) {
        if let plan = find(DBConfigPlan.self, configPlanId) {
            if let resumen = plan.resumenCotizacion {
                try! realm.write {
                    resumen.descuento = plan.plan.costoRentaPrecioLista * descuento / 100
                    resumen.porcentajeDescuento = descuento
                }
            }
        }
    }

    func actualizarConfigFlexNet(servicio: DBConfigServicioAdicional, flexnetModel: DialogoFlexNetModel) {
        let config = DBFlexnetConfig(
            ipOrigenDatos: flexnetModel.ipOrigenQOS,
            ipDestinoDatos: flexnetModel.ipDestinoQOS,
            listaDominiosBloqueo: flexnetModel.listadoDominios,
            determinarVLANs: flexnetModel.vlansServiciosCliente,
            salidaInternet: flexnetModel.salidaInternet,
            porcentajeAnchoBanda: flexnetModel.porcentajeAnchoBanda,
            determinarSegmentoOrigen: flexnetModel.segmentoOrigen,
            segmentosDestinos: flexnetModel.segmentoDestino,
            tipoRouteo: flexnetModel.tipoRouteo,
            areaId: flexnetModel.areaID
        )

        try! realm.write {
            servicio.configFlexnet = config
        }
    }

    func updateIVAResumen(_ configPlanId: Int, valor: Float) {
        if let plan = buscarConfigPlanPorId(configPlanId) {
            if let resumenCotizacion = plan.resumenCotizacion {
                try! realm.write {
                    resumenCotizacion.porcentajeImpuestos = valor
                }
            }
        }
    }

    func verificaFactibilidadSitios() -> Bool {
        if let sitios = getResumenCotizacionSitio()?.sitios {
            for sitio in sitios {
                if let cobertura = sitio.cobertura {
                    if cobertura.cobertura == false {
                        return false
                    }
                }
                for plan in sitio.planesConfig {
                    if esSolucionesAlaMedia(plan.plan.id ?? "") {
                        return false
                    }
                }
            }
        }
        return true
    }

    func getTipoFamiliaPlan(configPlanId: Int) -> String? {
        reloadRealm()
        if let plan = buscarConfigPlanPorId(configPlanId) {
            if let tipo = plan.plan.familia.first?.tipo {
                return tipo.nombre
            }
        }
        return nil
    }

    func esSolucionesAlaMedia(_ id: String) -> Bool {
        reloadRealm()
        if let plan = buscarPlanPorID(id: id) {
            if let tipo = plan.familia.first?.tipo?.nombre {
                return tipo == "Soluciones a la medida"
            }
        }
        return false
    }

    func getFamiliaSolucionesAlaMedia() -> DBFamilia? {
        reloadRealm()
        let tiposFamilias = all(DBTipoFamilia.self)
        for tipo in tiposFamilias {
            if tipo.nombre == "Soluciones a la medida" {
                return tipo.familias.first
            }
        }
        return nil
    }

    func getPlanSolucionesAlAMedida() -> DBPlan? {
        reloadRealm()
        if let familia = getFamiliaSolucionesAlaMedia() {
            if familia.planes.count > 0 {
                return familia.planes.first
            } else {
                Logger.d("No hay planes para la familia soluciones a la medida.")
            }
        } else {
            Logger.w("No se ha podido encontrar la familias soluciones a la medida.")
        }
        return nil
    }
}
