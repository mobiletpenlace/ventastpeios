//
//  CustomChart.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 11/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftCharts

struct ChartItem {
    let datos: [Double]
    let titulo: String
}

protocol CustomChartDelegate {
    func getChart(viewContainer: UIView, datos: [ChartItem], chart: Chart?) -> Chart
}

extension CustomChartDelegate {

    private func createColorView(color: UIColor) -> UIView {
        let cuadroColor = UIView()
        cuadroColor.backgroundColor = color
        cuadroColor.widthAnchor.constraint(equalToConstant: 20.0).isActive = true
        cuadroColor.heightAnchor.constraint(equalToConstant: 20.0).isActive = true
        return cuadroColor
    }

    private func createLabel(text: String) -> UILabel {
        let textLabel = UILabel()
        textLabel.widthAnchor.constraint(equalToConstant: 70).isActive = true
        textLabel.heightAnchor.constraint(equalToConstant: 100.0).isActive = true
        textLabel.text = text
        //textLabel.numberOfLines = 4
        textLabel.textAlignment = .left
        textLabel.font = UIFont(name: "Montserrat", size: 10)!
        return textLabel
    }

    private func createStack(space: CGFloat, orientation: UILayoutConstraintAxis, subviews: [UIView]) -> UIStackView {
        let stackView = UIStackView()
        stackView.axis = orientation
        stackView.distribution = UIStackViewDistribution.equalSpacing
        stackView.alignment = UIStackViewAlignment.fill
        stackView.spacing = space
        for subview in subviews {
            stackView.addArrangedSubview(subview)
        }
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }

    private func createItem(color: UIColor, text: String) -> UIStackView {
        let colorV = createColorView(color: color)
        let labelV = createLabel(text: text)
        let stackView1 = createStack(space: 5, orientation: .horizontal, subviews: [colorV, labelV])
        return stackView1
    }

    func addDescripcion(container: UIView, _ titulos: [String]) {
        let coloresGrafica = Colores.coloresGrafica
        guard titulos.count <= coloresGrafica.count else {
            Logger.s("error: el numero de colores es menor que el numero de titulos")
            return
        }

        var subviews = [UIStackView]()
        for (i, v) in titulos.enumerated() {
            subviews.append(createItem(color: coloresGrafica[i], text: v))
        }
        let stackView = createStack(space: 10, orientation: .horizontal, subviews: subviews)
        container.addSubview(stackView)
        stackView.centerXAnchor.constraint(equalTo: container.centerXAnchor).isActive = true
        stackView.centerYAnchor.constraint(equalTo: container.centerYAnchor).isActive = true

    }

    private func getChartFrame(viewContainer: UIView) -> CGRect {
        let bounds = viewContainer.bounds
        return CGRect(x: bounds.origin.x, y: bounds.origin.y, width: bounds.size.width, height: bounds.size.height
        )
    }

    private func getChartSettings() -> ChartSettings {
        var chartSettings = ChartSettings()
        chartSettings.leading = 10
        chartSettings.top = 50
        chartSettings.trailing = 10
        chartSettings.bottom = 10
        chartSettings.labelsToAxisSpacingX = 10
        chartSettings.labelsToAxisSpacingY = 10
        chartSettings.axisTitleLabelsToLabelsSpacing = 10
        chartSettings.axisStrokeWidth = 0.5
        chartSettings.spacingBetweenAxesX = 5
        chartSettings.spacingBetweenAxesY = 5
        chartSettings.labelsSpacing = 0

        chartSettings.zoomPan.panEnabled = true
        chartSettings.zoomPan.zoomEnabled = true

        chartSettings.zoomPan.maxZoomX = 4
        chartSettings.zoomPan.minZoomX = 1
        chartSettings.zoomPan.minZoomY = 1
        chartSettings.zoomPan.maxZoomY = 2
        return chartSettings
    }

    private func getLabelSettings() -> ChartLabelSettings {
        //let font: UIFont = UIFont(name: "Montserrat", size: 12)!
        let font: UIFont = UIFont(name: "Montserrat-SemiBold", size: 12)!
        return ChartLabelSettings(font: font)
    }

    private func getChartStackedBarModel(_ order: Int, _ valores: [Double], _ titulo: String, _ zero: ChartAxisValueDouble, _ labelSettings: ChartLabelSettings) -> ChartStackedBarModel {
        return ChartStackedBarModel(
            constant: ChartAxisValueString(
                titulo,
                order: order,
                labelSettings: labelSettings
            ),
            start: zero,
            items: getItems(valores: valores)
        )
    }

    private func getItems(valores: [Double]) -> [ChartStackedBarItemModel] {
        let colores = Colores.coloresGrafica
        guard valores.count <= colores.count else {
            Logger.s("el numero de valores es diferente al numero de colores")
            return [ChartStackedBarItemModel]()
        }

        var items = [ChartStackedBarItemModel]()
        for (i, v) in valores.enumerated() {
            items.append(ChartStackedBarItemModel(quantity: v, bgColor: colores[i]))
        }
        return items
    }

    private func getAxis(datos: [ChartItem]) -> (ChartAxisModel, ChartAxisModel, [ChartStackedBarModel]) {
        let labelSettings = getLabelSettings()
        let zero = ChartAxisValueDouble(0)

        var barModels = [ChartStackedBarModel]()
        var maxValue = 0.0
        for (i, v) in datos.enumerated() {
            barModels.append(getChartStackedBarModel(i + 1, v.datos, v.titulo, zero, labelSettings))
            let suma = v.datos.reduce(0, +)
            if suma > maxValue {
                maxValue = suma
            }
        }

        let stepV = 5

        let valorMinimo = 0
        let valorMaximo = Int(maxValue) + stepV

        let orderInit = 0
        let orderEnd = datos.count + 1

        let (axisValues1, axisValues2) = (
            stride(from: valorMinimo, through: valorMaximo, by: stepV).map {
                ChartAxisValueDouble(Double($0), labelSettings: labelSettings)
            },
            [ChartAxisValueString("", order: orderInit, labelSettings: labelSettings)] +
                barModels.map { $0.constant } +
                [ChartAxisValueString("", order: orderEnd, labelSettings: labelSettings)]
        )
        let (xValues, yValues) = (axisValues2, axisValues1)
        let xModel = ChartAxisModel(axisValues: xValues, axisTitleLabel: ChartAxisLabel(text: "Fecha de cierre", settings: labelSettings))
        let yModel = ChartAxisModel(axisValues: yValues, axisTitleLabel: ChartAxisLabel(text: "Suma de importe \n (millones)", settings: labelSettings.defaultVertical()))
        let axis = (xModel, yModel, barModels)
        return axis
    }

    func getChart(viewContainer: UIView, datos: [ChartItem], chart: Chart?) -> Chart {
        let (xModel, yModel, barModels) = getAxis(datos: datos)
        let coordsSpace = ChartCoordsSpaceLeftBottomSingleAxis(
            chartSettings: getChartSettings(),
            chartFrame: getChartFrame(viewContainer: viewContainer),
            xModel: xModel,
            yModel: yModel
        )

        let (xAxisLayer, yAxisLayer, innerFrame) = (coordsSpace.xAxisLayer, coordsSpace.yAxisLayer, coordsSpace.chartInnerFrame)

        let barViewSettings = ChartBarViewSettings(animDuration: 2)

        let chartStackedBarsLayer = ChartStackedBarsLayer(
            xAxis: xAxisLayer.axis,
            yAxis: yAxisLayer.axis,
            innerFrame: innerFrame,
            barModels: barModels,
            horizontal: false,
            barWidth: 25,
            settings: barViewSettings,
            stackFrameSelectionViewUpdater: ChartViewSelectorAlpha(
                selectedAlpha: 1,
                deselectedAlpha: 0.9
            )
        ) { tappedBar in
            guard let stackFrameData = tappedBar.stackFrameData else { return }

            let chartViewPoint = tappedBar.layer.contentToGlobalCoordinates(
                CGPoint(
                    x: tappedBar.barView.frame.midX,
                    y: stackFrameData.stackedItemViewFrameRelativeToBarParent.minY
                )
            )!
            let viewPoint = CGPoint(x: chartViewPoint.x, y: chartViewPoint.y + 30)
            let infoBubble = InfoBubble(
                point: viewPoint,
                preferredSize: CGSize(width: 70, height: 40),
                superview: viewContainer,
                text: "\(stackFrameData.stackedItemModel.quantity)",
                font: UIFont.systemFont(ofSize: 13),
                textColor: UIColor.white,
                bgColor: UIColor.black.withAlphaComponent(0.6)
            )
            infoBubble.tapHandler = {
                infoBubble.animateBound(view: infoBubble) { _ in
                    infoBubble.removeFromSuperview()
                }
            }
            Timer.scheduledTimer(withTimeInterval: 1, repeats: false) {_ in
                infoBubble.animateBound(view: infoBubble) {_ in
                    infoBubble.removeFromSuperview()
                }
            }
            viewContainer.addSubview(infoBubble)
        }

        let settings = ChartGuideLinesDottedLayerSettings(linesColor: UIColor.black, linesWidth: 0.2)
        let guidelinesLayer = ChartGuideLinesDottedLayer(xAxisLayer: xAxisLayer, yAxisLayer: yAxisLayer, settings: settings)

        if chart != nil {
            chart!.clearView()
        }
        let chart = Chart(
            frame: getChartFrame(viewContainer: viewContainer),
            innerFrame: innerFrame,
            settings: getChartSettings(),
            layers: [
                xAxisLayer,
                yAxisLayer,
                guidelinesLayer,
                chartStackedBarsLayer
            ]
        )
        viewContainer.addSubview(chart.view)
        return chart
    }
}
