//
//  BaseItemFotoFirmaVC.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 06/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class BaseItemFotoFirmaVC: BaseItemVC {
    var imagePicker: UIImagePickerController?
    var imageViewShow: UIImageView!
    let customdialogImage = DialogImageVC()
    let customdialogFirma = DialogFirmaVC()
    var nombreImagen: String = ""

    func showImageDialog(titulo: String, imagen: UIImage) {
        customdialogImage.iniciar(titulo: titulo, imagen: imagen)
        showDialog(customAlert: customdialogImage)
    }

    func showFirmaDialog(titulo: String, view: UIImageView, nombreFile: String) {
        customdialogFirma.iniciar(titulo: titulo, view: view, nombreFile: nombreFile)
        showDialog(customAlert: customdialogFirma)
    }

}

extension BaseItemFotoFirmaVC: UINavigationControllerDelegate, UIImagePickerControllerDelegate, BaseImageWriteReadDelegate {

    func importaGaleria(_ imageView: UIImageView, nombreImagen: String) {
        imageViewShow = imageView
        self.nombreImagen = nombreImagen
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            imagePicker = UIImagePickerController()
            imagePicker!.delegate = self
            imagePicker!.sourceType = .photoLibrary
            self.present(imagePicker!, animated: true, completion: nil)
        }
    }

    func tomarFoto(_ imageView: UIImageView, nombreImagen: String) {
        if imagePicker == nil {
            imageViewShow = imageView
            self.nombreImagen = nombreImagen
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePicker = UIImagePickerController()
                imagePicker!.delegate = self
                imagePicker!.sourceType = .camera
                imagePicker!.allowsEditing = false
                self.present(imagePicker!, animated: true, completion: nil)
            } else {
                showAlert(titulo: "Error", mensaje: "No hay cámara disponible")
            }
        }
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
        imagePicker!.dismiss(animated: true, completion: nil)
        if let imagen = info[UIImagePickerControllerOriginalImage] as? UIImage {
            guardarImagen(img: imagen, nombre: nombreImagen)
            //imageViewShow.image = imagen
            imageViewShow.image = leerImagen(nombre: nombreImagen)
            SwiftEventBus.post("ImagenTomada", sender: imageViewShow)
        }
        imagePicker = nil
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imagePicker!.dismiss(animated: true, completion: nil)
        Logger.w("la imagen quiere ser cancelada...")
    }
}
