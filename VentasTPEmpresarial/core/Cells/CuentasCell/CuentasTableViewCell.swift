//
//  CuentasTableViewCell.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 08/01/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class CuentasTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nombreCuentaLabel: UILabel!
    var index : IndexPath!
    

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func ActionLead(_ sender: Any) {
        SwiftEventBus.post("Acciones_Cuenta", sender: index)
        
    }
    
}
