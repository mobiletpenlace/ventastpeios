//
//  TablaLeadCell.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 07/12/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class TablaLeadCell: UITableViewCell {

    @IBOutlet weak var NameCompany: UILabel!
    var index : IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func ActionLead(_ sender: Any) {
        SwiftEventBus.post("Acciones_Lead", sender: index)
        
    }
    
}
