//
//  tablaVentasCell.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 24/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class TablaVentasCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var etapaLabel: UILabel!
    @IBOutlet weak var statusView: UIImageView!
    @IBOutlet weak var estatusLabel: UILabel!
    @IBOutlet weak var numeroLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
