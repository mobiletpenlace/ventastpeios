import UIKit
import SwiftEventBus

class DetalleSitiosCell: UITableViewCell {
    @IBOutlet weak var titulo: UILabel!
    @IBOutlet weak var texto: UILabel!
    @IBOutlet weak var imagenCobertura: UIImageView!
    @IBOutlet weak var precio: UILabel!
    @IBOutlet weak var stackviewLabel: UIStackView!
    var index: IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        texto.isHidden = false// el texto por default oculto
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    @IBAction func showAction(_ sender: Any) {
        SwiftEventBus.post("DetalleSitio_Editar", sender: index)
    }
}
