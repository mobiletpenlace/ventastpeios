//
//  DiaCitaCollectionViewCell.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 12/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class DiaCitaCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var labelDiaCita: UILabel!
    
    @IBOutlet weak var viewContainer: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
