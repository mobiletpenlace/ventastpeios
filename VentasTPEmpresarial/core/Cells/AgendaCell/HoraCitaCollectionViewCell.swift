//
//  HoraCitaCollectionViewCell.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 13/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class HoraCitaCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var labelHoraInicio: UILabel!
    @IBOutlet weak var labelHoraFin: UILabel!
    @IBOutlet weak var labelNombre: UILabel!
    @IBOutlet weak var labelAsunto: UILabel!
    @IBOutlet weak var viewCita: UIView!
    var activa: Bool = true

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func ocultar(_ value: Bool) {
        activa = !value
        if value {
            viewCita.alpha = 0
        } else {
            viewCita.alpha = 1
        }
    }
}
