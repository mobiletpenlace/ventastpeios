//
//  CoberturaCell.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 12/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class ConCoberturaCell: UICollectionViewCell {
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var viewButtonContainer: UIView!
    @IBOutlet weak var viewButtonModificar: UIView!
    @IBOutlet weak var viewButtonContainer2: UIView!
    @IBOutlet weak var viewButtonEliminar: UIView!
    @IBOutlet weak var labelCiudad: UILabel!
    @IBOutlet weak var labelCitio: UILabel!
    @IBOutlet weak var labelPlaza: UILabel!
    @IBOutlet weak var labelZona: UILabel!
    @IBOutlet weak var labelCluster: UILabel!
    @IBOutlet weak var labelDistrito: UILabel!
    @IBOutlet weak var labelMedioDeAcceso: UILabel!
    @IBOutlet weak var labelDatosDeUbicacion: UILabel!
    var ubicacion: ConfirmaUbicacionModel!
    var posLoc: Int!
    var posGen: Int!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewContainer.layer.borderColor = Colores.bordeCobertura.cgColor
        viewContainer.layer.borderWidth = 1
        viewContainer.layer.cornerRadius = 10
        viewButtonContainer.layer.borderColor = Colores.bordeCobertura.cgColor
        viewButtonContainer.layer.borderWidth = 1
        viewButtonContainer.layer.cornerRadius = 5
        viewButtonContainer2.layer.borderColor = Colores.bordeCobertura.cgColor
        viewButtonContainer2.layer.borderWidth = 1
        viewButtonContainer2.layer.cornerRadius = 5
    }

    @IBAction func clickModificar(_ sender: UIButton) {
        sender.animateBound(view: viewButtonModificar)
        SwiftEventBus.post("ConfirmaUbicacion-editar", sender: ubicacion)
        SwiftEventBus.post("ConfirmaUbicacion-editar(redirigir)", sender: nil)
        SwiftEventBus.post("ConfirmaUbicacion-editar-posLoc", sender: posLoc)
        SwiftEventBus.post("ConfirmaUbicacion-editar-posGen", sender: posGen)
    }
    
    @IBAction func clickEliminar(_ sender: UIButton) {
        sender.animateBound(view: viewButtonEliminar)
        SwiftEventBus.post("ConfirmaUbicacion-eliminar-posLoc", sender: posLoc)
        SwiftEventBus.post("ConfirmaUbicacion-eliminar-posGen", sender: posGen)
        SwiftEventBus.post("ConfirmaUbicacion-eliminar-ConCovertura", sender: posGen)
    }
    
    @IBAction func ClickMapaAction(_ sender: Any) {
        SwiftEventBus.post("ConfirmaUbicacion-PopMapa", sender: ubicacion)
    }
}
