//
//  DialogEditaPlanesCell.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 29/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class DialogEditaPlanesCell: UITableViewCell {
    @IBOutlet weak var texto: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
