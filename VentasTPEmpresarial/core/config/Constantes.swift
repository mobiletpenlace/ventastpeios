//
//  Constantes.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 08/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

struct Constantes {
    struct Url {
        struct Middleware {
            struct Qa {
                static let serverProd = "https://mss.totalplay.com.mx"
                static let serverProdQA = "https://msstest.totalplay.com.mx"
                
                static let server = serverProd
                
                static let login = serverProd + "/ventasmovilEmpresarial/LoginSF" //-----login-----
                
                static let factibilidad = server + "/ventasEmpresarial/FactibilidadMDL"
                
                static let familias = server + "/ventasEmpresarial/DP_Familias"
                static let planes = server + "/ventasEmpresarial/DP_Planes"
                static let detallePlan = server + "/ventasEmpresarial/planProductoServicio"
                static let serviciosAdicionales = server + "/ventasEmpresarial/addOnsServicios"
                static let productosAdicionalesAddons = server + "/ventasEmpresarial/AddOnsProductos"
                static let solucionesMedida = server + "/ventasEmpresarial/DetalleSolucionMedida"
                
                static let crearProspecto = server + "/ventasEmpresarial/CrearProspecto"
                static let misLeads = server + "/ventasEmpresarial/LeadEmpresarial"
                static let crearOportunidad = server + "/ventasEmpresarial/CrearOportunidad"
                static let misCuentas = server + "/ventasEmpresarial/MisCuentasEmpresarial"
                static let misVentas = server + "/ventasEmpresarial/MisVentas"
                static let creaCuenta = server + "/ventasEmpresarial/CreacionCuentaComercial"
                static let paternidad = server + "/ventasEmpresarial/PaternidadCuenta"
                
                static let generaContrato = server + "/ventasEmpresarial/GenerarContrato"
                static let metodoPago = server + "/ventasEmpresarial/MetodoPagoEmpresarial"
                static let descargaContrato = server + "/ventasEmpresarial/DescargaContrato"
                static let enviarContrato = server + "/ventasEmpresarial/EnviarContrato"
                static let crearCotizacion = server + "/ventasEmpresarial/crearCotizacion"
                
                static let subirDocumentos = server + "/ventasEmpresarial/SubirDocumentos"
                static let agenda = server + "/ventasEmpresarial/Agenda"
                
                static let consultaTareas = server + "/ventasEmpresarial/ConsultarTrabajo"
                
                static let graficasMesaControl = server + "/ventasEmpresarial/GraficaMesaControl"
                //                static let graficasMesaControl = server + "/ventasEmpresarial/GraficaCotizacion"
            }

            struct Prod {
                static let server = "https://mss.totalplay.com.mx"
                static let consultaCobertura = server + "/ventasmovil/Cobertura"
                static let consultaCp = server + "/ventasmovil/ConsultaCP"
            }
        }

        struct SalesFor {
            static let server = "https://cs3.salesforce.com"
            static let path = server + "/services/apexrest/"
            
            static let planes = path + "WS_DP_Planes"
            static let familias = path + "WS_DP_Familias"
            static let detallePlan = path + "WS_Plan_Producto_Servicios"
            static let serviciosAdicionales = path + "WS_AddonsServicios"
            static let productosAdicionalesAddons = path + "WS_WS_AddonsProductos"
            static let solucionesMedida = path + "WS_DetalleSolucionesMedida"
            static let crearProspecto = path + "WS_CrearProspecto"
            static let misLeads = path + "WS_LeadEmpresarial"
            static let crearOportunidad = path + "ws_CreacionOportunidad"
            static let misCuentas = path + "WS_MisCuentasEmpresarial"
            static let misVentas = path + "WS_MisVentas_Empresarial"
            static let creaCuenta = path + "WS_CreacionCuentaComercial"
            static let paternidad = path + "WS_PaternidadCuenta"
            
            static let generaContrato = path + "WS_GenerarContrato"
            static let metodoPago = path + "WS_metodoPagoEmpresarial"
            static let descargaContrato = path + "WS_DescargarContrato"
            static let enviarContrato = path + "WS_EnviarContrato"
            static let crearCotizacion = path + "WS_crearCotizacion2"
            static let consultaTareas = path + "WS_ConsultaTrabajo"
            
//            static let graficasMesaControl = path + "WS_Grafica_MesaControl"

            static let subirDocumentos = path + "WS_SubirDocumentos"
            static let agenda = path + "WS_Agenda"
            
            static let promociones = path + "WS_ListaPromociones"
            static let creaToken = server + "/services/oauth2/token"
        }

        struct AppsServer {
            static let server = "https://appstotalplay.com"
            static let videos: String = server + "/videos/apps/"
        }
    }

    struct Format {
        static let formatoFechaServer = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        static let formatoFechaHMS = "yyyy-MM-dd HH:mm:ss"
        static let formatoFecha = "yyyy-MM-dd"
        static let formatoHora = "HH:mm:ss"
        static let formatoFecha2 = "dd/MM/yyyy"
    }

    struct Names {
        static let firma1: String = "firmaHoja1.jpg"
        static let firma2: String = "firmaHoja2.jpg"
        static let firma3: String = "firmaHoja3.jpg"
        static let firmaCaratula1: String = "firmaCaratula1.jpg"
        static let firmaCaratula2: String = "firmaCaratula2.jpg"
        static let fotoHoja1: String = "fotoHoja1.jpg"
        static let fotoHoja2: String = "fotoHoja2.jpg"
        static let fotoHoja3: String = "fotoHoja3.jpg"
        static let fotoCaratula1: String = "fotoCaratula1.jpg"
        static let fotoCaratula2: String = "fotoCaratula2.jpg"
    }

    struct Eventbus {
        struct Id {
            static let header: String = "Header-cambioTitulo"
            static let menu: String = "BaseMenuVC-updateHeader"
            static let itemMisVentas: String = "MisVentas-cambioTitulo"
            static let menuItemCotizacion: String = "Cotizacion-cambioTitulo"
            static let itemGeneraCarga: String = "GenerarLead-cambioTitulo"
            static let menuItemSitios: String = "Sitios-cambioTitulo"
            static let menuItemVerificaCobertura: String = "VerificarCobertura-cambioTitulo"
            static let menuItemTipsVentas: String = "TipsVentas-cambioTitulo"
            static let menuItemPreguntasFrecuentes: String = "PreguntasFrecuentes-cambioTitulo"
            static let solucionesMedidaCambioPlan: String = "CotizacionCustomPlanes-cambioPlan"
            static let verificarCoberturaCambioPlan: String = "ubicacionCambiada"
        }
    }

    struct Enum {
        enum TipoEditar { case none, nuevo, agregar, actualizar, eliminar }
    }

    struct Array {
        static let ciudadesFronterizas = ["Tijuana", "Ciudad Juárez"]
    }

    struct Config {
        static let useToken = false
        static let DEBUG = false
        static let showHTTPLogs = false
        static let showInfoLifeCycle = false
        static let showTimeLoad = false
        static let showNillFromService = false
        
        static let loadHarcodeUser = false
        static let harcodeUser = [
            "id": "a0hQ0000004KXUiIAO",
            "nombre": "Juan Reynaldo Escobar"
        ]
        
        static let forceUpdateData = false //warning
        static let deleteAllData = false
        //warning
    }

    struct Passwords {
        static let user: String = "25631" //"26351"
        static let password: String = "Middle100$"
        static let ip: String = "1.1.1.1"
    }

    struct Fonts {
        struct Title {
            static let normal: UIFont = UIFont(name: "Montserrat", size: 16)!
            static let semiBold: UIFont = UIFont(name: "Montserrat-SemiBold", size: 16)!
        }

        struct Text {
            static let semiBold: UIFont = UIFont(name: "Montserrat-SemiBold", size: 13)!
        }
    }
}
