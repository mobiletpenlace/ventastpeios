//
//  Colores.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 28/06/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class Colores {
    static let azulOscuro: UIColor = UIColor(netHex: 0x3A4559)
    static let bordeCobertura: UIColor = UIColor(netHex: 0x4D5768)
    static let textoOscuroCobertura: UIColor = UIColor(netHex: 0x57575B)
    static let selectionFamiliaCelda: UIColor = UIColor(netHex: 0xB1B3B3)
    static let selectDlgMiPerfilCelda: UIColor = UIColor(netHex: 0xe7f2f7)
    static let Rojo: UIColor = UIColor(netHex: 0xFB0D1B)
    static let Verde: UIColor = UIColor(netHex: 0x00B501)
    static let negro: UIColor = UIColor(netHex: 0x000000)
    static let transparente: UIColor = UIColor.clear
    static let blanco: UIColor = UIColor.white

    static let coloresGrafica = [
        UIColor(netHex: 0x3b4558),
        UIColor(netHex: 0x75787b),
        UIColor(netHex: 0xc9c9c9),
        UIColor(netHex: 0xb1b3b3),
        UIColor(netHex: 0xa3a8b1),
        UIColor(netHex: 0xe5e3e3)
    ]
}
