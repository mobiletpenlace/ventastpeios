import Foundation

struct ListaValueTask: Codable {
    let telefono: String?
    let subject: String?
    let relacion: String?
    let prospecto: String?
    let nombreEmpresa: String?
    let nombreContacto: String?
    let id: String?
    let fechaInicio: String?
    let fechaFin: String?
    let comentario: String?
    let celular: String?

    enum CodingKeys: String, CodingKey {

        case telefono = "telefono"
        case subject = "Subject"
        case relacion = "relacion"
        case prospecto = "prospecto"
        case nombreEmpresa = "nombreEmpresa"
        case nombreContacto = "nombreContacto"
        case id = "id"
        case fechaInicio = "fechaInicio"
        case fechaFin = "fechaFin"
        case comentario = "comentario"
        case celular = "celular"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        telefono = try values.decodeIfPresent(String.self, forKey: .telefono)
        subject = try values.decodeIfPresent(String.self, forKey: .subject)
        relacion = try values.decodeIfPresent(String.self, forKey: .relacion)
        prospecto = try values.decodeIfPresent(String.self, forKey: .prospecto)
        nombreEmpresa = try values.decodeIfPresent(String.self, forKey: .nombreEmpresa)
        nombreContacto = try values.decodeIfPresent(String.self, forKey: .nombreContacto)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        fechaInicio = try values.decodeIfPresent(String.self, forKey: .fechaInicio)
        fechaFin = try values.decodeIfPresent(String.self, forKey: .fechaFin)
        comentario = try values.decodeIfPresent(String.self, forKey: .comentario)
        celular = try values.decodeIfPresent(String.self, forKey: .celular)
    }

}
