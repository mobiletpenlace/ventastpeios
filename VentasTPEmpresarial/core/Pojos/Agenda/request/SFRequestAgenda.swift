import Foundation

struct SFRequestAgenda: Codable {
    let idEmpleado: String?
    let metodo: String?
    let fechaInicio: String?
    let fechaFin: String?
    let valoresTask: ValoresTask?

}
