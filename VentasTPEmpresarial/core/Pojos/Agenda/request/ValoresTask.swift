import Foundation

struct ValoresTask: Codable {
    let id: String?
    let name: String?
    let esTodoElDia: Bool?
    let prospectoContacto: String?
    let comentario: String?
    let relacion: String?
    let telefono: String?
    let nombreContacto: String?
    let nombreEmpresa: String?
    let celular: String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
        case esTodoElDia = "esTodoElDia"
        case prospectoContacto = "prospectoContacto"
        case comentario = "comentario"
        case relacion = "relacion"
        case telefono = "telefono"
        case nombreContacto = "nombreContacto"
        case nombreEmpresa = "nombreEmpresa"
        case celular = "celular"
    }
}
