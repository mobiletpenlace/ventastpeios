import Foundation

struct MiddleRequestAgenda: Codable {
    let login: LoginMiddleMinus = LoginMiddleMinus()
    let idEmpleado: String?
    let metodo: String?
    let fechaInicio: String?
    let fechaFin: String?
    let valoresTask: ValoresTask?
    
}
