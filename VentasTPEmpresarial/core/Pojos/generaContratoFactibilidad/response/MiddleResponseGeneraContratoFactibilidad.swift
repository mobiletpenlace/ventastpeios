import Foundation

struct MiddleResponseGeneraContratoFactibilidad: Codable {
    let response: ResultMiddleMinus?
    let url: String?
}
