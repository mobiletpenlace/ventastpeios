import Foundation

struct SFResponseGeneraContratoFactibilidad: Codable {
	let url: String?
	let resultDescription: String?
	let result: String?
}
