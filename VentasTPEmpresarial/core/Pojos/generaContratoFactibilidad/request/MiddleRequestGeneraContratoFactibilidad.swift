import Foundation

struct MiddleRequestGeneraContratoFactibilidad: Codable {
    let login: LoginMiddleMinus = LoginMiddleMinus()
    let oppId: String
    let opcionesFac: OpcionesFac2
}
