import Foundation

struct OpcionesFac2: Codable {
    let nombre: String
    let metodoPago: String
    let razonSocialEmisora: String
    let tipoRenta: String
    let generarFactura: String
    let cicloFacturaccion: String
    let tipoMoneda: String
    let numeroDeFacturas: String
    let tipoCliente: String
    let momentoDeFacturacion: String
    let tipoFacturacion: String
    let tipoCuenta: String
}
