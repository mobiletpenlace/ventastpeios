import Foundation

struct Doc: Codable{
    let documentos: [Documentos]?
    
    init(documento: [Documentos]) {
        self.documentos = documento
    }
}
