import Foundation

struct MiddleSubirDocumentosRequest: Codable {
    let login: LoginMiddleMinus = LoginMiddleMinus()
    
    let idVenta: String
    let doc: Doc
    
    init(_ idVenta: String, _ doc : Doc) {
        self.idVenta = idVenta
        self.doc = doc
    }
}
