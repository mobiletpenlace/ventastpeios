import Foundation

struct Documentos: Codable{
    let tipoDocumento: String?
    let nombreArchivo: String?
    let archivoB64: String?
    
    init(tipoDocumento: String, nombreArchivo: String, archivoB64: String) {
        self.tipoDocumento = tipoDocumento
        self.nombreArchivo = nombreArchivo
        self.archivoB64 = archivoB64
    }
}
