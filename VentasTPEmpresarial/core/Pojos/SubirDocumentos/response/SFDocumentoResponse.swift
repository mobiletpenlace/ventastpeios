import Foundation

struct SFDocumentoResponse: Codable {
    let resultDescription: String?
    let result: String?
}
