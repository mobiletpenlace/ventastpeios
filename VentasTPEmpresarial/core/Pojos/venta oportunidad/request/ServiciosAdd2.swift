import Foundation

struct ServiciosAdd2: Codable {
    let servicios: [Servicios]

    init(servicios: [Servicios]) {
        self.servicios = servicios
    }
}
