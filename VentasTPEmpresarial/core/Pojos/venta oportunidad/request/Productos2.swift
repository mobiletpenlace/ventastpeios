import Foundation

struct Productos2: Codable {
    let id: String
    let nombre: String

    init(id: String, nombre: String) {
        self.id = id
        self.nombre = nombre
    }
}
