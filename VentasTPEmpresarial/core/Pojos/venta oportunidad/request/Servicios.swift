import Foundation

struct Servicios: Codable {
    let id: String
    let nombre: String
    let esSDWAN: Bool
    let ipOrigenDatos: String?
    let ipDestinoDatos: String?
    let listaDominiosBloqueo: String?
    let determinarVLANs: String?
    let salidaInternet: String?
    let porcentajeAnchoBanda: String?
    let determinarSegmentoOrigen: String?
    let segmentosDestinos: String?
    let tipoRouteo: String?
    let areaId: String?
    let idPlan: String

    init(id: String, nombre: String, esSDWAN: Bool, ipOrigenDatos: String?, ipDestinoDatos: String?, listaDominiosBloqueo: String?, determinarVLANs: String?, salidaInternet: String?, porcentajeAnchoBanda: String?, determinarSegmentoOrigen: String?, segmentosDestinos: String?, tipoRouteo: String?, areaId: String?, idPlan: String) {
        self.id = id
        self.nombre = nombre
        self.esSDWAN = esSDWAN
        self.ipOrigenDatos = ipOrigenDatos
        self.ipDestinoDatos = ipDestinoDatos
        self.listaDominiosBloqueo = listaDominiosBloqueo
        self.determinarVLANs = determinarVLANs
        self.salidaInternet = salidaInternet
        self.porcentajeAnchoBanda = porcentajeAnchoBanda
        self.determinarSegmentoOrigen = determinarSegmentoOrigen
        self.segmentosDestinos = segmentosDestinos
        self.tipoRouteo = tipoRouteo
        self.areaId = areaId
        self.idPlan = idPlan
    }
}
