import Foundation

struct MetodoPago2: Codable {
    let nombretitular: String
    let aPTitular: String
    let aMTitular: String
    let tipoTarjeta: String
    let numeroTarjeta: String
    let fechaMM: String
    let fechaAA: String

    init(nombretitular: String, aPTitular: String, aMTitular: String, tipoTarjeta: String, numeroTarjeta: String, fechaMM: String, fechaAA: String) {
        self.nombretitular = nombretitular
        self.aPTitular = aPTitular
        self.aMTitular = aMTitular
        self.tipoTarjeta = tipoTarjeta
        self.numeroTarjeta = numeroTarjeta
        self.fechaMM = fechaMM
        self.fechaAA = fechaAA
    }
}
