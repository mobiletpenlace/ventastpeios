import Foundation

struct AddonServicio: Codable {
    let id: String
    let descripcion: String
    let descuento: String
    let precio: String

    init(id: String, descripcion: String, descuento: String, precio: String) {
        self.id = id
        self.descripcion = descripcion
        self.descuento = descuento
        self.precio = precio
    }
}
