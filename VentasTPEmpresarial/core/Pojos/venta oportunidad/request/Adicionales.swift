import Foundation

struct Adicionales: Codable {
    let serviciosAdd: ServiciosAdd2?
    let productosAdd: ProductosAdd2?

    init(serviciosAdd: ServiciosAdd2?, productosAdd: ProductosAdd2?) {
        self.serviciosAdd = serviciosAdd
        self.productosAdd = productosAdd
    }
}
