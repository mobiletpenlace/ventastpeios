import Foundation

struct ProductosAdd2: Codable {
    let productos: [Productos2]

    init(productos: [Productos2]) {
        self.productos = productos
    }
}
