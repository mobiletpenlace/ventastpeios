import Foundation

struct IdPlanes: Codable {
    let id: String
    let cantidad: String
    let descuento: String?
    let addServicio: [AddServicio]?

    init(id: String, cantidad: String, descuento: String?, addServicio: [AddServicio]?) {
        self.id = id
        self.cantidad = cantidad
        self.descuento = descuento
        self.addServicio = addServicio
    }
}
