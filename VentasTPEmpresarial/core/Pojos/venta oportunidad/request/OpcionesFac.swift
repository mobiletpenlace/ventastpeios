import Foundation

struct OpcionesFac: Codable {
    let nombre: String
    let momentoDeFacturacion: String
    let tipoCuenta: String
    let tipoCliente: String
    let razonSocialEmisora: String
    let generarFactura: String
    let cicloFacturaccion: String
    let metodoPago: String
    let numeroDeFacturas: String
    let tipoFacturacion: String
    let tipoMoneda: String
    let tipoRenta: String

    init(nombre: String, momentoDeFacturacion: String, tipoCuenta: String, tipoCliente: String, razonSocialEmisora: String, generarFactura: String, cicloFacturaccion: String, metodoPago: String, numeroDeFacturas: String, tipoFacturacion: String, tipoMoneda: String, tipoRenta: String) {
        self.nombre = nombre
        self.momentoDeFacturacion = momentoDeFacturacion
        self.tipoCuenta = tipoCuenta
        self.tipoCliente = tipoCliente
        self.razonSocialEmisora = razonSocialEmisora
        self.generarFactura = generarFactura
        self.cicloFacturaccion = cicloFacturaccion
        self.metodoPago = metodoPago
        self.numeroDeFacturas = numeroDeFacturas
        self.tipoFacturacion = tipoFacturacion
        self.tipoMoneda = tipoMoneda
        self.tipoRenta = tipoRenta
    }
}
