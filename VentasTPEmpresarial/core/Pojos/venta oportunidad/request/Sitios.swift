import Foundation

struct Sitios: Codable {
    let nombreSitio: String
    let tipoCobertura: String
    let latitud: String
    let regionId: String
    let factibilidad: String
    let region: String
    let distrito: String
    let longitud: String
    let categoryService: String
    let plaza: String
    let zona: String
    let autorizacionSinCobertura: Bool
    let newAccountId: String
    let numExterior: String
    let numInteior: String
    let calleReferencia: String
    let referenciaUrbanada: String
    let codigoPostal: String
    let colonia: String
    let municipio: String
    let ciudad: String
    let estado: String
    let idPlanes: [IdPlanes]
    let adicionales: Adicionales?
    let calle: String
    let cluster: String
    let esSolucionesALaMedida: Bool
    let esServicio: Bool

    init(nombreSitio: String, tipoCobertura: String, latitud: String, regionId: String, factibilidad: String, region: String, distrito: String, longitud: String, categoryService: String, plaza: String, zona: String, autorizacionSinCobertura: Bool, newAccountId: String, numExterior: String, numInteior: String, calleReferencia: String, referenciaUrbanada: String, codigoPostal: String, colonia: String, municipio: String, ciudad: String, estado: String, idPlanes: [IdPlanes], adicionales: Adicionales?, calle: String, cluster: String, esSolucionesALaMedida: Bool, esServicio: Bool) {

        self.nombreSitio = nombreSitio
        self.tipoCobertura = tipoCobertura
        self.latitud = latitud
        self.regionId = regionId
        self.factibilidad = factibilidad
        self.region = region
        self.distrito = distrito
        self.longitud = longitud
        self.categoryService = categoryService
        self.plaza = plaza
        self.zona = zona
        self.autorizacionSinCobertura = autorizacionSinCobertura
        self.newAccountId = newAccountId
        self.numExterior = numExterior
        self.numInteior = numInteior
        self.calleReferencia = calleReferencia
        self.referenciaUrbanada = referenciaUrbanada
        self.codigoPostal = codigoPostal
        self.colonia = colonia
        self.municipio = municipio
        self.ciudad = ciudad
        self.estado = estado
        self.idPlanes = idPlanes
        self.adicionales = adicionales
        self.calle = calle
        self.cluster = cluster
        self.esSolucionesALaMedida = esSolucionesALaMedida
        self.esServicio = esServicio
    }

    enum CodingKeys: String, CodingKey {
        case nombreSitio, tipoCobertura, latitud, regionId, factibilidad, region, distrito
        case longitud, categoryService, plaza, zona, autorizacionSinCobertura, newAccountId
        case calleReferencia, numExterior, referenciaUrbanada, codigoPostal, colonia, municipio
        case ciudad, estado, idPlanes, adicionales, calle, esSolucionesALaMedida, esServicio
        case numInteior = "numInteior"
        case cluster = "clouster"
    }

}
