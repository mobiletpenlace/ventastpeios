import Foundation

struct AddServicio: Codable {
    let id: String
    let descripcion: String
    let productos: [AddonServicio]

    init(id: String, descripcion: String, productos: [AddonServicio]) {
        self.id = id
        self.descripcion = descripcion
        self.productos = productos
    }
}
