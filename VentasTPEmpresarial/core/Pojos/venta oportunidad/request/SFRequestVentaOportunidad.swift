import Foundation

struct SFRequestVentaOportunidad: Codable {
    let oportunidad: String
    let nombre: String
    let vigenciaCotizacion: String
    let plazo: String
    let accion: String
    let idEmpleado: String
    let sitios: [Sitios]
    let opcionesFac: OpcionesFac?
    let metodoPago: MetodoPago2?

    init(oportunidad: String, nombre: String, vigenciaCotizacion: String, plazo: String, accion: String, idEmpleado: String, sitios: [Sitios], opcionesFac: OpcionesFac?, metodoPago: MetodoPago2?) {
        self.oportunidad = oportunidad
        self.nombre = nombre
        self.vigenciaCotizacion = vigenciaCotizacion
        self.plazo = plazo
        self.accion = accion
        self.idEmpleado = idEmpleado
        self.sitios = sitios
        self.opcionesFac = opcionesFac
        self.metodoPago = metodoPago
    }
}
