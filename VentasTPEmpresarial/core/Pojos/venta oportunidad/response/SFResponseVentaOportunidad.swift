import Foundation

struct SFResponseVentaOportunidad: Codable {
    let resultDescription: String?
    let result: String?
    let idNuevaCotizacion: String?
    let listaTrabajos: [String]?
    let esSolucionesALaMedida: Bool?
}
