import Foundation

struct MiddleResponseVentaOportunidad: Codable {
    let response: ResultMiddleMinus?
    let idNuevaCotizacion: String?
    let listaTrabajos: [String]?
}
