import Foundation

struct SFGraficaMesaControlResponse: Codable {
    let resultDescription: String?
    let result: String?
    let listaParamChart: [ListaParamChart]?
}
