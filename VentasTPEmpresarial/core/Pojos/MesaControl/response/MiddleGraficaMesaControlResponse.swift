import Foundation

struct MiddleGraficaMesaControlResponse: Codable {
    let response: ResultMiddleMinus?
    let listaParamChart: [ListaParamChart]?
}
