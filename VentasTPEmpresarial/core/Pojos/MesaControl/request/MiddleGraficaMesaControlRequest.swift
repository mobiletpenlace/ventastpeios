import Foundation

struct MiddleGraficaMesaControlRequest: Codable {
    let login: LoginMiddleMinus = LoginMiddleMinus()
    let idEmpleado: String
    let fechaInicio: String
    let fechaFin: String
    
    init(idEmpleado: String, fechaInicio: String, FechaFin: String) {
        self.idEmpleado = idEmpleado
        self.fechaInicio = fechaInicio
        self.fechaFin = FechaFin
    }
}
