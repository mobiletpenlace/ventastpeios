import Foundation

struct SFRequestCorreoContrato: Codable {
    let correoElectronico: String
    let idVenta: String
    let idVendedor: String
    let actualizar: Bool
}
