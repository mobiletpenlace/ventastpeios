import Foundation

struct MiddleRequestCorreoContrato: Codable {
    let login: LoginMiddleMinus = LoginMiddleMinus()
    let correoElectronico: String
    let idVenta: String
    let idVendedor: String
    let actualizar: Bool
    
}
