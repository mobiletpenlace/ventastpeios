//
//  subidaDeImagenes.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 23/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
//import Alamofire
//protocol UploadImagesDelegate {
//    func onSuccess(type:String, URLEncrypted:String)
//    func onError(type:String)
//}
//
//extension CargaDocumentosContrato3VC{
//    func validateUploads() -> Bool{
//        for upload in uploads{
//            if !upload{
//                return false
//            }
//        }
//        return true
//    }
//    
//    func getDate()->String{
//        let date = Date()
//        let calendar = Calendar.current
//        let components = calendar.dateComponents([.year, .month], from: date)
//        let year =  components.year
//        let month = components.month
//        return String(year!)+"/"+String(month!)
//    }
//    
//    func uploadImageOf(type:String, image:UIImage){
//        let date = getDate()
//        let data = UIImageJPEGRepresentation(image, 0.1)
//        var body:[String:String] = ["":""]
//        let pathCrypted = Security.crypt(text: "SociosTotalplay/"+date)
//        body["FilePath"] = pathCrypted
//        switch type {
//        case "Selfie":
//            requestWith(endUrl: type, imageData: data, parameters: body, onCompletion: nil, onError: nil)
//        case "IDFront":
//            requestWith(endUrl: "IDFront", imageData: data, parameters: body, onCompletion: nil, onError: nil)
//        case "IDBack":
//            requestWith(endUrl: "IDBack", imageData: data, parameters: body, onCompletion: nil, onError: nil)
//        case "Comprobante":
//            requestWith(endUrl: "Comprobante", imageData: data, parameters: body, onCompletion: nil, onError: nil)
//        case "Signature":
//            requestWith(endUrl: "Signature", imageData: data, parameters: body, onCompletion: nil, onError: nil)
//        default:
//            print("case not expected")
//        }
//        
//    }
//    
//    
//    
//    func requestWith(endUrl: String, imageData: Data?, parameters: [String : Any], onCompletion: ((Any?) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
//        let url = "https://mss.totalplay.com.mx/FFMTpe/WriteFile"
//        let headers: HTTPHeaders = [ "Authorization": "Basic ZmZtYXBwOjRnZW5kNG1pM250bw=="]
//        Alamofire.upload(multipartFormData: { (multipartFormData) in
//            for (key, value) in parameters {
//                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
//            }
//            
//            switch endUrl {
//            case "identificacionOficialFrente":
//                if let data = imageData{
//                    multipartFormData.append(data, withName: "File", fileName: "identificacionOficialFrente.jpeg", mimeType: "image/jpeg")
//                }
//            case "identificacionOficialReverso":
//                if let data = imageData{
//                    multipartFormData.append(data, withName: "File", fileName: "identificacionOficialReverso.jpeg", mimeType: "image/jpeg")
//                }
//            case "identificacionComprobanteDomicilio":
//                if let data = imageData{
//                    multipartFormData.append(data, withName: "File", fileName: "identificacionComprobanteDomicilio.jpeg", mimeType: "image/jpeg")
//                }
//            case "fotoRFCFrente":
//                if let data = imageData{
//                    multipartFormData.append(data, withName: "File", fileName: "fotoRFCFrente.jpeg", mimeType: "image/jpeg")
//                }
//            case "fotoRFCReverso":
//                if let data = imageData{
//                    multipartFormData.append(data, withName: "File", fileName: "fotoRFCReverso.jpeg", mimeType: "image/jpeg")
//                }
//            case "fotoActaHoja1":
//                if let data = imageData{
//                    multipartFormData.append(data, withName: "File", fileName: "fotoActaHoja1.jpeg", mimeType: "image/jpeg")
//                }
//            case "fotoActaHoja2":
//                if let data = imageData{
//                    multipartFormData.append(data, withName: "File", fileName: "fotoActaHoja2.jpeg", mimeType: "image/jpeg")
//                }
//            case "fotoActaHoja3":
//                if let data = imageData{
//                    multipartFormData.append(data, withName: "File", fileName: "fotoActaHoja3.jpeg", mimeType: "image/jpeg")
//                }
//            case "fotoPoderNotarialCaratula1":
//                if let data = imageData{
//                    multipartFormData.append(data, withName: "File", fileName: "fotoPoderNotarialCaratula1.jpeg", mimeType: "image/jpeg")
//                }
//            case "fotoPoderNotarialCaratula2":
//                if let data = imageData{
//                    multipartFormData.append(data, withName: "File", fileName: "fotoPoderNotarialCaratula2.jpeg", mimeType: "image/jpeg")
//                }
//            case "Contrato hoja 1":
//                if let data = imageData{
//                    multipartFormData.append(data, withName: "File", fileName: "Contrato hoja 1.jpeg", mimeType: "image/jpeg")
//                }
//            case "Contrato hoja 2":
//                if let data = imageData{
//                    multipartFormData.append(data, withName: "File", fileName: "Contrato hoja 2.jpeg", mimeType: "image/jpeg")
//                }
//            default:
//                print("Case not Expected")
//            }
//            
//            
//            
//        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
//            switch result{
//            case .success(let upload, _, _):
//                upload.responseString(completionHandler:
//                    { response in
//                        var rutaEncriptada = response.result.debugDescription
//                        rutaEncriptada = String(rutaEncriptada.dropFirst(9))
//                        
//                        if rutaEncriptada.range(of:"Error") != nil { print("hay algun error en la respuesta del servidor"); self.onError(type: endUrl) }
//                        else{
//                            let libre = Security.decrypt(text: rutaEncriptada)
//                            print("Succesfully \(endUrl) uploaded...  \(rutaEncriptada) y \(libre)")
//                            self.onSuccess(type: endUrl, URLEncrypted: rutaEncriptada)
//                        }
//                        if let err = response.error{
//                            onError?(err)
//                            return
//                        }
//                        onCompletion?(nil)
//                })
//            case .failure(let error):
//                print("Error in upload: \(error.localizedDescription)")
//                onError?(error)
//            }
//        }
//    }
//}
//
//extension CargaDocumentosContrato3VC: UploadImagesDelegate{
//    func onError(type: String) {
//        switch type {
//        case "identificacionOficialFrente":
//            uploadImageOf(type: type, image: presenter.getImage(0))
//        case "identificacionOficialReverso":
//            uploadImageOf(type: type, image: presenter.getImage(1))
//        case "identificacionComprobanteDomicilio":
//            uploadImageOf(type: type, image: presenter.getImage(2))
//        case "fotoRFCFrente":
//            uploadImageOf(type: type, image: presenter.getImage(3))
//        case "fotoRFCReverso":
//            uploadImageOf(type: type, image: presenter.getImage(4))
//        case "fotoActaHoja1":
//            uploadImageOf(type: type, image: presenter.getImage(5))
//        case "fotoActaHoja2":
//            uploadImageOf(type: type, image: presenter.getImage(6))
//        case "fotoActaHoja3":
//            uploadImageOf(type: type, image: presenter.getImage(7))
//        case "fotoPoderNotarialCaratula1":
//            uploadImageOf(type: type, image: presenter.getImage(8))
//        case "fotoPoderNotarialCaratula2":
//            uploadImageOf(type: type, image: presenter.getImage(9))
//        case "ContratoHoja1":
//            uploadImageOf(type: type, image: presenter.getImage(10))
//        case "ContratoHoja2":
//            uploadImageOf(type: type, image: presenter.getImage(11))
//        default:
//            print("Not expected")
//        }
//    }
//
//    func onSuccess(type: String, URLEncrypted: String) {
//        switch type {
//        case "identificacionOficialFrente":
//            URSL[0] = "SociosTotalplay/"+getDate()
//        case "identificacionOficialReverso":
//            URSL[1] = "SociosTotalplay/"+getDate()
//        case "identificacionComprobanteDomicilio":
//            URSL[2] = "SociosTotalplay/"+getDate()
//        case "fotoRFCFrente":
//            URSL[3] = "SociosTotalplay/"+getDate()
//        case "fotoRFCReverso":
//            URSL[4] = "SociosTotalplay/"+getDate()
//        case "fotoActaHoja1":
//            URSL[5] = "SociosTotalplay/"+getDate()
//        case "fotoActaHoja2":
//            URSL[6] = "SociosTotalplay/"+getDate()
//        case "fotoActaHoja3":
//            URSL[7] = "SociosTotalplay/"+getDate()
//        case "fotoPoderNotarialCaratula1":
//            URSL[8] = "SociosTotalplay/"+getDate()
//        case "fotoPoderNotarialCaratula2":
//            URSL[9] = "SociosTotalplay/"+getDate()
//        case "Contrato hoja 1":
//            URSL[10] = "SociosTotalplay/"+getDate()
//        case "Contrato hoja 2":
//            URSL[11] = "SociosTotalplay/"+getDate()
//        default:
//            print("Not expected")
//        }
//        if validateUploads(){
//            
//        }
//    }
//}
