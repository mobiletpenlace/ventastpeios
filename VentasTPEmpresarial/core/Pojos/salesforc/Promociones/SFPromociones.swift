//
//  SFPromociones.swift
//  VentasTPEmpresarial
//
//  Created by eduardo mancilla on 3/13/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation

struct SFPromocionesRequest: Codable {
    var idPlan: String?

    enum CodingKeys: String, CodingKey {
        case idPlan = "idPlan"
    }

    init(idPlan: String) {
        self.idPlan = idPlan
    }
}

struct SFPromocionesResponse: Codable {
    let resultDescription: String?
    let result: String?
    let objectResponse: ObjectResponse2?

    enum CodingKeys: String, CodingKey {
        case resultDescription = "resultDescription"
        case result = "result"
        case objectResponse = "objectResponse"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        resultDescription = try values.decodeIfPresent(String.self, forKey: .resultDescription)
        result = try values.decodeIfPresent(String.self, forKey: .result)
        objectResponse = try values.decodeIfPresent(ObjectResponse2.self, forKey: .objectResponse)
    }

    struct ObjectResponse2: Codable {
        let promocionesLista: [PromocionesLista]?

        enum CodingKeys: String, CodingKey {
            case promocionesLista = "promocionesLista"
        }

        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            promocionesLista = try values.decodeIfPresent([PromocionesLista].self, forKey: .promocionesLista)
        }

        struct PromocionesLista: Codable {
            let precioPlan: Float?
            let name: String?
            var montoDescuento: Float = 0
            let mesInicio: Int?
            let iVA: Int?
            let iEPS: Int?
            let idServicio: String?
            let idPromocionExcl: String?
            let idProducto: String?
            let id: String?
            let fechaInicio: String?
            let fechaFin: String?
            let duracionMes: Int?
            let convivencia: [Convivencia]?

            enum CodingKeys: String, CodingKey {

                case precioPlan = "precioPlan"
                case name = "name"
                case montoDescuento = "montoDescuento"
                case mesInicio = "mesInicio"
                case iVA = "IVA"
                case iEPS = "IEPS"
                case idServicio = "idServicio"
                case idPromocionExcl = "idPromocionExcl"
                case idProducto = "idProducto"
                case id = "id"
                case fechaInicio = "FechaInicio"
                case fechaFin = "FechaFin"
                case duracionMes = "duracionMes"
                case convivencia = "convivencia"
            }

            init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                precioPlan = try values.decodeIfPresent(Float.self, forKey: .precioPlan)
                name = try values.decodeIfPresent(String.self, forKey: .name)
                montoDescuento = try values.decodeIfPresent(Float.self, forKey: .montoDescuento) ?? 0
                mesInicio = try values.decodeIfPresent(Int.self, forKey: .mesInicio)
                iVA = try values.decodeIfPresent(Int.self, forKey: .iVA)
                iEPS = try values.decodeIfPresent(Int.self, forKey: .iEPS)
                idServicio = try values.decodeIfPresent(String.self, forKey: .idServicio)
                idPromocionExcl = try values.decodeIfPresent(String.self, forKey: .idPromocionExcl)
                idProducto = try values.decodeIfPresent(String.self, forKey: .idProducto)
                id = try values.decodeIfPresent(String.self, forKey: .id)
                fechaInicio = try values.decodeIfPresent(String.self, forKey: .fechaInicio)
                fechaFin = try values.decodeIfPresent(String.self, forKey: .fechaFin)
                duracionMes = try values.decodeIfPresent(Int.self, forKey: .duracionMes)
                convivencia = try values.decodeIfPresent([Convivencia].self, forKey: .convivencia)
            }

            struct Convivencia: Codable {
                let idPromocionExcl: String?
                let idPromocion: String?

                enum CodingKeys: String, CodingKey {
                    case idPromocionExcl = "idPromocionExcl"
                    case idPromocion = "idPromocion"
                }

                init(from decoder: Decoder) throws {
                    let values = try decoder.container(keyedBy: CodingKeys.self)
                    idPromocionExcl = try values.decodeIfPresent(String.self, forKey: .idPromocionExcl)
                    idPromocion = try values.decodeIfPresent(String.self, forKey: .idPromocion)
                }
            }
        }
    }
}
