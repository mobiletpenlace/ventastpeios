import Foundation

struct VideoList: Codable {
	let videos: [Videos]?
}

struct Videos: Codable {
    let video: String?
    let description: String?
}
