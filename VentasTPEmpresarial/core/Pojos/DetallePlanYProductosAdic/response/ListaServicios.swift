import Foundation

struct ListaServicios: Codable {
    let tipoTelefonia: String?
    let tipoServicio: String?
    let srvMode: String?
    let servicioId: String?
    let seFactura: Bool?
    let seActiva: Bool?
    let precioDeLista: Float?
    let precio: Float?
    let planId: String?
    let ownerId: String?
    let orden: Int?
    let nombreEditable: Bool?
    let nombrecomercial: String?
    let name: String?
    let id: String?
    let estatus: String?
    let esServicioAdicional: String?
    let esSDWAN: Bool?
    let equipo: Bool?
    let cantidadEquiposAutomaticos: Int?
    
    enum CodingKeys: String, CodingKey {
        case tipoTelefonia = "TipoTelefonia"
        case tipoServicio = "TipoServicio"
        case srvMode = "SRV_Mode"
        case servicioId = "ServicioId"
        case seFactura = "SeFactura"
        case seActiva = "SeActiva"
        case precioDeLista = "Precio_de_Lista"
        case precio = "Precio"
        case planId = "PlanId"
        case ownerId = "OwnerId"
        case orden = "Orden"
        case nombreEditable = "NombreEditable"
        case nombrecomercial = "Nombrecomercial"
        case name = "Name"
        case id = "Id"
        case estatus = "Estatus"
        case esServicioAdicional = "EsServicioAdicional"
        case esSDWAN = "EsSDWAN"
        case equipo = "Equipo"
        case cantidadEquiposAutomaticos = "CantidadEquiposAutomaticos"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let converter = CodableConvert(keyContainer: values)
        
        tipoTelefonia = converter.string(.tipoTelefonia)
        tipoServicio = converter.string(.tipoServicio)
        srvMode = converter.string(.srvMode)
        servicioId = converter.string(.servicioId)
        nombrecomercial = converter.string(.nombrecomercial)
        name = converter.string(.name)
        id = converter.string(.id)
        estatus = converter.string(.estatus)
        esServicioAdicional = converter.string(.esServicioAdicional)
        planId = converter.string(.planId)
        ownerId = converter.string(.ownerId)
        
        seFactura = converter.bool(.seFactura)
        seActiva = converter.bool(.seActiva)
        nombreEditable = converter.bool(.nombreEditable)
        esSDWAN = converter.bool(.esSDWAN)
        equipo = converter.bool(.equipo)
        
        precioDeLista = converter.float(.precioDeLista)
        precio = converter.float(.precio)
        
        orden = converter.int(.orden)
        cantidadEquiposAutomaticos = converter.int(.cantidadEquiposAutomaticos)
    }
}

