import Foundation

struct SFDetallePlanResponse: Codable {
    let resultDescription: String?
    let result: String?
    let objectResponse: ObjectResponse?
}
