import Foundation

struct MiddleDetallePlanResponse: Codable {
    let response: ResultMiddleMinus?
    let objectResponse: ObjectResponse?
}
