import Foundation

struct ObjectResponse: Codable {
    let valoresAgregados: [ValoresAgregados]?
    let listaServicios: [ListaServicios]?
    let listaProductos: [ListaProductos]?
    let equpoAsignado: [EqupoAsignado]?
}
