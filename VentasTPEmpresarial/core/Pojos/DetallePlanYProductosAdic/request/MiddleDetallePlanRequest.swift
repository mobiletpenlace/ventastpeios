import Foundation

struct MiddleDetallePlanRequest: Codable {
    
    let login: LoginMiddleMinus = LoginMiddleMinus()
    
    var idPlan: String
    
    init(idPlan: String) {
        self.idPlan = idPlan
    }
    
}
