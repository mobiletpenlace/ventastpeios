import Foundation

struct SFDetallePlanRequest: Codable {
    var idPlan: String
    
    init(idPlan: String) {
        self.idPlan = idPlan
    }
}
