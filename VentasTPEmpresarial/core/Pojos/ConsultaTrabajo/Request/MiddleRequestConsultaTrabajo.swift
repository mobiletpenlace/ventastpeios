import Foundation

struct MiddleRequestConsultaTrabajo: Codable {
    let login: LoginMiddleMinus = LoginMiddleMinus()
    let idsJobs: [String]
}
