import Foundation

struct MiddleResponseConsultaTrabajo: Codable {
    let response: ResultMiddleMinus?
    let listJobs : [ListJobs]?
}
