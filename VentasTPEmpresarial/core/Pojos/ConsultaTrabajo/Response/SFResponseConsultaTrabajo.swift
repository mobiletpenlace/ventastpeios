import Foundation

struct SFResponseConsultaTrabajo: Codable {
    let resultDescription : String?
    let result : String?
    let listJobs : [ListJobs]?
}
