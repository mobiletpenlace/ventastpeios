import Foundation

struct MiddleConvertirLeadResponse: Codable {
    let response: ResultMiddleMinus?
    let oportunidad: String?
    let cuenta: String?
    let contacto: String?
}
