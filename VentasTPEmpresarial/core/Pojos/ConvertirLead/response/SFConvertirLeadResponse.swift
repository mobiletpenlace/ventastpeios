import Foundation

struct SFConvertirLeadResponse: Codable {
    let resultDescription: String?
    let result: String?
    let oportunidad: String?
    let cuenta: String?
    let contacto: String?
}
