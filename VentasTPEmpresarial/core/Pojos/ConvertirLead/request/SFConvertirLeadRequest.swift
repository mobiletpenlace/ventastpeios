import Foundation

struct SFConvertirLeadRequest: Codable {
    let idProspecto: String
    let calle: String
    let numExt: String
    let numInt: String
    let colonia: String
    let estado: String
    let ciudad: String
    let cp: String
    let municipio: String
    let fechaCierre: String
    let idEmpleado: String
    let facturacionP: String
    let rfc: String
    let nombreOportunidad: String
    let nombreCuenta: String

    init(idProspecto: String, calle: String, numExt: String, numInt: String, colonia: String, estado: String, ciudad: String, cp: String, municipio: String, fechaCierre: String, facturacionP: String, idEmpleado: String, rfc: String, nombreOportunidad: String, nombreCuenta: String) {
        self.idProspecto = idProspecto
        self.calle = calle
        self.numExt = numExt
        self.numInt = numInt
        self.colonia = colonia
        self.estado = estado
        self.ciudad = ciudad
        self.cp = cp
        self.municipio = municipio
        self.fechaCierre = fechaCierre
        self.idEmpleado = idEmpleado
        self.facturacionP = facturacionP
        self.rfc = rfc
        self.nombreOportunidad = nombreOportunidad
        self.nombreCuenta = nombreCuenta
    }

}
