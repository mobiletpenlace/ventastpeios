import Foundation

struct SFTablaLeadRequest: Codable {
    var idEmpleado: String
    
    init(idEmpleado: String) {
        self.idEmpleado = idEmpleado
    }
}
