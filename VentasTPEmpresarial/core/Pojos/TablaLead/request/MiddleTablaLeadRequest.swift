import Foundation

struct MiddleTablaLeadRequest: Codable {
    
    let login: LoginMiddleMinus = LoginMiddleMinus()
    var idEmpleado: String
    
    init(idEmpleado: String) {
        self.idEmpleado = idEmpleado
    }
}
