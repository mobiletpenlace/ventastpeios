import Foundation

struct SFTablaLeadResponse: Codable {
    let resultDescription: String?
    let result: String?
    let listaLead: [ListaLead]?
}
