import Foundation

struct MiddleTablaLeadResponse: Codable {
    
    let response: ResultMiddleMinus?
    let listaLead: [ListaLead]?
}
