import Foundation

struct ListaLead: Codable {
    let tipoPersona: String?
    let telefono: String?
    let razonSocial: String?
    let nombreEmpleado: String?
    let nombre: String?
    let primerApellido: String?
    let segundoApellido: String?
    let id: String?
    let correo: String?
    let estatus: String?
    let company: String?
}
