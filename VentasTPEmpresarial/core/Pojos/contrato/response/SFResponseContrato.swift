import Foundation

struct SFResponseContrato: Codable {
    let resultDescription: String?
    let result: String?
    let contrato: Contrato?
}
