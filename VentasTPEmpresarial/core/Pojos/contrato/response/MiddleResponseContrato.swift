import Foundation

struct MiddleResponseContrato: Codable {
    let response: ResultMiddleMinus?
    let contrato: Contrato?
}
