import Foundation

struct Contrato: Codable {
    let uRL: String?
    let idVenta: String?

    enum CodingKeys: String, CodingKey {
        case uRL = "URL"
        case idVenta = "idVenta"
    }
}
