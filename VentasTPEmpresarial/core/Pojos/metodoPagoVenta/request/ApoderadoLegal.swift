import Foundation

struct ApoderadoLegal: Codable {
    let nombre: String
    let apellidoPaterno: String
    let apellidoMaterno: String
    let correo: String
    let telefono: String
    let celular: String
}
