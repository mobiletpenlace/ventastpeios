import Foundation

struct SFRequestPagoVenta: Codable {
    let idOportunidad: String
    let idCuenta: String
    let metodoPago: MetodoPagoVenta
    let contactoFacturacion: ContactoFacturacion
    let apoderadoLegal: ApoderadoLegal
    let direccionFacturacion: DireccionFacturacion?
}
