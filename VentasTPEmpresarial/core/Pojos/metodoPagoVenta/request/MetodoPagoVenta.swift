import Foundation

struct MetodoPagoVenta: Codable {
    let nombretitular: String
    let aPTitular: String
    let aMTitular: String
    let tipoTarjeta: String
    let numeroTarjeta: String
    let fechaMM: String
    let fechaAA: String
    let metodoPago: String
    let tipoPago: String
}
