import Foundation

struct ContactoFacturacion: Codable {
    let nombre: String
    let apellidoPaterno: String
    let apellidoMaterno: String
    let telefono: String
    let celular: String
    let email: String
    let email2: String
}
