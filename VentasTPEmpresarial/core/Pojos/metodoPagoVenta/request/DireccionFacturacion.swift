import Foundation

struct DireccionFacturacion: Codable {
    let mismaDirInst: Bool
    let colonia: String
    let delegacionMunicipio: String
    let ciudad: String
    let estado: String
    let calle: String
    let numExterior: String
    let numInterior: String
    let codigoPostalFacturacion: String
}
