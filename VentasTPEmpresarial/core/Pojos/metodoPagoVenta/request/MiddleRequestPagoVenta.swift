import Foundation

struct MiddleRequestPagoVenta: Codable {
    let login: LoginMiddleMinus = LoginMiddleMinus()
    let idOportunidad: String
    let idCuenta: String
    let metodoPago: MetodoPagoVenta
    let contactoFacturacion: ContactoFacturacion
    let apoderadoLegal: ApoderadoLegal
    let direccionFacturacion: DireccionFacturacion?

}
