import Foundation

struct MiddleLoginRequest: Codable {
    let login: LoginMiddleMayus = LoginMiddleMayus()
    let noEmpleado: String
    let password: String
    let idSistema: String = ""

    init(noEmpleado: String, password: String) {
        self.noEmpleado = noEmpleado
        self.password = password
    }

    private enum CodingKeys: String, CodingKey {
        case login = "Login"
        case noEmpleado = "NoEmpleado"
        case password = "Password"
        case idSistema = "IdSistema"
    }
}
