import Foundation

struct MiddleLoginResponse: Codable {
    var response: ResultMiddleMayus?
    var infoUser: ResponseLoginInfouser?
    
    private enum CodingKeys: String, CodingKey {
        case response = "Response"
        case infoUser = "InfoUser"
    }
}
