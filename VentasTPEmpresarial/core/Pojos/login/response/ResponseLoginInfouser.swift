import Foundation

struct ResponseLoginInfouser: Codable {
    let name: String?
    let noEmpleado: String?
    let userType: String?
    let tipoC: String?
    let userRollID: String?
    
    let accountId: String?
    let email: String?
    let grupoC: String?
    let isActive: String?
    let id: String?
    let lastName: String?
    let grupoAsignadoC: String?
    
    private enum CodingKeys: String, CodingKey {
        case noEmpleado = "NoEmpledoC"
        case name = "Name"
        case userType = "UserType"
        case tipoC = "TipoC"
        case userRollID = "UserRollID"
        case accountId  = "AccountId"
        case email = "Email"
        case grupoC = "GrupoC"
        case isActive = "IsActive"
        case id = "Id"
        case lastName = "LastName"
        case grupoAsignadoC = "GrupoAsignadoC"
    }
    
}
