import Foundation

struct TokenResponse: Codable {
    let accessToken: String?
    let instanceUrl: String?
    let id: String?
    let tokenType: String?
    let issuedAt: String?
    let signature: String?

    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case instanceUrl = "instance_url"
        case id, signature
        case tokenType = "token_type"
        case issuedAt = "issued_at"
    }
}
