import Foundation

struct ConsultaCpRequest: Codable {
    let login: LoginMiddleMayus = LoginMiddleMayus()
    let codigoPostal: String
    
    private enum CodingKeys: String, CodingKey {
        case login = "Login"
        case codigoPostal = "CodigoPostal"
    }
    
    init(codigoPostal: String) {
        self.codigoPostal = codigoPostal
    }
}
