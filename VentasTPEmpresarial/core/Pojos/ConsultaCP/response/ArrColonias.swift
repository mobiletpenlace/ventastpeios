import Foundation

struct ArrColonias: Codable {
	let id: String?
	let nombre: String?
	let colonia: String?
	let ciudad: String?
	let clave: String?
	let cluster: String?
	let delegacionMunicipio: String?
	let estado: String?
	let idEstadoEN: String?
	let idEstadoTP: String?
	let idMunicipioEN: String?
	let idMunicipioTP: String?
	let idOrigen: String?
	let idPoblacionEN: String?
	let idPoblacionTP: String?
	let idRegionEN: String?
	let idRegionTP: String?
	let limite: String?
	let limite1: String?

	enum CodingKeys: String, CodingKey {
		case id = "Id"
		case nombre = "Nombre"
		case colonia = "Colonia"
		case ciudad = "Ciudad"
		case clave = "Clave"
		case cluster = "Cluster"
		case delegacionMunicipio = "DelegacionMunicipio"
		case estado = "Estado"
		case idEstadoEN = "IdEstadoEN"
		case idEstadoTP = "IdEstadoTP"
		case idMunicipioEN = "IdMunicipioEN"
		case idMunicipioTP = "IdMunicipioTP"
		case idOrigen = "idOrigen"
		case idPoblacionEN = "IdPoblacionEN"
		case idPoblacionTP = "IdPoblacionTP"
		case idRegionEN = "IdRegionEN"
		case idRegionTP = "IdRegionTP"
		case limite = "Limite"
		case limite1 = "Limite1"
	}
}
