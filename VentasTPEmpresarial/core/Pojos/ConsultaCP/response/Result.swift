import Foundation

struct Result: Codable {
	let result: String?
	let idResult: String?
	let description: String?

	enum CodingKeys: String, CodingKey {
		case result = "Result"
		case idResult = "IdResult"
		case description = "Description"
	}
}
