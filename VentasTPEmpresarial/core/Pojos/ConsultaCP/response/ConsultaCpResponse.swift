import Foundation

struct ConsultaCpResponse: Codable {
    let result: Result?
    let arrColonias: [ArrColonias]?

    enum CodingKeys: String, CodingKey {
        case result = "Result"
        case arrColonias = "ArrColonias"
    }
}
