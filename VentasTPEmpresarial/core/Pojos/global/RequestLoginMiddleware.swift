//
//  LoginRequest.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 16/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

struct LoginMiddleMayus: Codable {
    let user: String = Constantes.Passwords.user
    let password: String = Constantes.Passwords.password
    let ip: String = Constantes.Passwords.ip

    private enum CodingKeys: String, CodingKey {
        case user = "User"
        case password = "Password"
        case ip = "Ip"
    }
}
