import Foundation

struct LoginMiddleMinus: Codable {
    let user: String = Constantes.Passwords.user
    let password: String = Constantes.Passwords.password
    let ip: String = Constantes.Passwords.ip
}
