import Foundation

struct ResultMiddleMinus: Codable {
    let idResult: String?
    let result: String?
    let resultDescription: String?
}
