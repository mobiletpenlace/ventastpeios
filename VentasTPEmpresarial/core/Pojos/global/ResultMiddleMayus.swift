import Foundation

struct ResultMiddleMayus: Codable {
    var result: String?
    var resultId: String?
    var descriptionR: String?

    private enum CodingKeys: String, CodingKey {
        case result = "Result"
        case resultId = "ResultId"
        case descriptionR = "Description"
    }
}
