//
//  Login.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 30/01/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation

struct LoginMiddleMinus: Codable {
    let user: String = Constantes.Passwords.user
    let password: String = Constantes.Passwords.password
    let ip: String = Constantes.Passwords.ip

    enum CodingKeys: String, CodingKey {
        case user = "user"
        case password = "password"
        case ip = "ip"
    }
}
