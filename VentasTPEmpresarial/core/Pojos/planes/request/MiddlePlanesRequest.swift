import Foundation

struct MiddlePlanesRequest: Codable {
    let login: LoginMiddleMinus = LoginMiddleMinus()
    let idFamilia: String
    let valorFamilia: [String]

    init(valorFamilia: [String], idFamilia: String) {
        self.valorFamilia = valorFamilia
        self.idFamilia = idFamilia
    }
}
