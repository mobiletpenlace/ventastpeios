import Foundation

struct SFPlanesRequest: Codable {
    var idFamilia: String
    var valorFamilia: [String]

    init(valorFamilia: [String], idFamilia: String) {
        self.valorFamilia = valorFamilia
        self.idFamilia = idFamilia
    }
}
