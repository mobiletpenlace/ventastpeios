import Foundation

struct ListaPlanes: Codable {
    let valor: String?
    let unidadNegocioGIM: String?
    let tmcodeFwd: String?
    let tmcodeArrear: String?
    let tipoPlan: String?
    let tipoFamilia: String?
    let tipoFacturacion: String?
    let tipoCobertura: String?
    let tipoAprovisionamiento: String?
    let subtipoOportunidad: String?
    let productoPadreBRM: String?
    let precioProntoPagoSinIVa: Float?
    let precioProntoPago: Float?
    let precioListaSinIVA: Float?
    let precioDeLista: Float?
    let plazos: String?
    let plazoRetencion: String?
    let plazoPostventa: String?
    let planId: String?
    let nombreFamilia: String?
    let name: String?
    let montoPP: Float?
    let iVA: String?
    let iEPS: String?
    let id: String?
    let familiaId: String?
    let etiquetaBRM: String?
    let esVentaNueva: Bool?
    let estatus: String?
    let esPostVenta: Bool?
    let costoInstalacion: Float?
    
    enum CodingKeys: String, CodingKey {
        case valor, tmcodeFwd, tmcodeArrear, tipoPlan, tipoFamilia, tipoFacturacion
        case tipoCobertura, tipoAprovisionamiento, subtipoOportunidad
        case plazos, plazoRetencion, plazoPostventa, planId, nombreFamilia
        case name, montoPP, id, familiaId, productoPadreBRM
        case etiquetaBRM, esVentaNueva, estatus, esPostVenta, costoInstalacion
        case iVA = "IVA"
        case iEPS = "IEPS"
        case unidadNegocioGIM = "unidadNegocio_GIM"
        case precioProntoPagoSinIVa = "precio_Pronto_Pago_Sin_IVa"
        case precioProntoPago = "precio_Pronto_Pago"
        case precioListaSinIVA = "precio_Lista_Sin_IVA"
        case precioDeLista = "precio_de_Lista"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let converter = CodableConvert(keyContainer: values)
        
        valor = converter.string(.valor)
        unidadNegocioGIM = converter.string(.unidadNegocioGIM)
        tmcodeFwd = converter.string(.tmcodeFwd)
        tmcodeArrear = converter.string(.tmcodeArrear)
        tipoPlan = converter.string(.tipoPlan)
        tipoFacturacion = converter.string(.tipoFacturacion)
        tipoCobertura = converter.string(.tipoCobertura)
        tipoAprovisionamiento = converter.string(.tipoAprovisionamiento)
        subtipoOportunidad = converter.string(.subtipoOportunidad)
        productoPadreBRM = converter.string(.productoPadreBRM)
        plazos = converter.string(.plazos)
        plazoRetencion = converter.string(.plazoRetencion)
        plazoPostventa = converter.string(.plazoPostventa)
        planId = converter.string(.planId)
        name = converter.string(.name)
        iVA = converter.string(.iVA)
        iEPS = converter.string(.iEPS)
        id = converter.string(.id)
        familiaId = converter.string(.familiaId)
        etiquetaBRM = converter.string(.etiquetaBRM)
        estatus = converter.string(.estatus)
        tipoFamilia = converter.string(.tipoFamilia)
        nombreFamilia = converter.string(.nombreFamilia)
        
        precioProntoPagoSinIVa = converter.float(.precioProntoPagoSinIVa)
        precioProntoPago = converter.float(.precioProntoPago)
        precioListaSinIVA = converter.float(.precioListaSinIVA)
        precioDeLista = converter.float(.precioDeLista)
        montoPP = converter.float(.montoPP)
        esVentaNueva = converter.bool(.esVentaNueva)
        esPostVenta = converter.bool(.esPostVenta)
        costoInstalacion = converter.float(.costoInstalacion)
    }
}
