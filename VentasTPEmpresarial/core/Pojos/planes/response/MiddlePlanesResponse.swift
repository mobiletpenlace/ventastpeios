import Foundation

struct MiddlePlanesResponse: Codable {
    let response: ResultMiddleMinus?
    let listaPlanes: [ListaPlanes]?
}
