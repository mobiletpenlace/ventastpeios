import Foundation

struct SFPlanesResponse: Codable {
    let resultDescription: String
    let result: String
    let listaPlanes: [ListaPlanes]?
}
