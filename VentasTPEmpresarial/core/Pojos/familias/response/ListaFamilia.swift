import Foundation

struct ListaFamilia: Codable {
    let name: String?
    let id: String?
    let tipo: String?
}
