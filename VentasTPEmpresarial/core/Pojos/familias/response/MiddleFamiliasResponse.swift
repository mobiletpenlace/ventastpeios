import Foundation

struct MiddleFamiliasResponse: Codable {
    let response: ResultMiddleMinus?
    let listaFamilia: [ListaFamilia]?
}
