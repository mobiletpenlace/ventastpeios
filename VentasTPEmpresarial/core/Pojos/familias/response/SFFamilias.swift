import Foundation

struct SFFamiliasResponse: Codable {
    let resultDescription: String?
    let result: String?
    let listaFamilia: [ListaFamilia]?
}
