import Foundation

struct SFPaternityResponse: Codable {
    let resultDescription: String?
    let result: String?
    let validador: String?
}
