import Foundation

struct MiddlePaternityRequest: Codable {
    let login: LoginMiddleMinus = LoginMiddleMinus()
    let rfc: String?
    let idEmpleado: String?
    
    enum CodingKeys: String, CodingKey {
        case login
        case rfc = "RFC"
        case idEmpleado
    }
    
    init(idEmpleado: String, rfc: String) {
        self.rfc = rfc
        self.idEmpleado = idEmpleado
    }
    
}
