import Foundation

struct SFPaternityRequest: Codable {
    let rfc: String?
    let idEmpleado: String?
    
    enum CodingKeys: String, CodingKey {
        case rfc = "RFC"
        case idEmpleado
    }
    
    init(idEmpleado: String, rfc: String) {
        self.rfc = rfc
        self.idEmpleado = idEmpleado
    }
}
