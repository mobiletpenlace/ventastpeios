import Foundation

struct SFSolucionesMedidaResponse: Codable {
    let resultDescription: String?
    let result: String?
    let objectResponse: ObjectResponseSM?
}
