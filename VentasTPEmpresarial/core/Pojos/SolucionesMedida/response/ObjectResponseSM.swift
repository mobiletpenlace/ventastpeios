import Foundation

struct ObjectResponseSM: Codable {
    let valoresAgregados: [String]?
    let precios: String?
    let listaServicios: [ListaServiciosSM]?
    let listaProductos: [ListaProductosSM]?
    let equpoAsignado: [String]?
}
