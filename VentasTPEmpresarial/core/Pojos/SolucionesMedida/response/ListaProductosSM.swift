import Foundation

struct ListaProductosSM: Codable {
    let venta: Bool?
    let velocidadSubida: Int?
    let velocidadBajada: Int?
    let validarRed: Bool?
    let tipoProductoTexto: String?
    let tipoProducto: String?
    let regalo: Bool?
    let precioProntoPago: Float?
    let precioLista: Float?
    let precioEditable: Bool?
    let precioBase: Float?
    let postventa: Bool?
    let plazo: String?
    let nombreEditable: Bool?
    let nombreCarracteristica: String?
    let name: String?
    let montoImpuesto: Float?
    let megasBase: Bool?
    let maximoAgregar: Int?
    let iVA: Float?
    let iEPS: Float?
    let idBrmForward: String?
    let idBrmCU: String?
    let idBrmArrear: String?
    let id: String?
    let esVisible: Bool?
    let estatus: String?
    let esCargoUnico: Bool?
    let esAutomaticoCiudad: Bool?
    let dPProductoCaracteristicaId: String?
    let dPPlanServicioId: String?
    let costoIVA: Float?
    let cantidadDN: Int?
    let calcularRed: Bool?
    let agrupacionAddon: String?
    
    enum CodingKeys: String, CodingKey {
        case venta = "Venta"
        case velocidadSubida, velocidadBajada, nombreCarracteristica
        case validarRed = "ValidarRed"
        case tipoProductoTexto = "TipoProductoTexto"
        case tipoProducto = "TipoProducto"
        case regalo = "Regalo"
        case precioProntoPago = "PrecioProntoPago"
        case precioLista = "PrecioLista"
        case precioEditable = "PrecioEditable"
        case precioBase = "PrecioBase"
        case postventa = "Postventa"
        case plazo = "Plazo"
        case nombreEditable = "NombreEditable"
        case name = "Name"
        case montoImpuesto = "Monto_impuesto"
        case megasBase = "MegasBase"
        case maximoAgregar = "MaximoAgregar"
        case iVA = "IVA"
        case iEPS = "IEPS"
        case idBrmForward = "IdBrmForward"
        case idBrmCU = "IdBrmCU"
        case idBrmArrear = "IdBrmArrear"
        case id = "Id"
        case esVisible = "EsVisible"
        case estatus = "Estatus"
        case esCargoUnico = "EsCargoUnico"
        case esAutomaticoCiudad = "EsAutomaticoCiudad"
        case dPProductoCaracteristicaId = "DP_ProductoCaracteristicaId"
        case dPPlanServicioId = "DP_PlanServicioId"
        case costoIVA, cantidadDN
        case calcularRed = "CalcularRed"
        case agrupacionAddon = "AgrupacionAddon"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let converter = CodableConvert(keyContainer: values)
        
        tipoProductoTexto = converter.string(.tipoProductoTexto)
        tipoProducto = converter.string(.tipoProducto)
        plazo = converter.string(.plazo)
        nombreCarracteristica = converter.string(.nombreCarracteristica)
        name = converter.string(.name)
        idBrmForward = converter.string(.idBrmForward)
        idBrmCU = converter.string(.idBrmCU)
        idBrmArrear = converter.string(.idBrmArrear)
        id = converter.string(.id)
        estatus = converter.string(.estatus)
        dPProductoCaracteristicaId = converter.string(.dPProductoCaracteristicaId)
        dPPlanServicioId = converter.string(.dPPlanServicioId)
        agrupacionAddon = converter.string(.agrupacionAddon)
        
        precioProntoPago = converter.float(.precioProntoPago) ?? 0
        precioLista = converter.float(.precioLista) ?? 0
        precioBase = converter.float(.precioBase) ?? 0
        montoImpuesto = converter.float(.montoImpuesto) ?? 0
        iVA = converter.float(.iVA)
        iEPS = converter.float(.iEPS)
        costoIVA = converter.float(.costoIVA)
        
        velocidadSubida = converter.int(.velocidadSubida)
        velocidadBajada = converter.int(.velocidadBajada)
        maximoAgregar = converter.int(.maximoAgregar)
        cantidadDN = converter.int(.cantidadDN)
        
        venta = converter.bool(.venta)
        validarRed = converter.bool(.validarRed)
        regalo = converter.bool(.regalo)
        precioEditable = converter.bool(.precioEditable)
        postventa = converter.bool(.postventa)
        nombreEditable = converter.bool(.nombreEditable)
        megasBase = converter.bool(.megasBase)
        esVisible = converter.bool(.esVisible)
        esCargoUnico = converter.bool(.esCargoUnico)
        esAutomaticoCiudad = converter.bool(.esAutomaticoCiudad)
        calcularRed = converter.bool(.calcularRed)
    }
}
