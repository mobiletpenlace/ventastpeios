import Foundation

struct MiddleSolucionesMedidaRequest: Codable {
    let login: LoginMiddleMinus = LoginMiddleMinus()
    let idPlan: String
    
    init(idPlan: String) {
        self.idPlan = idPlan
    }
}
