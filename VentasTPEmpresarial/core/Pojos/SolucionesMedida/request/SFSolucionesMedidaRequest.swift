import Foundation

struct SFSolucionesMedidaRequest: Codable {
    let idPlan: String
    
    init(idPlan: String) {
        self.idPlan = idPlan
    }
}
