import Foundation

struct MiddleServiciosAdicionalesResponse: Codable {
    let response: ResultMiddleMinus?
    let listaAddonServicios: [ListaAddonServicios]?
}
