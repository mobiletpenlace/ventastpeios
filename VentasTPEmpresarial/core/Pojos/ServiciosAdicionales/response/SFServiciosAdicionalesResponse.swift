import Foundation

struct SFServiciosAdicionalesResponse: Codable {
    let resultDescription: String?
    let result: String?
    let listaAddonServicios: [ListaAddonServicios]?
}
