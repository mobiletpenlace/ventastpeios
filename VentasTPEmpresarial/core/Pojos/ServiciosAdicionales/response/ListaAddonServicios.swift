import Foundation

struct ListaAddonServicios: Codable {
    let tipoServicio: String?
    let sRVMode: String?
    let servicioAdicional: Bool?
    let seFactura: Bool?
    let precioLista: Float?
    let precio: Float?
    let nSGTemplate: String?
    let nSGIdTemplate: String?
    let nombreComercial: String?
    let name: String?
    var maximoAgregar: Int? = Int.max
    let id: String?
    let estatus: String?
    let esSDWAN: Bool?
    let cantidadAutomaticos: Int?
    let activo: Bool?
    
    enum CodingKeys: String, CodingKey {
        case sRVMode = "sRV_Mode"
        case tipoServicio, servicioAdicional, seFactura, precioLista, precio
        case nSGTemplate, nSGIdTemplate, nombreComercial, name, maximoAgregar
        case id, estatus, esSDWAN, cantidadAutomaticos, activo
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let converter = CodableConvert(keyContainer: values)
        
        tipoServicio = converter.string(.tipoServicio)
        sRVMode = converter.string(.sRVMode)
        nSGTemplate = converter.string(.nSGTemplate)
        nSGIdTemplate = converter.string(.nSGIdTemplate)
        nombreComercial = converter.string(.nombreComercial)
        name = converter.string(.name)
        id = converter.string(.id)
        estatus = converter.string(.estatus)
        
        servicioAdicional = converter.bool(.servicioAdicional)
        seFactura = converter.bool(.seFactura)
        esSDWAN = converter.bool(.esSDWAN)
        activo = converter.bool(.activo)
        
        precioLista = converter.float(.precioLista)
        precio = converter.float(.precio)
        
        maximoAgregar = converter.int(.maximoAgregar)
        cantidadAutomaticos = converter.int(.cantidadAutomaticos)
    }
}
