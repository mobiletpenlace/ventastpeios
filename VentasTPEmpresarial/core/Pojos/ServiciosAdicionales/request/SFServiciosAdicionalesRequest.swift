import Foundation

struct SFServiciosAdicionalesRequest: Codable {
    let idPlan: String

    init(idPlan: String) {
        self.idPlan = idPlan
    }
}
