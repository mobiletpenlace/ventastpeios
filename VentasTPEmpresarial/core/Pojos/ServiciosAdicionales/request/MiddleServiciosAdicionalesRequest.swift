import Foundation

struct MiddleServiciosAdicionalesRequest: Codable {
    
    let login: LoginMiddleMinus = LoginMiddleMinus()
    let idPlan: String
    
    init(idPlan: String) {
        self.idPlan = idPlan
    }
}
