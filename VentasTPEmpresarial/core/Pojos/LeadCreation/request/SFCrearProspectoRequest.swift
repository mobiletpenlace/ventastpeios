import Foundation

struct SFCrearProspectoRequest: Codable {
    let tipoPersona: String
    let company: String
    let lastName: String
    let middleName: String
    let firstName: String
    let idEmpleado: String
    let telefono: String
    let correo: String
    let accion: String
    let idProspecto: String
    let razonSocial: String
    
    init(tipoPersona: String, company: String, lastName: String, middleName: String, firstName: String, idEmpleado: String, telefono: String, correo: String, accion: String, idProspecto: String, razonSocial: String) {
        self.tipoPersona = tipoPersona
        self.company = company
        self.lastName = lastName
        self.middleName = middleName
        self.firstName = firstName
        self.idEmpleado = idEmpleado
        self.telefono = telefono
        self.correo = correo
        self.accion = accion
        self.idProspecto = idProspecto
        self.razonSocial = razonSocial
    }
}
