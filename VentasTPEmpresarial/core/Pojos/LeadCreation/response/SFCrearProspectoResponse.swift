import Foundation

struct SFCrearProspectoResponse: Codable {
    let resultDescription: String?
    let result: String?
    let listaMisVentas: [ListaMisVentas]?
}
