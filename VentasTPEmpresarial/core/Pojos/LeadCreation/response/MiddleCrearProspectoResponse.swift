import Foundation

struct MiddleCrearProspectoResponse: Codable {
    let response: ResultMiddleMinus?
    let listaMisVentas: [ListaMisVentas]?
}
