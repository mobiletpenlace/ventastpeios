import Foundation

struct SFCrearOportunidadRequest: Codable {
    let idCuenta: String
    let nombreOportunidad: String
    let fechaCierreOportunidad: String
    let empleado: String
    let accion: String
    
    init(idCuenta: String, nombreOportunidad: String, fechaCierreOportunidad: String, empleado: String, accion: String){
        self.idCuenta = idCuenta
        self.nombreOportunidad = nombreOportunidad
        self.fechaCierreOportunidad = fechaCierreOportunidad
        self.empleado = empleado
        self.accion = accion
    }
}
