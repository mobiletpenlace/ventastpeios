//
//  crearOportunidadRequest.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 09/01/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation

class CrearOportunidadRequest: Codable {
    var idCuenta : String?
    var nombreOportunidad : String?
    var fechaCierreOportunidad : String?
    var empleado : String?
    var accion : String?
    
    enum CodingKeys: String, CodingKey {
        case idCuenta = "idCuenta"
        case nombreOportunidad = "nombreOportunidad"
        case fechaCierreOportunidad = "fechaCierreOportunidad"
        case empleado = "empleado"
        case accion = "accion"
    }
    
    required init(idCuenta : String, nombreOportunidad : String, fechaCierreOportunidad : String, empleado : String, accion : String){
        self.idCuenta = idCuenta
        self.nombreOportunidad = nombreOportunidad
        self.fechaCierreOportunidad = fechaCierreOportunidad
        self.empleado = empleado
        self.accion = accion
    }
    
}
