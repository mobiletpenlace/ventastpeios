import Foundation

struct MiddleCrearOportunidadResponse: Codable {
    let response: ResultMiddleMinus?
    let numeroOportunidad: String?
    let idNuevaOportunidad: String?
}
