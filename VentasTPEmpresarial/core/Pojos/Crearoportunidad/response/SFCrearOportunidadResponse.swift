import Foundation

struct SFCrearOportunidadResponse: Codable {
    let resultDescription: String?
    let result: String?
    let numeroOportunidad: String?
    let idNuevaOportunidad: String?
}
