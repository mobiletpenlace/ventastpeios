import Foundation

struct MiddleTablaVentasRequest: Codable {
    let login: LoginMiddleMinus = LoginMiddleMinus()
    let idEmpleado: String
    
    init(idEmpleado: String) {
        self.idEmpleado = idEmpleado
    }
}
