import Foundation

struct SFTablaVentasRequest: Codable {
    let idEmpleado: String
//    let fechaInicio: String
//    let fechaFin: String
    
    enum CodingKeys: String, CodingKey {
        case idEmpleado = "idEmpleado"
//        case fechaInicio = "fechainicio"
//        case fechaFin = "fechaFin"
    }
    
    init(idEmpleado: String){//, fechaInicio : String, fechaFin : String) {
        self.idEmpleado = idEmpleado
//        self.fechaInicio = fechaInicio
//        self.fechaFin = fechaFin
    }
    
}
