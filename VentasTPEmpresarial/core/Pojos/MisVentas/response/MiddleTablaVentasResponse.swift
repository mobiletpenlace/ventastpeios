import Foundation

struct MiddleTablaVentasResponse: Codable {
    let response: ResultMiddleMinus?
    var listaMisVentas: [ListaMisVentas]?
}
