import Foundation

struct ListaMisVentas: Codable {
    let noOportunidad: String?
    let nombreVenta: String?
    let nombreEmpresa: String?
    let nombreCuenta: String?
    let nombreEmpleado: String?
    let nombreContactoPrincipal: String?
    let isBlock: Bool?
    let idOportunidad: String?
    let idCuenta: String?
    let fechaCreacion: String?
    let etapaOportunidad: String?
    let etapaCotizacion: String?
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let converter = CodableConvert(keyContainer: values)
        
        noOportunidad = converter.string(.noOportunidad)
        nombreVenta = converter.string(.nombreVenta)
        nombreEmpresa = converter.string(.nombreEmpresa)
        nombreCuenta = converter.string(.nombreCuenta)
        nombreEmpleado = converter.string(.nombreEmpleado)
        nombreContactoPrincipal = converter.string(.nombreContactoPrincipal)
        idOportunidad = converter.string(.idOportunidad)
        idCuenta = converter.string(.idCuenta)
        fechaCreacion = converter.string(.fechaCreacion)
        etapaOportunidad = converter.string(.etapaOportunidad)
        etapaCotizacion = converter.string(.etapaCotizacion)
        isBlock = converter.bool(.isBlock)
    }
}
