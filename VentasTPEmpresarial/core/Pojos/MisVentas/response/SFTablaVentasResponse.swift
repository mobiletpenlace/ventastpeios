import Foundation

struct SFTablaVentasResponse: Codable {
	let resultDescription: String?
	let result: String?
	var listaMisVentas: [ListaMisVentas]?
}
