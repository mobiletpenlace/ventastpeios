import Foundation

struct SFProductosAdicionalesAddonsRequest: Codable {
    let idServicio: String

    init(idServicio: String) {
        self.idServicio = idServicio
    }
}
