import Foundation

struct MiddleProductosAdicionalesAddonsRequest: Codable {
    let login: LoginMiddleMinus = LoginMiddleMinus()
    let idServicio: String
    
    init(idServicio: String) {
        self.idServicio = idServicio
    }
}
