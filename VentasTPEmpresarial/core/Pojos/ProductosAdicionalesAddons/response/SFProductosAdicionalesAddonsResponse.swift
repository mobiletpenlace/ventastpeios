import Foundation

struct SFProductosAdicionalesAddonsResponse: Codable {
    let resultDescription: String?
    let result: String?
    let listaProductos: [ListaProductosAdicionales]?
}
