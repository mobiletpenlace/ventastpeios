import Foundation

struct MiddleProductosAdicionalesAddonsResponse: Codable {
    let response: ResultMiddleMinus?
    let listaProductos: [ListaProductosAdicionales]?
    
}
