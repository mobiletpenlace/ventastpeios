import Foundation

struct ListaProductosAdicionales: Codable {
    let precioProntoPago: Float?
    let precioLista: Float?
    let precioBase: Float?
    let nameCarracteristica: String?
    let name: String?
    let monto_impuesto: Float?
    let maximoAgregar: Int?
    let iVA: Float?
    let id: String?
    let convivencia: [Convivencia]?
    let agrupacionAddon: String?
    
    enum CodingKeys: String, CodingKey {
        case nameCarracteristica, name, id, convivencia
        case precioProntoPago = "PrecioProntoPago"
        case precioLista = "PrecioLista"
        case precioBase = "PrecioBase"
        case monto_impuesto = "Monto_impuesto"
        case maximoAgregar = "MaximoAgregar"
        case iVA = "IVA"
        case agrupacionAddon = "AgrupacionAddon"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        let converter = CodableConvert(keyContainer: values)
        
        precioProntoPago = converter.float(.precioProntoPago)
        precioLista = converter.float(.precioLista)
        precioBase = converter.float(.precioBase)
        monto_impuesto = converter.float(.monto_impuesto)
        iVA = converter.float(.iVA)
        
        maximoAgregar = converter.int(.maximoAgregar)
        
        nameCarracteristica = converter.string(.nameCarracteristica)
        name = converter.string(.name)
        id = converter.string(.id)
        agrupacionAddon = converter.string(.agrupacionAddon)
        
        convivencia = converter.wrap([Convivencia].self, .convivencia)
    }
}
