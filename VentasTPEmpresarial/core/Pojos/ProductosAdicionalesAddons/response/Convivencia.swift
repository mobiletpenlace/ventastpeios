import Foundation

struct Convivencia: Codable {
    let productoId: String?
    let nombreProductoExcl: String?
    let nombreProducto: String?
    let idProductoExcl: String?
    let idConvivencia: String?
}
