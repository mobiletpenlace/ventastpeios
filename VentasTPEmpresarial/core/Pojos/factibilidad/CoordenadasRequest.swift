import Foundation

struct MiddleCoordenadasRequest: Codable {
    var login: LoginMiddleMayus
    var coordenadas: CoordenadasModel

    private enum CodingKeys: String, CodingKey {
        case login = "Login"
        case coordenadas = "Coordenadas"
    }
    
    init(lat: String, long: String, tCliente: String) {
        self.login = LoginMiddleMayus()
        self.coordenadas = CoordenadasModel(lat: lat, long: long, tCliente: tCliente)
    }
}

struct CoordenadasModel: Codable {
    var lat: String
    var long: String
    var tCliente: String
    
    init(lat: String, long: String, tCliente: String) {
        self.lat = lat
        self.long = long
        self.tCliente = tCliente
    }
    
    private enum CodingKeys: String, CodingKey {
        case lat = "latitud"
        case long = "longitud"
        case tCliente = "TipoCliente"
    }
}
