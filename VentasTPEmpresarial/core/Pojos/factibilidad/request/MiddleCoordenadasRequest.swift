import Foundation

struct MiddleCoordenadasRequest: Codable {
    var login: LoginMiddleMayus
    var coordenadas: CoordenadasModel

    private enum CodingKeys: String, CodingKey {
        case login = "Login"
        case coordenadas = "Coordenadas"
    }
    
    init(lat: String, long: String, tCliente: String) {
        self.login = LoginMiddleMayus()
        self.coordenadas = CoordenadasModel(lat: lat, long: long, tCliente: tCliente)
    }
}
