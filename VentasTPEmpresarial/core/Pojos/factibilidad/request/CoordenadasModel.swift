import Foundation

struct CoordenadasModel: Codable {
    var lat: String
    var long: String
    var tCliente: String
    
    init(lat: String, long: String, tCliente: String) {
        self.lat = lat
        self.long = long
        self.tCliente = tCliente
    }
    
    private enum CodingKeys: String, CodingKey {
        case lat = "latitud"
        case long = "longitud"
        case tCliente = "TipoCliente"
    }
}
