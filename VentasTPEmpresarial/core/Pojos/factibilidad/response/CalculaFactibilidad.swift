import Foundation

struct CalculaFactibilidad: Codable {
	let factible: String?
	let factibilidad: String?
	let direccion: String?
	let porcentaje: String?
	let criterios: String?
	let comentarios: String?
	let idRegion: String?
	let region: String?
	let cuidad: String?
	let distrito: String?
	let zona: String?
	let nombre_cluster: String?
	let categoryService: String?
	let nSE: String?
	let detalle_Respuesta: DetalleRespuesta?

	enum CodingKeys: String, CodingKey {
		case factible, factibilidad, direccion, porcentaje, criterios, comentarios, distrito, zona
		case idRegion = "IdRegion"
		case region = "Region"
		case cuidad = "Cuidad"
		case nombre_cluster = "nombre_cluster"
		case categoryService = "CategoryService"
		case nSE = "NSE"
		case detalle_Respuesta = "Detalle_Respuesta"
	}
}
