import Foundation

struct MiddleCoordenadasResponse: Codable {
    let calculaFactibilidad: CalculaFactibilidad?

    enum CodingKeys: String, CodingKey {
        case calculaFactibilidad = "CalculaFactibilidad"
    }
}


