import Foundation

struct DetalleRespuesta: Codable {
    let codigoRespuesta: String?
    let codigoError: String?
    let descripcionError: String?
    let mensajeError: String?

    enum CodingKeys: String, CodingKey {
        case codigoRespuesta = "CodigoRespuesta"
        case codigoError = "CodigoError"
        case descripcionError = "DescripcionError"
        case mensajeError = "MensajeError"
    }
}
