import Foundation

struct SFCuentasRequest: Codable {
    let idEmpleado: String
    
    init(idEmpleado: String){
        self.idEmpleado = idEmpleado
    }
}
