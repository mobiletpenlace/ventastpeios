import Foundation

struct ListaCuentas: Codable {
    let telefono: String?
    let segmentoFacturacion: String?
    let rfc: String?
    let name: String?
    let id: String?
    let folioCuenta: String?
    let email: String?
}
