import Foundation

struct MiddleCuentasResponse: Codable {
    
    let response: ResultMiddleMinus?
    let listaCuentas: [ListaCuentas]?
    
}
