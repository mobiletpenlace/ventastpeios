import Foundation

struct SFCuentasResponse: Codable {
	let resultDescription: String?
	let result: String?
	let listaCuentas: [ListaCuentas]?
}
