//
//  ViewController.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 01/06/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class MenuTabBarVC: BaseMenuVC {

    @IBOutlet weak var mContainerView: UIView!
    @IBOutlet weak var viewHeader: UIView!

    @IBOutlet weak var containerView1: UIView!
    @IBOutlet weak var containerView2: UIView!
    @IBOutlet weak var containerView3: UIView!
    @IBOutlet weak var containerView4: UIView!
    @IBOutlet weak var containerView5: UIView!
    @IBOutlet weak var containerView6: UIView!
//    @IBOutlet weak var containerView7: UIView!

    @IBOutlet weak var viewPadre1: UIView!
    @IBOutlet weak var viewPadre2: UIView!
    @IBOutlet weak var viewPadre3: UIView!
    @IBOutlet weak var viewPadre4: UIView!
    @IBOutlet weak var viewPadre5: UIView!
    @IBOutlet weak var viewPadre6: UIView!
//    @IBOutlet weak var viewPadre7: UIView!

    private var viewControllersButtons = [ItemBarVC]()
    private var viewContainers = [UIView]()
    private var viewContainersPadre = [UIView]()
    private var viewControllersName = [[String: String]]()

    override func viewDidLoad() {
        super.viewDidLoad()
        super.addHeaderView(container: viewHeader)
        addItemsButtons()

        selectBottonMenu(pos: 0, sender: nil)//seleccionar el menu 0
        super.cambiarVista(index: 0)//seleccionar el menu 0

        SwiftEventBus.onMainThread(self, name: "menuTabBar-cambiarMenuItem") { [weak self] result in
            if let (menuIndex, itemIndex) = result!.object as? (Int, Int) {
                self?.cambiarMenuYSubMenu(posMenu: menuIndex, posItem: itemIndex)
            }
        }

        SwiftEventBus.onMainThread(self, name: "menuTabBar-cambiarMenuChilds") { [weak self] result in
            if let posiciones = result!.object as? [Int] {
                self?.cambiarMenuChilds(posiciones)
            }
        }
    }

    func addItemsButtons() {
        addItemButton(
            titulo: "Mi Perfil",
            imagen: #imageLiteral(resourceName: "logo_MisVentas"),
            view: containerView1,
            viewPadre: viewPadre1,
            vcName: ["nombre": "MisVentasVC"]
        )

        addItemButton(
            titulo: "Cuentas",
            imagen: #imageLiteral(resourceName: "logo_GenerarLead"),
            view: containerView2,
            viewPadre: viewPadre2,
            vcName: ["nombre": "GenerarCargaVC"]
        )

        addItemButton(
            titulo: "Sitios",
            imagen: #imageLiteral(resourceName: "logo_Sitio"),
            view: containerView3,
            viewPadre: viewPadre3,
            vcName: ["nombre": "SitiosVC"]
        )

        addItemButton(
            titulo: "Cotizar",
            imagen: #imageLiteral(resourceName: "logo_Cotizar"),
            view: containerView4,
            viewPadre: viewPadre4,
            vcName: ["nombre": "CotizacionVC"]
        )

        addItemButton(
            titulo: "Tips de Venta",
            imagen: #imageLiteral(resourceName: "logo_tipsVentas"),
            view: containerView5,
            viewPadre: viewPadre5,
            vcName: ["nombre": "TipsVentasVC"]
        )
//        addItemButton(
//            titulo:"Preguntas Frecuentes",
//            imagen: #imageLiteral(resourceName: "logo_PreguntasFrecuentes"),
//            view: containerView6,
//            viewPadre: viewPadre6,
//            vcName: ["nombre":"PreguntasFrecuentesVC"]
//        )
        super.addViewStoryBoardList(view: mContainerView, viewControllers: viewControllersName)
    }

    @IBAction func buttonMenu1(_ sender: UIButton) {
        activarBoton(pos: 0, sender: sender)
    }

    @IBAction func buttonMenu2(_ sender: UIButton) {
        activarBoton(pos: 1, sender: sender)
    }

    @IBAction func buttonMenu3(_ sender: UIButton) {
        activarBoton(pos: 2, sender: sender)
    }

    @IBAction func buttonMenu4(_ sender: UIButton) {
        activarBoton(pos: 3, sender: sender)
    }

    @IBAction func buttonMenu5(_ sender: UIButton) {
        activarBoton(pos: 4, sender: sender)
    }

    @IBAction func buttonMenu6(_ sender: UIButton) {
        activarBoton(pos: 5, sender: sender)
    }
//
//    @IBAction func buttonMenu7(_ sender: UIButton) {
//        activarBoton(pos: 6, sender: sender)
//    }

    func addItemButton(titulo: String, imagen: UIImage, view: UIView, viewPadre: UIView, vcName: [String: String]) {
        viewControllersName.append(vcName)
        let imgTint = imagen.withRenderingMode(.alwaysTemplate)
        let vc = ItemBarVC(titulo: titulo, imagen: imgTint)
        viewControllersButtons.append(vc)
        viewContainers.append(view)
        viewContainersPadre.append(viewPadre)
        super.addViewXIB(vc: vc, container: view)
    }

    private func selectBottonMenu(pos: Int, sender: UIButton? = nil) {
        for (index, itemvc) in viewControllersButtons.enumerated() {
            if index == pos {
                itemvc.titleMenuBar.textColor = .black
                itemvc.iconMenuBar.tintColor = UIColor.red

                viewContainersPadre[index].backgroundColor = .white
                if let button = sender {
                    button.animateBound(view: viewContainers[index])
                    button.animateScale(view: viewContainersPadre[index])
                }
            } else {
                itemvc.iconMenuBar.tintColor = UIColor.white
                itemvc.titleMenuBar.textColor = .white
                viewContainersPadre[index].backgroundColor = Colores.azulOscuro
            }
        }
    }

    func activarBoton(pos: Int, sender: UIButton?) {
        guard posicionIndex != pos else { return }
        selectBottonMenu(pos: pos, sender: sender)
        super.cambiarVista(index: pos)
    }

    func cambiarMenuYSubMenu(posMenu: Int, posItem: Int) {
        selectBottonMenu(pos: posMenu)
        cambiarVistaChild(index: posMenu)
        viewControllersItems[posMenu].cambiarVista(index: posItem)
    }

    func cambiarMenuChilds(_ posiciones: [Int]) {
        var vc: BaseTreeVC = self
        for (ind, pos) in posiciones.enumerated() {
            if ind == 0 {
                vc.cambiarVistaInternamente(index: pos)
            } else {
                vc.cambiarVista(index: pos)
            }
            if vc.viewControllersItems.count > pos {
                vc = vc.viewControllersItems[pos]
            }
        }
    }
}
