import UIKit

class ItemBarVC: BaseItemVC {

    @IBOutlet weak var iconMenuBar: UIImageView!
    @IBOutlet weak var titleMenuBar: UILabel!
    
    var titulo: String
    var imagen: UIImage
    
    init(titulo: String, imagen: UIImage) {
        self.titulo = titulo
        self.imagen = imagen
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleMenuBar.text = titulo
        iconMenuBar.image = imagen
    }
}
