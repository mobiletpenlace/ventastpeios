//
//  HeaderEmpresarialViewController.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 03/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus
import DropDown

class HeaderViewController: BaseTreeVC {
    @IBOutlet weak var logoTpe: UIImageView!
    @IBOutlet weak var laberUser: UILabel!
    @IBOutlet weak var imageViewUser: UIImageView!
    @IBOutlet weak var buttonBack: UIButton!

    @IBOutlet weak var stackviewTitulo1: UIStackView!
    @IBOutlet weak var labelTitulo1: UILabel!

    @IBOutlet weak var stackviewTitulo2: UIStackView!
    @IBOutlet weak var labelTitulo2: UILabel!

    @IBOutlet weak var stackviewTitulo3: UIStackView!
    @IBOutlet weak var labelTitulo3: UILabel!

    @IBOutlet weak var imageViewTitulo: UIImageView!

    @IBOutlet weak var labelTitulo: UILabel!

    @IBOutlet weak var viewAgenda: UIView!
    @IBOutlet weak var viewGuardarVenta: UIView!
    @IBOutlet weak var viewUser: UIView!
    @IBOutlet weak var btnUser: UIButton!
    let dropManager = DropDownUtil()

    var closure: (() -> Void)?
    let service = HeaderViewService()

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }

    private func setUpView() {

        addBorderImage()
        self.viewGuardarVenta.isHidden = true

        actualizarUsuarioDatos()

        stackviewTitulo1.isHidden = true
        stackviewTitulo2.isHidden = true
        stackviewTitulo3.isHidden = true
        labelTitulo.text = ""
        labelTitulo1.text = ""
        labelTitulo2.text = ""
        labelTitulo3.text = ""

        //recibir eventos de actualizacion del header
        SwiftEventBus.onMainThread(self, name: Constantes.Eventbus.Id.header) { [weak self] result in
            if let headerData = result!.object as? HeaderData {
                self?.setTitleImage(
                    imagen: headerData.imagen,
                    titulo: headerData.titulo ?? "",
                    titulo2: headerData.titulo2,
                    titulo3: headerData.titulo3,
                    subtitulo: headerData.subtitulo,
                    closure: headerData.closure
                )
            }
        }

        SwiftEventBus.onMainThread(self, name: "Header(mostrarDialogoCustom)") { [weak self] result in
            if let dialogo = result!.object as? BaseDialogCustom {
                self?.showDialog(dialogo)
            }
        }
    }

    func actualizarUsuarioDatos() {
        let usuario = service.getNombreEmpleado()
        laberUser.text = "Bienvenido(a) " + usuario
        imageViewUser.image = UIImage(named: "img_UserProfile2")
    }

    func addBorderImage() {
        imageViewUser.layer.borderColor = UIColor.red.cgColor
        imageViewUser.layer.borderWidth = 1.5
        imageViewUser.layer.cornerRadius = (imageViewUser?.frame.size.height)! / 1.35
        imageViewUser.clipsToBounds = true
    }

    @IBAction func clickLogo(_ sender: UIButton) {
        sender.animateBound(view: logoTpe)
        //super.loadStoryBoard(name: "Login", viewController: "LoginViewController")
    }

    @IBAction func clickBack(_ sender: UIButton) {
        if closure != nil {
            closure!()
        }
    }

    @IBAction func clickButtonUser(_ sender: UIButton) {

        sender.animateBound(view: viewUser)/*{ [weak self] _ in
            self?.configurar(posicion:0)
        }*/

    }

    @IBAction func clickAgenda(_ sender: UIButton) {
        //DialogoAgendaVC
        sender.animateBound(view: viewAgenda) { [weak self] _ in
            self?.showDialog(customAlert: DialogoAgendaVC())
        }
    }

    @IBAction func clickButtonGuardarVenta(_ sender: UIButton) {
        sender.animateBound(view: viewGuardarVenta)
    }

    private func showDialog(customAlert: BaseDialogCustom) {
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(customAlert, animated: true, completion: nil)
    }

    func setTitleImage(imagen: UIImage?, titulo: String, titulo2: String? = nil, titulo3: String? = nil, subtitulo: String? = nil, closure: (() -> Void)? = nil) {
        labelTitulo.text? = titulo
        imageViewTitulo.image? = (imagen == nil) ? UIImage() : imagen!
        self.closure = closure

        if let titulo = titulo2 {
            labelTitulo1.text = titulo
            stackviewTitulo1.isHidden = false
        } else {
            labelTitulo1.text = ""
            stackviewTitulo1.isHidden = true
        }

        if let titulo = titulo3 {
            labelTitulo2.text = titulo
            stackviewTitulo2.isHidden = false
        } else {
            labelTitulo2.text = ""
            stackviewTitulo2.isHidden = true
        }

        if let titulo = subtitulo {
            stackviewTitulo3.isHidden = false //mostrar el subtitulo final
            labelTitulo3.text = titulo
        } else {
            stackviewTitulo3.isHidden = true //ocultar el subtitulo final
            labelTitulo3.text = ""
        }

        if closure == nil {
            desactivarBotonRegresar()
        } else {
            activarBotonRegresar()
        }
    }

    func desactivarBotonRegresar() {
        buttonBack.isHidden = true
//        buttonBack.alpha = 0
    }

    func activarBotonRegresar() {
        buttonBack.isHidden = false
//        buttonBack.alpha = 1
    }

}

extension HeaderViewController: BaseTreeDialogs {

    func showDialog(_ dialogo: BaseDialogCustom) {
        showDialog(vc: self, customAlert: dialogo)
    }

    /*func configurar(posicion:Int) {
        let alert = UIAlertController(
            title: "Configurar",
            message: "¿Que acción desea realizar?",
            preferredStyle: .alert
        )
        var titulo = "Tomar Foto"
        let action1 = UIAlertAction(title: titulo, style: .default){ [weak self] _ in
            
        }
        titulo = "Cerrar Sesión"
        let action2 = UIAlertAction(title: titulo, style: .default){ [weak self] _ in
            
        }
        
        
        titulo = "Cancelar"
        let cancel = UIAlertAction(title: titulo, style: .destructive, handler: nil)
        alert.view.tintColor = UIColor.blue
        alert.view.layer.cornerRadius = 25
        
        
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
        
        
    }*/

}
