//
//  HeaderViewPresenter.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 28/02/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol HeaderViewPresenterDelegate {
    func seleccionarDropDown(pos: Int)
}

class HeaderViewPresenter {

    let configurar = ["Cerrar Sesión", "Editar Foto"]

    func getConfigureUser() -> [String] {
        return configurar
    }

    func seleccionadoPlazo(configurar: String) {

    }

}
