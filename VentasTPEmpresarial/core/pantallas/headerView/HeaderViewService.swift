//
//  HeaderViewService.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 12/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import RealmSwift

class HeaderViewService: BaseDBManagerDelegate {
    var realm: Realm!
    var serverManager: ServerDataManager?
    var dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()
        serverManager = ServerDataManager()
        realm = getRealm()
    }

    func getNombreEmpleado() -> String {
        return dbManager.getNombreEmpleado()
    }
}
