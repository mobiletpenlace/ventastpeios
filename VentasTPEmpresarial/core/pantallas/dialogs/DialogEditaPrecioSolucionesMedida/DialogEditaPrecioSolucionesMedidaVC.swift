//
//  DialogEditaPrecioSolucionesMedidaVC.swift
//  VentasTPEmpresarial
//
//  Created by eduardo mancilla on 11/29/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class DialogEditaPrecioSolucionesMedidaVC: BaseDialogCustom, BaseFormatNumber {
    @IBOutlet weak var viewRoot: UIView!
    @IBOutlet weak var labelTitulo: UILabel!
    @IBOutlet weak var labelTexto1: UILabel!
    @IBOutlet weak var textField1: UITextField!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    var minimoPrecio: Float = 0

    init(delegate: CustomAlertViewDelegate?, minimoPrecio: Float) {
        super.init(nibName: nil, bundle: nil)
        self.delegate = delegate
        self.minimoPrecio = minimoPrecio
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        super.setView(view: viewRoot)
    }

    func setUpView() {
        textField1.delegate = self
        label3.text = "\(numeroAFormatoPrecio(numero: minimoPrecio))"
    }

    @IBAction func clickAceptar(_ sender: UIButton) {
        sender.animateBound(view: sender) { [unowned self] _ in
            if self.validarDatos() {
                self.finish()
            }
        }
    }

    func validarDatos() -> Bool {
        let texto = textField1.text ?? ""
        if texto.count == 0 {
            return false
        }
        guard let precio = Float(texto) else {
            return false
        }
        if precio < minimoPrecio {
            label3.textColor = UIColor.red
            label3.animateBound(view: label3)
            return false
        }
        return true
    }

    func finish() {
        super.result(value: self.textField1.text!)
    }

    func cancelar() {
        super.cancel()
    }

    @IBAction func clickCerrar(_ sender: UIButton) {
        sender.animateBound(view: sender) { [unowned self] _ in
            self.cancelar()
        }
    }

}

extension DialogEditaPrecioSolucionesMedidaVC: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let letras = CharacterSet.decimalDigits
        let limiteDeCaracteres = 10

        //revisamos si el caracter ingresado es válido
        if string.rangeOfCharacter(from: letras.inverted) != nil {
            return false
        }
        //revisamos si el caracter no rebasa el límite de caracteres
        if ((textField.text?.count)! + (string.count - range.length)) > limiteDeCaracteres {
            return false
        }
        return true
    }
}
