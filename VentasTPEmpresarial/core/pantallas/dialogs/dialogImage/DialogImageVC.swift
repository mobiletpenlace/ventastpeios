//
//  DialogImageVC.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 27/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class DialogImageVC: BaseDialogCustom {
    @IBOutlet weak var viewRoot: UIView!
    @IBOutlet weak var labelTitulo: UILabel!
    @IBOutlet weak var imageViewFoto: UIImageView!
    var titulo: String?
    var imagen: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: viewRoot)
        reloadFoto()
    }
    
    func iniciar(titulo: String, imagen: UIImage) {
        self.titulo = titulo
        self.imagen = imagen
        reloadFoto()
    }
    
    func reloadFoto() {
        if labelTitulo != nil && imageViewFoto != nil {
            labelTitulo.text = titulo
            imageViewFoto.image = imagen
        }
    }
    
    @IBAction func clickAceptar(_ sender: UIButton) {
        super.result(value: "bien")
    }
    
}
