//
//  DialogoInformacionTipsVentasVC.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 23/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import PDFKit

class DialogoPDF: BaseDialogCustom, BaseTreeLoadingDelegate {
    @IBOutlet weak var mViewContainerInformacion: UIView!
    @IBOutlet weak var viewRoot: UIView!
    @IBOutlet weak var pdfView: PDFView!
    @IBOutlet weak var imageViewDelete: UIImageView!
    var loadingBar: UIView?

    /*
     init(text: String, titulo: String){
     self.text = text
     self.titulo = titulo
     super.init(nibName: nil, bundle: nil)
     }
     
     required init?(coder aDecoder: NSCoder) {
     fatalError("init(coder:) has not been implemented")
     }
     */

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: viewRoot)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setUpView()
    }

    private func setUpView() {
        cargarPDFFromURL()
    }

    @IBAction func clickCerrar(_ sender: UIButton) {
        sender.animateBound(view: imageViewDelete) { [unowned self] _ in
            self.cerrar()
        }
    }
    private func cerrar() {
        super.cancel()
    }

    func cargarPDFFromURL() {
        guard let url = URL(string: "https://mss.totalplay.com.mx/TFE/DownloadFile/dGZldXNlcjp0ZjNwNHNzdzByZA==/L1ZlbnRhcy9Ub3RhbHBsYXkvNi4wMjcyMi9Eb2N1bWVudG9zL0NvbnRyYXRvNi4wMjcyMi5wZGY=") else { return }
        if let pdfDocument = PDFDocument(url: url) {
            loadingBar = showLoading(onView: pdfView)
            configPDF(pdfDocument: pdfDocument)
            if loadingBar != nil {
                hideLoading(spinner: loadingBar!)
            }
        } else {
            Logger.println("nose pudo crear el pdfdocument.")
        }
    }

    func configPDF(pdfDocument: PDFDocument) {
        pdfView.displayMode = .singlePageContinuous
        pdfView.autoScales = true
        //pdfView.displayDirection = .horizontal
        pdfView.document = pdfDocument
    }

}
