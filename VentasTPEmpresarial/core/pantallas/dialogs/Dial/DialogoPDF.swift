//
//  DialogoInformacionTipsVentasVC.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 23/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import PDFKit

class DialogoPDF: BaseDialogCustom, BaseTreeLoadingDelegate {
    @IBOutlet weak var mViewContainerInformacion: UIView!
    @IBOutlet weak var viewRoot: UIView!
    @IBOutlet weak var pdfView: PDFView!
    @IBOutlet weak var labelTitulo: UILabel!
    @IBOutlet weak var imageViewDelete: UIImageView!
    var loadingBar: UIView?
    var urlPDF: String = ""
    var titulo: String = ""
    var pdfDocument: PDFDocument?

    init(urlPDF: String, titulo: String) {
        self.urlPDF = urlPDF
        self.titulo = titulo
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: viewRoot)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setUpView()
    }

    private func setUpView() {
        labelTitulo.text = titulo
        cargarPDFFromURL()
    }

    @IBAction func clickCerrar(_ sender: UIButton) {
        sender.animateBound(view: imageViewDelete) { [unowned self] _ in
            self.cerrar()
        }
    }
    private func cerrar() {
        super.cancel()
    }

    func cargarPDFFromURL() {
        guard let url = URL(string: urlPDF) else { return }
        if ConnectionUtils.isConnectedToNetwork() {
            loadingBar = showLoading(onView: viewRoot)
            if let pdfDocument = PDFDocument(url: url) {
                showBannerOK(title: "PDF descargado")
                self.pdfDocument = pdfDocument
                configPDF(pdfDocument: pdfDocument)
                if loadingBar != nil {
                    hideLoading(spinner: loadingBar!)
                }
            } else {
                showBannerError(title: "Ha ocurrido un error al descargar el pdf.")
                Logger.e("nose pudo crear el pdfdocument.")
                if loadingBar != nil {
                    hideLoading(spinner: loadingBar!)
                }
            }
        } else {
            Logger.e("No hay conexion a internet.")
            showBannerError(title: "No hay conexion a internet.")
        }
    }

    func configPDF(pdfDocument: PDFDocument) {
        pdfView.displayMode = .singlePageContinuous
        pdfView.autoScales = true
        //pdfView.displayDirection = .horizontal
        pdfView.document = pdfDocument
    }

    @IBAction func clickCompartir(_ sender: UIButton) {
        sender.animateBound(view: sender) { [unowned self] _ in
            self.compartirPDF()
        }
    }

    func compartirPDF() {
        guard let pdfDocument = pdfDocument else { return }
        guard let data = pdfDocument.dataRepresentation() else { return }
        let activityViewController = UIActivityViewController(activityItems: [data], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.postToFacebook]
        self.present(activityViewController, animated: true, completion: nil)
    }

}
