//
//  AgregaCitaAgendaVC.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 20/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class AgregaCitaAgendaVC: BaseDialogCustom {
    @IBOutlet weak var viewRoot: UIView!
    @IBOutlet weak var editTextFechaInicio: UITextField!
    @IBOutlet weak var editTextFechaFin: UITextField!
    @IBOutlet weak var editTextContacto: UITextField!
    @IBOutlet weak var editTextEmpresa: UITextField!
    @IBOutlet weak var editTextTelefono: UITextField!
    @IBOutlet weak var editTextCelular: UITextField!
    @IBOutlet weak var textViewComentarios: UITextView!
    @IBOutlet weak var imageViewDelete: UIImageView!
    
    fileprivate let presenter = AgregaCitaAgendaPresenter(service: AgregaCitaAgendaService())
    var loadingView: UIView = UIView()
    
    let validatorManager = ValidatorManager()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        delegate = presenter
        super.cerrarConclickFuera = false
        setUpView()
        super.setView(view: viewRoot)
    }
    
    private func setUpView() {
        addBorder(view: textViewComentarios)
        setUpValidator()
        setUPBeginEdit()
        setUPEndEdit()
        
//        BaseRegexValidator.validateRegex(
//            .textField(editTextContacto, .max(5))
//        )
    }
    
    private func setUpValidator(){
        editTextFechaInicio.delegate = validatorManager
        editTextFechaFin.delegate = validatorManager
        editTextContacto.delegate = validatorManager
        editTextEmpresa.delegate = validatorManager
        editTextTelefono.delegate = validatorManager
        textViewComentarios.delegate = validatorManager
        
        validatorManager.addValidator(
        
            FieldValidator.textField(
            editTextFechaInicio,
            .required,
            .all,
            "El campo no puede estar vacío."
        ),
        
            FieldValidator.textField(
            editTextFechaFin,
            .required,
            .all,
            "El campo no puede estar vacío."
        ),
            
            FieldValidator.textField(
            editTextContacto,
            .required,
            .nombre,
            "El campo no puede estar vacío"
        ),
        
            FieldValidator.textField(
            editTextEmpresa,
            .required,
            .nombre,
            "El campo no puede estar vacío."
        ),
            
            FieldValidator.textField(
                editTextTelefono, //textfield
                .exact(10), //validacion del texto mientras.
                .numero, // tipo de caracteres validos
                "El campo debe tener 10 digitos"
            ),
            
            FieldValidator.textField(
                editTextCelular, //TextField
                .none, //validación del texto mientras.
                .numero, //tipo de caracter validos.
                ""
            ),
            
            FieldValidator.textView(
            textViewComentarios,
            .none,
            .nombre,
            "El campo no puede estar vacío.")
            
        )
    }
    
    private func setUPBeginEdit(){
     validatorManager.addChangeEnterField(from:editTextContacto, to: editTextEmpresa)
        validatorManager.addChangeEnterField(from: editTextEmpresa, to: editTextTelefono)
        validatorManager.addChangeEnterField(from: editTextTelefono, to: editTextCelular)
       
    }
    
    private func setUPEndEdit(){
        validatorManager.addBeginEditingFor(editTextFechaInicio) { [weak self] (textField) -> (Void) in
            self?.presenter.editFechaInicio()
        }
        
        validatorManager.addBeginEditingFor(editTextFechaFin) { [weak self] (textField) -> (Void) in
            self?.presenter.editFechaFin()
            
        }
        
        validatorManager.addEndEditingFor(editTextContacto) { [weak self] (textField) -> (Void) in
            self?.presenter.updateContacto(item: textField.text ?? "")
            
        }
        
        validatorManager.addEndEditingFor(editTextEmpresa) { [weak self] (textField) -> (Void) in
            self?.presenter.updateEmpresa(item: textField.text ?? "")
            
        }
        
        validatorManager.addEndEditingFor(editTextTelefono) {
            [weak self] (textField) -> (Void) in
            self?.presenter.updateTelefono(item: textField.text ?? "")
        }
        
        validatorManager.addEndEditingFor(editTextCelular){
            [weak self] (textField) -> (Void) in
            self?.presenter.updateCelular(item: textField.text ?? "")
        }
        
        validatorManager.addEndEditingFor(textViewComentarios){
            [weak self] (textField) -> (Void) in
            self?.presenter.updateComentarios(item: textField.text ?? "")
        }
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func clickAceptar(_ sender: UIButton) {
        self.view.endEditing(true)
        sender.animateBound(view: sender){ [unowned self] _ in
            self.aceptarYValidar()
        }
    }
    
    private func aceptarYValidar(){
        if self.validatorManager.validate() {
            presenter.clickAceptar()
        }else{
            self.showAlert(titulo: "Aviso", mensaje: "Favor de revisar los campos")
        }
    }
    
    
    @IBAction func clickCerra(_ sender: UIButton) {
        sender.animateBound(view: imageViewDelete){[unowned self] _ in
            self.cerraDialogo()
        }
    }
    
    private func cerraDialogo(){
        super.cancel()
    }
    
}


extension AgregaCitaAgendaVC: AgregaCitaAgendaDelegate, BaseTreeLoadingDelegate{
    
    func aceptar(){
        super.result(value: "ok")
    }
    
    func updateData() {
        
    }
    
    func startLoading() {
        loadingView = showLoading(onView: viewRoot)
    }
    
    func finishLoading() {
        hideLoading(spinner: loadingView)
    }
    
    func mostrarDialogFecha(titulo: String, fechaDef: Date, closure: @escaping ((Date) -> ())){
        DatePickerDialog().show(titulo, limiteInf: 0, limiteSup: 1, defaultDate: fechaDef, datePickerMode: .dateAndTime){
            (date) -> Void in
            if let dt = date {
                closure(dt)
            }
        }
    }
    
    func updateTexfieldFechaInicio(_ texto: String){
        editTextFechaInicio.text = texto
    }
    
    func updateTexfieldFechaFin(_ texto: String){
        editTextFechaFin.text = texto
    }
    
    func mostrarMensaje(titulo: String, mensaje: String, _ closure: @escaping (() -> Void) ){
        showAlert(titulo: titulo, mensaje: mensaje, closure: closure)
    }
}


    

