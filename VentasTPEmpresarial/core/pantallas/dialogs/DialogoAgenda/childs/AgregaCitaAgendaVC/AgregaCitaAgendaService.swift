//
//  AgregaCitaAgendaService.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 20/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import RealmSwift

class AgregaCitaAgendaService: BaseDBManagerDelegate {
    var dbManager: DBManager
    var realm: Realm!
    var serverManager: ServerDataManager?
    let formatFechaServer = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    
    init() {
        dbManager = DBManager.getInstance()
        serverManager = ServerDataManager()
        realm = getRealm()
    }

    func insertarAgenda(_ datos: AgregaCitaAgendaModel, _ callBack: @escaping ((Bool, String)) -> Void) {

        let fechaInicioEnviar = format(datos.fechaInicio, format: formatFechaServer)
        let fechaFinEnviar = format(datos.fechaFin, format: formatFechaServer)
        let valoresTask = ValoresTask(id: "", name: datos.contacto, esTodoElDia: false, prospectoContacto: "", comentario: datos.comentarios, relacion: "", telefono: datos.telefono, nombreContacto: datos.contacto, nombreEmpresa: datos.empresa, celular: datos.celular)
        let idEmpleado = dbManager.getConfig().idEmpleado
        
//        let model = SFRequestAgenda(idEmpleado: idEmpleado, metodo: "AltaTask", fechaInicio: fechaInicioEnviar, fechaFin: fechaFinEnviar, valoresTask: valoresTask)
//        let url = Constantes.Url.SalesFor.agenda
//        let tipo: TipoRequest = .sf
//        let decodeClass = SFResponseAgenda.self
        
        let model = MiddleRequestAgenda(idEmpleado: idEmpleado, metodo: "AltaTask", fechaInicio: fechaInicioEnviar, fechaFin: fechaFinEnviar, valoresTask: valoresTask)
        let url = Constantes.Url.Middleware.Qa.agenda
        let tipo: TipoRequest = .middle
        let decodeClass = MiddleResponseAgenda.self
        
        serverManager?.getData(model: model, response: decodeClass, url: url, tipo) { response in
            guard let response = response else {
                callBack((false, ""))
                return
            }
//            callBack((response.result == "0", response.resultDescription ?? ""))
            callBack((response.response?.result == "0", response.response?.resultDescription ?? ""))
        }
    }

    func format(_ fecha: Date, format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        let dayInWeek = formatter.string(from: fecha)
        return dayInWeek
    }

    func incrementDate(_ date: Date, value: Int, byAdding: Calendar.Component) -> Date {
        return Calendar.current.date(byAdding: byAdding, value: value, to: date)!
    }
}
