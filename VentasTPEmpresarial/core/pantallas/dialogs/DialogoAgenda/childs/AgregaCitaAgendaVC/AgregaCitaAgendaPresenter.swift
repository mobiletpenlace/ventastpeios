//
//  AgregaCitaAgendaPresenter.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 20/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import Foundation
import SwiftEventBus

protocol AgregaCitaAgendaDelegate {
    func updateData()
    func startLoading()
    func finishLoading()
    func mostrarDialogFecha(titulo: String, fechaDef: Date, closure: @escaping (Date) -> Void)
    func updateTexfieldFechaInicio(_ texto: String)
    func updateTexfieldFechaFin(_ texto: String)
    func mostrarMensaje(titulo: String, mensaje: String, _ closure: @escaping (() -> Void))
    func aceptar()
}

class AgregaCitaAgendaPresenter {
    fileprivate var view: AgregaCitaAgendaDelegate?
    fileprivate var data: AgregaCitaAgendaModel!
    fileprivate let service: AgregaCitaAgendaService
    var fecha = Date()
    let formatoFecha = "yyyy-MM-dd  HH:mm"

    init(service: AgregaCitaAgendaService) {
        self.service = service
    }

    func attachView(view: AgregaCitaAgendaDelegate) {
        self.view = view
        data = AgregaCitaAgendaModel(
            fechaInicio: Date(),
            fechaFin: Date(),
            contacto: "",
            empresa: "",
            telefono: "",
            celular: "",
            comentarios: "",
            id: ""
        )
    }

    func viewDidAppear() {

    }

    func detachView() {
        view = nil
    }

    func getData() -> AgregaCitaAgendaModel {
        return data
    }

    func clickAceptar() {
        service.insertarAgenda(data) { [unowned self] (res, _) in
            if res {
                self.view?.mostrarMensaje(titulo: "Felicidades", mensaje: "Ha sido insertada la cita") {
                    self.view?.aceptar()
                    SwiftEventBus.post("DialogoAgenda-actualizar")
                }
            } else {
                self.view?.mostrarMensaje(titulo: "Atención", mensaje: "Ocurrió un error al insertar la cita") {

                }
            }
        }
    }
}

extension AgregaCitaAgendaPresenter: BaseFormatDate {

    func updateContacto(item: String) {
        self.data.contacto = item
    }

    func updateEmpresa(item: String) {
        self.data.empresa = item
    }

    func updateTelefono(item: String) {
        self.data.telefono = item
    }

    func updateCelular(item: String) {
        self.data.celular = item
    }

    func updateComentarios(item: String) {
        Logger.i("se va a actualizar los comentarios con: \(item)")
        self.data.comentarios = item
    }

    func editFechaInicio() {
        view?.mostrarDialogFecha(titulo: "Seleccione la fecha de inicio", fechaDef: Date()) { [unowned self] fecha in
            self.data.fechaInicio = fecha
            let fechaFormato = self.formatDate(fecha, format: self.formatoFecha)
            self.view?.updateTexfieldFechaInicio(fechaFormato)
        }
    }

    func editFechaFin() {
        let fechafin = incrementDate(data.fechaInicio, value: 1, byAdding: .hour)
        view?.mostrarDialogFecha(titulo: "Seleccione la fecha final", fechaDef: fechafin) { [unowned self] fecha in
            self.data.fechaFin = fecha
            let fechaFormato = self.formatDate(fecha, format: self.formatoFecha)
            self.view?.updateTexfieldFechaFin(fechaFormato)
        }
    }
}

extension AgregaCitaAgendaPresenter: CustomAlertViewDelegate {
    func clickOKButtonDialog(value: Any) {

    }

    func clickCancelButtonDialog() {

    }

    func viewDidShow() {

    }
}
