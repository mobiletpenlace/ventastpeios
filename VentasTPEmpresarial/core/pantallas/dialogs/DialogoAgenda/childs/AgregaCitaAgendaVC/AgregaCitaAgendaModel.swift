//
//  AgregaCitaAgendaModel.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 20/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct AgregaCitaAgendaModel {
    var fechaInicio: Date
    var fechaFin: Date
    var contacto: String
    var empresa: String
    var telefono: String
    var celular: String
    var comentarios: String
    var id: String
}
