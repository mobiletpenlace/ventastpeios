//
//  DetalleCitaAgendaModel.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 21/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct DetalleCitaAgendaModel {
    var fechaInicio: String
    var fechaFin: String
    var dia: String
    var contacto: String
    var empresa: String
    var telefono: String
    var celular: String
    var comentarios: String
    var id: String
}
