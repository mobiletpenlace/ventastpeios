//
//  DetalleCitaAgendaPresenter.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 21/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import Foundation
import SwiftEventBus

protocol DetalleCitaAgendaDelegate {
    func updateData()
    func startLoading()
    func finishLoading()
    func aceptar()
    func mostrarMensaje(titulo: String, mensaje: String, _ closure: @escaping (() -> Void))
}

class DetalleCitaAgendaPresenter {
    fileprivate var view: DetalleCitaAgendaDelegate?
    fileprivate var data: DetalleCitaAgendaModel?
    fileprivate let service: DetalleCitaAgendaService
    var fecha = Date()
    let formatoFechaHMS = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

    init(service: DetalleCitaAgendaService) {
        self.service = service
    }

    func attachView(view: DetalleCitaAgendaDelegate) {
        self.view = view
        SwiftEventBus.onMainThread(self, name: "DialogoDetalleAgenda-mostrarDetalle") { [weak self] result in if let modelo = result!.object as? DetalleCitaAgendaModel {
                self?.updateDetalle(modelo)
            }
        }
    }

    func updateDetalle(_ modelo: DetalleCitaAgendaModel) {
        data = modelo
        view?.updateData()
    }

    func viewDidAppear() {

    }

    func detachView() {
        view = nil
    }

    func getData() -> DetalleCitaAgendaModel? {
        return data
    }

    func clickEditar() {

    }

    func clickEliminar() {
        if let data = data {
            let id = data.id
            service.eliminarAgenda(id) { (result, _) in
                if result {
                    self.view?.mostrarMensaje(titulo: "Mensaje", mensaje: "La cita ha sido eliminada") {
                        self.view?.updateData()
                        self.view?.aceptar()
                        SwiftEventBus.post("DialogoAgenda-actualizar")
                    }
                } else {
                    self.view?.mostrarMensaje(titulo: "Atencion", mensaje: "Ha ocurrido un error al eliminar la cita") {

                    }
                }
            }
        } else {
            Logger.e("no se puede eliminar la agenda, porque no existe un id")
        }
    }

    func clickCerrar() {

    }
}

extension DetalleCitaAgendaPresenter: CustomAlertViewDelegate {
    func clickOKButtonDialog(value: Any) {

    }

    func clickCancelButtonDialog() {

    }

    func viewDidShow() {

    }
}
