//
//  DetalleCitaAgendaService.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 21/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import RealmSwift

class DetalleCitaAgendaService: BaseDBManagerDelegate {
    var realm: Realm!
    let serverManager: ServerDataManager?
    var dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()
        serverManager = ServerDataManager()
        realm = getRealm()
    }

    func eliminarAgenda(_ id: String, _ callBack: @escaping ((Bool, String)) -> Void) {
        let valoresTask = ValoresTask(id: id, name: "", esTodoElDia: false, prospectoContacto: "", comentario: "", relacion: "", telefono: "", nombreContacto: "", nombreEmpresa: "", celular: "")
        let idEmpleado = dbManager.getConfig().idEmpleado
//        let model = SFRequestAgenda(idEmpleado: idEmpleado, metodo: "BajaTask", fechaInicio: nil, fechaFin: nil, valoresTask: valoresTask)
//        let url = Constantes.Url.SalesFor.agenda
//        let tipo: TipoRequest = .sf
//        let decodeClass = SFResponseAgenda.self
        
        let model = MiddleRequestAgenda(idEmpleado: idEmpleado, metodo: "BajaTask", fechaInicio: nil, fechaFin: nil, valoresTask: valoresTask)
        let url = Constantes.Url.Middleware.Qa.agenda
        let tipo: TipoRequest = .middle
        let decodeClass = MiddleResponseAgenda.self
        
        serverManager?.getData(model: model, response: decodeClass, url: url, tipo) { response in
            guard let response = response else {
                callBack((false, ""))
                return
            }
            //            callBack((response.result == "0", response.resultDescription ?? ""))
            callBack((response.response?.result == "0", response.response?.resultDescription ?? ""))
        }
    }
}
