//
//  DetalleCitaAgendaVC.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 21/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class DetalleCitaAgendaVC: BaseDialogCustom {
    @IBOutlet weak var viewRoot: UIView!
    @IBOutlet weak var labelFecha: UILabel!
    @IBOutlet weak var labelHora: UILabel!
    @IBOutlet weak var labelContacto: UILabel!
    @IBOutlet weak var labelEmpresa: UILabel!
    @IBOutlet weak var labelTelefono: UILabel!
    @IBOutlet weak var labelCelular: UILabel!
    @IBOutlet weak var textViewComentarios: UITextView!
    @IBOutlet weak var imageViewEditar: UIImageView!
    @IBOutlet weak var imageViewEliminar: UIImageView!
    @IBOutlet weak var imageViewCerrar: UIImageView!

    fileprivate let presenter = DetalleCitaAgendaPresenter(service: DetalleCitaAgendaService())
    var loadingView: UIView = UIView()

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        delegate = presenter
        setUpView()
        super.setView(view: viewRoot)
    }

    private func setUpView() {
        super.addBorder(view: textViewComentarios)
    }

    @IBAction func clickAceptar(_ sender: UIButton) {
        super.result(value: "ok")
    }

    @IBAction func clickEditar(_ sender: UIButton) {
        sender.animateBound(view: imageViewEditar) { [unowned self] _ in
            self.presenter.clickEditar()
        }
    }

    @IBAction func clickEliminar(_ sender: UIButton) {
        sender.animateBound(view: imageViewEliminar) { [unowned self] _ in
            self.presenter.clickEliminar()
        }
    }

    @IBAction func clickCerrar(_ sender: UIButton) {
        sender.animateBound(view: imageViewCerrar) { [unowned self] _ in
            self.cerrar()
        }
    }

    private func cerrar() {
        super.cancel()
    }
}

extension DetalleCitaAgendaVC: DetalleCitaAgendaDelegate, BaseTreeLoadingDelegate, BaseTreeDialogs {

    func updateData() {
        if let data = presenter.getData() {
            labelFecha.text = data.dia
            labelHora.text = "\(data.fechaInicio) - \(data.fechaFin)"
            labelContacto.text = data.contacto
            labelEmpresa.text = data.empresa
            labelTelefono.text = data.telefono
            labelCelular.text = data.celular
            textViewComentarios.text = data.comentarios
        }
    }

    func startLoading() {
        loadingView = showLoading(onView: viewRoot)
    }

    func finishLoading() {
        hideLoading(spinner: loadingView)
    }

    func mostrarMensaje(titulo: String, mensaje: String, _ closure: @escaping (() -> Void)) {
        showAlert(titulo: titulo, mensaje: mensaje, closure: closure)
    }

    func aceptar() {
        super.result(value: "ok")
    }
}
