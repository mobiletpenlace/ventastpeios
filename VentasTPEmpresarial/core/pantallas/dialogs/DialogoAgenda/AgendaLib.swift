//
//  AgendaLib.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 16/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

class AgendaLib {
    let calendar = Calendar.current
    var table = [[DialogoAgendaModel]]()
    var headersDate = [Date]()
    var cellHeaderDate = [Date]()
    var headers = [String]()
    var cellHeader = [String]()

    let formatoDiaNombre = "EEEE dd"
    let formatoHora = "HH:mm"
    let formatoFechaHora = "dd-mm-yyyy-HH:mm"
    let formatter = DateFormatter(withFormat: "dd-mm-yyyy-HH:mm", locale: "es_MX")

    private func createTable(_ fecha: Date, ndias: Int, nHoras: Int) {
        headersDate = generateDate(n: ndias, from: fecha, byAdding: .day, increment: 1)
        headers = format(headersDate, format: formatoDiaNombre)
        headers.insert(" ", at: 0)

        cellHeaderDate = generateDate(n: nHoras, from: fecha, byAdding: .hour, increment: 1)
        cellHeader = format(cellHeaderDate, format: formatoHora)
        cellHeader.insert(" ", at: 0)

        table = createEmptyTable(ancho: headers.count, alto: cellHeader.count, tipo: DialogoAgendaModel.self) {
            createCellEmpty()
        }
        for (pos, header) in headers.enumerated() {
            table[0][pos] = createCell(header)
        }
        for (pos, cellHeader) in cellHeader.enumerated() {
            table[pos][0] = createCell(cellHeader)
        }
    }

    func createTableWeek(_ fecha: Date) -> [[DialogoAgendaModel]] {
        createTable(fecha, ndias: 5, nHoras: 15)
        return table
    }

    func createTableDay(_ fecha: Date) -> [[DialogoAgendaModel]] {
        createTable(fecha, ndias: 1, nHoras: 15)
        return table
    }

    func createEmptyTable<T>(ancho: Int, alto: Int, tipo: T.Type, _ closure: (() -> T)) -> [[T]] {
        var tabla = [[T]]()
        for _ in 0..<alto {
            var fila = [T]()
            for _ in 0..<ancho {
                fila.append(closure())
            }
            tabla.append(fila)
        }
        return tabla
    }

    func add(fecha: Date, id: String, nombreContacto: String, nombreEmpresa: String, asunto: String, comentario: String, celular: String, telefono: String) {
        let fechaIncrementada = calendar.date(byAdding: .minute, value: 59, to: fecha)!
        addCellDate(
            celda: createCell(
                titulo: "",
                horaInicio: format(fecha, format: formatoHora),
                horaFin: format(fechaIncrementada, format: formatoHora),
                id: id,
                nombreContacto: nombreContacto,
                nombreEmpresa: nombreEmpresa,
                asunto: asunto,
                comentario: comentario,
                celular: celular,
                telefono: telefono,
                mostrar: true
            ),
            fecha
        )
    }

    func add(_ fecha: String, id: String, nombreContacto: String, nombreEmpresa: String, asunto: String, comentario: String, celular: String, telefono: String) {
        let testFecha = createDateFromString(fecha)
        add(fecha: testFecha, id: id, nombreContacto: nombreContacto, nombreEmpresa: nombreEmpresa, asunto: asunto, comentario: comentario, celular: celular, telefono: telefono)
    }

    func addCellDate(celda: DialogoAgendaModel, _ fecha: Date) {
        let fechaMinima = fecha
        let fechaMaxima = calendar.date(byAdding: .day, value: 5, to: fechaMinima)!

        var posY = -1
        if fecha.isBetween(fechaMinima, and: fechaMaxima) {
            for (i, fechaH) in headersDate.enumerated() {
                if fechaH.day == fecha.day {
                    posY = i
                    break
                }
            }
        }

        var posX = -1
        for (i, hora) in cellHeaderDate.enumerated() {
            let horaMaxima = calendar.date(byAdding: .minute, value: 59, to: hora)!
            if fecha.hour >= hora.hour && fecha.hour <= horaMaxima.hour {
                posX = i
                break
            }
        }

        if posX >= 0 && posY >= 0 {
            addCell(i: posX, j: posY, celda: celda)
        }
        //Logger.println("x:\(posX) y:\(posY)")
    }

    func diference(_ fecha1: Date, _ fecha2: Date, _ tipo: Calendar.Component) -> DateComponents {
        let components = Calendar.current.dateComponents(
            [tipo],
            from: fecha1,
            to: fecha2
        )
        return components
    }

    func incrementDate(_ date: Date, value: Int, byAdding: Calendar.Component) -> Date {
        return calendar.date(byAdding: byAdding, value: value, to: date)!
    }

    func format(_ fecha: Date, format: String) -> String {
        formatter.dateFormat = format
        let dayInWeek = formatter.string(from: fecha)
        return dayInWeek
    }

    func format(_ fechas: [Date], format: String) -> [String] {
        formatter.dateFormat = format
        var lista = [String]()
        for fecha in fechas {
            lista.append(formatter.string(from: fecha))
        }
        return lista
    }

    /*"01-01-2017-08:00"*/
    func createDateFromString(_ fecha: String) -> Date {
        formatter.dateFormat = formatoFechaHora
        guard let date = formatter.date(from: fecha) else {
            fatalError()
        }
        return date
    }

    func createDate(_ fecha: String, format: String) -> Date {
        formatter.dateFormat = format
        guard let date = formatter.date(from: fecha) else {
            fatalError()
        }
        return date
    }

    func generateDate(n: Int, from fecha: Date, byAdding: Calendar.Component, increment: Int, format: String) -> [String] {
        let calendar = Calendar.current
        formatter.dateFormat = format
        var lista = [String]()
        for i in 0..<n {
            let futuraHora = calendar.date(byAdding: byAdding, value: i * increment, to: fecha)!
            let horaString = formatter.string(from: futuraHora)
            lista.append(horaString)
        }
        return lista
    }

    func generateDate(n: Int, from fecha: Date, byAdding: Calendar.Component, increment: Int) -> [Date] {
        let calendar = Calendar.current
        var lista = [Date]()
        for i in 0..<n {
            let futuraHora = calendar.date(byAdding: byAdding, value: i * increment, to: fecha)!
            lista.append(futuraHora)
        }
        return lista
    }

    func addCell(i: Int, j: Int, celda: DialogoAgendaModel) {
        let posX = j + 1
        let posY = i + 1
        table[posY][posX] = celda
    }

    func createCellTable(_ titulo: String, _ horaInicio: String, _ horaFin: String, _ nombreContacto: String, _ nombreEmpresa: String, _ comentario: String, _ id: String, _ celular: String, _ telefono: String, _ asunto: String, _ mostrar: Bool) -> DialogoAgendaModel {
        return DialogoAgendaModel(
            titulo: titulo,
            mostrar: mostrar,
            fechaInicio: horaInicio,
            fechaFin: horaFin,
            nombreContacto: nombreContacto,
            nombreEmpresa: nombreEmpresa,
            asunto: asunto,
            comentario: comentario,
            id: id,
            celular: celular,
            telefono: telefono
        )
    }

    func createCell(titulo: String, horaInicio: String, horaFin: String, id: String, nombreContacto: String, nombreEmpresa: String, asunto: String, comentario: String, celular: String, telefono: String, mostrar: Bool) -> DialogoAgendaModel {
        return createCellTable(titulo, horaInicio, horaFin, nombreContacto, nombreEmpresa, comentario, id, celular, telefono, asunto, mostrar)
    }

    func createCellEmpty() -> DialogoAgendaModel {
        return createCell(titulo: "", horaInicio: "", horaFin: "", id: "", nombreContacto: "", nombreEmpresa: "", asunto: "", comentario: "", celular: "", telefono: "", mostrar: false)
    }

    func createCell(_ titulo: String) -> DialogoAgendaModel {
        return createCell(titulo: titulo, horaInicio: "", horaFin: "", id: "", nombreContacto: "", nombreEmpresa: "", asunto: "", comentario: "", celular: "", telefono: "", mostrar: true)
    }
}
