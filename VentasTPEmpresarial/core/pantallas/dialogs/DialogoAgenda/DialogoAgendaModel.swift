//
//  DialogoAgendaModel.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 13/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct DialogoAgendaModel {
    var titulo: String
    var mostrar: Bool
    var fechaInicio: String
    var fechaFin: String
    var nombreContacto: String
    var nombreEmpresa: String
    var asunto: String
    var comentario: String
    var id: String
    var celular: String
    var telefono: String
}
