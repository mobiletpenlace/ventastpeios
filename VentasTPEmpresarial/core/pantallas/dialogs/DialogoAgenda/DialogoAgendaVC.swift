//
//  DialogoAgendaVC.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 13/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class DialogoAgendaVC: BaseDialogCustom {
    @IBOutlet weak var viewRoot: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var labelFecha: UILabel!
    @IBOutlet weak var segmentedControlTipoAgenda: UISegmentedControl!
    @IBOutlet weak var viewAgregarCitaBoton: UIView!
    @IBOutlet weak var viewFecha: UIView!
    fileprivate let presenter = DialogoAgendaPresenter(service: DialogoAgendaService())
    let nombreCelda = "HoraCitaCollectionViewCell"
    let nombreCeldaDetalle = "DiaCitaCollectionViewCell"
    var loadingView: UIView = UIView()
    let heightHeader = CGFloat(30)
    let widthHeader = CGFloat(50)
    let nWidth = 6
    let nHeight = 6
    var anchoCelda = CGFloat(200)
    var altoCelda = CGFloat(100)
    var dateAnterior = Date()

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        delegate = presenter
        setUpView()
        super.setView(view: viewRoot)
    }

    @IBAction func clickAgregarCita(_ sender: UIButton) {
        sender.animateBound(view: viewAgregarCitaBoton) { _ in
            self.presenter.clickAgregarCita()
        }
    }

    @IBAction func clickCalendario(_ sender: UIButton) {
        DatePickerDialog().show("Seleccione Fecha", limiteInf: -3, limiteSup: 1, defaultDate: dateAnterior) { (date) -> Void in
            if let dt = date {
                self.dateAnterior = dt
                self.presenter.updateFecha(date: dt)
            }
        }
    }

    private func setUpView() {
        addBorder(view: viewFecha)
        addRoundCorner(view: viewFecha, value: 5)
        inicializarCollectionView()

        collectionView.delaysContentTouches = true
        anchoCelda = (collectionView.frame.size.width / CGFloat(nWidth))
        altoCelda = (collectionView.frame.size.height / CGFloat(nHeight))
    }

    private func inicializarCollectionView() {
        let nib = UINib.init(nibName: nombreCelda, bundle: nil)
        self.collectionView.register(nib, forCellWithReuseIdentifier: nombreCelda)

        let nib2 = UINib.init(nibName: nombreCeldaDetalle, bundle: nil)
        self.collectionView.register(nib2, forCellWithReuseIdentifier: nombreCeldaDetalle)

        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }

}

extension DialogoAgendaVC: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let celda = collectionView.cellForItem(at: indexPath) as? HoraCitaCollectionViewCell {
            if celda.activa {
                celda.animateBound(view: celda) { [weak self] _ in
                    self?.presenter.clickCell(indexPath: indexPath)
                }
            }
        }
    }
}

extension DialogoAgendaVC: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return presenter.getSize()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let seccion = indexPath.section
        let celda = indexPath.row

        if seccion == 0 || celda == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: nombreCeldaDetalle, for: indexPath) as! DiaCitaCollectionViewCell
            if let data = presenter.getItemIndex(index: indexPath.row, section: indexPath.section) {
                cell.labelDiaCita.text = data.titulo
            }
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: nombreCelda, for: indexPath) as! HoraCitaCollectionViewCell

            if let data = presenter.getItemIndex(index: indexPath.row, section: indexPath.section) {
                cell.labelAsunto.text = data.nombreEmpresa
                cell.labelHoraInicio.text = data.fechaInicio
                cell.labelHoraFin.text = data.fechaFin
                cell.labelNombre.text = data.nombreContacto
                cell.ocultar(!data.mostrar)
            }
            return cell
        }
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.getSizeRows(section)
    }

}

extension DialogoAgendaVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let seccion = indexPath.section
        let celda = indexPath.row
        if seccion == 0 && celda == 0 {
            return CGSize(width: widthHeader, height: heightHeader)
        } else if seccion == 0 {
            return CGSize(width: anchoCelda, height: heightHeader)
        } else if celda == 0 {
            return CGSize(width: widthHeader, height: altoCelda)
        } else {
            return CGSize(width: anchoCelda, height: altoCelda)
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

}

extension DialogoAgendaVC: DialogoAgendaDelegate, BaseTreeLoadingDelegate {
    func updateData() {
        self.collectionView.reloadData()
    }

    func startLoading() {
        loadingView = showLoading(onView: collectionView)
    }

    func finishLoading() {
        hideLoading(spinner: loadingView)
    }

    func updateSemanaLabel(_ fechaIni: String, _ fechaFin: String, _ year: String) {
        self.labelFecha.text = "Semana " + fechaIni + " al " + fechaFin + " del " + year
    }

    func mostrarDialogoNuevaCita() {
        showDialog(customAlert: AgregaCitaAgendaVC())
    }

    func mostrarDialogoDetalleAgenda() {
        showDialog(customAlert: DetalleCitaAgendaVC())
    }
}
