//
//  DialogoAgendaPresenter.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 13/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol DialogoAgendaDelegate {
    func updateData()
    func startLoading()
    func finishLoading()
    func updateSemanaLabel(_ fechaIni: String, _ fechaFin: String, _ year: String)
    func mostrarDialogoNuevaCita()
    func mostrarDialogoDetalleAgenda()
}

class DialogoAgendaPresenter: AgendaLib {
    fileprivate var view: DialogoAgendaDelegate?
    fileprivate var dataList = [[DialogoAgendaModel]]()
    fileprivate let service: DialogoAgendaService
    var fecha = Date()
    let formatoFechaServer = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

    init(service: DialogoAgendaService) {
        self.service = service
        super.init()

        fecha.hour = 6
        fecha.minute = 0
        actualizarDatos()
    }

    func attachView(view: DialogoAgendaDelegate) {
        self.view = view
        SwiftEventBus.onMainThread(self, name: "DialogoAgenda-actualizar") { [weak self] _ in
            self?.actualizarDatos()
        }
    }

    func viewDidAppear() {

    }

    func detachView() {
        view = nil
    }

    func getItemIndex(index: Int, section: Int) -> DialogoAgendaModel? {
        if getSize() == 0 {
            return nil
        }
        return dataList[section][index]
    }

    func getSize() -> Int {
        return dataList.count
    }

    func getSizeRows(_ section: Int) -> Int {
        guard dataList.count > 0 else {
            return 0
        }
        return dataList[section].count
    }

    func updateFecha(date: Date) {
        fecha = date
        fecha.hour = 6
        fecha.minute = 0
        actualizarDatos()
    }

    func clickAgregarCita() {
        view?.mostrarDialogoNuevaCita()
    }

    func clickCell(indexPath: IndexPath) {
        let celda = dataList[indexPath.section][indexPath.row]
        let data = DetalleCitaAgendaModel(
            fechaInicio: celda.fechaInicio,
            fechaFin: celda.fechaFin,
            dia: table[0][indexPath.row].titulo,
            contacto: celda.nombreContacto,
            empresa: celda.nombreEmpresa,
            telefono: celda.telefono,
            celular: celda.celular,
            comentarios: celda.comentario,
            id: celda.id
        )
        view?.mostrarDialogoDetalleAgenda()
        SwiftEventBus.post("DialogoDetalleAgenda-mostrarDetalle", sender: data)
    }

    func actualizarDatos() {
        view?.startLoading()
        service.consultarAgenda(fecha) { [weak self] data in
            self?.view?.finishLoading()
            self?.onFinishGetData(data: data)
        }
    }

    func onFinishGetData(data datos: [DialogoAgendaModel]) {
        dataList = [[DialogoAgendaModel]]()
        _ = createTableWeek(fecha)
        for dato in datos {
            let listaFechas = generarListaFechaHora(
                fechaIni: createDate(dato.fechaInicio, format: formatoFechaServer),
                fechaFin: createDate(dato.fechaFin, format: formatoFechaServer)
            )
            for fecha in listaFechas {
                add(fecha: fecha, id: dato.id, nombreContacto: dato.nombreContacto, nombreEmpresa: dato.nombreEmpresa, asunto: dato.asunto, comentario: dato.comentario, celular: dato.celular, telefono: dato.telefono)
            }
        }
        for section in table {
            var listaAgendaModel = [DialogoAgendaModel]()
            for row in section {
                listaAgendaModel.append(
                    DialogoAgendaModel(
                        titulo: row.titulo,
                        mostrar: row.mostrar,
                        fechaInicio: row.fechaInicio,
                        fechaFin: row.fechaFin,
                        nombreContacto: row.nombreContacto,
                        nombreEmpresa: row.nombreEmpresa,
                        asunto: row.asunto,
                        comentario: row.comentario,
                        id: row.id,
                        celular: row.celular,
                        telefono: row.telefono
                    )
                )
            }
            dataList.append(listaAgendaModel)
        }
        view?.updateData()
        let fechaIni = format(fecha, format: "dd 'de' LLLL")
        let fechaFuturo = incrementDate(fecha, value: 5, byAdding: .day)
        let fechaFin = format(fechaFuturo, format: "dd 'de' LLLL")

        let year = format(fecha, format: "yyyy")
        view?.updateSemanaLabel(fechaIni, fechaFin, year)
    }

    func generarListaFechaHora(fechaIni: Date, fechaFin: Date) -> [Date] {
        let diferencia = diference(fechaIni, fechaFin, .hour).hour ?? 0
        var listaFechas = [Date]()
        listaFechas.append(fechaIni)
        if diferencia > 1 {
            for value in 1..<diferencia {
                let fechaNew = incrementDate(fechaIni, value: value, byAdding: .hour)
                listaFechas.append(fechaNew)
            }
        }
        return listaFechas
    }
}

extension DialogoAgendaPresenter: CustomAlertViewDelegate {
    func clickOKButtonDialog(value: Any) {

    }

    func clickCancelButtonDialog() {

    }

    func viewDidShow() {

    }
}
