//
//  DialogoAgendaService.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 13/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import RealmSwift

class DialogoAgendaService: BaseDBManagerDelegate {
    var serverManager: ServerDataManager?
    var realm: Realm!
    let formatoFechaServer = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    var dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()
        realm = getRealm()
        serverManager = ServerDataManager()
    }

    func consultarAgenda(_ fecha: Date, _ callBack: @escaping ([DialogoAgendaModel]) -> Void) {
        let fechaInicioEnviar = format(fecha, format: formatoFechaServer)
        let fechaFin = incrementDate(fecha, value: 7, byAdding: .day)
        let fechaFinEnviar = format(fechaFin, format: formatoFechaServer)

        var lista = [DialogoAgendaModel]()
        let valoresTask = ValoresTask(id: nil, name: nil, esTodoElDia: nil, prospectoContacto: nil, comentario: nil, relacion: nil, telefono: nil, nombreContacto: nil, nombreEmpresa: nil, celular: nil)
        let idEmpleado = dbManager.getConfig().idEmpleado
        
//        let model = SFRequestAgenda(idEmpleado: idEmpleado, metodo: "ConsultaTask", fechaInicio: fechaInicioEnviar, fechaFin: fechaFinEnviar, valoresTask: valoresTask)
//        let url = Constantes.Url.SalesFor.agenda
//        let tipo: TipoRequest = .sf
//        let decodeClass = SFResponseAgenda.self
        
        let model = MiddleRequestAgenda(idEmpleado: idEmpleado, metodo: "ConsultaTask", fechaInicio: fechaInicioEnviar, fechaFin: fechaFinEnviar, valoresTask: valoresTask)
        let url = Constantes.Url.Middleware.Qa.agenda
        let tipo: TipoRequest = .middle
        let decodeClass = MiddleResponseAgenda.self
        
        serverManager?.getData(model: model, response: decodeClass, url: url, tipo) { response in
            guard let response = response else {
                callBack(lista)
                return
            }
            if let items = response.listaValueTask {
                for item in items {
                    lista.append(
                        DialogoAgendaModel(titulo: "", mostrar: true, fechaInicio: item.fechaInicio!, fechaFin: item.fechaFin!, nombreContacto: item.nombreContacto ?? "", nombreEmpresa: item.nombreEmpresa ?? "", asunto: item.subject ?? "", comentario: item.comentario ?? "", id: item.id ?? "", celular: item.celular ?? "", telefono: item.telefono ?? "")
                    )
                }
            }
            callBack(lista)
        }
    }

    func insertarAgenda(_ fechaInicio: Date, _ fechaFin: Date, nombre: String, _ callBack: @escaping ((Bool, String)) -> Void) {
        let fechaInicioEnviar = format(fechaInicio, format: formatoFechaServer)
        let fechaFinEnviar = format(fechaFin, format: formatoFechaServer)

        let valoresTask = ValoresTask(id: nil, name: nil, esTodoElDia: nil, prospectoContacto: nil, comentario: nil, relacion: nil, telefono: nil, nombreContacto: nil, nombreEmpresa: nil, celular: nil)
        let idEmpleado = dbManager.getConfig().idEmpleado
//        let model = SFRequestAgenda(idEmpleado: idEmpleado, metodo: "AltaTask", fechaInicio: fechaInicioEnviar, fechaFin: fechaFinEnviar, valoresTask: valoresTask)
//        let url = Constantes.Url.SalesFor.agenda
//        let tipo: TipoRequest = .sf
//        let decodeClass = SFResponseAgenda.self
        
        let model = MiddleRequestAgenda(idEmpleado: idEmpleado, metodo: "AltaTask", fechaInicio: fechaInicioEnviar, fechaFin: fechaFinEnviar, valoresTask: valoresTask)
        let url = Constantes.Url.Middleware.Qa.agenda
        let tipo: TipoRequest = .middle
        let decodeClass = MiddleResponseAgenda.self
        
        serverManager?.getData(model: model, response: decodeClass, url: url, tipo) { response in
            guard let response = response else {
                callBack((false, ""))
                return
            }
//            callBack((response.result == "0", response.resultDescription ?? ""))
            callBack((response.response?.result == "0", response.response?.resultDescription ?? ""))
        }
    }

    func format(_ fecha: Date, format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        let dayInWeek = formatter.string(from: fecha)
        return dayInWeek
    }

    func incrementDate(_ date: Date, value: Int, byAdding: Calendar.Component) -> Date {
        return Calendar.current.date(byAdding: byAdding, value: value, to: date)!
    }
}
