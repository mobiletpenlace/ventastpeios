//
//  DialogAclaracionExitosa.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 31/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class DialogAclaracionExitosa: BaseDialogCustom {
    @IBOutlet weak var viewRoot: UIView!
    @IBOutlet weak var labelFolio: UILabel!
    var folio: String?

    init(folio: String) {
        self.folio = folio
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: viewRoot)
        reloadData()
    }

    func reloadData() {
        if labelFolio != nil {
            labelFolio.text = folio
        }
    }

    @IBAction func clickAceptar(_ sender: UIButton) {
        super.result(value: "bien")
    }

}
