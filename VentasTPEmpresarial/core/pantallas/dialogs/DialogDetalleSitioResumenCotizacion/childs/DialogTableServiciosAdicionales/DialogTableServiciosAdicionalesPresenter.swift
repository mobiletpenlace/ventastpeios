//
//  DialodTablePresenter.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 26/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol DialogTableServiciosAdicionalesDelegate {
    func updateData()
}

class DialogTableServiciosAdicionalesPresenter {
    fileprivate var view: DialogTableServiciosAdicionalesDelegate?
    fileprivate var dataList = [DialogDetalleSitioServicioAdicionalModel]()

    func attachView(view: DialogTableServiciosAdicionalesDelegate) {
        self.view = view
        SwiftEventBus.onMainThread(self, name: "DialogDetalleSitioTableServicios(setDatalist)") { [weak self] result in
            if let data = result!.object as? [DialogDetalleSitioServicioAdicionalModel] {
                self?.dataList = data
                self?.actualizarDatos()
            }
        }
    }

    func viewDidAppear() {

    }

    func detachView() {
        view = nil
    }

    func getItemIndex(index: Int) -> DialogDetalleSitioServicioAdicionalModel? {
        if getSize() == 0 {
            return nil
        }
        return dataList[index]
    }

    func getSize() -> Int {
        return dataList.count
    }

    func actualizarDatos() {
        view?.updateData()
    }
}
