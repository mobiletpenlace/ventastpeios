//
//  DialogTableServiciosAdicionalesVC.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 26/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class DialogTableServiciosAdicionalesVC: BaseTreeVC {
    @IBOutlet weak var tableview1: UITableView!
    fileprivate let presenter = DialogTableServiciosAdicionalesPresenter()
    let nombreCelda1 = "DialogEditaPlanesCell"

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUpView()
    }

    private func setUpView() {
        cargarTableView()

    }

    func cargarTableView() {
        let nib = UINib.init(nibName: nombreCelda1, bundle: nil)
        self.tableview1.register(nib, forCellReuseIdentifier: nombreCelda1)
        self.tableview1.delegate = self
        self.tableview1.dataSource = self
    }

}

extension DialogTableServiciosAdicionalesVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: nombreCelda1, for: indexPath) as! DialogEditaPlanesCell
        if let data = presenter.getItemIndex(index: indexPath.row) {
            cell.texto.text = data.nombre + " \(data.cantidad) \(data.total)"
        }
        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getSize()
    }
}

extension DialogTableServiciosAdicionalesVC: UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
        //return UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
}

extension DialogTableServiciosAdicionalesVC: DialogTableServiciosAdicionalesDelegate, BaseFormatNumber {

    func updateData() {
        tableview1.reloadData()
    }

}
