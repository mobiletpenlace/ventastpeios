//
//  DialogDetalleSitioServicioAdicionalModel.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 26/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct DialogDetalleSitioServicioAdicionalModel {
    let nombre: String
    let cantidad: Int
    let total: Float
}
