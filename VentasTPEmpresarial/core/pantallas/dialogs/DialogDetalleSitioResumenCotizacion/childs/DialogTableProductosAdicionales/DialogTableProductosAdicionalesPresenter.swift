//
//  DialogTableProductosAdicionalesPresenter.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 26/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol DialogTableProductosAdicionalesDelegate {
    func updateData()
}

class DialogTableProductosAdicionalesPresenter {
    fileprivate var view: DialogTableProductosAdicionalesDelegate?
    fileprivate var dataList = [DialogDetalleSitioProductoAdicionalModel]()

    func attachView(view: DialogTableProductosAdicionalesDelegate) {
        self.view = view
        SwiftEventBus.onMainThread(self, name: "DialogDetalleSitioTableProductos(setDatalist)") { [weak self] result in
            if let data = result!.object as? [DialogDetalleSitioProductoAdicionalModel] {
                self?.dataList = data
                self?.actualizarDatos()
            }
        }
    }

    func viewDidAppear() {

    }

    func detachView() {
        view = nil
    }

    func getItemIndex(index: Int) -> DialogDetalleSitioProductoAdicionalModel? {
        if getSize() == 0 {
            return nil
        }
        return dataList[index]
    }

    func getSize() -> Int {
        return dataList.count
    }

    func actualizarDatos() {
        view?.updateData()
    }
}
