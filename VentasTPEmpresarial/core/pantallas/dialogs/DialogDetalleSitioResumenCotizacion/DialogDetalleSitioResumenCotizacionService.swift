//
//  DialogDetalle.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 22/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

class DialogDetalleSitioResumenCotizacionService: BaseDBManagerDelegate {
    var serverManager: ServerDataManager?
    var dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()
    }

    func getPlanesDeSitioFromDB(_ idSitio: Int, _ callBack: @escaping ([DialogDetalleSitioResumenCotizacionModel]) -> Void) {
        var data = [DialogDetalleSitioResumenCotizacionModel]()//crear lista de datos
        if let sitio = dbManager.buscarSitioPorID(id: idSitio) {
            for plan in sitio.planesConfig {
                if let resumenCotizacion = plan.resumenCotizacion {
                    var listaServAdic = [DialogDetalleSitioServicioAdicionalModel]()
                    for servAdic in plan.serviciosAdicionales {
                        let cantidad = servAdic.cantidad
                        if cantidad > 0 {
                            let total = servAdic.servicioAdicional.precio * Float(cantidad)
                            listaServAdic.append(
                                DialogDetalleSitioServicioAdicionalModel(
                                    nombre: servAdic.servicioAdicional.nombre,
                                    cantidad: cantidad,
                                    total: total
                                )
                            )
                        }
                    }
                    var listaProdAdic = [DialogDetalleSitioProductoAdicionalModel]()
                    for prodAdic in plan.productosAdicionales {
                        for addon in prodAdic.addons {
                            let cantidad = addon.cantidad
                            if cantidad > 0 {
                                let total = addon.addon.precio * Float(cantidad)
                                listaProdAdic.append(
                                    DialogDetalleSitioProductoAdicionalModel(
                                        nombre: addon.addon.nombre,
                                        cantidad: cantidad,
                                        total: total
                                    )
                                )
                            }
                        }
                    }
                    data.append(
                        DialogDetalleSitioResumenCotizacionModel(
                            id: plan.plan.id!,
                            nombre: plan.plan.nombre!,
                            costoRentaMensual: resumenCotizacion.costoRentaMensual,
                            costoInstalacion: resumenCotizacion.costoInstalacion,
                            costoAdicionales: resumenCotizacion.costoAdicionales,
                            descuento: resumenCotizacion.descuento,
                            subTotal: resumenCotizacion.subTotal,
                            impuestos: resumenCotizacion.impuestos,
                            cargosUnicos: resumenCotizacion.cargosUnicos,
                            total: resumenCotizacion.total,
                            productosAdicionales: listaProdAdic,
                            serviciosAdicionales: listaServAdic
                        )
                    )
                }
            }
        }
        callBack(data)
    }
}
