//
//  DialogDetalleSitioResumenCotizacionModel.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 22/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct DialogDetalleSitioResumenCotizacionModel {
    let id: String
    let nombre: String
    let costoRentaMensual: Float
    let costoInstalacion: Float
    let costoAdicionales: Float
    let descuento: Float
    let subTotal: Float
    let impuestos: Float
    let cargosUnicos: Float
    let total: Float
    let productosAdicionales: [DialogDetalleSitioProductoAdicionalModel]
    let serviciosAdicionales: [DialogDetalleSitioServicioAdicionalModel]
}
