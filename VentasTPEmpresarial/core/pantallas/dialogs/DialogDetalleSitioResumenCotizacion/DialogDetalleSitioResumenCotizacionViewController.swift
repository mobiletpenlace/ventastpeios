//
//  DialogDetalleSitioResumenCotizacionViewController.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 22/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class DialogDetalleSitioResumenCotizacionViewController: BaseDialogCustom {
    @IBOutlet weak var labelRentaMensual: UILabel!
    @IBOutlet weak var labelCostoInstalacion: UILabel!
    @IBOutlet weak var labelAdicionales: UILabel!
    @IBOutlet weak var labelDescuento: UILabel!
    @IBOutlet weak var labelSubtotal: UILabel!
    @IBOutlet weak var labelImpuestos: UILabel!
    @IBOutlet weak var labelTotalCargosUnicos: UILabel!
    @IBOutlet weak var labelTotal: UILabel!
    @IBOutlet weak var viewRoot: UIView!
    @IBOutlet weak var tableview1: UITableView!
    @IBOutlet weak var viewServiciosAdicionales: UIView!
    @IBOutlet weak var viewProductosAdicionales: UIView!
    fileprivate let presenter = DialogDetalleSitioResumenCotizacionPresenter(service: DialogDetalleSitioResumenCotizacionService())
    let nombreCelda1 = "DialogEditaPlanesCell"

//    init(idSitio: Int){
//        super.init(nibName: nil, bundle: nil)
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        delegate = presenter
        setUpView()
        super.setView(view: viewRoot)
    }

    @IBAction func clickAceptar(_ sender: UIButton) {
        sender.animateBound(view: sender) { _ in
            super.result(value: "bien")
        }
    }

    private func setUpView() {
        cargarTableView()
        addView(view: viewServiciosAdicionales, vc: DialogTableServiciosAdicionalesVC())
        addView(view: viewProductosAdicionales, vc: DialogTableProductosAdicionalesVC())
    }

    private func addView(view: UIView, vc: BaseTreeVC) {
        addChildViewController(vc)
        view.addSubview(vc.view)
        vc.view.frame = view.bounds
        vc.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        vc.didMove(toParentViewController: self)
    }

    func cargarTableView() {
        let nib = UINib.init(nibName: nombreCelda1, bundle: nil)
        self.tableview1.register(nib, forCellReuseIdentifier: nombreCelda1)
        self.tableview1.delegate = self
        self.tableview1.dataSource = self
    }

}

extension DialogDetalleSitioResumenCotizacionViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: nombreCelda1, for: indexPath) as! DialogEditaPlanesCell
        if let data = presenter.getItemIndex(index: indexPath.row) {
            cell.texto.text = data.nombre
        }
        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getSize()
    }
}

extension DialogDetalleSitioResumenCotizacionViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
        //return UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.clickPlan(indexPath.row)
    }
}

extension DialogDetalleSitioResumenCotizacionViewController: DialogDetalleSitioResumenCotizacionDelegate, BaseFormatNumber {

    func updateDetalle(index: Int) {
        if let data = presenter.getItemIndex(index: index) {
            labelRentaMensual.text = "$" + numeroAFormatoPrecio(numero: data.costoRentaMensual)
            labelCostoInstalacion.text = "$" + numeroAFormatoPrecio(numero: data.costoInstalacion)
            labelAdicionales.text = "$" + numeroAFormatoPrecio(numero: data.costoAdicionales)
            labelDescuento.text = "$" + numeroAFormatoPrecio(numero: data.descuento)
            labelSubtotal.text = "$" + numeroAFormatoPrecio(numero: data.subTotal)
            labelImpuestos.text = "$" + numeroAFormatoPrecio(numero: data.impuestos)
            labelTotalCargosUnicos.text = "$" + numeroAFormatoPrecio(numero: data.cargosUnicos)
            labelTotal.text = "$" + numeroAFormatoPrecio(numero: data.total)
        } else {
            labelRentaMensual.text = "$" + numeroAFormatoPrecio(numero: 0)
            labelCostoInstalacion.text = "$" + numeroAFormatoPrecio(numero: 0)
            labelAdicionales.text = "$" + numeroAFormatoPrecio(numero: 0)
            labelDescuento.text = "$" + numeroAFormatoPrecio(numero: 0)
            labelSubtotal.text = "$" + numeroAFormatoPrecio(numero: 0)
            labelImpuestos.text = "$" + numeroAFormatoPrecio(numero: 0)
            labelTotalCargosUnicos.text = "$" + numeroAFormatoPrecio(numero: 0)
            labelTotal.text = "$" + numeroAFormatoPrecio(numero: 0)
        }
    }

    func updateData() {
        tableview1.reloadData()
    }

    func seleccionarPlanCelda(index: Int) {
        if presenter.getSize() > 0 {
            let indexPath = IndexPath(row: index, section: 0)
            tableview1.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        }
    }
}
