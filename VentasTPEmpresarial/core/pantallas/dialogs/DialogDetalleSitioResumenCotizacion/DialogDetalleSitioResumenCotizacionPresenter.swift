//
//  DialoDetalleSitioPresenter.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 22/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol DialogDetalleSitioResumenCotizacionDelegate {
    func updateDetalle(index: Int)
    func updateData()
    func seleccionarPlanCelda(index: Int)
}

class DialogDetalleSitioResumenCotizacionPresenter {
    fileprivate var view: DialogDetalleSitioResumenCotizacionDelegate?
    fileprivate var dataList = [DialogDetalleSitioResumenCotizacionModel]()
    fileprivate let service: DialogDetalleSitioResumenCotizacionService
    var idSitio: Int = -1

    init(service: DialogDetalleSitioResumenCotizacionService) {
        self.service = service

        SwiftEventBus.onMainThread(self, name: "DialogDetalleSitioResumenCotizacion(cambioSitio)") { [weak self] result in
            if let idSitio = result!.object as? Int {
                Logger.d("se ha llamado a actualizar detalle sitio")
                self?.idSitio = idSitio
                self?.actualizarDatos()
                self?.view?.updateDetalle(index: 0)
                self?.view?.seleccionarPlanCelda(index: 0)
                self?.clickPlan(0)
            }
        }
    }

    func attachView(view: DialogDetalleSitioResumenCotizacionDelegate) {
        self.view = view
    }

    func viewDidAppear() {

    }

    func detachView() {
        view = nil
    }

    func getItemIndex(index: Int) -> DialogDetalleSitioResumenCotizacionModel? {
        if getSize() == 0 {
            return nil
        }
        return dataList[index]
    }

    func getSize() -> Int {
        return dataList.count
    }

    func clickPlan(_ index: Int) {
        view?.updateDetalle(index: index)

        let data = dataList[index].serviciosAdicionales
        SwiftEventBus.post("DialogDetalleSitioTableServicios(setDatalist)", sender: data)
        let data2 = dataList[index].productosAdicionales
        SwiftEventBus.post("DialogDetalleSitioTableProductos(setDatalist)", sender: data2)
    }

    func actualizarDatos() {
        service.getPlanesDeSitioFromDB(idSitio) { [weak self] data in
            self?.onFinishGetData(data: data)
        }
    }

    func onFinishGetData(data: [DialogDetalleSitioResumenCotizacionModel]) {
        if data.count == 0 {
            dataList = [DialogDetalleSitioResumenCotizacionModel]()
        } else {
            dataList = data
        }
        view?.updateData()
    }
}

extension DialogDetalleSitioResumenCotizacionPresenter: CustomAlertViewDelegate {
    func clickOKButtonDialog(value: Any) {

    }

    func clickCancelButtonDialog() {

    }

    func viewDidShow() {

    }
}
