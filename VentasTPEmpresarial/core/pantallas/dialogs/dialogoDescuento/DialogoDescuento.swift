//
//  DialogoDescuento.swift
//  VentasTPEmpresarial
//
//  Created by eduardo mancilla on 12/7/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class DialogoDescuento: BaseDialogCustom {
    @IBOutlet weak var viewRoot: UIView!
    @IBOutlet weak var editTextPorcentaje: UITextField!
    @IBOutlet weak var slider: UISlider!
    var porcentaje: Float = 0
    public var mFormValidator: FormValidator!

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: viewRoot)
        setUpView()
    }

    private func setUpView() {
        actualizarPorcentajeLabel()
        editTextPorcentaje.delegate = self
        validator()
    }

    func validator() {
        mFormValidator = FormValidator(showAllErrors: true)
        mFormValidator.addValidators(validators:
                TextFieldValidator(textField: editTextPorcentaje, regex: RegexEnum.NOT_EMPTY, messageError: "El campo no puede estar vacío")
        )
    }

    @IBAction func cambioSlider(_ sender: UISlider) {
        //porcentaje = Float(Int(sender.value))
        porcentaje = NumberUtils.roundedPlaces(number: Float(Int(sender.value)), toPlaces: 2)
        actualizarPorcentajeLabel()
    }

    func actualizarPorcentajeLabel() {
        editTextPorcentaje.text = "\(porcentaje)"
    }

    @IBAction func clickAceptar(_ sender: UIButton) {
        self.view.endEditing(true)
        sender.animateBound(view: sender) { [unowned self] _ in
            self.aceptar()
        }
    }

    func aceptar() {
        if self.mFormValidator.isValid() {
            super.result(value: porcentaje)
        } else {
            self.showAlert(titulo: "Aviso", mensaje: "No puede estar vacio el campo.")
        }
    }

    @IBAction func cambioPorcentajeEditText(_ sender: UITextField) {
        let n = Float(sender.text!) ?? 0
        porcentaje = NumberUtils.roundedPlaces(number: n, toPlaces: 2)
        sender.text = "\(porcentaje)"
        //porcentaje = Float(sender.text!) ?? 0
        slider.value = porcentaje
    }

}

extension DialogoDescuento: UITextFieldDelegate {

    func textFieldShouldReturn(_ scoreText: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        textField.text = textField.text?.uppercased()
        switch textField {
        case editTextPorcentaje:
            var letras = CharacterSet.decimalDigits
            letras.insert(charactersIn: ".")
            let limiteDeCaracteres = 10

            //revisamos si el caracter ingresado es válido
            if string.rangeOfCharacter(from: letras.inverted) != nil {
                return false
            }
            //revisamos si el caracter no rebasa el límite de caracteres
            if ((textField.text?.count)! + (string.count - range.length)) > limiteDeCaracteres {
                return false
            } else {
                return true
            }
        default:
            return true
        }
    }

}
