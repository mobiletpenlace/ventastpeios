//
//  DialogFirmaVC.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 27/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class DialogFirmaVC: BaseDialogCustom, BaseTreeDialogs, BaseImageWriteReadDelegate {
    @IBOutlet weak var viewRoot: UIView!
    @IBOutlet weak var labelTitulo: UILabel!
    @IBOutlet weak var viewFirmaPanel: UIView!
    var titulo: String?
    var imagenView: UIImageView?
    var vc = FirmaVC()
    var nombreFile: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: viewRoot)
        reloadData()
    }

    func iniciar(titulo: String, view: UIImageView, nombreFile: String) {
        self.titulo = titulo
        self.imagenView = view
        self.nombreFile = nombreFile
        reloadData()
    }

    @IBAction func clickAceptar(_ sender: UIButton) {
        if vc.isValid() {
            let imagen = vc.save()
            imagenView?.image = imagen
            guardarImagen(img: imagen, nombre: nombreFile)
            super.result(value: "ok")
        } else {
            showAlert(titulo: "Atención", mensaje: "La firma es muy corta", vc: self)
        }
    }

    func reloadData() {
        if labelTitulo != nil && viewFirmaPanel != nil {
            labelTitulo.text = titulo
            vc = FirmaVC()
            viewFirmaPanel.addSubview(vc.view)
            var frameRect: CGRect = vc.view.frame
            frameRect.size.height = viewFirmaPanel.frame.size.height
            frameRect.size.width = viewFirmaPanel.frame.size.width
            vc.view.frame = frameRect
            vc.view.clipsToBounds = true
            vc.view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
            addChildViewController(vc)
            vc.didMove(toParentViewController: self)

        }
    }
}
