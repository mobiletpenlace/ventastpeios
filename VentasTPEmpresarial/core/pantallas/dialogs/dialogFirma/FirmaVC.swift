//
//  FirmaVC.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 27/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class FirmaVC: UIViewController {
    var lastPoint = CGPoint.zero
    var swiped = false
    var path = UIBezierPath()
    var shapeLayer = CAShapeLayer()
    var distanciaRayon: Float = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }

    func setupView() {
        self.view.layer.addSublayer(shapeLayer)
        self.shapeLayer.lineWidth = 5
        self.shapeLayer.strokeColor = UIColor.black.cgColor

        path.lineCapStyle = .round
        path.lineJoinStyle = .round
    }

    func save() -> UIImage {
        let renderer = UIGraphicsImageRenderer(size: view.bounds.size)
        let image = renderer.image { _ in
            view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
        }
        return image
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        if let location = touches.first?.location(in: self.view) {
            lastPoint = location
        }
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
        swiped = true
        if let touch = touches.first {
            let currentPoint = touch.location(in: view)
            drawLineFrom(fromPoint: lastPoint, toPoint: currentPoint)

            let dstX = pow(lastPoint.x - currentPoint.x, 2)
            let dstY = pow(lastPoint.y - currentPoint.y, 2)
            let dst = Float(sqrt(dstX + dstY))
            distanciaRayon += dst
            lastPoint = currentPoint
        }
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !swiped {
            drawLineFrom(fromPoint: lastPoint, toPoint: lastPoint)
        }
    }

    func drawLineFrom(fromPoint: CGPoint, toPoint: CGPoint) {
        path.move(to: fromPoint)
        path.addLine(to: toPoint)
        shapeLayer.path = path.cgPath
    }

    func isValid() -> Bool {
        return !path.isEmpty && distanciaRayon > 70
    }

    func clear() {
        setupView()
        path.removeAllPoints()
    }
}
