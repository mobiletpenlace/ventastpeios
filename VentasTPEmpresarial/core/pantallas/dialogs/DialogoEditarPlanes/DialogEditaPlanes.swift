//
//  DialogEditaPlanes.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 29/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class DialogEditaPlanes: BaseDialogCustom, BaseAlertViewDelegate {
    @IBOutlet weak var labelTitulo: UILabel!
    @IBOutlet weak var buttonOK: UIButton!
    @IBOutlet weak var viewRoot: UIView!
    @IBOutlet weak var tableView: UITableView!
    var dataList = [String]()
    let nombreCelda1 = "DialogEditaPlanesCell"
    var posSeleccionada = -1
    enum TIPO {
        case editar, eliminar
    }
    var tipo: TIPO = .editar

    init(dataList: [String], tipo: TIPO) {
        self.dataList = dataList
        self.tipo = tipo
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: viewRoot)
        cargarTableView()
        switch tipo {
        case .editar:
            labelTitulo.text = "Seleccione el plan a modificar"
            buttonOK.setTitle("Editar", for: .normal)
        case .eliminar:
            labelTitulo.text = "Seleccione el plan a eliminar"
            buttonOK.setTitle("Eliminar", for: .normal)
        }
    }

    func cargarTableView() {
        let nib = UINib.init(nibName: nombreCelda1, bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: nombreCelda1)
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }

    @IBAction func clickAceptar(_ sender: UIButton) {
        sender.animateBound(view: sender)
        if posSeleccionada >= 0 {
            super.result(value: [posSeleccionada, tipo])
        } else {
            alert("Atención", "Seleccione un plan de la tabla.", self, "Aceptar", nil)
        }
    }

    @IBAction func clickCancelar(_ sender: UIButton) {
        sender.animateBound(view: sender)
        super.cancel()
    }
}

extension DialogEditaPlanes: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: nombreCelda1, for: indexPath) as! DialogEditaPlanesCell
        cell.texto.text = dataList[indexPath.row]
        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataList.count
    }
}

extension DialogEditaPlanes: UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        posSeleccionada = indexPath.row
    }
}
