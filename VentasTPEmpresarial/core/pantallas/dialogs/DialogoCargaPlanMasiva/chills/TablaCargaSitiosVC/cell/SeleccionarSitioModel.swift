//
//  SeleccionarSitioModel.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 16/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct SeleccionarSitioModel {
    var sitio: Int?
    var seleccionado: Bool?
}
