//
//  SeleccionarSitiosCell.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 13/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class SeleccionarSitiosCell: UITableViewCell {
    @IBOutlet weak var labelNombreSitio: UILabel!
    @IBOutlet weak var checkButtonSelectSitio: UIButton!
    var isChecked: Bool?
    var pos: Int?

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    @IBAction func checkAction(_ sender: Any) {
        Logger.d(isChecked!)
        SwiftEventBus.post(
            "Modelado_Masivo_change_seleccionado",
            sender: SeleccionarSitioModel(
                sitio: pos,
                seleccionado: isChecked)
        )
    }
}
