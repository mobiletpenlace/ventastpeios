//
//  TablaCargaSitiosPresenter.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 16/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol TablaCargaSitiosDelegate: BaseDelegate {
    func reloadTable()
    func selecciono()
    func noSelecciono()
}

class TablaCargaSitiosPresenter {

    weak fileprivate var view: TablaCargaSitiosDelegate?
    fileprivate let service: DetalleSitiosService
    var indexSeleccionado: Int = 0
    var posicionEditarPlan: Int = -1
    fileprivate var dataList = [DetalleSitiosModel]()
    fileprivate var data = [TablaCargaSitiosModel]()
    var tipo: Constantes.Enum.TipoEditar = .none
    var entro = false
    var count = 0

    init(service: DetalleSitiosService) {
        self.service = service
    }

    func attachView(view: TablaCargaSitiosDelegate) {
        self.view = view
        SwiftEventBus.onMainThread(self, name: "Modelado_Masivo_change_seleccionado") { [weak self] result in
            let obj = result?.object as! SeleccionarSitioModel
            self?.data[obj.sitio!].seleccionado = obj.seleccionado
            self?.view?.reloadTable()
        }
        actualizarDatos()
    }

    func viewDidAppear() {
    }

    /*protocol*/
    func viewWillAppear() {

    }

    func viewDidShow() {
        view?.updateTitleHeader()

    }

    func detachView() {
        view = nil
    }

    func getItemIndex(index: Int) -> TablaCargaSitiosModel {
        return data[index]
    }

    func getSize() -> Int {
        return dataList.count
    }

    func selectAll() {
        for _ in data {
            data[count].seleccionado = true
            count += 1
        }
        count = 0
        self.view?.reloadTable()
    }

    func verificaSeleccion() {
        for sitio in data {
            if sitio.seleccionado! {
                entro = true
                service.agregarPlanASitio(sitio: dataList[count])
            }
            count += 1
        }
        count = 0
        if entro {
            self.view!.selecciono()
        } else {
            self.view!.noSelecciono()
        }
    }

    func agregarPlanASitio(_ indexSeleccionado: Int, _ namePlan: String, _ id: String) {

    }

    func actualizarDatos() {
        self.view?.startLoading()
        service.getData { [weak self] data in
            self?.onFinishGetData(data: data)
        }
    }

    func onFinishGetData(data: [DetalleSitiosModel]) {
        view?.finishLoading()

        dataList = data
        for sitio in data {
            self.data.append(
                TablaCargaSitiosModel(
                    sitio: sitio,
                    seleccionado: false
                )
            )
            view?.reloadTable()
        }
    }
}
