//
//  TablaCargaSitiosModel.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 16/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct TablaCargaSitiosModel {
    var sitio: DetalleSitiosModel?
    var seleccionado: Bool?
}
