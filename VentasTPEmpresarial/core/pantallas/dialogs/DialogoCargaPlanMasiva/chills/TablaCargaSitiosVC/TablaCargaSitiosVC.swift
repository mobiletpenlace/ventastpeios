//
//  TablaCargaSitiosVC.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 13/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class TablaCargaSitiosVC: BaseItemVC {

    @IBOutlet weak var tablaViewSitios: UITableView!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var cancellButton: UIButton!
    @IBOutlet weak var allSelectButton: UIButton!
    fileprivate let presenter = TablaCargaSitiosPresenter(service: DetalleSitiosService())
    var nombreCelda2: String = "SeleccionarSitiosCell"

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUpView()

    }

    func setUpView() {
        addBorder(view: addButton)
        addRoundCorner(view: addButton, value: 5)
        addBorder(view: cancellButton)
        addRoundCorner(view: cancellButton, value: 5)
        addBorder(view: allSelectButton)
        addRoundCorner(view: allSelectButton, value: 5)
        let nib = UINib.init(nibName: nombreCelda2, bundle: nil)
        self.tablaViewSitios.register(nib, forCellReuseIdentifier: nombreCelda2)
        self.tablaViewSitios.delegate = self
        self.tablaViewSitios.dataSource = self
    }

    func addBorder(view: UIView) {
        view.layer.borderColor = UIColor.black.cgColor
        view.layer.borderWidth = CGFloat(0.5)
    }

    func addRoundCorner(view: UIView, value: Float) {
        view.layer.cornerRadius = CGFloat(value)
        view.clipsToBounds = true
    }

    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func addModelsAction(_ sender: Any) {
        presenter.verificaSeleccion()
    }
    @IBAction func allSelectAction(_ sender: Any) {
        presenter.selectAll()
    }
}

extension TablaCargaSitiosVC: UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return SitiosCellHeader().view
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = presenter.getItemIndex(index: indexPath.row)
        SwiftEventBus.post(
            "Modelado_Masivo_change_seleccionado",
            sender: SeleccionarSitioModel(
                sitio: indexPath.row,
                seleccionado: !data.seleccionado!)
        )
    }
}

extension TablaCargaSitiosVC: UITableViewDataSource, BaseFormatNumber {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getSize()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: nombreCelda2, for: indexPath) as! SeleccionarSitiosCell
        let data = presenter.getItemIndex(index: indexPath.row)
        cell.labelNombreSitio.text = data.sitio?.name
        cell.pos = indexPath.row
        if data.seleccionado! {
            ButtonUtils.setIcon(cell.checkButtonSelectSitio, icon: "icon_ok")
        } else {
            ButtonUtils.setIcon(cell.checkButtonSelectSitio, icon: "bg_login")
        }
        cell.isChecked = data.seleccionado
        cell.selectionStyle = .none //desactivar el color al seleccionar la celda
        return cell
    }

}

extension TablaCargaSitiosVC: TablaCargaSitiosDelegate {
    func selecciono() {
        SwiftEventBus.post("sitios-detalleSitios-informarTipoRegreso", sender: 0)
        SwiftEventBus.post("menuTabBar-cambiarMenuItem", sender: (2, 0))
        self.dismiss(animated: true, completion: nil)
    }

    func noSelecciono() {
        super.showAlert(titulo: "Alerta", mensaje: "No ha seleccionado ningun sitio")
    }

    func reloadTable() {
        tablaViewSitios.reloadData()
    }

    func updateTitleHeader() {

    }

    func startLoading() {

    }

    func finishLoading() {

    }
}
