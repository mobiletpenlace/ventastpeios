//
//  DialogoCargaPlanMasivaVC.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 13/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class DialogoCargaPlanMasivaVC: BaseDialogCustom {

    @IBOutlet weak var mViewContainerSitios: UIView!
    @IBOutlet weak var rootView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: rootView)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setUpView()
    }

    private func setUpView() {
        super.addViewXIB(vc: TablaCargaSitiosVC(), container: mViewContainerSitios)
    }

}
