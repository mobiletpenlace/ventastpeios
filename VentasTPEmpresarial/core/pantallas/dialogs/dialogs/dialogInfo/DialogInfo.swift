//
//  DialogInfo.swift
//  VentasTPEmpresarial
//
//  Created by eduardo mancilla on 12/3/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class DialogInfo: BaseDialogCustom {
    @IBOutlet weak var viewRoot: UIView!
    @IBOutlet weak var labelText: UILabel!
    @IBOutlet weak var labelTitulo: UILabel!
    var text: String?
    var titulo: String?

    init(text: String, titulo: String) {
        self.text = text
        self.titulo = titulo
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: viewRoot)
        reloadData()
    }

    func reloadData() {
        if labelText != nil {
            labelText.text = text
        }
        if labelTitulo != nil {
            labelTitulo.text = titulo
        }
    }

    func hideLoaging() {
        super.cancel()
    }

    func updateText(text: String) {
        if labelText != nil {
            labelText.text = text
        }
    }

    @IBAction func clickAceptar(_ sender: UIButton) {
        sender.animateBound(view: sender) { _ in

        }
    }

}
