//
//  ViewController.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 11/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class DialogLoadingCustom: BaseDialogCustom {
    @IBOutlet weak var viewRoot: UIView!
    @IBOutlet weak var labelText: UILabel!
    var text: String?

    init(text: String) {
        self.text = text
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: viewRoot)
        reloadData()
    }

    func reloadData() {
        if labelText != nil {
            labelText.text = text
        }
    }

    func hideLoaging() {
        super.cancel()
    }

    func updateText(text: String) {
        if labelText != nil {
            labelText.text = text
        }
    }
}
