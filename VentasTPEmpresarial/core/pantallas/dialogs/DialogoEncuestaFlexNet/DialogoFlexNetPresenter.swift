//
//  DialogoFlexNetPresenter.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 01/02/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol DialogoFlexNetDelegate: BaseDelegate {
    func update()
}

class DialogoFlexNetPresenter {
    fileprivate var data: DialogoFlexNetModel!
    var listRSalidaInternet: [String] = ["SI", "NO"]
    var listTipoRouteoEquipo: [String] = ["Routeo OSPF", "RouteoStatico"]

    init() {
        data = DialogoFlexNetModel(
            ipOrigenQOS: "",
            listadoDominios: "",
            salidaInternet: "",
            segmentoOrigen: "",
            tipoRouteo: "",
            ipDestinoQOS: "",
            vlansServiciosCliente: "",
            porcentajeAnchoBanda: "",
            segmentoDestino: "",
            areaID: ""
        )
    }

    func getArrayRSalidaInternet() -> [String] {
        return listRSalidaInternet
    }

    func getArrayTipoRouteoEquipo() -> [String] {
        return listTipoRouteoEquipo
    }

    func updateIngresarIPOrigen(item: String) {
        data.ipOrigenQOS = item
    }

    func updateListadoDominios(item: String) {
        Logger.i("se ha actualizado el valor: \(item)")
        data.listadoDominios = item
    }
    func seleccionarSalidaInternet(item: String) {
        data?.salidaInternet = item
    }

    func updateDeterminarSegmentoOrigen(item: String) {
        data.segmentoOrigen = item
    }

    func seleccionarTipoRouteoEquipo(item: String) {
        data?.tipoRouteo = item
    }

    func updateIngresarIPDestino(item: String) {
        data.ipDestinoQOS = item
    }

    func updateDeterminarVLAns(item: String) {
        Logger.i("se ha actualizado el valor: \(item)")
        data.vlansServiciosCliente = item
    }

    func updateDeterminarAnchoBanda(item: String) {
        data.porcentajeAnchoBanda = item
    }

    func updateDeterminarSegmentosDestino(item: String) {
        Logger.i("se ha actualizado el valor: \(item)")
        data.segmentoDestino = item
    }

    func updateIndicarAreaID(item: String) {
        data.areaID = item
    }

    func getData() -> DialogoFlexNetModel {
        return data
    }

    func clickSiguiente() {

    }

}
