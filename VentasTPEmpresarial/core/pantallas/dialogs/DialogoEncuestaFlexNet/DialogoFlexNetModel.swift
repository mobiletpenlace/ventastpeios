//
//  DialogoFlexNetModel.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 01/02/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation

struct DialogoFlexNetModel {

    var ipOrigenQOS: String
    var listadoDominios: String
    var salidaInternet: String
    var segmentoOrigen: String
    var tipoRouteo: String
    
    var ipDestinoQOS: String
    var vlansServiciosCliente: String
    var porcentajeAnchoBanda: String
    var segmentoDestino: String
    var areaID: String
    
}
