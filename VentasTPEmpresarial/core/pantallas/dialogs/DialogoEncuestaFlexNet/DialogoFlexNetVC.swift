//
//  DialogoFlexNetVC.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 30/01/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import UIKit

class DialogoFlexNetVC: BaseDialogCustom {
    @IBOutlet var viewRoot: UIView!
    @IBOutlet weak var textfieldIngresarIPOrigen: UITextField!
    @IBOutlet weak var textfieldListadoDominios: UITextView!
    @IBOutlet weak var textfieldRSalidaInternet: UITextField!
    @IBOutlet weak var textfieldDeterminarSegmentoOrigen: UITextField!
    @IBOutlet weak var textfieldTipoRouteo: UITextField!
    @IBOutlet weak var textfieldIngresarIPDestino: UITextField!
    @IBOutlet weak var textfieldDeterminarVLAns: UITextView!
    @IBOutlet weak var textfieldDeterminarAnchoBanda: UITextField!
    @IBOutlet weak var textfieldDeterminarSegmentosDestinos: UITextView!
    @IBOutlet weak var textfieldIndicarAreaID: UITextField!
    fileprivate let presenter = DialogoFlexNetPresenter()
    let dropManager = DropDownUtil()
    let validatorManager = ValidatorManager()
    var closure: ((DialogoFlexNetModel?) -> Void)!

    init(closure: @escaping ((DialogoFlexNetModel?) -> Void)) {
        self.closure = closure
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        super.setView(view: viewRoot)
        setUpView()
    }

    private func setUpView() {
        setUpValidator()
        setUPBeginEdit() 
        setUPEndEdit()
        setUpDropDown()
    }

    private func setUpValidator() {
        textfieldIngresarIPOrigen.delegate = validatorManager
        textfieldDeterminarSegmentoOrigen.delegate = validatorManager
        textfieldIngresarIPDestino.delegate = validatorManager
        textfieldDeterminarAnchoBanda.delegate = validatorManager
        textfieldIndicarAreaID.delegate = validatorManager

        textfieldListadoDominios.delegate = validatorManager
        textfieldDeterminarVLAns.delegate = validatorManager
        textfieldDeterminarSegmentosDestinos.delegate = validatorManager

        validatorManager.addValidator(

            FieldValidator.textField(
                textfieldIngresarIPOrigen, .regex(.ip), .numeroDecimal, "El formato está incorrecto: x.x.x.x"
            ),

            FieldValidator.textField(
                textfieldDeterminarSegmentoOrigen, .regex(.ip), .numeroDecimal, "El formato está incorrecto: x.x.x.x"
            ),

            FieldValidator.textField(
                textfieldIngresarIPDestino, .regex(.ip), .numeroDecimal, "El formato está incorrecto: x.x.x.x"
            ),

            FieldValidator.textField(
                textfieldDeterminarAnchoBanda, .required, .numero, "El campo no puede estar vacío"
            ),

            FieldValidator.textField(
                textfieldIndicarAreaID, .required, .numero, "El campo no puede estar vacío"
            )
        )

        validatorManager.addValidator(
            FieldValidator.textView(textfieldListadoDominios, .regex(.hostNameSeparateComa), .all, "El formato está incorrecto"),

            FieldValidator.textView(textfieldDeterminarVLAns, .regex(.ipSepateComa), .ipComa, "El formato está incorrecto"),

            FieldValidator.textView(textfieldDeterminarSegmentosDestinos, .regex(.ipSepateComa), .ipComa, "El formato está incorrecto")
        )
    }

    private func setUPBeginEdit() {
        validatorManager.addChangeEnterField(from: textfieldIngresarIPOrigen, to: textfieldDeterminarSegmentoOrigen)
        validatorManager.addChangeEnterField(from: textfieldDeterminarSegmentoOrigen, to: textfieldIngresarIPDestino)
        validatorManager.addChangeEnterField(from: textfieldIngresarIPDestino, to: textfieldDeterminarAnchoBanda)
        validatorManager.addChangeEnterField(from: textfieldDeterminarAnchoBanda, to: textfieldIndicarAreaID)
    }

    private func setUPEndEdit() {
        validatorManager.addEndEditingFor(textfieldIngresarIPOrigen) { [weak self] (textField) -> Void in
            self?.presenter.updateIngresarIPOrigen(item: textField.text ?? "")
        }
        validatorManager.addEndEditingFor(textfieldRSalidaInternet) { [weak self] (textField) -> Void in
            self?.presenter.seleccionarSalidaInternet(item: textField.text ?? "")
        }
        validatorManager.addEndEditingFor(textfieldDeterminarSegmentoOrigen) { [weak self] (textField) -> Void in
            self?.presenter.updateDeterminarSegmentoOrigen(item: textField.text ?? "")
        }
        validatorManager.addEndEditingFor(textfieldTipoRouteo) { [weak self] (textField) -> Void in
            self?.presenter.seleccionarTipoRouteoEquipo(item: textField.text ?? "")
        }
        validatorManager.addEndEditingFor(textfieldIngresarIPDestino) { [weak self] (textField) -> Void in
            self?.presenter.updateIngresarIPDestino(item: textField.text ?? "")
        }
        validatorManager.addEndEditingFor(textfieldDeterminarAnchoBanda) { [weak self] (textField) -> Void in
            self?.presenter.updateDeterminarAnchoBanda(item: textField.text ?? "")
        }
        validatorManager.addEndEditingFor(textfieldDeterminarAnchoBanda) { [weak self] (textField) -> Void in
            self?.presenter.updateDeterminarAnchoBanda(item: textField.text ?? "")
        }
        validatorManager.addEndEditingFor(textfieldIndicarAreaID) { [weak self] (textField) -> Void in
            self?.presenter.updateIndicarAreaID(item: textField.text ?? "")
        }

        validatorManager.addEndEditingFor(textfieldListadoDominios) { [weak self] (textField) -> Void in
            self?.presenter.updateListadoDominios(item: textField.text ?? "")
        }
        validatorManager.addEndEditingFor(textfieldDeterminarVLAns) { [weak self] (textField) -> Void in
            self?.presenter.updateDeterminarVLAns(item: textField.text ?? "")
        }

        validatorManager.addEndEditingFor(textfieldDeterminarSegmentosDestinos) { [weak self] (textField) -> Void in
            self?.presenter.updateDeterminarSegmentosDestino(item: textField.text ?? "")
        }

    }

    private func setUpDropDown() {
        textfieldTipoRouteo.delegate = dropManager
        textfieldRSalidaInternet.delegate = dropManager

        dropManager.add(textfield: textfieldRSalidaInternet, data: presenter.getArrayRSalidaInternet()) { [unowned self] (_, item: String) in
            self.textfieldRSalidaInternet.text = item
            self.presenter.seleccionarSalidaInternet(item: item)
        }

        dropManager.add(textfield: textfieldTipoRouteo, data: presenter.getArrayTipoRouteoEquipo()) { [unowned self] (_, item: String) in
            self.textfieldTipoRouteo.text = item
            self.presenter.seleccionarTipoRouteoEquipo(item: item)
        }
    }

    @IBAction func onClickCancelar(_ sender: UIButton) {
        closure(nil)
        super.cancel()
    }

    @IBAction func onClickGuardar(_ sender: UIButton) {
        sender.animateBound(view: sender) { [weak self] _ in
            guard let mSelf = self else { return }
            let validateDrop = mSelf.dropManager.validate()
            let validateTextFields = mSelf.validatorManager.validate()
            if validateDrop && validateTextFields {
                mSelf.onValidate()
            } else {
                mSelf.showBannerError(title: "Hay campos con errores.")
            }
        }
    }

    private func onValidate() {
        Logger.d("el modelos de flexnet es: \(presenter.getData())")
        closure(presenter.getData())
        super.cancel()
    }

}
