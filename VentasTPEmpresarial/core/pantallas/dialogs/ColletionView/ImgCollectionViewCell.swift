//
//  ImgCollectionViewCell.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 26/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class ImgCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgImage: UIImageView!
    
}
