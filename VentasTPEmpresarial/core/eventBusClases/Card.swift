//
//  Card.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 09/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
class Card {
    var numberCard: String?
    var expirityMonth: UInt
    var expirityYear: UInt

    init(numberCard: String?, expirityMonth: UInt, expirityYear: UInt) {
        self.numberCard = numberCard
        self.expirityMonth = expirityMonth
        self.expirityYear = expirityYear
    }
}
