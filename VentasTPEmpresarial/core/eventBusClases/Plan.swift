//
//  Plan.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 31/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

class Plan {
    var nombre: String = ""
    
    init(nombre: String) {
        self.nombre = nombre
    }
}
