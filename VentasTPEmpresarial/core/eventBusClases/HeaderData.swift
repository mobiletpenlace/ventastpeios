//
//  HeaderData.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 07/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

struct HeaderData {
    let imagen: UIImage?
    let titulo: String?
    let titulo2: String?
    let titulo3: String?
    let subtitulo: String?
    let closure: (() -> Void)?
}
