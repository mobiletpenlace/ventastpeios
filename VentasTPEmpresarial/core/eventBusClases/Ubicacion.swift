//
//  Ubicacion.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 18/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

class Ubicacion {
    var nombreSitio: String
    var numero: String
    var latitud: Double
    var longitud: Double
    var calle: String
    var colonia: String
    var estado: String
    var ciudad: String
    var codigoPostal: String
    var delegacionMunicipio: String

    init(nombreSitio: String, numero: String, latitud: Double, longitud: Double, calle: String, colonia: String, estado: String, ciudad: String, codigoPostal: String, delegacionMunicipio: String) {
        self.nombreSitio = nombreSitio
        self.numero = numero
        self.latitud = latitud
        self.longitud = longitud
        self.calle = calle
        self.colonia = colonia
        self.estado = estado
        self.ciudad = ciudad
        self.codigoPostal = codigoPostal
        self.delegacionMunicipio = delegacionMunicipio
    }

}
