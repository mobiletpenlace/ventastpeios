//
//  Mensaje.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 08/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import Foundation

class Mensaje {
    var opcion: Int = -1
    
    init(opcion: Int) {
        self.opcion = opcion
    }
}
