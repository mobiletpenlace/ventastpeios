//
//  SolucionMedidaViewController.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 29/06/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class SolucionMedidaVC: BaseItemVC{
    @IBOutlet weak var planesViewCollection: UIView!
    @IBOutlet weak var resumenCotizacionView: UIView!
    @IBOutlet weak var panel: UIView!
    fileprivate let presenter = SolucionMedidaPresenter(model: SolucionMedidaService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow() {
        super.viewDidShow()
        presenter.viewDidShow()
    }
    
    private func setUpView(){
        super.addShadowView(view: resumenCotizacionView)
        cargarSubVistas()
        addSubViewItems(viewContainer: panel)
    }
    
    func cargarSubVistas() -> Void {
        super.addViewXIB(vc: PlanesCustomSeleccionVC(), container: planesViewCollection)
        super.addViewXIB(vc: ResumenCotizacionCustomVC(), container: resumenCotizacionView)
    }
    
    func addSubViewItems(viewContainer: UIView) -> Void {
        var viewControllers = [BaseItemVC]()
        viewControllers.append(DefaultLogoVC())
        viewControllers.append(InformacionPlanCustomVC())
        super.addViewXIBList(view: panel, viewControllers: viewControllers)
    }
    
}

extension SolucionMedidaVC: SolucionMedidaDelegate{
    
    func cambioPlanSeleccionado(plan: Plan){
        let header = HeaderData(
//            imagen: #imageLiteral(resourceName: "icon_ConfiguraPlanes"),
            imagen: #imageLiteral(resourceName: "logo_Cotizar"),
            titulo: "Cotizar",
//            titulo2: "Configuración de Plan",
            titulo2: "Soluciones a la medida",
            titulo3: nil,
            subtitulo: plan.nombre,
            closure:{ [weak self] in
                self?.padreController?.cambiarVista(index: 0)
            }
        )
        super.updateTitle(headerData: header)
        cambiarVistaChild(index: 1)
    }
    
    func updateTitleHeader() {
        let header = HeaderData(
//            imagen: #imageLiteral(resourceName: "icon_ConfiguraPlanes"),
            imagen: #imageLiteral(resourceName: "logo_Cotizar"),
            titulo: "Cotizar",
//            titulo2: "Configuración de Plan",
            titulo2: "soluciones a la medida",
            titulo3: nil,
            subtitulo: nil,
            closure:{
                [weak self] in
                self?.padreController?.cambiarVista(index: 0)
            }
        )
        super.updateTitle(headerData: header)
    }
    
    func startLoading() {
        super.showLoading(view: view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
}
