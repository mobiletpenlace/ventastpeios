//
//  SolucionMedidaService.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 12/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import RealmSwift

class SolucionMedidaService: BaseDBManagerDelegate {
    var serverManager: ServerDataManager?
    var realm: Realm!
    var dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()
        realm = getRealm()
    }
    
    func crearPlanConfig(_ idPlan: String) -> Int?{
        if let configPlan = dbManager.crearConfigPlanSolucionesMedida(idPlan: idPlan){
            return configPlan.id
        }
        return nil
    }
    
    func getPlanSolucionesAlAMedida() -> DBPlan?{
        return dbManager.getPlanSolucionesAlAMedida()
    }
    
}
