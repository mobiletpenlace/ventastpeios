//
//  SolucionMedidaPresenter.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 12/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol SolucionMedidaDelegate: BaseDelegate {
    func cambioPlanSeleccionado(plan: Plan)
}

class SolucionMedidaPresenter {
    fileprivate let service: SolucionMedidaService
    weak fileprivate var view: SolucionMedidaDelegate?
    var planSeleccionado: SolucionMedidaModel!
    
    init(model: SolucionMedidaService){
        self.service = model
    }
    
    func attachView(view: SolucionMedidaDelegate){
        self.view = view
        
        guard let planSM = service.getPlanSolucionesAlAMedida() else {
            Logger.e("SMedida: No se ha podido obtener el plan soluciones a la medida desde al service.")
            return
        }
        
        planSeleccionado = SolucionMedidaModel(
            nombre: planSM.nombre ?? "",
            id: planSM.id!
        )
        
        SwiftEventBus.onMainThread(self, name: Constantes.Eventbus.Id.solucionesMedidaCambioPlan) {
            [weak self] result in
            if let plan = result!.object as? Plan{
                self?.view?.cambioPlanSeleccionado(plan: plan)
            }
        }
    }
    
    func viewDidAppear(){
        
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
        if let planSeleccionado = planSeleccionado{
            if let configPlanId = service.crearPlanConfig(planSeleccionado.id){
                Logger.d("Se ha creado config plan id: \(configPlanId) para el plan con id: \(planSeleccionado.id)")
                informarPlanConfigId(configPlanId)
            }
        }
    }
    
    private func informarPlanConfigId(_ configPlanId: Int){
        SwiftEventBus.post(
            "CotizacionCustomPlanes-actualizarconfigPlanId",
            sender: configPlanId
        )
        
        SwiftEventBus.post(
            "SolucionesALaMedidaPlanesSeleccion-cambioIdConfigPlan",
            sender: configPlanId
        )
        SwiftEventBus.post(
            "CotizacionSolucionesMedidaResumenCotizacionCustom-cambioIdConfigPlan",
            sender: configPlanId
        )
        SwiftEventBus.post(
            "CotizacionSolucionesMedidaResumenCotizacionCustom-cambioPlan",
            sender: SeleccionarPlanesModel(id: "", nombre: "")
        )
    }
    
    func detachView() {
        view = nil
    }
    
}
