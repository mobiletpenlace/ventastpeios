//
//  SolucionMedidaModel.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 12/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct SolucionMedidaModel {
    let nombre: String
    let id: String
}
