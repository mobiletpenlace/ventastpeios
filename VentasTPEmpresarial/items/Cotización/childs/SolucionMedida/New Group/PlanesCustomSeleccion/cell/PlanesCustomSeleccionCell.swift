//
//  CustomPlanCell.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 04/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class PlanesCustomSeleccionCell: UICollectionViewCell {
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var botonAgregar: UIButton!
    @IBOutlet weak var viewContainer: UIView!
    var idServicio: String = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        roundCorners(cornerRadius: 5, view: botonAgregar)
        isSelected = false
    }
    
    func roundCorners(cornerRadius: Double, view: UIView) {
        let path = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = view.bounds
        maskLayer.path = path.cgPath
        view.layer.mask = maskLayer
    }
    
    override var isSelected: Bool {
        /*se cambia el color de la celda que ha sido seleccionada*/
        didSet {
            if isSelected {
                botonAgregar.isHidden = false
            } else {
                botonAgregar.isHidden = true
            }
        }
    }
    
    @IBAction func clickagregar(_ sender: UIButton) {
        sender.animateScale(view: self.viewContainer)
        SwiftEventBus.post(
            "SolucionesALaMedidaPlanesSeleccion-agregarServicioPlan",
            sender: idServicio
        )
    }
    
}
