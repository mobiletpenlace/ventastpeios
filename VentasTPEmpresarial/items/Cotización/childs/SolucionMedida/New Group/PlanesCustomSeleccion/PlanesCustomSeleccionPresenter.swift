//
//  PlanesCustomSeleccionPresenter.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 23/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol PlanesCustomSeleccionDelegate: BaseDelegate {
    func updateData()
}

class PlanesCustomSeleccionPresenter {
    fileprivate let model: PlanesCustomSeleccionService
    weak fileprivate var view: PlanesCustomSeleccionDelegate?
    fileprivate var dataList = [PlanesCustomSeleccionModel]()
    var configPlanId: Int?
    
    init(model: PlanesCustomSeleccionService){
        self.model = model
    }
    
    func attachView(view: PlanesCustomSeleccionDelegate){
        self.view = view
        SwiftEventBus.onMainThread(self, name: "SolucionesALaMedidaPlanesSeleccion-cambioIdConfigPlan") {
            [weak self] result in
            if let configPlanId = result!.object as? Int{
                self?.configPlanId = configPlanId
                self?.getData()
            }
        }
        
        SwiftEventBus.onMainThread(self, name: "SolucionesALaMedidaPlanesSeleccion-agregarServicioPlan") {
            [weak self] result in
            if let idServicio = result!.object as? String{
                SwiftEventBus.post(
                    "CotizacionCustomPlanes-agregarServicioPlan",
                    sender: (self?.configPlanId, idServicio)
                )
            }
        }
    }
    
    func viewDidAppear(){
        
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
        getData()
    }
    
    func detachView() {
        view = nil
    }
    
    func getItemIndex(index: Int) -> PlanesCustomSeleccionModel {
        return dataList[index]
    }
    
    func getSize() -> Int{
        return dataList.count
    }
    
    func selectCell(index: Int){
        let data = dataList[index]
        SwiftEventBus.post(
            Constantes.Eventbus.Id.solucionesMedidaCambioPlan,
            sender: Plan(nombre: data.nombre)
        )
    }
    
    func getData(){
        self.view?.startLoading()
        if let configPlanId = configPlanId{
            model.getData(configPlanId){
                [weak self] data in
                self?.view?.finishLoading()
                if(data.count == 0){
                    self?.dataList = [PlanesCustomSeleccionModel]()
                }else{
                    self?.dataList = data
                }
                self?.view?.updateData()
            }
        }else{
            Logger.w("no hay configPlanId para obtener los planes de soluciones medida.")
            self.view?.finishLoading()
        }
    }
}
