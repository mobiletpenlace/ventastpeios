//
//  PlanesCustomSeleccionServicio.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 23/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import RealmSwift

class PlanesCustomSeleccionService: BaseDBManagerDelegate {
    var realm: Realm!
    var serverManager: ServerDataManager?
    var dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()
        realm = getRealm()
    }
    
    func getData(_ configPlanId: Int, _ callBack:@escaping ([PlanesCustomSeleccionModel]) -> Void) {
        var data = [PlanesCustomSeleccionModel]()//crear lista de datos
        for prod in dbManager.obtenerProductosAdicionales(configPlanId: configPlanId){
            Logger.d("agregando a la lista: \(prod.nombre)")
            data.append(
                PlanesCustomSeleccionModel(
                    nombre: prod.nombre,
                    id: prod.id!
                )
            )
        }
        callBack(data)
    }
    
}
