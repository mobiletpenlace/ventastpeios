//
//  PlanesCustomCollectionViewController.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 04/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class PlanesCustomSeleccionVC: BaseItemVC {
    @IBOutlet weak var collectionView: UICollectionView!
    fileprivate let presenter = PlanesCustomSeleccionPresenter(model: PlanesCustomSeleccionService())
    let customCell = "PlanesCustomSeleccionCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow() {
        super.viewDidShow()
        presenter.viewDidShow()
    }
    
    private func setUpView() {
        iniciarCollectionView()
        super.addShadowView(view: self.collectionView)
    }
    
    func iniciarCollectionView() -> Void {
        let nib = UINib.init(nibName: customCell, bundle: nil)
        self.collectionView.register(nib, forCellWithReuseIdentifier: customCell)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
}

extension PlanesCustomSeleccionVC: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.getSize()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: customCell, for: indexPath) as! PlanesCustomSeleccionCell
        let data = presenter.getItemIndex(index: indexPath.row)
        cell.label1.text = data.nombre
        cell.idServicio = data.id
        return cell
    }
}

extension PlanesCustomSeleccionVC: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = self.collectionView.cellForItem(at: indexPath) as? PlanesCustomSeleccionCell {
            cell.animateBound(view: cell){ [weak self] _ in
                self?.presenter.selectCell(index: indexPath.row)
            }
        }
    }
}

extension PlanesCustomSeleccionVC: PlanesCustomSeleccionDelegate {
    
    func updateData(){
        self.collectionView.reloadData()
    }
    
    func updateTitleHeader() {
        
    }
    
    func startLoading() {
        super.showLoading(view: view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
}
