//
//  PlanesCustomSeleccionModel.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 23/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct PlanesCustomSeleccionModel {
    let nombre: String
    let id: String
}
