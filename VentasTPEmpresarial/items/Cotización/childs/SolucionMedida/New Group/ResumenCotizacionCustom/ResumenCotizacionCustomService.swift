//
//  ResumenCotizacionCustomService.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 16/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

class ResumenCotizacionCustomService: BaseDBManagerDelegate {
    var serverManager: ServerDataManager?
    var dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()
    }
    
    func getDataFromDB(_ configPlanId: Int, _ callBack:@escaping ([ResumenCotizacionCustomModel]) -> Void) {
        var data = [ResumenCotizacionCustomModel]()//crear lista de datos
        if let plan = dbManager.buscarConfigPlanPorId(configPlanId){
            if let resumenCotizacion = plan.resumenCotizacion{
                data.append(
                    ResumenCotizacionCustomModel(titulo: "Renta mensual s/i", precio: resumenCotizacion.costoRentaMensual)
                )
                let costo = resumenCotizacion.costoInstalacion
                let plazoSeleccionado = dbManager.getPlazo()
                data.append(
                    ResumenCotizacionCustomModel(
                        titulo: "Costo de instalacion",
                        precio: (plazoSeleccionado == "36") ? 0 : costo
                    )
                )
                data.append(
                    ResumenCotizacionCustomModel(titulo: "Adicionales", precio: resumenCotizacion.costoAdicionales)
                )
                data.append(
                    ResumenCotizacionCustomModel(titulo: "Descuento", precio: resumenCotizacion.descuento)
                )
                data.append(
                    ResumenCotizacionCustomModel(titulo: "Sub Total", precio: resumenCotizacion.subTotal)
                )
                data.append(
                    ResumenCotizacionCustomModel(titulo: "Impuestos", precio: resumenCotizacion.impuestos)
                )
                data.append(
                    ResumenCotizacionCustomModel(titulo: "Total cargos únicos", precio: resumenCotizacion.cargosUnicos)
                )
                data.append(
                    ResumenCotizacionCustomModel(titulo: "Total", precio: resumenCotizacion.total)
                )
            }
        }
        callBack(data)
    }
    
    func getData(_ callBack:@escaping ([DetalleSitiosModel]) -> Void){
        getDataFromDB2(callBack)
    }
    
    func getDataFromDB2( _ callBack:@escaping ([DetalleSitiosModel]) -> Void) {
        var data = [DetalleSitiosModel]()//crear lista de datos
        //dbManager.reloadRealm()
        let sitios = dbManager.obtenerSitios()
        for sitio in sitios{
            var planes = [SitioPlanModel]()
            for plan in sitio.planesConfig{
                planes.append(
                    SitioPlanModel(
                        id: plan.id,
                        nombre: plan.plan.nombre!
                    )
                )
            }
            var imagen: String = "img_cobertura_nose"
            if let cobertura = sitio.cobertura{
                if (cobertura.cobertura == true){
                    imagen = "img_cobertura"
                }else{
                    imagen = "img_no_cobertura"
                }
            }
            data.append(
                DetalleSitiosModel(
                    id: sitio.id,
                    name: sitio.nombre!,
                    planes: planes,
                    imagen: imagen,
                    precio: sitio.total
                )
            )
        }
        callBack(data)
    }
}
