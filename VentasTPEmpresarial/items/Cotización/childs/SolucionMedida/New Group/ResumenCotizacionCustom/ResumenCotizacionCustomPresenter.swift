//
//  ResumenCotizacionCustomPresenter.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 16/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import Foundation
import SwiftEventBus

protocol ResumenCotizacionCustomDelegate: BaseDelegate {
    func updateData()
    func masiveModel()
    func noSites()
    func getPadreVC() -> BaseTreeVC?
    func msgAlert(titulo: String, mensaje: String)
    func msgAlert(titulo: String, mensaje: String, tituloBtn1: String, tituloBtn2: String, closureAgregar: @escaping ((UIAlertAction) -> Void), closureOmitir: @escaping ((UIAlertAction) -> Void))
}

class ResumenCotizacionCustomPresenter{
    fileprivate var view: ResumenCotizacionCustomDelegate?
    var planSeleccionado: SeleccionarPlanesModel?
    enum TipoEditar{ case Default, Agregar, Actualizar, Eliminar }
    var tipo: ResumenCotizacionPlanPresenter.TipoEditar = .Default
    fileprivate var data = [DetalleSitiosModel]()
    fileprivate var dataList = [ResumenCotizacionCustomModel]()
    fileprivate let service: ResumenCotizacionCustomService
    var configPlanId: Int?
    
    init(service: ResumenCotizacionCustomService){
        self.service = service
    }
    
    func attachView(view: ResumenCotizacionCustomDelegate){
        self.view = view
        SwiftEventBus.onMainThread(self, name: "CotizacionSolucionesMedidaResumenCotizacionCustom-cambioPlan") {
            [weak self] result in
            if let plan = result!.object as? SeleccionarPlanesModel{
                self?.planSeleccionado = plan
                self?.actualizarDatos()
            }
        }
        
        //igual que el de planes, para capturar los eventos de edicion
        SwiftEventBus.onMainThread(self, name: "CotizacionSolucionesMedidaResumenCotizacionCustom-cambioTipoEdicion") {
            [weak self] result in
            if let tipoSeleccionado = result!.object as? ResumenCotizacionPlanPresenter.TipoEditar{
                self?.tipo = tipoSeleccionado
            }
        }
        
        SwiftEventBus.onMainThread(self, name: "CotizacionSolucionesMedidaResumenCotizacionCustom-desseleccionPlan") {
            [weak self] result in
            self?.planSeleccionado = nil
            self?.actualizarDatos()
        }
        
        SwiftEventBus.onMainThread(self, name: "CotizacionSolucionesMedidaResumenCotizacionCustom-actualizar") {
            [weak self] result in
            self?.actualizarDatos()
        }
        
        SwiftEventBus.onMainThread(self, name: "CotizacionSolucionesMedidaResumenCotizacionCustom-cambioIdConfigPlan") {
            [weak self] result in
            if let configPlanId = result!.object as? Int{
                self?.configPlanId = configPlanId
                self?.actualizarDatos()
            }
        }
    }
    
    func viewDidAppear(){
    }
    
    func viewDidShow(){
        
    }
    
    func clickAgregarCarrito(){
        Logger.d("En click en el carrito, el tipo de soluciones a la medida a \(tipo)")
        switch tipo{
        case .Default:
            crearNuevoSitioConPlan()
        case .Agregar:
            agregarPlanASitio()
        case .Actualizar:
            actualizarPlanDeSitio()
        case .Eliminar:
            Logger.d("eliminar en sol medida.")
        }
    }
    
    func modeladoMasivo(){
        getSites()
    }
    
    func getSites(){
        service.getData(){[weak self] data in
            self?.onFinishGetData(data: data)
        }
    }
    
    func onFinishGetData( data: [DetalleSitiosModel]){
        if(data.count == 0) {
            self.view?.noSites()
        } else {
            self.view?.masiveModel()
        }
    }
    
    func crearNuevoSitioConPlan(){
        
        let closureagregar: ((UIAlertAction) -> Void) = { [weak self] _ in
            self?.informarTipo(tipo: .nuevo)
        }
        
        let closureMasivo: ((UIAlertAction) -> Void) = { [weak self] _ in
            self?.modeladoMasivo()
        }
        view?.msgAlert(
            titulo: "Agregar al carrito",
            mensaje: "¿Que acción desea realizar?",
            tituloBtn1: "Crear nuevo plan",
            tituloBtn2: "Modelar sitios existentes",
            closureAgregar: closureagregar,
            closureOmitir: closureMasivo
        )
    }
    
    func agregarPlanASitio(){
        informarTipo(tipo: .agregar)
    }
    
    func actualizarPlanDeSitio(){
        informarTipo(tipo: .actualizar)
    }
    
    private func informarTipo(tipo: Constantes.Enum.TipoEditar){
        if (seHaSeleccionadoUnPlan()){
            informarPlanSeleccionado()
            informarCambioTipo(tipo: tipo)
            avanzarPantalla()
        }
    }
    
    func informarPlanSeleccionado(){
        SwiftEventBus.post(
            "Cotizacion_DetalleSitio_DetalleSitios(cambioPlanSeleccionado)",
            sender: planSeleccionado
        )
    }
    
    func informarCambioTipo(tipo: Constantes.Enum.TipoEditar){
        SwiftEventBus.post(
            "Cotizacion_DetalleSitio_DetalleSitios(cambioTipoEdicion)",
            sender: tipo
        )
    }
    
    func seHaSeleccionadoUnPlan() -> Bool{
        if planSeleccionado == nil{
            view?.msgAlert(
                titulo: "Atención",
                mensaje: """
                    No hay ningun plan seleccionado.
                    Seleccione uno para continuar
                """
            )
            return false
        }
        return true
    }
    
    func avanzarPantalla(){
        SwiftEventBus.post("sitios-detalleSitios-informarTipoRegreso", sender: 1)
        SwiftEventBus.post("menuTabBar-cambiarMenuItem", sender: (2, 0))
    }
    
    func detachView() {
        view = nil
    }
    
    func getItemIndex(index: Int) -> ResumenCotizacionCustomModel?{
        if getSize() == 0 {
            return nil
        }
        return dataList[index]
    }
    
    func getSize() -> Int{
        return dataList.count
    }
    
    func actualizarDatos(){
        if let configPlanId = configPlanId{
            self.view?.startLoading()
            service.getDataFromDB(configPlanId){[weak self] data in
                self?.onFinishGetDataResumen(data: data)
            }
        }
    }
    
    func onFinishGetDataResumen( data: [ResumenCotizacionCustomModel]){
        view?.finishLoading()
        if (data.count == 0){
            dataList = [ResumenCotizacionCustomModel]()
        }else{
            dataList = data
        }
        view?.updateData()
    }
}
