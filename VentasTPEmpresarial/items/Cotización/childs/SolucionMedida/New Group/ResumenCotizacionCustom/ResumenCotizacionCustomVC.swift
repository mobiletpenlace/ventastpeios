//
//  resumenCotizacionCustomViewController.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 04/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class ResumenCotizacionCustomVC: BaseItemVC {
    @IBOutlet weak var labelRentaMensual: UILabel!
    @IBOutlet weak var labelCostoInstalacion: UILabel!
    @IBOutlet weak var labelAdicionales: UILabel!
    @IBOutlet weak var labelDescuento: UILabel!
    @IBOutlet weak var labelSubtotal: UILabel!
    @IBOutlet weak var labelImpuestos: UILabel!
    @IBOutlet weak var labelTotalCargosUnicos: UILabel!
    @IBOutlet weak var labelTotal: UILabel!
    fileprivate let presenter = ResumenCotizacionCustomPresenter(service: ResumenCotizacionCustomService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow() {
        super.viewDidShow()
        presenter.viewDidShow()
    }
    
    private func setUpView() {
        
    }
    
    @IBAction func clickSiguiente(_ sender: UIButton) {
        sender.animateBound(view: sender){ [unowned self] _ in
            self.presenter.clickAgregarCarrito()
        }
    }
    
    @IBAction func clickDescuentoSobreCotizacion(_ sender: UIButton) {
        sender.animateBound(view: sender)
    }
}


extension ResumenCotizacionCustomVC: ResumenCotizacionCustomDelegate, BaseFormatNumber, BaseAlertViewDelegate{
    
    func noSites() {
        super.showAlert(titulo: "Alerta", mensaje: "No hay sitios creados")
    }
    
    func masiveModel() {
        super.showDialog(customAlert: DialogoCargaPlanMasivaVC())
    }
    
    
    func updateData(){
        labelRentaMensual.text = getPrecio(0)
        labelCostoInstalacion.text = getPrecio(1)
        labelAdicionales.text = getPrecio(2)
        labelDescuento.text = getPrecio(3)
        labelSubtotal.text = getPrecio(4)
        labelImpuestos.text = getPrecio(5)
        labelTotalCargosUnicos.text = getPrecio(6)
        labelTotal.text = getPrecio(7)
    }
    
    private func getPrecio(_ index: Int) -> String{
        let precio = presenter.getItemIndex(index: index)?.precio ?? 0.0
        return "$" + numeroAFormatoPrecio(numero: precio)
    }
    
    func startLoading() {
        super.showLoading(view: self.view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
    func getPadreVC() -> BaseTreeVC? {
        return padreController
    }
    
    func updateTitleHeader() {
        
    }
    
    func msgAlert(titulo: String, mensaje: String){
        showAlert(titulo: titulo, mensaje: mensaje)
    }
    
    func msgAlert(titulo: String, mensaje: String, tituloBtn1: String, tituloBtn2: String, closureAgregar: @escaping ((UIAlertAction) -> Void), closureOmitir: @escaping ((UIAlertAction) -> Void)){
        alert(
            titulo,
            mensaje,
            self,
            tituloBtn1,
            closureAgregar,
            tituloBtn2,
            closureOmitir
        )
    }
    
}

