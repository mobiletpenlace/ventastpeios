//
//  InformacionPlanCustomViewController.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 05/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class InformacionPlanCustomVC: BaseItemVC {
    @IBOutlet weak var viewServicios: UIView!
    @IBOutlet weak var viewPromociones: UIView!
    var serviciosConf : ServiciosCustomVC?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.addViewXIB(vc: ServiciosCustomVC(), container: viewServicios)
        super.addViewXIB(vc: PromocionCustomVC(), container: viewPromociones)
    }
}
