//
//  PromocionCustomPresenter.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 14/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

protocol PromocionCustomDelegate: BaseDelegate {
    func updateData()
}

class PromocionCustomPresenter {
    fileprivate let model: PromocionCustomService
    weak fileprivate var view: PromocionCustomDelegate?
    fileprivate var dataList = [PromocionCustomModel]()
    
    init(model: PromocionCustomService){
        self.model = model
    }
    
    func attachView(view: PromocionCustomDelegate){
        self.view = view
        getData()
    }
    
    func viewDidAppear(){
        
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
    }
    
    func detachView() {
        view = nil
    }
    
    func getItemIndex(index: Int) -> PromocionCustomModel {
        return dataList[index]
    }
    
    func getSize() -> Int{
        return dataList.count
    }
    
    func getData(){
        self.view?.startLoading()
        model.getPromocionCustom{
            [weak self] data in
            self?.view?.finishLoading()
            if(data.count == 0){
                self?.dataList = [PromocionCustomModel]()
            }else{
                self?.dataList = data
            }
            self?.view?.updateData()
        }
    }
}

