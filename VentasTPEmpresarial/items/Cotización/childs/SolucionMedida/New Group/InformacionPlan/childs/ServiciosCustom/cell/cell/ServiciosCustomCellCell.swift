//
//  CustomServicioCell.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 06/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class ServiciosCustomCellCell: UITableViewCell {
    @IBOutlet weak var nombre: UILabel!
    @IBOutlet weak var precio: UILabel!
    @IBOutlet weak var cantidad: UILabel!
    @IBOutlet weak var imageViewAgregarServicio: UIImageView!
    @IBOutlet weak var imageViewRestarServicio: UIImageView!
    
    @IBOutlet weak var imageViewEditar: UIImageView!
    
    
    var posicion: Int = 0
    var posicionProducto: Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func clickAgregarservicio(_ sender: UIButton) {
        sender.animateBound(view: imageViewAgregarServicio)
        SwiftEventBus.post("SolucionesMedidaServiciosCustomCell-agregarServicio", sender: (self.posicion, self.posicionProducto))
    }
    
    @IBAction func clickRestarservicio(_ sender: UIButton) {
        sender.animateBound(view: imageViewRestarServicio)
        SwiftEventBus.post("SolucionesMedidaServiciosCustomCell-quitarServicio", sender: (self.posicion, self.posicionProducto))
    }
    
    @IBAction func clickEditarPrecio(_ sender: UIButton) {
        sender.animateBound(view: imageViewEditar){ _ in
            SwiftEventBus.post("SolucionesMedidaServiciosCustomCell-editarPrecio", sender: (self.posicion, self.posicionProducto))
        }
    }
    
}
