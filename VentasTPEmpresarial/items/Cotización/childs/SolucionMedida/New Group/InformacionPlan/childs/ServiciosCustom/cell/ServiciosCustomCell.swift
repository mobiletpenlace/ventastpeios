//
//  CustomPlanCollectionViewCell.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 05/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class ServiciosCustomCell: UICollectionViewCell {
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var viewTable: UIView!
    @IBOutlet weak var tableView: UITableView!
    var vc: ServiciosCustomCellCellHeader!
    var nombreCelda: String = "ServiciosCustomCellCell"
    fileprivate let presenter = ServiciosCustomCellPresenter(model: ServiciosCustomCellService())
    var loading: UIView?
    var padreViewController: UIViewController!
    var posicionAddon: Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        presenter.attachView(view: self)
        setUpView()
    }
    
    private func setUpView() {
        viewContainer.layer.borderColor = UIColor.lightGray.cgColor
        viewContainer.layer.borderWidth = 2
        viewContainer.layer.cornerRadius = 5
        iniciarTableView()
        vc = ServiciosCustomCellCellHeader(titulo: "")
        vc.view.layer.cornerRadius = 5
    }
    
    func updateTitle(text: String){
        vc.updateTitle(text: text)
    }
    
    private func iniciarTableView(){
        let nib = UINib.init(nibName: nombreCelda, bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: nombreCelda)
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    @IBAction func clickDeleteItem(_ sender: UIButton) {
        sender.animateShake(view: sender)
        SwiftEventBus.post(
            "CotizacionCustomPlanes-eliminarServicioPlan",
            sender: nil
        )
    }
    
    func setDataCell(posicion: Int, configPlanId: Int){
        presenter.setDataCell(posicion: posicion, configPlanId: configPlanId)
    }
    
}

extension ServiciosCustomCell: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
        return vc.view
    }
}

extension ServiciosCustomCell: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getSize()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: nombreCelda, for: indexPath) as! ServiciosCustomCellCell
        let data = presenter.getItemIndex(index: indexPath.row)
        cell.nombre.text = data.nombre
        cell.precio.text = "\(data.precio)"
        cell.cantidad.text = "\(data.cantidad)"
        cell.posicion = indexPath.row
        cell.posicionProducto = presenter.getPosicionProducto()
        cell.selectionStyle = .none //desactivar el color al seleccionar la celda
        return cell
    }
}

extension ServiciosCustomCell: ServiciosCustomCellDelegate, BaseTreeLoadingDelegate, BaseTreeDialogs, BaseBannerDelegate{
    
    func updateTextoLoading(texto: String) {
        
    }
   
    func updateData(){
        self.tableView.reloadData()
    }
    
    func updateTitleHeader() {
        
    }
    
    func startLoading() {
        loading = showLoading(onView: self)
    }
    
    func finishLoading() {
        if loading != nil{
            hideLoading(spinner: loading!)
        }
    }
    
    func editarPrecio(minimoPrecio: Float, posicionAddon: Int){
        self.posicionAddon = posicionAddon
        let dialogo = DialogEditaPrecioSolucionesMedidaVC(delegate: self, minimoPrecio: minimoPrecio)
        dialogo.cerrarConclickFuera = false
        SwiftEventBus.post("Header(mostrarDialogoCustom)", sender: dialogo)
    }
}


extension ServiciosCustomCell: CustomAlertViewDelegate{
    
    func clickOKButtonDialog(value: Any) {
        if let texto = value as? String{
            let actualPrecio: Float = Float(texto) ?? 0.0
            Logger.i("en la vista se recibió el valor del input: \(actualPrecio) para la posicion: \(posicionAddon)")
            presenter.seHaCambiadoElPrecio(actualPrecio: actualPrecio, posicionAddon: posicionAddon)
        }
    }
    
    func clickCancelButtonDialog() {
        
    }
    
    func viewDidShow() {
        
    }
}
