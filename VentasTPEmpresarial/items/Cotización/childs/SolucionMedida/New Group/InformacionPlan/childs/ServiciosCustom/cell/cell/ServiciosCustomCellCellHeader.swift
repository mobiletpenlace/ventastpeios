//
//  CustomServicioHeaderView.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 06/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class ServiciosCustomCellCellHeader: UIViewController {
    @IBOutlet weak var nombrePlan: UILabel!
    var titulo: String?
    
    init(titulo: String?) {
        self.titulo = titulo
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nombrePlan.text = titulo
    }
    
    func updateTitle(text: String){
        nombrePlan.text = text
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
