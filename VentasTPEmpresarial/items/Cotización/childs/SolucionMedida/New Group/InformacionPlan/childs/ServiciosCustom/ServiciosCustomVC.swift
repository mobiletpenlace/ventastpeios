//
//  ConfiguracionListViewController.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 05/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class ServiciosCustomVC: BaseItemVC {
    @IBOutlet weak var collectionView: UICollectionView!
    let customCell = "ServiciosCustomCell"
    var indexCurrent: IndexPath = IndexPath(row: 0, section: 0)
    fileprivate let presenter = ServiciosCustomPresenter(model: ServiciosCustomService())
    
    override func viewDidLoad(){
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow() {
        presenter.viewDidShow()
    }
    
    private func setUpView() {
        iniciarCollectionView()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        guard let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        flowLayout.invalidateLayout()
    }
    
    func iniciarCollectionView(){
        let nib = UINib.init(nibName: customCell, bundle: nil)
        self.collectionView.register(nib, forCellWithReuseIdentifier: customCell)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    
}

extension ServiciosCustomVC: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.getSize()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: customCell, for: indexPath) as! ServiciosCustomCell
        let data = presenter.getItemIndex(index: indexPath.row)
        cell.updateTitle(text: data.nombre)
        cell.setDataCell(posicion: data.pos, configPlanId: presenter.getConfigPlanId())
        cell.padreViewController = self
        return cell
    }
    
}

extension ServiciosCustomVC:UICollectionViewDelegate {
    
    func getCell(index: IndexPath) -> ServiciosCustomCell {
        return (self.collectionView.cellForItem(at: index) as? ServiciosCustomCell)!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //let cell = getCell(index: indexPath)
        //padreController?.cambiarVista(index: 1)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let x = scrollView.contentOffset.x
        let w = scrollView.bounds.size.width
        let currentPage = Int(ceil(x/w))
        indexCurrent = IndexPath(row: currentPage, section: 0)
    }
    
}

extension ServiciosCustomVC: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = collectionView.frame.size.width
        let h = collectionView.frame.size.height
        
        return CGSize(width: w, height: h)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}


extension ServiciosCustomVC: ServiciosCustomDelegate {
    
    func updateData(){
        self.collectionView.reloadData()
    }
    
    func updateTitleHeader() {
        
    }
    
    func startLoading() {
        
    }
    
    func finishLoading() {
        
    }

    func seleccionarUltimoItem(){
        let index = presenter.getSize()
        let indexPath = IndexPath(row: (index - 1), section: 0)
        collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)
    }
    
    func getItemPosicionVisible() -> IndexPath?{
        var visibleRect = CGRect()
        visibleRect.origin = collectionView.contentOffset
        visibleRect.size = collectionView.bounds.size
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        let visibleIndexPath: IndexPath = collectionView.indexPathForItem(at: visiblePoint)!
        return visibleIndexPath
    }
    
    func deleteItemCollection(indexPath: IndexPath){
        collectionView.deleteItems(at:[indexPath])
    }
    
    func scrollItemIndex(indexPath: IndexPath){
        if indexPath.row >= 1 {
            collectionView.scrollToItem(at: IndexPath.init(row: indexPath.row-1, section: 0), at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
        }
    }
}
