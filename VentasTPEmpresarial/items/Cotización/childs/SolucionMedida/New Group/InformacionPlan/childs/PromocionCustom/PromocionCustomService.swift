//
//  PromocionCustomService.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 14/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

class PromocionCustomService {
    
    func getPromocionCustom(_ callBack:@escaping ([PromocionCustomModel]) -> Void){
        let data = [
            PromocionCustomModel(nombre: "uno"),
            PromocionCustomModel(nombre: "dos"),
            PromocionCustomModel(nombre: "tres"),
            PromocionCustomModel(nombre: "cuatro"),
            ]
        let delayTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            callBack(data)
        }
    }
    
}
