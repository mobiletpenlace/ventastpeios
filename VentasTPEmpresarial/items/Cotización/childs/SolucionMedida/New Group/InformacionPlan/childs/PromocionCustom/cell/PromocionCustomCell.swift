//
//  PromocionCustomCell.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 06/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class PromocionCustomCell: UICollectionViewCell {
    @IBOutlet weak var viewContainer: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewContainer.layer.cornerRadius = 5
        // Initialization code
    }

}
