//
//  ServiciosCustomCellService.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 23/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import RealmSwift

class ServiciosCustomCellService: BaseDBManagerDelegate {
    var dbManager: DBManager
    var serverManager: ServerDataManager?
    var realm: Realm!
    
    init() {
        dbManager = DBManager.getInstance()
        realm = getRealm()
    }
    
    func getServiciosCustomCell(configPlanId: Int, posicion: Int, _ callBack: @escaping ([ServiciosCustomCellModel]) -> Void) {
        var data = [ServiciosCustomCellModel]()//crear lista de datos
        for prod in dbManager.obtenerConfigProductosAdicionalesAddons(configPlanId: configPlanId, posicion: posicion) {
            data.append(
                ServiciosCustomCellModel(
                    nombre: prod.addon.nombre,
                    precio: prod.precio,
                    cantidad: prod.cantidad
                )
            )
        }
        callBack(data)
    }
    
    func aumentarConfigProductosAdicionalesAddons(configPlanId: Int, posicion: Int, posicionAddon: Int){
        dbManager.aumentarConfigProductosAdicionalesAddons(configPlanId: configPlanId, posicion: posicion, posicionAddon: posicionAddon)
    }
    
    func disminuirConfigProductosAdicionalesAddons(configPlanId: Int, posicion: Int, posicionAddon: Int){
        dbManager.disminuirConfigProductosAdicionalesAddons(configPlanId: configPlanId, posicion: posicion, posicionAddon: posicionAddon)
    }
    
    func getConfigProductosAdicionalesAddonsMinimoPrecio(configPlanId: Int, posicion: Int, posicionAddon: Int) -> Float{
        if let addon = dbManager.getConfigProductosAdicionalesAddons(configPlanId: configPlanId, posicion: posicion, posicionAddon: posicionAddon){
            return addon.addon.precio
        }
        return 0.0
    }
    
    func editarPrecio(configPlanId: Int, posicion: Int, posicionAddon: Int, precio: Float){
        dbManager.editarPrecio(configPlanId: configPlanId, posicion: posicion, posicionAddon: posicionAddon, precio: precio)
    }
}
