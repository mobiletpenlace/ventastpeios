//
//  ServiciosCustomCellModel.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 23/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct ServiciosCustomCellModel {
    let nombre: String
    let precio: Float
    var cantidad: Int
}
