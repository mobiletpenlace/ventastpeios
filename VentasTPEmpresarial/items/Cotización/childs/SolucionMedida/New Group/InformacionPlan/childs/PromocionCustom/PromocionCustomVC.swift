//
//  ConfiguracionPromocionesViewController.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 06/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class PromocionCustomVC: BaseItemVC {
    @IBOutlet weak var collectionView: UICollectionView!
    fileprivate let presenter = PromocionCustomPresenter(model: PromocionCustomService())
    let customCell = "PromocionCustomCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow() {
        super.viewDidShow()
        presenter.viewDidShow()
    }
    
    private func setUpView() {
        iniciarCollectionView()
    }
    
    func iniciarCollectionView(){
        let nib = UINib.init(nibName: customCell, bundle: nil)
        self.collectionView.register(nib, forCellWithReuseIdentifier: customCell)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
}

extension PromocionCustomVC: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.getSize()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: customCell, for: indexPath) as! PromocionCustomCell
        return cell
    }
    
}

extension PromocionCustomVC:UICollectionViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
    }
}

extension PromocionCustomVC: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = collectionView.frame.size.width / 4
        let h = collectionView.frame.size.height
        return CGSize(width: w, height: h)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension PromocionCustomVC: PromocionCustomDelegate {
    
    func updateTitleHeader() {
        let header = HeaderData(
            imagen: #imageLiteral(resourceName: "icon_Comisiones"),
            titulo: "Comisiones",
            titulo2: nil,
            titulo3: nil,
            subtitulo: nil,
            closure:{ [weak self] in
                self?.padreController?.cambiarVista(index: 0)
            }
        )
        super.updateTitle(headerData: header)
    }
    
    func startLoading() {
        super.showLoading(view: self.view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
    func updateData() {
        self.collectionView.reloadData()
    }
}
