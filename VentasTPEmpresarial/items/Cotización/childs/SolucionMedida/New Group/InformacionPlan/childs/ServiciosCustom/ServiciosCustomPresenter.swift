//
//  ServiciosCustomPresenter.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 23/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol ServiciosCustomDelegate: BaseDelegate {
    func updateData()
    func seleccionarUltimoItem()
    func getItemPosicionVisible() -> IndexPath?
    func deleteItemCollection(indexPath: IndexPath)
    func scrollItemIndex(indexPath: IndexPath)
}

class ServiciosCustomPresenter {
    fileprivate let model: ServiciosCustomService
    weak fileprivate var view: ServiciosCustomDelegate?
    fileprivate var dataList = [ServiciosCustomModel]()
    var configPlanId: Int?
    var servicioId: String = ""
    
    init(model: ServiciosCustomService){
        self.model = model
    }
    
    func attachView(view: ServiciosCustomDelegate){
        self.view = view
        
        SwiftEventBus.onMainThread(self, name: "CotizacionCustomPlanes-actualizarconfigPlanId") { [weak self] result in
            if let configPlanId = result!.object as? Int{
                self?.configPlanId = configPlanId
                self?.getData()
            }
        }
        
        SwiftEventBus.onMainThread(self, name: "CotizacionCustomPlanes-agregarServicioPlan") { [weak self] result in
            if let (configPlanId, servicioId) = result!.object as? (Int, String){
                self?.agregarProductoAdicional(configPlanId: configPlanId, servicioId: servicioId)
            }
        }
        SwiftEventBus.onMainThread(self, name: "CotizacionCustomPlanes-eliminarServicioPlan") { [weak self] result in
            self?.eliminarServicioAdicional()
        }
    }
    
    func viewDidAppear(){
        
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
    }
    
    func detachView() {
        view = nil
    }
    
    func getItemIndex(index: Int) -> ServiciosCustomModel {
        return dataList[index]
    }
    
    func getSize() -> Int{
        return dataList.count
    }
    
    func getConfigPlanId() -> Int{
        return configPlanId!
    }
    
    func agregarProductoAdicional(configPlanId: Int, servicioId: String){
        self.configPlanId = configPlanId
        self.servicioId = servicioId
        model.agregarProductoAdicional(configPlanId: configPlanId, servicioId: servicioId)
        getData()
        notificarResumenCobertura()
        view?.scrollItemIndex(indexPath: IndexPath(row: dataList.count, section: 0))
    }
    
    func eliminarServicioAdicional(){
        if let visibleIndexPath = view?.getItemPosicionVisible(){
            if let configPlanId = configPlanId{
                model.eliminarProductosAdicionalConfig(configPlanId: configPlanId, posicion: visibleIndexPath.row)
            }
            getData()
            notificarResumenCobertura()
            view?.scrollItemIndex(indexPath: visibleIndexPath)
        }
    }
    
    func notificarResumenCobertura(){
        SwiftEventBus.post(
            "CotizacionSolucionesMedidaResumenCotizacionCustom-actualizar",
            sender: nil
        )
    }
    
    func getData(){
        self.view?.startLoading()
        if let configPlanId = configPlanId{
            model.getData(configPlanId: configPlanId){
                [weak self] data in
                self?.view?.finishLoading()
                if(data.count == 0){
                   self?.dataList = [ServiciosCustomModel]()
                }else{
                    self?.dataList = data
                }
                self?.view?.updateData()
            }
        }
    }
}
