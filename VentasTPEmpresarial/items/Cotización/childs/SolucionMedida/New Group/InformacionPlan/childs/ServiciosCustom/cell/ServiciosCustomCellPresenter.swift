//
//  ServiciosCustomCellPresenter.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 23/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

struct ServiciosCustomCellData{
    let nombre: String
    let precio: String
}

protocol ServiciosCustomCellDelegate: BaseDelegate {
    func updateData()
    func editarPrecio(minimoPrecio: Float, posicionAddon: Int)
}

class ServiciosCustomCellPresenter {
    fileprivate let model: ServiciosCustomCellService
    weak fileprivate var view: ServiciosCustomCellDelegate?
    fileprivate var dataList = [ServiciosCustomCellModel]()
    var configPlanId: Int?
    var posicion: Int?
    
    init(model: ServiciosCustomCellService){
        self.model = model
        SwiftEventBus.onMainThread(self, name: "SolucionesMedidaServiciosCustomCell-agregarServicio") {[weak self] result in
            if let (posicion, posicionProducto) = result!.object as? (Int, Int){
                if posicionProducto == self?.posicion{
                    self?.aumentarAddon(posicionAddon: posicion)
                }
            }
        }
        
        SwiftEventBus.onMainThread(self, name: "SolucionesMedidaServiciosCustomCell-quitarServicio") {[weak self] result in
            if let (posicion, posicionProducto) = result!.object as? (Int, Int){
                if posicionProducto == self?.posicion{
                    self?.disminuirAddons(posicionAddon: posicion)
                }
            }
        }
        
        SwiftEventBus.onMainThread(self, name: "SolucionesMedidaServiciosCustomCell-editarPrecio") {[weak self] result in
            if let (posicion, posicionProducto) = result!.object as? (Int, Int){
                if posicionProducto == self?.posicion{
                    self?.editarPrecio(posicionAddon: posicion)
                }
            }
        }
    }
    
    func editarPrecio(posicionAddon: Int){
        if let configPlanId = configPlanId{
            //view?.editarPrecio(configPlanId: configPlanId, posicion: posicion ?? 0, posicionAddon: posicionAddon)
            let precioMinimo = model.getConfigProductosAdicionalesAddonsMinimoPrecio(
                configPlanId: configPlanId,
                posicion: posicion ?? 0,
                posicionAddon: posicionAddon
            )
            view?.editarPrecio(minimoPrecio: precioMinimo, posicionAddon: posicionAddon)
            getData()
            notificarResumenCobertura()
        }
    }
    
    func seHaCambiadoElPrecio(actualPrecio: Float, posicionAddon: Int){
        if let configPlanId = configPlanId{
            model.editarPrecio(
                configPlanId: configPlanId,
                posicion: posicion ?? 0,
                posicionAddon: posicionAddon,
                precio: actualPrecio
            )
            getData()
            notificarResumenCobertura()
        }
    }
    
    func aumentarAddon(posicionAddon: Int){
        if let configPlanId = configPlanId{
            model.aumentarConfigProductosAdicionalesAddons(configPlanId: configPlanId, posicion: posicion ?? 0, posicionAddon: posicionAddon)
            getData()
            notificarResumenCobertura()
        }
    }
    
    func disminuirAddons(posicionAddon: Int){
        if let configPlanId = configPlanId{
            model.disminuirConfigProductosAdicionalesAddons(configPlanId: configPlanId, posicion: posicion ?? 0, posicionAddon: posicionAddon)
            getData()
            notificarResumenCobertura()
        }
    }
    
    func getPosicionProducto() -> Int{
        return posicion ?? 0
    }
    
    func notificarResumenCobertura(){
        SwiftEventBus.post(
            "CotizacionSolucionesMedidaResumenCotizacionCustom-actualizar",
            sender: nil
        )
    }
    
    func attachView(view: ServiciosCustomCellDelegate){
        self.view = view
    }
    
    func viewDidAppear(){
        
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
    }
    
    func detachView() {
        view = nil
    }
    
    func getItemIndex(index: Int) -> ServiciosCustomCellModel {
        return dataList[index]
    }
    
    func getSize() -> Int{
        return dataList.count
    }
    
    func setDataCell(posicion: Int, configPlanId: Int){
        self.configPlanId = configPlanId
        self.posicion = posicion
        Logger.d("se llamado a setDataCell con posicion:\(posicion) y configPlanId: \(configPlanId)")
        getData()
    }
    
    func getData(){
        self.view?.startLoading()
        if let configPlanId = configPlanId{
            model.getServiciosCustomCell(configPlanId: configPlanId, posicion: posicion!){
                [weak self] data in
                self?.view?.finishLoading()
                if(data.count == 0){
                    self?.dataList = [ServiciosCustomCellModel]()
                }else{
                    self?.dataList = data
                }
                self?.view?.updateData()
            }
        }
    }
}
