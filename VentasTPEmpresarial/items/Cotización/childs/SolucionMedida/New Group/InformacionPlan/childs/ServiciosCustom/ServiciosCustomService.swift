//
//  ServiciosCustomService.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 23/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import RealmSwift

class ServiciosCustomService: BaseDBManagerDelegate {
    var realm: Realm!
    var serverManager: ServerDataManager?
    var dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()
        realm = getRealm()
    }
    
    func getData(configPlanId: Int, _ callBack: @escaping ([ServiciosCustomModel]) -> Void) {
        var data = [ServiciosCustomModel]()//crear lista de datos
        for prod in dbManager.obtenerConfigProductosAdicionales(configPlanId: configPlanId){
            data.append(ServiciosCustomModel(nombre: prod.productoAdicional.nombre, pos: 0))
        }
        let size = data.count
        for i in 0..<size{
            data[i].pos = i
        }
        callBack(data)
    }
    
    func agregarProductoAdicional(configPlanId: Int, servicioId: String){
        dbManager.agregarProductosAdicionalConfig(configPlanId: configPlanId, servicioId: servicioId)
    }
    
    func eliminarProductosAdicionalConfig(configPlanId: Int, posicion: Int){
        dbManager.eliminarProductosAdicionalConfig(configPlanId: configPlanId, posicion: posicion)
    }
}
