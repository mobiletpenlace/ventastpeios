//
//  CollectionViewController.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 26/06/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class SeleccionarPlanesVC: BaseItemVC {
    @IBOutlet weak var collectionView: UICollectionView!
    var planes = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        iniciarCollectionView()
        planes = getPlanes()
    }
    
    func iniciarCollectionView() -> Void {
        let nib = UINib.init(nibName: "PlanCell", bundle: nil)
        self.collectionView.register(nib, forCellWithReuseIdentifier: "PlanCell")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        super.addShadowView(view: self.collectionView)
    }
}

extension SeleccionarPlanesVC{
    
    func getPlanes() -> [String] {
        return [
            "Conexión Analógica",
            "Conexión SIP sin PBX",
            "Conexión SIP sin PBX Panasonic",
            "Conexión SIP sin PBX AVAYA ",
            "Conexión SIP sin PBX Enlace",
            "PBX Cloud",
            "Superinternet plus (Asimétrico)",
            "Superinternet Avanzado",
            "Superinternet Seguro Huawei",
            "Superinternet Seguro Fortinet",
            "Oferta Especial 10 Mbps",
            "Superinternet 3/1",
            "Soluciones a la medida",
            "TEXT",
            "TEXT",
            "TEXT",
            "TEXT",
            "TEXT",
            "TEXT",
            "TEXT",
            "TEXT",
            "TEXT",
            "TEXT",
            "TEXT",
            "TEXT",
            "TEXT",
            "TEXT",
            "TEXT",
        ]
    }
}

extension SeleccionarPlanesVC: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return planes.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlanCell", for: indexPath) as! PlanCell
        cell.displayContent(texto: planes[indexPath.row])
        return cell
    }
}

extension SeleccionarPlanesVC:UICollectionViewDelegate {
    
    func getCell(index: IndexPath) -> PlanCell {
        return (self.collectionView.cellForItem(at: index) as? PlanCell)!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = getCell(index: indexPath)
        cell.animateBound(view: cell)
        SwiftEventBus.post(
            "CotizacionPlanes-cambioPlan",
            sender: Plan(nombre: planes[indexPath.row])
        )
    }
}

extension SeleccionarPlanesVC: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = (collectionView.frame.size.width / 7) 
        //let h = (collectionView.frame.size.height / 3) - 20
        
        return CGSize(width: w, height: 90)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}
