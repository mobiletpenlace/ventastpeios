//
//  PlanCell.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 22/06/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class SeleccionarPlanesCell: UICollectionViewCell {
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var iconoTriangulo: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        isSelected = false
    }
    
    override var isSelected: Bool {
        /*se cambia el color de la celda que ha sido seleccionada*/
        didSet {
            if isSelected {
                label1.textColor = .white
                self.backgroundColor  = Colores.azulOscuro
            } else {
                self.backgroundColor  = UIColor.white
                label1.textColor = Colores.azulOscuro
            }
        }
    }
    
    func displayContent(texto: String) -> Void {
        label1.text = texto
    }
}
