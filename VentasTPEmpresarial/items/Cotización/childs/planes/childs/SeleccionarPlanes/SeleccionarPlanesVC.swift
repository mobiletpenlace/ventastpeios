//
//  CollectionViewController.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 26/06/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class SeleccionarPlanesVC: BaseItemVC {
    @IBOutlet weak var collectionView: UICollectionView!
    let nombreCelda = "SeleccionarPlanesCell"
    fileprivate let presenter = SeleccionarPlanesPresenter(service: SeleccionarPlanesService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow() {
        super.viewDidShow()
        presenter.viewDidShow()
    }
    
    private func setUpView() {
        iniciarCollectionView()
        super.addShadowView(view: self.collectionView)
    }
    
    func iniciarCollectionView() -> Void {
        let nib = UINib.init(nibName: nombreCelda, bundle: nil)
        self.collectionView.register(nib, forCellWithReuseIdentifier: nombreCelda)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
}

extension SeleccionarPlanesVC: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.getSize()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: nombreCelda, for: indexPath) as! SeleccionarPlanesCell
        let data = presenter.getItemIndex(index: indexPath.row)
        cell.displayContent(texto: data.nombre)
        return cell
    }
}

extension SeleccionarPlanesVC:UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = self.collectionView.cellForItem(at: indexPath) as? SeleccionarPlanesCell{
            cell.animateBound(view: cell)
            presenter.clickCell(index: indexPath.row)
        }
    }
}

extension SeleccionarPlanesVC: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = (collectionView.frame.size.width / 6)
        return CGSize(width: w, height: 90)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}

extension SeleccionarPlanesVC: SeleccionarPlanesDelegate{
    
    func updateCollection(){
        self.collectionView.reloadData()
    }
    
    func setEmptyProgress() {
        
    }
    
    func startLoading() {
        super.showLoading(view: self.view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
    func updateTitleHeader() {
        
    }
    
    func regresarAFamilias(){
        padreController?.padreController?.cambiarVista(index: 0)
    }
}

