//
//  SeleccionarPlanesModel.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 04/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct SeleccionarPlanesModel {
    let id: String
    let nombre: String
}
