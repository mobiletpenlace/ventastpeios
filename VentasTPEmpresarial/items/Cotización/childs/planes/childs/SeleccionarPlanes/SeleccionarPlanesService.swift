//
//  SeleccionarPlanesService.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 04/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation


class SeleccionarPlanesService{
    
    func getData(idFamilia: String, _ callBack:@escaping ([SeleccionarPlanesModel]) -> Void){
        //getDataFromService(idFamilia, callBack)
        getDataFromDB(idFamilia, callBack)
    }
    
    func getDataFromService(_ idFamilia: String, _ callBack:@escaping ([SeleccionarPlanesModel]) -> Void) {
        let data = [
            SeleccionarPlanesModel(id: "a1", nombre: "Conexión Analógica"),
            SeleccionarPlanesModel(id: "a2", nombre: "Conexión SIP sin PBX"),
            SeleccionarPlanesModel(id: "a3", nombre: "Conexión SIP sin PBX Panasonic"),
            SeleccionarPlanesModel(id: "a4", nombre: "Conexión SIP sin PBX AVAYA "),
            SeleccionarPlanesModel(id: "a5", nombre: "Conexión SIP sin PBX Enlace"),
            SeleccionarPlanesModel(id: "a6", nombre: "PBX Cloud"),
            SeleccionarPlanesModel(id: "a7", nombre: "Superinternet plus (Asimétrico)"),
            SeleccionarPlanesModel(id: "a8", nombre: "Superinternet Avanzado"),
            SeleccionarPlanesModel(id: "a9", nombre: "Superinternet Seguro Huawei"),
            SeleccionarPlanesModel(id: "a10", nombre: "Superinternet Seguro Fortinet"),
            SeleccionarPlanesModel(id: "a11", nombre: "Oferta Especial 10 Mbps"),
            SeleccionarPlanesModel(id: "a12", nombre: "Superinternet 3/1"),
            SeleccionarPlanesModel(id: "a13", nombre: "Soluciones a la medida"),
            SeleccionarPlanesModel(id: "a14", nombre: "TEXT"),
        ]
        let delayTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            callBack(data)
        }
    }
    
    func getDataFromDB(_ idFamilia: String, _ callBack:@escaping ([SeleccionarPlanesModel]) -> Void) {
        var data = [SeleccionarPlanesModel]()//crear lista de datos
        let db = DBManager.getInstance()//obtener administrador de bd
        if let planes = db.obtenerPlanesPorFamilia(id: idFamilia){
            for plan in planes{
                data.append(
                    SeleccionarPlanesModel(
                        id: plan.id!,
                        nombre: plan.nombre!
                    )
                )
            }
        }
        callBack(data)
    }
    
    func crearPlanConfig(_ idPlan: String) -> Int?{
        let dbManager = DBManager.getInstance()//obtener administrador de bd
        if let configPlan = dbManager.crearConfigPlan(idPlan: idPlan){
            return configPlan.id
        }
        return nil
    }
}
