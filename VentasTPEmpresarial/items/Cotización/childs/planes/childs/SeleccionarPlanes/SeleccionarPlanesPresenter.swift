//
//  SeleccionarPlanesPresenter.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 04/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol SeleccionarPlanesDelegate: BaseDelegate {
    func updateCollection()
    func setEmptyProgress()
    func regresarAFamilias()
}

class SeleccionarPlanesPresenter{
    fileprivate let service: SeleccionarPlanesService
    weak fileprivate var view: SeleccionarPlanesDelegate?
    fileprivate var dataList = [SeleccionarPlanesModel]()
    var idFamiliaSeleccionada: String = ""
    
    
    init(service: SeleccionarPlanesService){
        self.service = service
    }
    
    func attachView(view: SeleccionarPlanesDelegate){
        self.view = view
        
        SwiftEventBus.onMainThread(self, name: "CotizacionPlanesSeleccionarPlan-cambiarFamilias") {
            [weak self] result in
            if let idFamilia = result!.object as? String{
                self?.cambioFamiliaSeleccionada(idFamilia: idFamilia)
            }
        }
    }
    
    func viewDidAppear(){
        
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
        
        actualizarDatos()
        
//        if idFamiliaSeleccionada == "" {
//            view?.regresarAFamilias()
//        }else{
//            actualizarDatos()
//        }
    }
    
    func detachView() {
        view = nil
    }
    
    func getItemIndex(index: Int) -> SeleccionarPlanesModel {
        return dataList[index]
    }
    
    func getSize() -> Int{
        return dataList.count
    }
    
    func cambioFamiliaSeleccionada(idFamilia: String){
        idFamiliaSeleccionada = idFamilia
    }
    
    func clickCell(index: Int){
        informarCambioPlan(plan: dataList[index])
        
        let plan = dataList[index]
        if let configPlanId = service.crearPlanConfig(plan.id){
            informarPlanConfigId(configPlanId)
        }
    }
    
    private func informarPlanConfigId(_ configPlanId: Int){
        SwiftEventBus.post(
            "CotizacionPlanesServiciosAdicionales-cambioIdConfigPlan",
            sender: configPlanId
        )
        SwiftEventBus.post(
            "CotizacionPlanesResumenCotizacion-cambioIdConfigPlan",
            sender: configPlanId
        )
        SwiftEventBus.post(
            "CotizacionPlanesServiciosAdicionalesProductosAddons-cambioIdConfigPlan",
            sender: configPlanId
        )
    }
    
    private func informarCambioPlan(plan: SeleccionarPlanesModel?){
        SwiftEventBus.post(
            "CotizacionPlanes-cambioPlan",
            sender: plan
        )
        SwiftEventBus.post("CotizacionPlanesResumenCotizacion-cambioPlan",
                           sender: plan
        )
        SwiftEventBus.post("CotizacionPlanesInformacionPlan-cambioPlan",
                           sender: plan
        )
        SwiftEventBus.post("CotizacionPlanesCostos-cambioPlan",
                           sender: plan
        )
        SwiftEventBus.post("CotizacionPlanesServiciosAdicionales-cambioPlan",
                           sender: plan
        )
        SwiftEventBus.post("CotizacionPlanesServiciosAdicionalesProductos-cambioPlan",
                           sender: plan
        )
        SwiftEventBus.post("CotizacionPlanesServiciosAdicionalesPromociones-cambioPlan",
                           sender: plan
        )
    }
    
    private func informarPlanSinSeleccionar(){
        SwiftEventBus.post(
            "CotizacionPlanesResumenCotizacion-desseleccionPlan",
            sender: nil
        )
        SwiftEventBus.post(
            "CotizacionPlanesInformacionPlan-desseleccionPlan",
            sender: nil
        )
        SwiftEventBus.post(
            "CotizacionPlanesCostos-desseleccionPlan",
            sender: nil
        )
        SwiftEventBus.post(
            "CotizacionPlanesServiciosAdicionales-desseleccionPlan",
            sender: nil
        )
        SwiftEventBus.post(
            "CotizacionPlanesServiciosAdicionalesProductos-desseleccionPlan",
            sender: nil
        )
        SwiftEventBus.post(
            "CotizacionPlanesServiciosAdicionalesPromociones-desseleccionPlan",
            sender: nil
        )
    }
    
    func actualizarDatos(){
        self.view?.startLoading()
        service.getData(idFamilia: idFamiliaSeleccionada){[weak self] data in
            self?.onFinishGetData(data: data)
        }
    }
    
    func onFinishGetData( data: [SeleccionarPlanesModel]){
        view?.finishLoading()
        informarPlanSinSeleccionar()
        if(data.count == 0){
            dataList = [SeleccionarPlanesModel]()
            view?.updateCollection()
        }else{
            dataList = data
            view?.updateCollection()
        }
    }
}
