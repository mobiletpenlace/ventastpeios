//
//  InformacionPlanCell.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 14/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class InformacionPlanCell: UICollectionViewCell {
    @IBOutlet weak var labelTituloPlan: UILabel!
    @IBOutlet weak var labelInformacionPlan: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
