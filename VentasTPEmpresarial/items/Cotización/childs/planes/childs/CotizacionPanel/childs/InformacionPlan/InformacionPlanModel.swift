//
//  InformacionPlanModel.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 16/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import Foundation

struct InformacionPlanModel {
    var labelTituloPlan: String
    var labelInformacionPlan: String
}
