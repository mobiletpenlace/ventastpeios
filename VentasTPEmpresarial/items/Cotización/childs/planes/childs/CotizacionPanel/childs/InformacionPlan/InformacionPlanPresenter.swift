//
//  InformacionPlanPresenter.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 16/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol InformacionPlanDelegate: BaseDelegate {
    func updateCollection()
}

class InformacionPlanPresenter{
    fileprivate let service: InformacionPlanService
    weak fileprivate var view: InformacionPlanDelegate?
    fileprivate var dataList = [InformacionPlanModel]()
    var planSeleccionado: SeleccionarPlanesModel?
    
    init(service: InformacionPlanService){
        self.service = service
    }
    
    func attachview(view:InformacionPlanDelegate){
        self.view = view
        
        SwiftEventBus.onMainThread(self, name: "CotizacionPlanesInformacionPlan-cambioPlan") {
            [weak self] result in
            if let plan = result!.object as? SeleccionarPlanesModel{
                self?.planSeleccionado = plan
                self?.actualizarDatos()
            }
        }
        
        SwiftEventBus.onMainThread(self, name: "CotizacionPlanesInformacionPlan-desseleccionPlan") {
            [weak self] result in
            self?.planSeleccionado = nil
            self?.actualizarDatos()
        }
    }
    
    func viewDidAppear(){
        
    }
    
    func viewDidShow(){
        
    }
    
    func datachView(){
        view = nil
    }
    
    func getItemIndex(index: Int) -> InformacionPlanModel{
        return dataList[index]
    }
    
    func getSize() -> Int{
        return dataList.count
    }
    
    func actualizarDatos(){
        self.view?.startLoading()
        let planID = planSeleccionado?.id ?? ""
        service.getData(idPlan: planID){[weak self] data in
            self?.onFinishGetData(data: data)
        }
    }
    
    func onFinishGetData( data: [InformacionPlanModel]){
        view?.finishLoading()
        if (data.count == 0){
            dataList = [InformacionPlanModel]()
            view?.updateCollection()
        }else{
            dataList = data
            view?.updateCollection()
        }
        
    }
    
}
