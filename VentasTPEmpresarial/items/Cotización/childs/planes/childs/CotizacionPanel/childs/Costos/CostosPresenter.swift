//
//  CostosPresenter.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 24/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol CostosDelegate: BaseDelegate {
    func updateData()
    func setEmptyProgress()
}

class CostosPresenter{
    fileprivate let service: CostosService
    weak fileprivate var view: CostosDelegate?
    fileprivate var dataList = [CostosModel]()
    var planSeleccionado: SeleccionarPlanesModel?
    
    init(service: CostosService){
        self.service = service
    }
    
    func attachview(view:CostosDelegate){
        self.view = view
        
        SwiftEventBus.onMainThread(self, name: "CotizacionPlanesCostos-cambioPlan") {
            [weak self] result in
            if let plan = result!.object as? SeleccionarPlanesModel{
                self?.planSeleccionado = plan
                self?.actualizarDatos()
            }
        }
        
        SwiftEventBus.onMainThread(self, name: "CotizacionPlanesCostos-desseleccionPlan") {
            [weak self] result in
            self?.planSeleccionado = nil
            self?.actualizarDatos()
        }
    }
    
    func viewDidAppear(){
        
    }
    
    func viewDidShow(){
        
    }
    
    func datachView(){
        view = nil
    }
    
    func getItemIndex(index: Int) -> CostosModel?{
        if getSize() == 0 {
            return nil
        }
        return dataList[index]
    }
    
    func getSize() -> Int{
        return dataList.count
    }
    
    func actualizarDatos(){
        self.view?.startLoading()
        let planID = planSeleccionado?.id ?? ""
        service.getData(idPlan: planID){[weak self] data in
            self?.onFinishGetData(data: data)
        }
    }
    
    func onFinishGetData( data: [CostosModel]){
        view?.finishLoading()
        if (data.count == 0){
            dataList = [CostosModel]()
        }else{
            dataList = data
        }
        view?.updateData()
    }
}
