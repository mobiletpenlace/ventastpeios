//
//  InformacionPlanService.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 16/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import RealmSwift

class InformacionPlanService: BaseDBManagerDelegate {
    var realm: Realm!
    let serverManager: ServerDataManager?
    var dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()
        serverManager = ServerDataManager()
        realm = getRealm()
    }
    
    
    func getData(idPlan: String, _ callBack:@escaping ([InformacionPlanModel]) -> Void){
        //getDataFromService(idPlan, callBack)
        getDataFromDB(idPlan, callBack)
    }
    
//    func getDataFromService(_ idPlan: String, _ callBack:@escaping ([InformacionPlanModel]) -> Void) {
//        let model = RequestProductoServicios(idPlan: idPlan)
//        serverManager.post(model: model, url: Constantes.URL_PLANES_DETALLE){ responseData in
//            var lista = [InformacionPlanModel]()
//            do{
//                let decoder = JSONDecoder()
//                let responseOBJ = try decoder.decode(ResponseProductoServicio.self, from: data!)
//                if let valoresAgregados = responseOBJ.objectResponse?.valoresAgregados{
//                    for valor in valoresAgregados{
//                        let nombre = valor.name ?? ""
//                        if let index = nombre.range(of: " "){
//                            let titulo = nombre[..<index.lowerBound].description
//                            let texto = nombre[index.upperBound...].description
//                            lista.append(
//                                InformacionPlanModel(labelTituloPlan: titulo, labelInformacionPlan: texto)
//                            )
//                        }
//                    }
//                }
//            }catch let err{
//                print(err)
//            }
//            callBack(lista)
//        }
//    }
    
    func getDataFromDB(_ idPlan: String, _ callBack:@escaping ([InformacionPlanModel]) -> Void) {
        var data = [InformacionPlanModel]()//crear lista de datos
        let db = DBManager.getInstance()//obtener administrador de bd
        
        if let plan = db.buscarPlanPorID(id: idPlan){
            let serviciosAdicionales = plan.productosIncluidos
            for servicio in serviciosAdicionales{
                data.append(
                    InformacionPlanModel(
                        labelTituloPlan: servicio.nombre,
                        labelInformacionPlan: servicio.descripcion
                    )
                )
            }
        }
        callBack(data)
    }
}
