//
//  InformacionPlanViewController.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 26/06/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class InformacionPlanVC: BaseItemVC {
    
    @IBOutlet weak var collectionView: UICollectionView!
    let nombreCelda = "InformacionPlanCell"
    fileprivate let presenter = InformacionPlanPresenter(service: InformacionPlanService())
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachview(view: self)
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow() {
        super.viewDidShow()
        presenter.viewDidShow()
    }
    
    private func setUpView(){
        iniciarCollectionView()
        super.addShadowView(view: self.collectionView)
    }
    
    
    func iniciarCollectionView() -> Void {
        let nib = UINib.init(nibName: nombreCelda, bundle: nil)
        self.collectionView.register(nib, forCellWithReuseIdentifier: nombreCelda)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    
}

extension InformacionPlanVC: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.getSize()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: nombreCelda, for: indexPath) as! InformacionPlanCell
        let data = presenter.getItemIndex(index: indexPath.row)
        cell.labelTituloPlan.text = data.labelTituloPlan
        cell.labelInformacionPlan.text = data.labelInformacionPlan
        return cell
    }
}

extension InformacionPlanVC: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}

extension InformacionPlanVC: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let h = (collectionView.frame.size.height / 2) - 5
        return CGSize(width: 140, height: h)
    }
    
    func collectionView(_ collectionview: UICollectionView, layoutcollectionViewLayout: UICollectionViewLayout, insertForSectionAt section: Int) -> UIEdgeInsets{
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layoutcollectionViewLayout: UICollectionView,minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, layoutCollectionViewLayout: UICollectionViewLayout, minimumInteritenSpacingForSectionAt section: Int)  -> CGFloat{
        return 10
    }
}

extension InformacionPlanVC: InformacionPlanDelegate{
    
    func updateCollection(){
        self.collectionView.reloadData()
    }
    
    func startLoading() {
        super.showLoading(view: self.view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
    func updateTitleHeader() {
        
    }
    
}
