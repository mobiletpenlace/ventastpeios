//
//  CostosService.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 24/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation


class CostosService: BaseFormatNumber {
    
    func getData(idPlan: String, _ callBack:@escaping ([CostosModel]) -> Void){
        //getDataFromService(idPlan, callBack)
        getDataFromDB(idPlan, callBack)
    }
    
    func getDataFromService(_ idPlan: String, _ callBack:@escaping ([CostosModel]) -> Void) {
        let data = [
            CostosModel(titulo: "Costo de instalación", precio: "$23,899.00"),
            CostosModel(titulo: "Renta Pronto Pago s/i", precio: "$20,600.00"),
            CostosModel(titulo: "Renta Pronto Pago IVA/ IEPS", precio: "$22,899.00"),
            CostosModel(titulo: "Renta Precio de Lista s/i", precio: "$21,899.00"),
            CostosModel(titulo: "Renta Precio de Lista IVA/ IEPS", precio: "$23,899.00"),
            ]
        let delayTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            callBack(data)
        }
    }
    
    func getDataFromDB(_ idPlan: String, _ callBack:@escaping ([CostosModel]) -> Void) {
        var data = [CostosModel]()//crear lista de datos
        let db = DBManager.getInstance()//obtener administrador de bd
        if let plan = db.buscarPlanPorID(id: idPlan){
            data.append(
                CostosModel(titulo: "Costo de instalación", precio: "$ \(numeroAFormatoPrecio(numero: plan.costoInstalacion))")
            )
            data.append(
                CostosModel(titulo: "Renta Pronto Pago s/i", precio: "$ \(numeroAFormatoPrecio(numero: plan.costoRentaProntoPago))")
            )
            data.append(
                CostosModel(titulo: "Renta Pronto Pago IVA/ IEPS", precio: "$ \(numeroAFormatoPrecio(numero: plan.costoRentaProntoPagoIVA))")
            )
            data.append(
                CostosModel(titulo: "Renta Precio de Lista s/i", precio: "$ \(numeroAFormatoPrecio(numero: plan.costoRentaPrecioLista))")
            )
            data.append(
                CostosModel(titulo: "Renta Precio de Lista IVA/ IEPS", precio: "$ \(numeroAFormatoPrecio(numero: plan.costoRentaPrecioListaIVA))")
            )
        }
        callBack(data)
    }
}
