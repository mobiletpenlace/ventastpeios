//
//  CostosViewController.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 26/06/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class CostosVC: BaseItemVC {
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var view5: UIView!
    @IBOutlet weak var costInstallationLabel: UILabel!
    @IBOutlet weak var rentaProntoPagoLabel: UILabel!
    @IBOutlet weak var rentaPrecioListaLabel: UILabel!
    @IBOutlet weak var retaProntoPagoIVALabel: UILabel!
    @IBOutlet weak var rentaPrecioListaIVALabel: UILabel!
    fileprivate let presenter = CostosPresenter(service: CostosService())
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachview(view: self)
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow() {
        super.viewDidShow()
        presenter.viewDidShow()
    }
    
    private func setUpView(){
        agregarBordes()
    }
    
    func agregarBordes() -> Void {
        super.addBorder(view: view1)
        super.addBorder(view: view2)
        super.addBorder(view: view3)
        super.addBorder(view: view4)
        super.addBorder(view: view5)
    }
}

extension CostosVC: CostosDelegate{
    func updateData(){
        costInstallationLabel.text = presenter.getItemIndex(index: 0)?.precio ?? "$ 0.0"
        rentaProntoPagoLabel.text = presenter.getItemIndex(index: 1)?.precio ?? "$ 0.0"
        rentaPrecioListaLabel.text = presenter.getItemIndex(index: 3)?.precio ?? "$ 0.0"
        retaProntoPagoIVALabel.text = presenter.getItemIndex(index: 2)?.precio ?? "$ 0.0"
        rentaPrecioListaIVALabel.text = presenter.getItemIndex(index: 4)?.precio ?? "$ 0.0"
    }
    
    func setEmptyProgress() {
        
    }
    
    func startLoading() {
        super.showLoading(view: self.view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
    func updateTitleHeader() {
        
    }
}
