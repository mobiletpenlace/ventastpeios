//
//  CotizacionPanelViewController.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 29/06/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class CotizacionPanelVC: BaseItemVC {
    @IBOutlet weak var informacionPlanView: UIView!
    @IBOutlet weak var costosView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cargarSubVistas()
    }
    
    func cargarSubVistas() -> Void {
        super.addViewXIB(vc: InformacionPlanVC(), container: informacionPlanView)
        super.addViewXIB(vc: CostosVC(), container: costosView)
    }
    
    @IBAction func clickbuttonPersonaliza(_ sender: UIButton) {
        padreController?.cambiarVistaChild(index: 2)
    }
    
}
