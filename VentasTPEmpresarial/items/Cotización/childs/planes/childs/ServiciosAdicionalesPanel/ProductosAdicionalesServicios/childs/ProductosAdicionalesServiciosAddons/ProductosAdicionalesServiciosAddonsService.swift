//
//  ProductosAdicionalesServiciosAddonsService.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 16/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import RealmSwift

class ProductosAdicionalesServiciosAddonsService: BaseDBManagerDelegate {
    var realm: Realm!
    var serverManager: ServerDataManager?
    var dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()
        serverManager = ServerDataManager()
        realm = getRealm()
    }
    
    func getData(configPlanId: Int, servicioId: String, _ callBack:@escaping ([ProductosAdicionalesServiciosAddonsModel]) -> Void) {
        var data = [ProductosAdicionalesServiciosAddonsModel]()//crear lista de datos
        for prod in dbManager.obtenerProductosAdicionalesAddons(configPlanId: configPlanId, servicioId: servicioId){
            data.append(
                ProductosAdicionalesServiciosAddonsModel(
                    nombre: prod.addon.nombre,
                    precio: prod.addon.precio,
                    impuesto: prod.addon.impuesto,
                    cantidad: prod.cantidad,
                    limiteSup: prod.addon.maximo,
                    id: prod.addon.id!,
                    isActivo: prod.isActivo
                )
            )
        }
        callBack(data)
    }
    
    func restarProductoAddon(idAddon: String, configPlanId: Int, prodAdicId: String){
        dbManager.restarProductoAddon(idAddon: idAddon, configPlanId: configPlanId, prodAdicId: prodAdicId)
    }

    func sumarProductoAddon(idAddon: String, configPlanId: Int, prodAdicId: String){
        dbManager.sumarProductoAddon(idAddon: idAddon, configPlanId: configPlanId, prodAdicId: prodAdicId)
    }
}
