//
//  ProductosAdicionalesServiciosVC.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 15/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class ProductosAdicionalesServiciosVC: BaseItemVC {
    @IBOutlet weak var tblUsers: UITableView!
    @IBOutlet weak var viewAddons: UIView!
    
    var nombreCelda2: String = "ProductosAdicionalesServiciosCell"
    fileprivate let presenter = ProductosAdicionalesServiciosPresenter(service: ProductosAdicionalesServiciosService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachview(view: self)
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow() {
        super.viewDidShow()
        presenter.viewDidShow()
    }
    
    private func setUpView(){
        iniciarTableView()
        super.addViewXIB(vc: ProductosAdicionalesServiciosAddonsVC(), container: viewAddons)
    }
    
    func iniciarTableView() -> Void {
        let nib = UINib.init(nibName: nombreCelda2, bundle: nil)
        self.tblUsers.register(nib, forCellReuseIdentifier: nombreCelda2)
        self.tblUsers.delegate = self
        self.tblUsers.dataSource = self
    }
    
}

extension ProductosAdicionalesServiciosVC: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.seleccionadoProductoAdicional(index: indexPath.row)
    }
}

extension ProductosAdicionalesServiciosVC: UITableViewDataSource, BaseFormatNumber {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getSize()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: nombreCelda2, for: indexPath) as! ProductosAdicionalesServiciosCell
        let data = presenter.getItemIndex(index: indexPath.row)
        cell.nombre.text = data.nombre
        //cell.selectionStyle = .none //desactivar el color al seleccionar la celda
        return cell
    }
}

extension ProductosAdicionalesServiciosVC: ProductosAdicionalesServiciosDelegate{
    
    func seleccionarCelda(_ pos: Int){
        let indexPath = IndexPath(row: pos, section: 0)
        self.tblUsers.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
    }
    
    func updateData() {
        self.tblUsers.reloadData()
    }
    
    func setEmptyProgress() {
        
    }
    
    func startLoading() {
        super.showLoading(view: self.view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
    func updateTitleHeader() {
        
    }
    
    func reloadRows(pos: Int){
        let indexPath = [IndexPath(row: pos, section: 0)]
        tblUsers.reloadRows(at: indexPath, with: .automatic)
    }
}
