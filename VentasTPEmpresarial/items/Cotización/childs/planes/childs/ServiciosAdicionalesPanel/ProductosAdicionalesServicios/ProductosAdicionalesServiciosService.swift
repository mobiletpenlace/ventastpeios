//
//  ProductosAdicionalesServiciosService.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 15/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import RealmSwift

class ProductosAdicionalesServiciosService: BaseDBManagerDelegate{
    var serverManager: ServerDataManager?
    var realm: Realm!
    var dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()
        realm = getRealm()
    }
    
    func getData(idPlan: String, _ callBack:@escaping ([ProductosAdicionalesServiciosModel]) -> Void){
        getDataFromDB(idPlan, callBack)
    }
    
    func getDataFromDB(_ idPlan: String, _ callBack:@escaping ([ProductosAdicionalesServiciosModel]) -> Void) {
        var data = [ProductosAdicionalesServiciosModel]()//crear lista de datos
        for prod in dbManager.obtenerProductosAdicionales(idPlan){
            data.append(
                ProductosAdicionalesServiciosModel(
                    nombre: prod.nombre,
                    precio: prod.precio,
                    cantidad: prod.cantidad,
                    id: prod.id!
                )
            )
        }
        callBack(data)
    }
    
}
