//
//  ProductosAdicionalesServiciosAddonsVC.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 16/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class ProductosAdicionalesServiciosAddonsVC: BaseItemVC {
    @IBOutlet weak var tableview: UITableView!
    
    var nombreCelda2: String = "ProductosAdicionalesServiciosAddonsCell"
    fileprivate let presenter = ProductosAdicionalesServiciosAddonsPresenter(service: ProductosAdicionalesServiciosAddonsService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachview(view: self)
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow() {
        super.viewDidShow()
        presenter.viewDidShow()
    }
    
    private func setUpView(){
        iniciarTableView()
    }
    
    func iniciarTableView() -> Void {
        let nib = UINib.init(nibName: nombreCelda2, bundle: nil)
        self.tableview.register(nib, forCellReuseIdentifier: nombreCelda2)
        self.tableview.delegate = self
        self.tableview.dataSource = self
    }
    
}

extension ProductosAdicionalesServiciosAddonsVC: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension ProductosAdicionalesServiciosAddonsVC: UITableViewDataSource, BaseFormatNumber {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getSize()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: nombreCelda2, for: indexPath) as! ProductosAdicionalesServiciosAddonsCell
        let data = presenter.getItemIndex(index: indexPath.row)
        cell.nombre.text = data.nombre
        cell.cantidad.text = "\(data.cantidad)"
        cell.precio.text = "$"+numeroAFormatoPrecio(numero: data.precio)
        cell.posicion = indexPath.row
        cell.updateActive(data.isActivo)
        cell.selectionStyle = .none //desactivar el color al seleccionar la celda
        return cell
    }
}

extension ProductosAdicionalesServiciosAddonsVC: ProductosAdicionalesServiciosAddonsDelegate{
    func updateData() {
        self.tableview.reloadData()
    }
    
    func setEmptyProgress() {
        
    }
    
    func startLoading() {
        super.showLoading(view: self.view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
    func updateTitleHeader() {
        
    }
    
    func reloadRows(pos: Int){
        let indexPath = [IndexPath(row: pos, section: 0)]
        tableview.reloadRows(at: indexPath, with: .automatic)
    }
}
