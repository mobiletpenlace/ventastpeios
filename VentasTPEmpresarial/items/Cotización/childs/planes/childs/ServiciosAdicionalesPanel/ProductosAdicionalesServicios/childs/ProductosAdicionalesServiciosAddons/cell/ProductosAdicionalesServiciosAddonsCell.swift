//
//  ProductosAdicionalesServiciosAddonsCell.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 16/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class ProductosAdicionalesServiciosAddonsCell: UITableViewCell {
    @IBOutlet weak var nombre: UILabel!
    @IBOutlet weak var precio: UILabel!
    @IBOutlet weak var cantidad: UILabel!
    @IBOutlet weak var imageViewAgregarServicio: UIImageView!
    @IBOutlet weak var imageViewRestarServicio: UIImageView!
    @IBOutlet weak var viewContenido: UIView!
    
    var posicion: Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func clickAgregar(_ sender: UIButton) {
        sender.animateBound(view: imageViewAgregarServicio)
        notificarAgregarServiciosadicionales()
    }
    
    @IBAction func clickEliminar(_ sender: UIButton) {
        sender.animateBound(view: imageViewRestarServicio)
        notificarQuitarServiciosadicionales()
    }
    
    func notificarAgregarServiciosadicionales(){
        SwiftEventBus.post(
            "CotizacionPlanesServiciosAdicionalesProductosAddons-sumar",
            sender: posicion
        )
    }

    func notificarQuitarServiciosadicionales(){
        SwiftEventBus.post(
            "CotizacionPlanesServiciosAdicionalesProductosAddons-restar",
            sender: posicion
        )
    }
    
    func updateActive(_ isActivo: Bool){
        if isActivo{
            viewContenido.isUserInteractionEnabled = true
            nombre.isEnabled = true
            precio.isEnabled = true
            cantidad.isEnabled = true
            viewContenido.backgroundColor = UIColor(netHex: 0xFFFFFF).withAlphaComponent(0.0)
        }else{
            viewContenido.isUserInteractionEnabled = false
            nombre.isEnabled = false
            precio.isEnabled = false
            cantidad.isEnabled = false
            viewContenido.backgroundColor = UIColor(netHex: 0xEEEEEE).withAlphaComponent(0.4)
        }
    }
}
