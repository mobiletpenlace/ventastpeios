//
//  productosAdicionalesServiciosModel.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 15/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct ProductosAdicionalesServiciosModel {
    var nombre: String
    var precio: Float
    var cantidad: Int
    var id: String
}
