//
//  ProductosAdicionalesServiciosAddonsPresenter.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 16/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import Foundation
import SwiftEventBus

protocol ProductosAdicionalesServiciosAddonsDelegate: BaseDelegate {
    func updateData()
    func setEmptyProgress()
    func reloadRows(pos: Int)
}

class ProductosAdicionalesServiciosAddonsPresenter{
    fileprivate let service: ProductosAdicionalesServiciosAddonsService
    weak fileprivate var view: ProductosAdicionalesServiciosAddonsDelegate?
    fileprivate var dataList = [ProductosAdicionalesServiciosAddonsModel]()
    var servicioSeleccionado: String = ""
    var configPlanId: Int?
    
    init(service: ProductosAdicionalesServiciosAddonsService){
        self.service = service
    }
    
    func attachview(view:ProductosAdicionalesServiciosAddonsDelegate){
        self.view = view
        
        SwiftEventBus.onMainThread(self, name: "CotizacionPlanesServiciosAdicionalesProductosAddons-cambioServicio") {
            [weak self] result in
            if let idServicio = result!.object as? String{
                self?.servicioSeleccionado = idServicio
                self?.actualizarDatos()
            }
        }
        
        SwiftEventBus.onMainThread(self, name: "CotizacionPlanesServiciosAdicionalesProductosAddons-desseleccionadoServicio") {
            [weak self] result in
            self?.servicioSeleccionado = ""
            self?.actualizarDatos()
        }
        
        SwiftEventBus.onMainThread(self, name: "CotizacionPlanesServiciosAdicionalesProductosAddons-sumar") {
            [weak self] result in
            if let pos = result?.object as? Int{
                self?.sumarServicio(pos)
            }
        }
        SwiftEventBus.onMainThread(self, name: "CotizacionPlanesServiciosAdicionalesProductosAddons-restar") {
            [weak self] result in
            if let pos = result?.object as? Int{
                self?.restarServicio(pos)
            }
        }
        
        SwiftEventBus.onMainThread(self, name: "CotizacionPlanesServiciosAdicionalesProductosAddons-cambioIdConfigPlan") {
            [weak self] result in
            if let configPlanId = result!.object as? Int{
                self?.configPlanId = configPlanId
                self?.actualizarDatos()
            }
        }
        
    }
    
    func restarServicio(_ pos: Int){
        let addonId = dataList[pos].id
        if let configPlanId = configPlanId{
            service.restarProductoAddon(idAddon: addonId, configPlanId: configPlanId, prodAdicId: servicioSeleccionado)
            actualizarVista()
        }
    }
    
    func sumarServicio(_ pos: Int){
        let addonId = dataList[pos].id
        if let configPlanId = configPlanId{
            service.sumarProductoAddon(idAddon: addonId, configPlanId: configPlanId, prodAdicId: servicioSeleccionado)
            actualizarVista()
        }
    }
    
    func actualizarVista(){
        actualizarDatos()
        notificarResumenCobertura()
    }

    func notificarResumenCobertura(){
        SwiftEventBus.post(
            "CotizacionPlanesResumenCotizacion-actualizar",
            sender: nil
        )
    }
    
    func viewDidAppear(){
        
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
        actualizarDatos()
    }
    
    func datachView(){
        view = nil
    }
    
    func getItemIndex(index: Int) -> ProductosAdicionalesServiciosAddonsModel{
        return dataList[index]
    }
    
    func getSize() -> Int{
        return dataList.count
    }
    
    func actualizarDatos(){
        if let configPlanId = configPlanId{
            self.view?.startLoading()
            service.getData(configPlanId: configPlanId, servicioId: servicioSeleccionado){[weak self] data in
                self?.onFinishGetData(data: data)
            }
        }
    }
    
    func onFinishGetData( data: [ProductosAdicionalesServiciosAddonsModel]){
        view?.finishLoading()
        if (data.count == 0){
            dataList = [ProductosAdicionalesServiciosAddonsModel]()
        }else{
            dataList = data
        }
        view?.updateData()
    }
}
