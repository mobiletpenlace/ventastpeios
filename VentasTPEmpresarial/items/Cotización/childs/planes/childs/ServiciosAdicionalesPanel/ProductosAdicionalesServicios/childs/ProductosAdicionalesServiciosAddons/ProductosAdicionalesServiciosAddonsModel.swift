//
//  ProductosAdicionalesServiciosAddonsModel.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 16/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct ProductosAdicionalesServiciosAddonsModel {
    var nombre: String
    var precio: Float
    var impuesto: Float
    var cantidad: Int
    var limiteSup: Int
    var id: String
    var isActivo: Bool
}
