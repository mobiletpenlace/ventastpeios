//
//  ProductosAdicionalesServiciosPresenter.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 15/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol ProductosAdicionalesServiciosDelegate: BaseDelegate {
    func updateData()
    func setEmptyProgress()
    func reloadRows(pos: Int)
    func seleccionarCelda(_ pos: Int)
}

class ProductosAdicionalesServiciosPresenter{
    fileprivate let service: ProductosAdicionalesServiciosService
    weak fileprivate var view: ProductosAdicionalesServiciosDelegate?
    fileprivate var dataList = [ProductosAdicionalesServiciosModel]()
    var planSeleccionado: SeleccionarPlanesModel?
    var servicioSeleccionadoPos: Int = -1
    
    init(service: ProductosAdicionalesServiciosService){
        self.service = service
    }
    
    func attachview(view:ProductosAdicionalesServiciosDelegate){
        self.view = view
        
        SwiftEventBus.onMainThread(self, name: "CotizacionPlanesServiciosAdicionalesProductos-cambioPlan") {
            [weak self] result in
            if let plan = result!.object as? SeleccionarPlanesModel{
                self?.planSeleccionado = plan
                self?.actualizarDatos()
            }
        }
        
        SwiftEventBus.onMainThread(self, name: "CotizacionPlanesServiciosAdicionalesProductos-desseleccionPlan") {
            [weak self] result in
            self?.planSeleccionado = nil
            self?.desseleccionadoProductoAdicional()
            self?.actualizarDatos()
        }
    }
    
    func viewDidAppear(){
        
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
        actualizarDatos()
    }
    
    func datachView(){
        view = nil
    }
    
    func getItemIndex(index: Int) -> ProductosAdicionalesServiciosModel{
        return dataList[index]
    }
    
    func getSize() -> Int{
        return dataList.count
    }
    
    func seleccionadoProductoAdicional(index: Int){
        servicioSeleccionadoPos = index
        let idServicio = dataList[index].id
        SwiftEventBus.post(
            "CotizacionPlanesServiciosAdicionalesProductosAddons-cambioServicio",
            sender: idServicio
        )
    }
    
    func desseleccionadoProductoAdicional(){
        servicioSeleccionadoPos = -1
        SwiftEventBus.post(
            "CotizacionPlanesServiciosAdicionalesProductosAddons-desseleccionadoServicio",
            sender: nil
        )
    }
    
    func actualizarDatos(){
        self.view?.startLoading()
        let planID = planSeleccionado?.id ?? ""
        service.getData(idPlan: planID){[weak self] data in
            self?.onFinishGetData(data: data)
        }
    }
    
    func onFinishGetData( data: [ProductosAdicionalesServiciosModel]){
        view?.finishLoading()
        if (data.count == 0){
            dataList = [ProductosAdicionalesServiciosModel]()
        }else{
            dataList = data
        }
        view?.updateData()
        if servicioSeleccionadoPos >= 0{
            view?.seleccionarCelda(servicioSeleccionadoPos)
        }
    }
}
