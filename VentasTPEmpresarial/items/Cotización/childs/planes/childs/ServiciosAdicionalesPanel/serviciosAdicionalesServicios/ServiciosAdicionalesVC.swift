//
//  ServiciosAdicionalesViewController.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 19/06/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class ServiciosAdicionalesVC: BaseItemVC{
    @IBOutlet weak var tblUsers: UITableView!
    var nombreCelda2: String = "ServiciosAdicionalesCell"
    fileprivate let presenter = ServiciosAdicionalesPresenter(service: ServiciosAdicionalesService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachview(view: self)
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow() {
        super.viewDidShow()
        presenter.viewDidShow()
    }
    
    private func setUpView(){
        iniciarTableView()
    }
    
    func iniciarTableView() -> Void {
        let nib = UINib.init(nibName: nombreCelda2, bundle: nil)
        self.tblUsers.register(nib, forCellReuseIdentifier: nombreCelda2)
        self.tblUsers.delegate = self
        self.tblUsers.dataSource = self
    }
    
}

extension ServiciosAdicionalesVC: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return ServiciosAdicionalesCellHeader().view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension ServiciosAdicionalesVC: UITableViewDataSource, BaseFormatNumber {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getSize()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: nombreCelda2, for: indexPath) as! ServiciosAdicionalesCell
        let data = presenter.getItemIndex(index: indexPath.row)
        cell.nombre.text = data.servicio
        cell.precio.text = "$\(numeroAFormatoPrecio(numero: data.precio))"
        cell.cantidad.text = "\(data.cantidad)"
        cell.posicion = indexPath.row
        cell.selectionStyle = .none //desactivar el color al seleccionar la celda
        return cell
    }
}

extension ServiciosAdicionalesVC: ServiciosAdicionalesDelegate{
    func updateData() {
        self.tblUsers.reloadData()
    }
    
    func setEmptyProgress() {
        
    }
    
    func startLoading() {
        super.showLoading(view: self.view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
    func updateTitleHeader() {
        
    }
    
    func reloadRows(pos: Int){
        let indexPath = [IndexPath(row: pos, section: 0)]
        tblUsers.reloadRows(at: indexPath, with: .automatic)
    }
}
