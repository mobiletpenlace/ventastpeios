//
//  MyCustomCell2.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 19/06/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class ServiciosAdicionalesCell: UITableViewCell {
    @IBOutlet weak var nombre: UILabel!
    @IBOutlet weak var precio: UILabel!
    @IBOutlet weak var cantidad: UILabel!
    @IBOutlet weak var imageViewAgregarServicio: UIImageView!
    @IBOutlet weak var imageViewRestarServicio: UIImageView!
    var posicion: Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func clickAgregar(_ sender: UIButton) {
        sender.animateBound(view: imageViewAgregarServicio)
        notificarAgregarServiciosadicionales()
    }
    
    @IBAction func clickEliminar(_ sender: UIButton) {
        sender.animateBound(view: imageViewRestarServicio)
        notificarQuitarServiciosadicionales()
    }
    
    func notificarAgregarServiciosadicionales(){
        SwiftEventBus.post(
            "CotizacionPlanesServiciosAdicionales-sumar",
            sender: posicion
        )
    }
    
    func notificarQuitarServiciosadicionales(){
        SwiftEventBus.post(
            "CotizacionPlanesServiciosAdicionales-restar",
            sender: posicion
        )
    }
}
