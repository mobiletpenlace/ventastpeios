//
//  ServiciosAdicionalesService.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 24/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import RealmSwift

class ServiciosAdicionalesService: BaseDBManagerDelegate {
    var realm: Realm!
    var serverManager: ServerDataManager?
    var dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()
        realm = getRealm()
    }
    
    func getData(_ configPlanId: Int, _ callBack:@escaping ([ServiciosAdicionalesModel]) -> Void){
        //getDataFromService(idPlan, callBack)
        getDataFromDB(configPlanId, callBack)
    }
    
//    func getDataFromService(_ idPlan: String, _ callBack:@escaping ([ServiciosAdicionalesModel]) -> Void) {
//        let data = [
//            ServiciosAdicionalesModel(servicio: "smart wifi", precio: 1649, cantidad: 0),
//            ServiciosAdicionalesModel(servicio: "smart wifi Exterior", precio: 2249, cantidad: 0),
//            ServiciosAdicionalesModel(servicio: "seguridad TI Pyme 20", precio: 2500, cantidad: 0),
//            ServiciosAdicionalesModel(servicio: "seguridad TI Pyme 10", precio: 1249, cantidad: 0),
//            ServiciosAdicionalesModel(servicio: "cambios seguridad", precio: 325, cantidad: 0),
//            ServiciosAdicionalesModel(servicio: "prueba 1", precio: 325, cantidad: 1),
//            ServiciosAdicionalesModel(servicio: "prueba 2", precio: 1234567325, cantidad: 2),
//            ServiciosAdicionalesModel(servicio: "prueba 3", precio: 45325, cantidad: 3),
//            ServiciosAdicionalesModel(servicio: "prueba 4", precio: 122323325, cantidad: 4),
//            ServiciosAdicionalesModel(servicio: "prueba 5", precio: 325233, cantidad: 5),
//        ]
//        let delayTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
//        DispatchQueue.main.asyncAfter(deadline: delayTime) {
//            callBack(data)
//        }
//    }
    
    func getDataFromDB(_ configPlanId: Int, _ callBack:@escaping ([ServiciosAdicionalesModel]) -> Void) {
        var data = [ServiciosAdicionalesModel]()//crear lista de datos
        for servicioConfig in dbManager.obtenerServiciosAdicionales(configPlanId){
            data.append(
                ServiciosAdicionalesModel(
                    id: servicioConfig.servicioAdicional.id!,
                    servicio: servicioConfig.servicioAdicional.nombre,
                    precio: servicioConfig.servicioAdicional.precio,
                    cantidad: servicioConfig.cantidad
                )
            )
        }
        callBack(data)
    }
    
    func updateRow(_ idPlan: String, _ pos: Int) -> ServiciosAdicionalesModel{
        let plan = dbManager.buscarPlanPorID(id: idPlan)
        let row = plan!.serviciosAdicionales[pos]
        let res = ServiciosAdicionalesModel(
            id: row.id!,
            servicio: row.nombre,
            precio: row.precio,
            cantidad: row.cantidad
        )
        return res
    }
    
    func restarServicioPlan(_ configPlanId: Int, _ idServAdici: String){
        dbManager.restarServicioPlan(configPlanId, idServAdici)
    }
    
    func sumarServicioPlan(_ configPlanId: Int, _ idServAdici: String){
        dbManager.sumarServicioPlan(configPlanId, idServAdici)
    }
    
}
