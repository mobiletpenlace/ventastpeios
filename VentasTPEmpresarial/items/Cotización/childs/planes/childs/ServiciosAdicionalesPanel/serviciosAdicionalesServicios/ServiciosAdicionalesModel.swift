//
//  ServiciosAdicionalesModel.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 24/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct ServiciosAdicionalesModel {
    var id: String
    var servicio: String
    var precio: Float
    var cantidad: Int
}
