//
//  ServiciosAdicionalesPresenter.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 24/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol ServiciosAdicionalesDelegate: BaseDelegate {
    func updateData()
    func setEmptyProgress()
    func reloadRows(pos: Int)
}

class ServiciosAdicionalesPresenter{
    fileprivate let service: ServiciosAdicionalesService
    weak fileprivate var view: ServiciosAdicionalesDelegate?
    fileprivate var dataList = [ServiciosAdicionalesModel]()
    var planSeleccionado: SeleccionarPlanesModel?
    var configPlanId: Int?
    
    init(service: ServiciosAdicionalesService){
        self.service = service
    }
    
    func attachview(view:ServiciosAdicionalesDelegate){
        self.view = view
        
        SwiftEventBus.onMainThread(self, name: "CotizacionPlanesServiciosAdicionales-cambioPlan") {
            [weak self] result in
            if let plan = result!.object as? SeleccionarPlanesModel{
                self?.planSeleccionado = plan
                self?.actualizarDatos()
            }
        }
        
        SwiftEventBus.onMainThread(self, name: "CotizacionPlanesServiciosAdicionales-desseleccionPlan") {
            [weak self] result in
            self?.planSeleccionado = nil
            self?.actualizarDatos()
        }
  
        SwiftEventBus.onMainThread(self, name: "CotizacionPlanesServiciosAdicionales-sumar") {
            [weak self] result in
            if let pos = result?.object as? Int{
                self?.sumarServicio(pos)
            }
        }
        SwiftEventBus.onMainThread(self, name: "CotizacionPlanesServiciosAdicionales-restar") {
            [weak self] result in
            if let pos = result?.object as? Int{
                self?.restarServicio(pos)
            }
        }
        SwiftEventBus.onMainThread(self, name: "CotizacionPlanesServiciosAdicionales-cambioIdConfigPlan") {
            [weak self] result in
            if let configPlanId = result!.object as? Int{
                self?.configPlanId = configPlanId
                self?.actualizarDatos()
            }
        }
    }
    
    func restarServicio(_ pos: Int){
        let idSerAdic = dataList[pos].id
        if let configPlanId = configPlanId{
            service.restarServicioPlan(configPlanId, idSerAdic)
            actualizarDatos()
            notificarResumenCobertura()
        }
    }
    
    func sumarServicio(_ pos: Int){
        let idSerAdic = dataList[pos].id
        if let configPlanId = configPlanId{
            service.sumarServicioPlan(configPlanId, idSerAdic)
            actualizarDatos()
            notificarResumenCobertura()
        }
    }
    
    func notificarResumenCobertura(){
        SwiftEventBus.post(
            "CotizacionPlanesResumenCotizacion-actualizar",
            sender: nil
        )
    }
    
    func viewDidAppear(){
        
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
    }
    
    func datachView(){
        view = nil
    }
    
    func getItemIndex(index: Int) -> ServiciosAdicionalesModel{
        return dataList[index]
    }
    
    func getSize() -> Int{
        return dataList.count
    }
    
    func actualizarDatos(){
        if let configPlanId = configPlanId{
            self.view?.startLoading()
            service.getData(configPlanId){[weak self] data in
                self?.onFinishGetData(data: data)
            }
        }
    }
    
    func onFinishGetData( data: [ServiciosAdicionalesModel]){
        view?.finishLoading()
        if (data.count == 0){
            dataList = [ServiciosAdicionalesModel]()
        }else{
            dataList = data
        }
        view?.updateData()
    }
}
