//
//  MyCustomCell2.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 19/06/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class ServiciosAdicionalesCell: UITableViewCell {
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var imageViewAgregarServicio: UIImageView!
    @IBOutlet weak var imageViewRestarServicio: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func clickAgregar(_ sender: UIButton) {
        sender.animateScale(view: imageViewAgregarServicio)
        print("agregando servicio")
    }
    
    @IBAction func clickEliminar(_ sender: UIButton) {
        sender.animateScale(view: imageViewRestarServicio)
        print("eliminando servicio")
    }
    
}
