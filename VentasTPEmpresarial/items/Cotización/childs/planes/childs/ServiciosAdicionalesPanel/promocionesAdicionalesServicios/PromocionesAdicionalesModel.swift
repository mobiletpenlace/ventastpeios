//
//  PromocionesAdicionalesService.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 15/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
struct PromocionesAdicionalesModel {
    var nombre: String
    var id: String
    var fechaInicio : String
    var fechaFin : String
    var Activo: Bool
    var Seleccionado : Bool
}

