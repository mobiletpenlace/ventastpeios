//
//  PromocionesAdicionalesServiciosVC.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 15/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class PromocionesAdicionalesServiciosVC: BaseItemVC {
    @IBOutlet weak var tabPromotions: UITableView!
    var nombreCelda2: String = "promocionesAdicionalesCell"
    fileprivate let presenter = PromocionesAdicionalesPresenter(service: PromocionesAdicionalesService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachview(view: self)
        setUpView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidShow() {
        super.viewDidShow()
        presenter.viewDidShow()
    }
    
    private func setUpView(){
        iniciarTableView()
    }
    
    func iniciarTableView() -> Void {
        let nib = UINib.init(nibName: nombreCelda2, bundle: nil)
        self.tabPromotions.register(nib, forCellReuseIdentifier: nombreCelda2)
        self.tabPromotions.delegate = self
        self.tabPromotions.dataSource = self
    }
                                                                                                                                                                                          

}

extension PromocionesAdicionalesServiciosVC: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return PromocionesAdicionalesHeaderCell().view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension PromocionesAdicionalesServiciosVC: UITableViewDataSource, BaseFormatNumber {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getSize()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: nombreCelda2, for: indexPath) as! PromocionesAdicionalesCell
        let data = presenter.getItemIndex(index: indexPath.row)
        cell.nombre.text = data.nombre
        cell.pos = indexPath.row
        if(data.Seleccionado){
            ButtonUtils.setIcon(cell.ActiveButton, icon: "icon_check_on")
        }else{
            ButtonUtils.setIcon(cell.ActiveButton, icon: "icon_check_off")
        }
        cell.activar(data.Activo)
        Logger.d("\(data.Activo)")
        cell.selectionStyle = .none //desactivar el color al seleccionar la celda
        return cell
    }
    
}

extension PromocionesAdicionalesServiciosVC: PromocionesAdicionalesDelegate{
    func updateData() {
        self.tabPromotions.reloadData()
    }
    
    func setEmptyProgress() {
        
    }
    
    func startLoading() {
        super.showLoading(view: self.view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
    func updateTitleHeader() {
        
    }
    
    func updateTitleHeader(header: HeaderData) {
        super.updateTitle(headerData: header)
    }
    
    func reloadRows(pos: Int){
        let indexPath = [IndexPath(row: pos, section: 0)]
        tabPromotions.reloadRows(at: indexPath, with: .automatic)
    }
}
