//
//  PromocionesAdicionalesService.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 15/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import RealmSwift

class PromocionesAdicionalesService: BaseDBManagerDelegate {
    var serverManager: ServerDataManager?
    var realm: Realm!
    var dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()
        realm = getRealm()
    }
    
    func getData(idPlan: String, _ callBack:@escaping([PromocionesAdicionalesModel]) -> Void){
        getDataFromDB(idPlan, callBack)
        //getDataFromService(idPlan, callBack)
    }
    
    func getDataFromService(_ idPlan: String, _ callBack:@escaping ([PromocionesAdicionalesModel]) -> Void) {
        let data = [
            PromocionesAdicionalesModel(nombre: "", id: "", fechaInicio: "", fechaFin: "", Activo: true, Seleccionado: false),
            ]
        let delayTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            callBack(data)
        }
    }
    
    func getDataFromDB(_ idPlan: String, _ callBack:@escaping ([PromocionesAdicionalesModel]) -> Void) {
        var data = [PromocionesAdicionalesModel]()//crear lista de datos
        let promociones = dbManager.buscarPromociones(idPlan: idPlan)
        for promocion in promociones{
//            if(validateDate(promocion.fechaInicio)){
                data.append(
                    PromocionesAdicionalesModel(
                        nombre: promocion.nombre,
                        id: promocion.id!,
                        fechaInicio: promocion.fechaInicio,
                        fechaFin: promocion.fechaFin,
                        Activo: promocion.activo,
                        Seleccionado: promocion.seleccionado
                    )
                )
            //}
        }
        callBack(data)
    }
    
    func updateRow(_ idPromocion: String) -> PromocionesAdicionalesModel{
        let row = dbManager.buscarPromocionPorID(id : idPromocion)!
        let res = PromocionesAdicionalesModel(
            nombre: row.nombre,
            id: row.id!,
            fechaInicio: row.fechaInicio,
            fechaFin: row.fechaFin,
            Activo: row.activo,
            Seleccionado: row.seleccionado
        )
        return res
    }
    
    func SeleccionarPromocion(_ idPromocion: String){
       //dbManager.seleccionarPromocion(idPromocion)
    }
    
    func validateDate(_ date : String) -> Bool{
        let dateStartString = date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.locale = NSLocale.current
        guard let dateStartDate = dateFormatter.date(from: dateStartString) else {
            fatalError("ERROR: Date conversion failed due to mismatched format.")
        }
        
        var dateToday = Date()
        let dateTodaystr = dateFormatter.string(from: dateToday)
        dateToday = dateFormatter.date(from: dateTodaystr)!
        
        var isValid : Bool?
        if(dateStartDate>=dateToday){
            isValid = true
        }
        else{
            isValid = false
        }
        return isValid!
    }
   
    
}
