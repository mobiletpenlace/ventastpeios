//
//  promocionesAdicionalesCell.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 16/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class PromocionesAdicionalesCell: UITableViewCell {
    @IBOutlet weak var mView: UIView!
    @IBOutlet weak var nombre: UILabel!
    @IBOutlet weak var ActiveButton: UIButton!
    var pos : Int = 0

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func clickActivar(_ sender: UIButton) {
        notificarSeleccionarPromocionesAdicionales()
    }
    
    func notificarSeleccionarPromocionesAdicionales(){
        SwiftEventBus.post(
            "CotizacionPlanesServiciosAdicionalesPromociones-Seleccionar",
            sender: pos
        )
    }
    func activar(_ activo: Bool){
        if(activo){
            mView.backgroundColor = UIColor(netHex: 0xFFFFFF)
        }else{
            mView.backgroundColor = UIColor(netHex: 0x000000)
        }
    }
    
}
