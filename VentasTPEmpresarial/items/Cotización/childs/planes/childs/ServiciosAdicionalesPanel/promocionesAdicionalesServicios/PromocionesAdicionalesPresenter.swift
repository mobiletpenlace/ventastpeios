//
//  PromocionesAdicionalesPresenter.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 15/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol PromocionesAdicionalesDelegate: BaseDelegate {
    func updateData()
    func setEmptyProgress()
    func updateTitleHeader(header: HeaderData)
    func reloadRows(pos: Int)
}

class PromocionesAdicionalesPresenter{
    fileprivate let service: PromocionesAdicionalesService
    weak fileprivate var view: PromocionesAdicionalesDelegate?
    fileprivate var dataList = [PromocionesAdicionalesModel]()
    var planSeleccionado: SeleccionarPlanesModel?
    
    init(service: PromocionesAdicionalesService){
        self.service = service
    }
    
    func attachview(view:PromocionesAdicionalesDelegate){
        self.view = view
        
        SwiftEventBus.onMainThread(self, name: "CotizacionPlanesServiciosAdicionalesPromociones-cambioPlan") {
            [weak self] result in
            if let plan = result!.object as? SeleccionarPlanesModel{
                self?.planSeleccionado = plan
                self?.actualizarDatos()
            }
        }
        
        SwiftEventBus.onMainThread(self, name: "CotizacionPlanesServiciosAdicionalesPromociones-desseleccionPlan") {
            [weak self] result in
            self?.planSeleccionado = nil
            self?.actualizarDatos()
        }
        
        SwiftEventBus.onMainThread(self, name: "CotizacionPlanesServiciosAdicionalesPromociones-Seleccionar") {
            [weak self] result in
            if let pos = result?.object as? Int{
                self?.seleccionarCelda(pos)
            }
        }
    }
    
//    func restarServicio(_ pos: Int){
//        if (dataList[pos].cantidad > 0){
//            if let plan  = planSeleccionado{
//                service.restarServicioPlan(plan.id, pos)
//                updateRow(plan: plan, pos: pos)
//            }
//        }
//    }
//
    func seleccionarCelda(_ pos: Int){
//        if let plan  = planSeleccionado{
//            service.SeleccionarPromocion(dataList[pos].id)
//            actualizarDatos()
//            //updateRow(promocionId: dataList[pos].id, pos: pos)
//        }
    }
    
    func updateRow(promocionId: String, pos: Int){
        dataList[pos] = service.updateRow(promocionId)
        view?.reloadRows(pos: pos)
        notificarResumenCobertura()
    }
    
    func notificarResumenCobertura(){
        SwiftEventBus.post(
            "CotizacionPlanesResumenCotizacion-actualizar",
            sender: nil
        )
    }
    
    func viewDidAppear(){
        
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
        actualizarDatos()
    }
    
    func datachView(){
        view = nil
    }
    
    func getItemIndex(index: Int) -> PromocionesAdicionalesModel{
        return dataList[index]
    }
    
    func getSize() -> Int{
        return dataList.count
    }
    
    func actualizarDatos(){
        self.view?.startLoading()
        let planID = planSeleccionado?.id ?? ""
        service.getData(idPlan: planID){[weak self] data in
            self?.onFinishGetData(data: data)
        }
    }
    
    func onFinishGetData( data: [PromocionesAdicionalesModel]){
        view?.finishLoading()
        if (data.count == 0){
            dataList = [PromocionesAdicionalesModel]()
        }else{
            dataList = data
        }
        view?.updateData()
    }
}
