//
//  ServiciosAdicionalesPanelVCViewController.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 12/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class ServiciosAdicionalesPanelVCViewController: BaseItemVC {
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var segmentControl: CustomUISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSubView()
    }
    
    @IBAction func clickButtonInfoPlan(_ sender: UIButton) {
        padreController?.cambiarVistaChild(index: 1)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        segmentControl.changeUnderlinePosition(0)
    }
    
    func addSubView() -> Void {
        var viewControllers2 = [BaseItemVC]()
        viewControllers2.append(ServiciosAdicionalesVC())
        viewControllers2.append(ProductosAdicionalesServiciosVC())
        //viewControllers2.append(PromocionesAdicionalesServiciosVC())
        super.addViewXIBList(view: viewContent, viewControllers: viewControllers2)
    }
    
    @IBAction func selectedView(_ sender: CustomUISegmentedControl) {
        super.cambiarVista(index: sender.selectedSegmentIndex)
        sender.changeUnderlinePosition(sender.currentIndex)
    }
}
