//
//  ResumenCotizacionPlanService.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 25/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

class ResumenCotizacionPlanService: BaseDBManagerDelegate {
    var serverManager: ServerDataManager?
    var dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()
     }
    
    func getDataFromDB(_ configPlanId: Int, _ callBack:@escaping ([ResumenCotizacionPlanModel]) -> Void) {
        Logger.i("se va actualizar los datos del resumen de cotizacion con configPlanId:\(configPlanId)")
        var data = [ResumenCotizacionPlanModel]()//crear lista de datos
        if let plan = dbManager.buscarConfigPlanPorId(configPlanId){
            if let resumenCotizacion = plan.resumenCotizacion{
                data.append(
                    ResumenCotizacionPlanModel(titulo: "Renta mensual s/i", precio: resumenCotizacion.costoRentaMensual)
                )
                let costo = resumenCotizacion.costoInstalacion
                let plazoSeleccionado = dbManager.getPlazo()
                data.append(
                    ResumenCotizacionPlanModel(
                        titulo: "Costo de instalacion",
                        precio: (plazoSeleccionado == "36") ? 0 : costo
                    )
                )
                data.append(
                    ResumenCotizacionPlanModel(titulo: "Adicionales", precio: resumenCotizacion.costoAdicionales)
                )
                data.append(
                    ResumenCotizacionPlanModel(titulo: "Descuento", precio: resumenCotizacion.descuento)
                )
                data.append(
                    ResumenCotizacionPlanModel(titulo: "Sub Total", precio: resumenCotizacion.subTotal)
                )
                data.append(
                    ResumenCotizacionPlanModel(titulo: "Impuestos", precio: resumenCotizacion.impuestos)
                )
                data.append(
                    ResumenCotizacionPlanModel(titulo: "Total cargos únicos", precio: resumenCotizacion.cargosUnicos)
                )
                data.append(
                    ResumenCotizacionPlanModel(titulo: "Total", precio: resumenCotizacion.total)
                )
                data.append(
                    ResumenCotizacionPlanModel(titulo: "DescuentoPorcentaje", precio: Float(resumenCotizacion.porcentajeDescuento))
                )
            }
        }
        callBack(data)
    }
    
    func getData(_ callBack:@escaping ([DetalleSitiosModel]) -> Void){
        getDataFromDB2(callBack)
    }
    
    func getDataFromDB2( _ callBack:@escaping ([DetalleSitiosModel]) -> Void) {
        var data = [DetalleSitiosModel]()//crear lista de datos
        //dbManager.reloadRealm()
        let sitios = dbManager.obtenerSitios()
        for sitio in sitios{
            var planes = [SitioPlanModel]()
            for plan in sitio.planesConfig{
                planes.append(
                    SitioPlanModel(
                        id: plan.id,
                        nombre: plan.plan.nombre!
                    )
                )
            }
            var imagen: String = "img_cobertura_nose"
            if let cobertura = sitio.cobertura{
                if (cobertura.cobertura == true){
                    imagen = "img_cobertura"
                }else{
                    imagen = "img_no_cobertura"
                }
            }
            data.append(
                DetalleSitiosModel(
                    id: sitio.id,
                    name: sitio.nombre!,
                    planes: planes,
                    imagen: imagen,
                    precio: sitio.total
                )
            )
        }
        callBack(data)
    }
    
    func agregarDescuentoConfigPlan(configPlanId: Int, descuento: Float){
        dbManager.agregarDescuentoConfigPlan(configPlanId, descuento: descuento)
    }
    
    func verificarTipoFamilia(configPlanId: Int) -> String?{
        if let plan = dbManager.buscarConfigPlanPorId(configPlanId){
            if let tipo = plan.plan.familia.first!.tipo{
                return tipo.nombre
            }
        }
        return nil
    }
    
    func verificarServiciosFlexNet(configPlanId: Int) -> [DBConfigServicioAdicional]{
        var serviciosFlexnet = [DBConfigServicioAdicional]()
        if let plan = dbManager.buscarConfigPlanPorId(configPlanId){
            for servicio in plan.serviciosAdicionales{
                if servicio.cantidad > 0 && servicio.servicioAdicional.esSDWAN{
                    serviciosFlexnet.append(servicio)
                }
            }
        }
        return serviciosFlexnet
    }
    
    func actualizarConfigFlexNet(servicio: DBConfigServicioAdicional, flexnetModel:  DialogoFlexNetModel){
        dbManager.actualizarConfigFlexNet(servicio: servicio, flexnetModel: flexnetModel)
    }
    
    func verificarSiEsCiudadFronteriza(sitioId: Int) -> Bool?{
        if let sitio = dbManager.buscarSitioPorID(id: sitioId){
            if let cobertura = sitio.cobertura{
                let ciudad = cobertura.ciudad
                Logger.d("la ciudad a la que pertenece es: \(ciudad)")
                for ciudadFronteriza in Constantes.Array.ciudadesFronterizas{
                    if ciudad == ciudadFronteriza{
                        return true
                    }
                }
                return false
            }else{
                Logger.w("no se ha validado la cobetura")
                return nil
            }
        }else{
            Logger.w("no se ha encontrado el plan")
            return nil
        }
    }
    
    func updateIVAResumen(_ configPlanId: Int, valor: Float){
        dbManager.updateIVAResumen(configPlanId, valor: valor)
    }
}
