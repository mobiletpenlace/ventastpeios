//
//  ResumenCotizacionPlanModel.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 25/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct ResumenCotizacionPlanModel {
    let titulo: String
    let precio: Float
}
