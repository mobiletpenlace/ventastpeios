//
//  ResumenCotizacionPlanPresenter.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 16/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import Foundation
import SwiftEventBus

protocol ResumenCotizacionPlanDelegate: BaseDelegate {
    func regresar()
    func updateData()
    func masiveModel()
    func noSites()
    func msgAlert(titulo: String, mensaje: String, tituloBtn1: String, tituloBtn2: String, closureAgregar: @escaping ((UIAlertAction) -> Void), closureOmitir: @escaping ((UIAlertAction) -> Void))
    func msgAlert(titulo: String, mensaje: String, tituloBtn1: String, tituloBtn2: String, tituloBtn3: String, closure1: @escaping ((UIAlertAction) -> Void), closure2: @escaping ((UIAlertAction) -> Void), closure3: @escaping ((UIAlertAction) -> Void))
    func activarDescuento()
    func desactivarDescuento()
    func mostrarDialogoFlexnet(closure: @escaping ((DialogoFlexNetModel?) -> Void))
    func updateTextoEsFrontera(texto: String)
}

class ResumenCotizacionPlanPresenter{
    fileprivate var view: ResumenCotizacionPlanDelegate?
    var planSeleccionado: SeleccionarPlanesModel?
    enum TipoEditar{ case Default, Agregar, Actualizar, Eliminar }
    var tipo: TipoEditar = .Default
    fileprivate var data = [DetalleSitiosModel]()
    fileprivate var dataList = [ResumenCotizacionPlanModel]()
    fileprivate let service: ResumenCotizacionPlanService
    var configPlanId: Int?
    var sitioId: Int?
    var normal: Bool = true
    
    init(service: ResumenCotizacionPlanService){
        self.service = service
    }
    
    func attachView(view: ResumenCotizacionPlanDelegate){
        self.view = view
        SwiftEventBus.onMainThread(self, name: "CotizacionPlanesResumenCotizacion-cambioPlan") {
            [weak self] result in
            if let plan = result!.object as? SeleccionarPlanesModel{
                self?.planSeleccionado = plan
                //self?.actualizarDatos()
            }
        }
        
        SwiftEventBus.onMainThread(self, name: "CotizacionPlanesResumenCotizacion-cambioTipoEdicion") {
            [weak self] result in
            if let tipoSeleccionado = result!.object as? TipoEditar{
                Logger.d("se ha cambiado el tipo de planes a \(tipoSeleccionado)")
                self?.tipo = tipoSeleccionado
            }
        }
        
        SwiftEventBus.onMainThread(self, name: "CotizacionPlanesResumenCotizacion-desseleccionPlan") {
            [weak self] result in
            self?.planSeleccionado = nil
            self?.dataList = [ResumenCotizacionPlanModel]()
            self?.view?.updateData()
        }
        
        SwiftEventBus.onMainThread(self, name: "CotizacionPlanesResumenCotizacion-actualizar") {
            [weak self] result in
            self?.actualizarDatos()
        }
        
        SwiftEventBus.onMainThread(self, name: "Resumen_editaSitio") {
            [weak self] result in
            self?.normal = false
        }
        
        SwiftEventBus.onMainThread(self, name: "CotizacionPlanesResumenCotizacion-cambioIdConfigPlan") {
            [weak self] result in
            if let configPlanId = result!.object as? Int{
                self?.configPlanId = configPlanId
                self?.actualizarDatos()
                self?.verificarSiEsCiudadFronteriza()
                self?.verificarTipoFamilia()
            }
        }
        
        SwiftEventBus.onMainThread(self, name: "CotizacionPlanesResumenCotizacion-setIdSitio") {
            [weak self] result in
            if let sitioId = result!.object as? Int{
                self?.sitioId = sitioId
            }
        }
    }
    
    func viewDidAppear(){
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
    }
    
    func clickAgregarCarrito(){
        guard planSeleccionado != nil else {
            Logger.i("No se ha seleccionado un plan aún.")
            view?.showBannerAdvertencia(title: "Por favor seleccione un plan.")
            return
        }
        
        if let configPlanId = configPlanId{
            let servicios = service.verificarServiciosFlexNet(configPlanId: configPlanId)
            if servicios.count == 0{
                preguntarTipoCreacionSitios()
            }else{
                var configurado = true
                for serv in servicios{
                    if serv.configFlexnet == nil{
                        configurado = false
                    }
                }
                if configurado == false{
                    view?.mostrarDialogoFlexnet(){ flexnetModel in
                        guard let flexnetModel = flexnetModel else{
                            return
                        }
                        for serv in servicios{
                            if serv.configFlexnet == nil{
                                self.service.actualizarConfigFlexNet(servicio: serv, flexnetModel: flexnetModel)
                            }
                        }
                    }
                }else{
                    preguntarTipoCreacionSitios()
                }
            }
        }
    }
    
    private func preguntarTipoCreacionSitios(){
        if normal{
            let action1: ((UIAlertAction) -> Void) = { _ in
                self.crearUnSitio()
            }
            let action2: ((UIAlertAction) -> Void) = { _ in
                self.modeladoMasivo()
            }
            let action3: ((UIAlertAction) -> Void) = { _ in
                
            }
            view?.msgAlert(titulo: "Agregar al carrito", mensaje: "¿Que acción desea realizar?", tituloBtn1: "Crear nuevo plan", tituloBtn2: "Modelar sitios existentes", tituloBtn3: "Cancelar", closure1: action1, closure2: action2, closure3: action3)
        }else{
            crearUnSitio()
        }
    }
    
    private func crearUnSitio(){
        normal = true
        switch tipo{
        case .Default:
            crearNuevoSitioConPlan()
        case .Agregar:
            agregarPlanASitio()
        case .Actualizar:
            actualizarPlanDeSitio()
        case .Eliminar:
            Logger.d("eliminar en resumancotización no hace nada.")
        }
    }
    
    private func modeladoMasivo(){
        getSites()
    }
    
    private func getSites(){
        service.getData(){[weak self] data in
            self?.onFinishGetData(data: data)
        }
    }
    
    func onFinishGetData( data: [DetalleSitiosModel]){
        if(data.count == 0) {
            self.view?.noSites()
        } else {
            self.view?.masiveModel()
        }
    }
    
    private func crearNuevoSitioConPlan(){
        informarTipo(tipo: .nuevo)
    }
    
    private func agregarPlanASitio(){
        informarTipo(tipo: .agregar)
    }
    
    private func actualizarPlanDeSitio(){
        informarTipo(tipo: .actualizar)
    }
    
    private func informarTipo(tipo: Constantes.Enum.TipoEditar){
        informarPlanSeleccionado()
        informarCambioTipo(tipo: tipo)
        avanzarPantalla()
    }
    
    private func informarPlanSeleccionado(){
        SwiftEventBus.post(
            "Cotizacion_DetalleSitio_DetalleSitios(cambioPlanSeleccionado)",
            sender: planSeleccionado
        )
    }
    
    private func informarCambioTipo(tipo: Constantes.Enum.TipoEditar){
        SwiftEventBus.post(
            "Cotizacion_DetalleSitio_DetalleSitios(cambioTipoEdicion)",
            sender: tipo
        )
    }
    
    private func avanzarPantalla(){
        view?.regresar()
        SwiftEventBus.post("sitios-detalleSitios-informarTipoRegreso", sender: 0)
        SwiftEventBus.post("menuTabBar-cambiarMenuItem", sender: (2, 0))
    }
    
    func detachView() {
        view = nil
    }
    
    func getItemIndex(index: Int) -> ResumenCotizacionPlanModel?{
        if getSize() == 0 {
            return nil
        }
        return dataList[index]
    }
    
    func getSize() -> Int{
        return dataList.count
    }
    
    private func verificarTipoFamilia(){
        if let configPlanId = configPlanId{
            if let tipo = service.verificarTipoFamilia(configPlanId: configPlanId){
                if tipo == "Servicios"{
                    view?.activarDescuento()
                }else{
                    view?.desactivarDescuento()
                }
            }
        }
    }
    
    func actualizarDatos(){
        guard let configPlanId = configPlanId else { return }
        self.view?.startLoading()
        service.getDataFromDB(configPlanId){[weak self] data in
            self?.onFinishGetData(data: data)
        }
    }
    
    func onFinishGetData( data: [ResumenCotizacionPlanModel]){
        view?.finishLoading()
        if (data.count == 0){
            dataList = [ResumenCotizacionPlanModel]()
        }else{
            dataList = data
        }
        view?.updateData()
    }
    
    func verificarSiEsCiudadFronteriza(){
        guard let sitioId = sitioId else { return }
        guard let configPlanId = configPlanId else { return }
        
        if let res = service.verificarSiEsCiudadFronteriza(sitioId: sitioId){
            if res{
                view?.updateTextoEsFrontera(texto: "SI")
                service.updateIVAResumen(configPlanId, valor: 0.08)
            }else{
                view?.updateTextoEsFrontera(texto: "NO")
                service.updateIVAResumen(configPlanId, valor: 0.16)
            }
            actualizarDatos()
        }else{
            view?.updateTextoEsFrontera(texto: "?")
        }
    }
}


extension ResumenCotizacionPlanPresenter: CustomAlertViewDelegate{
    func clickOKButtonDialog(value: Any) {
        if let porcentaje = value as? Float{
            if let configPlanId = configPlanId{
                service.agregarDescuentoConfigPlan(configPlanId: configPlanId, descuento: porcentaje)
                actualizarDatos()
            }
        }
    }
    
    func clickCancelButtonDialog() {
        
    }
    
    
}
