//
//  ResumenCotizacionViewController.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 26/06/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class ResumenCotizacionPlanVC: BaseItemVC {
    @IBOutlet weak var labelRentaMensual: UILabel!
    @IBOutlet weak var labelCostoInstalacion: UILabel!
    @IBOutlet weak var labelAdicionales: UILabel!
    @IBOutlet weak var labelDescuento: UILabel!
    @IBOutlet weak var labelDescuentoPorcentaje: UILabel!
    @IBOutlet weak var labelSubtotal: UILabel!
    @IBOutlet weak var labelImpuestos: UILabel!
    @IBOutlet weak var labelTotalCargosUnicos: UILabel!
    @IBOutlet weak var labelTotal: UILabel!
    @IBOutlet weak var labelEsCiudad: UILabel!
    fileprivate let presenter = ResumenCotizacionPlanPresenter(service: ResumenCotizacionPlanService())
    @IBOutlet weak var buttonDescuento: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow() {
        super.viewDidShow()
        presenter.viewDidShow()
    }
    
    private func setUpView() {
        desactivarDescuento()
    }
    
    @IBAction func clickSiguiente(_ sender: UIButton) {
        sender.animateBound(view: sender)
        presenter.clickAgregarCarrito()
    }
    
    @IBAction func clickDescuento(_ sender: UIButton) {
        sender.animateBound(view: sender){[unowned self] _ in
            self.showDescuento()
        }
    }
    
    private func showDescuento(){
        let dialogo = DialogoDescuento()
        dialogo.delegate = presenter
        super.showDialog(customAlert: dialogo)
    }
}

extension ResumenCotizacionPlanVC: ResumenCotizacionPlanDelegate, BaseAlertViewDelegate, BaseFormatNumber{
    
    func regresar(){
        padreController?.padreController?.cambiarVista(FamiliasVC.self)
    }
    
    func updateTextoEsFrontera(texto: String){
        labelEsCiudad.text = texto
    }
    
    func mostrarDialogoFlexnet(closure: @escaping ((DialogoFlexNetModel?) -> Void)){
        let dialogo = DialogoFlexNetVC(closure: closure)
        dialogo.delegate = presenter
        super.showDialog(customAlert: dialogo)
    }
    
    func noSites() {
        super.showAlert(titulo: "Alerta", mensaje: "No hay sitios creados")
    }
    
    func masiveModel() {
        super.showDialog(customAlert: DialogoCargaPlanMasivaVC())
    }
    
    
    func updateData(){
        labelRentaMensual.text = getPrecio(0)
        labelCostoInstalacion.text = getPrecio(1)
        labelAdicionales.text = getPrecio(2)
        labelDescuento.text = getPrecio(3)
        labelSubtotal.text = getPrecio(4)
        labelImpuestos.text = getPrecio(5)
        labelTotalCargosUnicos.text = getPrecio(6)
        labelTotal.text = getPrecio(7)
        
        
        let porcentaje = presenter.getItemIndex(index: 8)?.precio ?? 0.0
        labelDescuentoPorcentaje.text = "\(porcentaje) %"
    }
    
    private func getPrecio(_ index: Int) -> String{
        let precio = presenter.getItemIndex(index: index)?.precio ?? 0.0
        return "$" + numeroAFormatoPrecio(numero: precio)
    }
    
    func startLoading() {
        super.showLoading(view: self.view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
    func updateTitleHeader() {
        
    }
    
    func msgAlert(titulo: String, mensaje: String, tituloBtn1: String, tituloBtn2: String, closureAgregar: @escaping ((UIAlertAction) -> Void), closureOmitir: @escaping ((UIAlertAction) -> Void)){
        alert(
            titulo,
            mensaje,
            self,
            tituloBtn1,
            closureAgregar,
            tituloBtn2,
            closureOmitir
        )
    }
    
    func msgAlert(titulo: String, mensaje: String, tituloBtn1: String, tituloBtn2: String, tituloBtn3: String, closure1: @escaping ((UIAlertAction) -> Void), closure2: @escaping ((UIAlertAction) -> Void), closure3: @escaping ((UIAlertAction) -> Void)){
        alert(
            titulo,
            mensaje,
            self,
            tituloBtn1,
            closure1,
            tituloBtn2,
            closure2,
            tituloBtn3,
            closure3
        )
    }
    
    func activarDescuento(){
        buttonDescuento.isHidden = false
    }

    func desactivarDescuento(){
        buttonDescuento.isHidden = true
    }
}

