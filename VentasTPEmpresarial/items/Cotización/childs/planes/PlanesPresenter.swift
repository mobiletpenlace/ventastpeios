//
//  PlanesPresenter.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 04/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol PlanesDelegate: BaseDelegate{
    func cambiarVistaDetallePlan(index: Int)
    func cambioPlanSeleccionado(plan : SeleccionarPlanesModel)
}

class PlanesPresenter{
    fileprivate var view: PlanesDelegate?
    
    
    func attachView(view: PlanesDelegate){
        self.view = view
        SwiftEventBus.onMainThread(self, name: "CotizacionPlanes-cambioPlan") {
            [weak self] result in
            if let plan = result!.object as? SeleccionarPlanesModel{
                self?.view?.cambioPlanSeleccionado(plan: plan)
            }
        }
    }
    
    func viewDidAppear(){
        
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
        view?.cambiarVistaDetallePlan(index: 0)
    }
    
    func detachView() {
        view = nil
    }
}
