//
//  ItemPlanesViewController.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 15/06/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class PlanesVC: BaseItemVC{
    @IBOutlet weak var planesViewCollection: UIView!
    @IBOutlet weak var resumenCotizacionView: UIView!
    @IBOutlet weak var labelNombrePlan: UILabel!
    @IBOutlet weak var panel: UIView!
    
    fileprivate let presenter = PlanesPresenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow() {
        super.viewDidShow()
        presenter.viewDidShow()
    }
    
    private func setUpView() {
        cargarSubVistas()
        addSubViewItems(viewContainer: panel)
        super.addShadowView(view: resumenCotizacionView)
    }
    
    func cargarSubVistas() -> Void {
        super.addViewXIB(vc: SeleccionarPlanesVC(), container: planesViewCollection)
        super.addViewXIB(vc: ResumenCotizacionPlanVC(), container: resumenCotizacionView)
    }
    
    func addSubViewItems(viewContainer: UIView) -> Void {
        var viewControllers = [BaseItemVC]()
        viewControllers.append(DefaultLogoVC())
        viewControllers.append(CotizacionPanelVC())
        viewControllers.append(ServiciosAdicionalesPanelVCViewController())
        super.addViewXIBList(view: panel, viewControllers: viewControllers)
    }
}

extension PlanesVC: PlanesDelegate{
    
    func startLoading() {
        
    }
    
    func finishLoading() {
        
    }
    
    func updateTitleHeader() {
        let header = HeaderData(
            imagen: #imageLiteral(resourceName: "logo_Cotizar"),
            titulo: "Cotizar",
            titulo2: "Planes",
            titulo3: nil,
            subtitulo: nil,
            closure:{ [weak self] in
                self?.padreController?.cambiarVista(FamiliasVC.self)
            }
        )
        super.updateTitle(headerData: header)
    }
    
    func cambioPlanSeleccionado(plan : SeleccionarPlanesModel){
        let header = HeaderData(
            imagen: #imageLiteral(resourceName: "logo_Cotizar"),
            titulo: "Cotizar",
            titulo2: "Planes",
            titulo3: nil,
            subtitulo: plan.nombre,
            closure:{ [weak self] in
                self?.padreController?.cambiarVista(FamiliasVC.self)
            }
        )
        super.updateTitle(headerData: header)
        cambiarVistaDetallePlan(index: 1)
    }
    
    func cambiarVistaDetallePlan(index: Int){
        cambiarVistaChild(index: index)
    }
    
}
