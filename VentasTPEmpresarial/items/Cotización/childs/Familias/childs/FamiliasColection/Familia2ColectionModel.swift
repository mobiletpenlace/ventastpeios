//
//  FamiliasModel.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 13/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct Familia2ColectionModel {
    let id: String
    let nombre: String
    let tipo: String
}
