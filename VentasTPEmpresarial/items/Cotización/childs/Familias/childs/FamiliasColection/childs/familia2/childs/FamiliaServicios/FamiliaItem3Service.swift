//
//  CollectionFamilia2Service.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 03/12/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

class FamiliaItem3Service{
    let dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()//obtener administrador de bd
    }
    
    func getData(_ callBack:@escaping ([FamiliaItem3Model]) -> Void){
        getDataFromDB(callBack)
    }
    
    func getDataFromDB(_ callBack:@escaping ([FamiliaItem3Model]) -> Void) {
        var data = [FamiliaItem3Model]()//crear lista de datos
        if let familias = dbManager.obtenerFamilias(tipo: "Servicios"){
            for familia in familias{
                var tipo = "1"
                if let planSM = dbManager.getPlanSolucionesAlAMedida(){
                    if familia.id == planSM.id!{
                        tipo = "2"
                    }
                }
                data.append(
                    FamiliaItem3Model(
                        id: familia.id!,
                        nombre: familia.nombre!,
                        tipo: tipo//tipo establecido
                    )
                )
            }
        }
        callBack(data)
    }
    
    func getPlazo() -> String{
        return dbManager.getPlazo()
    }
}
