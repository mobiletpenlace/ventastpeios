//
//  CollectionFamilia2VC.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 30/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class FamiliaItem1VC: BaseItemVC{
    @IBOutlet weak var collectionView: UICollectionView!
    let nombreCelda = "Familia2ColectionCell"
    fileprivate let presenter = FamiliaItem1Presenter(service: FamiliaItem1Service())

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        presenter.attachView(view: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow() {
        super.viewDidShow()
        presenter.viewDidShow()
    }
    
    private func setUpView() {
        inicializarCollectionView()
        super.addShadowView(view: self.collectionView)
    }
    
    private func inicializarCollectionView() -> Void {
        let nib = UINib.init(nibName: nombreCelda, bundle: nil)
        self.collectionView.register(nib, forCellWithReuseIdentifier: nombreCelda)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    
}

extension FamiliaItem1VC: UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let celda = collectionView.cellForItem(at: indexPath){
            celda.animateBound(view: celda){[weak self] _ in
                self?.presenter.clickCell(index: indexPath.row)
            }
        }
    }
}

extension FamiliaItem1VC: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: nombreCelda, for: indexPath) as! Familia2ColectionCell
        let data = presenter.getItemIndex(index: indexPath.row)
        cell.displayContent(texto: data.nombre, tipo: data.tipo)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.getSize()
    }
}

extension FamiliaItem1VC: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = (collectionView.frame.size.width / 5) - 10
        let h = collectionView.frame.size.height - 5
        return CGSize(width: w, height:h)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}

extension FamiliaItem1VC: FamiliaItem1Delegate, BaseTreeDialogs{
    
    func avanzar(){
        padreController?.padreController?.padreController?.cambiarVista(index: 2)//soluciones a la medida
    }
    
    func seleccionarDropDown(pos: Int) {
        //eliminar este metodo
    }
    
    func updateCollection(){
        self.collectionView.reloadData()
    }
    
    func setEmptyProgress() {
    }
    
    func startLoading() {
        super.showLoading(view: self.view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
    func getPadreController() -> BaseTreeVC?{
        return padreController
    }
    
    func updateTitleHeader() {
        
    }
    
    func showMensajeAdvertencia(closureY: ((UIAlertAction) -> Void)?, closureN: ((UIAlertAction) -> Void)?){
        showAlertYesNo(
            titulo: "Advertencia",
            mensaje: "Los sitios que se han modelado, van a ser eliminados por el cambio de plazo.",
            vc: self, closureY: closureY, closureN: closureN)
    }
    
}
