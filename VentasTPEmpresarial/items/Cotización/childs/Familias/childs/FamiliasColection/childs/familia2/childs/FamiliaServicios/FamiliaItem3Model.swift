//
//  CollectionFamilia2Model.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 03/12/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct FamiliaItem3Model{
    let id: String
    let nombre: String
    let tipo: String
}
