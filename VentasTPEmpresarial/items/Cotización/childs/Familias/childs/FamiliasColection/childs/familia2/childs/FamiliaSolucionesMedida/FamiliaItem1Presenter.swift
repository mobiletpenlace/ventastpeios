//
//  CollectionFamilia2Presenter.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 30/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol FamiliaItem1Delegate: BaseDelegate {
    func updateCollection()
    func setEmptyProgress()
    func getPadreController() -> BaseTreeVC?
    func showMensajeAdvertencia(closureY: ((UIAlertAction) -> Void)?, closureN: ((UIAlertAction) -> Void)?)
    func avanzar()
}

class FamiliaItem1Presenter {
    fileprivate let service: FamiliaItem1Service
    weak fileprivate var view: FamiliaItem1Delegate?
    fileprivate var dataList = [FamiliaItem1Model]()
    var plazo: String? = nil
    
    init(service: FamiliaItem1Service){
        self.service = service
    }
    
    func attachView(view: FamiliaItem1Delegate){
        self.view = view
        addEventBusFunctions()
    }
    
    func addEventBusFunctions(){
        SwiftEventBus.onMainThread(self, name: "CotizacionFamiliasItem1-cambioPlazo") {
            [weak self] result in
            if let plazo = result!.object as? String{
                self?.plazo = plazo
            }
        }
    }
    
    func viewDidAppear(){
        
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
        actualizarDatos()
    }
    
    func detachView() {
        view = nil
    }
    
    func getItemIndex(index: Int) -> FamiliaItem1Model {
        return dataList[index]
    }
    
    func getSize() -> Int{
        return dataList.count
    }
    
    func clickCell(index: Int){
        if plazo != nil && plazo!.trim() != ""{
//            let data = dataList[index]
            view?.avanzar()
//            let IdFamiliaSeleccionada = data.id
//            SwiftEventBus.post(
//                "CotizacionPlanesSeleccionarPlan-cambiarFamilias",
//                sender: IdFamiliaSeleccionada
//            )
        }else{
            view?.showBannerAdvertencia(title: "Seleccione un plazo para continuar")
        }
    }
    
    func getPlazo() -> String{
        return service.getPlazo()
    }
    
    func actualizarDatos(){
        self.view?.startLoading()
        service.getData{[weak self] data in
            self?.onFinishGetData(data: data)
        }
    }
    
    func onFinishGetData( data: [FamiliaItem1Model]){
        view?.finishLoading()
        if(data.count == 0){
            dataList = [FamiliaItem1Model]()
            view?.updateCollection()
        }else{
            dataList = data
            view?.updateCollection()
        }
    }
}
