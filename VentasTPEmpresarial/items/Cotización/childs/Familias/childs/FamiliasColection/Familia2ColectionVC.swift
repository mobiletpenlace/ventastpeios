//
//  FamiliasColectionViewController.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 21/06/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class Familia2ColectionVC: BaseItemVC{
    @IBOutlet weak var mViewContainerFamilia1: UIView!
    @IBOutlet weak var mViewContainerFamilia2: UIView!
    @IBOutlet weak var mViewContainerFamilia3: UIView!
    fileprivate let presenter = Familia2ColectionPresenter(service: Familia2ColectionService())
    @IBOutlet weak var textFieldPlazos: UITextField!
    let dropManager = DropDownUtil()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        presenter.attachView(view: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow() {
        super.viewDidShow()
        presenter.viewDidShow()
    }
    
    private func setUpView() {
        dropManager.add(textfield: textFieldPlazos, data: presenter.getPlazosArray()){ [unowned self] (index: Int, item: String) in
            self.presenter.seleccionadoPlazo(plazo: item)
        }
        textFieldPlazos.delegate = dropManager
        super.addViewXIB(vc: FamiliaItem2VC(), container: mViewContainerFamilia1)
        super.addViewXIB(vc: FamiliaItem3VC(), container: mViewContainerFamilia2)
        super.addViewXIB(vc: FamiliaItem1VC(), container: mViewContainerFamilia3)
    }
    
}

extension Familia2ColectionVC: Familia2ColectionDelegate, BaseTreeDialogs{
    
    func showMensajeAdvertencia(closureY: ((UIAlertAction) -> Void)?, closureN: ((UIAlertAction) -> Void)?){
        showAlertYesNo(
            titulo: "Advertencia",
            mensaje: "Los sitios modelados que no cuenten con el nuevo plazo van a ser eliminados.",
            vc: self, closureY: closureY, closureN: closureN)
    }
    
    func updateTitleHeader() {
        
    }
    
    func startLoading() {
        super.showLoading(view: self.view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
    func seleccionarDropDown(pos: Int){
        dropManager.seleccionarDropDown(pos: pos, textFieldPlazos)
    }
    
    func clear(){
        textFieldPlazos.text = ""
    }
}
