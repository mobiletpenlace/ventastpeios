//
//  FamilyCell.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 21/06/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class Familia2ColectionCell: UICollectionViewCell {
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var iconoTriangulo: UIImageView!
    
    
    var tipo: String = "0"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        isSelected = false
    }
    
    override var isSelected: Bool {
        /*se cambia el color de la celda que ha sido seleccionada*/
        didSet {
            if isSelected {
                label1.textColor = .white
                self.backgroundColor  = Colores.selectionFamiliaCelda
            } else {
                self.backgroundColor  = UIColor.white
                label1.textColor = Colores.azulOscuro
            }
        }
    }
    
    func displayContent(texto: String, tipo: String) -> Void {
        label1.text = texto
        self.tipo = tipo
    }
}
