//
//  FamiliasPresenter.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 13/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol Familia2ColectionDelegate: BaseDelegate {
    func showMensajeAdvertencia(closureY: ((UIAlertAction) -> Void)?, closureN: ((UIAlertAction) -> Void)?)
    func seleccionarDropDown(pos: Int)
    func clear()
}

class Familia2ColectionPresenter{
    fileprivate let service: Familia2ColectionService
    weak fileprivate var view: Familia2ColectionDelegate?
    fileprivate var dataList = [Familia2ColectionModel]()
    let plazos = ["Sin plazo", "12", "18", "24", "36"]
    
    init(service: Familia2ColectionService){
        self.service = service
    }
    
    func attachView(view: Familia2ColectionDelegate){
        self.view = view
        addEventBusFunctions()
        actualizarPlazo()
    }
    
    func addEventBusFunctions(){
        SwiftEventBus.onMainThread(self, name: "All-clearData") { [weak self] result in
            self?.view?.clear()
        }
    }
    
    func viewDidAppear(){
        
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
        actualizarPlazo()
    }
    
    func detachView() {
        view = nil
    }
    
    func getPlazosArray() -> [String]{
        return plazos
    }
    
    private func getPosForItem(_ item: String) -> Int{
        for (pos, value) in plazos.enumerated(){
            if (value == item){
                return pos
            }
        }
        return -1
    }
    
    private func actualizarPlazo(){
        let posPlazo = getPosForItem(service.getPlazo())
        if (posPlazo >= 0){
            view?.seleccionarDropDown(pos: posPlazo)
            informarCambioPlazo(plazo: service.getPlazo())
        }else{
            informarCambioPlazo(plazo: "")
        }
    }
    
    func seleccionadoPlazo(plazo: String){
        let plazoActual = service.getPlazo()
        if plazoActual != plazo{
            if service.hayUnSitioCreado(){
                let closureYes:((UIAlertAction) -> Void)? = { [weak self] _ in
                    self?.service.seleccionarPlazo(plazo: plazo)
                    self?.actualizarPlazo()
                    self?.service.eliminarPlanesEnSitioDiferentePlazo()
                }
                let closureNo:((UIAlertAction) -> Void)? = { [weak self] _ in
                    self?.actualizarPlazo()
                }
                view?.showMensajeAdvertencia(closureY: closureYes, closureN: closureNo)
            }else{
                service.seleccionarPlazo(plazo: plazo)
            }
        }   
        actualizarPlazo()
    }
    
    func getPlazo() -> String{
        return service.getPlazo()
    }
    
    func informarCambioPlazo(plazo: String){
        SwiftEventBus.post("CotizacionFamiliasItem1-cambioPlazo", sender: plazo)
        SwiftEventBus.post("CotizacionFamiliasItem2-cambioPlazo", sender: plazo)
        SwiftEventBus.post("CotizacionFamiliasItem3-cambioPlazo", sender: plazo)
    }
    
}
