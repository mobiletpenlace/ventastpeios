//
//  FamiliasService.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 13/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

class Familia2ColectionService{
    let dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()//obtener administrador de bd
    }
    
    func seleccionarPlazo(plazo: String){
        dbManager.actualizarPlazoSeleccionado(plazo: plazo)
    }
    
    func getPlazo() -> String{
        return dbManager.getPlazo()
    }
    
    func hayUnSitioCreado() -> Bool{
        return dbManager.hayUnSitioCreado()
    }
    
    func eliminarPlanesEnSitioDiferentePlazo(){
        dbManager.eliminarPlanesEnSitioDiferentePlazo()
    }
}
