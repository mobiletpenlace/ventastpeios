//
//  ItemFamiliaViewController.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 14/06/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class FamiliasVC: BaseItemVC {
    @IBOutlet weak var vistaTabla: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSubViews()
    }
    
    func addSubViews() -> Void {
        super.addViewXIB(vc: Familia2ColectionVC(), container: vistaTabla)
    }
    
    override func viewDidShow() {
        super.viewDidShow()
        let header = HeaderData(
            imagen: #imageLiteral(resourceName: "logo_Cotizar"),
            titulo: "Cotizar",
            titulo2: "Familias",
            titulo3: nil,
            subtitulo: nil,
            closure: nil
        )
        super.updateTitle(headerData: header)
        SwiftEventBus.post(
            "CotizacionPlanesResumenCotizacion-desseleccionPlan",
            sender: nil
        )
    }
    
}
