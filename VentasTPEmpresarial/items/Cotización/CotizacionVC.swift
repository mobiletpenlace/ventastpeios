//
//  QuotationViewController.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 01/06/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class CotizacionVC: BaseMenuItemVC, TimeMeasureDelegate {
    @IBOutlet weak var viewContainer2: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tiempoInicio = iniciarTiempo()
        
        super.subscribe(nameItem: Constantes.Eventbus.Id.menuItemCotizacion)
        super.headerData = HeaderData(
            imagen: #imageLiteral(resourceName: "logo_Cotizar"),
            titulo: "Cotización",
            titulo2: nil,
            titulo3: nil,
            subtitulo: nil,
            closure: nil
        )
        addItems()
        medirTiempo(tipoInicio: tiempoInicio, nombre: "Cotización")
    }
    
    func addItems() -> Void {
        var viewControllers = [BaseItemVC]()
        viewControllers.append(FamiliasVC())
        viewControllers.append(PlanesVC())
        viewControllers.append(SolucionMedidaVC())
        super.addViewXIBList(view: viewContainer2, viewControllers: viewControllers)
    }
    
    override func viewDidShow() {
        super.viewDidShow()
    }
}
