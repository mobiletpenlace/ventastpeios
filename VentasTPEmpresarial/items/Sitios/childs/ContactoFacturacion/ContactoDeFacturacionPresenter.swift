//
//  ContactoDeFacturacionPresenter.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 26/12/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol ContactoDeFacturacionDelegate: BaseDelegate {
    func updateData()
    func cambiarPantallaDireccionFacturacion()
    func cambiarPantallaCrearCotizacion()
    func checkBoxEstaSeleccionado() -> Bool
    func clear()
}

class ContactoDeFacturacionPresenter {
    weak fileprivate var view: ContactoDeFacturacionDelegate?
    fileprivate let service: ContactoDeFacturacionService
    fileprivate var data: ContactoDeFacturacionModel!

    init(service: ContactoDeFacturacionService) {
        self.service = service
        data = ContactoDeFacturacionModel()
    }

    func attachView(view: ContactoDeFacturacionDelegate) {
        self.view = view
        SwiftEventBus.onMainThread(self, name: "All-clearData") { [weak self] _ in
            self?.view?.clear()
        }
    }

    func viewDidAppear() {

    }

    func viewDidShow() {
        view?.updateTitleHeader()
    }

    func detachView() {
        view = nil
    }

    func willHideView() {

    }

    func updateNombre(item: String) {
        data?.nombre = item
    }

    func updateApellidoPaterno(item: String) {
        data?.apellidoPaterno = item
    }

    func updateApellidoMaterno(item: String) {
        data?.apellidoMaterno = item
    }

    func updateTelefono(item: String) {
        data?.telefono = item
    }

    func updateCelular(item: String) {
        data?.celular = item
    }

    func updateCorreo(item: String) {
        data?.correo = item
    }
    func updateOtroCorreo(item: String) {
        data?.otroCorreo = item
    }

    func clickSiguiente() {
        let seleccionado = view!.checkBoxEstaSeleccionado()
        data.seleccionado = seleccionado
        SwiftEventBus.post("crearCotizacion(setContactoFact)", sender: data)

        if seleccionado {
            view?.cambiarPantallaCrearCotizacion()
        } else {
            view?.cambiarPantallaDireccionFacturacion()
        }
    }

}
