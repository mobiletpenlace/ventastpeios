//
//  ContactoDeFacturacionVC.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 19/12/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import DLRadioButton


class ContactoDeFacturacionVC: BaseItemVC {

    @IBOutlet weak var mViewDatosContactoFacturacion: UIView!
    @IBOutlet weak var textfieldNombre: UITextField!
    @IBOutlet weak var textfieldApellPaterno: UITextField!
    @IBOutlet weak var textfieldApellMaterno: UITextField!
    @IBOutlet weak var textfieldTelefono: UITextField!
    @IBOutlet weak var textfieldCelular: UITextField!
    @IBOutlet weak var textfieldCorreo: UITextField!
    @IBOutlet weak var textfieldOtroCorreo: UITextField!
    @IBOutlet weak var radioButton1: DLRadioButton!
    @IBOutlet weak var nextButton: UIButton!
    let validatorManager = ValidatorManager()
    fileprivate let presenter = ContactoDeFacturacionPresenter(service: ContactoDeFacturacionService())


    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUpView()
        super.addShadowView(view: mViewDatosContactoFacturacion)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }

    override func viewDidShow() {
        super.viewDidShow()
        presenter.viewDidShow()
        validatorManager.setUpTextfields()
        //clear()
    }

    private func setUpView() {
        radioButton1.isMultipleSelectionEnabled = true
        setUpValidator()
        setUPBeginEdit()
        setUPEndEdit()

    }

    func setUpValidator() {
        textfieldNombre.delegate = validatorManager
        textfieldApellPaterno.delegate = validatorManager
        textfieldApellMaterno.delegate = validatorManager
        textfieldTelefono.delegate = validatorManager
        textfieldCelular.delegate = validatorManager
        textfieldCorreo.delegate = validatorManager
        textfieldOtroCorreo.delegate = validatorManager

        validatorManager.addValidator(

            FieldValidator.textField(
                textfieldNombre, //TextField
                .required, //validación del texto mientras.
                .nombre, //tipo de caracteres validos
                "El campo no puede estar vacío"
            ),
            FieldValidator.textField(
                textfieldApellPaterno, //TexField
                .required, //validación del texto mientras.
                .nombre, //tipo de caracteres validos.
                "El campo no puede estar vacío"
            ),
            FieldValidator.textField(
                textfieldApellMaterno, //TexField
                .required, //validación del texto mientras.
                .nombre, //tipo de caracteres validos.
                "El campo no puede estar vacío"
            ),

            FieldValidator.textField(
                textfieldTelefono, //textfield
                .exact(10), //validación del texto mientras.
                .numero, // tipo de caracteres validos
                "El debe tener 10 números"
            ),
            FieldValidator.textField(
                textfieldCelular, //TextField
                .none, //validación del texto mientras.
                .numero, //tipo de caracter validos.
                ""
            ),
            FieldValidator.textField(
                textfieldOtroCorreo, // textfield
                .none, //validación del texto mientras.
                .all, // tipo de caracteres validos.
                " "

            ),

            FieldValidator.textField(
                textfieldCorreo, //textfield
                .regex(.email), //validacion del texto mientras.
                .all, // tipo de caracteres validos
                "Error en el formato de correo electronico"
            )


        )
    }

    func setUPBeginEdit() {
        validatorManager.addChangeEnterField(from: textfieldNombre, to: textfieldApellPaterno)
        validatorManager.addChangeEnterField(from: textfieldApellPaterno, to: textfieldApellMaterno)
        validatorManager.addChangeEnterField(from: textfieldApellMaterno, to: textfieldTelefono)
        validatorManager.addChangeEnterField(from: textfieldTelefono, to: textfieldCelular)
        validatorManager.addChangeEnterField(from: textfieldCelular, to: textfieldCorreo)
        validatorManager.addChangeEnterField(from: textfieldCorreo, to: textfieldOtroCorreo)
    }

    func setUPEndEdit() {
        validatorManager.addEndEditingFor(textfieldNombre) { [weak self]
            (textField) -> Void in
            let text = textField.text?.uppercased() ?? ""
            self?.presenter.updateNombre(item: text)
        }
        validatorManager.addEndEditingFor(textfieldApellPaterno) { [weak self]
            (textField) -> Void in
            let text = textField.text?.uppercased() ?? ""
            self?.presenter.updateApellidoPaterno(item: text)
        }

        validatorManager.addEndEditingFor(textfieldApellMaterno) { [weak self]
            (textField) -> Void in
            let text = textField.text?.uppercased() ?? ""
            self?.presenter.updateApellidoMaterno(item: text)
        }


        validatorManager.addEndEditingFor(textfieldTelefono) { [weak self]
            (textField) -> Void in
            let text = textField.text?.uppercased() ?? ""
            self?.presenter.updateTelefono(item: text)
        }


        validatorManager.addEndEditingFor(textfieldCelular) { [weak self]
            (textField) -> Void in
            let text = textField.text?.uppercased() ?? ""
            self?.presenter.updateCelular(item: text)
        }


        validatorManager.addEndEditingFor(textfieldCorreo) { [weak self]
            (textField) -> Void in
            let text = textField.text?.uppercased() ?? ""
            self?.presenter.updateCorreo(item: text)
        }


        validatorManager.addEndEditingFor(textfieldOtroCorreo) { [weak self]
            (textField) -> Void in
            let text = textField.text?.uppercased() ?? ""
            self?.presenter.updateOtroCorreo(item: text)
        }




    }

    @IBAction func clickSiguiente(_ sender: UIButton) {
        self.view.endEditing(true)
        sender.animateBound(view: sender) { [unowned self] _ in
            let validateTextFields = self.validatorManager.validate()
            if validateTextFields {
                self.presenter.clickSiguiente()
            } else {
                self.showBannerError(title: "Hay campos con errores.")
            }
        }
    }


    @IBAction func isCheckedAction(_ sender: Any) {
        if radioButton1.selectedButtons().count != 0 {
            nextButton.setTitle("Enviar", for: .normal)
        } else {
            nextButton.setTitle("Siguiente", for: .normal)
        }
    }

}

extension ContactoDeFacturacionVC: ContactoDeFacturacionDelegate {

    func updateData() {

    }

    func updateTitleHeader() {
        let header = HeaderData(
            imagen: #imageLiteral(resourceName: "logo_Sitio"),
            titulo: "Sitios",
            titulo2: "Crear Contrato",
            titulo3: nil,
            subtitulo: "Contacto de Facturación.",
            closure: { [weak self] in
                self?.padreController?.cambiarVista(DatosApoderadoLegalVC.self)
            }
        )
        super.updateTitle(headerData: header)
    }

    func startLoading() {
        super.showLoading(view: self.view)
    }

    func finishLoading() {
        super.hideLoading()
    }

    func checkBoxEstaSeleccionado() -> Bool {
        return radioButton1.selectedButtons().count > 0
    }

    func cambiarPantallaDireccionFacturacion() {
        padreController?.cambiarVista(DireccionDeFacturacionVC.self)
    }

    func cambiarPantallaCrearCotizacion() {
        padreController?.cambiarVista(CrearCotizacionVC.self)
    }

    func clear() {
        radioButton1.isSelected = false
        textfieldNombre.text = ""
        textfieldApellPaterno.text = ""
        textfieldApellMaterno.text = ""
        textfieldTelefono.text = ""
        textfieldCorreo.text = ""
        textfieldCelular.text = ""
        textfieldOtroCorreo.text = ""

    }
}



