//
//  ContactoDeFacturacionModel.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 26/12/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct ContactoDeFacturacionModel {
    var seleccionado: Bool = false
    var nombre: String = ""
    var apellidoPaterno: String = ""
    var apellidoMaterno: String = ""
    var telefono: String = ""
    var celular: String = ""
    var correo: String = ""
    var otroCorreo: String = ""
}
