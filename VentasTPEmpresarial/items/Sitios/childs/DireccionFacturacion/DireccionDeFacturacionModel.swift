//
//  DireccionDeFacturacionModel.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 26/12/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct DireccionDeFacturacionModel {
    var mismaDirInst: Bool = true
    var colonia: String = ""
    var delegacionMunicipio: String = ""
    var ciudad: String = ""
    var estado: String = ""
    var calle: String = ""
    var numExterior: String = ""
    var numInterior: String = ""
    var codigoPostalFacturacion: String = ""
}
