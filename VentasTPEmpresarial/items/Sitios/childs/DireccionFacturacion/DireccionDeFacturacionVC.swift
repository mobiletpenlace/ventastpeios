//
//  DireccionDeFacturacionVC.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 20/12/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class DireccionDeFacturacionVC: BaseItemVC {
    
    @IBOutlet weak var mViewDatosDireccionFacturacion: UIView!
    @IBOutlet weak var textfieldCalle: UITextField!
    @IBOutlet weak var textfieldNumExterior: UITextField!
    @IBOutlet weak var textfieldNumInterior: UITextField!
    @IBOutlet weak var textfieldColonia: UITextField!
    @IBOutlet weak var textfieldCP: UITextField!
    @IBOutlet weak var textfieldEstado: UITextField!
    @IBOutlet weak var textfieldCiudad: UITextField!
    @IBOutlet weak var textfieldDelegacionMunicipio: UITextField!
    
    let validatorManager = ValidatorManager()
    fileprivate let presenter = DireccionDeFacturacionPresenter(service: DireccionDeFacturacionService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUpView()
        super.addShadowView(view:mViewDatosDireccionFacturacion)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow(){
        super.viewDidShow()
        presenter.viewDidShow()
        validatorManager.setUpTextfields()
        //clear()
    }
    
    private func setUpView() {
        setUpValidator()
        setUPBeginEdit()
        setUPEndEdit()
        
    }
    
    func setUpValidator (){
        textfieldCalle.delegate = validatorManager
        textfieldNumExterior.delegate = validatorManager
        textfieldNumInterior.delegate = validatorManager
        textfieldColonia.delegate = validatorManager
        textfieldCP.delegate = validatorManager
        textfieldEstado.delegate = validatorManager
        textfieldCiudad.delegate = validatorManager
        textfieldDelegacionMunicipio.delegate = validatorManager
        
        validatorManager.addValidator(
            
            FieldValidator.textField(
                textfieldCalle,//TextField
                .required,//validación del texto mientras.
                .nombre,//tipo de caracteres validos
                "El campo no puede estar vacío"
                ),
            FieldValidator.textField(
                textfieldNumExterior,//TextField
                .required,//validación del texto mientras.
                .calleNumero,//tipo de caracter validos.
                "El campo no puede estar vacío"
                ),
            FieldValidator.textField(
                textfieldNumInterior,//TextField
                .none,//validación del texto mientras.
                .calleNumero,//tipo de caracter validos.
                ""
            ),
            
            FieldValidator.textField(
                textfieldColonia,//TextField
                .required,//validación del texto mientras.
                .nombre,//tipo de caracter validos.
                "El campo no puede estar vacío"
            ),
            
            FieldValidator.textField(
                textfieldCP, //textfield
                .exact(5), //validacion del texto mientras.
                .numero, // tipo de caracteres validos
                "El campo debe tener 5 números"
            ),
            
            FieldValidator.textField(
                textfieldEstado,//TextField
                .required,//validación del texto mientras.
                .nombre,//tipo de caracteres validos
                "El campo no puede estar vacío"
            ),
            
            FieldValidator.textField(
                textfieldCiudad,//TextField
                .required,//validación del texto mientras.
                .nombre,//tipo de caracteres validos
                "El campo no puede estar vacío"
            ),
            
            FieldValidator.textField(
                textfieldDelegacionMunicipio,//TextField
                .required,//validación del texto mientras.
                .nombre,//tipo de caracteres validos
                "El campo no puede estar vacío"
            )
        
        )
        
    }
    
    func setUPBeginEdit (){
        validatorManager.addChangeEnterField(from: textfieldCalle, to: textfieldNumExterior)
        validatorManager.addChangeEnterField(from: textfieldNumExterior, to: textfieldNumInterior)
        validatorManager.addChangeEnterField(from: textfieldNumInterior, to: textfieldColonia)
        validatorManager.addChangeEnterField(from: textfieldColonia, to: textfieldCP)
        validatorManager.addChangeEnterField(from: textfieldCP, to: textfieldEstado)
        validatorManager.addChangeEnterField(from: textfieldEstado, to: textfieldCiudad)
        validatorManager.addChangeEnterField(from: textfieldCiudad, to: textfieldDelegacionMunicipio)
    }
    
    func setUPEndEdit(){
        validatorManager.addEndEditingFor(textfieldCalle) { [weak self]
         (textField) -> (Void) in
         let text = textField.text?.uppercased() ?? ""
         self?.presenter.updateCalle(item: text)
         }
         validatorManager.addEndEditingFor(textfieldNumExterior) { [weak self]
         (textField) -> (Void) in
         let text = textField.text?.uppercased() ?? ""
         self?.presenter.updateNumeroExterior(item: text)
         }
         
         validatorManager.addEndEditingFor(textfieldNumInterior) { [weak self]
         (textField) -> (Void) in
         let text = textField.text?.uppercased() ?? ""
         self?.presenter.updateNumeroInterior(item: text)
         }
         
         
         validatorManager.addEndEditingFor(textfieldColonia) { [weak self]
         (textField) -> (Void) in
         let text = textField.text?.uppercased() ?? ""
         self?.presenter.updateColonia(item: text)
         }
         
         
         validatorManager.addEndEditingFor(textfieldCP) { [weak self]
         (textField) -> (Void) in
         let text = textField.text?.uppercased() ?? ""
         self?.presenter.updateCP(item: text)
         }
         
         
         validatorManager.addEndEditingFor(textfieldEstado) { [weak self]
         (textField) -> (Void) in
         let text = textField.text?.uppercased() ?? ""
         self?.presenter.updateEstado(item: text)
         }
         
         
         validatorManager.addEndEditingFor(textfieldCiudad) { [weak self]
         (textField) -> (Void) in
         let text = textField.text?.uppercased() ?? ""
         self?.presenter.updateCiudad(item: text)
         }
        
        validatorManager.addEndEditingFor(textfieldDelegacionMunicipio){ [weak self]
            (textField) -> (Void) in
            let text = textField.text?.uppercased() ?? ""
            self?.presenter.updateDelegacionMunicipio(item: text)
            
        }
         
        
    }
    
    
    @IBAction func isCheckedAction(_ sender: Any) {
        
    }
    
    @IBAction func clickSiguiente(_ sender: UIButton) {
        self.view.endEditing(true)
        sender.animateBound(view: sender){[unowned self] _ in
            let validateTextFields = self.validatorManager.validate()
            if validateTextFields{
                self.presenter.clickSiguiente()
            }else{
                self.showBannerError(title: "Hay campos con errores.")
            }
        }
    }
    
    
}



extension DireccionDeFacturacionVC: DireccionDeFacturacionDelegate{
    
    func updateData() {
        
    }
    
    func updateTitleHeader() {
        let header = HeaderData(
            imagen: #imageLiteral(resourceName: "logo_Sitio"),
            titulo: "Sitios",
            titulo2: "Crear Contrato",
            titulo3: nil,
            subtitulo: "Dirección de Facturación.",
            closure:{ [weak self] in
                self?.padreController?.cambiarVista(ContactoDeFacturacionVC.self)
            }
        )
        super.updateTitle(headerData: header)
    }
    
    func startLoading() {
        super.showLoading(view: self.view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
    func cambiarSiguienteVista() {
        padreController?.cambiarVista(CrearCotizacionVC.self)
    }
    
    func clear(){
        textfieldCalle.text = ""
        textfieldNumExterior.text = ""
        textfieldNumInterior.text = ""
        textfieldColonia.text = ""
        textfieldCP.text = ""
        textfieldEstado.text = ""
        textfieldCiudad.text = ""
        textfieldDelegacionMunicipio.text = ""
    }
}



