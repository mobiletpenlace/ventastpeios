//
//  DireccionDeFacturacionPresenter.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 26/12/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol DireccionDeFacturacionDelegate: BaseDelegate{
    func updateData()
    func cambiarSiguienteVista()
    func clear()
}

class DireccionDeFacturacionPresenter{
    weak fileprivate var view: DireccionDeFacturacionDelegate?
    fileprivate let service: DireccionDeFacturacionService
    fileprivate var data: DireccionDeFacturacionModel!
    
    init(service: DireccionDeFacturacionService){
        self.service = service
        data = DireccionDeFacturacionModel()
    }
    
    func attachView(view: DireccionDeFacturacionDelegate){
        self.view = view
        SwiftEventBus.onMainThread(self, name: "All-clearData") { [weak self] result in
            self?.view?.clear()
        }
    }
    
    func viewDidAppear(){
        
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
    }
    
    func detachView() {
        view = nil
    }
    
    func willHideView(){
        
    }
    
    func updateCalle(item: String){
        data?.calle = item
    }
    
    func updateNumeroExterior(item: String){
        data?.numExterior = item
    }
    
    func updateNumeroInterior(item: String){
        data?.numInterior = item
    }
    
    func updateColonia(item: String){
        data?.colonia = item
    }
    
    func updateCP(item: String){
        data?.codigoPostalFacturacion = item
    }
    
    func updateEstado(item: String){
        data?.estado = item
    }
    
    func updateCiudad(item: String){
        data?.ciudad = item
    }
    
    func updateDelegacionMunicipio(item: String){
        data?.delegacionMunicipio = item
    }
    
    func clickSiguiente(){
        SwiftEventBus.post("crearCotizacion(setDirFact)", sender: data)
        view?.cambiarSiguienteVista()
    }
    
}
