//
//  ItemDetalleSitioViewController.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 20/06/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class DetalleSitioVC: BaseItemVC{
    @IBOutlet weak var viewContainer2: UIView!
    @IBOutlet weak var viewResumenCotizacion: UIView!
    var opcion: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    private func setUpView(){
        super.addViewXIB(vc: DetalleSitios(), container: viewContainer2)
        super.addViewXIB(vc: ResumenCotizacionVC(), container: viewResumenCotizacion)
        super.addShadowView(view: viewResumenCotizacion)
        addEventBusFunctions()
    }
    
    func addEventBusFunctions(){
        SwiftEventBus.onMainThread(self, name: "sitios-detalleSitios-informarTipoRegreso") {
            [weak self] result in
            if let opcion = result!.object as? Int{
                self?.opcion = opcion
                self?.updateHeaderOption()
            }
        }
    }
    
    override func viewDidShow() {
        super.viewDidShow()
        updateHeaderOption()
        informarCambioEnTipoRegreso()
    }
    
    func informarCambioEnTipoRegreso(){
        SwiftEventBus.post("metodoPago(changeTypeReturnScreen)", sender: 1)
    }
    
    func updateHeaderOption(){
        let header = HeaderData(
            imagen: #imageLiteral(resourceName: "logo_Sitio"),
            titulo: "Sitios",
            titulo2: nil,
            titulo3: nil,
            subtitulo: "detalle de sitios",
            closure: nil
        )
        super.updateTitle(headerData: header)
    }
}
