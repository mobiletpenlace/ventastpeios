//
//  ResuemnCotizacionSitioViewController.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 03/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class ResumenCotizacionVC: BaseItemVC {
    @IBOutlet weak var labelRentaMensual: UILabel!
    @IBOutlet weak var labelCostoInstalacion: UILabel!
    @IBOutlet weak var labelAdicionales: UILabel!
    @IBOutlet weak var labelDescuento: UILabel!
    @IBOutlet weak var labelSubtotal: UILabel!
    @IBOutlet weak var labelImpuestos: UILabel!
    @IBOutlet weak var labelCargosUnicos: UILabel!
    @IBOutlet weak var labelTotal: UILabel!
    @IBOutlet weak var ContratoButton: UIButton!
    @IBOutlet weak var labelNombreOportunidad: UILabel!
    @IBOutlet weak var labelNombreCuenta: UILabel!
    
    fileprivate let presenter = ResumenCotizacionPresenter(service: ResumenCotizacionService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow() {
        super.viewDidShow()
        presenter.viewDidShow()
    }
    
    private func setUpView() {
        
    }
    
    @IBAction func clickCompartir(_ sender: UIButton) {
        self.view.endEditing(true)
        sender.animateBound(view: sender){ [weak self] _ in
            self?.presenter.clickCompartir()
        }
    }
    
    @IBAction func clickGenerarContrato(_ sender: UIButton) {
        self.view.endEditing(true)
        sender.animateBound(view: sender){ [weak self] _ in
            self?.presenter.clickGenerarContrato()
        }
    }
    
}

extension ResumenCotizacionVC: ResumenCotizacionDelegate, BaseFormatNumber{
    func factibility(_ factible: Bool) {
        if factible{
            self.ContratoButton.setTitle("Generar contrato", for: .normal)
        }
        else{
            self.ContratoButton.setTitle("Enviar a factibilidad", for: .normal)
        }
    }
    
    
    func updateData(){
        labelRentaMensual.text = getPrecio(0)
        labelCostoInstalacion.text = getPrecio(1)
        labelAdicionales.text = getPrecio(2)
        labelDescuento.text = getPrecio(3)
        labelSubtotal.text = getPrecio(4)
        labelImpuestos.text = getPrecio(5)
        labelCargosUnicos.text = getPrecio(6)
        labelTotal.text = getPrecio(7)
    }
    
    private func getPrecio(_ index: Int) -> String{
        let precio = presenter.getItemIndex(index: index)?.precio ?? 0.0
        return "$" + numeroAFormatoPrecio(numero: precio)
    }
    
    func startLoading() {
        super.showLoading(view: self.view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
    func updateTitleHeader() {
        
    }
    
    func cambiarSiguienteVista(){
        padreController?.padreController?.cambiarVista(EnvioCotizacionVC.self)
    }
    
    func actualizarTituloResumen(_ oportunidadNombre: String, _ cuentaNombre: String){
        labelNombreOportunidad.text = oportunidadNombre
        labelNombreCuenta.text = cuentaNombre
    }
}

