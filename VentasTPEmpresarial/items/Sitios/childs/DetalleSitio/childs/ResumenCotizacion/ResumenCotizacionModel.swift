//
//  ResumenCotizacionModel.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 16/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct ResumenCotizacionModel {
    let titulo: String
    let precio: Float
}
