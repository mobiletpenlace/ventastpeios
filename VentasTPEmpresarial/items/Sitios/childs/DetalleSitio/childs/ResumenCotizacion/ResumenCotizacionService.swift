//
//  ResumenCotizacionService.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 16/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

class ResumenCotizacionService: BaseDBManagerDelegate, BaseFormatDate {
    var serverManager: ServerDataManager?
    var dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()
        serverManager = ServerDataManager()
    }
    
    func getDataFromDB(_ callBack:@escaping ([ResumenCotizacionModel]) -> Void) {
        var data = [ResumenCotizacionModel]()//crear lista de datos
        if let resumenCotizacion = dbManager.getResumenCotizacionSitio(){
            data.append(
                ResumenCotizacionModel(titulo: "Renta mensual s/i", precio: resumenCotizacion.costoRentaMensual)
            )
            data.append(
                ResumenCotizacionModel(titulo: "Costo de instalacion", precio: resumenCotizacion.costoInstalacion)
            )
            data.append(
                ResumenCotizacionModel(titulo: "Adicionales", precio: resumenCotizacion.costoAdicionales)
            )
            data.append(
                ResumenCotizacionModel(titulo: "Descuento", precio: resumenCotizacion.descuento)
            )
            data.append(
                ResumenCotizacionModel(titulo: "Sub Total", precio: resumenCotizacion.subTotal)
            )
            data.append(
                ResumenCotizacionModel(titulo: "Impuestos", precio: resumenCotizacion.impuestos)
            )
            data.append(
                ResumenCotizacionModel(titulo: "Total cargos únicos", precio: resumenCotizacion.cargosUnicos)
            )
            data.append(
                ResumenCotizacionModel(titulo: "Total", precio: resumenCotizacion.total)
            )
        }
        callBack(data)
    }
    
    func validarSitiosConcobertura() -> Int{
        return dbManager.validarSitiosConcobertura()
    }
    
    func validarSitiosConPlanes() -> Int{
        return dbManager.validarSitiosConPlanes()
    }
    
    func noHaysitios() -> Bool{
        return dbManager.noHaysitios()
    }
    
}
