//
//  ResumenCotizacionPresenter.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 16/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol ResumenCotizacionDelegate: BaseDelegate {
    func updateData()
    func cambiarSiguienteVista()
    func factibility(_ factible : Bool)
    func actualizarTituloResumen(_ oportunidadNombre: String, _ cuentaNombre: String)
}

class ResumenCotizacionPresenter{
    fileprivate var view: ResumenCotizacionDelegate?
    fileprivate var dataList = [ResumenCotizacionModel]()
    fileprivate let service: ResumenCotizacionService
    fileprivate let serviceCot = CrearCotizacionService()
    var oportunidadId: String?
    var oportunidadNombre: String?
    var newAccountId: String?
    var cuentaNombre: String?
    
    init(service: ResumenCotizacionService){
        self.service = service
    }
    
    func attachView(view: ResumenCotizacionDelegate){
        self.view = view
        SwiftEventBus.onMainThread(self, name: "Cotizacion_DetalleSitio_Resuemncotizacion(actualizar)") {
            [weak self] result in
            self?.actualizarDatos()
        }
        
        SwiftEventBus.onMainThread(self, name: "Boton_Factibilidad") {
            [weak self] result in
            self?.view?.factibility(result?.object as! Bool)
        }
        
        SwiftEventBus.onMainThread(self, name: "Cotizacion_DetalleSitio_Resumencotizacion(setdatosOportunidad)") {
            [unowned self] result in
            if let (oportunidadId, oportunidadNombre, newAccountId, cuentaNombre) = result?.object as? (String, String, String, String){
                self.oportunidadId = oportunidadId
                self.oportunidadNombre = oportunidadNombre
                self.newAccountId = newAccountId
                self.cuentaNombre = cuentaNombre
                self.view?.actualizarTituloResumen(oportunidadNombre, cuentaNombre)
            }
        }
        
        SwiftEventBus.onMainThread(self, name: "All-clearData") { [weak self] result in
            self?.borrarIdSeleccionadoProspecto()
            self?.view?.actualizarTituloResumen("", "")
        }
    }
    
    func viewDidAppear(){
        
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
        actualizarDatos()
    }
    
    func detachView() {
        view = nil
    }
    
    func getItemIndex(index: Int) -> ResumenCotizacionModel?{
        if getSize() == 0 {
            return nil
        }
        return dataList[index]
    }
    
    
    func getSize() -> Int{
        return dataList.count
    }
    
    func actualizarDatos(){
        self.view?.startLoading()
        service.getDataFromDB(){ [weak self] data in
            self?.onFinishGetData(data: data)
        }
    }
    
    func onFinishGetData( data: [ResumenCotizacionModel]){
        view?.finishLoading()
        if (data.count == 0){
            dataList = [ResumenCotizacionModel]()
        }else{
            dataList = data
        }
        view?.updateData()
    }
    
    func clickCompartir(){
        
    }
    
    private func informarCeldaEventBus(pos: Int){
        SwiftEventBus.post(
            "Cotizacion_DetalleSitio_DetalleSitios(seleccionarCelda)",
            sender: pos
        )
    }
    
    func clickGenerarContrato(){
        let posicionCobertura = service.validarSitiosConcobertura()
        let posicionPlanes = service.validarSitiosConPlanes()
        guard !(service.noHaysitios()) else {
            view?.showBannerAdvertencia(title: "No hay sitios.")
            return
        }
        guard posicionCobertura == -1 else {
            view?.showBannerAdvertencia(title: "Falta validar cobertura de algunos sitios.")
            return
        }
        guard posicionPlanes == -1 else {
            view?.showBannerAdvertencia(title: "Hay sitios sin planes.")
            return
        }
        
        guard oportunidadId != nil && oportunidadNombre != nil && newAccountId != nil else {
            view?.showBannerAdvertencia(title: "Falta seleccionar una venta para agregar la cotización.")
            SwiftEventBus.post("menuTabBar-cambiarMenuItem", sender: (1, 2))
            return
        }
        view?.cambiarSiguienteVista()
    }
    
    func borrarIdSeleccionadoProspecto(){
        oportunidadId = nil
        oportunidadNombre = nil
        newAccountId = nil
        cuentaNombre = nil
    }
}
