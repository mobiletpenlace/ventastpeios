//
//  DetalleSitiosService.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 05/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import RealmSwift

class DetalleSitiosService: BaseDBManagerDelegate {
    var serverManager: ServerDataManager?
    var realm: Realm!
    var dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()
        realm = getRealm()
    }
    
    func getData(_ callBack:@escaping ([DetalleSitiosModel]) -> Void){
        getDataFromDB(callBack)
    }

    func getDataFromDB( _ callBack:@escaping ([DetalleSitiosModel]) -> Void) {
        var data = [DetalleSitiosModel]()//crear lista de datos
        //dbManager.reloadRealm()
        let sitios = dbManager.obtenerSitios()
        for sitio in sitios{
            var planes = [SitioPlanModel]()
            for plan in sitio.planesConfig{
                planes.append(
                    SitioPlanModel(
                        id: plan.id,
                        nombre: plan.plan.nombre ?? ""
                    )
                )
            }
            var imagen: String = "img_cobertura_nose"
            if let cobertura = sitio.cobertura{
                if (cobertura.cobertura == true){
                    imagen = "img_cobertura"
                }else{
                    imagen = "img_no_cobertura"
                }
            }
            data.append(
                DetalleSitiosModel(
                    id: sitio.id,
                    name: sitio.nombre ?? "",
                    planes: planes,
                    imagen: imagen,
                    precio: sitio.total
                )
            )
        }
        callBack(data)
    }
    
    func agregarSitioNuevo(){
        dbManager.agregarSitioNuevo()
    }
    
    func agregarSitio(sitio: DetalleSitiosModel){
        dbManager.agregarSitio(id: sitio.id, nombre: sitio.name)
    }
    
    func agregarPlanASitio(sitio sitioP: DetalleSitiosModel){
        dbManager.agregarPlanASitio(idSitio: sitioP.id)
    }
    
    func modificarPlanSitio( sitio sitioP: DetalleSitiosModel, plan: SitioPlanModel){
        dbManager.modificarPlanSitio(idConfigPlan: plan.id, idSitio: sitioP.id)
    }
    
    func eliminarPlanSitio( sitio sitioP: DetalleSitiosModel, plan: SitioPlanModel){
        dbManager.eliminarPlanSitio(idConfigPlan: plan.id, idSitio: sitioP.id)
    }
    
    func eliminarSitio(sitio sitioP: DetalleSitiosModel){
        dbManager.eliminarSitio(idSitio: sitioP.id)
    }
    
    func getMaxIndexID() -> Int{
        //dbManager.reloadRealm()
        let sitios = realm.objects(DBSitio.self)
        let id = sitios.max(ofProperty: "id") as Int? ?? 0
        return id
    }
}
