//
//  DetalleSitiosPresenter.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 20/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol DetalleSitiosDelegate: BaseDelegate {
    func getPadreVC() -> BaseTreeVC?
    func reloadTable()
    func seleccionarCeldaTabla(indexPath: IndexPath)
    func mostrarDialogoEditarPlan(_ lista: [String], tipo: DialogEditaPlanes.TIPO)
    func reloadRows(indexPath: [IndexPath])
    func mostrarDialogoDetallePlan()
    func showEditar(index: IndexPath)
    func cargaMasivaChange()
    func changeToValidate()
    func PlanesSinCovertura()
}

class DetalleSitiosPresenter{
    weak fileprivate var view: DetalleSitiosDelegate?
    fileprivate let service: DetalleSitiosService
    fileprivate let serviceTipo: ResumenCotizacionPlanService
    var indexSeleccionado: Int = 0
    var posicionEditarPlan: Int = -1
    fileprivate var dataList = [DetalleSitiosModel]()
    var tipo : Constantes.Enum.TipoEditar = .none
    var planSeleccionado: SeleccionarPlanesModel?
    var count2: Int = 0
    var count : Int = 0
    
    init(service: DetalleSitiosService, service2: ResumenCotizacionPlanService){
        self.service = service
        self.serviceTipo = service2
    }
    
    func attachView(view: DetalleSitiosDelegate){
        self.view = view
        addEventBusFunctions()
    }
    
    func addEventBusFunctions(){
        SwiftEventBus.onMainThread(self, name: "DetalleSitio_Editar") { [weak self] result in
            self?.view?.showEditar(index: result?.object as! IndexPath)
        }
        
        SwiftEventBus.onMainThread(self, name: "Cotizacion_DetalleSitio_DetalleSitios(actualizar)") {
            [weak self] result in
            self?.actualizarDatos()
        }
        
        SwiftEventBus.onMainThread(self, name: "Cotizacion_DetalleSitio_DetalleSitios(cambioTipoEdicion)") {
            [weak self] result in
            if let tipoSeleccionado = result!.object as? Constantes.Enum.TipoEditar{
                self?.tipo = tipoSeleccionado
            }
        }
        
        SwiftEventBus.onMainThread(self, name: "Cotizacion_DetalleSitio_DetalleSitios(cambioPlanSeleccionado)") {
            [weak self] result in
            if let plan = result!.object as? SeleccionarPlanesModel{
                Logger.d("se ha pasado un plan seleccionado a los sitios")
                self?.planSeleccionado = plan
            }
        }
        
        SwiftEventBus.onMainThread(self, name: "Cotizacion_DetalleSitio_DetalleSitios(actualizar)") {
            [weak self] _ in
            self?.actualizarDatos()
        }
        
        SwiftEventBus.onMainThread(self, name: "Cotizacion_DetalleSitio_DetalleSitios(seleccionarCelda)") {
            [weak self] result in
            if let pos = result!.object as? Int{
                let indexPath = IndexPath(row: pos, section: 0)
                self?.view?.seleccionarCeldaTabla(indexPath: indexPath)
            }
        }
        
    }
    
    func viewDidAppear(){
    }
    
    /*protocol*/
    func viewWillAppear() {
        
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
        validarDatosDePlanSeleccionado()
        actualizarDatos()
    }
    
    func cambiarAMenuCotizacion(){
        SwiftEventBus.post("menuTabBar-cambiarMenuItem", sender: (3, 0))
    }
    
    private func validarDatosDePlanSeleccionado(){
        if planSeleccionado != nil {
            switch tipo{
            case .none:
                Logger.d("default al plan seleccionado")
            case .nuevo:
                let maxId = service.getMaxIndexID()
                crearNuevoSitioConPlan(" nuevo sitio ( \(maxId) )", planSeleccionado!.id, planSeleccionado!.nombre)
            case .agregar:
                agregarPlanASitio(indexSeleccionado, planSeleccionado!.nombre, planSeleccionado!.id)
            case .actualizar:
                if (posicionEditarPlan >= 0){
                    modificarPlanDeSitio(indexSeleccionado, posicionEditarPlan, planSeleccionado!.nombre, planSeleccionado!.id)
                }
            case .eliminar:
                Logger.i("se ha llamado a eliminar en sitios desde cotización, pero no hace nada.")
            }
            informarModificacionPlan(tipo: .Default)
            tipo = .none
        }else{
            Logger.i("detalleSitios(planseleccionado) es nil")
        }
    }
    
    func agregarPlanASitio(_ indexSeleccionado: Int, _ namePlan: String, _ id: String){
        let delayTime = DispatchTime.now() + Double(Int64(0.3 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            self.service.agregarPlanASitio(sitio: self.dataList[indexSeleccionado])
            self.view?.showBannerOK(title: "Plan agregado al sitio.")
            self.informarResumenCotizacionActualizar()
            self.actualizarDatos()
            
            let indexPath = IndexPath(row: indexSeleccionado, section: 0)
            self.view?.seleccionarCeldaTabla(indexPath: indexPath)
        }
    }
    
    func detachView() {
        view = nil
    }
    
    func clickBotonNuevoSitio(){
        crearNuevoSitio()
    }
    
    func crearNuevoSitio(){
        service.agregarSitioNuevo()
        view?.showBannerOK(title: "Sitio vacio creado.")
        self.informarResumenCotizacionActualizar()
        actualizarDatos()
        let indexPath = IndexPath(row: (dataList.count - 1), section: 0)
        view?.seleccionarCeldaTabla(indexPath: indexPath)
    }
    
    func crearNuevoSitioConPlan(_ name: String,_ idPlan: String?, _ planName: String?){
        let sitio = DetalleSitiosModel(
            id: nil,
            name: name,
            planes: [SitioPlanModel](),
            imagen: "logo_PreguntasFrecuentes",
            precio: 0.0
        )
        service.agregarSitio(sitio: sitio)
        view?.showBannerOK(title: "Sitio creado con el plan seleccionado.")
        self.informarResumenCotizacionActualizar()
        actualizarDatos()
        let indexPath = IndexPath(row: (dataList.count - 1), section: 0)
        view?.seleccionarCeldaTabla(indexPath: indexPath)
    }
    
    func cargaMasiva(){
        view?.cargaMasivaChange()
    }
    
    func informarModificacionPlan(tipo: ResumenCotizacionPlanPresenter.TipoEditar){
        SwiftEventBus.post("CotizacionSolucionesMedidaResumenCotizacionCustom-cambioTipoEdicion",sender: tipo)
        SwiftEventBus.post("CotizacionPlanesResumenCotizacion-cambioTipoEdicion",sender: tipo)
    }
    
    private func pasarIdSitioAPlan(indice: Int){
        let id = dataList[indice].id
        SwiftEventBus.post("CotizacionPlanesResumenCotizacion-setIdSitio",sender: id)
    }
    
    func clickAgregarPlanSitio(indice: Int){
        indexSeleccionado = indice
        pasarIdSitioAPlan(indice: indice)
        informarModificacionPlan(tipo: .Agregar)
        cambiarAMenuCotizacion()
    }
    
    func clickEditarPlanSitio(indice: Int){
        pasarIdSitioAPlan(indice: indice)
        showEditaDialogo(indice: indice, tipo: .editar)
    }
    
    func clickEliminarPlanSitio(indice: Int){
        showEditaDialogo(indice: indice, tipo: .eliminar)
    }
    
    private func showEditaDialogo(indice: Int, tipo: DialogEditaPlanes.TIPO){
        indexSeleccionado = indice
        var lista = [String]()
        for v in dataList[indexSeleccionado].planes{
            lista.append(v.nombre)
        }
        view?.mostrarDialogoEditarPlan(lista, tipo: tipo)
    }
    
    func isNormal(){
        SwiftEventBus.post("Resumen_editaSitio",sender: nil)
    }
    
    func informarResumenCotizacionActualizar(){
        SwiftEventBus.post(
            "Cotizacion_DetalleSitio_Resuemncotizacion(actualizar)",
            sender: tipo
        )
    }
    
    func eliminarPlanDeSitio(_ posicionSitio: Int, _ posicionPlan: Int){
        let sitio = dataList[posicionSitio]
        let plan = sitio.planes[posicionPlan]
        service.eliminarPlanSitio( sitio: sitio, plan: plan)
        informarResumenCotizacionActualizar()
        actualizarDatos()
    }
    
    func eliminarPlanDeSitioCovertura(_ posicionSitio: Int, _ posicionPlan: Int){
        let sitio = dataList[posicionSitio]
        let plan = sitio.planes[posicionPlan]
        service.eliminarPlanSitio( sitio: sitio, plan: plan)
        tipo = .eliminar
        informarResumenCotizacionActualizar()
    }
    
    func modificarPlanDeSitio(_ posicionSitio: Int, _ posicionPlan: Int, _ valor: String, _ id: String){
        let sitio = dataList[posicionSitio]
        let plan = sitio.planes[posicionPlan]
        service.modificarPlanSitio(sitio: sitio, plan: plan)
        view?.showBannerOK(title: "Plan actualizado.")
        informarResumenCotizacionActualizar()
        actualizarDatos()
    }
    
    
    func getItemIndex(index: Int) -> DetalleSitiosModel {
        return dataList[index]
    }
    
    func getSize() -> Int{
        return dataList.count
    }
    
    func eliminarSitio(index: Int){
        service.eliminarSitio(sitio: dataList[index])
        view?.showBannerOK(title: "Sitio eliminado.")
        informarResumenCotizacionActualizar()
        actualizarDatos()
    }
    
    func checkNormality(){
        var Contrato = true
        for sitio in dataList{
            if(sitio.imagen == "img_no_cobertura" || sitio.imagen == "img_cobertura_nose"){
                Contrato = false
            }
            for plan in sitio.planes{
                if(plan.nombre == "Solución a la Medida"){
                   Contrato = false
                }
            }
        }
        
        if !Contrato{
            SwiftEventBus.post("Boton_Factibilidad",sender: false)
        }else{
            SwiftEventBus.post("Boton_Factibilidad",sender: true)
        }
    }
    
    func convertirListaAString(array: [SitioPlanModel]) -> String{
        var texto = ""
        for v in array{
            texto += "\(v.nombre)\n"
        }
        if array.count == 0{
            texto = "(vacio)\n\n"
        }
        return texto
    }
    
    func validarCoberturaSitio(_ index: Int){
        let idSitio = dataList[index].id
        SwiftEventBus.post("CoberturaResumen-validarCobertura", sender: idSitio)
        SwiftEventBus.post("CoberturaResumen-validarCobertura-child", sender: nil)
        //SwiftEventBus.post("menuTabBar-cambiarItem", sender: 2)
        view?.changeToValidate()
    }
    
    func detallePlanes(_ index: Int){
        let idSitio = dataList[index].id!
        view?.mostrarDialogoDetallePlan()
        SwiftEventBus.post("DialogDetalleSitioResumenCotizacion(cambioSitio)", sender: idSitio)
    }
    
    func CheckCoverage() -> Bool{
        var noChanges = true
        for sitio in dataList{
            if (sitio.imagen == "img_no_cobertura"){
                count = 0
                for plan in sitio.planes{
                    if let tipo = serviceTipo.verificarTipoFamilia(configPlanId: plan.id){
                        if tipo == "Planes"{
                            self.eliminarPlanDeSitioCovertura(count2, count)
                            noChanges = false
                        }
                    }
                    count = count + 1
                }
            }
            count = 0
           count2 = count2 + 1
        }
        count2 = 0
        return noChanges
    }
    
    func borrarSitios(){
        count = 0
        for _ in dataList{
            service.eliminarSitio(sitio: dataList[count])
            count += 1
        }
        count = 0
        dataList = [DetalleSitiosModel]()
        view?.showBannerOK(title: "Se han eliminado todos los sitios.")
        view?.reloadTable()
    }
    
    func actualizarDatos(){
        self.view?.startLoading()
        service.getData(){[weak self] data in
            let delayTime = DispatchTime.now() + Double(Int64(0.3 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                self?.onFinishGetData(data: data)
            }
        }
    }

    func onFinishGetData( data: [DetalleSitiosModel]){
        self.checkNormality()
        view?.finishLoading()
        if(data.count == 0){
            dataList = [DetalleSitiosModel]()
        }else{
            dataList = data
        }
        if CheckCoverage(){
            
            view?.reloadTable()
        }else{
            actualizarDatos()
            view?.PlanesSinCovertura()
        }
        
    }
}

extension DetalleSitiosPresenter: CustomAlertViewDelegate{
    
    
    //delegate CustomAlertViewDelegate
    func clickOKButtonDialog(value: Any) {
        if let res = value as? [Any]{
            if let posicionPlan = res[0] as? Int{
                if let tipoModificacion = res[1] as? DialogEditaPlanes.TIPO{
                    switch tipoModificacion {
                    case .eliminar:
                        view?.showBannerOK(title: "Plan eliminado.")
                        eliminarPlanDeSitio(indexSeleccionado, posicionPlan)
                    case .editar:
                        posicionEditarPlan = posicionPlan
                        informarModificacionPlan(tipo: .Actualizar)
                        cambiarAMenuCotizacion()
                    }
                }
            }
        }
    }
    
    func clickCancelButtonDialog() {
        Logger.d("cancelado el dialogo")
    }
}
