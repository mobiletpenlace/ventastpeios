//
//  DetalleSitiosModel.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 05/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct DetalleSitiosModel {
    var id: Int?
    var name: String
    var planes: [SitioPlanModel]
    var imagen: String
    var precio: Float
}

struct SitioPlanModel{
    var id: Int
    let nombre: String
}
