//
//  TablaDetalleSitios.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 20/06/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class DetalleSitios: BaseItemVC {
    @IBOutlet weak var buttonNuevoSitio: UIButton!
    @IBOutlet weak var cargaMasivaButton: UIButton!
    @IBOutlet weak var borrarSitiosButton: UIButton!
    @IBOutlet weak var tableview: UITableView!
    var nombreCelda1: String = "DetalleSitiosCell"
    fileprivate let presenter = DetalleSitiosPresenter(service: DetalleSitiosService(), service2: ResumenCotizacionPlanService())
    var isFirst : Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUpView()
        presenter.checkNormality()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
        presenter.checkNormality()
    }
    
    func isEmpty(){ // revisar
        if(isFirst){
            isFirst = false
            presenter.actualizarDatos()
            let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                if(self.presenter.getSize() < 1){
                    self.mostrarPantallaCargaMasiva()
                }
            }
        }
    }
    
    func mostrarPantallaCargaMasiva(){
        padreController?.padreController?.cambiarVista(CoberturaResumenVC.self)
    }
    
    override func viewDidShow() {
        super.viewDidShow()
        
        isEmpty() // revisar estos
        presenter.checkNormality() // revisar estos
        
        presenter.viewDidShow()
    }
    
    private func setUpView() {
        cargarTableView()
        super.addBorder(view: buttonNuevoSitio)
        super.addRoundCorner(view: buttonNuevoSitio, value: 5)
        super.addBorder(view: cargaMasivaButton)
        super.addRoundCorner(view: cargaMasivaButton, value: 5)
        super.addBorder(view: borrarSitiosButton)
        super.addRoundCorner(view: borrarSitiosButton, value: 5)
    }
    
    func cargarTableView() -> Void {
        let nib = UINib.init(nibName: nombreCelda1, bundle: nil)
        self.tableview.register(nib, forCellReuseIdentifier: nombreCelda1)
        self.tableview.delegate = self
        self.tableview.dataSource = self
    }
    
    @IBAction func clickButtonNuevoSitio(_ sender: UIButton) {
        sender.animateScale(view: sender)
        presenter.clickBotonNuevoSitio()
    }
    
    @IBAction func cargaMasivaAction(_ sender: Any) {
        presenter.cargaMasiva()
    }
    
    @IBAction func borrarSitiosAction(_ sender: Any) {
        let alert = UIAlertController(
            title: "Borrar",
            message: "¿Seguro que quieres eliminar todos los sitios?",
            preferredStyle: .alert
        )
        let deleteAction = UIAlertAction(title: "SI", style: .default){
            [weak self] _ in
            self?.presenter.borrarSitios()
        }
        let cancelAction = UIAlertAction(title: "NO", style: .default, handler: nil)
        alert.addAction(cancelAction)
        alert.addAction(deleteAction)
        present(alert, animated: true)
    }
    
}

extension DetalleSitios: UITableViewDataSource, BaseFormatNumber{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: nombreCelda1, for: indexPath) as! DetalleSitiosCell
        let data =  presenter.getItemIndex(index: indexPath.row)
        cell.titulo.text = data.name
        cell.texto.text = presenter.convertirListaAString(array: data.planes)
        cell.imagenCobertura.image = UIImage(named: data.imagen)
        cell.precio.text = "$" + numeroAFormatoPrecio(numero: data.precio)
        cell.index = indexPath
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        var actions = [UITableViewRowAction]()
        var titulo = "🗑️ \n eliminar"
        let actionEliminar = UITableViewRowAction(style: .destructive, title: titulo) {
            _, indexPath in
            self.borrar(indexPath: indexPath)
        }
        titulo = "\u{22EE} \n más"
        let actionMas = UITableViewRowAction(style: .normal, title: titulo) {
            _, indexPath in
            self.editar(indexPath: indexPath)
        }
        actionEliminar.backgroundColor = UIColor(red: 200, green: 0, blue: 0)
        actionMas.backgroundColor = UIColor(red: 0, green: 0, blue: 100)
        actions.append(actionEliminar)
        return actions
    }
    
    func borrar(indexPath: IndexPath) {
        eliminarSitio(indexPath: indexPath)
//        let alert = UIAlertController(
//            title: "Borrar",
//            message: "¿Seguro que quieres eliminar el sitio?",
//            preferredStyle: .alert
//        )
//        let deleteAction = UIAlertAction(title: "SI", style: .default){
//            [weak self] _ in
//            self?.eliminarSitio(indexPath: indexPath)
//        }
//        let cancelAction = UIAlertAction(title: "NO", style: .default, handler: nil)
//        alert.addAction(cancelAction)
//        alert.addAction(deleteAction)
//        present(alert, animated: true)
    }
    
    func eliminarSitio(indexPath: IndexPath){
        presenter.eliminarSitio(index: indexPath.row)
    }
    
    func validarCoberturaSitio(indexPath: IndexPath){
        presenter.validarCoberturaSitio(indexPath.row)
    }
    
    func detallePlanes(indexPath: IndexPath){
        presenter.detallePlanes(indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getSize()
    }
}

extension DetalleSitios:UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return DetalleSitiosCellHeader().view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentCell = tableView.cellForRow(at: indexPath)! as! DetalleSitiosCell
        tableView.beginUpdates()
        currentCell.texto.isHidden = !(currentCell.texto.isHidden)
        tableView.endUpdates()
    }
}

extension DetalleSitios: DetalleSitiosDelegate{
    func PlanesSinCovertura() {
        super.showAlert(titulo: "Aviso", mensaje: "Un sitio sin cobertura solo puede contener planes de tipo servicios o soluciones a la medida. Los planes normales van a ser eliminados.")
    }
    
    func changeToValidate() {
        padreController?.padreController?.cambiarVista(CoberturaMapaVC.self)
    }
    
    func cargaMasivaChange() {
        padreController?.padreController?.cambiarVista(CoberturaResumenVC.self)
    }
    
    func showEditar(index: IndexPath) {
        editar(indexPath: index)
    }
    
    
    func getPadreVC() -> BaseTreeVC? {
        return padreController
    }
    
    func reloadTable(){
        tableview.reloadData()
        presenter.checkNormality()
    }
    
    func reloadRows(indexPath: [IndexPath]){
        tableview.reloadRows(at: indexPath, with: .left)
    }
    
    func seleccionarCeldaTabla(indexPath: IndexPath){
        let n = tableview.numberOfRows(inSection: 0)
        if n > 0{
            tableview.selectRow(at: indexPath, animated: true, scrollPosition: .bottom)
        }
    }
    
    func mostrarDialogoEditarPlan(_ lista: [String], tipo: DialogEditaPlanes.TIPO){
        super.showDialog(
            customAlert: DialogEditaPlanes(dataList: lista, tipo: tipo),
            delegate: presenter
        )
    }
    
    func updateTitleHeader() {
        
    }
    
    func startLoading() {
        super.showLoading(view: self.view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
    func mostrarDialogoDetallePlan(){
        super.showDialog(
            customAlert: DialogDetalleSitioResumenCotizacionViewController(),
            delegate: presenter
        )
    }
    
    func editar(indexPath: IndexPath) {
        let alert = UIAlertController(
            title: "Editar",
            message: "¿Que acción desea realizar?",
            preferredStyle: .alert
        )
        var titulo = "Añadir un nuevo plan"
        let action1 = UIAlertAction(title: titulo, style: .default){ [weak self] _ in
            self?.presenter.clickAgregarPlanSitio(indice: indexPath.row)
            self?.presenter.isNormal()
        }
        titulo = "Editar un plan"
        let action2 = UIAlertAction(title: titulo, style: .default){ [weak self] _ in
            self?.presenter.clickEditarPlanSitio(indice: indexPath.row)
            self?.presenter.isNormal()
        }
        titulo = "Eliminar un plan"
        let action3 = UIAlertAction(title: titulo, style: .default){ [weak self] _ in self?.presenter.clickEliminarPlanSitio(indice: indexPath.row)
        }
        titulo = "Validar cobertura"
        let action4 = UIAlertAction(title: titulo, style: .default){ [weak self] _ in self?.validarCoberturaSitio(indexPath: indexPath)
        }
        titulo = "Detalle planes"
        let action5 = UIAlertAction(title: titulo, style: .default){ [weak self] _ in self?.detallePlanes(indexPath: indexPath)
        }
        titulo = "Cancelar"
        let cancel = UIAlertAction(title: titulo, style: .destructive, handler: nil)
        alert.view.tintColor = UIColor.blue
        alert.view.layer.cornerRadius = 25
        
        alert.addAction(action4)
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(action3)
        alert.addAction(action5)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
}
