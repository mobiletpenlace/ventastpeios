//
//  ScannerCardViewController.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 02/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftBaseLibrary
import CreditCardForm
import SwiftEventBus

class EscanearTarjetaVC: BaseItemVC,CardIOViewDelegate {
    @IBOutlet weak var mScannerView: UIView!
    var cardIO: CardIOView? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func addCardIIO(){
        if cardIO == nil{
            cardIO = CardIOView()
            cardIO!.delegate = self
            mScannerView.addSubview(cardIO!)
            cardIO!.snp.makeConstraints { (make) -> Void in
                make.edges.equalTo(mScannerView).inset(UIEdgeInsetsMake(10, 10, 10, 10))
            }
        }
    }
    
    func deleteCardIO(){
        if cardIO != nil{
            cardIO?.removeFromSuperview()
            cardIO = nil
        }
    }
    
    override func viewDidShow() {
        super.viewDidShow()
        CardIOUtilities.preload()
        if !CardIOUtilities.canReadCardWithCamera(){
            super.showBannerError(title: "No se puede escanear con la cámara.")
        }else {
            addCardIIO()
        }
    }
    
    func cardIOView(_ cardIOView: CardIOView!, didScanCard cardInfo: CardIOCreditCardInfo!) {
        SwiftEventBus.post(
            "GenerarCarga-DatosTarjeta-InformacionTarjeta",
            sender: cardInfo
        )
        deleteCardIO()
        padreController?.cambiarVista(InformacionTarjeta2VC.self)
    }
    
    @IBAction func regresarAction(_ sender: Any) {
        padreController?.cambiarVista(InformacionTarjeta2VC.self)
    }
    
}
