//
//  InformacionTarjetaModel.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 14/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct InformacionTarjetaModel {
    var tipoTarjeta: String
    var numeroTarjeta: String
    var mesVencimiento: String
    var añoVencimiento: String
    var marKCard: String
    var nameCard: String
    var logoCard: UIImage
    var owner: String
    var tipoDePago: String
    var metodoPago: String
}



