//
//  InformacionTarjetaService.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 14/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

class InformacionTarjetaService: BaseDBManagerDelegate {
    var serverManager: ServerDataManager?
    var dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()
    }
    
    func insertarInformacionTarjeta(_ data : InformacionTarjetaModel, _ callBack:@escaping (Bool) -> Void) {
        dbManager.agregarMetodoPago(anio: data.añoVencimiento, digitosTarjeta: data.numeroTarjeta, mes: data.mesVencimiento, metodoPago: data.metodoPago, tipoTarjeta: data.tipoTarjeta)
         callBack(true)
    }
}
