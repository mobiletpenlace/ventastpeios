//
//  InformacionTarjetaPresenter.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 31/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol InformacionTarjetaDelegate: BaseDelegate {
    func update()
    func cambiarSiguienteVista()
    func seguirDatos()
    func flujoDiseño()
    func flujoNormal()
    func clear()
}

class InformacionTarjetaPresenter{
    fileprivate let service: InformacionTarjetaService
    fileprivate let servicePagoEfectivo: PagoEfectivoService
    weak fileprivate var view: InformacionTarjetaDelegate?
    fileprivate var data: InformacionTarjetaModel!
    fileprivate var dataTitular: DatosTitularModel!
    fileprivate var dataContacto : ContactoModel!
    //fileprivate var dataRepresentante : DatosRepresentanteLegalModel?
    var mTypeCard: [String] = ["Credito","Debito"]
    var mPayType: [String] = ["Prepago","Pospago"]
    var mMonths: [String] = ["01","02","03","04","05","06","07","09","10","11","12"]
    var mYears: [String] = ["2019","2020","2021","2022","2023","2024","2025"]
    var isTheSame : Bool = false
    
    init(service: InformacionTarjetaService){
        self.service = service
        servicePagoEfectivo = PagoEfectivoService()
        dataContacto = ContactoModel(
            Name: "",
            LastName: "",
            SecondLastName: "",
            Phone: "",
            Email: "",
            SecondEmail: "")
        data = InformacionTarjetaModel(
            tipoTarjeta: "",
            numeroTarjeta: "",
            mesVencimiento: "",
            añoVencimiento: "",
            marKCard: "",
            nameCard: "",
            logoCard: UIImage(),
            owner: "",
            tipoDePago: "",
            metodoPago: ""
        )
        dataTitular =  DatosTitularModel(nombre: "", apellidoPaterno: "", apellidoMaterno: "")
    }
    
    func attachView(view: InformacionTarjetaDelegate){
        self.view = view
        SwiftEventBus.onMainThread(self, name: "GenerarCarga-DatosTarjeta-InformacionTarjeta") {
            [weak self] result in
            self?.updateCardData(result!.object as! CardIOCreditCardInfo)
        }
        
        SwiftEventBus.onMainThread(self, name: "All-clearData") { [weak self] result in
            self?.view?.clear()
        }
    }
    
    func updateCardData(_ cardObj : CardIOCreditCardInfo){
        data.numeroTarjeta = cardObj.redactedCardNumber
        data.añoVencimiento = "\(cardObj.expiryYear)"
        data.mesVencimiento = "\(cardObj.expiryMonth)"
        validaNumero(cardObj.cardNumber)
        view?.update()
    }
    
    func validaNumero(_ numero: String) {
        var digitos = numero.compactMap{Int(String($0))}
        guard digitos.count > 0 else { return }
        let valor = "\(digitos[0])"
        if(valor == "4"){
            data.tipoTarjeta = "Visa"
            data.nameCard = "visa"
            data.logoCard = UIImage(named: "icon_TarjetaVisa")!
        }else if(valor == "3"){
            data.tipoTarjeta = "American Express"
            data.nameCard = "american express"
            data.logoCard = UIImage(named: "icon_TarjetaAmericanExpres")!
        }else if(valor == "5"){
            data.tipoTarjeta = "Mastercard"
            data.nameCard = "mastercard"
            data.logoCard = UIImage(named: "icon_TarjetaMastercard")!
        }
    }
    
    func viewDidAppear(){
        
    }
    
    func viewDidShow(){
        //        view?.updateTitleHeader()
        //        actualizarDatos()
    }
    
    func detachView() {
        view = nil
    }
    
    func getData() -> InformacionTarjetaModel{
        return data
    }
    
    func seleccionarTipoTarjeta(item: String){                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
        var metodoPago = ""
        if item == "Credito"{
            metodoPago = "TDC"
        }else if item == "Debito"{
            metodoPago = "TDD"
        }
        data?.metodoPago = metodoPago
    }
    
    func seleccionarMesTarjeta(item: String){
        data?.mesVencimiento = item
    }
    
    func seleccionarAñoTarjeta(item: String){
        data?.añoVencimiento = item
    }
    
    func updateMismaDireccionFacturacion(_ value: Bool){
        isTheSame = value
    }
    
    func seleccionaTipoDePago(item: String){
        data?.tipoDePago = item
    }
    
    func updateTarjeta(item: String){
        data.numeroTarjeta = item
    }
    
    func selectContactoName(item: String){
        dataContacto.Name = item
    }
    
    func selectContactoLastName(item: String){
        dataContacto.Name = item
    }
    
    func selectContactoSecondLastName(item: String){
        dataContacto.SecondLastName = item
    }
    
    func selectContactoPhone(item: String){
        dataContacto.Phone = item
    }
    
    func selectContactoEmail(item: String){
        dataContacto.Email = item
    }
    
    func selectContactoSecondEmail(item: String){
        dataContacto.SecondEmail = item
    }
    
    func getPayType() -> [String]{
        return self.mPayType
    }
    
    func clickSiguiente(){
        if isTheSame{
            view?.startLoading()
            service.insertarInformacionTarjeta(data){
                [unowned self] _ in
                self.view?.finishLoading()
                SwiftEventBus.post("crearCotizacion(setMismaDireccionMetodoPago)", sender: self.isTheSame)
                self.view?.cambiarSiguienteVista()
            }
        }else{
            self.view?.startLoading()
            service.insertarInformacionTarjeta(data){ [unowned self] _ in
                self.view?.finishLoading()
                SwiftEventBus.post("crearCotizacion(setMismaDireccionMetodoPago)", sender: self.isTheSame)
                self.view?.seguirDatos()
            }
        }
    }
    
    func getArrayTypeCard() -> [String]{
        return mTypeCard
    }
    
    func getArrayMonths() -> [String]{
        return mMonths
    }
    
    func getArrayYears() -> [String]{
        return mYears
    }
    
}
