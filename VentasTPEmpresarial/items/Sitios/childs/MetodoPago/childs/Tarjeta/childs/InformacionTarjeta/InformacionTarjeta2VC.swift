//
//  InformacionTarjeta2VC.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 20/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import DLRadioButton

class InformacionTarjeta2VC: BaseItemVC {
    @IBOutlet weak var textNumberCard: UITextField!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var textfieldTypeCard: UITextField!
    @IBOutlet weak var textfieldMonth: UITextField!
    @IBOutlet weak var textfieldYear: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var checkButtonDireccionFacturacion: DLRadioButton!
    
    let dropManager = DropDownUtil()
    let validatorManager = ValidatorManager()
    
    fileprivate let presenter = InformacionTarjetaPresenter(service: InformacionTarjetaService())
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        presenter.attachView(view: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow() {
        super.viewDidShow()
        presenter.viewDidShow()
        checkButtonDireccionFacturacion.isMultipleSelectionEnabled = true
        validatorManager.setUpTextfields()
        //clearData()
    }
    
    
    
    private func setUpView() {
        checkButtonDireccionFacturacion.isMultipleSelectionEnabled = true
        super.addShadowView(view: textNumberCard)
        super.addShadowView(view: textfieldTypeCard)
        super.addShadowView(view: textfieldMonth)
        super.addShadowView(view: textfieldYear)
        
        setUpValidator()
        setUPEndEdit()
        setUpDropDown()
        
    }
    
    func setUpDropDown(){
        textfieldTypeCard.delegate = dropManager
        textfieldMonth.delegate = dropManager
        textfieldYear.delegate = dropManager
        
        dropManager.add(textfield: textfieldTypeCard, data: presenter.getArrayTypeCard()){ [weak self] (index: Int, item: String) in
            self?.presenter.seleccionarTipoTarjeta(item: item)
        }
        
        dropManager.add(textfield: textfieldMonth, data: presenter.getArrayMonths()){ [weak self] (index: Int, item: String) in
            self?.presenter.seleccionarMesTarjeta(item: item)
        }
        
        dropManager.add(textfield: textfieldYear, data: presenter.getArrayYears()){ [weak self] (index: Int, item: String) in
            self?.presenter.seleccionarAñoTarjeta(item: item)
        }
    }
    
    func setUpValidator(){
        textNumberCard.delegate = validatorManager
        
        validatorManager.addValidator(
        
            FieldValidator.textField(
                textNumberCard, //textfield
                .exact(16), //validacion del texto mientras.
                .numero, // tipo de caracteres validos
                "El debe tener 16 números"
            )
        
        )
    }
    
    func setUPEndEdit(){
        validatorManager.addEndEditingFor(textNumberCard) { [weak self] (textField) -> (Void) in
            self?.presenter.validaNumero(textField.text ?? "")
        }
    }
    
    @IBAction func clickEscanearNuevamente(_ sender: UIButton) {
        padreController?.cambiarVista(index: 1)
    }
    
    @IBAction func clickEnviar(_ sender: UIButton) {
        self.view.endEditing(true)
        sender.animateBound(view: sender){[unowned self] _ in
            let validateDrop = self.dropManager.validate()
            let validateTextFields = self.validatorManager.validate()
            if validateDrop && validateTextFields{
                self.presenter.clickSiguiente()
            }else{
                self.showBannerError(title: "Hay campos con errores.")
            }
        }
    }
    
    @IBAction func isCheckedAction(_ sender: Any) {
        if(checkButtonDireccionFacturacion.selectedButtons().count != 0){
            nextButton.setTitle("Siguiente", for: .normal)
            presenter.updateMismaDireccionFacturacion(true)
        }else{
            nextButton.setTitle("Siguiente", for: .normal)
            presenter.updateMismaDireccionFacturacion(false)
        }
    }
}




extension InformacionTarjeta2VC: InformacionTarjetaDelegate{
    
    func cambiarSiguienteVista() {
        padreController?.padreController?.padreController?.cambiarVista(DatosApoderadoLegalVC.self)
    }
    
    func flujoDiseño() {
        messageLabel.isHidden = true
        checkButtonDireccionFacturacion.isHidden = true
    }
    
    func clear(){
        textNumberCard.text = ""
        textfieldTypeCard.text = ""
        textfieldMonth.text = ""
        textfieldYear.text = ""
        
    }
    
    func flujoNormal(){
        messageLabel.isHidden = false
        checkButtonDireccionFacturacion.isHidden = false
    }
    
    func seguirDatos() {
        padreController?.cambiarVista(DatosTitularTarjetaVC.self)
    }
    
    func update() {
        let data = presenter.getData()
        textfieldTypeCard.text = data.tipoTarjeta
        textNumberCard.text = data.numeroTarjeta
        textfieldMonth.text = data.mesVencimiento
        textfieldYear.text = data.añoVencimiento
    }
    
    func updateTitleHeader() {
        let header = HeaderData(
            imagen: #imageLiteral(resourceName: "logo_GenerarLead"),
            titulo: "Información de la tarjeta.",
            titulo2: nil,
            titulo3: nil,
            subtitulo: nil,
            closure:{[weak self] in
                self?.padreController?.padreController?.cambiarVista(DetalleSitioVC.self)
            }
        )
        super.updateTitle(headerData: header)
    }
    
    func startLoading() {
        super.showLoading(view: self.view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
}
