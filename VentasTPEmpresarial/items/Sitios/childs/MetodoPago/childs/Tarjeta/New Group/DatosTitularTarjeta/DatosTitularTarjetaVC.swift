//
//  DatosTitularTarjetaVC.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 01/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class DatosTitularTarjetaVC: BaseItemVC {
    
    @IBOutlet weak var mViewContainerDatosTitularTarjeta: UIView!
    @IBOutlet weak var nombreTextField: UITextField!
    @IBOutlet weak var apellidoPaternoTextField: UITextField!
    @IBOutlet weak var apellidoMaternoTextField: UITextField!
    
    fileprivate let presenter = DatosTitularPresenter(service: DatosTitularService(), servicePago : PagoEfectivoService())
    let validatorManager = ValidatorManager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        presenter.attachView(view: self)
        //super.addShadowView(view:mViewContainerDatosTitularTarjeta)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow() {
        super.viewDidShow()
        presenter.viewDidShow()
        validatorManager.setUpTextfields()
        //clear()
    }
    
    private func setUpView() {
        setUpValidator()
        setUPBeginEdit()
        setUPEndEdit()
        
    }
    
    func setUpValidator(){
        nombreTextField.delegate = validatorManager
        apellidoPaternoTextField.delegate = validatorManager
        apellidoMaternoTextField.delegate = validatorManager
        
        
        validatorManager.addValidator(
            
            FieldValidator.textField(
                nombreTextField, //textfield
                .required, //validacion del texto mientras.
                .nombre, // tipo de caracteres validos
                "El campo no puede estar vacío"
            ),
            
            FieldValidator.textField(
                apellidoPaternoTextField, //textfield
                .required, //validacion del texto mientras.
                .nombre, // tipo de caracteres validos
                "El campo no puede estar vacío"
            ),
            
            FieldValidator.textField(
                apellidoMaternoTextField, //textfield
                .required, //validacion del texto mientras.
                .nombre, // tipo de caracteres validos
                "El campo no puede estar vacío"
            )
            
        )
    }
    
    func setUPBeginEdit(){
        validatorManager.addChangeEnterField(from: nombreTextField, to: apellidoPaternoTextField)
        validatorManager.addChangeEnterField(from: apellidoPaternoTextField, to: apellidoMaternoTextField)
    }
    
    func setUPEndEdit(){
        
        validatorManager.addEndEditingFor(nombreTextField) { [weak self]
            (textField) -> (Void) in
            let text = textField.text?.uppercased() ?? ""
            self?.presenter.updateNombre(item: text)
        }
        validatorManager.addEndEditingFor(apellidoPaternoTextField) { [weak self]
            (textField) -> (Void) in
            let text = textField.text?.uppercased() ?? ""
            self?.presenter.updateApellidoPaterno(item: text)
        }
        
        validatorManager.addEndEditingFor(apellidoMaternoTextField) { [weak self]
            (textField) -> (Void) in
            let text = textField.text?.uppercased() ?? ""
            self?.presenter.updateApellidoMaterno(item: text)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.view.endEditing(true)
        presenter.backAction()
    }
    
    
    
    @IBAction func enviarAction(_ sender: UIButton) {
        self.view.endEditing(true)
        sender.animateBound(view: sender){[unowned self] _ in
            let validateTextFields = self.validatorManager.validate()
            if validateTextFields{
                self.presenter.clickSiguiente()
            }else{
                self.showBannerError(title: "Hay campos con errores.")
            }
        }
    }
    
}


extension DatosTitularTarjetaVC: DatosTitularDelegate{
    
    func cambiarSiguienteVista() {
        padreController?.padreController?.padreController?.cambiarVista(
            DatosApoderadoLegalVC.self)
    }
    
    func mostrarMensaje(_ mensaje: String, _ titulo: String) {
        super.showAlert(titulo: titulo, mensaje: mensaje)
    }
    
    func update() {
        
    }
    
    func updateTitleHeader() {
        let header = HeaderData(
            imagen:  #imageLiteral(resourceName: "logo_Sitio"),
            titulo: "Titular de la Tarjeta.",
            titulo2: nil,
            titulo3: nil,
            subtitulo: nil,
            closure:{[weak self] in
                self?.padreController?.cambiarVista(InformacionTarjeta2VC.self)
            }
        )
        super.updateTitle(headerData: header)
    }
    
    func startLoading() {
        super.showLoading(view: view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
    func regresar(){
        padreController?.cambiarVista(InformacionTarjeta2VC.self)
    }
    
    func clear(){
        nombreTextField.text = ""
        apellidoPaternoTextField.text = ""
        apellidoMaternoTextField.text = ""
    }
}

