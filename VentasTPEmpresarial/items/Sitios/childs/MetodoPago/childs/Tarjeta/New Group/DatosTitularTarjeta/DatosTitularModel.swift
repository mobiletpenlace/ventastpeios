//
//  DatosTitularModel.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 06/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct DatosTitularModel {
    var nombre: String
    var apellidoPaterno: String
    var apellidoMaterno: String
}
