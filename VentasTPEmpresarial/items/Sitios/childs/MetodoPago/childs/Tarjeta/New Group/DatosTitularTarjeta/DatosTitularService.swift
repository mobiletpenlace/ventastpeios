//
//  DatosTitularService.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 06/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

class DatosTitularService: BaseDBManagerDelegate {
    var serverManager: ServerDataManager?
    var dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()
    }
    
    func insertarInformacionTarjeta(_ data : DatosTitularModel){
        dbManager.actualizarTitularTarjeta(data.nombre, data.apellidoPaterno, data.apellidoMaterno)
    }
    
}
