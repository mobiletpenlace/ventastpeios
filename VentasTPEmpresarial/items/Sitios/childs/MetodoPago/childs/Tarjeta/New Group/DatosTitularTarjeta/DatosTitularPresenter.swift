//
//  DatosTitularPresenter.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 06/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol DatosTitularDelegate: BaseDelegate {
    func update()
    func mostrarMensaje(_ mensaje: String, _ titulo: String)
    func cambiarSiguienteVista()
    func regresar()
    func clear()
}

class DatosTitularPresenter{
    fileprivate let servicePagoEfectivo: PagoEfectivoService
    fileprivate let service: DatosTitularService
    weak fileprivate var view: DatosTitularDelegate?
    fileprivate var data: DatosTitularModel!
    
    init(service: DatosTitularService, servicePago: PagoEfectivoService){
        self.service = service
        self.servicePagoEfectivo = servicePago
        data = DatosTitularModel(
            nombre: "",
            apellidoPaterno: "",
            apellidoMaterno: ""
        )
    }
    
    func attachView(view: DatosTitularDelegate){
        self.view = view
        SwiftEventBus.onMainThread(self, name: "All-clearData") { [weak self] result in
            self?.view?.clear()
        }
    }
    
    func viewDidAppear(){
        
    }
    
    func viewDidShow(){
        //        view?.updateTitleHeader()
        //        actualizarDatos()
    }
    
    func detachView() {
        view = nil
    }
    
    
    func updateNombre(item: String){
        data?.nombre = item
    }
    
    func updateApellidoPaterno(item: String){
        data?.apellidoPaterno = item
    }
    
    func updateApellidoMaterno(item: String){
        data?.apellidoMaterno = item
    }
    
    func clickSiguiente(){
        service.insertarInformacionTarjeta(data)
        view?.cambiarSiguienteVista()
    }
    
    func backAction(){
        view?.regresar()
    }
}
