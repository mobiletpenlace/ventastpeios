//
//  PagoTarjetaVC.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 01/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class PagoTarjetaVC: BaseItemVC{
    @IBOutlet weak var mViewContainerCard: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.addShadowView(view:mViewContainerCard)
        let controllers = [
            InformacionTarjeta2VC(),
            EscanearTarjetaVC(),
            DatosTitularTarjetaVC()
        ]
        super.addViewXIBList(view:mViewContainerCard, viewControllers: controllers)
    }
    
    override func viewDidShow() {
        super.viewDidShow()
//        let header = HeaderData(
//            imagen: #imageLiteral(resourceName: "logo_GenerarLead"),
//            titulo: "Método de pago.",
//            subtitulo: nil,
//            closure:{ [weak self] in
//                self?.padreController?.cambiarVista(index: 5)
//            }
//        )
//        super.updateTitle(headerData: header)
    }
    
   
}
