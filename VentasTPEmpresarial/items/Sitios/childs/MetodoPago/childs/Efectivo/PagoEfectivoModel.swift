//
//  PagoEfectivoModel.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 05/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct PagoEfectivoModel {
    let anio = ""
    let apellidoMaternoTitular = ""
    let apellidoPaternoTitular = ""
    let digitosTarjeta = ""
    let mes = ""
    var metodoPago = "Efectivo"
    let nombreTitularTarjeta = ""
    let tipoTarjeta = ""
}
