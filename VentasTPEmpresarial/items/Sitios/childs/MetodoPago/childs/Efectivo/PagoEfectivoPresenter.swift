//
//  PagoEfectivoPresenter.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 05/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import SwiftEventBus

protocol PagoEfectivoDelegate: BaseDelegate {
    func cambiarSiguienteVista()
    func clean()
}

class PagoEfectivoPresenter{
    fileprivate let service: PagoEfectivoService
    weak fileprivate var view: PagoEfectivoDelegate?
    fileprivate var dataContacto : ContactoModel!
    fileprivate var data = PagoEfectivoModel()
    var tipoPago: String?
    var mPayType: [String] = ["Prepago","Pospago"]
    
    init(service: PagoEfectivoService){
        self.service = service
        dataContacto = ContactoModel(
            Name: "",
            LastName: "",
            SecondLastName: "",
            Phone: "",
            Email: "",
            SecondEmail: "")
    }
    
    func attachView(view: PagoEfectivoDelegate){
        self.view = view
    }
    
    func viewDidAppear(){
        
    }
    
    func getPayType() -> [String]{
        return self.mPayType
    }
    
    func seleccionaTipoDePago(item: String){
        self.tipoPago = item
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
    }
    
    func detachView() {
        view = nil
    }
    
    func selectContactoName(item: String){
        dataContacto.Name = item
    }
    
    func selectContactoLastName(item: String){
        dataContacto.LastName = item
    }
    
    func selectContactoSecondLastName(item: String){
        dataContacto.SecondLastName = item
    }
    
    func selectContactoPhone(item: String){
        dataContacto.Phone = item
    }
    
    func selectContactoEmail(item: String){
        dataContacto.Email = item
    }
    
    func selectContactoSecondEmail(item: String){
        dataContacto.SecondEmail = item
    }
    
    func onClickContinuar(){
        view?.startLoading()
        service.insertarMetodoPago(data){ [unowned self] _ in
            self.view?.finishLoading()
            self.view?.cambiarSiguienteVista()
        }
    }
}
