//
//  PagoEfectivoServicio.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 05/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct StructVentaOportunidadRes {
    let resultado: String
    let codigoOportunidad: String
    let listaTrabajos: [String]
}

class PagoEfectivoService: BaseDBManagerDelegate, BaseFormatDate {
    var serverManager: ServerDataManager?
    var dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()
        serverManager = ServerDataManager()
    }
    
    func insertarMetodoPago(_ datos: PagoEfectivoModel, _ callBack:@escaping (Bool) -> Void) {
        dbManager.agregarMetodoPago(
            anio: datos.anio,
            digitosTarjeta: datos.digitosTarjeta,
            mes: datos.mes,
            metodoPago: datos.metodoPago,
            tipoTarjeta: datos.tipoTarjeta
        )
        callBack(true)
    }
    
}
