//
//  PagoEfectivoVC.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 01/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class PagoEfectivoVC: BaseItemVC {
    fileprivate let presenter = PagoEfectivoPresenter(service: PagoEfectivoService())
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        presenter.attachView(view: self)
    }
    
    private func setUpView() {
        
    }
    
    override func viewDidShow() {
        super.viewDidShow()
    }
    
    @IBAction func onClickContinuar(_ sender: UIButton) {
        self.view.endEditing(true)
        self.presenter.onClickContinuar()
    }
    
}

extension PagoEfectivoVC: PagoEfectivoDelegate{
    
    func cambiarSiguienteVista() {
        padreController?.padreController?.cambiarVista(DatosApoderadoLegalVC.self)
    }
    
    func clean() {
        
    }
    
    func updateTitleHeader() {
        
    }
    
    func startLoading() {
        super.showLoading(view: self.view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
}

