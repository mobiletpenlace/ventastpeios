//
//  ContactoModel.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 12/12/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct ContactoModel {
    var Name : String
    var LastName : String
    var SecondLastName : String
    var Phone : String
    var Email : String
    var SecondEmail : String
}
