//
//  PagoTransferenciaVC.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 08/12/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit


class PagoTransferenciaVC: BaseItemVC {
    fileprivate let presenter = PagoTransferenciaPresenter(service: PagoEfectivoService())
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        presenter.attachView(view: self)
    }
    
    private func setUpView() {
        
    }
    
    override func viewDidShow() {
        super.viewDidShow()
    }
    
    @IBAction func onClickContinuar(_ sender: UIButton) {
        self.view.endEditing(true)
        self.presenter.onClickContinuar()
    }
    
}

extension PagoTransferenciaVC: PagoTransferenciaDelegate{
    
    func cambiarSiguienteVista() {
        padreController?.padreController?.cambiarVista(DatosApoderadoLegalVC.self)
    }
    
    func clean() {
        
    }
    
    func updateTitleHeader() {
        
    }
    
    func startLoading() {
        super.showLoading(view: self.view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
}



