//
//  PagoTransferenciaPresenter.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 08/12/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol PagoTransferenciaDelegate: BaseDelegate {
    func cambiarSiguienteVista()
    func clean()
}


class PagoTransferenciaPresenter{
    
    weak fileprivate var view: PagoTransferenciaDelegate?
    fileprivate let service: PagoEfectivoService!
    fileprivate var dataContacto : ContactoModel!
    fileprivate var data: PagoEfectivoModel!
    var tipoPago: String?
    var mPayType: [String] = ["Prepago","Pospago"]
    
    init(service: PagoEfectivoService){
        self.service = service
        
        dataContacto = ContactoModel(
            Name: "",
            LastName: "",
            SecondLastName: "",
            Phone: "",
            Email: "",
            SecondEmail: "")
        data = PagoEfectivoModel()
        data.metodoPago = "Transferencia"
    }
    
    func attachView(view: PagoTransferenciaDelegate){
        self.view = view
    }
    
    func viewDidAppear(){
        
    }
    
    func getPayType() -> [String]{
        return self.mPayType
    }
    
    func seleccionaTipoDePago(item: String){
        self.tipoPago = item
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
    }
    
    func detachView() {
        view = nil
    }
    
    func selectContactoName(item: String){
        dataContacto.Name = item
    }
    
    func selectContactoLastName(item: String){
        dataContacto.Name = item
    }
    
    func selectContactoSecondLastName(item: String){
        dataContacto.SecondLastName = item
    }
    
    func selectContactoPhone(item: String){
        dataContacto.Phone = item
    }
    
    func selectContactoEmail(item: String){
        dataContacto.Email = item
    }
    
    func selectContactoSecondEmail(item: String){
        dataContacto.SecondEmail = item
    }
    
    func onClickContinuar(){
        view?.startLoading()
        service.insertarMetodoPago(data){ [unowned self] _ in
            self.view?.finishLoading()
            self.view?.cambiarSiguienteVista()
        }
    }
}
