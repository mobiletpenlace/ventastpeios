//
//  DatosTarjeta2VC.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 01/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import UIKit
import SwiftEventBus

class MetodoPagoVC: BaseItemVC {
    @IBOutlet weak var mViewContainerCard: UIView!
    @IBOutlet weak var segmentedControl: CustomUISegmentedControl!
    var tipoRegreso = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.addShadowView(view:mViewContainerCard)
        let controllers = [
            PagoTarjetaVC(),
            PagoEfectivoVC(),
            PagoTransferenciaVC()
        ]
        super.addViewXIBList(view: mViewContainerCard, viewControllers: controllers)
        initEventBusfunctions()
    }
    
    func initEventBusfunctions(){
        SwiftEventBus.onMainThread(self, name: "metodoPago(changeChild)") {
            [unowned self] result in
            if let pos = result?.object as? Int{
                self.changeView(pos: pos)
            }
        }
        
        SwiftEventBus.onMainThread(self, name: "metodoPago(changeTypeReturnScreen)") {
            [unowned self] result in
            if let pos = result?.object as? Int{
                self.tipoRegreso = pos
            }
        }
    }
    
    func changeView(pos : Int){
        segmentedControl.selectedSegmentIndex = pos
        segmentedControl.sendActions(for: UIControlEvents.valueChanged)
    }
    
    @IBAction func selectedView(_ sender: CustomUISegmentedControl) {
        sender.changeUnderlinePosition(sender.currentIndex)
        super.cambiarVista(index: sender.selectedSegmentIndex)
    }
    
    override func viewDidShow() {
        super.viewDidShow()
//        let closure: (() -> Void)? = { [weak self] in
//            self?.padreController?.cambiarVista(DetalleSitioVC.self)
//        }
//        switch tipoRegreso {
//        case 0:
//            closure = { [weak self] in
//                self?.padreController?.cambiarVista(index: 0)
//            }
//        case 1:
//            closure = {
//                SwiftEventBus.post("menuTabBar-cambiarMenuItem", sender: (1, 2))
//            }
//        default:
//            closure = nil
//        }
        let header = HeaderData(
            imagen: #imageLiteral(resourceName: "logo_Sitio"),
            titulo: "Sitios",
            titulo2: "Crear Contrato",
            titulo3: nil,
            subtitulo: "Método de Pago.",
            closure: nil
        )
        super.updateTitle(headerData: header)
    }
 
    
}
