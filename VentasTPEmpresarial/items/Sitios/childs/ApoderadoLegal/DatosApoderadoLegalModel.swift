//
//  DatosApoderadoLegalModel.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 26/12/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct DatosApoderadoLegalModel {
    var seleccionado: Bool = false
    var nombre: String = ""
    var apellidoPaterno: String = ""
    var apellidoMaterno: String = ""
    var correo: String = ""
    var telefono: String = ""
    var celular: String = ""
    var tipoDePago: String
}
