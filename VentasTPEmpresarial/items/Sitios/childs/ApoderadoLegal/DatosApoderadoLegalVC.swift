//
//  DatosApoderadoLegalVC.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 20/12/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import DLRadioButton

class DatosApoderadoLegalVC: BaseItemVC {
    @IBOutlet weak var textfieldTipoPago: UITextField!
    @IBOutlet weak var textfieldNombre: UITextField!
    @IBOutlet weak var textfieldApellPaterno: UITextField!
    @IBOutlet weak var textfieldApellMaterno: UITextField!
    @IBOutlet weak var textfieldTelefono: UITextField!
    @IBOutlet weak var textfieldCorreo: UITextField!
    @IBOutlet weak var radioButton1: DLRadioButton!
    @IBOutlet weak var mViewDatosApoderado: UIView!
    @IBOutlet weak var checkButtonApoderadoLegal: DLRadioButton!
    @IBOutlet weak var nextButton: UIButton!

    let dropManager = DropDownUtil()
    let validatorManager = ValidatorManager()
    fileprivate let presenter = DatosApoderadoLegalPresenter(service: DatosApoderadoLegalService())

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUpView()
        super.addShadowView(view: mViewDatosApoderado)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }

    override func viewDidShow() {
        super.viewDidShow()
        presenter.viewDidShow()
        validatorManager.setUpTextfields()
        //clearData()
    }

    private func setUpView() {
        super.addShadowView(view: textfieldTipoPago)
        radioButton1.isMultipleSelectionEnabled = true
        setUpValidator()
        setUPBeginEdit()
        setUPEndEdit()
        setUpDropDown()
    }

    @IBAction func clickSiguiente(_ sender: UIButton) {
        self.view.endEditing(true)
        sender.animateBound(view: sender) { [unowned self] _ in
            let validateDrop = self.dropManager.validate()
            let validateTextFields = self.validatorManager.validate()
            if validateDrop && validateTextFields {
                self.presenter.clickSiguiente()
            } else {
                self.showBannerError(title: "Hay campos con errores.")
            }
        }
    }

    func setUpDropDown() {
        textfieldTipoPago.delegate = dropManager

        dropManager.add(textfield: textfieldTipoPago, data: presenter.getPayType()) { [weak self] (_, item: String) in
            self?.presenter.seleccionaTipoDePago(item: item)
        }

    }

    func setUpValidator() {
        textfieldNombre.delegate = validatorManager
        textfieldApellPaterno.delegate = validatorManager
        textfieldApellMaterno.delegate = validatorManager
        textfieldTelefono.delegate = validatorManager
        textfieldCorreo.delegate = validatorManager

        validatorManager.addValidator(

            FieldValidator.textField(
                textfieldNombre, //textfield
                .required, //validacion del texto mientras.
                .nombre, // tipo de caracteres validos
                "El campo no puede estar vacío"
            ),

            FieldValidator.textField(
                textfieldApellPaterno, //textfield
                .required, //validacion del texto mientras.
                .nombre, // tipo de caracteres validos
                "El campo no puede estar vacío"
            ),

            FieldValidator.textField(
                textfieldApellMaterno, //textfield
                .required, //validacion del texto mientras.
                .nombre, // tipo de caracteres validos
                "El campo no puede estar vacío"
            ),

            FieldValidator.textField(
                textfieldTelefono, //textfield
                .exact(10), //validacion del texto mientras.
                .numero, // tipo de caracteres validos
                "El debe tener 10 números"
            ),

            FieldValidator.textField(
                textfieldCorreo, //textfield
                .regex(.email), //validacion del texto mientras.
                .all, // tipo de caracteres validos
                "Error en el formato de correo electronico"
            )

        )

    }

    func setUPBeginEdit() {
        validatorManager.addChangeEnterField(from: textfieldNombre, to: textfieldApellPaterno)
        validatorManager.addChangeEnterField(from: textfieldApellPaterno, to: textfieldApellMaterno)
        validatorManager.addChangeEnterField(from: textfieldApellMaterno, to: textfieldTelefono)
        validatorManager.addChangeEnterField(from: textfieldTelefono, to: textfieldCorreo)

    }

    func setUPEndEdit() {
        validatorManager.addEndEditingFor(textfieldNombre) { [weak self] (textField) -> Void in
            self?.presenter.updateNombre(item: textField.text ?? "")
        }
        validatorManager.addEndEditingFor(textfieldApellPaterno) { [weak self] (textField) -> Void in
            self?.presenter.updateApellidoPaterno(item: textField.text ?? "")
        }
        validatorManager.addEndEditingFor(textfieldApellMaterno) { [weak self] (textField) -> Void in
            self?.presenter.updateApellidoMaterno(item: textField.text ?? "")
        }
        validatorManager.addEndEditingFor(textfieldTelefono) { [weak self] (textField) -> Void in
            self?.presenter.updateTelefono(item: textField.text ?? "")
        }
        validatorManager.addEndEditingFor(textfieldCorreo) { [weak self] (textField) -> Void in
            self?.presenter.updateCorreo(item: textField.text ?? "")
        }

        validatorManager.addEndEditingFor(textfieldTipoPago) { [weak self] (textField) -> Void in
            self?.presenter.seleccionaTipoDePago(item: textField.text ?? "")
        }

    }

    @IBAction func isCheckedAction(_ sender: Any) {
        if checkButtonApoderadoLegal.selectedButtons().count != 0 {
            nextButton.setTitle("Enviar", for: .normal)
        } else {
            nextButton.setTitle("Siguiente", for: .normal)
        }
    }

}

extension DatosApoderadoLegalVC: DatosApoderadoLegalDelegate {

    func updateData() {

    }

    func updateTitleHeader() {
        let header = HeaderData(
            imagen: #imageLiteral(resourceName: "logo_Sitio"),
            titulo: "Crear Contrato",
            titulo2: nil,
            titulo3: nil,
            subtitulo: "Apoderado Legal.",
            closure: { [weak self] in
                self?.padreController?.cambiarVista(MetodoPagoVC.self)
            }
        )
        super.updateTitle(headerData: header)
    }

    func startLoading() {
        super.showLoading(view: self.view)
    }

    func finishLoading() {
        super.hideLoading()
    }

    func cambiarPantallaContactoFacturacion() {
        padreController?.cambiarVista(ContactoDeFacturacionVC.self)
    }

    func cambiarPantallaCrearCotizacion() {
        padreController?.cambiarVista(CrearCotizacionVC.self)
    }

    func checkBoxEstaSeleccionado() -> Bool {
        return radioButton1.selectedButtons().count > 0
    }

    func clear() {
        radioButton1.isSelected = false
        textfieldTipoPago.text = ""
        textfieldNombre.text = ""
        textfieldApellPaterno.text = ""
        textfieldApellMaterno.text = ""
        textfieldTelefono.text = ""
        textfieldCorreo.text = ""
    }
}
