//
//  DatosApoderadoLegalPresenter.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 26/12/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol DatosApoderadoLegalDelegate: BaseDelegate {
    func updateData()
    func cambiarPantallaContactoFacturacion()
    func cambiarPantallaCrearCotizacion()
    func checkBoxEstaSeleccionado() -> Bool
    func clear()
}

class DatosApoderadoLegalPresenter {
    weak fileprivate var view: DatosApoderadoLegalDelegate?
    fileprivate let service: DatosApoderadoLegalService
    fileprivate var dataApoderoLegal: DatosApoderadoLegalModel!
    var mPayType: [String] = ["Prepago", "Pospago"]

    init(service: DatosApoderadoLegalService) {
        self.service = service
        dataApoderoLegal = DatosApoderadoLegalModel(seleccionado: false, nombre: "", apellidoPaterno: "", apellidoMaterno: "", correo: "", telefono: "", celular: "", tipoDePago: "")
    }

    func attachView(view: DatosApoderadoLegalDelegate) {
        self.view = view
        SwiftEventBus.onMainThread(self, name: "All-clearData") { [weak self] _ in
            self?.view?.clear()
        }
    }

    func viewDidAppear() {

    }

    func viewDidShow() {
        view?.updateTitleHeader()
    }

    func detachView() {
        view = nil
    }

    func willHideView() {

    }

    func updateNombre(item: String) {
        dataApoderoLegal?.nombre = item
    }

    func updateApellidoPaterno(item: String) {
        dataApoderoLegal?.apellidoPaterno = item
    }

    func updateApellidoMaterno(item: String) {
        dataApoderoLegal?.apellidoMaterno = item
    }

    func updateTelefono(item: String) {
        dataApoderoLegal?.telefono = item
    }

    func updateCorreo(item: String) {
        dataApoderoLegal?.correo = item
    }

    func seleccionaTipoDePago(item: String) {
        dataApoderoLegal?.tipoDePago = item
    }

    func clickSiguiente() {
        let seleccionado = view!.checkBoxEstaSeleccionado()
        dataApoderoLegal.seleccionado = seleccionado
        SwiftEventBus.post("crearCotizacion(setApoderado)", sender: dataApoderoLegal)
        if seleccionado {
            view?.cambiarPantallaCrearCotizacion()
        } else {
            view?.cambiarPantallaContactoFacturacion()
        }
    }

    func getPayType() -> [String] {
        return self.mPayType
    }

}
