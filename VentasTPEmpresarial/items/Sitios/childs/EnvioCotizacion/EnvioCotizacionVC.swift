//
//  EnvioCotizacionVC.swift
//  VentasTPEmpresarial
//
//  Created by eduardo mancilla on 2/18/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import UIKit

class EnvioCotizacionVC: BaseItemVC  {
    @IBOutlet weak var buttonAceptar: UIButton!
    @IBOutlet weak var stackviewInfo: UIStackView!
    
    
    @IBOutlet weak var activityIndicator1: UIActivityIndicatorView!
    @IBOutlet weak var iconOk1: UIImageView!
    @IBOutlet weak var iconFail1: UIImageView!
    @IBOutlet weak var iconValidacion: UIImageView!
    @IBOutlet weak var labelContrato: UILabel!
    @IBOutlet weak var labelValidacionFaltante: UILabel!
    
    fileprivate let presenter = EnvioCotizacionPresenter(service: EnvioCotizacionService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow(){
        super.viewDidShow()
        presenter.viewDidShow()
    }
    
    private func setUpView() {
        buttonAceptar.isHidden = true
        stackviewInfo.isHidden = true
        
    }
    
    @IBAction func clickAceptar(_ sender: UIButton) {
        sender.animateBound(view: sender){[unowned self] _ in
            self.presenter.clickAceptar()
        }
    }
    
    override func willHideView(){
        presenter.willHideView()
    }
}


extension EnvioCotizacionVC: EnvioCotizacionDelegate{
    
    func cambiarSiguienteVista() {
        padreController?.cambiarVista(MetodoPagoVC.self)
    }
    
    func showButtonAceptar(isHidden: Bool){
        buttonAceptar.isHidden = isHidden
    }
    
    func showStackInfo(isHidden: Bool){
        stackviewInfo.isHidden = isHidden
    }
    
    func setLabelInfo(texto: String, imageName: String){
        labelValidacionFaltante.text = texto
        iconValidacion.image = UIImage(named: imageName)
    }
    
    func updateData(){
        
    }
    
    func updateTitleHeader() {
      
    }
    
    func startLoading() {
        super.showLoading(view: view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
    func inicioEnvioCotizacion(){
        activityIndicator1.startAnimating()
        activityIndicator1.isHidden = false
        iconOk1.isHidden = true
        iconFail1.isHidden = true
    }
    
    func finEnvioCotizacion(_ mensaje: String, res: Bool){
        labelContrato.text = "Cotización: \(mensaje)"
        activityIndicator1.stopAnimating()
        activityIndicator1.isHidden = true
        if res{
            iconOk1.isHidden = false
        }else{
            iconFail1.isHidden = false
        }
    }
    
    func mostratBotonAceptar(){
        buttonAceptar.isHidden = false
    }
    
    func deshabilitarRegresar(){
        let header = HeaderData(
            imagen: #imageLiteral(resourceName: "logo_Sitio"),
            titulo: "Sitios",
            titulo2: nil,
            titulo3: nil,
            subtitulo: "Crear Cotización",
            closure: nil
        )
        super.updateTitle(headerData: header)
    }
    
    func reinit(){
        buttonAceptar.isHidden = true
        stackviewInfo.isHidden = true
    }
    
}

