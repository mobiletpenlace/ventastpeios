//
//  EnvioCotizacionPresenter.swift
//  VentasTPEmpresarial
//
//  Created by eduardo mancilla on 2/18/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus
import TaskQueue

protocol EnvioCotizacionDelegate: BaseDelegate {
    func inicioEnvioCotizacion()
    func finEnvioCotizacion(_ mensaje: String, res: Bool)
    func cambiarSiguienteVista()
    
    func deshabilitarRegresar()
    func setLabelInfo(texto: String, imageName: String)
    func showStackInfo(isHidden: Bool)
    func showButtonAceptar(isHidden: Bool)
}

enum TipoCotizacion {
    case normal, solucionesMedida, descuento
}

class EnvioCotizacionPresenter {
    fileprivate weak var view: EnvioCotizacionDelegate?
    fileprivate let service: EnvioCotizacionService
    fileprivate var dataList = [CrearCotizacionModel]()
    fileprivate var oportunidadId, oportunidadNombre, idCuenta: String?
    fileprivate var finalizadaPorOcultamiento = false, procesandoCotizacion = false
    fileprivate var tipoCotizacion: TipoCotizacion = .normal

    init(service: EnvioCotizacionService) {
        self.service = service
    }
    
    func attachView(view: EnvioCotizacionDelegate) {
        self.view = view
        SwiftEventBus.onMainThread(self, name: "Cotizacion_DetalleSitio_Resumencotizacion(setdatosOportunidad)") {
            [unowned self] result in
            if let (oportunidadId, oportunidadNombre, newAccountId, _) = result?.object as? (String, String, String, String){
                self.oportunidadId = oportunidadId
                self.oportunidadNombre = oportunidadNombre
                self.idCuenta = newAccountId
            }
        }
    }
    
    func viewDidAppear(){
        
    }
    
    func detachView() {
        view = nil
    }
    
    private func setUpInitConfig() {
        finalizadaPorOcultamiento = false
        view?.deshabilitarRegresar()
        view?.showButtonAceptar(isHidden: true)
        view?.showStackInfo(isHidden: true) //ocultar el stackkview de la información.
//        view?.startLoading()
        view?.inicioEnvioCotizacion()
    }
    
    func viewDidShow() {
        view?.updateTitleHeader()
        if procesandoCotizacion == false {
            procesandoCotizacion = true
            setUpInitConfig()
            crearCotizacion()
        }
    }
    
    private func ventaNormal() {
        tipoCotizacion = .normal
        view?.showStackInfo(isHidden: false)  //mostrar el stackkview de la información.
        view?.setLabelInfo(texto: "se ha creado la cotización.", imageName: "icon_ok")
    }
    
    private func ventaDescuento() {
        tipoCotizacion = .descuento
        view?.showStackInfo(isHidden: false)  //mostrar el stackkview de la información.
        view?.setLabelInfo(texto: "La venta ha pasado a la fase de aprobación del descuento.", imageName: "logo_tipsVentas")
    }
    
    private func ventaSolucionesMedida() {
        tipoCotizacion = .solucionesMedida
        view?.showStackInfo(isHidden: false)  //mostrar el stackkview de la información.
        view?.setLabelInfo(texto: "La venta ha pasado a la fase de validación de factibilidad.", imageName: "logo_tipsVentas")
    }
    
    private func validarResultado(_ resultadoOpotunidad: StructVentaOportunidadRes) {
//        view?.finishLoading()
        view?.showButtonAceptar(isHidden: false)
        var resultadoRequest = true
        switch resultadoOpotunidad.resultado {
        case "0":
            if service.verificaFactibilidadSitios() {
                ventaNormal()
            } else {
                ventaSolucionesMedida()
            }
        case "2":
            ventaDescuento()
        default:
            resultadoRequest = false
        }
        view?.finEnvioCotizacion(" \(resultadoOpotunidad.codigoOportunidad)", res: resultadoRequest)
    }
    
    private func crearCotizacion() {
        guard let oportunidadId = oportunidadId, let oportunidadNombre = oportunidadNombre, let newAccountId = idCuenta else {
//            self.view?.finishLoading()
            return
        }
        
        let queue = TaskQueue()
        queue.tasks +=! { result, next in
            self.service.realizarVentaOportunidad(oportunidadId: oportunidadId, oportunidadNombre: oportunidadNombre, newAccountId: newAccountId){ [weak self] resultadoOpotunidad in
                next((resultadoOpotunidad.listaTrabajos, resultadoOpotunidad))
            }
        }
        
        var count = 0
        queue.tasks +=! { [weak queue, weak self] result, next in
            guard let `self` = self else { return }
            Logger.d("En el segundo metodo de consulta task.")
            
            guard let (listaTrabajos, resultadoOpotunidad) = result as? ([String], StructVentaOportunidadRes) else {
                Logger.e("No se ha obtenido la lista de ids")
                return
            }
            Logger.d("llamando a consulta tarea....")
            self.service.consultaTarea(idsJobs: listaTrabajos) { res in
                guard let res = res else {
                    Logger.e("Error en el servicio de consulta tarea.")
                    return
                }
                if res {
                    self.validarResultado(resultadoOpotunidad)
                    next(nil)
                } else {
                    count += 1
                    if count < 1000 {
                        Logger.i("Aun no termina el servicio.. (\(count))")
                        queue?.retry(1)
                    } else {
                        Logger.e("Error, se ha escedido el numero de intentos.")
                    }
                }
            }
        }
        
        queue.run {
            self.procesandoCotizacion = false
            self.service.borraDatosCotizacion()
        }
    }
    
    func clickAceptar() {
        if !procesandoCotizacion && !finalizadaPorOcultamiento {
            switch tipoCotizacion {
            case .normal:
                view?.cambiarSiguienteVista()
            case .solucionesMedida:
                cambiarAPantallaVentas()
            case .descuento:
                cambiarAPantallaVentas()
            }
        }
    }
    
    private func cambiarAPantallaVentas() {
        SwiftEventBus.post("menuTabBar-cambiarMenuItem", sender: (1, 2))
    }
    
    func willHideView(){
        finalizadaPorOcultamiento = true
        clickAceptar()
    }
}
