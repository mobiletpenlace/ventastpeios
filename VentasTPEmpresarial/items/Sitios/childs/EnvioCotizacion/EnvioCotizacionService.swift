//
//  EnvioCotizacionService.swift
//  VentasTPEmpresarial
//
//  Created by eduardo mancilla on 2/18/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//
import RealmSwift

class EnvioCotizacionService: BaseDBManagerDelegate, BaseFormatDate {
    var realm: Realm!
    var serverManager: ServerDataManager?
    var dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()
        serverManager = ServerDataManager()
        realm = getRealm()
    }
    
    func verificaFactibilidadSitios() -> Bool {
        return dbManager.verificaFactibilidadSitios()
    }
    
    func consultaTarea(idsJobs: [String],  _ callBack:@escaping (Bool?) -> Void) {
//        let modelo = SFRequestConsultaTrabajo(idsJobs: idsJobs)
//        let url = Constantes.Url.SalesFor.consultaTareas
//        let tipo: TipoRequest = .sf
//        let decodeClass = SFResponseConsultaTrabajo.self
        
        let modelo = MiddleRequestConsultaTrabajo(idsJobs: idsJobs)
        let url = Constantes.Url.Middleware.Qa.consultaTareas
        let tipo: TipoRequest = .middle
        let decodeClass = MiddleResponseConsultaTrabajo.self
        
        serverManager?.getData(model: modelo, response: decodeClass, url: url, tipo){ response in
            guard let listaTrabajos = response?.listJobs else {
                callBack(nil)
                return
            }
            let size = listaTrabajos.count
            var nJobsDone = 0
            for job in listaTrabajos {
                if job.status == "Completed" || job.status == "Error" {
                    nJobsDone += 1
                }
            }
            if nJobsDone < size {
                callBack(false)
            } else {
                callBack(true)
            }
        }
    }
    
    func realizarVentaOportunidad(oportunidadId: String, oportunidadNombre: String, newAccountId: String,  _ callBack:@escaping (StructVentaOportunidadRes) -> Void) {
        let configDB = dbManager.getConfig()
        var sitiosCobertura = [Sitios]()
        if let resumenCot = dbManager.getResumenCotizacionSitio() {
            for sitio in resumenCot.sitios {
                var planes = [IdPlanes]()
                var servicios = [Servicios]()
                var productos = [Productos2]()
                var esSolucionesALaMedida: Bool = false
                var esServicio: Bool = false
                
                for plan in sitio.planesConfig {
                    if let tipo = dbManager.getTipoFamiliaPlan(configPlanId: plan.id) {
                        if tipo == "Servicios" {
                            esServicio = true
                        }
                    }
                    var soluciones = [AddServicio]()
                    if dbManager.esSolucionesAlaMedia(plan.plan.id!) {
                        esSolucionesALaMedida = true
                        for productoAdic in plan.productosAdicionales {
                            var productos = [AddonServicio]()
                            for addon in productoAdic.addons{
                                if addon.cantidad > 0 {
                                    for _ in 0..<addon.cantidad{
                                        productos.append(
                                            AddonServicio(
                                                id: addon.addon.id!,
                                                descripcion: addon.addon.nombre,
                                                descuento: "0",
                                                precio: "\(addon.precio)"
                                            )
                                        )
                                    }
                                }
                            }
                            
                            soluciones.append(
                                AddServicio(
                                    id: productoAdic.productoAdicional.id!,
                                    descripcion: productoAdic.productoAdicional.nombre,
                                    productos: productos
                                )
                            )
                        }
                        
                        planes.append(
                            IdPlanes(
                                id: plan.plan.id!,
                                cantidad: "1",
                                descuento: "0",
                                addServicio: soluciones
                            )
                        )
                        
                    } else {
                        for serAdic in plan.serviciosAdicionales {
                            if serAdic.cantidad > 0 {
                                for _ in 0..<serAdic.cantidad {
                                    let serv = serAdic.servicioAdicional!
                                    let configFlex = serAdic.configFlexnet
                                    servicios.append(
                                        Servicios(
                                            id: serv.id!,
                                            nombre: serv.nombre,
                                            esSDWAN: serv.esSDWAN,
                                            ipOrigenDatos: configFlex?.ipOrigenDatos,
                                            ipDestinoDatos: configFlex?.ipDestinoDatos,
                                            listaDominiosBloqueo: configFlex?.listaDominiosBloqueo,
                                            determinarVLANs: configFlex?.determinarVLANs,
                                            salidaInternet: configFlex?.salidaInternet,
                                            porcentajeAnchoBanda: configFlex?.porcentajeAnchoBanda,
                                            determinarSegmentoOrigen: configFlex?.determinarSegmentoOrigen,
                                            segmentosDestinos: configFlex?.segmentosDestinos,
                                            tipoRouteo: configFlex?.tipoRouteo,
                                            areaId: configFlex?.areaId,
                                            idPlan: plan.plan.id ?? ""
                                        )
                                    )
                                }
                            }
                        }
                        for prodAdic in plan.productosAdicionales {
                            for addon in prodAdic.addons {
                                if addon.cantidad > 0 {
                                    for _ in 0..<addon.cantidad {
                                        productos.append(
                                            Productos2(
                                                id: addon.addon.id!,
                                                nombre: addon.addon.nombre
                                            )
                                        )
                                    }
                                }
                            }
                        }
                        
                        planes.append(
                            IdPlanes(
                                id: plan.plan.id!,
                                cantidad: "1",
                                descuento: "\(plan.resumenCotizacion!.porcentajeDescuento)",
                                addServicio: nil
                            )
                        )
                    }
                }
                
                if let cobertura = sitio.cobertura {
                    sitiosCobertura.append(
                        Sitios(
                            nombreSitio: sitio.nombre ?? "",
                            tipoCobertura: cobertura.tipoCobertura,
                            latitud: String(cobertura.latitud),
                            regionId: cobertura.regionId,
                            factibilidad: cobertura.factibilidad,
                            region: cobertura.region,
                            distrito: cobertura.distrito,
                            longitud: String(cobertura.longitud),
                            categoryService: cobertura.categoryService,
                            plaza: cobertura.plaza,
                            zona: cobertura.zona,
                            autorizacionSinCobertura: false,
                            newAccountId: newAccountId,
                            numExterior: cobertura.numExt,
                            numInteior: cobertura.numInt,
                            calleReferencia: cobertura.observaciones,
                            referenciaUrbanada: cobertura.observaciones,
                            codigoPostal: cobertura.codigoPostal,
                            colonia: cobertura.colonia,
                            municipio: cobertura.municipio,
                            ciudad: cobertura.ciudad,
                            estado: cobertura.estado,
                            idPlanes: planes,
                            adicionales: Adicionales(
                                serviciosAdd: ServiciosAdd2(servicios: servicios),
                                productosAdd: ProductosAdd2(productos: productos)
                            ),
                            calle: cobertura.calle,
                            cluster: cobertura.cluster,
                            esSolucionesALaMedida: esSolucionesALaMedida,
                            esServicio: esServicio
                        )
                    )
                }
            }
        }
        let vigenciaCot = formatDate(Date(), format: "dd/MM/yyyy")
        
//        let model = SFRequestVentaOportunidad(
//            oportunidad: oportunidadId,
//            nombre: oportunidadNombre,
//            vigenciaCotizacion: vigenciaCot,
//            plazo: dbManager.getPlazo(),
//            accion: "Nuevo",
//            idEmpleado: configDB.idEmpleado,
//            sitios: sitiosCobertura,
//            opcionesFac: nil,
//            metodoPago: nil
//        )
//
//        let url = Constantes.Url.SalesFor.crearCotizacion
//        let tipo: TipoRequest = .sf
//        let decodeClass = SFResponseVentaOportunidad.self
        
        let model = MiddleRequestVentaOportunidad(
            oportunidad: oportunidadId,
            nombre: oportunidadNombre,
            vigenciaCotizacion: vigenciaCot,
            plazo: dbManager.getPlazo(),
            accion: "Nuevo",
            idEmpleado: configDB.idEmpleado,
            sitios: sitiosCobertura,
            opcionesFac: nil,
            metodoPago: nil
        )
        
        let url = Constantes.Url.Middleware.Qa.crearCotizacion
        let tipo: TipoRequest = .middle
        let decodeClass = MiddleResponseVentaOportunidad.self
        
        serverManager?.getData(model: model, response: decodeClass, url: url, tipo) { response in
            guard let response = response else {
                callBack(StructVentaOportunidadRes(resultado: "1", codigoOportunidad: "", listaTrabajos: [String]()))
                return
            }
            let resVenta = StructVentaOportunidadRes(
                resultado: response.response?.result ?? "",
                codigoOportunidad: response.idNuevaCotizacion!,
                listaTrabajos: response.listaTrabajos ?? [String]()
            )
            callBack(resVenta)
        }
    }
    
    func borraDatosCotizacion() {
        dbManager.borraDatosCotizacion()
    }
}
