import RealmSwift

class CrearCotizacionService: BaseDBManagerDelegate, BaseFormatDate {
    var realm: Realm!
    var serverManager: ServerDataManager?
    var dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()
        serverManager = ServerDataManager()
        realm = getRealm()
    }
    
    func esSolucionesAlaMedia(_ id: String) -> Bool {
        return dbManager.esSolucionesAlaMedia(id)
    }
    
    func borraDatosVenta() {
        dbManager.borraDatosVenta()
    }
    
    func enviarContrato(idVenta: String, correoElectronico: String, _ callBack: @escaping (Bool) -> Void) {
        let configDB = dbManager.getConfig()
        
//        let model = SFRequestCorreoContrato(correoElectronico: correoElectronico, idVenta: idVenta,
//            idVendedor: configDB.idEmpleado, actualizar: true)
//        let url = Constantes.Url.SalesFor.enviarContrato
//        let tipo: TipoRequest = .sf
//        let decodeClass = SFResponseCorreoContrato.self
        
        let model = MiddleRequestCorreoContrato(correoElectronico: correoElectronico, idVenta: idVenta,
                                            idVendedor: configDB.idEmpleado, actualizar: true)
        let url = Constantes.Url.Middleware.Qa.enviarContrato
        let tipo: TipoRequest = .middle
        let decodeClass = MiddleResponseCorreoContrato.self
        
        serverManager?.getData(model: model, response: decodeClass, url: url, tipo) { response in
            guard let response = response else {
                callBack(false)
                return
            }
//            callBack(response.result == "0")
            callBack(response.response?.result == "0")
        }
    }
    
    
    func insertarInformacionTitularTarjeta(_ nombre: String, _ apellidoPaterno: String, _ apellidoMaterno: String) {
        dbManager.actualizarTitularTarjeta(nombre, apellidoPaterno, apellidoMaterno)
    }
    
    func generaContratoFactibilidad(idVenta: String, _ callBack: @escaping (Bool, String) -> Void) {
        guard let metodoPagoDB = dbManager.getResumenCotizacionSitio()?.metodoPago else {
            Logger.e("metodo de pago en bd no existe")
            callBack(false, "")
            return
        }
        
        let opfact = OpcionesFac2(nombre: "", metodoPago: metodoPagoDB.metodoPago, razonSocialEmisora: "Enlace", tipoRenta: "Arrear", generarFactura: "Sitio", cicloFacturaccion: "7", tipoMoneda: "MXN", numeroDeFacturas: "Más de una", tipoCliente: "Empresarial", momentoDeFacturacion: "Después de Instalar", tipoFacturacion: "Sitio", tipoCuenta: "Hija")
        
//        let model = SFRequestGeneraContratoFactibilidad(oppId: idVenta, opcionesFac: opfact)
//        let url = Constantes.Url.SalesFor.generaContrato
//        let tipo: TipoRequest = .sf
//        let decodeClass = SFResponseGeneraContratoFactibilidad.self
        
        let model = MiddleRequestGeneraContratoFactibilidad(oppId: idVenta, opcionesFac: opfact)
        let url = Constantes.Url.Middleware.Qa.generaContrato
        let tipo: TipoRequest = .middle
        let decodeClass = MiddleResponseGeneraContratoFactibilidad.self
        
        serverManager?.getData(model: model, response: decodeClass, url: url, tipo){ response in
            guard let response = response else {
                callBack(false, "")
                return
            }
            //            if response.result == "0" {
            if response.response?.result == "0" {
                callBack(true, response.url ?? "")
            } else {
                callBack(false, "")
            }
        }
    }
    
    func visualizarContrato(idVenta: String, _ callBack: @escaping (Bool, String) -> Void) {
        //        let model = SFRequestContrato(idVenta: idVenta)
        //        let url = Constantes.Url.SalesFor.descargaContrato
        //        let tipo: TipoRequest = .sf
        //        let decodeClass = SFResponseContrato.self
        
        let model = MiddleRequestContrato(idVenta: idVenta)
        let url = Constantes.Url.Middleware.Qa.descargaContrato
        let tipo: TipoRequest = .middle
        let decodeClass = MiddleResponseContrato.self
        
        serverManager?.getData(model: model, response: decodeClass, url: url, tipo){ response in
            guard let response = response else {
                callBack(false, "")
                return
            }
            //            if response.result == "0" {
            if response.response?.result == "0" {
                callBack(true, response.contrato?.uRL ?? "")
            } else {
                callBack(false, "")
            }
        }
    }
    
    func insertarMetodoPagoServicio(nombreFact: String, apellidoPaternoFact: String, apellidoMaternoFact: String, telefonoFact: String, celularFact: String, emailFact: String, email2Fact: String, tipoPago: String, oportunidadId: String, newAccountId: String, nombreApod: String, apellidoPaternoApod: String, apellidoMaternoApod: String, emailApod: String, telefonoApod: String, celularApod: String, mismaDirInst: Bool, colonia: String, delegacionMunicipio: String, ciudad: String, estado: String, calle: String, numExterior: String, numInterior: String, codigoPostalFacturacion: String, _ callBack: @escaping (Bool) -> Void) {
        
        guard let metodoPagoDB = dbManager.getResumenCotizacionSitio()?.metodoPago else {
            Logger.e("metodo de pago en bd no existe")
            callBack(false)
            return
        }
        
        let metodoPago = MetodoPagoVenta(
            nombretitular: metodoPagoDB.nombreTitularTarjeta,
            aPTitular: metodoPagoDB.apellidoPaternoTitular,
            aMTitular: metodoPagoDB.apellidoMaternoTitular,
            tipoTarjeta: metodoPagoDB.tipoTarjeta,
            numeroTarjeta: metodoPagoDB.digitosTarjeta,
            fechaMM: metodoPagoDB.mes,
            fechaAA: metodoPagoDB.anio,
            metodoPago: metodoPagoDB.metodoPago,
            tipoPago: tipoPago //pospago o prepago
        )
        
        let contactoFacturacion = ContactoFacturacion(
            nombre: nombreFact,
            apellidoPaterno: apellidoPaternoFact,
            apellidoMaterno: apellidoMaternoFact,
            telefono: telefonoFact,
            celular: celularFact,
            email: emailFact,
            email2: email2Fact
        )
        
        let apoderadoLegal = ApoderadoLegal(
            nombre: nombreApod,
            apellidoPaterno: apellidoPaternoApod,
            apellidoMaterno: apellidoMaternoApod,
            correo: emailApod,
            telefono: telefonoApod,
            celular: celularApod
        )
        
        //let direccionFacturacion: DireccionFacturacion? = nil
        
        let direccionFacturacion = DireccionFacturacion(
            mismaDirInst: mismaDirInst,
            colonia: colonia,
            delegacionMunicipio: delegacionMunicipio,
            ciudad: ciudad,
            estado: estado,
            calle: calle,
            numExterior: numExterior,
            numInterior: numInterior,
            codigoPostalFacturacion: codigoPostalFacturacion
        )
        
//        let model = SFRequestPagoVenta(
//            idOportunidad: oportunidadId,
//            idCuenta: newAccountId,
//            metodoPago: metodoPago,
//            contactoFacturacion: contactoFacturacion,
//            apoderadoLegal: apoderadoLegal,
//            direccionFacturacion: direccionFacturacion
//        )
//        let url = Constantes.Url.SalesFor.metodoPago
//        let tipo: TipoRequest = .sf
//        let decodeClass = SFResponsePagoVenta.self
        
        let model = MiddleRequestPagoVenta(
            idOportunidad: oportunidadId,
            idCuenta: newAccountId,
            metodoPago: metodoPago,
            contactoFacturacion: contactoFacturacion,
            apoderadoLegal: apoderadoLegal,
            direccionFacturacion: direccionFacturacion
        )
        let url = Constantes.Url.Middleware.Qa.metodoPago
        let tipo: TipoRequest = .middle
        let decodeClass = MiddleResponsePagoVenta.self
        
        serverManager?.getData(model: model, response: decodeClass, url: url, tipo) { response in
            guard let response = response else {
                callBack(false)
                return
            }
//            callBack(response.result == "0")
            callBack(response.response?.result == "0")
        }
    }
    
    
    func verificarTipoFamilia(configPlanId: Int) -> String? {
        if let plan = dbManager.buscarConfigPlanPorId(configPlanId) {
            if let tipo = plan.plan.familia.first?.tipo {
                return tipo.nombre
            }
        }
        return nil
    }
    
}
