import Foundation

struct RequestVentaEmpresarial: Codable {
    let venta: Venta?

    enum CodingKeys: String, CodingKey {

        case venta = "venta"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        venta = try values.decodeIfPresent(Venta.self, forKey: .venta)
    }

}
