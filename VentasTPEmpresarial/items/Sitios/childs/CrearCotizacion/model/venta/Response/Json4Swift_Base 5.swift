import Foundation

struct ResponseVenta: Codable {
    let resultDescription: String?
    let result: String?
    let actualizacionDatos: Bool?

    enum CodingKeys: String, CodingKey {

        case resultDescription = "resultDescription"
        case result = "result"
        case actualizacionDatos = "actualizacionDatos"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        resultDescription = try values.decodeIfPresent(String.self, forKey: .resultDescription)
        result = try values.decodeIfPresent(String.self, forKey: .result)
        actualizacionDatos = try values.decodeIfPresent(Bool.self, forKey: .actualizacionDatos)
    }

}
