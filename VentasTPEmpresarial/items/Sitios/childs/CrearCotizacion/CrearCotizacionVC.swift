import UIKit

class CrearCotizacionVC: BaseItemVC {

    @IBOutlet weak var buttonAceptar: UIButton!
    @IBOutlet weak var buttonVerPDF: UIButton!
    @IBOutlet weak var stackviewInfo: UIStackView!

    @IBOutlet weak var activityIndicator2: UIActivityIndicatorView!
    @IBOutlet weak var iconOk2: UIImageView!
    @IBOutlet weak var iconFail2: UIImageView!

    @IBOutlet weak var activityIndicator4: UIActivityIndicatorView!
    @IBOutlet weak var iconOk4: UIImageView!
    @IBOutlet weak var iconFail4: UIImageView!

    @IBOutlet weak var activityIndicator5: UIActivityIndicatorView!
    @IBOutlet weak var iconOk5: UIImageView!
    @IBOutlet weak var iconFail5: UIImageView!

    @IBOutlet weak var activityIndicator6: UIActivityIndicatorView!
    @IBOutlet weak var iconOk6: UIImageView!
    @IBOutlet weak var iconFail6: UIImageView!

    @IBOutlet weak var stackviewEnvioContrato: UIStackView!
    @IBOutlet weak var stackviewDescargaContrato: UIStackView!

    @IBOutlet weak var viewLoadingContainer: UIView!

    fileprivate let presenter = CrearCotizacionPresenter(service: CrearCotizacionService())

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUpView()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }

    override func viewDidShow() {
        super.viewDidShow()
        presenter.viewDidShow()
    }

    private func setUpView() {
        buttonAceptar.isHidden = true
        buttonVerPDF.isHidden = true
        stackviewInfo.isHidden = true
    }

    @IBAction func clickCrearCotizacion(_ sender: UIButton) {
        sender.animateBound(view: sender) { [unowned self] _ in
            self.presenter.onClickCrearCotizacion()
        }
    }

    @IBAction func clickVerPDf(_ sender: UIButton) {
        sender.animateBound(view: sender) { [unowned self] _ in
            self.presenter.clickVerPDf()
        }
    }

    @IBAction func clickAceptar(_ sender: UIButton) {
        sender.animateBound(view: sender) { [unowned self] _ in
            self.presenter.clickAceptar()
        }
    }

    override func willHideView() {
        presenter.willHideView()
    }
}

extension CrearCotizacionVC: CrearCotizacionDelegate {

    func updateData() {

    }

    func updateTitleHeader() {

    }

    func startLoading() {
        super.showLoading(view: viewLoadingContainer)
    }

    func finishLoading() {
        super.hideLoading()
    }

    func mostrarMensaje(_ mensaje: String, _ titulo: String) {
        super.showAlert(titulo: titulo, mensaje: mensaje)
    }

    func mostrarPDF(urlPDF: String, titulo: String) {
        super.showDialog(customAlert: DialogoPDF(urlPDF: urlPDF, titulo: titulo))
    }

    private func initDefaults() {
        stackviewInfo.isHidden = false
        stackviewEnvioContrato.isHidden = false
        stackviewDescargaContrato.isHidden = false

        activityIndicator2.isHidden = true
        activityIndicator4.isHidden = true
        activityIndicator5.isHidden = true
        activityIndicator6.isHidden = true

        iconOk2.isHidden = true
        iconOk4.isHidden = true
        iconOk5.isHidden = true
        iconOk6.isHidden = true

        iconFail2.isHidden = true
        iconFail4.isHidden = true
        iconFail5.isHidden = true
        iconFail6.isHidden = true
    }

    func inicioEnvioMetodopago() {
        initDefaults()
        activityIndicator2.isHidden = false
        activityIndicator2.startAnimating()
    }

    func finEnvioMetodopago(res: Bool) {
        activityIndicator2.isHidden = true
        activityIndicator2.stopAnimating()
        if res {
            iconOk2.isHidden = false
        } else {
            iconFail2.isHidden = false
        }
    }

    func inicioGeneraContrato() {
        activityIndicator6.isHidden = false
        activityIndicator6.startAnimating()
    }

    func finGeneraContrato(res: Bool) {
        activityIndicator6.isHidden = true
        activityIndicator6.stopAnimating()
        if res {
            iconOk6.isHidden = false
        } else {
            iconFail6.isHidden = false
        }
    }

    func inicioEnvioContratoCorreo() {
        activityIndicator4.isHidden = false
        activityIndicator4.startAnimating()
    }

    func finEnvioContratoCorreo(res: Bool) {
        activityIndicator4.isHidden = true
        activityIndicator4.stopAnimating()
        if res {
            iconOk4.isHidden = false
        } else {
            iconFail4.isHidden = false
        }
    }

    func inicioDescargaContrato() {
        activityIndicator5.isHidden = false
        activityIndicator5.startAnimating()
    }

    func finDescargaContrato(res: Bool) {
        activityIndicator5.isHidden = true
        activityIndicator5.stopAnimating()
        if res {
            iconOk5.isHidden = false
        } else {
            iconFail5.isHidden = false
        }
    }

    func mostratBotonesContrato() {
        buttonAceptar.isHidden = false
        buttonVerPDF.isHidden = false
    }

    func mostratBotonAceptar() {
        buttonAceptar.isHidden = false
        buttonVerPDF.isHidden = true
    }

    func deshabilitarRegresar() {
        let header = HeaderData(
            imagen: #imageLiteral(resourceName: "logo_Sitio"),
            titulo: "Crear Contrato",
            titulo2: nil,
            titulo3: nil,
            subtitulo: "Envío de Información",
            closure: nil
        )
        super.updateTitle(headerData: header)
    }

    func clear() {
        buttonAceptar.isHidden = true
        buttonVerPDF.isHidden = true
        stackviewInfo.isHidden = true
    }

    func tipoRegresar(tipo: Int) {
        Logger.d("cambiado el tip regresar a \(tipo)")
        var closure: (() -> Void)?
        switch tipo {
        case 0:
            closure = { [weak self] in
                self?.padreController?.cambiarVista(DatosApoderadoLegalVC.self)
            }
        case 1:
            closure = { [weak self] in
                self?.padreController?.cambiarVista(ContactoDeFacturacionVC.self)
            }
        case 2:
            closure = { [weak self] in
                self?.padreController?.cambiarVista(DireccionDeFacturacionVC.self)
            }
        default: break
        }

        let header = HeaderData(
            imagen: #imageLiteral(resourceName: "logo_Sitio"),
            titulo: "Sitios",
            titulo2: "Crear Contrato",
            titulo3: nil,
            subtitulo: "Envío de Información",
            closure: closure
        )
        super.updateTitle(headerData: header)
    }
}
