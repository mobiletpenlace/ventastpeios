import Foundation
import SwiftEventBus
import TaskQueue

protocol CrearCotizacionDelegate: BaseDelegate {
    func updateData()
    func mostrarPDF(urlPDF: String, titulo: String)
    func mostrarMensaje(_ mensaje: String, _ titulo: String)
    func mostratBotonesContrato()
    func deshabilitarRegresar()

    func inicioEnvioMetodopago()
    func finEnvioMetodopago(res: Bool)

    func inicioEnvioContratoCorreo()
    func finEnvioContratoCorreo(res: Bool)

    func inicioDescargaContrato()
    func finDescargaContrato(res: Bool)

    func inicioGeneraContrato()
    func finGeneraContrato(res: Bool)

    func clear()
    func tipoRegresar(tipo: Int)
    func mostratBotonAceptar()
}

class CrearCotizacionPresenter {
    weak fileprivate var view: CrearCotizacionDelegate?
    fileprivate let service: CrearCotizacionService
    fileprivate var dataList = [CrearCotizacionModel]()
    var oportunidadId: String?
    var oportunidadNombre: String?
    var idCuenta: String?
    var urlPDF: String?
    var dataApoderoLegal: DatosApoderadoLegalModel?
    var contactoDeFacturacion: ContactoDeFacturacionModel?
    var direccionDeFacturacion: DireccionDeFacturacionModel?
    var mismaDireccionMetodoPago: Bool = false
    var ready: Bool = false
    var iniciadaVenta = false

    init(service: CrearCotizacionService) {
        self.service = service
    }

    func attachView(view: CrearCotizacionDelegate) {
        self.view = view
        SwiftEventBus.onMainThread(self, name: "Cotizacion_DetalleSitio_Resumencotizacion(setdatosOportunidad)") {
            [unowned self] result in
            if let (oportunidadId, oportunidadNombre, newAccountId, _) = result?.object as? (String, String, String, String) {
                self.oportunidadId = oportunidadId
                self.oportunidadNombre = oportunidadNombre
                self.idCuenta = newAccountId
            }
        }

        SwiftEventBus.onMainThread(self, name: "crearCotizacion(setApoderado)") {
            [unowned self] result in
            if let dataApoderoLegal = result?.object as? DatosApoderadoLegalModel {
                self.dataApoderoLegal = dataApoderoLegal
                self.view?.tipoRegresar(tipo: 0)
            }
        }

        SwiftEventBus.onMainThread(self, name: "crearCotizacion(setContactoFact)") {
            [unowned self] result in
            if let contactoDeFacturacion = result?.object as? ContactoDeFacturacionModel {
                self.contactoDeFacturacion = contactoDeFacturacion
                self.view?.tipoRegresar(tipo: 1)
            }
        }

        SwiftEventBus.onMainThread(self, name: "crearCotizacion(setDirFact)") {
            [unowned self] result in
            if let direccionDeFacturacion = result?.object as? DireccionDeFacturacionModel {
                self.direccionDeFacturacion = direccionDeFacturacion
                self.view?.tipoRegresar(tipo: 2)
            }
        }

        SwiftEventBus.onMainThread(self, name: "crearCotizacion(setMismaDireccionMetodoPago)") {
            [weak self] result in
            if let mismaDireccionMetodoPago = result?.object as? Bool {
                self?.mismaDireccionMetodoPago = mismaDireccionMetodoPago
            }
        }
    }

    func viewDidAppear() {

    }

    func viewDidShow() {
        view?.updateTitleHeader()
        if iniciadaVenta == false {
            onClickCrearCotizacion()
        }
    }

    func detachView() {
        view = nil
    }

    func onClickCrearCotizacion() {
        iniciadaVenta = true
        ready = false
        view?.deshabilitarRegresar()
//        view?.startLoading()
        guard let oportunidadId = oportunidadId, let idCuenta = idCuenta, let dataApoderoLegal = dataApoderoLegal else {
            self.view?.finishLoading()
            Logger.e("no hay oportunidadId o newAccountId o dataApoderoLegal")
            return
        }
        //var contactoDeFacturacion: ContactoDeFacturacionModel?
        //var direccionDeFacturacion: DireccionDeFacturacionModel?

        var nombreFacturacion = ""
        var apellidoPaternoFacturacion = ""
        var apellidoMaternoFacturacion = ""
        var telefonoFacturacion = ""
        var celularFacturacion = ""
        var correoFacturacion = ""
        var correo2Facturacion = ""

        let tipoPago = dataApoderoLegal.tipoDePago
        let nombreApod = dataApoderoLegal.nombre
        let apellidoPaternoApod = dataApoderoLegal.apellidoPaterno
        let apellidoMaternoApod = dataApoderoLegal.apellidoMaterno
        let emailApod = dataApoderoLegal.correo
        let telefonoApod = dataApoderoLegal.telefono
        let celularApod = dataApoderoLegal.celular

        var mismaDirInst = true
        var colonia = ""
        var delegacionMunicipio = ""
        var ciudad = ""
        var estado = ""
        var calle = ""
        var numExterior = ""
        var numInterior = ""
        var codigoPostalFacturacion = ""

        if dataApoderoLegal.seleccionado {
            nombreFacturacion = dataApoderoLegal.nombre
            apellidoPaternoFacturacion = dataApoderoLegal.apellidoPaterno
            apellidoMaternoFacturacion = dataApoderoLegal.apellidoMaterno
            telefonoFacturacion = dataApoderoLegal.telefono
            celularFacturacion = dataApoderoLegal.celular
            correoFacturacion = dataApoderoLegal.correo
            correo2Facturacion = ""
        } else {
            guard let contactoDeFacturacion = contactoDeFacturacion else {
                self.view?.finishLoading()
                Logger.e("no hay contactoDeFacturacion")
                return
            }

            nombreFacturacion = contactoDeFacturacion.nombre
            apellidoPaternoFacturacion = contactoDeFacturacion.apellidoPaterno
            apellidoMaternoFacturacion = contactoDeFacturacion.apellidoMaterno
            telefonoFacturacion = contactoDeFacturacion.telefono
            celularFacturacion = contactoDeFacturacion.celular
            correoFacturacion = contactoDeFacturacion.correo
            correo2Facturacion = contactoDeFacturacion.otroCorreo
            if contactoDeFacturacion.seleccionado == false {
                guard let direccionDeFacturacion = direccionDeFacturacion else {
                    self.view?.finishLoading()
                    Logger.e("no hay direccionDeFacturacion")
                    return
                }
                mismaDirInst = false
                colonia = direccionDeFacturacion.colonia
                delegacionMunicipio = direccionDeFacturacion.delegacionMunicipio
                ciudad = direccionDeFacturacion.ciudad
                estado = direccionDeFacturacion.estado
                calle = direccionDeFacturacion.calle
                numExterior = direccionDeFacturacion.numExterior
                numInterior = direccionDeFacturacion.numInterior
                codigoPostalFacturacion = direccionDeFacturacion.codigoPostalFacturacion
            }
        }

        if mismaDireccionMetodoPago {
            let nombreApod = dataApoderoLegal.nombre
            let apellidoPaternoApod = dataApoderoLegal.apellidoPaterno
            let apellidoMaternoApod = dataApoderoLegal.apellidoMaterno

            service.insertarInformacionTitularTarjeta(nombreApod, apellidoPaternoApod, apellidoMaternoApod)
        }

        let queue = TaskQueue()

        queue.tasks +=! {
            self.view?.inicioEnvioMetodopago()
        }

        queue.tasks +=! { [weak queue] result, next in
            self.service.insertarMetodoPagoServicio(nombreFact: nombreFacturacion, apellidoPaternoFact: apellidoPaternoFacturacion, apellidoMaternoFact: apellidoMaternoFacturacion, telefonoFact: telefonoFacturacion, celularFact: celularFacturacion, emailFact: correoFacturacion, email2Fact: correo2Facturacion, tipoPago: tipoPago, oportunidadId: oportunidadId, newAccountId: idCuenta, nombreApod: nombreApod, apellidoPaternoApod: apellidoPaternoApod, apellidoMaternoApod: apellidoMaternoApod, emailApod: emailApod, telefonoApod: telefonoApod, celularApod: celularApod, mismaDirInst: mismaDirInst, colonia: colonia, delegacionMunicipio: delegacionMunicipio, ciudad: ciudad, estado: estado, calle: calle, numExterior: numExterior, numInterior: numInterior, codigoPostalFacturacion: codigoPostalFacturacion) { resContrato in
                if resContrato {
                    self.view?.finEnvioMetodopago(res: true)
                    Logger.d("se ha insertado el metodo de pago servicio...")
                    next(nil)
                } else {
                    Logger.e("ERROR NO se ha insertado el metodo de pago servicio...")
                    self.view?.finEnvioMetodopago(res: false)
                    self.endService()
                    queue!.skip()
                }
            }
        }

        queue.tasks +=! {
            self.view?.inicioGeneraContrato()
        }

        var cnt = 1
        queue.tasks +=! { [weak queue] result, next in
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.service.generaContratoFactibilidad(idVenta: oportunidadId) { resContrato, url in
                    if resContrato {
                        Logger.d("se va a generar el contrato con: \(url)")
                        self.view?.finGeneraContrato(res: true)
                        next(nil)
                    } else {
                        cnt += 1
                        if cnt > 30 {
                            Logger.e("No se pudo generar el contrato para sol medida")
                            self.endService()
                            self.view?.finGeneraContrato(res: false)
                            queue!.skip()
                        } else {
                            Logger.i("se va reintentar la generación del contrato por \(cnt) vez")
                            queue!.retry(1)
                        }
                    }
                }
            }
        }

        queue.tasks +=! {
            self.view?.inicioEnvioContratoCorreo()
        }

        cnt = 1
        queue.tasks +=! { [weak queue] result, next in
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                self.service.enviarContrato(idVenta: oportunidadId, correoElectronico: correoFacturacion) { resContrato in
                    if resContrato {
                        self.view?.finEnvioContratoCorreo(res: true)
                        Logger.i("el contrato se envió al correo: \(correoFacturacion)")
                        next(nil)
                    } else {
                        cnt += 1
                        if cnt > 30 {
                            Logger.e("No se pudo enviar el contrato al correo: \(correoFacturacion)")
                            self.endService()
                            self.view?.finEnvioContratoCorreo(res: false)
                            queue!.skip()
                        } else {
                            Logger.i("se va reintentar el envio del correo por \(cnt) vez")
                            queue!.retry(1)
                        }
                    }
                }
            }
        }

        queue.tasks +=! {
            self.view?.inicioDescargaContrato()
        }

        cnt = 1
        queue.tasks +=! { [weak queue] result, next in
            self.service.visualizarContrato(idVenta: oportunidadId) { (resURL, urlPDF) in
                if resURL {
                    self.view?.finDescargaContrato(res: true)
                    Logger.i("se va a visualizar el contrato en: \(urlPDF)")
                    self.urlPDF = urlPDF
                    next(nil)
                } else {
                    cnt += 1
                    if cnt > 60 {
                        Logger.e("no se puede visualizar el contrato...")
                        self.endService()
                        self.view?.finDescargaContrato(res: false)
                        queue!.skip()
                    } else {
                        Logger.i("se va reintentar la descarga del contrato por \(cnt) vez")
                        queue!.retry(1)
                    }
                }
            }
        }

        queue.run {
            self.view?.mostratBotonesContrato()
            self.endService()
            Logger.i("finished")
        }
    }

    func endService() {
//        view?.finishLoading()
        ready = true
    }

    func clickVerPDf() {
        if let url = urlPDF {
            view?.mostrarPDF(urlPDF: url, titulo: "Contrato")
        }
    }

    func clickAceptar() {
        ready = false
        urlPDF = nil
        service.borraDatosVenta()
        SwiftEventBus.post("All-clearData")
        SwiftEventBus.post("menuTabBar-cambiarMenuChilds", sender: [1, 0])
        SwiftEventBus.post("menuTabBar-cambiarMenuChilds", sender: [2, 2, 0, 0])
        SwiftEventBus.post("menuTabBar-cambiarMenuChilds", sender: [2, 2, 0])
        SwiftEventBus.post("menuTabBar-cambiarMenuChilds", sender: [2, 0])
        SwiftEventBus.post("metodoPago(changeChild)", sender: 0)
        view?.clear()
        SwiftEventBus.post("menuTabBar-cambiarMenuItem", sender: (1, 2))
        iniciadaVenta = false
    }


    func willHideView() {
        Logger.d("se va a cambiar de pestaña con una venta pendiente.")
        if self.ready {
            self.ready = false
            Logger.i("se va a dar click en aceptar por dejar la pestaña")
            clickAceptar()
        }
    }
}
