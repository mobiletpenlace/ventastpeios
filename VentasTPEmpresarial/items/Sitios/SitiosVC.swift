//
//  StartSaleViewController.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 01/06/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class SitiosVC: BaseMenuItemVC, TimeMeasureDelegate {
    @IBOutlet weak var viewContainer: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        let tiempoInicio = iniciarTiempo()

        super.subscribe(nameItem: Constantes.Eventbus.Id.menuItemSitios)
        super.headerData = HeaderData(
            imagen: #imageLiteral(resourceName: "logo_Sitio"),
            titulo: "Sitios",
            titulo2: nil,
            titulo3: nil,
            subtitulo: nil,
            closure: nil
        )
        addItems()

        medirTiempo(tipoInicio: tiempoInicio, nombre: "Sitios")

    }

    func addItems() {
        var viewControllers = [BaseItemVC]()

        viewControllers.append(DetalleSitioVC())

        viewControllers.append(EnvioCotizacionVC())

        viewControllers.append(MetodoPagoVC())

        viewControllers.append(CoberturaResumenVC())
        viewControllers.append(CoberturaMapaVC())
        viewControllers.append(ConfirmaUbicacionVC())

        viewControllers.append(DatosApoderadoLegalVC())
        viewControllers.append(ContactoDeFacturacionVC())
        viewControllers.append(DireccionDeFacturacionVC())
        viewControllers.append(CrearCotizacionVC())
        super.addViewXIBList(view: viewContainer, viewControllers: viewControllers)
    }

    override func viewDidShow() {
        super.viewDidShow()
    }

}
