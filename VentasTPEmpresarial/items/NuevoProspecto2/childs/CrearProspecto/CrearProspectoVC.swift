//
//  VisualizaLeadActionVC.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 08/12/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class CrearProspectoVC: BaseItemVC {
    @IBOutlet weak var textFieldTipoPersona: UITextField!
    @IBOutlet weak var textFieldRazonSocial: UITextField!
    @IBOutlet weak var textFieldNombre: UITextField!
    @IBOutlet weak var textFieldApellidoPat: UITextField!
    @IBOutlet weak var textFieldApellidoMat: UITextField!
    @IBOutlet weak var textFieldTelefono: UITextField!
    @IBOutlet weak var textFieldCorreo: UITextField!
    @IBOutlet weak var viewProspecto: UIView!
    @IBOutlet weak var buttonSiguiente: UIButton!
    @IBOutlet weak var viewRazonSocial: UIStackView!
    fileprivate let presenter = CrearProspectoPresenter(service: CrearProspectoService())
    let dropManager = DropDownUtil()
    let validatorManager = ValidatorManager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        presenter.attachView(view: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow() {
        super.viewDidShow()
        presenter.viewDidShow()
        validatorManager.setUpTextfields()
    }
    
    private func setUpView() {
        self.viewRazonSocial.isHidden = true
        setUpValidator()
        setUPBeginEdit()
        setUPEndEdit()
        setUpDropDown()
    }
    
    @IBAction func clickSiguiente(_ sender: UIButton) {
        view.endEditing(true)
        sender.animateBound(view: sender){ [weak self] anim in
            guard let mSelf = self else { return }
            let validateRazonSocial = mSelf.validarRazonSocial()
            let validateDrop = mSelf.dropManager.validate()
            let validateTextFields = mSelf.validatorManager.validate()
            if validateRazonSocial && validateDrop && validateTextFields {
                mSelf.presenter.clickSiguiente()
            }else{
                mSelf.showBannerError(title: "Hay campos con errores.")
            }
        }
    }
    
    private func validarRazonSocial() -> Bool{
        var res = true
        if(textFieldTipoPersona.text == "Moral"){
            res = validatorManager.validateRegexOnce(
                textField: textFieldRazonSocial,
                regexType: .required,
                mensajeError: "El campo no puede estar vacío."
            )
        }else{
            textFieldRazonSocial.text = ""
            presenter.updateRazonSocial(item: "")
        }
        return res
    }
    
    func setUpDropDown(){
        textFieldTipoPersona.delegate = dropManager
        dropManager.add(textfield: textFieldTipoPersona, data: presenter.getArrayTipoPersona()){ [weak self] (index: Int, item: String) in
            guard let mSelf = self else { return }
            if (index == 1){
                mSelf.moralAcciones()
            }else if (index == 0){
                mSelf.fisicaAcciones()
            }
            mSelf.presenter.seleccionarTipoPersona(item: item)
            let _ = mSelf.dropManager.validate()
            mSelf.validatorManager.setUpTextfields()
        }
    }
    
    func setUpValidator(){
        textFieldNombre.delegate = validatorManager
        textFieldApellidoPat.delegate = validatorManager
        textFieldApellidoMat.delegate = validatorManager
        textFieldTelefono.delegate = validatorManager
        textFieldCorreo.delegate = validatorManager
        textFieldRazonSocial.delegate = validatorManager
        
        validatorManager.addValidator(
            
            FieldValidator.textField(
                textFieldNombre, //textfield
                .required, //validacion del texto mientras.
                .nombre, // tipo de caracteres validos
                "El campo no puede estar vacío"
            ),
            
            FieldValidator.textField(
                textFieldApellidoPat, //textfield
                .required, //validacion del texto mientras.
                .nombre, // tipo de caracteres validos
                "El campo no puede estar vacío"
            ),
            
            FieldValidator.textField(
                textFieldApellidoMat, //textfield
                .required, //validacion del texto mientras.
                .nombre, // tipo de caracteres validos
                "El campo no puede estar vacío"
            ),
            
            FieldValidator.textField(
                textFieldTelefono, //textfield
                .exact(10), //validacion del texto mientras.
                .numero, // tipo de caracteres validos
                "El campo debe tener 10 digitos"
            ),
            FieldValidator.textField(
                textFieldCorreo, //textfield
                .regex(.email), //validacion del texto mientras.
                .all, // tipo de caracteres validos
                "Error en el formato de correo electronico"
            ),
            FieldValidator.textField(
                textFieldRazonSocial, //textfield
                .none, //validacion del texto mientras.
                .nombre, // tipo de caracteres validos
                ""
            )
        )
    }
    
    func setUPBeginEdit(){
        validatorManager.addChangeEnterField(from: textFieldRazonSocial, to: textFieldNombre)
        validatorManager.addChangeEnterField(from: textFieldNombre, to: textFieldApellidoPat)
        validatorManager.addChangeEnterField(from: textFieldApellidoPat, to: textFieldApellidoMat)
        validatorManager.addChangeEnterField(from: textFieldApellidoMat, to: textFieldTelefono)
        validatorManager.addChangeEnterField(from: textFieldTelefono, to: textFieldCorreo)
    }
    
    func setUPEndEdit(){
        
        validatorManager.addEndEditingFor(textFieldRazonSocial) { [weak self]
            (textField) -> (Void) in
            self?.presenter.updateRazonSocial(item: textField.text ?? "")
        }
        
        validatorManager.addEndEditingFor(textFieldNombre) { [weak self]
            (textField) -> (Void) in
            self?.presenter.updateNombre(item: textField.text ?? "")
        }
        
        validatorManager.addEndEditingFor(textFieldApellidoPat) { [weak self]
            (textField) -> (Void) in
            self?.presenter.updateApellidoPaterno(item: textField.text ?? "")
        }
        
        validatorManager.addEndEditingFor(textFieldApellidoMat) { [weak self]
            (textField) -> (Void) in
            self?.presenter.updateApellidoMaterno(item: textField.text ?? "")
        }
        
        validatorManager.addEndEditingFor(textFieldTelefono) { [weak self]
            (textField) -> (Void) in
            self?.presenter.updateTelefonoPrincipal(item: textField.text ?? "")
        }
        
        validatorManager.addEndEditingFor(textFieldCorreo) { [weak self]
            (textField) -> (Void) in
            self?.presenter.updateCorreoElectronico(item: textField.text ?? "")
        }
        
    }
    
    func moralAcciones(){
        viewRazonSocial.isHidden = false
        view.layoutIfNeeded()
    }
    
    func fisicaAcciones(){
        viewRazonSocial.isHidden = true
        view.layoutIfNeeded()
    }

}

extension CrearProspectoVC: CrearProspectoDelegate{
    
    func cambiaButton(edit: Bool) {
        ButtonUtils.setTitle(buttonSiguiente, "Guardar", "Crear", res: edit)
    }
    
    func update() {
        guard let data = presenter.getData() else {
            return
        }
        textFieldRazonSocial.text = data.empresa
        textFieldNombre.text = data.nombre
        textFieldApellidoPat.text = data.apellidoPaterno
        textFieldApellidoMat.text = data.apellidoMaterno
        textFieldTelefono.text = data.telefono
        textFieldCorreo.text = data.correo

        dropManager.updateDropdown(
            value: data.tipoPersona,
            array: presenter.getArrayTipoPersona(),
            textfield: textFieldTipoPersona
        )
        
        if data.tipoPersona == "Moral"{
            moralAcciones()
        }else{
            fisicaAcciones()
        }
    }
    
    func clearData() {
        Logger.d("Limpiando los datos.")
        textFieldRazonSocial.text = ""
        textFieldNombre.text = ""
        textFieldApellidoPat.text = ""
        textFieldApellidoMat.text = ""
        textFieldTelefono.text = ""
        textFieldCorreo.text = ""
        textFieldTipoPersona.text = ""
        dropManager.clear()
    }
    
    func setEmptyProgress() {
        
    }
    
    func updateTitleHeader() {
        let header = HeaderData(
            imagen: #imageLiteral(resourceName: "logo_GenerarLead"),
            titulo: "Cuentas",
            titulo2: "Prospectos",
            titulo3: nil,
            subtitulo: "Nuevo Prospecto",
            closure:{[weak self] in
                self?.padreController?.cambiarVista(index: 0)
            }
        )
        super.updateTitle(headerData: header)
    }
    
    func startLoading() {
        super.showLoading(view: view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
    func cambiarSiguienteVista(){
        padreController?.cambiarVista(index: 0)
    }
    
}
