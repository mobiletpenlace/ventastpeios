//
//  VizualizaLeadActionPresenter.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 08/12/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus
import TaskQueue

protocol CrearProspectoDelegate: BaseDelegate {
    func update()
    func cambiarSiguienteVista()
    func clearData()
    func cambiaButton(edit : Bool)
}

class CrearProspectoPresenter{
    fileprivate let service: CrearProspectoService
    weak fileprivate var view: CrearProspectoDelegate?
    fileprivate var data: TablaLeadModel!
    let listTipoPersona: [String] = ["Física con actividad empresarial","Moral"]
    var isEdit : Bool = false
    
    init(service: CrearProspectoService){
        self.service = service
        data = TablaLeadModel(
            tipoPersona: "",
            empresa: "",
            nombre: "",
            idLead: "",
            apellidoPaterno: "",
            apellidoMaterno: "",
            telefono: "",
            correo: "",
            etapaOportunidad: "")
    }
    
    func attachView(view: CrearProspectoDelegate){
        self.view = view
        SwiftEventBus.onMainThread(self, name: "nuevoProspecto-Tip") { result in
            //borrar este metodo
        }
        
        SwiftEventBus.onMainThread(self, name: "Flujo-Edita-Lead") { [unowned self] result in
            if let data = result?.object as? TablaLeadModel{
                Logger.d("se ha llamado a edita lead con \(data).")
                self.data = data
                self.isEdit = true
                self.view?.cambiaButton(edit: self.isEdit)
                self.view?.update()
            }
        }
        
        SwiftEventBus.onMainThread(self, name: "Flujo-Crea-Lead") { [unowned self] result in
            self.isEdit = false
            self.view?.cambiaButton(edit: self.isEdit)
            self.view?.clearData()
        }
        
    }
    
    func viewDidAppear(){
        
    }
    
    func getArrayTipoPersona() -> [String]{
        return listTipoPersona
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
    }
    
    func detachView() {
        view = nil
    }
    
    func getData() -> TablaLeadModel? {
        return data
    }
    
    
    func seleccionarTipoPersona(item: String){
        data?.tipoPersona = item
    }
    
    
    func updateRazonSocial(item: String){
        data?.empresa = item
    }
    
    func updateNombre(item: String){
        data?.nombre = item
    }
    
    func updateApellidoPaterno(item: String){
        data?.apellidoPaterno = item
    }
    
    func updateApellidoMaterno(item: String){
        data?.apellidoMaterno = item
    }
    
    func updateTelefonoPrincipal(item: String) {
        data?.telefono = item
    }
    
    func updateCorreoElectronico(item: String) {
        data?.correo = item
    }
    
    func clickSiguiente() {
        Logger.i(data)
        view?.startLoading()
        if isEdit {
            service.editaLead(modelo: data){[weak self] data in
                self?.onFinishEditaLead(data: data)
            }
        } else {
            service.crea(modelo: data){[weak self] res in
                self?.onFinishCreateLead(res: res)
            }
        }
    }
    
    func onFinishCreateLead(res: Bool?) {
        view?.finishLoading()
        if let res = res {
            if res {
                view?.showBannerOK(title: "Lead creado.")
                view?.cambiarSiguienteVista()
                view?.clearData()
            } else {
                view?.showBannerError(title: "Error al crear el lead.")
            }
        } else {
            view?.showBannerError(title: "Error en el servicio crear el lead.")
        }
    }
    
    func onFinishEditaLead(data: MiddleCrearProspectoResponse?) {
        view?.finishLoading()
        if data != nil {
            view?.showBannerOK(title: "Lead actualizado.")
            view?.cambiarSiguienteVista()
            view?.clearData()
        } else{
            view?.showBannerError(title: "No se ha podido editar el lead.")
        }
    }
}
