//
//  VizualizaLeadActionService.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 08/12/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import RealmSwift

class CrearProspectoService: BaseDBManagerDelegate {
    var realm: Realm!
    var serverManager: ServerDataManager?
    var dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()
        serverManager = ServerDataManager()
        realm = getRealm()
    }
    
    func editaLead(modelo: TablaLeadModel, _ callBack: @escaping (MiddleCrearProspectoResponse?) -> Void) {
        let idEmpleado = dbManager.getConfig().idEmpleado
        
//        let model = SFCrearProspectoRequest(
//            tipoPersona: modelo.tipoPersona,
//            company: modelo.empresa,
//            lastName: modelo.apellidoMaterno,
//            middleName: modelo.apellidoPaterno,
//            firstName: modelo.nombre,
//            idEmpleado: idEmpleado,
//            telefono: modelo.telefono,
//            correo: modelo.correo,
//            accion: "Actualizar",
//            idProspecto: modelo.idLead,
//            razonSocial : modelo.empresa
//        )
//        let url = Constantes.Url.SalesFor.crearProspecto
//        let tipo: TipoRequest = .sf
//        let decodeClass = SFCrearProspectoResponse.self
        
        let model = MiddleCrearProspectoRequest(
            tipoPersona: modelo.tipoPersona,
            company: modelo.empresa,
            lastName: modelo.apellidoMaterno,
            middleName: modelo.apellidoPaterno,
            firstName: modelo.nombre,
            idEmpleado: idEmpleado,
            telefono: modelo.telefono,
            correo: modelo.correo,
            accion: "Actualizar",
            idProspecto: modelo.idLead,
            razonSocial : modelo.empresa
        )
        let url = Constantes.Url.Middleware.Qa.crearProspecto
        let tipo: TipoRequest = .middle
        let decodeClass = MiddleCrearProspectoResponse.self
        
        serverManager?.post(model: model, url: url, tipo: tipo) { responseData in
            guard let data = responseData.data else {
                Logger.e("error en el servicio de editar lead")
                callBack(nil)
                return
            }
            do {
                let decoder = JSONDecoder()
                let responseOBJ = try decoder.decode(decodeClass, from: data)
//                if responseOBJ.result == "0" {
                if responseOBJ.response!.result == "0" {
                    callBack(responseOBJ)
                } else {
                    callBack(nil)
                }
            } catch let err {
                Logger.e("error: --> \(err)")
                callBack(nil)
            }
        }
    }
    
    func crea(modelo: TablaLeadModel, _ callBack: @escaping (Bool?) -> Void) {
        let idEmpleado = dbManager.getConfig().idEmpleado
        
//        let model = SFCrearProspectoRequest(
//            tipoPersona: modelo.tipoPersona,
//            company: modelo.empresa,
//            lastName: modelo.apellidoMaterno,
//            middleName: modelo.apellidoPaterno,
//            firstName: modelo.nombre,
//            idEmpleado: idEmpleado,
//            telefono: modelo.telefono,
//            correo: modelo.correo,
//            accion: "Nuevo",
//            idProspecto: "",
//            razonSocial : modelo.empresa
//        )
//        let url = Constantes.Url.SalesFor.crearProspecto
//        let tipo: TipoRequest = .sf
//        let decodeClass = SFCrearProspectoResponse.self
        
        let model = MiddleCrearProspectoRequest(
            tipoPersona: modelo.tipoPersona,
            company: modelo.empresa,
            lastName: modelo.apellidoMaterno,
            middleName: modelo.apellidoPaterno,
            firstName: modelo.nombre,
            idEmpleado: idEmpleado,
            telefono: modelo.telefono,
            correo: modelo.correo,
            accion: "Nuevo",
            idProspecto: "",
            razonSocial : modelo.empresa
        )
        let url = Constantes.Url.Middleware.Qa.crearProspecto
        let tipo: TipoRequest = .middle
        let decodeClass = MiddleCrearProspectoResponse.self
        
        serverManager?.post(model: model, url: url, tipo: tipo) { responseData in
            guard let data = responseData.data else {
                Logger.e("error en el servicio de crear lead")
                callBack(nil)
                return
            }
            do {
                let decoder = JSONDecoder()
                let responseOBJ = try decoder.decode(decodeClass, from: data)
//                callBack(responseOBJ.result == "0")
                callBack(responseOBJ.response!.result == "0")
            } catch let err {
                Logger.e("error: --> \(err)")
                callBack(nil)
            }
        }
    }
    
}
