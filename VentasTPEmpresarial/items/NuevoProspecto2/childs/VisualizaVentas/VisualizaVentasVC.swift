//
//  VisualizaVentasVC.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 22/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class VisualizaVentasVC: BaseItemVC {
    @IBOutlet weak var viewContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SwiftEventBus.onMainThread(self, name: "Activa-Progress-Documentos") { [weak self] result in
            self!.start()
        }
        
        SwiftEventBus.onMainThread(self, name: "Desactiva-Progress-Documentos") { [weak self] result in
            self!.finish()
        }

        let controllers = [
            TablaVentasVC(),
            CapturaDocumentos2VC(),
            CargaDocumentosContrato3VC()
        ]
        super.addViewXIBList(view: viewContainer, viewControllers: controllers)
    }
    
    func start(){
        super.showLoading(view: self.view)
    }
    
    func finish(){
        super.hideLoading()
    }

    override func viewDidShow() {
        super.viewDidShow()
    }
}
