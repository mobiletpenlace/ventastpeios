import Foundation

struct TablaVentasModel {
    var noOportunidad: String
    var nombreVenta: String
    var nombreEmpresa: String
    var nombreCuenta: String
    var nombreEmpleado: String
    var nombreContactoPrincipal: String
    var idOportunidad: String
    var idCuenta: String
    var fechaCreacion: String
    var etapaOportunidad: String
    var isBlock: Bool
    var etapaCotizacion: String
    var pos: Int
}
