//
//  tablaVentasVC.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 22/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class TablaVentasVC: BaseItemVC {

    @IBOutlet weak var VentasTableView: UITableView!
    @IBOutlet weak var ViewContainerTable: UIView!
    @IBOutlet weak var ordenPorTextField: UITextField!
    @IBOutlet weak var imageVenta: UIImageView!
    @IBOutlet weak var imageNoOP: UIImageView!
    @IBOutlet weak var imageEtapa: UIImageView!
    @IBOutlet weak var filtroView: UIView!
    @IBOutlet weak var anchoFiltroView: NSLayoutConstraint!
    @IBOutlet weak var altoViewSimbologia: NSLayoutConstraint!
    var venta = false, noOP = false, etapa = false, simbologia = false
    var nombreCelda2: String = "TablaVentasCell"
    var presenter = TablaVentasPresenter(service: TablaVentasService())

    override func viewDidLoad() {
        super.viewDidLoad()
        altoViewSimbologia.constant = 0
        anchoFiltroView.constant = 0
        view.layoutIfNeeded()
        presenter.attachView(view: self)
        setUpView()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }

    override func viewDidShow() {
        super.viewDidShow()
        presenter.viewDidShow()
    }

    @IBAction func onClickVenta(_ sender: Any) {
        if venta {
            imageVenta.image = UIImage(named: "icon_FlechaAbajo")
            venta = false
            presenter.VentaDESC()
        } else {
            imageVenta.image = UIImage(named: "icon_FlechaArriba")
            venta = true
            presenter.VentaASC()
        }
    }

    @IBAction func onClickNoOp(_ sender: Any) {
        if venta {
            imageNoOP.image = UIImage(named: "icon_FlechaAbajo")
            venta = false
            presenter.NoOpDESC()
        } else {
            imageNoOP.image = UIImage(named: "icon_FlechaArriba")
            venta = true
            presenter.NoOpASC()
        }
    }

    @IBAction func onClickEtapa(_ sender: Any) {
        if venta {
            imageEtapa.image = UIImage(named: "icon_FlechaAbajo")
            venta = false
            presenter.EtapaDESC()
        } else {
            imageEtapa.image = UIImage(named: "icon_FlechaArriba")
            venta = true
            presenter.EtapaASC()
        }
    }

    @IBAction func onClickCancel(_ sender: Any) {
        UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseOut, animations: {
            self.anchoFiltroView.constant = 0
            self.view.layoutIfNeeded()
        }, completion: nil)
        self.view.endEditing(true)
        ordenPorTextField.text = ""
        presenter.filtro = ""
        presenter.filtrar(filtro: "")
    }

    @IBAction func onClickFiltrar(_ sender: Any) {
        UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseOut, animations: {
            self.anchoFiltroView.constant = 240
            self.view.layoutIfNeeded()
        }, completion: nil)
    }

    @IBAction func onClickSimbologia(_ sender: Any) {
        if simbologia {
            simbologia = false
            UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseOut, animations: {
                self.altoViewSimbologia.constant = 0
                self.view.layoutIfNeeded()
            }, completion: nil)

        } else {
            simbologia = true
            UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseOut, animations: {
                self.altoViewSimbologia.constant = 200
                self.view.layoutIfNeeded()
            }, completion: nil)
        }

    }

    func setUpView() {
        let nib = UINib.init(nibName: nombreCelda2, bundle: nil)
        VentasTableView.register(nib, forCellReuseIdentifier: nombreCelda2)
        VentasTableView.delegate = self
        VentasTableView.dataSource = self
        super.addShadowView(view: ViewContainerTable)
        ordenPorTextField.delegate = self

        ordenPorTextField.addTarget(self, action: #selector(textFieldEditingDidChange(_:)), for: UIControlEvents.editingChanged)
    }

    @objc func textFieldEditingDidChange(_ sender: UITextField) {
        presenter.filtro = sender.text ?? ""
        presenter.filtrar(filtro: sender.text ?? "")
    }

}

extension TablaVentasVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return TablaVentasHeaderCell().view
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let data = presenter.getItemIndex(index: indexPath.row) else { return }
        if data.etapaOportunidad == "Firma" && data.etapaCotizacion == "Completada" {
            padreController?.cambiarVista(index: 1)
            SwiftEventBus.post("id_venta", sender: data.idOportunidad)
        } else if data.etapaOportunidad == "Diseño" && data.etapaCotizacion == "Completada"
                && !data.isBlock {
            clickMetodoPagoScreen(data: data)
        } else if data.etapaOportunidad == "Crédito" && data.etapaCotizacion == "Completada" {
            presenter.mostrarPDF(idOportunidad: data.idOportunidad)
        } else if data.etapaOportunidad == "Necesidades" {
            SwiftEventBus.post(
                "Cotizacion_DetalleSitio_Resumencotizacion(setdatosOportunidad)",
                sender: (data.idOportunidad, data.nombreVenta, data.idCuenta, data.nombreCuenta)
            )
            SwiftEventBus.post("menuTabBar-cambiarMenuItem", sender: (2, 0))
        } else {
            super.showAlert(
                titulo: "Aviso",
                mensaje: "Para agregar los documentos la venta debe estar en etapa de diseño o firma y en estatus completada"
            )
        }
    }

    private func clickMetodoPagoScreen(data: TablaVentasModel) {
        SwiftEventBus.post("Cotizacion_DetalleSitio_Resumencotizacion(setdatosOportunidad)", sender: (data.idOportunidad, data.nombreVenta, data.idCuenta, data.nombreCuenta))
        SwiftEventBus.post("menuTabBar-cambiarMenuItem", sender: (2, 2))
//        SwiftEventBus.post("metodoPago(changeTypeReturnScreen)", sender: 1)
    }
}

extension TablaVentasVC: UITableViewDataSource, BaseFormatNumber {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getSize()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: nombreCelda2, for: indexPath) as! TablaVentasCell
        guard let data = presenter.getItemIndex(index: indexPath.row) else { return cell }
        cell.nameLabel.text = String("\(data.nombreCuenta) - \(data.nombreVenta)")
        cell.etapaLabel.text = data.etapaOportunidad
        cell.numeroLabel.text = data.noOportunidad
        cell.estatusLabel.text = data.etapaCotizacion

        if data.isBlock {
            cell.statusView.isHidden = false
        }
        cell.selectionStyle = .none //desactivar el color al seleccionar la celda
        return cell
    }

}

extension TablaVentasVC: tablaVentasDelegate {

    func seleccionarCeldaTabla(indexPath: IndexPath) {
        let n = VentasTableView.numberOfRows(inSection: 0)
        if n > 0 {
            VentasTableView.selectRow(at: indexPath, animated: true, scrollPosition: .bottom)
        }
    }

    func reloadTable() {
        VentasTableView.reloadData()
    }

    func updateTitleHeader() {
        let header = HeaderData(
            imagen: #imageLiteral(resourceName: "logo_GenerarLead"),
            titulo: "Cuentas",
            titulo2: nil,
            titulo3: nil,
            subtitulo: "Oportunidades",
            closure: nil
        )
        super.updateTitle(headerData: header)
    }

    func startLoading() {
        super.showLoading(view: VentasTableView)
    }

    func finishLoading() {
        super.hideLoading()
    }

    func mostrarPDF(urlPDF: String, titulo: String) {
        super.showDialog(customAlert: DialogoPDF(urlPDF: urlPDF, titulo: titulo))
    }
}

extension TablaVentasVC: UITextFieldDelegate {

//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        presenter.filtro = textField.text ?? ""
//        presenter.filtrar(filtro: textField.text ?? "")
//        return true
//    }

}
