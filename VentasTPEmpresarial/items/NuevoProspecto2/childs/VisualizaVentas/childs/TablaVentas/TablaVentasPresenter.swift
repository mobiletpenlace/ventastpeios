//
//  tablaVentasPresenter.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 22/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol tablaVentasDelegate: BaseDelegate {
    func reloadTable()
    func mostrarPDF(urlPDF: String, titulo: String)
    func seleccionarCeldaTabla(indexPath: IndexPath)
}

class TablaVentasPresenter{
    
    weak fileprivate var view: tablaVentasDelegate?
    fileprivate let service: TablaVentasService?
    fileprivate var dataFilter = [TablaVentasModel]()
    fileprivate var dataDownloaded = [TablaVentasModel]()
    var filtro: String = ""
    var procesandoConsulta = false
    
    init(service: TablaVentasService){
        self.service = service
    }
    
    func attachView(view: tablaVentasDelegate){
        self.view = view
    }
    
    func viewDidAppear(){
        
    }
    
    /*protocol*/
    func viewWillAppear() {
        
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
        if procesandoConsulta == false{
            procesandoConsulta = true
            actualizarDatos()
        }
    }
    
    func detachView() {
        view = nil
    }
    
    func getItemIndex(index: Int) -> TablaVentasModel? {
        if index < 0 && index >= dataFilter.count {
            return nil
        }
        return dataFilter[index]
    }
    
    func getSize() -> Int {
        return dataFilter.count
    }
    
    func VentaASC() {
        dataFilter.sort(by: { $0.nombreVenta < $1.nombreVenta })
        view?.reloadTable()
    }
    
    func VentaDESC() {
        dataFilter.sort(by: { $0.nombreVenta > $1.nombreVenta })
        view?.reloadTable()
    }
    
    func EtapaASC() {
        dataFilter.sort(by: { $0.etapaOportunidad < $1.etapaOportunidad })
        view?.reloadTable()
    }
    
    func EtapaDESC() {
        dataFilter.sort(by: { $0.etapaOportunidad > $1.etapaOportunidad })
        view?.reloadTable()
    }
    
    func NoOpASC() {
        dataFilter.sort(by: { $0.noOportunidad < $1.noOportunidad })
        view?.reloadTable()
    }
    
    func NoOpDESC() {
        dataFilter.sort(by: { $0.noOportunidad > $1.noOportunidad })
        view?.reloadTable()
    }
    
    func filtrar(filtro: String) {
        let filtroUpper = filtro.uppercased()
        dataFilter = dataDownloaded.filter { dato in
            return dato.nombreVenta.uppercased().contains(filtroUpper)
            || dato.idOportunidad.uppercased().contains(filtroUpper)
            || dato.nombreCuenta.uppercased().contains(filtroUpper)
            || filtroUpper.count == 0
        }
        NoOpDESC()
        view?.reloadTable()
        view?.seleccionarCeldaTabla(indexPath: IndexPath(row: 0, section: 0))
    }
    
    func actualizarDatos() {
        dataFilter = [TablaVentasModel]()
        view?.reloadTable()
        self.view?.startLoading()
        
        
        let id = service?.getIdEmpleado() ?? ""
//        let model = SFTablaVentasRequest(idEmpleado: id)
//        let url = Constantes.Url.SalesFor.misVentas
//        let tipo: TipoRequest = .sf
//        let decodeClass = SFTablaVentasResponse.self
        
        let model = MiddleTablaVentasRequest(idEmpleado: id)
        let url = Constantes.Url.Middleware.Qa.misVentas
        let tipo: TipoRequest = .middle
        let decodeClass = MiddleTablaVentasResponse.self
        
        service?.getData(model: model, response: decodeClass, url: url, tipo) { [weak self] response in
            self?.onFinishGetData(data: response)
        }
    }
    
    func onFinishGetData(data: MiddleTablaVentasResponse?) {
        view?.finishLoading()
        guard let data = data, let listaMisVentas = data.listaMisVentas else {
            Logger.e("no hay data en lista de ventas...")
            view?.showBannerError(title: "No se ha podido obtener las ventas.")
            procesandoConsulta = false
            return
        }
        dataDownloaded = [TablaVentasModel]()
        var countLista = 0
        for venta in listaMisVentas {
            self.dataDownloaded.append(
                TablaVentasModel(
                    noOportunidad: venta.noOportunidad ?? "",
                    nombreVenta: venta.nombreVenta ?? "",
                    nombreEmpresa: venta.nombreEmpresa ?? "",
                    nombreCuenta: venta.nombreCuenta ?? "",
                    nombreEmpleado: venta.nombreEmpleado ?? "",
                    nombreContactoPrincipal: venta.nombreContactoPrincipal ?? "",
                    idOportunidad: venta.idOportunidad ?? "",
                    idCuenta: venta.idCuenta ?? "",
                    fechaCreacion: venta.fechaCreacion ?? "",
                    etapaOportunidad: venta.etapaOportunidad ?? "",
                    isBlock : venta.isBlock ?? false,
                    etapaCotizacion: venta.etapaCotizacion ?? "",
                    pos: countLista
                )
            )
            countLista = countLista + 1
        }
        procesandoConsulta = false
        filtrar(filtro: filtro)
    }
    
    func mostrarPDF(idOportunidad: String) {
        self.view?.startLoading()
        
        let model = SFRequestContrato(idVenta: idOportunidad)
        let url = Constantes.Url.SalesFor.descargaContrato
        let tipo: TipoRequest = .sf
        let decodeClass = SFResponseContrato.self
        
        service?.getData(model: model, response: decodeClass, url: url, tipo) { [weak self] response in
            self?.view?.finishLoading()
            if let urlPDF = response?.contrato?.uRL {
                self?.view?.mostrarPDF(urlPDF: urlPDF, titulo: "Contrato")
            } else {
                self?.view?.showBannerError(title: "No se ha podido descargar el contrato.")
                Logger.e("no se puede visualizar el contrato...")
            }
        }
        
//        self.service!.visualizarContrato(idVenta: idOportunidad){ (resURL, urlPDF) in
//            self.view?.finishLoading()
//            if resURL{
//                self.view?.mostrarPDF(urlPDF: urlPDF, titulo: "Contrato")
//            }else{
//                self.view?.showBannerError(title: "No se ha podido descargar el contrato.")
//                Logger.e("no se puede visualizar el contrato...")
//            }
//        }
    }
}

