//
//  tablaVentasService.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 22/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import RealmSwift

class TablaVentasService: BaseDBManagerDelegate {
    var realm: Realm!
    var serverManager: ServerDataManager?
    var dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()
        serverManager = ServerDataManager()
        realm = getRealm()
    }
}
