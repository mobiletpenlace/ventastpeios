//
//  CargaDocumentosContrato3VC.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 26/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import DLRadioButton
import Alamofire

class CargaDocumentosContrato3VC: BaseItemFotoFirmaVC, UIDocumentPickerDelegate {
    //@IBOutlet weak var checkButtonTerminosCondiciones: DLRadioButton!
    @IBOutlet weak var viewFirm1: UIView!
    @IBOutlet weak var viewFirm2: UIView!
    @IBOutlet weak var buttonHoja1TomarFoto: UIButton!
    @IBOutlet weak var buttonHojaMostrar: UIButton!
    @IBOutlet weak var imageViewHoja1: UIImageView!
    @IBOutlet weak var viewHoja1Firma: UIImageView!
    @IBOutlet weak var buttonHoja2TomarFoto: UIButton!
    @IBOutlet weak var buttonHoja2Mostrar: UIButton!
    @IBOutlet weak var imageViewHoja2: UIImageView!
    @IBOutlet weak var viewHoja2Firma: UIImageView!
    //    var uploads: [Bool] = [false,false,false,false,false,false,false,false,false,false,false,false]
    //    var URSL : [String] = ["","","","","","","","","","","",""]    ISILON
    var docu: Doc?
    var docs: [Documentos?] = [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil]
    var images: [CapturaDocumentosModel?] = [nil, nil]
    let presenter = CargaDocumentosContratoPresenter3(service: CargaDocumentosService())
    var contadorFotos = 0
    var posActual: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow(){
        super.viewDidShow()
        presenter.viewDidShow()
    }
    
    private func setUpView() {
       // checkButtonTerminosCondiciones.isMultipleSelectionEnabled = true
        
        super.addBorder(view: viewFirm1)
        super.addBorder(view: viewFirm2)
        
        let cornerSize: Double = 5
        super.addRoundCornersInBottom(cornerRadius: cornerSize, view: buttonHoja1TomarFoto)
        super.addRoundCornersInBottom(cornerRadius: cornerSize, view: buttonHoja2TomarFoto)
    }
    
    @IBAction func onClickSiguiente(_ sender: UIButton) {
        images[0] = CapturaDocumentosModel(Pdf: "", Imagen: String("data:image/jpeg;base64,\(imagenTomada(image: imageViewHoja1))"), pos: 9, Tipo: "IMAGEN")
        images[1] = CapturaDocumentosModel(Pdf: "", Imagen: String("data:image/jpeg;base64,\(imagenTomada(image: imageViewHoja2))"), pos: 10, Tipo: "IMAGEN")
        presenter.clickSiguiente(images)
    }
    
    @IBAction func clickTomarFotoHoja1(_ sender: UIButton) {
        sender.animateBound(view: sender){ [weak self] _ in
            self?.capturar(posicion: 9)
        }
    }
    
    @IBAction func clickTomarFotoHoja2(_ sender: UIButton) {
        sender.animateBound(view: sender){ [weak self] _ in
            self?.capturar(posicion: 10)
        }
    }
    
    @IBAction func clickMostrarHoja1(_ sender: UIButton) {
        sender.animateBound(view: sender)
        mostrarHoja1()
    }
    
    @IBAction func clickMostrarHoja2(_ sender: UIButton) {
        sender.animateBound(view: sender)
        mostrarHoja2()
    }
    
    @IBAction func clickFirmarHoja1(_ sender: UIButton) {
        sender.animateBound(view: sender){ [weak self] _ in
            self?.presenter.clickFirmarHoja1()
        }
    }
    
    @IBAction func clickFirmarHoja2(_ sender: UIButton) {
        sender.animateBound(view: sender){ [weak self] _ in
            self?.presenter.clickFirmarHoja2()
        }
    }
    
    func importPDF(){
        let importMenu = UIDocumentPickerViewController(documentTypes: ["com.adobe.pdf"], in: .import)
        
        importMenu.delegate = self
        
        self.present(importMenu, animated: true, completion: nil)
    }
    
    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        
        documentPicker.delegate = self
        
        self.present(documentPicker, animated: true, completion: nil)
        
    }
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL){
        switch posActual {
        case 9:
            imageViewHoja1.image = UIImage(named: "icon_PDF")
        case 10:
            imageViewHoja2.image = UIImage(named: "icon_PDF")
        default: break
            
        }
        let documento = CapturaDocumentosModel(
            Pdf: String("data:pdf/pdf;base64,\(FilesBase64.pdf(url))"),
            Imagen: "",
            pos: posActual,
            Tipo: "PDF"
        )
        presenter.saveDoc(posActual, documento)
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        
    }
    
    func imagenTomada(image: UIImageView) -> String{
        var imagen : String = ""
        let tmpImagen = imageWithSize(
            image: image.image!,
           size: CGSize(width: 720, height: 480)
        )
        if let imgJpeg = UIImageJPEGRepresentation(tmpImagen,0.65) {
            imagen = imgJpeg.base64EncodedString()
        }
        return imagen
    }
    
    public func imageWithSize(image: UIImage, size:CGSize) -> UIImage {
        var scaledImageRect = CGRect.zero;
        
        let aspectWidth:CGFloat = size.width / image.size.width;
        let aspectHeight:CGFloat = size.height / image.size.height;
        let aspectRatio:CGFloat = min(aspectWidth, aspectHeight);
        
        scaledImageRect.size.width = image.size.width * aspectRatio;
        scaledImageRect.size.height = image.size.height * aspectRatio;
        scaledImageRect.origin.x = (size.width - scaledImageRect.size.width) / 2.0;
        scaledImageRect.origin.y = (size.height - scaledImageRect.size.height) / 2.0;
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0);
        
        image.draw(in: scaledImageRect);
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return scaledImage!;
    }
    
    func galeria(){
        switch posActual {
        case 9:
            tomarFoto1(isTake: false)
        case 10:
            tomarFoto2(isTake: false)
            
        default: break
            
        }
    }
    
}

extension CargaDocumentosContrato3VC: CargaDocumentosContratoDelegate3{
    
    func clean() {
        imageViewHoja1.image = UIImage(named: "img_camera")
        imageViewHoja2.image = UIImage(named: "img_camera")
    }
    
    func savePhotos() {
        //        self.setSale()
    }
    
    func subirDocumentos() {
        //        setSale()
    }
    
    func tomarFoto1(isTake : Bool){
        if isTake{
            tomarFoto(imageViewHoja1, nombreImagen: Constantes.Names.fotoHoja1)
            contadorFotos += 1
        }else{
            importaGaleria(imageViewHoja1, nombreImagen: Constantes.Names.fotoHoja1)
            contadorFotos += 1
        }
        
    }
    
    func tomarFoto2(isTake : Bool){
        if isTake{
            tomarFoto(imageViewHoja2, nombreImagen: Constantes.Names.fotoHoja2)
            contadorFotos += 1
        }else{
            importaGaleria(imageViewHoja2, nombreImagen: Constantes.Names.fotoHoja2)
            contadorFotos += 1
        }
    }
    
    
    
    func mostrarHoja1(){
        showImageDialog(titulo: "Firma de contrato, hoja 1", imagen: (imageViewHoja1?.image)!)
    }
    
    func mostrarHoja2(){
        showImageDialog(titulo: "Firma de contrato, hoja 2", imagen: imageViewHoja2.image!)
    }
    
    
    
    
    func showFirmarHoja1(){
        showFirmaDialog(titulo: "Firma de hoja 1", view: viewHoja1Firma, nombreFile: Constantes.Names.firma1)
    }
    
    func showFirmarHoja2(){
        showFirmaDialog(titulo: "Firma de hoja 2", view: viewHoja2Firma, nombreFile: Constantes.Names.firma2)
    }
    
    
    
    func updateTitleHeader() {
        let header = HeaderData(
            imagen: #imageLiteral(resourceName: "logo_GenerarLead"),
            titulo: "Cuentas",
            titulo2: nil,
            titulo3: nil,
            subtitulo: "Carga de documentos",
            closure:{ [weak self] in
                self?.padreController?.cambiarVista(CapturaDocumentos2VC.self)
            }
        )
        super.updateTitle(headerData: header)
    }
    
    func startLoading() {
        super.showLoading(view: self.view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
    func cambiarvista(index: Int){
        padreController?.cambiarVista(index: index)
    }
    
    func mostrarMensaje(titulo: String, mensaje: String){
        super.showAlert(titulo: titulo, mensaje: mensaje)
    }
    
    func seHanAceptadoLasCondiciones() ->Bool{
//        if (checkButtonTerminosCondiciones.isMultipleSelectionEnabled) {
//            if checkButtonTerminosCondiciones.selectedButtons().count == 2 {
//                return true
//            }
//        }
        return true
    }
    
    
    func seHaFirmadoLasHojas() -> Bool{
        if (imageViewHoja1.image == nil){
            return false
        }
        if (imageViewHoja2.image == nil){
            return false
        }
        
        return true
    }
    
    func capturar(posicion:Int) {
        let alert = UIAlertController(
            title: "Capturar",
            message: "¿Que acción desea realizar?",
            preferredStyle: .alert
        )
        var titulo = "Tomar Foto"
        let action1 = UIAlertAction(title: titulo, style: .default){ [weak self] _ in
            self?.posActual = posicion
            self?.presenter.clickTomarFoto(posicion)
        }
        titulo = "Importar de Galleria"
        let action2 = UIAlertAction(title: titulo, style: .default){ [weak self] _ in
            self?.posActual = posicion
            self?.galeria()
        }
        titulo = "Importar PDF"
        let action3 = UIAlertAction(title:titulo,style: .default){ [weak self] _ in
            self?.posActual = posicion
            self?.importPDF()
        }
        
        titulo = "Cancelar"
        let cancel = UIAlertAction(title: titulo, style: .destructive, handler: nil)
        alert.view.tintColor = UIColor.blue
        alert.view.layer.cornerRadius = 25
        
        
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(action3)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
        
        
    }
    
}
