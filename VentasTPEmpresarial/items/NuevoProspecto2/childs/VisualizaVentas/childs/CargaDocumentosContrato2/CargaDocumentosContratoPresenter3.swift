//
//  CargaDocumentosContratoPresenter3.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 26/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus
import TaskQueue

protocol CargaDocumentosContratoDelegate3: BaseDelegate {
    func seHanAceptadoLasCondiciones() ->Bool
    func cambiarvista(index: Int)
    func mostrarMensaje(titulo: String, mensaje: String)
    func seHaFirmadoLasHojas() -> Bool
    func subirDocumentos()
    
    func tomarFoto1(isTake : Bool)
    func tomarFoto2(isTake : Bool)
    
    
    func showFirmarHoja1()
    func showFirmarHoja2()
    func savePhotos()
    func clean()
    
}


class CargaDocumentosContratoPresenter3{
    weak fileprivate var view: CargaDocumentosContratoDelegate3?
    fileprivate let service: CargaDocumentosService?
    var valores = [false, false, true, true, true]
    var data: [CapturaDocumentosModel?] = [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil]
    var Tipos: [String] = ["identificacionOficialFrente",
                           "identificacionOficialReverso",
                           "identificacionComprobanteDomicilio",
                           "fotoRFCFrente",
                           "Acta",
                           "fotoActaHoja2",
                           "fotoActaHoja3",
                           "PoderNotarial",
                           "fotoPoderNotarialCaratula2",
                           "ContratoHoja1",
                           "ContratoHoja2"
    ]
    var Nombres : [String] = ["identificacionOficialFrente.jpeg",
                              "identificacionOficialReverso.jpeg",
                              "identificacionComprobanteDomicilio.jpeg",
                              "fotoRFCFrente.jpeg",
                              "Acta.pdf",
                              "fotoActaHoja2.jpeg",
                              "fotoActaHoja3.jpeg",
                              "PoderNotarial.pdf",
                              "fotoPoderNotarialCaratula2.jpeg",
                              "ContratoHoja1.jpeg",
                              "ContratoHoja2.jpeg"
    ]
    var idVenta: String?
    var count : Int = 0
    var documentos : Doc? = nil
    var documento : [Documentos?] = [nil]
    
    init(service: CargaDocumentosService){
        self.service = service
    }
    
    func attachView(view: CargaDocumentosContratoDelegate3) {
        self.view = view
        
        
        SwiftEventBus.onMainThread(self, name: "id_venta") { [weak self] result in
            if let idVenta = result?.object as? String {
                self?.idVenta = idVenta
            }
        }
        SwiftEventBus.onMainThread(self, name: "CapturaDocumentos_primeras") { [weak self] result in
            let firstDocuments = result?.object as! [CapturaDocumentosModel?]
            self?.data[0] = firstDocuments[0]
            self?.data[1] = firstDocuments[1]
            self?.data[2] = firstDocuments[2]
            self?.data[3] = firstDocuments[3]
            self?.data[4] = firstDocuments[4]
            self?.data[5] = firstDocuments[5]
            self?.data[6] = firstDocuments[6]
            self?.data[7] = firstDocuments[7]
            self?.data[8] = firstDocuments[8]
        }
    }
    
    func viewDidAppear(){
        
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
    }
    
    func detachView() {
        view = nil
    }
    
    func clickSiguiente(_ images: [CapturaDocumentosModel?]){
        if(self.data[9] == nil){
            self.data[9] = images[0]
        }
        if(self.data[10] == nil){
            self.data[10] = images[1]
        }
//        mandarDocumentos()
        if sonValidosLosDatos(){
            if(self.data[9] == nil){
                self.data[9] = images[0]
            }
            if(self.data[10] == nil){
                self.data[10] = images[1]
            }
            mandarDocumentos()
        }
    }
    
    func mandarDocumentos(){
        view?.startLoading()
        for doc in data{
            if let doc = doc{
                if(doc.Tipo == "PDF"){
                    Logger.d("doc pdf : \(doc.Pdf)")
                }
                Logger.d("doc tipo : \(doc.Tipo)")
            }
        }
        
        let delayTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            let group2 = DispatchGroup()
            for doc in self.data{
                if(doc != nil){
                    group2.enter()
                    if(doc?.Tipo == "PDF"){
                        self.documento[0] = Documentos(tipoDocumento: self.Tipos[self.count], nombreArchivo: String("\(self.Tipos[self.count]).pdf"), archivoB64: (doc?.Pdf)!)
                        self.documentos = Doc(documento: self.documento as! [Documentos])
                        self.service?.cargaPDF(documentos: self.documentos!, idVenta: self.idVenta!){data in
                            if data!{
                                group2.leave()
                            }else{
                                group2.leave()
                            }
                        }
                        self.count = self.count + 1
                    }else{
                        self.documento[0] = nil
                        self.documento[0] = Documentos(tipoDocumento: self.Tipos[self.count], nombreArchivo: String("\(self.Tipos[self.count]).jpge"), archivoB64: (doc?.Imagen)!)
                        self.documentos = Doc(documento: self.documento as! [Documentos])
                        self.service?.cargaDoc(documentos: self.documentos!, idVenta: self.idVenta!){ data in
                            if data!{
                                group2.leave()
                            }else{
                                group2.leave()
                            }
                        }
                        self.count = self.count + 1
                    }
                }else{
                    
                }
            }
            group2.notify(queue: .main, execute: {
                self.count = 0
                self.view?.finishLoading()
                let delayTime = DispatchTime.now() + Double(Int64(0.3 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                    //                    self.view?.mostrarMensaje(titulo: "Aviso", mensaje: "Documentos enviados")
                    self.view?.showBannerOK(title: "Los documentos han sido enviados.")
                    SwiftEventBus.post(
                        "OnSuccessDocs",sender: nil)
                    self.view!.cambiarvista(index: 0)
                    self.data = [nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil]
                    self.valores = [false,false,true,true,true]
                    self.view!.clean()
                }
            })
        }
        
    }
    
    private func sonValidosLosDatos() -> Bool{
        if Constantes.Config.DEBUG{
            return true//no validar los datos en debug
        }else{
            if let vista = view {
                if !seHaCapturadoLasFotos(){//si no se ha capturado las fotos
                    //                    vista.mostrarMensaje(titulo: "Atención", mensaje: "Debes capturar todas las fotos")
                    vista.showBannerError(title: "Debes capturar todas las fotos para continuar.")
                    return false
                }
            }else{
                return false // si no ha vista
            }
            return true
        }
    }
    
    func seHaCapturadoLasFotos() -> Bool {
        if let vista = view {
            for v in valores {
                if v == false {
                    //                    vista.mostrarMensaje(titulo: "Atención", mensaje: "Debes capturar todas las fotos")
                    vista.showBannerError(title: "Debes capturar todas las fotos para continuar.")
                    return false
                }
            }
        } else {
            return false // si no hay vista
        }
        return true
    }
    
    func saveDoc(_ pos: Int,_ doc : CapturaDocumentosModel?){
        data[pos] = doc
        valores[pos] = true
    }
    
    func clickTomarFoto(_ posicion:Int){
        if posicion == 9{
            view?.tomarFoto1(isTake: true)
            valores[0] = true
        }else if posicion == 10{
            view?.tomarFoto2(isTake: true)
            valores[1] = true
        }
        
    }
    
    func clickTomarFotoHoja1(){
        view?.tomarFoto1(isTake : true)
        valores[0] = true
    }
    
    func clickTomarFotoHoja2(){
        view?.tomarFoto2(isTake : true)
        valores[1] = true
    }
    
    
    func clickFirmarHoja1(){
        view?.showFirmarHoja1()
    }
    
    func clickFirmarHoja2(){
        view?.showFirmarHoja2()
    }
    
    
}
