//
//  CapturaDocumentosService.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 15/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

class CargaDocumentosService: BaseDBManagerDelegate {
    var serverManager: ServerDataManager?
    var dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()
        serverManager = ServerDataManager()
    }
    
    func cargaDoc(documentos: Doc, idVenta : String,_ callBack:@escaping (Bool?) -> Void) {
        var res: Bool = true
//        let model = SFSubirArchivosRequest(idVenta, documentos)
//        let url = Constantes.Url.SalesFor.subirDocumentos
//        let tipo: TipoRequest = .sf
//        let decodeClass = SFDocumentoResponse.self
        
        let model = MiddleSubirArchivosRequest(idVenta, documentos)
        let url = Constantes.Url.Middleware.Qa.subirDocumentos
        let tipo: TipoRequest = .middle
        let decodeClass = MiddleDocumentoResponse.self
        
        serverManager?.getData(model: model, response: decodeClass, url: url, tipo) { response in
            guard let response = response else {
                callBack(nil)
                return
            }
//            if(response.result == "0") {
            if(response.response?.result == "0") {
                res = true
            } else {
                res = false
            }
            callBack(res)
        }
    }
    
    func cargaPDF(documentos: Doc, idVenta: String,_ callBack: @escaping (Bool?) -> Void) {
        var res: Bool = true
//        let model = SFSubirDocumentosRequest(idVenta, documentos)
//        let url = Constantes.Url.SalesFor.subirDocumentos
//        let tipo: TipoRequest = .sf
//        let decodeClass = SFDocumentoResponse.self
        
        let model = MiddleSubirDocumentosRequest(idVenta, documentos)
        let url = Constantes.Url.Middleware.Qa.subirDocumentos
        let tipo: TipoRequest = .middle
        let decodeClass = MiddleDocumentoResponse.self
        
        serverManager?.getData(model: model, response: decodeClass, url: url, tipo) { response in
            guard let response = response else {
                callBack(nil)
                return
            }
//            if(response.result == "0") {
            if(response.response?.result == "0") {
                res = true
            } else {
                res = false
            }
            callBack(res)
        }
    }
}
