//
//  CapturaDocumentos2VC.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 28/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftBaseLibrary
import DLRadioButton


class CapturaDocumentos2VC: BaseItemFotoFirmaVC, UIDocumentPickerDelegate{
    @IBOutlet weak var mViewOtherID: UIView!
    
    @IBOutlet weak var userIDButton: DLRadioButton!
    
    @IBOutlet weak var buttonTomarfotoIdentificacionFrente: UIButton!
    @IBOutlet weak var buttonMostrarIdentificacionFrente: UIButton!
    @IBOutlet weak var imageViewIdentificacionFrente: UIImageView!
    
    @IBOutlet weak var buttonTomarfotoIdentificacionReverso: UIButton!
    @IBOutlet weak var buttonMostrarIdentificacionReverso: UIButton!
    @IBOutlet weak var imageViewIdentificacionReverso: UIImageView!
    
    @IBOutlet weak var buttonTomarfotoComprobanteDomicilio: UIButton!
    @IBOutlet weak var buttonMostrarfotoComprobanteDomicilio: UIButton!
    @IBOutlet weak var imageViewfotoComprobanteDomicilio: UIImageView!
    
    @IBOutlet weak var buttonTomarfotoRFCFrente: UIButton!
    @IBOutlet weak var buttonMostrarfotoRFCFrente: UIButton!
    @IBOutlet weak var imageViewfotoRFCFrente: UIImageView!
    
    @IBOutlet weak var buttonTomarfotoActaHoja1: UIButton!
    @IBOutlet weak var buttonMostrarfotoActaHoja1: UIButton!
    @IBOutlet weak var imageViewfotoActaHoja1: UIImageView!
    
    @IBOutlet weak var buttonTomarfotoPoderNotarialCaratula1: UIButton!
    @IBOutlet weak var buttonMostrarfotoPoderNotarialCaratula1: UIButton!
    @IBOutlet weak var imageViewfotoPoderNotarialCaratula1: UIImageView!
    
    fileprivate let presenter = CapturaDocumentosPresenter()
    var posActual : Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.inicio()
        presenter.attachView(view: self)
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow(){
        super.viewDidShow()
        presenter.viewDidShow()
    }
    
    private func setUpView() {
        userIDButton.isMultipleSelectionEnabled = true
        let cornerSize:Double = 5
        super.addRoundCornersInBottom(cornerRadius: cornerSize, view: buttonTomarfotoIdentificacionFrente)
        super.addRoundCornersInBottom(cornerRadius: cornerSize, view: buttonTomarfotoIdentificacionReverso)
        super.addRoundCornersInBottom(cornerRadius: cornerSize, view: buttonTomarfotoComprobanteDomicilio)
        super.addRoundCornersInBottom(cornerRadius: cornerSize, view: buttonTomarfotoRFCFrente)
        super.addRoundCornersInBottom(cornerRadius: cornerSize, view: buttonTomarfotoActaHoja1)
        super.addRoundCornersInBottom(cornerRadius: cornerSize, view: buttonTomarfotoPoderNotarialCaratula1)
    }
    
    @IBAction func onClickSiguiente(_ sender: UIButton) {
        presenter.clickSiguiente()
    }
    
    @IBAction func cambioValorCheckBox(_ sender: DLRadioButton) {
        presenter.cambioValorCheckBox()
    }
    
    
    @IBAction func clickTomarFotoIdentificacionOficialFrente(_ sender: UIButton) {
        sender.animateBound(view: sender){ [weak self] _ in
            self?.capturar(posicion: 0)
        }
    }
    
    @IBAction func clickTomarFotoIdentificacionOficialReverso(_ sender: UIButton) {
        sender.animateBound(view: sender){ [weak self] _ in
            self?.capturar(posicion: 1)
        }
    }
    
    @IBAction func clickTomarFotoComprobanteDomicilio(_ sender: UIButton) {
        sender.animateBound(view: sender){ [weak self] _ in
            self?.capturar(posicion: 2)
        }
    }
    
    @IBAction func clickTomarFotoRFCFrente(_ sender: UIButton) {
        sender.animateBound(view: sender){ [weak self] _ in
            self?.capturar(posicion: 3)
        }
    }
    
    @IBAction func clickTomarFotoActaHoja1(_ sender: UIButton) {
        sender.animateBound(view: sender){ [weak self] _ in
            self?.posActual = 4
            self?.importPDF()
        }
    }
    
    @IBAction func clickTomarFotoPoderNotarialCaratula1(_ sender: UIButton) {
        sender.animateBound(view: sender){ [weak self] _ in
            self?.posActual = 7
            self?.importPDF()
        }
    }
    
    @IBAction func clickMostrarIdentificacionOficialFrente(_ sender: UIButton) {
        sender.animateBound(view: sender)
        showImageDialog(titulo: "Identificacion oficial frontal", imagen: imageViewIdentificacionFrente.image!)
    }
    
    @IBAction func clickMostrarIdentificacionOficialReverso(_ sender: UIButton) {
        sender.animateBound(view: sender)
        showImageDialog(titulo: "Identificacion oficial reverso", imagen: imageViewIdentificacionReverso.image!)
    }
    
    @IBAction func clickMostrarComprobanteDomicilio(_ sender: UIButton) {
        sender.animateBound(view: sender)
        showImageDialog(titulo: "Comprobante de domicilio", imagen: imageViewfotoComprobanteDomicilio.image!)
    }
    
    @IBAction func clickMostrarRFCFrente(_ sender: UIButton) {
        sender.animateBound(view: sender)
        showImageDialog(titulo: "RFC frontal", imagen: imageViewfotoRFCFrente.image!)
    }
    
   
    @IBAction func clickMostrarActaHoja1(_ sender: UIButton) {
        sender.animateBound(view: sender)
        showImageDialog(titulo: "Acta constitutiva hoja 1", imagen: imageViewfotoActaHoja1.image!)
    }
    

    
    @IBAction func clickMostrarPoderNotarialCaratula1(_ sender: UIButton) {
        sender.animateBound(view: sender)
        showImageDialog(titulo: "Poder notarial, Carátula 1", imagen: imageViewfotoPoderNotarialCaratula1.image!)
    }
    
    
    func importPDF(){
        let importMenu = UIDocumentPickerViewController(documentTypes: ["com.adobe.pdf"], in: .import)
        
        importMenu.delegate = self
        
        self.present(importMenu, animated: true, completion: nil)
    }
    
    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        
        documentPicker.delegate = self
        
        self.present(documentPicker, animated: true, completion: nil)
        
    }
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL){
        switch posActual {
        case 0:
            imageViewIdentificacionFrente.image = UIImage(named: "icon_PDF")
        case 1:
            imageViewIdentificacionReverso.image = UIImage(named: "icon_PDF")
        case 2:
            imageViewfotoComprobanteDomicilio.image = UIImage(named: "icon_PDF")
        case 3:
            imageViewfotoRFCFrente.image = UIImage(named: "icon_PDF")
        case 4:
            imageViewfotoActaHoja1.image = UIImage(named: "icon_PDF")
        case 7:
            imageViewfotoPoderNotarialCaratula1.image = UIImage(named: "icon_PDF")
        default: break
            
        }
        let documento = CapturaDocumentosModel(
            Pdf: String("data:pdf/pdf;base64,\(FilesBase64.pdf(url))"),
            Imagen: "",
            pos: posActual,
            Tipo: "PDF"
        )
        presenter.saveDoc(posActual, documento)
    }

    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        
    }
    
    public func imageWithSize(image: UIImage, size:CGSize) -> UIImage {
        var scaledImageRect = CGRect.zero;
        
        let aspectWidth:CGFloat = size.width / image.size.width;
        let aspectHeight:CGFloat = size.height / image.size.height;
        let aspectRatio:CGFloat = min(aspectWidth, aspectHeight);
        
        scaledImageRect.size.width = image.size.width * aspectRatio;
        scaledImageRect.size.height = image.size.height * aspectRatio;
        scaledImageRect.origin.x = (size.width - scaledImageRect.size.width) / 2.0;
        scaledImageRect.origin.y = (size.height - scaledImageRect.size.height) / 2.0;
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0);
        
        image.draw(in: scaledImageRect);
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return scaledImage!;
    }
    
    }

extension CapturaDocumentos2VC: CapturaDocumentosDelegate{
    
    func clean() {
        imageViewIdentificacionFrente.image = UIImage(named: "img_camera")
        imageViewIdentificacionReverso.image = UIImage(named: "img_camera")
        imageViewfotoComprobanteDomicilio.image = UIImage(named: "img_camera")
        imageViewfotoRFCFrente.image = UIImage(named: "img_camera")
        imageViewfotoActaHoja1.image = UIImage(named: "img_camera")
        imageViewfotoPoderNotarialCaratula1.image = UIImage(named: "img_camera")
    }
    
    func tomarFotoActaHoja2(isTake: Bool) {
        
    }
    
    func tomarFotoActaHoja3(isTake: Bool) {
        
    }
    
    func tomarFotoPoderNotarialCaratula2(isTake: Bool) {
        
    }
    
    func imagenTomada(image: UIImageView) {
        let delayTime = DispatchTime.now() + Double(Int64(0.3 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            if let thumbnailBase64String = UIImageJPEGRepresentation(self.imageWithSize(image: image.image as! UIImage, size: CGSize(width: 1200, height: 800)), 0.65)?.base64EncodedString() {
                self.presenter.saveDoc(self.posActual, CapturaDocumentosModel(Pdf: "", Imagen: String("data:image/jpeg;base64,\(thumbnailBase64String)"), pos: self.posActual, Tipo: "IMAGEN"))
            }
        }
    }
    
    func faltanFotos() {
        super.showAlert(titulo: "Alerta", mensaje: "Debes tomar todas las fotos")
    }
    
    
    func cambiarvista(index: Int) {
        padreController?.cambiarVista(index: index)
    }
    
    func updateTitleHeader() {
        let header = HeaderData(
            imagen: #imageLiteral(resourceName: "logo_GenerarLead"),
            titulo: "Cuentas",
            titulo2: nil,
            titulo3: nil,
            subtitulo: "Carga de documentos",
            closure:{ [weak self] in
                self?.padreController?.cambiarVista(TablaVentasVC.self)
            }
       
        )
        super.updateTitle(headerData: header)
    }
    
    func startLoading() {
        super.showLoading(view: self.view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
    func estaSeleccionadoElCheckbox() -> Bool {
        if (userIDButton.isMultipleSelectionEnabled) {
            if userIDButton.selectedButtons().count > 0 {
                return true
            }
        }
        return false
    }
    
    func galeria(){
        switch posActual {
        case 0:
        tomarFotoIdentificacionOficialFrente(isTake: false)
        case 1:
        tomarFotoIdentificacionOficialReverso(isTake: false)
        case 2:
        tomarFotoComprobanteDomicilio(isTake: false)
        case 3:
        tomarFotoRFCFrente(isTake: false)
        case 4:
        tomarFotoActaHoja1(isTake: false)
        case 5:
        tomarFotoActaHoja2(isTake: false)
        case 6:
        tomarFotoActaHoja3(isTake: false)
        case 7:
        tomarFotoPoderNotarialCaratula1(isTake: false)
        case 8:
        tomarFotoPoderNotarialCaratula2(isTake: false)
        default: break
            
        }
    }
    
   
    
    func ocultarPanelComprobanteDomicilio(){
        mViewOtherID.isHidden = true
    }
    
    func mostrarPanelComprobanteDomicilio(){
        mViewOtherID.isHidden = false
    }
    
    func tomarFotoIdentificacionOficialFrente(isTake : Bool){
        if isTake{
            tomarFoto(imageViewIdentificacionFrente, nombreImagen: "identificacionOficialFrente.jpg")
        }else{
            importaGaleria(imageViewIdentificacionFrente, nombreImagen: "identificacionOficialFrente.jpg")
        }
    }
    
    
    
    func tomarFotoIdentificacionOficialReverso(isTake : Bool){
        if isTake{
            tomarFoto(imageViewIdentificacionReverso, nombreImagen: "identificacionOficialReverso.jpg")
        }else{
            importaGaleria(imageViewIdentificacionReverso, nombreImagen: "identificacionOficialReverso.jpg")
        }
    }
    
    func tomarFotoComprobanteDomicilio(isTake : Bool){
        if isTake{
            tomarFoto(imageViewfotoComprobanteDomicilio, nombreImagen: "identificacionComprobanteDomicilio.jpg")
        }else{
            importaGaleria(imageViewfotoComprobanteDomicilio, nombreImagen: "identificacionComprobanteDomicilio.jpg")
        }
    }
    
    func tomarFotoRFCFrente(isTake : Bool){
        if isTake{
            tomarFoto(imageViewfotoRFCFrente, nombreImagen: "fotoRFCFrente")
        }else{
            importaGaleria(imageViewfotoRFCFrente, nombreImagen: "fotoRFCFrente")
        }
    }
    
    func tomarFotoActaHoja1(isTake : Bool){
        if isTake{
            tomarFoto(imageViewfotoActaHoja1, nombreImagen: "fotoActaHoja1.jpg")
        }else{
            importaGaleria(imageViewfotoActaHoja1, nombreImagen: "fotoActaHoja1.jpg")
        }
    }
    
    
    
    func tomarFotoPoderNotarialCaratula1(isTake : Bool){
        if isTake{
            tomarFoto(imageViewfotoPoderNotarialCaratula1, nombreImagen: "fotoPoderNotarialCaratula1.jpg")
        }else{
            importaGaleria(imageViewfotoPoderNotarialCaratula1, nombreImagen: "fotoPoderNotarialCaratula1.jpg")
        }
    }
    
    
    
    func capturar(posicion:Int) {
        let alert = UIAlertController(
            title: "Capturar",
            message: "¿Que acción desea realizar?",
            preferredStyle: .alert
        )
        var titulo = "Tomar Foto"
        let action1 = UIAlertAction(title: titulo, style: .default){ [weak self] _ in
            self?.posActual = posicion
            self?.presenter.clickTomarFoto(posicion)
        }
        titulo = "Importar de Galleria"
        let action2 = UIAlertAction(title: titulo, style: .default){ [weak self] _ in
            self?.posActual = posicion
            self?.galeria()
        }
        titulo = "Importar PDF"
        let action3 = UIAlertAction(title:titulo,style: .default){ [weak self] _ in
            self?.posActual = posicion
            self?.importPDF()
        }
        
        titulo = "Cancelar"
        let cancel = UIAlertAction(title: titulo, style: .destructive, handler: nil)
        alert.view.tintColor = UIColor.blue
        alert.view.layer.cornerRadius = 25
        
        
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(action3)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
        
        
    }
}
