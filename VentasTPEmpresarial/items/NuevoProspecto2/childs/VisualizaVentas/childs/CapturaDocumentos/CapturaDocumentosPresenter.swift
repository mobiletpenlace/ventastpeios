//
//  CapturaDocumentosPresenter.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 15/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol CapturaDocumentosDelegate: BaseDelegate {
    func cambiarvista(index: Int)
    func estaSeleccionadoElCheckbox() -> Bool
    func ocultarPanelComprobanteDomicilio()
    func mostrarPanelComprobanteDomicilio()
    func faltanFotos()
    func tomarFotoIdentificacionOficialFrente(isTake : Bool)
    func tomarFotoIdentificacionOficialReverso(isTake : Bool)
    func tomarFotoComprobanteDomicilio(isTake : Bool)
    func tomarFotoRFCFrente(isTake : Bool)
    func tomarFotoActaHoja1(isTake : Bool)
    func tomarFotoActaHoja2(isTake : Bool)
    func tomarFotoActaHoja3(isTake : Bool)
    func tomarFotoPoderNotarialCaratula1(isTake : Bool)
    func tomarFotoPoderNotarialCaratula2(isTake : Bool)
    func imagenTomada(image : UIImageView)
    func clean()
}

class CapturaDocumentosPresenter{
    weak fileprivate var view: CapturaDocumentosDelegate?
    var data : [CapturaDocumentosModel?] = [nil,nil,nil,nil,nil,nil,nil,nil,nil]
    var valores = [false,false,true,false,false,true,true,false,true,]
    var count = 0
    
    func attachView(view: CapturaDocumentosDelegate){
        self.view = view
        view.updateTitleHeader()
        SwiftEventBus.onMainThread(self, name: "ImagenTomada") { [weak self] result in
            self?.view?.imagenTomada(image : result?.object as! UIImageView)
        }
        SwiftEventBus.onMainThread(self, name: "OnSuccessDocs") { [weak self] result in
            self?.view?.clean()
            self?.data = [nil,nil,nil,nil,nil,nil,nil,nil,nil]
            self?.valores = [false,false,true,false,false,true,true,false,true,]
            self?.count = 0
        }
    }
    
    func viewDidAppear(){
        view?.updateTitleHeader()
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
    }
    
    func detachView() {
        view = nil
    }
    
    func inicio(){
        count = 0
        for _ in data{
            data[count] = CapturaDocumentosModel(Pdf: "", Imagen: "", pos: count, Tipo: "")
            count = count + 1
        }
    }
    
    func saveDoc(_ pos: Int,_ doc : CapturaDocumentosModel?){
        data[pos] = doc
        valores[pos] = true
    }
    
    func clickTomarFoto(_ posicion:Int){
        if posicion == 0{
            view?.tomarFotoIdentificacionOficialFrente(isTake : true)
            valores[0] = true
        }else if posicion == 1{
            view?.tomarFotoIdentificacionOficialReverso(isTake : true)
            
        }else if posicion == 2{
            view?.tomarFotoComprobanteDomicilio(isTake : true)
            valores[2] = true
        }else if posicion == 3{
            view?.tomarFotoRFCFrente(isTake : true)
            valores[3] = true
        }else if posicion == 4{
            view?.tomarFotoActaHoja1(isTake : true)
            valores[4] = true
        }else if posicion == 5{
            view?.tomarFotoActaHoja2(isTake : true)
            valores[5] = true
        }else if posicion == 6{
            view?.tomarFotoActaHoja3(isTake : true)
            valores[6] = true
        }else if posicion == 7{
            view?.tomarFotoPoderNotarialCaratula1(isTake : true)
            valores[7] = true
        }else if posicion == 8{
            view?.tomarFotoPoderNotarialCaratula2(isTake : true)
            valores[8] = true
        }
        
    }
    
    
    
    func clickSiguiente(){
        if let vista = view{
            if sonValidosLosDatos(){
                SwiftEventBus.post(
                    "CapturaDocumentos_primeras",sender: data)
                vista.cambiarvista(index: 2)
            }
        }
    }
    
    func cambioValorCheckBox(){
        if let vista = view {
            vista.mostrarPanelComprobanteDomicilio()
            if (vista.estaSeleccionadoElCheckbox()) {
                vista.ocultarPanelComprobanteDomicilio()
            }
        }
    }
    
    private func sonValidosLosDatos() -> Bool{
        for v in valores{
            if v == false{
                view?.showBannerError(title: "Debes capturar todas las fotos para continuar.")
                return false
            }
        }
        return true
    }
    
}
