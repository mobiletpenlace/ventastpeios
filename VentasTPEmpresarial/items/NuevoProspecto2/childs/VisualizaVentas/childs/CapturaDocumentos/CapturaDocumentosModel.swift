//
//  CapturaDocumentosModel.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 15/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct CapturaDocumentosModel{
    var Pdf : String
    var Imagen : String
    var pos : Int
    var Tipo : String
}
