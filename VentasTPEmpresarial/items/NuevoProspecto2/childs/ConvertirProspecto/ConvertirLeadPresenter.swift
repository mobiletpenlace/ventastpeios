//
//  ConvertirLeadPresenter.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 10/12/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol ConvertirLeadDelegate: BaseDelegate {
    func update()
    func cambiarSiguienteVista()
    func clean()
    func actualizaFact(_ fact: String)
    func updateNombreCuenta(_ nombreCuenta: String)
}

class ConvertirLeadPresenter{
    fileprivate let service: ConvertirLeadService
    weak fileprivate var view: ConvertirLeadDelegate?
    fileprivate var data: ConvertirLeadModel!
    fileprivate var dataProspecto : TablaLeadModel?
    var listaEstados: [String] = [String]()
    var listaColonias: [String] = [String]()
    var listaCiudades: [String] = [String]()
    var listaDelegaciones: [String] = [String]()
    let listaSegmentos : [String] = ["Pequeñas","Medianas","Grande","Corporativos","Estratégicas","Gobierno"]
    
    init(service: ConvertirLeadService){
        self.service = service
        data = ConvertirLeadModel(
            idProspecto: "",
            calle: "",
            numExt: "",
            numInt: "",
            colonia: "",
            estado: "",
            ciudad: "",
            cp: "",
            municipio: "",
            fechaCierre: "",
            idEmpleado: "",
            facturacionP: "",
            segmento: "",
            rfc: "",
            nombreOportunidad: "",
            nombreCuenta: "")
    }
    
    func attachView(view: ConvertirLeadDelegate){
        self.view = view
        SwiftEventBus.onMainThread(self, name: "Flujo-Convierte-Lead") { [weak self] result in
            if let dataProspecto = result?.object as? TablaLeadModel{
                self?.dataProspecto = dataProspecto
            }
        }
        SwiftEventBus.onMainThread(self, name: "nuevoProspecto-Tip") { [weak self] result in
            //borrar esto.
        }
        SwiftEventBus.onMainThread(self, name: "convertirLead(setNombreCuenta)") { [weak self] result in
            if let nombreCuenta = result?.object as? String{
                self?.view?.updateNombreCuenta(nombreCuenta)
            }
        }
    }
    
    func viewDidAppear(){
        
    }
    
    func getEstados() -> [String]{
        return listaEstados
    }
    
    func getCiudades() -> [String]{
        return listaCiudades
    }
    
    func getDelegaciones() -> [String]{
        return listaDelegaciones
    }
    
    func getColonias() -> [String]{
        return listaColonias
    }
    
    func getArraySegmentos() -> [String]{
        return listaSegmentos
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
    }
    
    func detachView() {
        view = nil
    }
    
    func updateNombreOportunidad(item: String){
        data?.nombreOportunidad = item
    }
    
    func updateNombreCuenta(item: String){
        data?.nombreCuenta = item
    }
    
    func updateRFC(item: String){
        data?.rfc = item
    }
    
    func seleccionarCalle(item: String){
        data?.calle = item
    }
    
    func seleccionarMontoFacturacion(item: String){
        data?.segmento = item
        var facturacion = ""
        switch item {
        case "Pequeñas":
            facturacion = "De $1,000 a $30,000"
        case "Medianas":
            facturacion = "De $30,001 a $100,000"
        case "Grande":
            facturacion = "De $100,001 a $500,000"
        case "Corporativos":
            facturacion = " Más de $500,001"
        case "Estratégicas":
            facturacion = " Más de $1,000,000"
        case "Gobierno":
            facturacion = "N/A"
        default:
            facturacion = ""
        }
        data?.facturacionP = facturacion
        self.view!.actualizaFact(facturacion)
    }
    
    func seleccionarNumInt(item: String){
        data?.numInt = item
    }
    
    func updateNumExt(item: String){
        data?.numExt = item
    }
    
    func updateFecha(item: String){
        data?.fechaCierre = item
    }
    
    func updateEstado(item: String){
        data?.estado = item
    }
    
    func updateCiudad(item: String){
        data?.ciudad = item
    }
    
    func updateMunicipio(item: String){
        data?.municipio = item
    }
    
    func updateCodigoPostal(item: String){
        data?.cp = item
    }
    
    func updatecolonia(item: String){
        data?.colonia = item
    }
    
    func search(item: String, closure: (() -> Void)? = nil) {
        updateCodigoPostal(item: item)
        listaEstados = []
        listaColonias = []
        listaCiudades = []
        listaDelegaciones = []
        guard !item.isEmpty else {
            self.view?.showBannerAdvertencia(title: "codigo postal vacio.")
            return
        }
        view?.startLoading()
        
        let model = ConsultaCpRequest(codigoPostal: item)
        let url = Constantes.Url.Middleware.Prod.consultaCp
        let tipo: TipoRequest = .middle
        let decodeClass = ConsultaCpResponse.self
        
        service.getData(model: model, response: decodeClass, url: url, tipo) { [weak self] response in
            guard let `self` = self else { return }
            self.view?.finishLoading()
            guard let response = response else {
                self.view?.showBannerError(title: "Error en el servicio de consulta CP")
                return
            }
            guard let arrColonias = response.arrColonias else {
                self.view?.update()
                self.view?.showBannerError(title: "No hay colonias para ese codigo postal")
                return
            }
            self.llenarEstados(arrColonias: arrColonias)
            self.llenarColonias(arrColonias: arrColonias)
            self.llenarCiudades(arrColonias: arrColonias)
            self.llenarDelegaciones(arrColonias: arrColonias)
            self.view?.update()
            if let closure = closure {
                closure()
            }
        }
    }
    
    func llenarEstados(arrColonias: [ArrColonias]){
        var conteo = 0
        for colonia in arrColonias{
            let estado = colonia.estado?.trim().uppercased() ?? ""
            if self.listaEstados.count > 0{
                for elemento in self.listaEstados{
                    if elemento.trim().uppercased() == estado{
                        conteo = 1
                    }
                }
                if (conteo == 0){
                    self.listaEstados.append(estado)
                }
            }else{
                self.listaEstados.append(estado)
            }
        }
    }
    
    func llenarColonias(arrColonias: [ArrColonias]){
        var conteo = 0
        for colonia in arrColonias{
            let coloniaRes = colonia.colonia?.trim().uppercased() ?? ""
            if listaColonias.count > 0{
                for elemento in listaColonias{
                    if elemento.trim().uppercased() == coloniaRes{
                        conteo = 1
                    }
                }
                if (conteo == 0){
                    listaColonias.append(coloniaRes)
                }
            }else{
                listaColonias.append(coloniaRes)
            }
        }
    }
    
    func llenarCiudades(arrColonias: [ArrColonias]){
        var conteo = 0
        for colonia in arrColonias{
            let ciudad = colonia.ciudad?.trim().uppercased() ?? ""
            if listaCiudades.count > 0{
                for elemento in listaCiudades{
                    if elemento.trim().uppercased() == ciudad{
                        conteo = 1
                    }
                }
                if (conteo == 0){
                    listaCiudades.append(ciudad)
                }
            }else{
                listaCiudades.append(ciudad)
            }
        }
    }
    
    func llenarDelegaciones(arrColonias: [ArrColonias]) {
        var conteo = 0
        for colonia in arrColonias{
            let delMun = colonia.delegacionMunicipio?.trim().uppercased() ?? ""
            if listaDelegaciones.count > 0{
                for elemento in listaDelegaciones{
                    if elemento.trim().uppercased() == delMun{
                        conteo = 1
                    }
                }
                if (conteo == 0){
                    listaDelegaciones.append(delMun)
                }
            }else{
                listaDelegaciones.append(delMun)
            }
        }
    }
    
    func clickSiguiente() {
        view?.startLoading()
        let id = service.getIdEmpleado()
        let rfc = data.rfc
        
//        let model = SFPaternityRequest(idEmpleado: id, rfc: rfc)
//        let url = Constantes.Url.SalesFor.paternidad
//        let tipo: TipoRequest = .sf
//        let decodeClass = SFPaternityResponse.self
        
        let model = MiddlePaternityRequest(idEmpleado: id, rfc: rfc)
        let url = Constantes.Url.Middleware.Qa.paternidad
        let tipo: TipoRequest = .middle
        let decodeClass = MiddlePaternityResponse.self
        
        service.getData(model: model, response: decodeClass, url: url, tipo) { [weak self] response in
            guard let `self` = self else { return }
            guard let response = response else {
                self.view?.finishLoading()
                self.view?.showBannerError(title: "Error en el servicio de validación de paternidad de la venta")
                return
            }
            if response.validador == "0" {
                self.convierteLead()
            } else {
                self.view?.finishLoading()
                self.view?.showBannerError(title: "No se puede continuar, por la validación de paternidad del RFC.")
            }
        }
    }
    
    func convierteLead() {
        let idProspecto = dataProspecto?.idLead ?? ""
        let id = service.getIdEmpleado()
        
//        let modelo = SFConvertirLeadRequest(idProspecto: idProspecto, calle: data.calle, numExt: data.numExt, numInt: data.numInt, colonia: data.colonia, estado: data.estado, ciudad: data.ciudad, cp: data.cp, municipio: data.municipio, fechaCierre: data.fechaCierre, facturacionP: data.facturacionP, idEmpleado: id, rfc: data.rfc, nombreOportunidad: data.nombreOportunidad, nombreCuenta: data.nombreCuenta)
//        let url = Constantes.Url.SalesFor.creaCuenta
//        let tipo: TipoRequest = .sf
//        let decodeClass = SFConvertirLeadResponse.self

        let modelo = MiddleConvertirLeadRequest(idProspecto: idProspecto, calle: data.calle, numExt: data.numExt, numInt: data.numInt, colonia: data.colonia, estado: data.estado, ciudad: data.ciudad, cp: data.cp, municipio: data.municipio, fechaCierre: data.fechaCierre, facturacionP: data.facturacionP, idEmpleado: id, rfc: data.rfc, nombreOportunidad: data.nombreOportunidad, nombreCuenta: data.nombreCuenta)
        let url = Constantes.Url.Middleware.Qa.creaCuenta
        let tipo: TipoRequest = .middle
        let decodeClass = MiddleConvertirLeadResponse.self
        
        service.getData(model: modelo, response: decodeClass, url: url, tipo) { [weak self] response in
            guard let `self` = self else { return }
            self.view?.finishLoading()
            guard let response = response else {
                self.view?.showBannerError(title: "No se ha podido convertir el lead.")
                Logger.e("No se ha convetido el lead")
                return
            }
            if response.response?.result == "0" {
                self.informarDatosOportunidad(oportunidad: response.oportunidad, cuenta: response.cuenta)
                self.view?.showBannerOK(title: "Se ha convertido el lead.")
                self.view?.cambiarSiguienteVista()
                self.view?.clean()
            } else {
                self.view?.showBannerError(title: "Error en el servicio.")
            }
        }
    }
    
    func informarDatosOportunidad(oportunidad: String?, cuenta: String?) {
        SwiftEventBus.post(
            "Cotizacion_DetalleSitio_Resumencotizacion(setdatosOportunidad)",
            sender: (oportunidad, data.nombreOportunidad, cuenta)
        )
    }
    
    func getData() -> ConvertirLeadModel? {
        return data
    }
}
