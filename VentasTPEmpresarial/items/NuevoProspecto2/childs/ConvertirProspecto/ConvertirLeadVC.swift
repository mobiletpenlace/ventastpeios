//
//  ConvertirLeadVC.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 09/12/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import UIKit
import SwiftEventBus

class ConvertirLeadVC: BaseItemVC , BaseFormatDate{
    @IBOutlet weak var viewDatosContactoComercial: UIView!
    @IBOutlet weak var textfieldNombreOportunidad: UITextField!
    @IBOutlet weak var textfieldNombreCuenta: UITextField!
    @IBOutlet weak var textfieldRFC: UITextField!
    @IBOutlet weak var textfieldFecha: UITextField!
    @IBOutlet weak var textfieldMontoFacturacion: UITextField!
    @IBOutlet weak var textfieldSegmento: UITextField!
    @IBOutlet weak var textfieldCalle: UITextField!
    @IBOutlet weak var textfieldNumInterior: UITextField!
    @IBOutlet weak var textfieldNumExterior: UITextField!
    @IBOutlet weak var textfieldEstado: UITextField!
    @IBOutlet weak var textfieldCiudad: UITextField!
    @IBOutlet weak var textfieldDelegacionMunicipio: UITextField!
    @IBOutlet weak var textfieldCP: UITextField!
    @IBOutlet weak var textfieldColonia: UITextField!
    fileprivate let presenter = ConvertirLeadPresenter(service: ConvertirLeadService())
    let dropManager = DropDownUtil()
    let validatorManager = ValidatorManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        presenter.attachView(view: self)
    }
    
    override func viewDidShow() {
        super.viewDidShow()
        presenter.viewDidShow()
        validatorManager.setUpTextfields()
        clean()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    private func setUpView() {
        setUpValidator()
        setUPBeginEdit()
        setUPEndEdit()
        setUpDropDown()
    }
    
    @IBAction func searchAction(_ sender: UIButton) {
        sender.animateBound(view: sender){ [weak self] anim in
            guard let mSelf = self else { return }
            let text = mSelf.textfieldCP.text ?? ""
            mSelf.presenter.search(item: text)
        }
    }
    
    @IBAction func clickSiguiente(_ sender: UIButton) {
        view.endEditing(true)
        sender.animateBound(view: sender) { [weak self] anim in
            guard let mSelf = self else { return }
            mSelf.presenter.updateCiudad(item: mSelf.textfieldCiudad.text ?? "")
            mSelf.presenter.updateEstado(item: mSelf.textfieldEstado.text ?? "")
            mSelf.presenter.updateMunicipio(item: mSelf.textfieldDelegacionMunicipio.text ?? "")
            mSelf.presenter.updatecolonia(item: mSelf.textfieldColonia.text ?? "")
            let validateDrop = mSelf.dropManager.validate()
            let validateTextFields = mSelf.validatorManager.validate()
            if validateDrop && validateTextFields {
                mSelf.presenter.clickSiguiente()
            } else {
                mSelf.showBannerError(title: "Hay campos con errores.")
            }
        }
    }
    
    func setUpDropDown() {
        textfieldEstado.delegate = dropManager
        textfieldCiudad.delegate = dropManager
        textfieldDelegacionMunicipio.delegate = dropManager
        textfieldColonia.delegate = dropManager
        textfieldSegmento.delegate = dropManager
        
        dropManager.add(textfield: textfieldEstado, data: presenter.getEstados()) { [weak self] (index: Int, item: String) in
            self?.presenter.updateEstado(item: item)
        }
        
        dropManager.add(textfield: textfieldCiudad, data: presenter.getCiudades()) { [weak self] (index: Int, item: String) in
            self?.presenter.updateCiudad(item: item)
        }
        
        dropManager.add(textfield: textfieldDelegacionMunicipio, data: presenter.getDelegaciones()) { [weak self] (index: Int, item: String) in
            self?.presenter.updateMunicipio(item: item)
        }
        
        dropManager.add(textfield: textfieldColonia, data: presenter.getColonias()) { [weak self] (index: Int, item: String) in
            self?.presenter.updatecolonia(item: item)
        }
        
        dropManager.add(textfield: textfieldSegmento, data: presenter.getArraySegmentos()) { [weak self] (index: Int, item: String) in
            self?.presenter.seleccionarMontoFacturacion(item: item)
        }
    }
    
    func setUpValidator() {
        textfieldNombreOportunidad.delegate = validatorManager
        textfieldNombreCuenta.delegate = validatorManager
        textfieldRFC.delegate = validatorManager
        textfieldFecha.delegate = validatorManager
        textfieldMontoFacturacion.delegate = validatorManager
        textfieldCalle.delegate = validatorManager
        textfieldNumInterior.delegate = validatorManager
        textfieldNumExterior.delegate = validatorManager
        textfieldCP.delegate = validatorManager
        
        validatorManager.addValidator(
            
            FieldValidator.textField(
                textfieldNombreOportunidad, //textfield
                .required, //validacion del texto mientras.
                .all, // tipo de caracteres validos
                "El campo no puede estar vacío"
            ),
            
            FieldValidator.textField(
                textfieldNombreCuenta, //textfield
                .required, //validacion del texto mientras.
                .nombre, // tipo de caracteres validos
                "El campo no puede estar vacío"
            ),
            
            FieldValidator.textField(
                textfieldRFC, //textfield
                .regexWithLimit(.rfc, 13), //validacion del texto mientras.
                .rfc, // tipo de caracteres validos
                "Revisa el formato de RFC con homoclave"
            ),
            
            FieldValidator.textField(
                textfieldMontoFacturacion, //textfield
                .none, //validacion del texto mientras.
                .all, // tipo de caracteres validos
                ""
            ),
            
            FieldValidator.textField(
                textfieldCalle, //textfield
                .required, //validacion del texto mientras.
                .nombre, // tipo de caracteres validos
                "El campo no puede estar vacío"
            ),
            
            FieldValidator.textField(
                textfieldCP, //textfield
                .exact(5), //validacion del texto mientras.
                .numero, // tipo de caracteres validos
                "El debe tener 5 números"
            ),
            
            FieldValidator.textField(
                textfieldNumExterior, //textfield
                .required, //validacion del texto mientras.
                .calleNumero, // tipo de caracteres validos
                "El campo no puede estar vacío"
            ),
            
            FieldValidator.textField(
                textfieldNumInterior, //textfield
                .none, //validacion del texto mientras.
                .calleNumero, // tipo de caracteres validos
                ""
            )
        )
    }
    
    func setUPBeginEdit() {
        validatorManager.addChangeEnterField(from: textfieldNombreOportunidad, to: textfieldNombreCuenta)
        validatorManager.addChangeEnterField(from: textfieldNombreCuenta, to: textfieldRFC)
        validatorManager.addChangeEnterField(from: textfieldRFC, to: textfieldCP)
        validatorManager.addChangeEnterField(from: textfieldCP, to: textfieldCalle)
        validatorManager.addChangeEnterField(from: textfieldCalle, to: textfieldNumExterior)
        validatorManager.addChangeEnterField(from: textfieldNumExterior, to: textfieldNumInterior)
    }
    
    func setUPEndEdit() {
        validatorManager.addEndEditingFor(textfieldNombreOportunidad) { [weak self] (textField) -> (Void) in
            self?.presenter.updateNombreOportunidad(item: textField.text ?? "")
        }
        validatorManager.addEndEditingFor(textfieldNombreCuenta) { [weak self] (textField) -> (Void) in
            self?.presenter.updateNombreCuenta(item: textField.text ?? "")
        }
        validatorManager.addEndEditingFor(textfieldRFC) { [weak self] (textField) -> (Void) in
            self?.presenter.updateRFC(item: textField.text ?? "")
        }
        validatorManager.addEndEditingFor(textfieldCalle) { [weak self] (textField) -> (Void) in
            self?.presenter.seleccionarCalle(item: textField.text ?? "")
        }
        validatorManager.addEndEditingFor(textfieldFecha) { [weak self] (textField) -> (Void) in
            self?.presenter.updateFecha(item: textField.text ?? "")
        }
        validatorManager.addEndEditingFor(textfieldNumInterior) { [weak self] (textField) -> (Void) in
            self?.presenter.seleccionarNumInt(item: textField.text ?? "")
        }
        validatorManager.addEndEditingFor(textfieldNumExterior) { [weak self] (textField) -> (Void) in
            self?.presenter.updateNumExt(item: textField.text ?? "")
        }
        validatorManager.addEndEditingFor(textfieldMontoFacturacion) { [weak self] (textField) -> (Void) in
            self?.presenter.seleccionarMontoFacturacion(item: textField.text ?? "")
        }
        validatorManager.addEndEditingFor(textfieldEstado) { [weak self] (textField) -> (Void) in
            self?.presenter.updateEstado(item: textField.text ?? "")
        }
        validatorManager.addEndEditingFor(textfieldCiudad) { [weak self] (textField) -> (Void) in
            self?.presenter.updateCiudad(item: textField.text ?? "")
        }
        validatorManager.addEndEditingFor(textfieldDelegacionMunicipio) { [weak self] (textField) -> (Void) in
            self?.presenter.updateMunicipio(item: textField.text ?? "")
        }
        validatorManager.addEndEditingFor(textfieldCP) { [weak self] (textField) -> (Void) in
            self?.presenter.updateCodigoPostal(item: textField.text ?? "")
            self?.presenter.search(item: textField.text ?? "")
        }
        validatorManager.addEndEditingFor(textfieldColonia) { [weak self] (textField) -> (Void) in
            self?.presenter.updatecolonia(item: textField.text ?? "")
        }
    }
    
    public func clean() {
        textfieldNombreOportunidad.text = ""
        textfieldNombreCuenta.text = ""
        textfieldRFC.text = ""
        textfieldFecha.text = ""
        textfieldMontoFacturacion.text = ""
        textfieldCalle.text = ""
        textfieldNumInterior.text = ""
        textfieldNumExterior.text = ""
        textfieldCP.text = ""
        
        textfieldEstado.text = ""
        textfieldCiudad.text = ""
        textfieldDelegacionMunicipio.text = ""
        textfieldColonia.text = ""
        textfieldSegmento.text = ""
        
        dropManager.clear()
    }
    
}

extension ConvertirLeadVC: ConvertirLeadDelegate {
    
    func updateNombreCuenta(_ nombreCuenta: String){
        Logger.i("nombre cuenta: \(nombreCuenta)")
        textfieldNombreCuenta.text = nombreCuenta
        presenter.updateNombreCuenta(item: nombreCuenta)
    }
    
    func actualizaFact(_ fact: String) {
        textfieldMontoFacturacion.text = fact
    }
    
    func update() {
        dropManager.updateDropdown(pos: 0, array: presenter.getEstados(), textfield: textfieldEstado)
        dropManager.updateDropdown(pos: 0, array: presenter.getCiudades(), textfield: textfieldCiudad)
        dropManager.updateDropdown(pos: 0, array: presenter.getDelegaciones(), textfield: textfieldDelegacionMunicipio)
        dropManager.updateDropdown(pos: 0, array: presenter.getColonias(), textfield: textfieldColonia)
    }
    
    func setEmptyProgress() {
        
    }
    
    func updateTitleHeader() {
        let header = HeaderData(
            imagen: #imageLiteral(resourceName: "logo_GenerarLead"),
            titulo: "Cuentas",
            titulo2: "Prospectos",
            titulo3: nil,
            subtitulo: "Convertir Prospecto",
            closure:{ [weak self] in
                self?.padreController?.cambiarVista(index: 0)
            }
        )
        super.updateTitle(headerData: header)
    }
    
    func startLoading() {
        super.showLoading(view: view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
    func cambiarSiguienteVista() {
        SwiftEventBus.post("Reload-table", sender: nil)
        padreController?.cambiarVista(index: 2)
    }
    
}

