import Foundation

struct ConvertirLeadModel {
    var idProspecto: String
    var calle: String
    var numExt: String
    var numInt: String
    var colonia: String
    var estado: String
    var ciudad: String
    var cp: String
    var municipio: String
    var fechaCierre: String
    var idEmpleado: String
    var facturacionP: String
    var segmento: String
    var rfc: String
    var nombreOportunidad: String
    var nombreCuenta: String
}
