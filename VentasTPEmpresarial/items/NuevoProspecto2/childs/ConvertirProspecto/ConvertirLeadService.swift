import Foundation
import RealmSwift

class ConvertirLeadService: BaseDBManagerDelegate {
    var serverManager: ServerDataManager?
    var realm: Realm!
    var dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()
        serverManager = ServerDataManager()
        realm = getRealm()
    }
    
    //Se ocupa por verificar cobertura
    func consultaCp(cp: String,_ callBack:@escaping (ConsultaCpResponse?) -> Void){
        let model = ConsultaCpRequest(codigoPostal: cp)
        let url = Constantes.Url.Middleware.Prod.consultaCp
        let tipo: TipoRequest = .middle
        let decodeClass = ConsultaCpResponse.self
        serverManager?.post(model: model, url: url, tipo: tipo) { responseData in
            guard let data = responseData.data else {
                Logger.e("error en el servicio de consulta cp")
                callBack(nil)
                return
            }
            do {
                let decoder = JSONDecoder()
                let responseOBJ = try decoder.decode(decodeClass, from: data)
                callBack(responseOBJ)
            } catch let err {
                Logger.e("error: \(err)")
                callBack(nil)
            }
        }
    }
}
