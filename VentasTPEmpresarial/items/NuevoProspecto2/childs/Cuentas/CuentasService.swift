//
//  CuentasService.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 08/01/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//


import Foundation
import RealmSwift

class CuentasService: BaseDBManagerDelegate {
    var realm: Realm!
    var serverManager: ServerDataManager?
    var dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()
        realm = getRealm()
        serverManager = ServerDataManager()
    }
}
