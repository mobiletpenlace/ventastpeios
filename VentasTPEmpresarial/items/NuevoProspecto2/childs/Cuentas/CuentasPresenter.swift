//
//  CuentasPresenter.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 08/01/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//


import Foundation
import SwiftEventBus

protocol CuentasDelegate: BaseDelegate {
    func reloadTable()
    func showEditar(index: IndexPath)
    func quitDialog()
    func seleccionarCeldaTabla(indexPath: IndexPath)
}

class CuentasPresenter {
    weak fileprivate var view: CuentasDelegate?
    fileprivate let service: CuentasService?
    fileprivate var dataFilter = [CuentasModel]()
    fileprivate var dataDownloaded = [CuentasModel]()
    var filtro: String = ""
    
    init(service: CuentasService){
        self.service = service
    }
    
    func attachView(view: CuentasDelegate){
        self.view = view
        SwiftEventBus.onMainThread(self, name: "Acciones_Cuenta") { [weak self] result in
            if let indexpath = result?.object as? IndexPath{
                self?.view?.showEditar(index: indexpath)
            }
        }
    }
    
    func viewDidAppear(){
        view?.updateTitleHeader()
    }
    
    /*protocol*/
    func viewWillAppear() {
        
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
        actualizarDatos()
    }
    
    func detachView() {
        view = nil
    }
    
    func getItemIndex(index: Int) -> CuentasModel? {
        if index < 0 && index >= dataFilter.count{
            return nil
        }
        return dataFilter[index]
    }
    
    func getSize() -> Int{
        return dataFilter.count
    }
    
    func cuentaASC(){
        dataFilter.sort(by: { $0.name < $1.name })
        view?.reloadTable()
    }
    
    func cuentaDESC(){
        dataFilter.sort(by: { $0.name > $1.name })
        view?.reloadTable()
    }
    
    func nuevaOP(index: Int, nombre: String, fecha: String){
        let id = service?.getIdEmpleado() ?? ""
        let cuenta = dataFilter[index].id
        
//        let model = SFCrearOportunidadRequest(idCuenta: cuenta, nombreOportunidad: nombre,
//          fechaCierreOportunidad: fecha, empleado: id, accion: "Nuevo")
//        let url = Constantes.Url.SalesFor.crearOportunidad
//        let tipo: TipoRequest = .sf
//        let decodeClass = SFCrearOportunidadResponse.self
        
        let model = MiddleCrearOportunidadRequest(idCuenta: cuenta, nombreOportunidad: nombre,
                                              fechaCierreOportunidad: fecha, empleado: id, accion: "Nuevo")
        let url = Constantes.Url.Middleware.Qa.crearOportunidad
        let tipo: TipoRequest = .middle
        let decodeClass = MiddleCrearOportunidadResponse.self
        
        service?.getData(model: model, response: decodeClass, url: url, tipo) { response in
            self.view?.finishLoading()
            guard let data = response else {
                Logger.d("no hay data en crear oportunidad...")
                self.view?.showBannerError(title: "Ha ocurrido un error en el servicio de crear oportunidad")
                return
            }
//            if data.result == "0" {
            if data.response?.result == "0" {
                self.view?.showBannerOK(title: "Se ha creado la oportunidad.")
                self.view?.quitDialog()
            } else {
                self.view?.showBannerError(title: "No se ha podido crear la oportunidad.")
            }
        }
    }
    
    func filtrar(filtro : String){
        let filtroUpper = filtro.uppercased()
        dataFilter = dataDownloaded.filter { dato in
            return dato.name.uppercased().contains(filtroUpper)
                || filtroUpper.count == 0
        }
        cuentaDESC()
        view?.reloadTable()
        view?.seleccionarCeldaTabla(indexPath: IndexPath(row: 0, section: 0))
    }
    
    func actualizarDatos() {
        dataDownloaded = [CuentasModel]()
        dataFilter = [CuentasModel]()
        view?.reloadTable()
        
        self.view?.startLoading()
        
        let id = service?.getIdEmpleado() ?? ""
        
//        let model = SFCuentasRequest(idEmpleado: id)
//        let url = Constantes.Url.SalesFor.misCuentas
//        let tipo: TipoRequest = .sf
//        let decodeClass = SFCuentasResponse.self
        
        let model = MiddleCuentasRequest(idEmpleado: id)
        let url = Constantes.Url.Middleware.Qa.misCuentas
        let tipo: TipoRequest = .middle
        let decodeClass = MiddleCuentasResponse.self
        
        service?.getData(model: model, response: decodeClass, url: url, tipo){ [weak self] response in
            self?.onFinishGetData(data: response)
        }
    }
    
    func onFinishGetData(data: MiddleCuentasResponse?) {
        view?.finishLoading()
        guard let data = data, let lista = data.listaCuentas else {
            Logger.d("no hay data en lista de ventas...")
            self.view?.showBannerError(title: "No se ha podido obtener los datos de las ventas.")
            return
        }
        var countLista = 0
        for cuenta in lista {
            self.dataDownloaded.append(
                CuentasModel(
                    telefono: cuenta.telefono ?? "",
                    segmentoFacturacion: cuenta.segmentoFacturacion ?? "",
                    rfc: cuenta.rfc ?? "",
                    name: cuenta.name ?? "",
                    id: cuenta.id ?? "",
                    folioCuenta: cuenta.folioCuenta ?? "",
                    email: cuenta.email ?? ""
                )
            )
            countLista = countLista + 1
        }
        filtrar(filtro: filtro)
    }
    
}


