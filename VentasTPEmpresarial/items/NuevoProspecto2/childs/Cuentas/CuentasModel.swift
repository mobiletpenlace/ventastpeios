import Foundation

struct CuentasModel {
    var telefono: String
    var segmentoFacturacion: String
    var rfc: String
    var name: String
    var id: String
    var folioCuenta: String
    var email: String
}
