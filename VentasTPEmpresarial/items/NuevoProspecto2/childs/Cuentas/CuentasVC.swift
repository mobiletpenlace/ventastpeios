//
//  CuentasVC.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 08/01/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import UIKit

class CuentasVC: BaseItemVC, BaseFormatDate {
    
    @IBOutlet weak var CuentasTableView: UITableView!
    @IBOutlet weak var ViewContainerTable: UIView!
    @IBOutlet weak var ordenPorTextField: UITextField!
    @IBOutlet weak var imageCuenta: UIImageView!
    @IBOutlet weak var anchoFiltroView: NSLayoutConstraint!
    
    @IBOutlet weak var popTable: UIView!
    @IBOutlet weak var altoTableView: NSLayoutConstraint!
    @IBOutlet weak var nameOPTextField: UITextField!
    @IBOutlet weak var fechaOPTextField: UITextField!
    var cuenta = false
    var nombreCelda2: String = "CuentasTableViewCell"
    var indexActual = 0
    var limiteSuperiorFechaNacimientoAños = 1
    
    var presenter = CuentasPresenter(service : CuentasService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        anchoFiltroView.constant = 0
        altoTableView.constant = 0
        view.layoutIfNeeded()
        presenter.attachView(view: self)
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow(){
        super.viewDidShow()
        presenter.viewDidShow()
    }
    
    @IBAction func onClickFiltrar(_ sender: Any) {
        UIView.animate(withDuration: 0.3,delay: 0.1, options: .curveEaseOut, animations:{
            self.anchoFiltroView.constant = 240
            self.view.layoutIfNeeded()
        }, completion : nil)
    }
    
    
    @IBAction func onClickCuenta(_ sender: Any) {
        if cuenta{
            imageCuenta.image = UIImage(named: "icon_FlechaAbajo")
            cuenta = false
            presenter.cuentaDESC()
        }else{
            imageCuenta.image = UIImage(named: "icon_FlechaArriba")
            cuenta = true
            presenter.cuentaASC()
        }
    }
    
    @IBAction func onClickCancel(_ sender: Any) {
        UIView.animate(withDuration: 0.3,delay: 0.1, options: .curveEaseOut, animations:{
            self.anchoFiltroView.constant = 0
            self.view.layoutIfNeeded()
        }, completion : nil)
        self.view.endEditing(true)
        ordenPorTextField.text = ""
        presenter.filtro = ""
        presenter.filtrar(filtro : "")
    }
    
    func setUpView(){
        let nib = UINib.init(nibName: nombreCelda2, bundle: nil)
        self.CuentasTableView.register(nib, forCellReuseIdentifier: nombreCelda2)
        self.CuentasTableView.delegate = self
        self.CuentasTableView.dataSource = self
        self.fechaOPTextField.delegate = self
        super.addShadowView(view: ViewContainerTable)
        self.ordenPorTextField.delegate = self
        
        addLineBotom(nameOPTextField, color: Colores.negro, width: 1)
        
        ordenPorTextField.addTarget(self, action: #selector(textFieldEditingDidChange(_:)), for: UIControlEvents.editingChanged)
    }
    
    @objc func textFieldEditingDidChange(_ sender: UITextField) {
        presenter.filtro = sender.text ?? ""
        presenter.filtrar(filtro: sender.text ?? "")
    }
    
    @IBAction func Continue(_ sender: Any) {
        startLoading()
        presenter.nuevaOP(index: indexActual, nombre: nameOPTextField.text!, fecha: fechaOPTextField.text!)
    }
    
    @IBAction func Cancel(_ sender: Any) {
        UIView.animate(withDuration: 0.3,delay: 0.1, options: .curveEaseOut, animations:{
            self.altoTableView.constant = 0
            self.view.layoutIfNeeded()
        }, completion : nil)
    }
    
    
    func nuevaOP(index : Int){
        indexActual =  index
        UIView.animate(withDuration: 0.3,delay: 0.1, options: .curveEaseOut, animations:{
            self.altoTableView.constant = 550
            self.view.layoutIfNeeded()
        }, completion : nil)
        
    }
    
    func showDatePicker(){
        let dateNow = Date()
        let limitInf = incrementDate(dateNow, value: -100, byAdding: .year)
        let limitSup = incrementDate(dateNow, value: limiteSuperiorFechaNacimientoAños, byAdding: .year)
        DatePickerDialog().show("Seleccione Fecha", defaultDate: dateNow, minDate: limitInf, maxDate: limitSup, datePickerMode: .date){
            (date) -> Void in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = Constantes.Format.formatoFecha2
                self.fechaOPTextField.text = formatter.string(from: dt)
            }
        }
    }
    
    private func addLineBotom(_ view: UIView, color: UIColor, width: CGFloat) {
        let tag = 1000
        if let viewWithTag = view.viewWithTag(tag) {
            viewWithTag.removeFromSuperview()
        }
        
        let underlineFrame = CGRect(
            x: 0,
            y: view.bounds.size.height - width,
            width: view.frame.size.width,
            height: width
        )
        
        let underline = UIView(frame: underlineFrame)
        underline.backgroundColor = color
        underline.tag = tag
        view.addSubview(underline)
        view.layer.masksToBounds = true
    }
    
}

extension CuentasVC: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return CuentaHeaderVC().view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        let data = presenter.getItemIndex(index: indexPath.row)
        
    }
}

extension CuentasVC: UITableViewDataSource, BaseFormatNumber {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getSize()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: nombreCelda2, for: indexPath) as! CuentasTableViewCell
        guard let data = presenter.getItemIndex(index: indexPath.row) else { return cell}
        cell.nombreCuentaLabel.text = data.name
        cell.index = indexPath
        cell.selectionStyle = .none //desactivar el color al seleccionar la celda
        return cell
    }
    
}

extension CuentasVC: CuentasDelegate {
    
    func seleccionarCeldaTabla(indexPath: IndexPath){
        
        let n = CuentasTableView.numberOfRows(inSection: 0)
        if n > 0{
            CuentasTableView.selectRow(at: indexPath, animated: true, scrollPosition: .bottom)
        }
    }
    
    func quitDialog() {
        UIView.animate(withDuration: 0.3,delay: 0.1, options: .curveEaseOut, animations:{
            self.altoTableView.constant = 0
            self.view.layoutIfNeeded()
        }, completion : nil)
        nameOPTextField.text = ""
        fechaOPTextField.text = ""
        indexActual = 0
    }
    
    func showEditar(index: IndexPath) {
        let alert = UIAlertController(
            title: "Cuenta",
            message: "¿Que acción desea realizar?",
            preferredStyle: .alert
        )
        var titulo = "Crear una nueva oportunidad"
        let action1 = UIAlertAction(title: titulo, style: .default){ [weak self] _ in
            self?.nuevaOP(index : index.row)
        }
        titulo = "Cancelar"
        let cancel = UIAlertAction(title: titulo, style: .destructive, handler: nil)
        alert.view.tintColor = UIColor.blue
        alert.view.layer.cornerRadius = 25
        
        alert.addAction(action1)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    func reloadTable() {
        CuentasTableView.reloadData()
    }
    
    
    func updateTitleHeader() {
        let header = HeaderData(
            imagen: #imageLiteral(resourceName: "logo_GenerarLead"),
            titulo: "Cuentas",
            titulo2: nil,
            titulo3: nil,
            subtitulo: "Cuentas",
            closure: nil
        )
        super.updateTitle(headerData: header)
    }
    
    func startLoading() {
        super.showLoading(view: CuentasTableView)
    }
    
    func finishLoading() {
        super.hideLoading()
    } 
    
    
}

extension CuentasVC: UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == fechaOPTextField{
            showDatePicker()
            return false;
        }
        return true
    }
}
