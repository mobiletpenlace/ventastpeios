//
//  TablaLeadService.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 07/12/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import RealmSwift

class TablaLeadService: BaseDBManagerDelegate {
    var realm: Realm!
    let serverManager: ServerDataManager?
    var dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()
        serverManager = ServerDataManager()
        realm = getRealm()
    }
}
