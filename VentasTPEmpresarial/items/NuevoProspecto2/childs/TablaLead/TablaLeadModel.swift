import Foundation

struct TablaLeadModel {
    var tipoPersona: String
    var empresa: String
    var nombre: String
    var idLead: String
    var apellidoPaterno: String
    var apellidoMaterno: String
    var telefono: String
    var correo: String
    var etapaOportunidad: String
}
