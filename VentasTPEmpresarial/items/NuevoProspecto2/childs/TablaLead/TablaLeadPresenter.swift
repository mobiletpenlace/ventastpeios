//
//  TablaLeadPresenter.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 07/12/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

import SwiftEventBus

protocol TablaLeadDelegate: BaseDelegate {
    func reloadTable()
    func showEditar(index : IndexPath)
    func cambiaVista(index : Int)
}

class TablaLeadPresenter {
    weak fileprivate var view: TablaLeadDelegate?
    fileprivate let service: TablaLeadService?
    fileprivate var data = [TablaLeadModel]()
    
    init(service: TablaLeadService){
        self.service = service
        SwiftEventBus.onMainThread(self, name: "Acciones_Lead") { [weak self] result in
            self?.view?.showEditar(index: result?.object as! IndexPath)
        }
    }
    
    func attachView(view: TablaLeadDelegate){
        self.view = view
        SwiftEventBus.onMainThread(self, name: "Reload-table") { [weak self] result in
            self?.actualizarDatos()
        }
    }
    
    func viewDidAppear(){
//        view?.updateTitleHeader()
    }
    
    /*protocol*/
    func viewWillAppear() {
        
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
        actualizarDatos()
    }
    
    func detachView() {
        view = nil
    }
    
    func getItemIndex(index: Int) -> TablaLeadModel {
        return data[index]
    }
    
    func getSize() -> Int{
        return data.count
    }
    
    func clickConvierte(index: Int){
        SwiftEventBus.post("Flujo-Convierte-Lead", sender: data[index])
        self.view?.cambiaVista(index: 4)
        SwiftEventBus.post("convertirLead(setNombreCuenta)", sender: data[index].empresa)
    }
    
    func clickEditaLead(index : Int){
        SwiftEventBus.post("Flujo-Edita-Lead", sender: data[index])
        self.view?.cambiaVista(index: 3)
    }
    
    func clickNuevo(){
        SwiftEventBus.post("Flujo-Crea-Lead", sender: nil)
        self.view?.cambiaVista(index: 3)
    }
    
    func eliminarLead(index : IndexPath){
        self.view?.startLoading()
        let id: String = data[index.row].idLead
        
//        let model = SFCrearProspectoRequest(tipoPersona: "", company: "", lastName: "", middleName: "", firstName: "", idEmpleado: "", telefono: "", correo: "", accion: "Eliminar", idProspecto: id, razonSocial: "")
//        let url = Constantes.Url.SalesFor.crearProspecto
//        let tipo: TipoRequest = .sf
//        let decodeClass = SFCrearProspectoResponse.self
        
        let model = MiddleCrearProspectoRequest(tipoPersona: "", company: "", lastName: "", middleName: "", firstName: "", idEmpleado: "", telefono: "", correo: "", accion: "Eliminar", idProspecto: id, razonSocial: "")
        let url = Constantes.Url.Middleware.Qa.crearProspecto
        let tipo: TipoRequest = .middle
        let decodeClass = MiddleCrearProspectoResponse.self
        
        service!.getData(model: model, response: decodeClass, url: url, tipo) { [weak self] data in
            if let data = data {
                self?.view?.finishLoading()
//                if data.result == "0" {
                if data.response?.result == "0" {
                    self?.view?.showBannerOK(title: "Oportunidad eliminada.")
                    self?.actualizarDatos()
                } else {
                    self?.view?.showBannerError(title: "No se ha podido eliminar la oportunidad.")
                }
            } else {
                self?.view?.showBannerError(title: "No se ha podido eliminar la oportunidad.")
            }
        }
    }
    
    func actualizarDatos() {
        data = []
        self.view?.startLoading()
        
        let id = service?.getIdEmpleado() ?? ""
        
//        let model = SFTablaLeadRequest(idEmpleado: id)
//        let url = Constantes.Url.SalesFor.misLeads
//        let tipo: TipoRequest = .sf
//        let decodeClass = SFTablaLeadResponse.self
        
        let model = MiddleTablaLeadRequest(idEmpleado: id)
        let url = Constantes.Url.Middleware.Qa.misLeads
        let tipo: TipoRequest = .middle
        let decodeClass = MiddleTablaLeadResponse.self
        
        service!.getData(model: model, response: decodeClass, url: url, tipo) { [weak self] data in
            self?.onFinishGetData(data: data)
        }
    }
    
    func onFinishGetData( data: MiddleTablaLeadResponse?) {
        view?.finishLoading()
        guard let data = data else {
            view?.showBannerError(title: "La lista de prospectos esta vacia.")
            Logger.e("no hay data en lista de Leads...")
            self.data = [TablaLeadModel]()
            self.view?.reloadTable()
            return
        }
        let lista: [ListaLead] = data.listaLead!
        for Lead in lista {
            self.data.append(TablaLeadModel(
                tipoPersona: Lead.tipoPersona ?? "",
                empresa: Lead.company ?? "",
                nombre: Lead.nombre ?? "",
                idLead: Lead.id ?? "",
                apellidoPaterno: Lead.primerApellido ?? "",
                apellidoMaterno: Lead.segundoApellido ?? "",
                telefono: Lead.telefono ?? "",
                correo: Lead.correo ?? "",
                etapaOportunidad: Lead.estatus ?? ""))
        }
        view?.reloadTable()
        
    }
}

