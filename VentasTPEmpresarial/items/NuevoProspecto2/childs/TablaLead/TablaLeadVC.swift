//
//  TablaLeadVC.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 07/12/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class TablaLeadVC: BaseItemVC {
    
    @IBOutlet weak var LeadTableView: UITableView!
    @IBOutlet weak var ViewContainerTable: UIView!
    var nombreCelda2: String = "TablaLeadCell"
    var presenter = TablaLeadPresenter(service : TablaLeadService())

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUpView()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow(){
        super.viewDidShow()
        presenter.viewDidShow()
    }
    
    func setUpView(){
        let nib = UINib.init(nibName: nombreCelda2, bundle: nil)
        self.LeadTableView.register(nib, forCellReuseIdentifier: nombreCelda2)
        self.LeadTableView.delegate = self
        self.LeadTableView.dataSource = self
        super.addShadowView(view: ViewContainerTable)
    }
    
    @IBAction func newLeadAtion(_ sender: Any) {
        presenter.clickNuevo()
    }
    
}

extension TablaLeadVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return TablaLeadHeader().view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension TablaLeadVC: UITableViewDataSource, BaseFormatNumber {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getSize()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: nombreCelda2, for: indexPath) as! TablaLeadCell
        let data = presenter.getItemIndex(index: indexPath.row)
        cell.NameCompany.text = data.empresa
        cell.index = indexPath
        cell.selectionStyle = .none //desactivar el color al seleccionar la celda
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        var actions = [UITableViewRowAction]()
        let titulo = "🗑️ \n eliminar"
        let actionEliminar = UITableViewRowAction(style: .destructive, title: titulo) {
            _, indexPath in
            self.borrar(indexPath: indexPath)
        }
        actionEliminar.backgroundColor = UIColor(red: 200, green: 0, blue: 0)
        actions.append(actionEliminar)
        return actions
    }
    
    func borrar(indexPath: IndexPath) {
        let alert = UIAlertController(
            title: "Borrar",
            message: "¿Seguro que quieres eliminar el sitio?",
            preferredStyle: .alert
        )
        let deleteAction = UIAlertAction(title: "SI", style: .default){
            [weak self] _ in
            self?.eliminarSitio(indexPath: indexPath)
        }
        let cancelAction = UIAlertAction(title: "NO", style: .default, handler: nil)
        alert.addAction(deleteAction)
        alert.addAction(cancelAction)
        present(alert, animated: true)
    }
    
    func eliminarSitio(indexPath: IndexPath){
        presenter.eliminarLead(index: indexPath)
    }
    
}

extension TablaLeadVC: TablaLeadDelegate {
    
    
    func cambiaVista(index: Int) {
        padreController?.cambiarVista(index: index)
    }
    
    func showEditar(index: IndexPath) {
        let alert = UIAlertController(
            title: "Editar",
            message: "¿Que acción desea realizar?",
            preferredStyle: .alert
        )
        var titulo = "Convertir a oportunidad"
        let action1 = UIAlertAction(title: titulo, style: .default){ [weak self] _ in
            self?.presenter.clickConvierte(index : index.row)
        }
        titulo = "Editar Lead"
        let action2 = UIAlertAction(title: titulo, style: .default){ [weak self] _ in
            self?.presenter.clickEditaLead(index : index.row)
        }
        titulo = "Cancelar"
        let cancel = UIAlertAction(title: titulo, style: .destructive, handler: nil)
        alert.view.tintColor = UIColor.blue
        alert.view.layer.cornerRadius = 25
        
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    func reloadTable() {
        LeadTableView.reloadData()
    }
    
    func updateTitleHeader() {
        let header = HeaderData(
            imagen: #imageLiteral(resourceName: "logo_GenerarLead"),
            titulo: "Cuentas",
            titulo2: nil,
            titulo3: nil,
            subtitulo: "Prospectos",
            closure: nil
        )
        super.updateTitle(headerData: header)
    }
    
    func startLoading() {
        super.showLoading(view: LeadTableView)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
}
