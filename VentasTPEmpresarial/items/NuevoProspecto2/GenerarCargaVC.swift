//
//  GenerateLoadViewController.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 01/06/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class GenerarCargaVC: BaseMenuItemVC, TimeMeasureDelegate {
    @IBOutlet weak var viewContainer2: UIView!
    @IBOutlet weak var segmentControl: CustomUISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tiempoInicio = iniciarTiempo()
        
        super.subscribe(nameItem: Constantes.Eventbus.Id.itemGeneraCarga)
        super.headerData = HeaderData(
            imagen: #imageLiteral(resourceName: "logo_GenerarLead"),
            titulo: "Cuentas",
            titulo2: nil,
            titulo3: nil,
            subtitulo: nil,
            closure: nil
        )
        addItems()
        
        medirTiempo(tipoInicio: tiempoInicio, nombre: "Nuevo Prospecto")
    }
    
    func addItems() {
        var viewControllers2 = [BaseItemVC]()
        viewControllers2.append(TablaLeadVC())
        viewControllers2.append(CuentasVC())
        viewControllers2.append(VisualizaVentasVC())
        viewControllers2.append(CrearProspectoVC())
        viewControllers2.append(ConvertirLeadVC())
        super.addViewXIBList(view: viewContainer2, viewControllers: viewControllers2)
    }
    
    @IBAction func selectedView(_ sender: CustomUISegmentedControl) {
        super.cambiarVista(index: sender.selectedSegmentIndex)
        sender.changeUnderlinePosition(sender.currentIndex)
    }
    
    override func viewDidShow() {
        super.viewDidShow()
        segmentControl.selectedSegmentIndex = super.posicionIndex;
        segmentControl.changeUnderlinePosition(super.posicionIndex)
        
    }
}
