//
//  TipsSalesViewController.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 01/06/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class TipsVentasVC: BaseMenuItemVC, TimeMeasureDelegate {
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var segmentControl: CustomUISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tiempoInicio = iniciarTiempo()
        
        super.subscribe(nameItem: Constantes.Eventbus.Id.menuItemTipsVentas)
        super.headerData = HeaderData(
            imagen: #imageLiteral(resourceName: "logo_MisVentas"),
            titulo: "Tips de Venta",
            titulo2: nil,
            titulo3: nil,
            subtitulo: nil,
            closure: nil
        )
        addSubView()
        
        medirTiempo(tipoInicio: tiempoInicio, nombre: "Tips de venta")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        segmentControl.changeUnderlinePosition(0)
    }
    
    func addSubView() -> Void {
        var viewControllers2 = [BaseItemVC]()
        viewControllers2.append(TipsVC())
        viewControllers2.append(VideosVC())
        super.addViewXIBList(view: viewContent, viewControllers: viewControllers2)
    }
    
    @IBAction func selectedView(_ sender: CustomUISegmentedControl) {
        sender.changeUnderlinePosition(sender.currentIndex)
        super.cambiarVista(index: sender.selectedSegmentIndex)
    }
    
    override func viewDidShow() {
        super.viewDidShow()
    }
}
