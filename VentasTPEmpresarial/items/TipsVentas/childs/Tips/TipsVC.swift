//
//  TipsVC.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 06/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class TipsVC: BaseItemVC  {
    @IBOutlet weak var viewContainer3: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let controllers = [
            FactoresClaveVC(),
            EnlacesDedicadosVC(),
            InternetVC(),
            InternetCVozVC(),
            SeguridadVC(),
            ServiciosNubeVC(),
            SolucionSectorVC()
        ]
        super.addViewXIBList(view: viewContainer3, viewControllers: controllers)
    }

    override func viewDidShow(){
        super.viewDidShow()
        let headerData = HeaderData(
            imagen: #imageLiteral(resourceName: "icon_Comisiones"),
            titulo: "Tips de Venta",
            titulo2: nil,
            titulo3: nil,
            subtitulo: nil,
            closure: nil
        )
        super.updateTitle(headerData: headerData)
    }
}
