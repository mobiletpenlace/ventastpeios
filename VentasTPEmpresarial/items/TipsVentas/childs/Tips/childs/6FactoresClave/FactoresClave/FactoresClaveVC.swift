//
//  FactoresClaveVC.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 15/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class FactoresClaveVC: BaseItemVC {
    
    @IBOutlet weak var viewEnlacesDedicados: UIView!
    @IBOutlet weak var viewED: UIView!
    @IBOutlet weak var viewInternet: UIView!
    @IBOutlet weak var viewIter: UIView!
    @IBOutlet weak var viewInternetCVoz: UIView!
    @IBOutlet weak var viewICV: UIView!
    
    @IBOutlet weak var viewSeguridad: UIView!
    @IBOutlet weak var viewSeg: UIView!
    @IBOutlet weak var viewServiciosNube: UIView!
    @IBOutlet weak var viewSN: UIView!
    @IBOutlet weak var viewSolucionSector: UIView!
    @IBOutlet weak var viewSS: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        
    }
    
    func setUpView(){
        super.addShadowView(view: viewED)
        super.addShadowView(view: viewIter)
        super.addShadowView(view: viewICV)
        super.addShadowView(view: viewSeg)
        super.addShadowView(view: viewSN)
        super.addShadowView(view: viewSS)
    }
    
    
    @IBAction func clickEnlaceDedicado(_ sender: UIButton){
        sender.animateBound(view:viewEnlacesDedicados){ [unowned self] _ in
            self.padreController?.cambiarVista(index: 1)
        }
    }
    
    @IBAction func clickInternet(_ sender: UIButton) {
        sender.animateBound(view:viewInternet)
        {[unowned self] _ in
            self.padreController?.cambiarVista(index: 2)
        }
    }
    
    @IBAction func clickInternetCVoz(_ sender: UIButton) {
        sender.animateBound(view:viewInternetCVoz)
        {[unowned self] _ in
            self.padreController?.cambiarVista(index: 3)
        }
    }
    
    @IBAction func clickSeguridad(_ sender: UIButton) {
        sender.animateBound(view:viewSeguridad)
        {[unowned self] _ in
            self.padreController?.cambiarVista(index: 4)
            
        }
    }
    
    @IBAction func clickServicioNube(_ sender: UIButton) {
        sender.animateBound(view:viewServiciosNube)
        {[unowned self] _ in
            self.padreController?.cambiarVista(index: 5)
            
        }
    }
    
    @IBAction func clickSolucionSector(_ sender: UIButton) {
        sender.animateBound(view:viewSolucionSector)
        {[unowned self] _ in
            self.padreController?.cambiarVista(index: 6)
            
        }
    }
    
}
