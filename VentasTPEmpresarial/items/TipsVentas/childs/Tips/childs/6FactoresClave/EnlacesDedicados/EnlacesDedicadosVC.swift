//
//  EnlacesDedicadosVC.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 16/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class EnlacesDedicadosVC: BaseItemVC {
    
    @IBOutlet weak var viewFlexnet: UIView!
    @IBOutlet weak var viewFN: UIView!
    @IBOutlet weak var viewLanToLan: UIView!
    @IBOutlet weak var viewLTL: UIView!
    @IBOutlet weak var viewMPLS: UIView!
    @IBOutlet weak var viewMPLS2: UIView!
    @IBOutlet weak var viewBack: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        
    }
    
    func setUpView(){
        super.addShadowView(view: viewFN)
        super.addShadowView(view: viewLTL)
        super.addShadowView(view: viewMPLS2)
    }
    
    
    @IBAction func clickFlexnet(_ sender: UIButton) {
        sender.animateBound(view: viewFlexnet)
        let urlPDF = "https://appstotalplay.com/media/files/Diptico_FlexNet_Movil.pdf"
        super.showDialog(customAlert: DialogoPDF(urlPDF: urlPDF, titulo: "Flexnet."))
    }
    
    @IBAction func clickLanToLan(_ sender: UIButton) {
        sender.animateBound(view: viewLanToLan)
        let urlPDF = "https://appstotalplay.com/media/files/Diptico_Redes_Movil.pdf"
        super.showDialog(customAlert: DialogoPDF(urlPDF: urlPDF, titulo: "Lan to lan."))
    }
    
    @IBAction func clickMPLS(_ sender: UIButton) {
        sender.animateBound(view: viewMPLS)
        let urlPDF = "https://appstotalplay.com/media/files/Diptico_MPLS.pdf"
        super.showDialog(customAlert: DialogoPDF(urlPDF: urlPDF, titulo: "MPLS."))
    }
    
    
    
    @IBAction func clickBackButton(_ sender: UIButton) {
        sender.animateBound(view: viewBack)
        {[unowned self] _ in
            self.padreController?.cambiarVista(index: 0)
            
        }
    }
    
    
}
