//
//  InternetCVozVC.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 16/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class InternetCVozVC: BaseItemVC {

    @IBOutlet weak var viewAnalogicos: UIView!
    @IBOutlet weak var viewAL1: UIView!
    @IBOutlet weak var viewVozMovil: UIView!
    @IBOutlet weak var viewVM2: UIView!
    @IBOutlet weak var viewVozSip: UIView!
    @IBOutlet weak var viewVS3: UIView!
    @IBOutlet weak var viewBack: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        }

   
    func setUpView(){
        super.addShadowView(view: viewAL1)
        super.addShadowView(view: viewVM2)
        super.addShadowView(view: viewVS3)
    }
    
    @IBAction func clickAnalogico(_ sender: UIButton) {
        sender.animateBound(view: viewAnalogicos)
        let urlPDF = "https://appstotalplay.com/media/files/Diptico__Voz_Movil.pdf"
        super.showDialog(customAlert: DialogoPDF(urlPDF: urlPDF, titulo: "Analógicos."))
    }
    
    @IBAction func clickVozMovil(_ sender: UIButton) {
        sender.animateBound(view: viewVozMovil)
        let urlPDF = "https://appstotalplay.com/media/files/Diptico__Voz_Movil_x2mUEDQ.pdf"
        super.showDialog(customAlert: DialogoPDF(urlPDF: urlPDF, titulo: "Voz/Móvil."))
    }
    
    @IBAction func clickVozSip(_ sender: UIButton) {
        sender.animateBound(view: viewVozSip)
        let urlPDF = "https://appstotalplay.com/media/files/Diptico__Voz_Movil_1.pdf"
        super.showDialog(customAlert: DialogoPDF(urlPDF: urlPDF, titulo: "Voz/SIP."))
    }
    
    @IBAction func clickButttonBack(_ sender: UIButton) {
        sender.animateBound(view: viewBack)
        {[unowned self] _ in
            self.padreController?.cambiarVista(index: 0)
            
        }
    }
    
}
