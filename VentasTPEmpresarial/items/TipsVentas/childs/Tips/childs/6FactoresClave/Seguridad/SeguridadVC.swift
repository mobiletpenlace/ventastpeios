//
//  SeguridadVC.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 16/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class SeguridadVC: BaseItemVC {

    @IBOutlet weak var viewSeguridadPerimetral: UIView!
    @IBOutlet weak var viewSP1: UIView!
    @IBOutlet weak var viewBack: UIView!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        
    }
    
    func setUpView(){
        super.addShadowView(view: viewSP1)
    }
    
    
    @IBAction func clickSeguridadPerimetral(_ sender: UIButton) {
        sender.animateBound(view: viewSeguridadPerimetral)
        let urlPDF = "https://appstotalplay.com/media/files/COMERCIAL_Seguridad_Perimetral_V3.0_Julio18.pdf"
        super.showDialog(customAlert: DialogoPDF(urlPDF: urlPDF, titulo: "Seguridad Perimetral."))
    }
    
    
    @IBAction func clickBackButton(_ sender: UIButton) {
        sender.animateBound(view: viewBack)
        {[unowned self] _ in
            self.padreController?.cambiarVista(index: 0)
            
        }
    }
    

    

}
