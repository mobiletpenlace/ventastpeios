//
//  InternetVC.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 16/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class InternetVC: BaseItemVC {

    @IBOutlet weak var viewAsimetrico: UIView!
    @IBOutlet weak var viewA1: UIView!
    @IBOutlet weak var viewDedicado: UIView!
    @IBOutlet weak var viewD2: UIView!
    @IBOutlet weak var viewSeguridadPerimetral: UIView!
    @IBOutlet weak var viewSP3: UIView!
    @IBOutlet weak var viewSimetrico: UIView!
    @IBOutlet weak var viewS4: UIView!
    @IBOutlet weak var viewBack: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        }

    func setUpView(){
        super.addShadowView(view: viewA1)
        super.addShadowView(view: viewD2)
        super.addShadowView(view: viewSP3)
        super.addShadowView(view: viewS4)
    }
    
    @IBAction func clickAsimetrico(_ sender: UIButton) {
        sender.animateBound(view: viewAsimetrico)
        let urlPDF = "https://appstotalplay.com/media/files/Diptico__Internet_Movil.pdf"
        super.showDialog(customAlert: DialogoPDF(urlPDF: urlPDF, titulo: "Asimétrico."))
    }
    
    @IBAction func clickDedicados(_ sender: UIButton) {
        sender.animateBound(view: viewDedicado)
        let urlPDF = "https://appstotalplay.com/media/files/Diptico_Redes_Movil_tPihY7s.pdf"
        super.showDialog(customAlert: DialogoPDF(urlPDF: urlPDF, titulo: "Dedicado."))
    }
    
    @IBAction func clickSeguridadPerimetral(_ sender: UIButton){
        sender.animateBound(view: viewSeguridadPerimetral)
        let urlPDF = "https://appstotalplay.com/media/files/Ficha_SeguridadPerimetral.pdf"
        super.showDialog(customAlert: DialogoPDF(urlPDF: urlPDF, titulo: "Seguridad Perimetral."))
    }
    
    @IBAction func clickSimetrico(_ sender: UIButton) {
        sender.animateBound(view: viewSimetrico)
        let urlPDF = "https://appstotalplay.com/media/files/Diptico__Internet_Movil_1_e3bZfAF.pdf"
        super.showDialog(customAlert: DialogoPDF(urlPDF: urlPDF, titulo: "Simétrico."))
    }
    
    @IBAction func clickBackButton(_ sender: UIButton) {
   sender.animateBound(view: viewBack)
   {[unowned self] _ in
    self.padreController?.cambiarVista(index: 0)
    
        }
    }
    
    
}
