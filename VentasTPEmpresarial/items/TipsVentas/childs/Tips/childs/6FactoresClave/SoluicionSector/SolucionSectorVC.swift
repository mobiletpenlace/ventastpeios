//
//  SolucionSectorVC.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 16/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class SolucionSectorVC: BaseItemVC {

    
    @IBOutlet weak var viewHotelera: UIView!
    @IBOutlet weak var viewH1: UIView!
    @IBOutlet weak var viewBack: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()

    }
    
    func setUpView(){
        super.addShadowView(view: viewH1)
        
        }

    
    @IBAction func clickHotelera(_ sender: UIButton) {
        sender.animateBound(view: viewHotelera)
        let urlPDF = "https://appstotalplay.com/media/files/COMERCIAL_Solucion_Hotel_Totalplay_ENCODER_ESPANOL_V2.1_Septiembre18.pdf"
        super.showDialog(customAlert: DialogoPDF(urlPDF: urlPDF, titulo: "Hotelera."))
    }
    
    @IBAction func clickBackButton(_ sender: UIButton) {
        sender.animateBound(view: viewBack)
        {[unowned self] _ in
            self.padreController?.cambiarVista(index: 0)
        }
            
    }
    
    
    
}
