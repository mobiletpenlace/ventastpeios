//
//  ServiciosNubeVC.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 16/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class ServiciosNubeVC: BaseItemVC {

    @IBOutlet weak var viewFlotillas: UIView!
    @IBOutlet weak var viewF1: UIView!
    @IBOutlet weak var viewGSuite: UIView!
    @IBOutlet weak var viewGS2: UIView!
    @IBOutlet weak var viewServidoresVirtuales: UIView!
    @IBOutlet weak var viewSV3: UIView!
    @IBOutlet weak var viewVideoVigilancia: UIView!
    @IBOutlet weak var viewVV4: UIView!
    @IBOutlet weak var viewBack: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        
    }
    
    func setUpView(){
        super.addShadowView(view: viewF1)
        super.addShadowView(view: viewGS2)
        super.addShadowView(view: viewSV3)
        super.addShadowView(view: viewVV4)
    }

    @IBAction func clickFlotillas(_ sender: UIButton) {
        sender.animateBound(view: viewFlotillas)
        let urlPDF = "https://appstotalplay.com/media/files/Diptico-flotilla_040418_Movil.pdf"
        super.showDialog(customAlert: DialogoPDF(urlPDF: urlPDF, titulo: "Flotillas."))
    }
    
    @IBAction func clickGSuite(_ sender: UIButton) {
        sender.animateBound(view: viewGSuite)
        let urlPDF = "https://appstotalplay.com/media/files/Ficha_GSuite.pdf"
        super.showDialog(customAlert: DialogoPDF(urlPDF: urlPDF, titulo: "G-Suite."))
    }
    
    @IBAction func clickServidoresVirtuales(_ sender: UIButton) {
        sender.animateBound(view: viewServidoresVirtuales)
        let urlPDF = "https://appstotalplay.com/media/files/Ficha_ServidoresVirtuales.pdf"
        super.showDialog(customAlert: DialogoPDF(urlPDF: urlPDF, titulo: "Servidores Virtuales."))
    }
    
    @IBAction func clickVideoVigilancia(_ sender: UIButton) {
        sender.animateBound(view: viewVideoVigilancia)
        let urlPDF = "https://appstotalplay.com/media/files/Diptico_Videovigilancia230318_Movil.pdf"
        super.showDialog(customAlert: DialogoPDF(urlPDF: urlPDF, titulo: "Video Vigilancia."))
    }
    
    @IBAction func clickBackButton(_ sender: UIButton) {
        sender.animateBound(view: viewBack)
        {[unowned self] _ in
            self.padreController?.cambiarVista(index: 0)
            }
    }
    
    
}
