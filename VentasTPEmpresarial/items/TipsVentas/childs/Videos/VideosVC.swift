//
//  VideosVC.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 06/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class VideosVC: BaseItemVC {
    @IBOutlet weak var viewContainer4: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let controllers: [BaseTreeVC] = [ VideosEmpresarialVC(), ]
        super.addViewXIBList(view: viewContainer4, viewControllers: controllers)
    }

    override func viewDidShow(){
        super.viewDidShow()
        let headerData = HeaderData(
            imagen: #imageLiteral(resourceName: "icon_Comisiones"),
            titulo: "Tips de Venta",
            titulo2: nil,
            titulo3: nil,
            subtitulo: nil,
            closure: nil
        )
        super.updateTitle(headerData: headerData)
    }
}
