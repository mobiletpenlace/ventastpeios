//
//  VideosEmpresarialVC.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 10/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import SnapKit

class VideosEmpresarialVC: BaseItemVC  {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var containerPlayVideo: UIView!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var containerListVideos: UIView!
    @IBOutlet weak var controlesView: UIView!
    @IBOutlet weak var controlesTimeStackView: UIStackView!
    
    @IBOutlet weak var controlReproduccionView: UIView!
    @IBOutlet weak var labelTiempoInicio: UILabel!
    @IBOutlet weak var labelTiempoFin: UILabel!
    @IBOutlet weak var sliderTiempo: UISlider!
    
    @IBOutlet weak var imageViewVolumen: UIImageView!
    @IBOutlet weak var labelvolumen: UILabel!
    @IBOutlet weak var sliderVolumen: UISlider!
    @IBOutlet weak var buttonPlay: UIButton!
    var playerLayer: AVPlayerLayer? = nil
    
    var nombreCelda1: String = "VideosVCCell"
    fileprivate let presenter = VideosEmpresarialPresenter(service: VideosEmpresarialService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow(){
        super.viewDidShow()
        presenter.viewDidShow()
    }
    
    private func setUpView() {
        cargarTableView()
        
        sliderTiempo.minimumValue = 0
        sliderTiempo.isContinuous = true
        
        sliderVolumen.isContinuous = true
        sliderVolumen.maximumValue = 1.0
        sliderVolumen.minimumValue = 0.0
        
        controlReproduccionView.layer.cornerRadius = 8
    }
    
    func cargarTableView() -> Void {
        let nib = UINib.init(nibName: nombreCelda1, bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: nombreCelda1)
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    @IBAction func clickPlayPauseButton(_ sender: UIButton) {
        sender.animateBound(view: sender)
        presenter.clickPlayButton()
    }
    
    @IBAction func cambioSliderTiempo(_ sender: UISlider) {
        presenter.cambioSliderTiempo(value: sender.value)
    }
    
    
    @IBAction func cambioSliderVolumen(_ sender: UISlider) {
        presenter.cambioSliderVolumen(value: sender.value)
    }
    
    @IBAction func clickHideButtonControls(_ sender: UIButton) {
        presenter.clickHideButtonControls()
    }
    
    override func willHideView(){
        presenter.willHideView()
    }
}

extension VideosEmpresarialVC: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: nombreCelda1, for: indexPath)
        if let cell = cell as? VideosVCCell {
            let data =  presenter.getItemIndex(index: indexPath.row)
            cell.labelVideo.text = data.labelVideo
            cell.labelTiempo.text = data.labelTiempo
            cell.imgVideo.image = data.imgVideo
        }
        cambiarColorCelda(cell: cell)
        return cell
    }
    
    func cambiarColorCelda(cell: UITableViewCell){
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.white.withAlphaComponent(0.8)
        cell.selectedBackgroundView = backgroundView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getSize()
    }
}


extension VideosEmpresarialVC: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? VideosVCCell{
            cell.animateBound(view: cell){[weak self] _ in
                self?.presenter.clickVideoItem(index: indexPath.row)
            }
        }
    }
}

extension VideosEmpresarialVC: VideosEmpresarialDelegate{
    
    func reloadTable(){
        tableView.reloadData()
        let indexPath = IndexPath(row: 0, section: 0)
        seleccionarCeldaTabla(indexPath: indexPath)
    }
    
    func seleccionarCeldaTabla(indexPath: IndexPath){
        let n = tableView.numberOfRows(inSection: 0)
        if n > 0{
            tableView.selectRow(at: indexPath, animated: true, scrollPosition: .bottom)
        }
    }
    
    func reloadRows(indexPath: [IndexPath]){
        tableView.reloadRows(at: indexPath, with: .left)
    }
    
    func updateTitleHeader() {
        
    }
    
    func startLoading() {
        super.showLoading(view: tableView)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
    func createVideoLayer(playerLayer: AVPlayerLayer){
        playerLayer.frame = videoView.bounds
        videoView.layer.addSublayer(playerLayer)
        self.playerLayer = playerLayer
    }
    
    func mostrarPausarButton(){
        ButtonUtils.setIcon(buttonPlay, icon: #imageLiteral(resourceName: "img_play"))
//        buttonPlay.setImage(#imageLiteral(resourceName: "img_play"), for: .normal)
    }
    
    func mostrarPlayButton(){
        ButtonUtils.setIcon(buttonPlay, icon: #imageLiteral(resourceName: "img_pause"))
//        buttonPlay.setImage(#imageLiteral(resourceName: "img_pause"), for: .normal)
    }
    
    func actualizarVolumenControl(value: Float){
        sliderVolumen.value = value
    }
    
    func showOrHidecontrols(isHidden: Bool){
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.controlesTimeStackView.isHidden = isHidden
            self.controlReproduccionView.isHidden = isHidden
        })
    }
    
    func updateSliderValues(tIni: String, tFin: String, seconds: Float){
        sliderTiempo.maximumValue = seconds
        labelTiempoInicio.text = tIni
        labelTiempoFin.text = tFin
    }
    
    func updateSliderTime(tIni: String, seconds: Float){
        sliderTiempo.value = seconds
        labelTiempoInicio.text = tIni
//        if abs(sliderTiempo.value - seconds) < 4{
//            sliderTiempo.value = seconds
//            labelTiempoInicio.text = tIni
//        }
    }
    
    func addUpdatertime(player: AVPlayer) -> Any{
        let interval: CMTime = CMTimeMakeWithSeconds(1.0, CMTimeScale(NSEC_PER_SEC)) // 1 second
        let mainQueue = DispatchQueue.main
        let playbackTimeObserver = player.addPeriodicTimeObserver(forInterval: interval, queue: mainQueue){ [weak self] tiempo in
            self?.presenter.shouldUpdateTime(tiempo)
        }
        return playbackTimeObserver
    }
    
    func addListenerWhenEndVideo(item: AVPlayerItem){
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying(sender:)) , name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: item)
    }
        
    @objc func playerDidFinishPlaying(sender: Notification) {
        presenter.playerDidFinishPlaying()
    }
}

