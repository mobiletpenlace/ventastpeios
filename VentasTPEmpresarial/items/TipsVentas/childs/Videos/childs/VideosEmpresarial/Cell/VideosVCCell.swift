//
//  VideosVCCell.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 11/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class VideosVCCell: UITableViewCell {
    @IBOutlet weak var imgVideo: UIImageView!
    @IBOutlet weak var labelVideo: UILabel!
    @IBOutlet weak var labelTiempo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
