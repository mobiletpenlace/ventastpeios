//
//  VideosEmpresarialPresenter.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 11/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import AVFoundation

protocol VideosEmpresarialDelegate: BaseDelegate {
    func reloadTable()
    func reloadRows(indexPath: [IndexPath])
    
    func createVideoLayer(playerLayer: AVPlayerLayer)
    func updateSliderValues(tIni: String, tFin: String, seconds: Float)
    func updateSliderTime(tIni: String, seconds: Float)
    func addUpdatertime(player: AVPlayer) -> Any
    func addListenerWhenEndVideo(item: AVPlayerItem)
    
    func mostrarPausarButton()
    func mostrarPlayButton()
    func actualizarVolumenControl(value: Float)
    func showOrHidecontrols(isHidden: Bool)
}

class VideosEmpresarialPresenter {
    weak fileprivate var view: VideosEmpresarialDelegate?
    fileprivate let service: VideosEmpresarialService
    fileprivate var dataList = [VideosEmpresarialModel]()
    var showControls = true
    var player: AVPlayer? = nil
    var playbackTimeObserver: Any? = nil
    
    init(service: VideosEmpresarialService){
        self.service = service
    }
    
    func attachView(view:VideosEmpresarialDelegate){
        self.view = view
    }
    
    func viewDidAppear(){
    }
    
    func viewDidShow(){
        actualizarDatos()
    }
    
    func datachView(){
        view = nil
        if let observer = playbackTimeObserver{
            player?.removeTimeObserver(observer)
        }
    }
    
    func willHideView(){
        pauseIfNeed()
    }
    
    func getItemIndex(index: Int) -> VideosEmpresarialModel {
        return dataList[index]
    }
    
    func getSize() -> Int{
        return dataList.count
    }
    
    private func play(){
        if let vista = view{
            vista.mostrarPlayButton()
        }
        if player != nil{
            player!.play()
        }
    }
    
    private func pause(){
        if let vista = view{
            vista.mostrarPausarButton()
        }
        if player != nil{
            player!.pause()
        }
    }
    
    private func isPause() -> Bool{
        if player != nil{
            return player?.rate == 0
        }
        return true
    }
    
    private func playIfNeed(){
        if isPause()
        {
            play()
        }
    }
    
    private func pauseIfNeed(){
        if !isPause()
        {
            pause()
        }
    }
    
    func clickPlayButton(){
        if isPause()
        {
            play()
        }else{
            pause()
        }
    }
    
    func clickVideoItem(index: Int){
        let url = dataList[index].url
        
        let videoURL = URL(string: url)
        let playerItem = AVPlayerItem(url: videoURL!)
        
        if (player == nil){
            player = AVPlayer(playerItem: playerItem)
            view?.createVideoLayer(playerLayer: AVPlayerLayer(player: player))
        } else {
            pauseIfNeed()
            player!.replaceCurrentItem(with: playerItem)
        }
        updateSlider(playerItem: playerItem)
        playbackTimeObserver = view?.addUpdatertime(player: player!)
        view?.addListenerWhenEndVideo(item: playerItem)
        playIfNeed()
    }
    
    func updateSlider(playerItem: AVPlayerItem){
        let duration : CMTime = playerItem.asset.duration
        let seconds : Float64 = CMTimeGetSeconds(duration)
        let strFin = secondToLabelTime(seconds: Int(duration.seconds))
        let strIni = secondToLabelTime(seconds: Int(0))
        view?.updateSliderValues(tIni: strIni, tFin: strFin, seconds: Float(seconds))
    }
    
    func secondToLabelTime(seconds : Int) -> String{
        let (h,m,s) = (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
        return String(format:"%02i:%02i:%02i", h, m, s)
    }
    
    func cambioSliderTiempo(value: Float){
        if player != nil{
            player!.seek(to: CMTimeMake(Int64(value), 1))//avanzar video
        }
        playIfNeed()
    }
    
    func cambioSliderVolumen(value: Float){
        let step: Float = 0.1
        let roundedValue = round(value / step) * step
        if player != nil{
            player!.volume = value
        }
        view?.actualizarVolumenControl(value: roundedValue)
    }
    
    func clickHideButtonControls(){
        view?.showOrHidecontrols(isHidden: showControls)
        showControls = !showControls//invertir los valores
    }
    
    func shouldUpdateTime(_ tiempo: CMTime){
        let value = Float(tiempo.seconds)
        let tiempo = secondToLabelTime(seconds: Int(tiempo.seconds))
        view?.updateSliderTime(tIni: tiempo, seconds: value)
    }

    func playerDidFinishPlaying(){
        view?.mostrarPausarButton()
    }
    
    func actualizarDatos(){
        self.view?.startLoading()
        service.getData(){[weak self] data in
            self?.onFinishGetData(data: data)
        }
    }
    
    func onFinishGetData( data: [VideosEmpresarialModel]){
        view?.finishLoading()
        if (data.count == 0){
            dataList = [VideosEmpresarialModel]()
        }else{
            dataList = data
        }
        view?.reloadTable()
        actualizarTumbnailsAndtiempo()
    }
    
    func actualizarTumbnailsAndtiempo(){
        for (pos, data) in dataList.enumerated(){
            let url = data.url
            DispatchQueue.global().async { [weak self] in
                guard let tiempo = self?.getMediaDuration(url: URL(string: url)!) else{return}
                guard let tumbnail = self?.createThumbnailOfVideoFromRemoteUrl(url: url) else{return}
                self?.dataList[pos].imgVideo = tumbnail
                self?.dataList[pos].labelTiempo = tiempo
                DispatchQueue.main.async {
                    self?.view?.reloadRows(indexPath: [IndexPath(row: pos, section: 0)])
                }
            }
        }
    }
    
    func createThumbnailOfVideoFromRemoteUrl(url: String) -> UIImage? {
        let asset = AVAsset(url: URL(string: url)!)
        let assetImgGenerate = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        let time = CMTimeMakeWithSeconds(0, 2)
        do {
            let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: img)
            return thumbnail
        } catch {
            Logger.e("error: \(error)")
            return nil
        }
    }
    
    func getMediaDuration(url: URL) -> String{
        let asset : AVURLAsset = AVURLAsset(url: url)
        let duration : CMTime = asset.duration
        let label = secondToLabelTime(seconds: Int(CMTimeGetSeconds(duration)))
        return label
    }
}
