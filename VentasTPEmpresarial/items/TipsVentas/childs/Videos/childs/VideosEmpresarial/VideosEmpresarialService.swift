//
//  VideosEmpresarialService.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 11/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import AVFoundation

class VideosEmpresarialService {
    let serverManager: ServerDataManager
    
    init() {
        serverManager = ServerDataManager()
    }
    
    func getData(_ callBack:@escaping ([VideosEmpresarialModel]) -> Void){
        var videoList = [VideosEmpresarialModel]()
        let parameters = ["app_code": "ventasEmpresarial"]
        serverManager.get(url: Constantes.Url.AppsServer.videos, parameters: parameters){ responseData in
            guard let data = responseData.data else{
                Logger.e("no hay datos del servicio de agendas")
                callBack(videoList)
                return
            }
            do{
                let decoder = JSONDecoder()
                let responseOBJ = try decoder.decode(VideoList.self, from: data)
                if let videos = responseOBJ.videos{
                    for video in videos{
                        let url = Constantes.Url.AppsServer.server + (video.video ?? "")
                        let tiempo = "00:00"
                        let tumbnail = #imageLiteral(resourceName: "img_video_empty")
                        videoList.append(VideosEmpresarialModel(
                            imgVideo: tumbnail,
                            labelVideo: video.description!,
                            labelTiempo: tiempo,
                            url: url)
                        )
                    }
                }
                callBack(videoList)
            }catch let err{
                Logger.e("error: \(err)")
                callBack(videoList)
            }
        }
    }
}
