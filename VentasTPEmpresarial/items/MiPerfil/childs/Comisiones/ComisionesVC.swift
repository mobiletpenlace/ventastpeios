//
//  Item2ViewController.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 06/06/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class ComisionesVC: BaseItemVC {
    @IBOutlet weak var viewContainer2: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let controllers = [
            MenuComisionesVC(),
            ComisionesPagarVC(),
            ConsultaAclaracionVC(),//consulta y detalle de aclaraciones
            IngresoAclaracionVC(),//aclaraciones del detalle de la cuenta
        ]
        super.addViewXIBList(view: viewContainer2, viewControllers: controllers)
    }
    
    override func viewDidShow(){
        super.viewDidShow()
    }
}
