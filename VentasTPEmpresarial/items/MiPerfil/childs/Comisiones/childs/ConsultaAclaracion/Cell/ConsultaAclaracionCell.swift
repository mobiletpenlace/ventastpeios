//
//  AclaracionesTableViewCell.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 24/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class ConsultaAclaracionCell: UITableViewCell {
    @IBOutlet weak var labelTicket: UILabel!
    @IBOutlet weak var labelCuenta: UILabel!
    @IBOutlet weak var labelStatus: UILabel!
    @IBOutlet weak var checkBoxDetalleSeleccionado: CheckBoxButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        checkBoxDetalleSeleccionado.isChecked = selected
    }
    
    @IBAction func selectButtonDetail(_ sender: Any) {
    
    }
}
