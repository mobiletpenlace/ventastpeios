//
//  ConsultaAclaracionPresenter.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 13/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

protocol ConsultaAclaracionDelegate: BaseDelegate {
    func actualizar()
}

class ConsultaAclaracionPresenter {
    fileprivate let model: ConsultaAclaracionService
    weak fileprivate var view: ConsultaAclaracionDelegate?
    fileprivate var dataList = [ConsultaAclaracionModel]()
    
    init(model: ConsultaAclaracionService){
        self.model = model
    }
    
    func attachView(view: ConsultaAclaracionDelegate){
        self.view = view
    }
    
    func viewDidAppear(){
        
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
        getData()
    }
    
    func detachView() {
        view = nil
    }
    
    func getItemIndex(index: Int) -> ConsultaAclaracionModel {
        return dataList[index]
    }
    
    func getSize() -> Int{
        return dataList.count
    }
    
    func getData(){
        self.view?.startLoading()
        model.getConsultaAclaracion{[weak self] data in
            self?.view?.finishLoading()
            if(data.count == 0){
                self?.dataList = [ConsultaAclaracionModel]()
            }else{
                self?.dataList = data
            }
            self?.view?.actualizar()
        }
    }
}
