//
//  ClarificationOfCommissionsViewController.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 23/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class ConsultaAclaracionVC: BaseItemVC {    
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var viewConsultaAclaraciones: UIView!
    @IBOutlet weak var viewDetalleAclaración: UIView!
    fileprivate let presenter = ConsultaAclaracionPresenter(model: ConsultaAclaracionService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow() {
        super.viewDidShow()
        presenter.viewDidShow()
    }
    
    private func setUpView() {
        cargarTableView()
        super.addShadowView(view:viewDetalleAclaración)
        super.addViewXIB(vc:DetalleAclaracionesVC(), container: viewDetalleAclaración)
    }
    
    
    func cargarTableView() -> Void {
        let nib = UINib.init(nibName: "ConsultaAclaracionCell", bundle: nil)
        self.tableview.register(nib, forCellReuseIdentifier: "ConsultaAclaracionCell")
        self.tableview.delegate = self
        self.tableview.dataSource = self
    }
}

extension ConsultaAclaracionVC:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConsultaAclaracionCell", for: indexPath) as! ConsultaAclaracionCell
        let data =  presenter.getItemIndex(index: indexPath.row)
        cell.labelTicket.text = data.ticket
        cell.labelCuenta.text = data.cuenta
        cell.labelStatus.text = data.status
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getSize()
    }
}

extension ConsultaAclaracionVC:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return ConsultaAclaracionCellHeader().view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //let currentCell = tableView.cellForRow(at: indexPath)! as! AclaracionesTableViewCell
    }
    
}

extension ConsultaAclaracionVC: ConsultaAclaracionDelegate{
    func updateTitleHeader() {
        let header = HeaderData(
            imagen: #imageLiteral(resourceName: "icon_Comisiones"),
            titulo: "Comisiones",
            titulo2: nil,
            titulo3: nil,
            subtitulo: nil,
            closure:{ [weak self] in
                self?.padreController?.cambiarVista(index: 0)
            }
        )
        super.updateTitle(headerData: header)
    }
    
    func startLoading() {
        super.showLoading(view: self.view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
    func actualizar(){
        self.tableview.reloadData()
        let indexPath = IndexPath(row: 0, section: 0)
        tableview.selectRow(at: indexPath, animated: true, scrollPosition: .bottom)
    }
}
