//
//  ConsultaAclaracionModel.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 13/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct ConsultaAclaracionModel {
    let ticket: String
    let cuenta: String
    let status: String
}
