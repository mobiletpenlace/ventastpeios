//
//  DetalleDeAclaracionModel.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 16/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct DetalleDeAclaracionModel {
    let ticket: String
    let tipoAclaracion: String
    let comision: String
    let cuenta: String
    let paquete: String
    let addon: String
    let fechaSolicitud: String
    let fechaPagoFactura: String
    let fechaInstalacion: String
    let comentarios: String
}
