//
//  DetalleAclaracionesViewController.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 25/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class DetalleAclaracionesVC: BaseItemVC{
    @IBOutlet weak var textViewComentarios: UITextView!
    @IBOutlet weak var ticketLabel: UILabel!
    @IBOutlet weak var typeAclaracionLabel: UILabel!
    @IBOutlet weak var commissionLabel: UILabel!
    @IBOutlet weak var accountAclaracionLabel: UILabel!
    @IBOutlet weak var packageLabel: UILabel!
    @IBOutlet weak var addonLabel: UILabel!
    @IBOutlet weak var solicitudeDateLabel: UILabel!
    @IBOutlet weak var datePaymentBillLabel: UILabel!
    @IBOutlet weak var dateInstallationLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.addBorder(view: textViewComentarios)
    }
}
