//
//  ConsultaAclaracionService.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 13/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

class ConsultaAclaracionService{
    
    func getConsultaAclaracion(_ callBack:@escaping ([ConsultaAclaracionModel]) -> Void){
        let data = [
            ConsultaAclaracionModel(ticket: "AC00000001", cuenta: "0200121180", status: "Aprobada"),
            ConsultaAclaracionModel(ticket: "AC00000002", cuenta: "0123928945", status: "Rechazada"),
            ConsultaAclaracionModel(ticket: "AC00000003", cuenta: "0232392383", status: "En revisión"),
            ConsultaAclaracionModel(ticket: "AC00000004", cuenta: "0200121130", status: "Rechazada"),
            ConsultaAclaracionModel(ticket: "AC00000005", cuenta: "0232392343", status: "En revision"),
            ConsultaAclaracionModel(ticket: "AC00000006", cuenta: "0232392634", status: "Aprobada"),
            ConsultaAclaracionModel(ticket: "AC00000007", cuenta: "0232394564", status: "En revision"),
            ]
        let delayTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            callBack(data)
        }
    }
}
