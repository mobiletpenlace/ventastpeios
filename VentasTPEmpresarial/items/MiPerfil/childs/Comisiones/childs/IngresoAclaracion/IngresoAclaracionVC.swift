//
//  ClarificationsViewController.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 18/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftBaseLibrary
import DLRadioButton

class IngresoAclaracionVC: BaseItemVC, CustomAlertViewDelegate {
    @IBOutlet weak var packagePaymentButton: DLRadioButton!
    @IBOutlet weak var mDescribeClarification: UITextView!
    fileprivate let presenter = IngresoAclaracionPresenter(model: IngresoAclaracionService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow() {
        super.viewDidShow()
        presenter.viewDidShow()
    }
    
    private func setUpView() {
        super.addBorder(view: mDescribeClarification)
        packagePaymentButton.isSelected = true
    }

    @IBAction func selectButtonEnviar(_ sender: UIButton) {
        let seleccionada = packagePaymentButton.selected()
        let tipoAclaracion = seleccionada!.titleLabel!.text!
        let descripcion = mDescribeClarification.text!
        presenter.enviarData(tipoAclaracion, descripcion)
    }
    
    func clickOKButtonDialog(value: Any) {
        self.padreController?.cambiarVista(index: 0)//regresar al menu, cuando se acepta la notificación
    }
    
    func clickCancelButtonDialog() {
        Logger.d("se ha cancelado el dialogo")
    }
    
    /*protocol*/
    func viewWillAppear() {
        
    }
}

extension IngresoAclaracionVC: IngresoAclaracionDelegate{
    
    func limpiarDatos(){
        mDescribeClarification.text = ""
    }
    
    func setData(_ data: IngresoAclaracionModel) {
        let folio = data.folio
        super.showDialog(customAlert: DialogAclaracionExitosa(folio: folio), delegate: self)
    }
    
    func updateTitleHeader() {
        let header = HeaderData(
            imagen: #imageLiteral(resourceName: "icon_Comisiones"),
            titulo: "Comisiones",
            titulo2: nil,
            titulo3: nil,
            subtitulo: nil,
            closure:{
                [weak self] in
                self?.padreController?.cambiarVista(index: 1)
            }
        )
        super.updateTitle(headerData: header)
    }
    
    func startLoading() {
        super.showLoading(view: self.view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
}
