//
//  IngresoAclaracionPresenter.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 14/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

protocol IngresoAclaracionDelegate: BaseDelegate {
    func setData(_ data: IngresoAclaracionModel)
    func limpiarDatos()
}

class IngresoAclaracionPresenter {
    fileprivate let model: IngresoAclaracionService
    weak fileprivate var view: IngresoAclaracionDelegate?
    
    init(model: IngresoAclaracionService){
        self.model = model
    }
    
    func attachView(view: IngresoAclaracionDelegate){
        self.view = view
    }
    
    func viewDidAppear(){
        
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
    }
    
    func detachView() {
        view = nil
    }
    
    func enviarData(_ tipoAclaracion: String, _ descripcion: String){
        self.view?.startLoading()
        model.enviarAclaracion{[weak self] data in
            self?.view?.finishLoading()
            self?.view?.limpiarDatos()
            self?.view?.setData(data)
        }
    }
}
