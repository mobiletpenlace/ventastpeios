//
//  IngresoAclaracionService.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 14/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

class IngresoAclaracionService{
    
    func enviarAclaracion(_ callBack: @escaping (IngresoAclaracionModel) -> Void){
        
        let data = IngresoAclaracionModel(folio: "FTGT200001")
        
        let delayTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            callBack(data)
        }
    }
    
    
}
