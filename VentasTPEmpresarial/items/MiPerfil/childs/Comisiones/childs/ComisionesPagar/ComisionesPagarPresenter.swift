//
//  ComisionesPagarPresenter.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 10/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

protocol ComisionesPagarDelegate: BaseDelegate {
    func updateData()
}

class ComisionesPagarPresenter {
    fileprivate let model: ComisionesPagarService
    weak fileprivate var view: ComisionesPagarDelegate?
    fileprivate var dataList = [ComisionesPagarModel]()
    
    init(model: ComisionesPagarService){
        self.model = model
    }
    
    func attachView(view: ComisionesPagarDelegate){
        self.view = view
    }
    
    func viewDidAppear(){
        
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
        getData()
    }
    
    func detachView() {
        view = nil
    }
    
    func getItemIndex(index: Int) -> ComisionesPagarModel {
        return dataList[index]
    }
    
    func getSize() -> Int{
        return dataList.count
    }
    
    func getData(){
        self.view?.startLoading()
        model.getComisionesPagar{
            [weak self] data in
            self?.view?.finishLoading()
            if(data.count == 0){
                self?.dataList = [ComisionesPagarModel]()
            }else{
                self?.dataList = data
            }
            self?.view?.updateData()
        }
    }
}
