//
//  ComisionesPagarService.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 10/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

class ComisionesPagarService {
    
    func getComisionesPagar(_ callBack:@escaping ([ComisionesPagarModel]) -> Void){
        let data = [
            ComisionesPagarModel(fecha: "21/01/2018", cuenta: "1233717791"),
            ComisionesPagarModel(fecha: "11/02/2018", cuenta: "1255717792"),
            ComisionesPagarModel(fecha: "15/03/2018", cuenta: "1233717793"),
            ComisionesPagarModel(fecha: "12/04/2018", cuenta: "1233717794"),
            ComisionesPagarModel(fecha: "22/05/2018", cuenta: "1233717795"),
            ComisionesPagarModel(fecha: "24/06/2018", cuenta: "1233717796"),
            ]
        let delayTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            callBack(data)
        }
    }
    
}
