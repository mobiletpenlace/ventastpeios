//
//  HistorialPagoPresenter.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 20/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

protocol HistorialPagoDelegate: BaseDelegate {
    func actualizar()
}

class HistorialPagoPresenter {
    fileprivate let model: HistorialPagoService
    weak fileprivate var view: HistorialPagoDelegate?
    fileprivate var dataList = [HistorialPagoModel]()
    
    init(model: HistorialPagoService){
        self.model = model
    }
    
    func attachView(view: HistorialPagoDelegate){
        self.view = view
    }
    
    func viewDidAppear(){
        
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
        getData()
    }
    
    func detachView() {
        view = nil
    }
    
    func getItemIndex(index: Int) -> HistorialPagoModel {
        return dataList[index]
    }
    
    func getSize() -> Int{
        return dataList.count
    }
    
    func getData(){
        self.view?.startLoading()
        model.getHistorialPago{[weak self] data in
            self?.view?.finishLoading()
            if(data.count == 0){
                self?.dataList = [HistorialPagoModel]()
            }else{
                self?.dataList = data
            }
            self?.view?.actualizar()
        }
    }
}
