//
//  DetalleCuentaPresenter.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 10/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

protocol DetalleCuentaDelegate: BaseDelegate {
    func actualizar()
}

class DetalleCuentaPresenter {
    fileprivate let model: DetalleCuentaService
    weak fileprivate var view: DetalleCuentaDelegate?
    fileprivate var data: DetalleCuentaModel? = nil
    
    init(model: DetalleCuentaService){
        self.model = model
    }
    
    func attachView(view: DetalleCuentaDelegate){
        self.view = view
    }
    
    func viewDidAppear(){
        
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
        getData()
    }
    
    func detachView() {
        view = nil
    }
    
    func getDetalleCuenta() -> DetalleCuentaModel? {
        return data
    }
    
    func getData(){
        self.view?.startLoading()
        model.getDetalleCuenta{[weak self] data in
            self?.view?.finishLoading()
            self?.data = data
            self?.view?.actualizar()
        }
    }
}
