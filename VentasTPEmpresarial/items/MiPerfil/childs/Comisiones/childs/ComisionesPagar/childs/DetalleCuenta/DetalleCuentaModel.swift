//
//  DetalleCuentaModel.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 10/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import Foundation

struct DetalleCuentaModel {
    let cuenta: String
    let cliente: String
    let producto: String
    let factorPago: String
    let ranking: String
    let valorTotOpor: Float
    let pagos: Float
    let pagosPendientes: Float
    
}
