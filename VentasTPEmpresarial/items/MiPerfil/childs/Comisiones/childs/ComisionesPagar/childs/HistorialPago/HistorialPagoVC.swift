//
//  TablaHistorialPagoViewController.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 17/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class HistorialPagoVC: BaseItemVC{
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var historyAccountLabel: UILabel!
    fileprivate let presenter = HistorialPagoPresenter(model: HistorialPagoService())
    var nombreCelda: String = "HistorialPagoCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow() {
        super.viewDidShow()
        presenter.viewDidShow()
    }
    
    private func setUpView(){
        cargarTableView()
    }
    
    func cargarTableView() -> Void {
        let nib = UINib.init(nibName: nombreCelda, bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: nombreCelda)
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    @IBAction func detailAccountButton(_ sender: UIButton) {
        self.padreController?.cambiarVista(index: 0)
    }
}

extension HistorialPagoVC: UITableViewDataSource, BaseFormatNumber{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: nombreCelda, for: indexPath) as! HistorialPagoCell
        let data = presenter.getItemIndex(index: indexPath.row)
        
        cell.labelFechaPago.text = data.fechaPago
        cell.labelCantidad.text = "$"+numeroAFormatoPrecio(numero: data.cantidad)  
        cell.labelTipoMovimiento.text = data.tipoMovimiento
        cell.labelObservaciones.text = data.observaciones
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getSize()
    }
}

extension HistorialPagoVC:UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return HistorialPagoCellHeader().view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //let currentCell = tableView.cellForRow(at: indexPath)! as! PaymentHistoryTableViewCell
    }
}

extension HistorialPagoVC: HistorialPagoDelegate{
    func updateTitleHeader() {
        let header = HeaderData(
            imagen: nil,
            titulo: nil,
            titulo2: nil,
            titulo3: nil,
            subtitulo: nil,
            closure: nil
        )
        super.updateTitle(headerData: header)
    }
    
    func startLoading() {
        super.showLoading(view: self.view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
    func actualizar() {
        tableView.reloadData()
        let indexPath = IndexPath(row: 0, section: 0)
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .bottom)
    }
    
}
