//
//  PaymentHistoryTableViewCell.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 17/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class HistorialPagoCell: UITableViewCell {
    @IBOutlet weak var labelFechaPago: UILabel!
    @IBOutlet weak var labelCantidad: UILabel!
    @IBOutlet weak var labelTipoMovimiento: UILabel!
    @IBOutlet weak var labelObservaciones: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
