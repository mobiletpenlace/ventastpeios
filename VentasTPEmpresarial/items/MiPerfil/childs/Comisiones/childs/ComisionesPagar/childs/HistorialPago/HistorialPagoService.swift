//
//  HistorialPagoService.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 20/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

class HistorialPagoService {
    
    func getHistorialPago(_ callBack:@escaping ([HistorialPagoModel]) -> Void){
        let data = [
            HistorialPagoModel(fechaPago: "21/01/2011", cantidad: 8500.0, tipoMovimiento: "Pago primer factura", observaciones: "Sin observaciones"),
            HistorialPagoModel(fechaPago: "21/01/2011", cantidad: 10500, tipoMovimiento: "Pago segunda factura", observaciones: "Sin observaciones"),
            HistorialPagoModel(fechaPago: "21/01/2011", cantidad: 15500, tipoMovimiento: "Chargeback factura", observaciones: "Sin observaciones"),
            HistorialPagoModel(fechaPago: "21/01/2011", cantidad: 6500, tipoMovimiento: "Pago tercera factura", observaciones: "Sin observaciones"),
            HistorialPagoModel(fechaPago: "21/01/2011", cantidad: 9500, tipoMovimiento: "Pago cuarta factura", observaciones: "Sin observaciones"),
            HistorialPagoModel(fechaPago: "21/01/2011", cantidad: 13500, tipoMovimiento: "Chargeback factura", observaciones: "Sin observaciones"),
            HistorialPagoModel(fechaPago: "21/01/2011", cantidad: 5500, tipoMovimiento: "Chargeback factura", observaciones: "Sin observaciones"),
            HistorialPagoModel(fechaPago: "21/01/2011", cantidad: 4500, tipoMovimiento: "Pago quinta factura", observaciones: "Sin observaciones"),
            ]
        let delayTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            callBack(data)
        }
    }
    
}
