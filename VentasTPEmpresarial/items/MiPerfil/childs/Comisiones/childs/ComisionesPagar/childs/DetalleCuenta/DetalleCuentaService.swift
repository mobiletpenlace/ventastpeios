//
//  DetalleCuentaService.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 10/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

class DetalleCuentaService {

    func getDetalleCuenta(_ callBack:@escaping (DetalleCuentaModel?) -> Void){
        let data = DetalleCuentaModel(
                cuenta: "0132586744",
                cliente: "Controladora de Negocios Comerciales S.A. de C.V. ",
                producto: "Super Internet 3Mbps",
                factorPago: "Super Internet 3Mbps",
                ranking: "Super Internet 3Mbps",
                valorTotOpor: 25000,
                pagos: -4500,
                pagosPendientes: 0
            )
        let delayTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            callBack(data)
        }
    }
}
