//
//  HistorialPagoModel.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 20/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct HistorialPagoModel {
    let fechaPago: String
    let cantidad: Float
    let tipoMovimiento: String
    let observaciones: String
}
