//
//  AccountDetailViewController.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 18/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class DetalleCuentaVC: BaseItemVC{
    @IBOutlet weak var labelCuenta: UILabel!
    @IBOutlet weak var labelCliente: UILabel!
    @IBOutlet weak var labelProducto: UILabel!
    @IBOutlet weak var labelFactorPago: UILabel!
    @IBOutlet weak var labelRanking: UILabel!
    @IBOutlet weak var labelValorTotalOportunidad: UILabel!
    @IBOutlet weak var labelPagos: UILabel!
    @IBOutlet weak var labelPagosPendientes: UILabel!
    fileprivate let presenter = DetalleCuentaPresenter(model: DetalleCuentaService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow() {
        super.viewDidShow()
        presenter.viewDidShow()
    }
    
    private func setUpView() {
    }
    
    @IBAction func historyPaymentButton(_ sender: UIButton) {
        self.padreController?.cambiarVista(index: 1)
    }
    
    @IBAction func aclaracionesButton(_ sender: UIButton) {
        self.padreController?.padreController?.cambiarVista(index: 3)
    }
}

extension DetalleCuentaVC: DetalleCuentaDelegate, BaseFormatNumber{
    
    func updateTitleHeader() {
        let header = HeaderData(
            imagen: nil,
            titulo: nil,
            titulo2: nil,
            titulo3: nil,
            subtitulo: nil,
            closure:nil
        )
        super.updateTitle(headerData: header)
    }
    
    func startLoading() {
        super.showLoading(view: self.view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
    func actualizar(){
        let data  =  presenter.getDetalleCuenta()
        labelCuenta.text = data?.cuenta ?? ""
        labelCliente.text = data?.cliente ?? ""
        labelProducto.text = data?.producto ?? ""
        labelFactorPago.text = data?.factorPago ?? ""
        labelRanking.text = data?.ranking ?? ""
        
        labelValorTotalOportunidad.text = "$" + numeroAFormatoPrecio(numero: data?.valorTotOpor ?? 0)
        labelPagos.text = "$" + numeroAFormatoPrecio(numero: data?.pagos ?? 0)
        labelPagosPendientes.text = "$" + numeroAFormatoPrecio(numero: data?.pagosPendientes ?? 0)
    }
}

