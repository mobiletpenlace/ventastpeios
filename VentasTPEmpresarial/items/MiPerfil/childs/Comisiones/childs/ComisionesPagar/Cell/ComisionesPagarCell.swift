//
//  AccountDetailTableViewCell.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 17/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class ComisionesPagarCell: UITableViewCell {
    @IBOutlet weak var labelFecha: UILabel!
    @IBOutlet weak var labelCuenta: UILabel!
    @IBOutlet weak var checkBoxDetallecuenta: CheckBoxButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        checkBoxDetallecuenta.isChecked = selected
    }
    
    @IBAction func selectAccountDetailButton(_ sender: Any) {
        
    }
}
