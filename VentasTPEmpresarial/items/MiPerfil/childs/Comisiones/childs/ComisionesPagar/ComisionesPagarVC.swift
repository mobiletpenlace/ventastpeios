//
//  ComisionesToPayViewController.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 12/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftBaseLibrary
import SwiftEventBus

class ComisionesPagarVC: BaseItemVC {
    @IBOutlet weak var textFieldFechaInicial: UITextField!
    @IBOutlet weak var textFieldFechaFinal: UITextField!
    
    @IBOutlet weak var viewDetalleDeLaCuenta: UIView!
    @IBOutlet weak var tableview: UITableView!
    fileprivate let presenter = ComisionesPagarPresenter(model: ComisionesPagarService())
    var nombreCelda: String = "ComisionesPagarCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow() {
        super.viewDidShow()
        presenter.viewDidShow()
    }
    
    private func setUpView() {
        configurarTableView()
        agregarSubviews()
        /*textFieldFechaFinal.setDatePicker()
        textFieldFechaInicial.setDatePicker()*/
        super.addShadowView(view: viewDetalleDeLaCuenta)
        super.addBorder(view: textFieldFechaInicial)
        super.addBorder(view: textFieldFechaFinal)
    }
    
    
    
    private func agregarSubviews(){
        var viewControllers = [BaseItemVC]()
        viewControllers.append(DetalleCuentaVC())
        viewControllers.append(HistorialPagoVC())
        super.addViewXIBList(view: viewDetalleDeLaCuenta, viewControllers: viewControllers)
    }
    
    private func configurarTableView() -> Void {
        let nib = UINib.init(nibName: nombreCelda, bundle: nil)
        self.tableview.register(nib, forCellReuseIdentifier: nombreCelda)
        self.tableview.delegate = self
        self.tableview.dataSource = self
    }
}

extension ComisionesPagarVC:UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: nombreCelda, for: indexPath) as! ComisionesPagarCell
        let data = presenter.getItemIndex(index: indexPath.row)
        cell.labelFecha.text =  data.fecha
        cell.labelCuenta.text = data.cuenta
        cell.selectionStyle = .none //desactivar el color al seleccionar la celda
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getSize()
    }
}

extension ComisionesPagarVC:UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return ComisionesPagar2CellHeader().view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentCell = tableView.cellForRow(at: indexPath)! as! ComisionesPagarCell
        currentCell.setSelected(true, animated: false)
    }
}

extension ComisionesPagarVC: ComisionesPagarDelegate{
    
    func updateTitleHeader() {
        let header = HeaderData(
            imagen: #imageLiteral(resourceName: "icon_Comisiones"),
            titulo: "Comisiones",
            titulo2: nil,
            titulo3: nil,
            subtitulo: nil,
            closure:{ [weak self] in
                self?.padreController?.cambiarVista(index: 0)
            }
        )
        super.updateTitle(headerData: header)
    }
    
    func startLoading() {
        super.showLoading(view: self.view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
    func updateData(){
        self.tableview.reloadData()
        let indexPath = IndexPath(row: 0, section: 0)
        seleccionarCeldaTabla(indexPath: indexPath)
    }
    
    func seleccionarCeldaTabla(indexPath: IndexPath){
        tableview.selectRow(at: indexPath, animated: true, scrollPosition: .bottom)
    }
    
    
}
