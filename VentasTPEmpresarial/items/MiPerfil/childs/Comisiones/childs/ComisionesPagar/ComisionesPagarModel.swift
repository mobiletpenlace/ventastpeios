//
//  ComisionesPagarModel.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 10/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

struct ComisionesPagarModel {
    let fecha: String
    let cuenta: String
}
