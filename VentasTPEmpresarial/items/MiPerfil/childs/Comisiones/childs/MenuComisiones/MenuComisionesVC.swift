//
//  ComisionesMenuViewController.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 12/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class MenuComisionesVC: BaseItemVC {
    @IBOutlet weak var viewPorPagar: UIView!
    @IBOutlet weak var viewPagadas: UIView!
    @IBOutlet weak var viewBonoSemestral: UIView!
    @IBOutlet weak var viewAclaraciones: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.addShadowView(view: viewPorPagar)
        super.addShadowView(view: viewPagadas)
        super.addShadowView(view: viewBonoSemestral)
        super.addShadowView(view: viewAclaraciones)
    }
    
    override func viewDidShow() {
        super.viewDidShow()
        let header = HeaderData(
            imagen: #imageLiteral(resourceName: "icon_Comisiones"),
            titulo: "Comisiones",
            titulo2: nil,
            titulo3: nil,
            subtitulo: nil,
            closure: nil
        )
        super.updateTitle(headerData: header)
    }
    
    @IBAction func clickPagar(_ sender: UIButton) {
        sender.animateBound(view: viewPorPagar){ [unowned self] _ in
            self.padreController?.cambiarVista(index: 1)
        }
    }
    
    @IBAction func clickPagadas(_ sender: UIButton) {
        sender.animateBound(view: viewPagadas){ [unowned self] _ in
            self.padreController?.cambiarVista(index: 0)
        }
    }
    
    @IBAction func clickBono(_ sender: UIButton) {
        sender.animateBound(view: viewBonoSemestral){ [unowned self] _ in
            self.padreController?.cambiarVista(index: 0)
        }
    }
    
    @IBAction func clickAclaraciones(_ sender: UIButton) {
        sender.animateBound(view: viewAclaraciones){ [unowned self] _ in
            self.padreController?.cambiarVista(index: 2)
        }
    }
    
}
