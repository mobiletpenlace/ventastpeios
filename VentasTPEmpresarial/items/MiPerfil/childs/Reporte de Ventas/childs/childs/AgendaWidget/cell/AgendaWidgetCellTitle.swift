//
//  AgendaWidgetCellTitle.swift
//  VentasTPEmpresarial
//
//  Created by eduardo mancilla on 11/30/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class AgendaWidgetCellTitle: UITableViewCell {
    @IBOutlet weak var labelTitulo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
