//
//  AgendaWidgetVC.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 07/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class AgendaWidgetVC: BaseItemVC {
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var buttonVerMas: UIButton!
    var nombreCelda1: String = "AgendaWidgetCell"
    var nombreCelda2: String = "AgendaWidgetCellTitle"
    fileprivate let presenter = AgendaWidgetPresenter(model: DialogoAgendaService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow() {
        super.viewDidShow()
        presenter.viewDidShow()
    }
    
    private func setUpView() {
        cargarTableView()
        super.addShadowView(view: buttonVerMas)
    }
    
    func cargarTableView() -> Void {
        
        let nib = UINib.init(nibName: nombreCelda1, bundle: nil)
        self.tableview.register(nib, forCellReuseIdentifier: nombreCelda1)
        
        let nib2 = UINib.init(nibName: nombreCelda2, bundle: nil)
        self.tableview.register(nib2, forCellReuseIdentifier: nombreCelda2)
        
        self.tableview.delegate = self
        self.tableview.dataSource = self
    }
    
    @IBAction func clickVerMas(_ sender: UIButton) {
        sender.animateBound(view: sender){ [weak self] _  in
            self?.showDialog(customAlert: DialogoAgendaVC())
        }
    }
    
}

extension AgendaWidgetVC: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: nombreCelda2, for: indexPath) as! AgendaWidgetCellTitle
            
//            let hora =  presenter.getItemIndex(section: indexPath.row, index: 0)
            let data =  presenter.getItemIndex(section: indexPath.row, index: 1)
            cell.labelTitulo.text = data.hora
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: nombreCelda1, for: indexPath) as! AgendaWidgetCell
            
            
            let hora =  presenter.getItemIndex(section: indexPath.row, index: 0)
            let data =  presenter.getItemIndex(section: indexPath.row, index: 1)
            
            cell.labelHora.text = hora.hora
            
            cell.labelHoraInicio.text = data.horaInicio + "-"
            cell.labelHoraFin.text = data.horaFin
            cell.labelNombre.text = data.nombre
            cell.labelAsunto.text = data.asunto
            cell.selectionStyle = .none //desactivar el color al seleccionar la celda
            cell.ocultar(!data.mostrar)
            return cell
        }
//        if indexPath.row == 0{
//            cell.labelNombre.text = data.hora
//        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getSize()
    }
}

extension AgendaWidgetVC: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 50
        }
        return UITableViewAutomaticDimension
    }
    
}

extension AgendaWidgetVC: AgendaWidgetDelegate{
   
    func reloadTable(){
        tableview.reloadData()
    }
    
    func reloadRows(indexPath: [IndexPath]){
        tableview.reloadRows(at: indexPath, with: .left)
    }
    
    func seleccionarCeldaTabla(indexPath: IndexPath){
        let size = tableview.numberOfRows(inSection: 0)
        if size > 0{
            tableview.selectRow(at: indexPath, animated: true, scrollPosition: .bottom)
        }
    }
    
    func updateTitleHeader() {
        
    }
    
    func startLoading() {
        super.showLoading(view: self.view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
}
