//
//  AgendaWidgetPresenter.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 07/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol AgendaWidgetDelegate: BaseDelegate {
    func reloadTable()
    func reloadRows(indexPath: [IndexPath])
    func seleccionarCeldaTabla(indexPath: IndexPath)
}

class AgendaWidgetPresenter: AgendaLib{
    fileprivate let model: DialogoAgendaService
    weak fileprivate var view: AgendaWidgetDelegate?
    fileprivate var dataList = [[AgendaWidgetModel]]()
    var fecha: Date = Date()
    let FORMAT_FECHA_SERVER = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    
    init(model: DialogoAgendaService){
        self.model = model
    }
    
    func attachView(view: AgendaWidgetDelegate){
        self.view = view
        SwiftEventBus.onMainThread(self, name: "DialogoAgenda-actualizar") { [weak self] result in
            self?.getData()
        }
    }
    
    func viewDidAppear(){
        
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
        getData()
    }
    
    func detachView() {
        view = nil
    }
    
    func getItemIndex(section: Int, index: Int) -> AgendaWidgetModel {
        return dataList[section][index]
    }
    
    func getSize() -> Int{
        return dataList.count
    }
    
    func getSize(section: Int) -> Int{
        if section >= 0 && section < getSize(){
            return dataList[section].count
        }
        return 0
    }
    
    func getData(){
        view?.startLoading()
        fecha = Date()
        fecha.hour = 6
        fecha.minute = 0
        model.consultarAgenda(fecha){[weak self] data in
            self?.onFinishGetData(data: data)
        }
    }
    
    func onFinishGetData( data datos: [DialogoAgendaModel]){
        view?.finishLoading()
        dataList = [[AgendaWidgetModel]]()
        let _ = createTableDay(fecha)
        for dato in datos{
            let listaFechas = generarListaFechaHora(
                fechaIni: createDate(dato.fechaInicio, format: FORMAT_FECHA_SERVER),
                fechaFin: createDate(dato.fechaFin, format: FORMAT_FECHA_SERVER)
            )
            for fecha in listaFechas{
                add(fecha: fecha, id: dato.id, nombreContacto: dato.nombreContacto, nombreEmpresa: dato.nombreEmpresa, asunto: dato.asunto, comentario: dato.comentario, celular: dato.celular, telefono: dato.telefono)
            }
        }
        for (_, section) in table.enumerated(){
            var l = [AgendaWidgetModel]()
            for row in section{
                l.append(
                    AgendaWidgetModel(
                      hora: row.titulo,
                      horaInicio: row.fechaInicio,
                      horaFin: row.fechaFin,
                      nombre: row.nombreContacto,
                      asunto: row.nombreEmpresa,
                      mostrar: row.mostrar
                    )
                )
            }
            dataList.append(l)
        }
        view?.reloadTable()
    }
    
    func generarListaFechaHora(fechaIni: Date, fechaFin: Date) -> [Date]{
        let diferencia = diference(fechaIni, fechaFin, .hour).hour ?? 0
        var listaFechas = [Date]()
        listaFechas.append(fechaIni)
        if diferencia > 1 {
            for i in 1..<diferencia{
                let fechaNew = incrementDate(fechaIni, value: i, byAdding: .hour)
                listaFechas.append(fechaNew)
            }
        }
        return listaFechas
    }
}
