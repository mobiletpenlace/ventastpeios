//
//  AgendaWidgetModel.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 07/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct AgendaWidgetModel {
    let hora: String
    let horaInicio: String
    let horaFin: String
    let nombre: String
    let asunto: String
    var mostrar: Bool
}
