//
//  AgendaWidgetCell.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 06/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class AgendaWidgetCell: UITableViewCell {
    @IBOutlet weak var labelHora: UILabel!
    @IBOutlet weak var labelHoraInicio: UILabel!
    @IBOutlet weak var labelHoraFin: UILabel!
    @IBOutlet weak var labelNombre: UILabel!
    @IBOutlet weak var labelAsunto: UILabel!
    @IBOutlet weak var panelCita: UIView!
    var activa: Bool = true
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func ocultar(_ value: Bool){
        activa = !value
        if value{
            panelCita.alpha = 0
        }else{
            panelCita.alpha = 1
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)   
    }
    
}
