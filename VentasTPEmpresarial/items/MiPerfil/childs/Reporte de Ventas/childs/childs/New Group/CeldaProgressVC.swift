//
//  CeldaProgressViewController.swift
//  HelloWorldProyect
//
//  Created by julian dorantes fuertes on 12/06/18.
//  Copyright © 2018 total. All rights reserved.
//

import UIKit
import UICircularProgressRing

class CeldaProgressVC: BaseItemVC {
    @IBOutlet weak var circleProgress: UICircularProgressRingView!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var celda1: UIView!
    var texto: String
    var progress: Int
    var maxValue: Int
    
    override func viewDidLoad() {
        super.viewDidLoad()
        label1.text = texto
        circleProgress.maxValue = CGFloat(maxValue)
        circleProgress.valueIndicator = String("/\(maxValue)")
        circleProgress.innerRingColor = Colores.azulOscuro
        circleProgress.outerRingWidth = 8
        circleProgress.innerRingWidth = 8
        circleProgress.font = UIFont(name: "Montserrat", size: 13)!
        circleProgress.fontColor = .black
        circleProgress.ringStyle = .gradient
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        animar(closure: nil)
    }
    
    init( texto: String, progress: Int, maxVal: Int) {
        self.progress = progress
        self.texto = texto
        self.maxValue = maxVal
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func animar(closure: (() -> (Void))?){
        DispatchQueue.main.async {
            self.circleProgress.setProgress(
                to: CGFloat(self.progress),
                duration: 2.0,
                completion: nil
            )
        }
    }

}
