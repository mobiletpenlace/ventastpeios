//
//  DialogVentaPendientePresenter.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 10/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

protocol DialogVentaAprovadaDelegate: BaseDelegate {
    func reloadTable()
    func reloadRows(indexPath: [IndexPath])
}

class DialogVentaAprovadaPresenter{
    weak fileprivate var view: DialogVentaAprovadaDelegate?
    fileprivate let service: DialogVentaAprovadaService
    fileprivate var dataList = [DialogVentaAprovadaModel]()
    
    init(service: DialogVentaAprovadaService){
        self.service = service
    }

    func attachView(view: DialogVentaAprovadaDelegate){
        self.view = view
        actualizarDatos()
    }
    
    func viewDidAppear(){
    }

    func detachView() {
        view = nil
    }
    
    func getItemIndex(index: Int) -> DialogVentaAprovadaModel {
        return dataList[index]
    }
    
    func getSize() -> Int{
        return dataList.count
    }
    
    func actualizarDatos(){
        self.view?.startLoading()
        service.getData(){[weak self] data in
            self?.onFinishGetData(data: data)
        }
    }
    
    func onFinishGetData( data: [DialogVentaAprovadaModel]){
        view?.finishLoading()
        if(data.count == 0){
            dataList = [DialogVentaAprovadaModel]()
            view?.reloadTable()
        }else{
            dataList = data
            view?.reloadTable()
        }
    }
}
