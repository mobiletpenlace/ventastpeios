//
//  DialogVentaPendienteVC.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 07/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class DlgVtsProcesoInstalacionProcesoVC: BaseDialogCustom {
    @IBOutlet weak var viewRoot: UIView!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var textViewcomentarios: UITextView!
    @IBOutlet weak var buttonEnviar: UIButton!
    var nombreCelda1: String = "DlgVtsProcesoInstalacionProcesoCell"
    fileprivate let presenter = DlgVtsProcesoInstalacionProcesoPresenter(service: DlgVtsProcesoInstalacionProcesoService())
    var spinner :UIView?
   
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    private func setUpView() {
        super.setView(view: viewRoot)
        cargarTableView()
        textViewcomentarios.layer.borderWidth = CGFloat(0.5)
        textViewcomentarios.layer.borderColor = UIColor.gray.cgColor
        buttonEnviar.layer.cornerRadius = 5
    }
   
    func cargarTableView() -> Void {
        let nib = UINib.init(nibName: nombreCelda1, bundle: nil)
        self.tableview.register(nib, forCellReuseIdentifier: nombreCelda1)
        self.tableview.delegate = self
        self.tableview.dataSource = self
    }

    @IBAction func clickCancelar(_ sender: UIButton) {
        super.cancel()
    }
}

extension DlgVtsProcesoInstalacionProcesoVC: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: nombreCelda1, for: indexPath)
        if let cell = cell as? DlgVtsProcesoInstalacionProcesoCell {
            let data =  presenter.getItemIndex(index: indexPath.row)
            cell.labeltitulo.text = data.titulo
        }
        cambiarColorCelda(cell: cell)
        return cell
    }

    func cambiarColorCelda(cell: UITableViewCell){
        let backgroundView = UIView()
        backgroundView.backgroundColor = Colores.selectDlgMiPerfilCelda
        cell.selectedBackgroundView = backgroundView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getSize()
    }
}


extension DlgVtsProcesoInstalacionProcesoVC:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension DlgVtsProcesoInstalacionProcesoVC: DlgVtsProcesoInstalacionProcesoDelegate, BaseTreeLoadingDelegate{
    
    func updateTextoLoading(texto: String) {
        
    }
    
    func reloadTable(){
        tableview.reloadData()
        let indexPath = IndexPath(row: 0, section: 0)
        tableview.selectRow(at: indexPath, animated: true, scrollPosition: .bottom)
    }
    
    func reloadRows(indexPath: [IndexPath]){
        tableview.reloadRows(at: indexPath, with: .left)
    }
    
    func updateTitleHeader() {
        
    }
    
    func startLoading() {
        spinner = showLoading(onView: self.tableview)
    }
    
    func finishLoading() {
        if let loadview = spinner{
            hideLoading(spinner: loadview)
        }
    }
}
