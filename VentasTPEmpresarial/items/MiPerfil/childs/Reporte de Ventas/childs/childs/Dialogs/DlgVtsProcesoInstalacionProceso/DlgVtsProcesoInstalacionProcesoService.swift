//
//  DialogVentaPendienteService.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 10/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

class DlgVtsProcesoInstalacionProcesoService {
    
    func getData(_ callBack:@escaping ([DlgVtsProcesoInstalacionProcesoModel]) -> Void){
        let data = [
            DlgVtsProcesoInstalacionProcesoModel(titulo: "Número de Contrato RE5253577238"),
            DlgVtsProcesoInstalacionProcesoModel(titulo: "Número de Contrato RE5253577239"),
            DlgVtsProcesoInstalacionProcesoModel(titulo: "Número de Contrato RE5253534231"),
            DlgVtsProcesoInstalacionProcesoModel(titulo: "Número de Contrato RE5253571133"),
            DlgVtsProcesoInstalacionProcesoModel(titulo: "Número de Contrato RE5253577232"),
            DlgVtsProcesoInstalacionProcesoModel(titulo: "Número de Contrato RE5253577234"),
            DlgVtsProcesoInstalacionProcesoModel(titulo: "Número de Contrato RE5253577235"),
            ]
        let delayTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            callBack(data)
        }
    }
}
