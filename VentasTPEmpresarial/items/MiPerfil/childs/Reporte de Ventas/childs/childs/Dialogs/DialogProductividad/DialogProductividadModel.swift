//
//  DialogForecastModel.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 12/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct DialogProductividadModel {
    let datos: [Double]
    let titulo: String
}
