//
//  DialogForecastPresenter.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 12/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

protocol DialogForecastDelegate: BaseDelegate {
    func reloadData()
}

class DialogForecastPresenter{
    weak fileprivate var view: DialogForecastDelegate?
    fileprivate let service: DialogForecastService
    fileprivate var dataList = [DialogForecastModel]()
    
    init(service: DialogForecastService){
        self.service = service
    }
    
    func attachView(view: DialogForecastDelegate){
        self.view = view
        actualizarDatos()
    }
    
    func viewDidAppear(){
    }
    
    func detachView() {
        view = nil
    }
    
    func getItemIndex(index: Int) -> DialogForecastModel {
        return dataList[index]
    }
    
    func getSize() -> Int{
        return dataList.count
    }
    
    func actualizarDatos(){
        self.view?.startLoading()
        service.getData(){[weak self] data in
            self?.onFinishGetData(data: data)
        }
    }
    
    func onFinishGetData( data: [DialogForecastModel]){
        view?.finishLoading()
        if(data.count == 0){
            dataList = [DialogForecastModel]()
            view?.reloadData()
        }else{
            dataList = data
            view?.reloadData()
        }
    }
}
