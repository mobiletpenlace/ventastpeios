//
//  DialogVentaPendientePresenter.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 10/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

protocol DialogVentaPendienteDelegate: BaseDelegate {
    func reloadTable()
    func reloadRows(indexPath: [IndexPath])
}

class DialogVentaPendientePresenter{
    weak fileprivate var view: DialogVentaPendienteDelegate?
    fileprivate let service: DialogVentaPendienteService
    fileprivate var dataList = [DialogVentaPendienteModel]()
    
    init(service: DialogVentaPendienteService){
        self.service = service
    }

    func attachView(view: DialogVentaPendienteDelegate){
        self.view = view
        actualizarDatos()
    }
    
    func viewDidAppear(){
    }

    func detachView() {
        view = nil
    }
    
    func getItemIndex(index: Int) -> DialogVentaPendienteModel {
        return dataList[index]
    }
    
    func getSize() -> Int{
        return dataList.count
    }
    
    func actualizarDatos(){
        self.view?.startLoading()
        service.getData(){[weak self] data in
            self?.onFinishGetData(data: data)
        }
    }
    
    func onFinishGetData( data: [DialogVentaPendienteModel]){
        view?.finishLoading()
        if(data.count == 0){
            dataList = [DialogVentaPendienteModel]()
            view?.reloadTable()
        }else{
            dataList = data
            view?.reloadTable()
        }
    }
}
