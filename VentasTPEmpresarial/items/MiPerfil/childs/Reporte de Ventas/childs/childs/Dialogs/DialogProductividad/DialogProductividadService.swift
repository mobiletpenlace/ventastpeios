//
//  DialogForecastService.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 12/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
class DialogProductividadService{
    func getData(_ callBack:@escaping ([DialogProductividadModel]) -> Void){
        let data = [
            DialogProductividadModel(datos: [1, 2, 1, 1, 1, 1], titulo: "abril\n 2017"),
            DialogProductividadModel(datos: [1, 3, 1, 1, 1, 1], titulo: "mayo\n 2017"),
            DialogProductividadModel(datos: [2, 3, 2, 2, 2, 2], titulo: "junio\n 2017"),
            DialogProductividadModel(datos: [1, 3, 1, 1, 1, 1], titulo: "julio\n 2017"),
            DialogProductividadModel(datos: [1, 2, 1, 1, 1, 1], titulo: "agosto\n 2017"),
            DialogProductividadModel(datos: [2, 3, 1, 1, 1, 1], titulo: "septiembre\n 2017"),
            
            DialogProductividadModel(datos: [1, 2, 1, 1, 1, 1], titulo: "octubre\n 2017"),
            DialogProductividadModel(datos: [1, 3, 1, 1, 1, 1], titulo: "noviembre\n 2017"),
            DialogProductividadModel(datos: [2, 3, 2, 2, 2, 2], titulo: "diciembre\n 2017"),
            DialogProductividadModel(datos: [1, 3, 1, 1, 1, 1], titulo: "enero\n 2017"),
            DialogProductividadModel(datos: [1, 2, 1, 1, 1, 1], titulo: "febrero\n 2017"),
            DialogProductividadModel(datos: [2, 3, 1, 1, 1, 1], titulo: "marzo\n 2017"),
            ]
        let delayTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            callBack(data)
        }
    }
}
