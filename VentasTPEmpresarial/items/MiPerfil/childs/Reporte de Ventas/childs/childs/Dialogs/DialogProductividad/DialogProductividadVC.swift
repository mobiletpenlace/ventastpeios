//
//  DialogForecastVC.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 07/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftCharts

class DialogProductividadVC: BaseDialogCustom {
    @IBOutlet weak var viewRoot: UIView!
    @IBOutlet weak var viewGrafica: UIView!
    @IBOutlet weak var viewDescripcion: UIView!
    fileprivate var chart: Chart?
    
    fileprivate let presenter = DialogProductividadPresenter(service: DialogProductividadService())
    var spinner :UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    private func setUpView() {
        super.setView(view: viewRoot)
    }
    
    @IBAction func clickCancelar(_ sender: UIButton) {
        super.cancel()
    }
}

extension DialogProductividadVC: DialogProductividadDelegate, BaseTreeLoadingDelegate, CustomChartDelegate{
    
    func updateTextoLoading(texto: String) {
        
    }
    
    func reloadData() {
        var datos = [ChartItem]()
        for i in 0 ..< presenter.getSize(){
            let v = presenter.getItemIndex(index: i)
            datos.append(ChartItem(datos: v.datos, titulo: v.titulo))
        }
        chart = getChart(viewContainer: viewGrafica, datos: datos, chart: chart)
        let titulos = [
            "Necesidades",
            "Diseño",
            "Firma",
            "Crédito",
            "Propuesta",
            "Negociación"
        ]
        addDescripcion(container: viewDescripcion, titulos)
        //viewDescripcion
    }
    
    func updateTitleHeader() {
        
    }
    
    func startLoading() {
        spinner = showLoading(onView: viewGrafica)
    }
    
    func finishLoading() {
        if let loadview = spinner{
            hideLoading(spinner: loadview)
        }
    }
}

