//
//  DialogVentaPendientePresenter.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 10/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

protocol DlgVtsProcesoInstalacionProcesoDelegate: BaseDelegate {
    func reloadTable()
    func reloadRows(indexPath: [IndexPath])
}

class DlgVtsProcesoInstalacionProcesoPresenter{
    weak fileprivate var view: DlgVtsProcesoInstalacionProcesoDelegate?
    fileprivate let service: DlgVtsProcesoInstalacionProcesoService
    fileprivate var dataList = [DlgVtsProcesoInstalacionProcesoModel]()
    
    init(service: DlgVtsProcesoInstalacionProcesoService){
        self.service = service
    }

    func attachView(view: DlgVtsProcesoInstalacionProcesoDelegate){
        self.view = view
        actualizarDatos()
    }
    
    func viewDidAppear(){
    }

    func detachView() {
        view = nil
    }
    
    func getItemIndex(index: Int) -> DlgVtsProcesoInstalacionProcesoModel {
        return dataList[index]
    }
    
    func getSize() -> Int{
        return dataList.count
    }
    
    func actualizarDatos(){
        self.view?.startLoading()
        service.getData(){[weak self] data in
            self?.onFinishGetData(data: data)
        }
    }
    
    func onFinishGetData( data: [DlgVtsProcesoInstalacionProcesoModel]){
        view?.finishLoading()
        if(data.count == 0){
            dataList = [DlgVtsProcesoInstalacionProcesoModel]()
            view?.reloadTable()
        }else{
            dataList = data
            view?.reloadTable()
        }
    }
}
