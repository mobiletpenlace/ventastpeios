//
//  DialogForecastPresenter.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 12/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

protocol DialogProductividadDelegate: BaseDelegate {
    func reloadData()
}

class DialogProductividadPresenter{
    weak fileprivate var view: DialogProductividadDelegate?
    fileprivate let service: DialogProductividadService
    fileprivate var dataList = [DialogProductividadModel]()
    
    init(service: DialogProductividadService){
        self.service = service
    }
    
    func attachView(view: DialogProductividadDelegate){
        self.view = view
        actualizarDatos()
    }
    
    func viewDidAppear(){
    }
    
    func detachView() {
        view = nil
    }
    
    func getItemIndex(index: Int) -> DialogProductividadModel {
        return dataList[index]
    }
    
    func getSize() -> Int{
        return dataList.count
    }
    
    func actualizarDatos(){
        self.view?.startLoading()
        service.getData(){[weak self] data in
            self?.onFinishGetData(data: data)
        }
    }
    
    func onFinishGetData( data: [DialogProductividadModel]){
        view?.finishLoading()
        if(data.count == 0){
            dataList = [DialogProductividadModel]()
            view?.reloadData()
        }else{
            dataList = data
            view?.reloadData()
        }
    }
}
