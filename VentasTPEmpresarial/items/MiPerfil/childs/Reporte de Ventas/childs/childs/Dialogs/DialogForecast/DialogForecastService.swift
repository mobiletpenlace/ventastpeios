//
//  DialogForecastService.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 12/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
class DialogForecastService{
    func getData(_ callBack:@escaping ([DialogForecastModel]) -> Void){
        let data = [
            DialogForecastModel(datos: [14, 1, 0, 0], titulo: "marzo\n 2017"),
            DialogForecastModel(datos: [5, 5, 0, 0], titulo: "abril\n 2017"),
            DialogForecastModel(datos: [5, 0, 0, 0], titulo: "mayo\n 2017"),
            DialogForecastModel(datos: [0, 3, 0, 0], titulo: "junio\n 2017"),
            DialogForecastModel(datos: [5, 3, 0, 0], titulo: "julio\n 2017"),
            DialogForecastModel(datos: [2, 2, 4, 1], titulo: "agosto\n 2017"),
            DialogForecastModel(datos: [3, 4, 5, 6], titulo: "septiembre\n 2017"),
            ]
        let delayTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            callBack(data)
        }
    }
}
