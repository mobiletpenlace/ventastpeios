//
//  DialogForecastPresenter.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 12/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

protocol DialogfunnelDelegate: BaseDelegate {
    func reloadData()
}

class DialogfunnelPresenter{
    weak fileprivate var view: DialogfunnelDelegate?
    fileprivate let service: DialogfunnelService
    fileprivate var dataList = [DialogfunnelModel]()
    
    init(service: DialogfunnelService){
        self.service = service
    }
    
    func attachView(view: DialogfunnelDelegate){
        self.view = view
        actualizarDatos()
    }
    
    func viewDidAppear(){
    }
    
    func detachView() {
        view = nil
    }
    
    func getItemIndex(index: Int) -> DialogfunnelModel {
        return dataList[index]
    }
    
    func getSize() -> Int{
        return dataList.count
    }
    
    func actualizarDatos(){
        self.view?.startLoading()
        service.getData(){[weak self] data in
            self?.onFinishGetData(data: data)
        }
    }
    
    func onFinishGetData( data: [DialogfunnelModel]){
        view?.finishLoading()
        if(data.count == 0){
            dataList = [DialogfunnelModel]()
            view?.reloadData()
        }else{
            dataList = data
            view?.reloadData()
        }
    }
}
