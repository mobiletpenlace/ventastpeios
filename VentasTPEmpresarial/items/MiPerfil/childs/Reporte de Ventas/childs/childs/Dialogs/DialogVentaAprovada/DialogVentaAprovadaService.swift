//
//  DialogVentaPendienteService.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 10/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

class DialogVentaAprovadaService {
    
    func getData(_ callBack:@escaping ([DialogVentaAprovadaModel]) -> Void){
        let data = [
            DialogVentaAprovadaModel(titulo: "Número de Contrato RE5253577238"),
            DialogVentaAprovadaModel(titulo: "Número de Contrato RE5253577239"),
            DialogVentaAprovadaModel(titulo: "Número de Contrato RE5253534231"),
            DialogVentaAprovadaModel(titulo: "Número de Contrato RE5253571133"),
            DialogVentaAprovadaModel(titulo: "Número de Contrato RE5253577232"),
            DialogVentaAprovadaModel(titulo: "Número de Contrato RE5253577234"),
            DialogVentaAprovadaModel(titulo: "Número de Contrato RE5253577235"),
            ]
        let delayTime = DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            callBack(data)
        }
    }
}
