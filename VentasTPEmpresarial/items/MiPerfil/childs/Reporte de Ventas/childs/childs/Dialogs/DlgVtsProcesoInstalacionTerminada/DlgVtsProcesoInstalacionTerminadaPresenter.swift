//
//  DialogVentaPendientePresenter.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 10/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

protocol DlgVtsProcesoInstalacionTerminadaDelegate: BaseDelegate {
    func reloadTable()
    func reloadRows(indexPath: [IndexPath])
}

class DlgVtsProcesoInstalacionTerminadaPresenter{
    weak fileprivate var view: DlgVtsProcesoInstalacionTerminadaDelegate?
    fileprivate let service: DlgVtsProcesoInstalacionTerminadaService
    fileprivate var dataList = [DlgVtsProcesoInstalacionTerminadaModel]()
    
    init(service: DlgVtsProcesoInstalacionTerminadaService){
        self.service = service
    }

    func attachView(view: DlgVtsProcesoInstalacionTerminadaDelegate){
        self.view = view
        actualizarDatos()
    }
    
    func viewDidAppear(){
    }

    func detachView() {
        view = nil
    }
    
    func getItemIndex(index: Int) -> DlgVtsProcesoInstalacionTerminadaModel {
        return dataList[index]
    }
    
    func getSize() -> Int{
        return dataList.count
    }
    
    func actualizarDatos(){
        self.view?.startLoading()
        service.getData(){[weak self] data in
            self?.onFinishGetData(data: data)
        }
    }
    
    func onFinishGetData( data: [DlgVtsProcesoInstalacionTerminadaModel]){
        view?.finishLoading()
        if(data.count == 0){
            dataList = [DlgVtsProcesoInstalacionTerminadaModel]()
            view?.reloadTable()
        }else{
            dataList = data
            view?.reloadTable()
        }
    }
}
