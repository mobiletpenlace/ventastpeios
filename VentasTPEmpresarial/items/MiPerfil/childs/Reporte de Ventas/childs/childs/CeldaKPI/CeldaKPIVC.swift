//
//  CeldaKPIViewController.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 14/06/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class CeldaKPIVC: BaseItemVC {
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var celda1: UIView!
    var texto: String
    
    override func viewDidLoad() {
        super.viewDidLoad()
        label1.text = texto
    }
    
    init( texto: String) {
        self.texto = texto
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
