//
//  ReporteVentasModel.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 10/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import RealmSwift
import Alamofire

class ReporteVentasService: BaseDBManagerDelegate, BaseFormatDate {
    var realm: Realm!
    var serverManager: ServerDataManager?
    let jsonEncoder = JSONEncoder()
    let jsonDecoder = JSONDecoder()
    var dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()
        serverManager = ServerDataManager()
        realm = getRealm()
    }
    
    func getProgressData(_ callBack:@escaping ([ReporteVentasModel]) -> Void){
        var progressData = [
            ReporteVentasModel(titulo: "Aprobadas", progress: 0, MaxValue: 0),//mesa de control
            ReporteVentasModel(titulo: "Pendientes", progress: 0, MaxValue: 0),//mesa de control
            ReporteVentasModel(titulo: "Rechazadas", progress: 0, MaxValue: 0),//mesa de control
            ReporteVentasModel(titulo: "En proceso", progress: 0, MaxValue: 0),//proceso instalacion
            ReporteVentasModel(titulo: "Terminadas", progress: 0, MaxValue: 0),//proceso instalacion
            ReporteVentasModel(titulo: "Aprobadas", progress: 0, MaxValue: 0),//factibilidad
            ReporteVentasModel(titulo: "Aprobadas", progress: 0, MaxValue: 0),//en costeo
        ]
        
        getMesa() { data in
            
            guard let data = data else {
                Logger.e("no hay data en Reporte de ventas...")
                callBack(progressData)
                return
            }
            var count: Int = 0
            for param in data.listaParamChart! {
                count += Int(param.data ?? "0") ?? 0
            }
            
            for indicador in data.listaParamChart! {
                switch indicador.alias {
                case "Perdida":
                    let progress = Int(indicador.data ?? "0") ?? 0
                    progressData[2] = ReporteVentasModel(titulo: "Rechazadas", progress: progress, MaxValue: count)
                case "Ganada":
                    let progress = Int(indicador.data ?? "0") ?? 0
                    progressData[0] = ReporteVentasModel(titulo: "Aprobadas", progress: progress, MaxValue: count)
                case "Crédito":
                    let progress = Int(indicador.data ?? "0") ?? 0
                    progressData[1] = ReporteVentasModel(titulo: "Pendientes", progress: progress, MaxValue: count)
                default:
                    Logger.w("Reporte de ventas opción defaul.")
                }
            }
            
            callBack(progressData)
        }
    }
    
    func getMesa(_ callBack:@escaping (MiddleGraficaMesaControlResponse?) -> Void) {
        let fechaFinal: Date = Date()
        let fechaInicial = incrementDate(fechaFinal, value: -60, byAdding: .day)
        let fechaFinalString = formatDate(fechaFinal, format: Constantes.Format.formatoFechaServer)
        let fechaInicialString = formatDate(fechaInicial, format: Constantes.Format.formatoFechaServer)
//        var dt: Date = Date()
//        dt = Calendar.current.date(byAdding: Calendar.Component.day, value: 1, to: dt)!
//        let formatter = DateFormatter()
//        formatter.dateFormat = Constantes.Format.formatoFecha
//        var fechaFin = "\(formatter.string(from: dt))T"
//        formatter.dateFormat = Constantes.Format.formatoHora
//        fechaFin = "\(fechaFin)\(formatter.string(from: dt))Z"
//        var fechaInicio = ""
//        formatter.dateFormat = "MM"
//        if(Int(formatter.string(from: dt))! < 2){
//            formatter.dateFormat = "yyyy"
//            let año = Int(formatter.string(from: dt))! - 1
//            fechaInicio = "\(año)-12-"
//        }else{
//            formatter.dateFormat = "yyyy"
//            fechaInicio = "\(formatter.string(from: dt))-"
//            formatter.dateFormat = "MM"
//            let mes = Int(formatter.string(from: dt))! - 1
//            fechaInicio = "\(fechaInicio)\(mes)-"
//        }
//        formatter.dateFormat = "dd"
//        fechaInicio = "\(fechaInicio)\(formatter.string(from: dt))T"
//        formatter.dateFormat = Constantes.Format.formatoHora
//        fechaInicio = "\(fechaInicio)\(formatter.string(from: dt))Z"
        
        let id = dbManager.getConfig().idEmpleado
        var res: MiddleGraficaMesaControlResponse?
        let model = MiddleGraficaMesaControlRequest(idEmpleado: id, fechaInicio: fechaInicialString, FechaFin: fechaFinalString)
        
        serverManager?.post(model: model, url: Constantes.Url.Middleware.Qa.graficasMesaControl, tipo: .middle) { responseData in
            
            guard let data = responseData.data else {
                Logger.e("no hay data en graficas de mesa de control...")
                callBack(res)
                return
            }
            do {
                let decoder = JSONDecoder()
                let responseOBJ = try decoder.decode(MiddleGraficaMesaControlResponse.self, from: data)
                if responseOBJ.response?.result == "0" {
                    res = responseOBJ
                } else {
                    res = nil
                }
                callBack(res)
            } catch let err {
                Logger.e("error: --> \(err)")
                callBack(nil)
            }
        }
    }
}
