//
//  Item1ViewController.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 06/06/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SnapKit

class ReporteVentasVC: BaseItemVC {
    @IBOutlet weak var viewAgenda: UIView!
    //@IBOutlet weak var cellFactibilidadAprovada: UIView!
    //@IBOutlet weak var cellEnCosteoAprobada: UIView!
    @IBOutlet weak var cellMesaControlAprovada: UIView!
    @IBOutlet weak var cellMesaControlPendiente: UIView!
    @IBOutlet weak var cellMesaControlRechazada: UIView!
    //@IBOutlet weak var cellKPIFunnel: UIView!
    //@IBOutlet weak var cellKPIForecast: UIView!
    /*@IBOutlet weak var cellKPIHitRate: UIView!
    @IBOutlet weak var cellProcesoInstalacionEnProceso: UIView!
    @IBOutlet weak var cellProcesoInstalacionTerminada: UIView!*/
    @IBOutlet weak var progressBarMeta: UIProgressView!
    @IBOutlet weak var viewContainerProgressBar: UIView!
    @IBOutlet weak var viewGraphics: UIView!
    @IBOutlet weak var labelProgress1: UILabel!
    @IBOutlet weak var labelProgress2: UILabel!
    @IBOutlet weak var labelProgress3: UILabel!
    @IBOutlet weak var mViewMeta: UIView!
    //@IBOutlet weak var mViewVentaFactCos: UIView!
    //@IBOutlet weak var mStackPInstaladas: UIStackView!
    
    
    fileprivate let presenter = ReporteVentasPresenter(model: ReporteVentasService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow(){
        super.viewDidShow()
        presenter.viewDidShow()
    }
    
    private func setUpView() {
        progressBarMeta.layer.cornerRadius = 5
        progressBarMeta.clipsToBounds = true
        progressBarMeta.layer.borderWidth = 1
        progressBarMeta.layer.borderColor = UIColor.lightGray.cgColor
        super.addShadowView(view: cellMesaControlAprovada)
        super.addShadowView(view: cellMesaControlPendiente)
        super.addShadowView(view: cellMesaControlRechazada)
        /*super.addShadowView(view: cellProcesoInstalacionEnProceso)
        super.addShadowView(view: cellProcesoInstalacionTerminada)*/
        //super.addShadowView(view: cellFactibilidadAprovada)
        //super.addShadowView(view: cellKPIFunnel)
        
        //super.addShadowView(view: cellKPIHitRate)
        //super.addShadowView(view: cellEnCosteoAprobada)
        super.addShadowView(view: viewAgenda)
        super.addViewXIB(vc: AgendaWidgetVC(), container: viewAgenda)
        self.mViewMeta.isHidden = true
        //self.mViewVentaFactCos.isHidden = true
        
        
    }
    
    private func addButton(mView: UIView, selector: Selector){
        let button = UIButton()
        button.backgroundColor = .clear
        button.setTitle("", for: .normal)
        button.addTarget(self, action: selector, for: .touchUpInside)
        mView.addSubview(button)
        button.snp.makeConstraints { (make) -> Void in
            make.edges.equalTo(mView)
        }
    }
    
    private func updateProgressCell(pos: Int, container: UIView, selector: Selector){
        let data = presenter.getItemIndex(index: pos)
        let controller = CeldaProgressVC(texto: data.titulo, progress: data.progress, maxVal: data.MaxValue)
        super.addViewXIB(vc: controller, container: container)
        addButton(mView: container, selector: selector)
    }
    
    private func addKPICell(title: String, container: UIView, selector: Selector){
        let controller = CeldaKPIVC(texto: title)
        super.addViewXIB(vc: controller, container: container)
        addButton(mView: container, selector: selector)
    }
    
    private func animateAndShowDialog(sender: UIButton, customAlert: BaseDialogCustom?){
        if let contenedor = sender.superview{
            sender.animateBound(view: contenedor){[weak self] _ in
                self?.showDialogAlert(customAlert)
            }
        }
    }
    
    private func showDialogAlert(_ customAlert: BaseDialogCustom?){
        if let dialog = customAlert {
            super.showDialog(customAlert: dialog)
        }
    }
    
    /*@IBAction func clickEnFactibilidadAprovada(_ sender: UIButton) {
        animateAndShowDialog(sender: sender, customAlert: nil)
    }
    
    @IBAction func clickEnCosteoAprovada(_ sender: UIButton) {
        animateAndShowDialog(sender: sender, customAlert: nil)
    }*/
    
    @IBAction func clickMesaControlAprovada(_ sender: UIButton) {
        //animateAndShowDialog(sender: sender, customAlert: DialogVentaAprovadaVC())
    }
    
    @IBAction func clickMesaControlPendiente(_ sender: UIButton) {
        //animateAndShowDialog(sender: sender, customAlert: DialogVentaPendienteVC())
    }
    
    @IBAction func clickMesaControlRechazada(_ sender: UIButton) {
        //animateAndShowDialog(sender: sender, customAlert: nil)
    }
    
    
    /*@IBAction func clickKPIVentaFunnel(_ sender: UIButton) {
        animateAndShowDialog(sender: sender, customAlert: DialogfunnelVC())
    }
    
    @IBAction func clickKPIVentaForecast(_ sender: UIButton) {
        animateAndShowDialog(sender: sender, customAlert: DialogForecastVC())
    }*/
    
    @IBAction func clickKPIVentaProductividad(_ sender: UIButton) {
        animateAndShowDialog(sender: sender, customAlert: DialogProductividadVC())
    }
    
    /*@IBAction func clickKPIVentaHitRate(_ sender: UIButton) {
        animateAndShowDialog(sender: sender, customAlert: nil)
    }
    
    
    @IBAction func clickProcesoInstalacionEnProceso(_ sender: UIButton) {
        animateAndShowDialog(sender: sender, customAlert: DlgVtsProcesoInstalacionProcesoVC())
    }
    
    @IBAction func clickProcesoInstalacionTerminada(_ sender: UIButton) {
        animateAndShowDialog(sender: sender, customAlert: DlgVtsProcesoInstalacionTerminadaVC())
    }*/
    
}

extension ReporteVentasVC: ReporteVentasDelegate{
    func updateTitleHeader(){
        let headerData = HeaderData(
            imagen: #imageLiteral(resourceName: "logo_MisVentas"),
            titulo: "Mi Perfil",
            titulo2: nil,
            titulo3: nil,
            subtitulo: nil,
            closure: nil
        )
        super.updateTitle(headerData: headerData)
    }
    
    func animateProgressBar(valor: Float, label1:String, label2:String, label3:String){
        DispatchQueue.main.async {
            self.progressBarMeta.setProgress(0.0, animated: true)
            UIView.animate(withDuration: 2.0) {
                self.progressBarMeta.setProgress(valor, animated: true)
            }
        }
        labelProgress1.text = label1
        labelProgress2.text = label2
        labelProgress3.text = label3
    }
    
    func startLoading() {
        super.showLoading(view: viewGraphics)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
    func actualizarProgreso(){
        //updateProgressCell(pos: 5, container: cellFactibilidadAprovada, selector: #selector(clickEnFactibilidadAprovada))
        //updateProgressCell(pos: 6, container: cellEnCosteoAprobada, selector: #selector(clickEnCosteoAprovada))
        
        updateProgressCell(pos: 0, container: cellMesaControlAprovada, selector: #selector(clickMesaControlAprovada))
        updateProgressCell(pos: 1, container: cellMesaControlPendiente, selector: #selector(clickMesaControlPendiente))
        updateProgressCell(pos: 2, container: cellMesaControlRechazada, selector: #selector(clickMesaControlRechazada))
        
        /*updateProgressCell(pos: 3, container: cellProcesoInstalacionEnProceso, selector: #selector(clickProcesoInstalacionEnProceso))
        updateProgressCell(pos: 4, container: cellProcesoInstalacionTerminada, selector: #selector(clickProcesoInstalacionTerminada))*/
        
        //addKPICell(title: "Funnel", container: cellKPIFunnel, selector: #selector(clickKPIVentaFunnel))
        //addKPICell(title: "Forecast", container: cellKPIForecast, selector: #selector(clickKPIVentaForecast))
        //addKPICell(title: "Hit Rate", container: cellKPIHitRate, selector: #selector(clickKPIVentaHitRate))
    }
}
