//
//  ReporteVentasModel.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 10/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct ReporteVentasModel {
    var titulo: String
    var progress: Int
    var MaxValue: Int
}
