//
//  ReporteVentasPre.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 10/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

protocol ReporteVentasDelegate: BaseDelegate {
    func animateProgressBar(valor: Float, label1:String, label2:String, label3:String)
    func actualizarProgreso()
}

class ReporteVentasPresenter{
    fileprivate let model: ReporteVentasService
    weak fileprivate var view: ReporteVentasDelegate?
    fileprivate var dataList = [ReporteVentasModel]()
    
    init(model: ReporteVentasService){
        self.model = model
    }
    
    func attachView(view: ReporteVentasDelegate){
        self.view = view
    }
    
    func viewDidAppear(){
        
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
        getProgressData()
    }
    
    func detachView() {
        view = nil
    }
    
    func getItemIndex(index: Int) -> ReporteVentasModel {
        return dataList[index]
    }
    
    func getProgressData(){
        self.view?.startLoading()
        model.getProgressData{[weak self] data in
            self?.view?.finishLoading()
            if(data.count == 0){
                self?.dataList = [ReporteVentasModel]()
            }else{
                self?.dataList = data
            }
            self?.view?.actualizarProgreso()
            self?.view?.animateProgressBar(valor: 0.8, label1: "$0", label2: "$400,000", label3: "$900,000")
        }
    }
}
