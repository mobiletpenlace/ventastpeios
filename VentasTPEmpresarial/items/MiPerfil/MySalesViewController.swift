//
//  MySalesViewController.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 01/06/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class MisVentasVC: BaseMenuItemVC {
    @IBOutlet weak var viewContent: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.subscribe(nameItem: Constantes.EVENTBUS_MENU_ITEM_MIS_VENTAS_ID)
        super.headerData = HeaderData(
            imagen: #imageLiteral(resourceName: "logo_MisVentas"),
            titulo: "Mis Ventas",
            subtitulo: nil,
            closure: nil
        )
        addSubView()
    }
    
    func addSubView() -> Void {
        var viewControllers2 = [BaseItemVC]()
        viewControllers2.append(ReporteVentasVC())
        viewControllers2.append(ComisionesVC())
        super.addViewXIBList(view: viewContent, viewControllers: viewControllers2)
    }
   
    @IBAction func selectedView(_ sender: CustomUISegmentedControl) {
        super.cambiarVista(index: sender.selectedSegmentIndex)
        sender.changeUnderlinePosition(sender.currentIndex)
    }
}
