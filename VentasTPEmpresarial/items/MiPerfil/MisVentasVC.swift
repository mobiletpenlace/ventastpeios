//
//  MySalesViewController.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 01/06/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class MisVentasVC: BaseMenuItemVC, TimeMeasureDelegate {
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var segmentControl: CustomUISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tiempoInicio = iniciarTiempo()
        
        super.subscribe(nameItem: Constantes.Eventbus.Id.itemMisVentas)
        super.headerData = HeaderData(
            imagen: #imageLiteral(resourceName: "logo_MisVentas"),
            titulo: "Mi Perfil",
            titulo2: nil,
            titulo3: nil,
            subtitulo: nil,
            closure: nil
        )
        addSubView()
        medirTiempo(tipoInicio: tiempoInicio, nombre: "Mi Perfil")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        segmentControl.changeUnderlinePosition(0)
    }
    
    func addSubView() -> Void {
        var viewControllers2 = [BaseItemVC]()
        viewControllers2.append(ReporteVentasVC())
//        viewControllers2.append(VisualizaVentasVC())
 //        viewControllers2.append(ComisionesVC())
        super.addViewXIBList(view: viewContent, viewControllers: viewControllers2)
    }
   
    @IBAction func selectedView(_ sender: CustomUISegmentedControl) {
        super.cambiarVista(index: sender.selectedSegmentIndex)
        sender.changeUnderlinePosition(sender.currentIndex)
    }
}
