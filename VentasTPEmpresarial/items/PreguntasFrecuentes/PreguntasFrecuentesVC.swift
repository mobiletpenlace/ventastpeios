//
//  FrequentQuestionsViewController.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 01/06/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class PreguntasFrecuentesVC: BaseMenuItemVC, TimeMeasureDelegate {
    
    @IBOutlet weak var viewContainer: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tiempoInicio = iniciarTiempo()
        super.subscribe(nameItem: Constantes.Eventbus.Id.menuItemPreguntasFrecuentes)
        super.headerData = HeaderData(
            imagen: #imageLiteral(resourceName: "logo_GenerarLead"),
            titulo: "Preguntas frecuentes",
            titulo2: nil,
            titulo3: nil,
            subtitulo: nil,
            closure: nil
        )
        medirTiempo(tipoInicio: tiempoInicio, nombre: "Preguntas frecuentes")
    }
    
    
    
    
}
    
    

