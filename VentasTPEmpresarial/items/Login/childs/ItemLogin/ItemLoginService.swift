import RealmSwift

class ItemLoginService: BaseDBManagerDelegate {
    var realm: Realm!
    var serverManager: ServerDataManager?
    var dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()
        serverManager = ServerDataManager()
        realm = getRealm()
        
        UserDefaults.standard.setValue(false, forKey: "_UIConstraintBasedLayoutLogUnsatisfiable")
    }

    func getLastDate() -> Date {
        let fecha = dbManager.getUltimaHoraDescarga()
        return fecha
    }

    func InsertaHora(_ fecha: Date) {
        dbManager.insertarUltimaHoraDescarga(fecha)
    }

    func loginUser(noEmpleado: String, password: String, _ callBack: @escaping (MiddleLoginResponse?) -> Void) {
        let model = MiddleLoginRequest(noEmpleado: noEmpleado, password: password)
        
        serverManager?.getData(model: model, response: MiddleLoginResponse.self, url: Constantes.Url.Middleware.Qa.login, .middle) { response in
            callBack(response)
        }
//        serverManager?.post(model: model, url: Constantes.Url.Middleware.Qa.login, tipo: .middle) { responseData in
//            guard let data = responseData.data else {
//                Logger.e("No sirve el servicio de login...")
//                callBack(nil)
//                return
//            }
//            do {
//                let decoder = JSONDecoder()
//                let response = try decoder.decode(MiddleLoginResponse.self, from: data)
//                callBack(response)
//            } catch let err {
//                Logger.e(err)
//                callBack(nil)
//            }
//        }
    }

    func getToken(_ callBack: @escaping ((Bool, String)) -> Void) {
        var token: String = ""
        let model = "grant_type=password&client_id=3MVG9pHRjzOBdkd_3vq.kUaru1ixdCQ6zecb_57WRY9LdgGYBwp5yMjdW.vbzYjsbh6ilQo8HcBM5By1Fwyof&client_secret=1091014965615192457&username=arturoceron93@gmail.com.qa&password=sistemas10"
        serverManager?.post(model: model, url: Constantes.Url.SalesFor.creaToken, tipo: .form) { responseData in
            guard let data = responseData.data else {
                callBack((false, ""))
                return
            }
            do {
                let decoder = JSONDecoder()
                let res = try decoder.decode(TokenResponse.self, from: data)
                token = res.accessToken ?? ""
                callBack((true, token))
            } catch let err {
                Logger.e(err)
                callBack((false, ""))
            }
        }
    }

    func actualizarIdEmpleado(_ id: String) {
        dbManager.actualizarIdEmpleado(id)
    }

    func actualizarNombreEmpleado(_ nombreEmpleado: String) {
        dbManager.actualizarNombreEmpleado(nombreEmpleado)
    }

    func ActualizarFamilias(_ callBack: @escaping (Bool) -> Void) {
        var res = false
        
//        let model = SFFamiliasRequest()
//        let url = Constantes.Url.SalesFor.familias
//        let tipo: TipoRequest = .sf
//        let decodeClass = SFFamiliasResponse.self
        
        let model = MiddleFamiliasRequest()
        let url = Constantes.Url.Middleware.Qa.familias
        let tipo: TipoRequest = .middle
        let decodeClass = MiddleFamiliasResponse.self
        
        serverManager?.post(model: model, url: url, tipo: tipo) { [weak self] responseData in
            do {
                guard let data = responseData.data else {
                    Logger.e("No sirve el servicio de consulta familias...")
                    callBack(res)
                    return
                }
                let decoder = JSONDecoder()
                var familiasNuevas = Set<String>()
                let responseOBJ = try decoder.decode(decodeClass, from: data)
                if let familias = responseOBJ.listaFamilia {
                    for familia in familias {
                        self?.dbManager.addFamilia(
                            nombre: familia.name ?? "",
                            id: familia.id ?? "",
                            tipo: familia.tipo ?? ""
                        )
                        familiasNuevas.insert(familia.id ?? "")
                    }
                }
                self?.dbManager.borraFamilias(set: familiasNuevas)

                res = true
            } catch let err {
                Logger.e("error: --> \(err)")
            }
            callBack(res)
        }
    }

    func actualizarPlanes(idFamilia: String, _ callBack: @escaping (Bool) -> Void) {
        var res = false
        let ciudades = ["Todas"]
        
//        let model = SFPlanesRequest(valorFamilia: ciudades, idFamilia: idFamilia)
//        let url = Constantes.Url.SalesFor.planes
//        let tipo: TipoRequest = .sf
//        let decodeClass = SFPlanesResponse.self
        
        let model = MiddlePlanesRequest(valorFamilia: ciudades, idFamilia: idFamilia)
        let url = Constantes.Url.Middleware.Qa.planes
        let tipo: TipoRequest = .middle
        let decodeClass = MiddlePlanesResponse.self
        
        serverManager?.post(model: model, url: url, tipo: tipo) { [weak self] responseData in
            do {
                guard let data = responseData.data else {
                    Logger.e("No sirve el servicio de consulta planes de familia: \(idFamilia)...")
                    callBack(res)
                    return
                }
                let decoder = JSONDecoder()
                var planesNuevos = Set<String>()
//                let responseOBJ = try decoder.decode(SFPlanesResponse.self, from: data)
                let responseOBJ = try decoder.decode(decodeClass, from: data)
                if let planes = responseOBJ.listaPlanes {
                    for plan in planes {
                        if plan.id != nil {
                            self?.dbManager.addPlan(
                                nombrePlan: plan.name ?? "",
                                idPlan: plan.id ?? "",
                                precioLista: plan.precioListaSinIVA ?? 0.0,
                                precioListaIVA: plan.precioDeLista ?? 0.0,
                                precioProntoPago: plan.precioProntoPagoSinIVa ?? 0.0,
                                precioProntoPagoIVA: plan.precioProntoPago ?? 0.0,
                                costoInstalacion: plan.costoInstalacion ?? 0.0,
                                idFamilia: idFamilia
                            )

                            planesNuevos.insert(plan.id ?? "")
                            Logger.i("Insertado el plan: \( plan.name ?? "" )")

                            let respPlazos = plan.plazos ?? ""
                            let split = respPlazos.split(separator: ";")
                            var plazos = [String]()
                            for v in split {
                                plazos.append(v.description)
                            }
                            self?.dbManager.agregarPlazosPlan(
                                planId: plan.id!,
                                plazos: plazos
                            )
                        }
                    }
                }
                self?.dbManager.borraPlanes(set: planesNuevos)

                res = true
            } catch let err {
                Logger.e("error familias \(idFamilia): --> \(err)")
            }
            callBack(res)
        }
    }

    func getFamiliasIds() -> [String] {
        var ids = [String]()
        let familias = dbManager.obtenerFamilias()
        for familia in familias {
            ids.append(familia.id!)
        }
        return ids
    }

    func getPlanesIds() -> [String] {
        var ids = [String]()
        let planes = dbManager.obtenerPlanes()
        for plan in planes {
            ids.append(plan.id!)
        }
        return ids
    }

    func getProductosAdicionalesIds() -> [String] {
        var ids = [String]()
        let prodAdicionales = dbManager.obtenerProductosAdicionales()
        for prodAdic in prodAdicionales {
            ids.append(prodAdic.id!)
        }
        return ids
    }

    func actualizarDetallePlanes(idPlan: String, _ callBack: @escaping (Bool) -> Void) {
        var res = false
        
//        let model = SFDetallePlanRequest(idPlan: idPlan)
//        let url = Constantes.Url.SalesFor.detallePlan
//        let tipo: TipoRequest = .sf
//        let decodeClass = SFDetallePlanResponse.self
        
        let model = MiddleDetallePlanRequest(idPlan: idPlan)
        let url = Constantes.Url.Middleware.Qa.detallePlan
        let tipo: TipoRequest = .middle
        let decodeClass = MiddleDetallePlanResponse.self
        
        serverManager?.post(model: model, url: url, tipo: tipo) { responseData in

            guard let data = responseData.data else {
                Logger.e("No sirve el servicio de consulta detalle plan...")
                callBack(res)
                return
            }

            var datos = [[String]]()
            var titulo = ""
            if let plan = self.dbManager.buscarPlanPorID(id: idPlan) {
                let plazos = plan.plazos
                let size = plazos.count
                for (pos, plazo) in plazos.enumerated() {
                    titulo += (plazo.nombre == "Sin plazo") ? "0" : plazo.nombre
                    if pos < size - 2 {
                        titulo += ", "
                    } else if pos < size - 1 {
                        titulo += " y "
                    }
                }
                datos.append([titulo, "Plazos disponibles de contratación"])
            }

            do {
                let decoder = JSONDecoder()
                let responseOBJ = try decoder.decode(decodeClass, from: data)
                if let equipoAsignado = responseOBJ.objectResponse?.equpoAsignado?.first {
                    let titulo = equipoAsignado.nombre ?? ""
                    let texto = "Equipo asignado"
                    datos.append([titulo, texto])
                }
                if let listaProd = responseOBJ.objectResponse?.listaProductos {
                    for prod in listaProd {
                        if prod.megasBase! {
                            let titulo = "\(prod.velocidadBajada!)"
                            let texto = "Internet de down (hasta)"
                            datos.append([titulo, texto])

                            let titulo2 = "\(prod.velocidadSubida!)"
                            let texto2 = "Internet de up (hasta)"
                            datos.append([titulo2, texto2])
                            break
                        }
                    }
                }

                if let valoresAgregados = responseOBJ.objectResponse?.valoresAgregados {
                    for valor in valoresAgregados {
                        let nombre = valor.name ?? ""
                        if let index = nombre.range(of: " ") {
                            let titulo = nombre[..<index.lowerBound].description
                            let texto = nombre[index.upperBound...].description
                            datos.append([titulo, texto])
                        }
                    }
                }
                self.agregarIncluido(
                    id: idPlan,
                    datos: datos
                )

                if let plan = self.dbManager.buscarPlanPorID(id: idPlan) {
                    if let listaServicios = responseOBJ.objectResponse?.listaServicios {
                        var prodAdicNuevos = Set<String>()
                        for servicio in listaServicios {
                            let servicioDB = DBProductoAdicional(
                                nombre: servicio.name!,
                                precio: servicio.precioDeLista!,
                                id: servicio.id!
                            )
                            try! self.realm.write {
                                self.realm.add(servicioDB, update: true)
                                plan.productosAdicionales.append(servicioDB)
                            }
                            prodAdicNuevos.insert(servicio.id ?? "")
                        }
                        self.dbManager.borraProductoAdicional(set: prodAdicNuevos)
                    }
                }

                res = true
            } catch let err {
                Logger.e("error: \(err)")
            }
            callBack(res)
        }
    }

    func actualizarServiciosAdicionales(idPlan: String, _ callBack: @escaping (Bool) -> Void) {
        var res = false
        
//        let model = SFServiciosAdicionalesRequest(idPlan: idPlan)
//        let url = Constantes.Url.SalesFor.serviciosAdicionales
//        let tipo: TipoRequest = .sf
//        let decodeClass = SFServiciosAdicionalesResponse.self
        
        let model = MiddleServiciosAdicionalesRequest(idPlan: idPlan)
        let url = Constantes.Url.Middleware.Qa.serviciosAdicionales
        let tipo: TipoRequest = .middle
        let decodeClass = MiddleServiciosAdicionalesResponse.self
        
        serverManager?.post(model: model, url: url, tipo: tipo) { [weak self] responseData in
            guard let data = responseData.data else {
                Logger.e("No sirve el servicio de consulta servicios adicionales...")
                callBack(res)
                return
            }
            do {
                let decoder = JSONDecoder()
                let responseOBJ = try decoder.decode(decodeClass, from: data)
                if let listaAddonServicios = responseOBJ.listaAddonServicios {
                    var servAdicNuevos = Set<String>()
                    for addonServicios in listaAddonServicios {
                        self?.dbManager.agregarServicioAdicionalPlan(
                            idPlan,
                            nombre: addonServicios.name!,
                            precio: addonServicios.precioLista!,
                            idAddonServicio: addonServicios.id!,
                            maximoAgregar: addonServicios.maximoAgregar!,
                            esSDWAN: addonServicios.esSDWAN!
                        )
                        servAdicNuevos.insert(addonServicios.id ?? "")
                    }
                    self?.dbManager.borraServicioAdicional(set: servAdicNuevos)
                }
                res = true
            } catch let err {
                Logger.e("error: --> \(err)")
            }
            callBack(res)
        }
    }

    func actualizarAddonsProductos(idServicio: String, _ callBack: @escaping (Bool) -> Void) {
        var res = false
        
//        let model = SFProductosAdicionalesAddonsRequest(idServicio: idServicio)
//        let url = Constantes.Url.SalesFor.productosAdicionalesAddons
//        let tipo: TipoRequest = .sf
//        let decodeClass = SFProductosAdicionalesAddonsResponse.self
        
        let model = MiddleProductosAdicionalesAddonsRequest(idServicio: idServicio)
        let url = Constantes.Url.Middleware.Qa.productosAdicionalesAddons
        let tipo: TipoRequest = .middle
        let decodeClass = MiddleProductosAdicionalesAddonsResponse.self
        
        serverManager?.post(model: model, url: url, tipo: tipo) { [weak self] responseData in
            guard let data = responseData.data else {
                Logger.e("No sirve el servicio de consulta addons productos...")
                callBack(res)
                return
            }
            do {
                let decoder = JSONDecoder()
                let responseOBJ = try decoder.decode(decodeClass, from: data)
                if let listaProductos = responseOBJ.listaProductos {
                    var prodAdicAddonsNuevos = Set<String>()
                    for producto in listaProductos {
                        var convIds = [String]()
                        if let convivencia = producto.convivencia {
                            for v in convivencia {
                                convIds.append(v.idProductoExcl ?? "")
                            }
                        }
                        self?.dbManager.agregarAddonProductoAdicional(
                            nombre: producto.name ?? "",
                            precio: producto.precioLista ?? 0.0,
                            id: producto.id ?? "",
                            maximoAgregar: producto.maximoAgregar ?? 0,
                            impuesto: producto.monto_impuesto ?? 0.0,
                            convivenciaIds: convIds,
                            idServicio: idServicio
                        )
                        prodAdicAddonsNuevos.insert(producto.id ?? "")
                    }
                    self?.dbManager.borraProductoAdicionalAddon(set: prodAdicAddonsNuevos)
                }
                res = true
            } catch let err {
                Logger.e("error: --> \(err)")
            }
            callBack(res)
        }
    }

    func actualizarPromociones(idPlan: String, _ callBack: @escaping (Bool) -> Void) {
        var res = false
        
        let model = SFPromocionesRequest(idPlan: idPlan)
        let url = Constantes.Url.SalesFor.promociones
        let tipo: TipoRequest = .sf
        let decodeClass = SFPromocionesResponse.self
        
        serverManager?.post(model: model, url: url, tipo: tipo) { [weak self] responseData in
            guard let data = responseData.data else {
                Logger.e("No sirve el servicio de consulta promociones...")
                callBack(res)
                return
            }
            do {
                let decoder = JSONDecoder()
                let responseOBJ = try decoder.decode(decodeClass, from: data)
                if let promocionesLista = responseOBJ.objectResponse?.promocionesLista {
                    for promocion in promocionesLista {
                        var convIds = [String]()
                        if let convivencia = promocion.convivencia {
                            for v in convivencia {
                                convIds.append(v.idPromocionExcl!)
                            }
                        }
                        self?.dbManager.agregarPromocion(nombre: promocion.name!, id: promocion.id!, descuento: promocion.montoDescuento, fechaInicio: promocion.fechaInicio!, fechaFin: promocion.fechaFin!, convivenciaIds: convIds, idPlan: idPlan, idProducto: promocion.idProducto!, idServicio: promocion.idServicio!)
                    }
                }
                res = true
            } catch let err {
                Logger.e("error: --> \(err)")
            }
            callBack(res)
        }
    }

    func agregarIncluido(id: String, datos: [[String]]) {
        let plan = dbManager.buscarPlanPorID(id: id)
        for dato in datos {
            try! realm.write {
                plan?.productosIncluidos.append(
                    DBProductoIncluido(nombre: dato[0], descripcion: dato[1])
                )
            }
        }
    }

    func addFamilia(nombre: String, id: String, planes: [DBPlan]) {
        let familia = DBFamilia(nombre: nombre, id: id)
        for plan in planes {
            familia.planes.append(plan)
        }
        try! realm.write {
            realm.add(familia, update: true)
        }
    }

    func borraFamiliasYPlanes() {
        dbManager.borraFamiliasYPlanes()
    }

    func actualizarSolucionesAlaMedida(_ callBack: @escaping (Bool) -> Void) {
        guard let familiaSM = dbManager.getFamiliaSolucionesAlaMedia() else {
            Logger.e("SMedida: No se ha podido obtener la familia soluciones a la medida.")
            return
        }
        guard let planSM = dbManager.getPlanSolucionesAlAMedida() else {
            Logger.e("SMedida: No se ha podido obtener el plan soluciones a la medida.")
            return
        }
        let nombrePlan = planSM.nombre ?? ""
        self.dbManager.addPlan(nombrePlan: nombrePlan, idPlan: planSM.id!, precioLista: 0.0, precioListaIVA: 0.0, precioProntoPago: 0.0, precioProntoPagoIVA: 0.0, costoInstalacion: 0.0, idFamilia: familiaSM.id!)
        
        var res = false
        
        let idPlan = planSM.id ?? ""
//        let model = SFSolucionesMedidaRequest(idPlan: idPlan)
//        let url = Constantes.Url.SalesFor.solucionesMedida
//        let tipo: TipoRequest = .sf
//        let decodeClass = SFSolucionesMedidaResponse.self
        
        let model = MiddleSolucionesMedidaRequest(idPlan: idPlan)
        let url = Constantes.Url.Middleware.Qa.solucionesMedida
        let tipo: TipoRequest = .middle
        let decodeClass = MiddleSolucionesMedidaResponse.self
        
        serverManager?.post(model: model, url: url, tipo: tipo) { [weak self] responseData in
            do {
                guard let data = responseData.data else {
                    Logger.e("No sirve el servicio de consulta soluciones medida...")
                    callBack(res)
                    return
                }
                let decoder = JSONDecoder()
                let responseOBJ = try decoder.decode(decodeClass, from: data)
                if let response = responseOBJ.objectResponse {
                    if let productoAdic = response.listaServicios {
                        for servicio in productoAdic {
                            self?.dbManager.agregarProductoAdicional(
                                nombre: servicio.name!,
                                precio: servicio.precioDeLista!,
                                id: servicio.id!,
                                idPlan: planSM.id!
                            )
                        }

                    }
                    if let productoAdicAddons = response.listaProductos {
                        for addon in productoAdicAddons {
                            self?.dbManager.agregarAddonProducto(
                                nombre: addon.name!,
                                precio: addon.precioLista!,
                                id: addon.id!,
                                idProdAdic: addon.dPPlanServicioId!
                            )
                        }
                    }
                }
                res = true
            } catch let err {
                Logger.e("error: --> \(err)")
                res = false
            }
            callBack(res)
        }
    }
}
