//
//  ResponseLoginResponse.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 16/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

class ResponseLoginMiddleware: Codable{
    var result:String?
    var resultId:String?
    var descriptionR:String?
    
    private enum CodingKeys: String, CodingKey {
        case result = "Result"
        case resultId = "ResultId"
        case descriptionR = "Description"
    }
}
