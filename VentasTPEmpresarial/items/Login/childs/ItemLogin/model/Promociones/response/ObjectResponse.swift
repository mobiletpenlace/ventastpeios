
import Foundation
struct ObjectResponse2 : Codable {
	let promocionesLista : [PromocionesLista]?

	enum CodingKeys: String, CodingKey {

		case promocionesLista = "promocionesLista"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		promocionesLista = try values.decodeIfPresent([PromocionesLista].self, forKey: .promocionesLista)
	}

}
