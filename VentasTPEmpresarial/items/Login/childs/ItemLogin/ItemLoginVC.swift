//
//  ItemLoginViewController.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 25/06/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import TextFieldEffects

class ItemLoginVC: BaseItemVC {
    @IBOutlet weak var mViewNumberEmploye: UIView!
    @IBOutlet weak var mViewPassword: UIView!
    @IBOutlet weak var employeeTextField: HoshiTextField!
    @IBOutlet weak var passwordTextField: HoshiTextField!
    fileprivate let presenter = ItemLoginPresenter(service: ItemLoginService())
    var iconClick : Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow(){
        presenter.viewDidShow()
    }
    
    private func setUpView() {
        iconClick = true
        super.addShadowView(view: mViewNumberEmploye)
        super.addShadowView(view: mViewPassword)
    }
    
    @IBAction func visiblePasswordButton(_ sender: Any) {
        if (iconClick){
            passwordTextField.isSecureTextEntry = false
            iconClick = false
        }else {
            passwordTextField.isSecureTextEntry = true
            iconClick = true
        }
        
    }
    
    @IBAction func clickLogin(_ sender: UIButton) {
        self.view.endEditing(true)
        sender.animateBound(view: sender){[weak self] _ in
            self?.login()
        }
    }
    
    func login(){
        presenter.clickLogin(user: self.employeeTextField.text!, pass: self.passwordTextField.text!)
    }
}

extension ItemLoginVC: ItemLoginDelegate{
    func updateTitleHeader() {
        
    }
    
    func startLoading() {
        super.showLoading(view: self.view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
    func iniciarVC(name: String, identificador: String) -> UIViewController {
        return super.iniciarStoryBoard(name: name, identificador: identificador)
    }
    
    func mostrarStoryBoard(vc: UIViewController, completion:(() -> Void)?){
        super.showStoryBoardView(vc: vc,completion: completion)
    }
    
    func setTextoLoading(texto: String){
        super.updateTextoLoading(texto: texto)
    }
}
