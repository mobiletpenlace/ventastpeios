//
//  ItemLoginPresenter.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 15/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import TaskQueue

protocol ItemLoginDelegate: BaseDelegate {
    func iniciarVC(name: String, identificador: String) -> UIViewController
    func mostrarStoryBoard(vc: UIViewController, completion: (() -> Void)?)
    func setTextoLoading(texto: String)
}

class ItemLoginPresenter: TimeMeasureDelegate {
    fileprivate let service: ItemLoginService
    weak fileprivate var view: ItemLoginDelegate?
    var horaDescargaInicio: Date = Date()

    init(service: ItemLoginService) {
        self.service = service
    }

    func attachView(view: ItemLoginDelegate) {
        self.view = view
    }

    func viewDidAppear() {

    }

    func viewDidShow() {

    }

    func detachView() {
        view = nil
    }
    
    private func obtenerToken() {
        service.getToken { [weak self] (res, token) in
            guard let `self` = self else { return }
            if res {
                Variables.TOKEN = token
                self.verificaDescarga()
            } else {
                self.view?.showBannerError(title: "No se pudo obtener el token.")
                self.view?.finishLoading()
            }
        }
    }

    private func verificaDescarga() {
        if seDebeActualizarDB() {
//            service.dbManager.eliminarAllData()
            descargarDatos()
        } else {
            cambiarVistaMenu()
        }
    }
    
    func clickLogin(user: String, pass: String) {

        self.view?.startLoading()
        let noEmpleado = user.trimmingCharacters(in: .whitespacesAndNewlines)
        let password = pass.trimmingCharacters(in: .whitespacesAndNewlines)
        if Constantes.Config.deleteAllData {
            service.dbManager.eliminarAllData()
        }

        if Constantes.Config.loadHarcodeUser {
            service.actualizarIdEmpleado(Constantes.Config.harcodeUser["id"] ?? "")
            service.actualizarNombreEmpleado(Constantes.Config.harcodeUser["nombre"] ?? "")
            if Constantes.Config.useToken {
                obtenerToken()
            } else {
                verificaDescarga()
            }
            return
        }
        
        service.loginUser(noEmpleado: noEmpleado, password: password) { [weak self] data in
            guard let `self` = self else { return }
            guard let response = data else {
                self.view?.finishLoading()
                self.view?.showBannerError(title: "Error en el servico de login.")
                return
            }
            let isOk = response.response?.result == "0"
            if isOk {
                self.service.actualizarIdEmpleado(response.infoUser!.id!)
                self.service.actualizarNombreEmpleado(response.infoUser!.name!)
                if Constantes.Config.useToken {
                    self.obtenerToken()
                } else {
                    self.verificaDescarga()
                }
            } else {
                self.view?.finishLoading()
                self.view?.showBannerError(title: "Credenciales incorrectas.")
            }
        }
    }

    func seDebeActualizarDB() -> Bool {
        if Constantes.Config.forceUpdateData {
            return true
        }
        var descargar: Bool
        let formatedStartDate = service.getLastDate()
        let currentDate = Date()
        let components = Set<Calendar.Component>([.second, .minute, .hour])
        let differenceOfDate = Calendar.current.dateComponents(components, from: formatedStartDate, to: currentDate)
        if(differenceOfDate.hour! > 23) {
            descargar = true
            horaDescargaInicio = currentDate
        } else {
            if service.getPlanesIds().count <= 0 {
                descargar = true
                horaDescargaInicio = currentDate
            } else {
                descargar = false
            }
        }
        return descargar
    }

    func descargarDatos() {
        let queue = TaskQueue()

        queue.tasks +=! {
            self.view?.setTextoLoading(texto: "Descargando familias")
        }
        
        var count = 0
        queue.tasks +=~ { [weak queue, weak self] result, next in
            guard let `self` = self else { return }
            self.service.ActualizarFamilias() { res in
                if res {
                    next(nil)
                } else {
                    count += 1
                    if count < 30 {
                        Logger.i("reintentando descarga familias: (\(count)) ...")
                        queue?.retry(1)
                    } else {
                        Logger.e("Error, se ha escedido el numero de intentos.")
                    }
                }
            }
        }

        queue.tasks +=! {
            self.view?.setTextoLoading(texto: "Descargando planes")
        }
        
        count = 0
        queue.tasks +=~ { result, next in
            let familiasIds = self.service.getFamiliasIds()
            let group2 = DispatchGroup()
            for idFamilia in familiasIds {
                group2.enter()
                self.service.actualizarPlanes(idFamilia: idFamilia) { res in
                    if res {
                        Logger.i("actualizado un plan de la familia: \(idFamilia)")
                    } else {
                        Logger.w("No se pudo descargar el plan de la familias: \(idFamilia)")
                    }
                    group2.leave()
                }
            }
            group2.notify(queue: .main, execute: {
                next(nil)
            })
        }

        queue.tasks +=! {
            self.view?.setTextoLoading(texto: "Descargando detalle de los planes")
        }

        queue.tasks +=~ { result, next in
            let planesIds = self.service.getPlanesIds()
            let group2 = DispatchGroup()
            for idPlan in planesIds {
                group2.enter()
                self.service.actualizarDetallePlanes(idPlan: idPlan) { data in
                    group2.leave()
                }
            }
            group2.notify(queue: .main, execute: {
                next(nil)
            })
        }

        queue.tasks +=! {
            self.view?.setTextoLoading(texto: "Descargando productos y servicios adicionales")
        }

        queue.tasks +=~ { result, next in
            let planesIds = self.service.getPlanesIds()
            let group2 = DispatchGroup()
            for idPlan in planesIds {
                group2.enter()
                self.service.actualizarServiciosAdicionales(idPlan: idPlan) { data in
                    group2.leave()
                }
            }
            group2.notify(queue: .main, execute: {
                next(nil)
            })
        }

        queue.tasks +=! {
            self.view?.setTextoLoading(texto: "Descargando addons")
        }

        queue.tasks +=~ { result, next in
            let serviciosIds = self.service.getProductosAdicionalesIds()
            let group2 = DispatchGroup()
            for idServicio in serviciosIds {
                group2.enter()
                self.service.actualizarAddonsProductos(idServicio: idServicio) { data in
                    group2.leave()
                }
            }
            group2.notify(queue: .main, execute: {
                next(nil)
            })
        }

        queue.tasks +=! {
            self.view?.setTextoLoading(texto: "Descargando soluciones a la medida")
        }

        queue.tasks +=! { result, next in
            self.service.actualizarSolucionesAlaMedida() { data in
                next(nil)
            }
        }

        //        queue.tasks +=~ { result, next in
        //            Log.println("se va aactualizar las promociones")
        //            let planesIds = self.service.getPlanesIds()
        //            let group2 = DispatchGroup()
        //            for idPlan in planesIds{
        //                group2.enter()
        //                self.service.actualizarPromociones(idPlan: idPlan){ data in
        //                    Log.println("promociones actualizados.")
        //                    group2.leave()
        //                }
        //            }
        //            group2.notify(queue: .main, execute: {
        //                Log.println("se ha terminado de actualizar las promociones")
        //                next(nil)
        //            })
        //        }

        queue.run {
            self.service.InsertaHora(self.horaDescargaInicio)
            self.cambiarVistaMenu()
        }
    }

    func cambiarVistaMenu() {
        self.view?.setTextoLoading(texto: "Iniciando")
        DispatchQueue.global(qos: .userInitiated).async {
            let vc = self.view!.iniciarVC(name: "MenuTabBar", identificador: "MenuViewController")
            DispatchQueue.main.async {
                self.view?.mostrarStoryBoard(vc: vc) {
                    self.view?.finishLoading()
                }
            }
        }
    }
}
