//
//  LoginViewController.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 01/06/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import TextFieldEffects
import SwiftBaseLibrary

class LoginViewController: BaseItemVC {
    @IBOutlet weak var containView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.addViewXIB(vc: ItemLoginVC(), container: containView)
    }
    
}
