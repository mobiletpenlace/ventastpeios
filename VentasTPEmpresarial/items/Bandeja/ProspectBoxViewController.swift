//
//  ProspectBoxViewController.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 21/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class ProspectBoxViewController: BaseMenuItemVC, TimeMeasureDelegate {

    @IBOutlet weak var mViewContainer: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        let tiempoInicio = iniciarTiempo()

        super.subscribe(nameItem: Constantes.Eventbus.Id.menuItemSitios)
        super.headerData = HeaderData(
            imagen: #imageLiteral(resourceName: "logo_Sitio"),
            titulo: "Sitios",
            titulo2: nil,
            titulo3: nil,
            subtitulo: nil,
            closure: nil
        )
        addItems()
        medirTiempo(tipoInicio: tiempoInicio, nombre: "Sitios")
    }

    func addItems() {
        var viewControllers = [BaseItemVC]()
        viewControllers.append(ProspectTableVC())
        super.addViewXIBList(view: mViewContainer, viewControllers: viewControllers)
    }

}
