//
//  prospectBoxCell.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 22/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class ProspectBoxCell: UITableViewCell {
    
    @IBOutlet weak var clientNameLabel: UILabel!
    @IBOutlet weak var statusView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
