//
//  prospectTablePresenter.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 22/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import Foundation
import SwiftEventBus

protocol prospectTableDelegate: BaseDelegate {
    func reloadTable()
    func selecciono()
    func noSelecciono()
}

class ProspectTablePresenter {

    weak fileprivate var view: prospectTableDelegate?
    fileprivate let service: ProspectTableService?
    fileprivate var dataList = [DetalleSitiosModel]()

    init(service: ProspectTableService) {
        self.service = service
    }

    func attachView(view: prospectTableDelegate) {
        self.view = view
    }

    func viewDidAppear() {
    }

    /*protocol*/
    func viewWillAppear() {

    }

    func viewDidShow() {
        view?.updateTitleHeader()

    }

    func detachView() {
        view = nil
    }

//    func getItemIndex(index: Int) -> TablaCargaSitiosModel {
//        return data[index]
//    }

    func getSize() -> Int {
        return dataList.count
    }

    func actualizarDatos() {
        self.view?.startLoading()
    }

    func onFinishGetData(data: [DetalleSitiosModel]) {
        view?.finishLoading()
        view?.reloadTable()

    }
}
