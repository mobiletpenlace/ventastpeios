//
//  prospectTableVC.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 22/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class ProspectTableVC: BaseItemVC {

    @IBOutlet weak var prospectTabeleView: UITableView!
    var nombreCelda2: String = "SeleccionarSitiosCell"

    var presenter = ProspectTablePresenter(service: ProspectTableService())

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
    }

}

extension ProspectTableVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return SitiosCellHeader().view
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let data = presenter.getItemIndex(index: indexPath.row)
//        print(indexPath.row)
//        SwiftEventBus.post(
//            "Modelado_Masivo_change_seleccionado",
//            sender: SeleccionarSitioModel(
//                Sitio: indexPath.row,
//                Seleccionado: !data.Seleccionado!)
//        )
    }
}

extension ProspectTableVC: UITableViewDataSource, BaseFormatNumber {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getSize()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: nombreCelda2, for: indexPath) as! SeleccionarSitiosCell
        return cell
    }

}

extension ProspectTableVC: prospectTableDelegate {
    func reloadTable() {

    }

    func selecciono() {

    }

    func noSelecciono() {

    }

    func updateTitleHeader() {

    }

    func startLoading() {

    }

    func finishLoading() {

    }

}
