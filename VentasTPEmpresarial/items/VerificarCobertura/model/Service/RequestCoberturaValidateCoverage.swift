//
//  RequestCoberturaValidateCoverage.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 05/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

class RequestCoberturaValidateCoverage: Codable {
    
    var data: RequestCoberturaData
    
    private enum CodingKeys: String, CodingKey {
        case data = "Data"
        
    }
}
