//
//  RequestCobertura.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 05/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

class RequestCobertura: Codable {
    var login: RequestLoginMiddleware
    var validarCobertura: RequestCoberturaValidateCoverage
    
    
    private enum CodingKeys: String, CodingKey {
        case login = "Login"
        case validarCobertura = "ValidateCoverage"
        
    }

}
