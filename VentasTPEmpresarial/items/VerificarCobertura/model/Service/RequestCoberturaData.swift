//
//  RequestCoberturaData.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 05/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

class RequestCoberturaData: Codable {
    var idSitio: String?
    var nombreSitio: String?
    var longitude: String?
    var latitude: String?
    var bandwidth: String?
    var tipoCliente: String?
    
    required init(idSitio: String?,nombreSitio: String?,longitude: String?,latitude: String?,bandwidth: String?,tipoCliente: String?) {
        self.idSitio = idSitio
        self.nombreSitio = nombreSitio
        self.longitude = longitude
        self.latitude = latitude
        self.bandwidth = bandwidth
        self.tipoCliente = tipoCliente
    }
    
    private enum CodingKeys: String, CodingKey {
        case idSitio
        case nombreSitio
        case longitude
        case latitude
        case bandwidth
        case tipoCliente
        
    }
}
