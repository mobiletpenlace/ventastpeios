//
//  CoberturaResumenPresenter.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 26/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus
import TaskQueue
import GoogleMaps

protocol CoberturaResumenDelegate: BaseDelegate {
    func titleCobertura(numero : String)
    func titleSinCobertura(numero : String)
    func mapa(ubicacion : ConfirmaUbicacionModel)
    func conneccionError(message : String)
}

class CoberturaResumenPresenter {
    fileprivate let model: CoberturaResumenServie?
    fileprivate let locationModel: LocationService?
    weak fileprivate var view: CoberturaResumenDelegate?
    fileprivate var data: ConfirmaUbicacionModel?
    fileprivate var dataList = [ConfirmaUbicacionModel]()
    fileprivate var listaCarga = [ConfirmaUbicacionModel?]()
    let geocoder = GMSGeocoder()
    var posLoc : Int!
    var posGen : Int!
    var count = 0
    var cargaCount = 0
    
    
    init(model: CoberturaResumenServie, locationService: LocationService){
        self.model = model
        self.locationModel = locationService
    }
    
    func attachView(view: CoberturaResumenDelegate){
        self.view = view
        SwiftEventBus.onMainThread(self, name: "ConfirmaUbicacion-Actualizar-Numeracion-Cobertura") { [weak self] result in
            if let numero = result?.object as? String{
                self?.view?.titleCobertura(numero: numero)
            }
        }
        SwiftEventBus.onMainThread(self, name: "ConfirmaUbicacion-Actualizar-Numeracion-SinCobertura") { [weak self] result in
            if let numero = result?.object as? String{
                self?.view?.titleSinCobertura(numero: numero)
            }
        }
        SwiftEventBus.onMainThread(self, name: "ConfirmaUbicacion-PopMapa") { [weak self] result in
            if let ubicacion = result?.object as? ConfirmaUbicacionModel{
                self?.view?.mapa(ubicacion: ubicacion)
            }
        }
        
    }
    
    func viewDidAppear(){
        
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
    }
    
    func detachView() {
        view = nil
    }
    
    func matches(for regex: String, in text: String) -> [String] {
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let results = regex.matches(in: text,
                                        range: NSRange(text.startIndex..., in: text))
            return results.map {
                String(text[Range($0.range, in: text)!])
            }
        } catch _ {
            return []
        }
    }
    
    func getStreetNumber(street : String) -> String{
        let matched = self.matches(for: "(MZ)?[0-9]+(\\s)*(LT)?[0-9]+", in: street)
        return matched.first ?? ""
    }
    
    func onClickCarga(_ data : [CoberturaResumenModel]){
        DispatchQueue.main.sync {
            view?.startLoading()
        }
        let group = DispatchGroup()
        for sitio in data{
            group.enter()
            listaCarga.append(initializaModel())
            if(sitio.Latitud == "" || sitio.Longitud == ""){
                self.locationModel?.getPredictionAddress(keyword: NSString(string: "\(sitio.Calle) \(sitio.NumExt) \(sitio.Colonia) \(sitio.Municipio) Mexico")){ [weak self] data in//metodo encargado de obtener lat long
                    if(data != nil){
                        self!.listaCarga[self!.cargaCount]!.latitud = (data?.result?.geometry?.location?.lat) ?? 0
                        self!.listaCarga[self!.cargaCount]!.longitud = (data?.result?.geometry?.location?.lng) ?? 0
                        self!.cargaCount = self!.cargaCount + 1
                        group.leave()
                    }else{
                        self!.listaCarga[self!.cargaCount] = nil
                        self!.cargaCount = self!.cargaCount + 1
                        group.leave()
                    }
                }
            }else{
                listaCarga[cargaCount]!.latitud = Double(sitio.Latitud)!
                listaCarga[cargaCount]!.longitud = Double(sitio.Longitud)!
                self.cargaCount = self.cargaCount + 1
                group.leave()
            }
            
        }
        group.notify(queue: .main, execute: {
            self.cargaCount = 0
            self.procesaDireccion(data)
                    })
    }
    
    func procesaDireccion(_ data : [CoberturaResumenModel]){
        let group = DispatchGroup()
        let queue = TaskQueue()
        for sitio in data{
            group.enter()
            queue.tasks +=~ { result, next in
                if(self.listaCarga[self.cargaCount] == nil){
                    self.cargaCount = self.cargaCount + 1
                    group.leave()
                    next(nil)
                }else{
                    let coordinate : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: (self.listaCarga[self.cargaCount]!.latitud), longitude: (self.listaCarga[self.cargaCount]!.longitud))
                    Logger.d("\(coordinate.latitude),\(coordinate.longitude)")
                    self.geocoder.reverseGeocodeCoordinate(coordinate) { [unowned self] response, error in//metodo encargado de obtener los datos de la ubicacion
                        if let address = response?.firstResult() {//verificamos que el servicio devuelba valores
                            let str = address.thoroughfare ?? ""
                            let numero = self.getStreetNumber(street: str)
                            let calle = str.replacingOccurrences(of: numero, with: "", options: .literal, range: nil)
                            if(sitio.Calle == ""){self.listaCarga[self.cargaCount]!.calle = calle } else{self.listaCarga[self.cargaCount]!.calle = sitio.Calle}
                            if(sitio.NumExt == ""){self.listaCarga[self.cargaCount]!.numExt = numero } else{self.listaCarga[self.cargaCount]!.numExt = sitio.NumExt}
                            if(sitio.Colonia == ""){self.listaCarga[self.cargaCount]!.colonia = address.subLocality ?? ""} else{self.listaCarga[self.cargaCount]!.colonia = sitio.Colonia}
                            if(sitio.Municipio == ""){self.listaCarga[self.cargaCount]!.municipio =  ""} else{self.listaCarga[self.cargaCount]!.municipio = sitio.Municipio}
                            self.listaCarga[self.cargaCount]!.numInt = sitio.NumInt
                            self.listaCarga[self.cargaCount]!.nombreSitio = sitio.Nombre
                            if(sitio.Cp == ""){self.listaCarga[self.cargaCount]!.codigoPostal = address.postalCode ?? ""} else{self.listaCarga[self.cargaCount]!.codigoPostal = sitio.Cp}
                            if(sitio.Ciudad == ""){self.listaCarga[self.cargaCount]!.ciudad = address.locality ?? ""} else{self.listaCarga[self.cargaCount]!.ciudad = sitio.Ciudad}
                            if(sitio.Estado == ""){self.listaCarga[self.cargaCount]!.estado = address.administrativeArea  ?? ""} else{self.listaCarga[self.cargaCount]!.estado = sitio.Estado}
                            self.cargaCount = self.cargaCount + 1
                            group.leave()
                            next(nil)
                        }else{
                            self.cargaCount = self.cargaCount + 1
                            self.listaCarga[self.cargaCount] = nil
                            Logger.e("No se ha encontrado la direccion")
                            group.leave()
                            next(nil)
                        }
                    }
                }
            }
            
            queue.run {
                Logger.i("finished")
            }
            
        }
        group.notify(queue: .main, execute: {
            self.cargaCount = 0
            self.procesaFactibilidad()
        })
    }
    
    func procesaFactibilidad(){
        let group = DispatchGroup()
        let queue = TaskQueue()
        for sitio in listaCarga{
            group.enter()
            queue.tasks +=~ { result, next in
                if(sitio == nil){
                    self.cargaCount = self.cargaCount + 1
                    group.leave()
                    next(nil)
                }else{
                    if ConnectionUtils.isConnectedToNetwork(){//verificamos la coneccion
                        self.model?.validaCP(latitude: String("\(self.listaCarga[self.cargaCount]!.latitud)"), Longitude: String("\(self.listaCarga[self.cargaCount]!.longitud)"), TipoCliente: "RESIDENCIAL"){ [weak self] (res,data) in// metodo de cansulta cp
                            if res{
                                if(data!.calculaFactibilidad != nil){// si factibilidad no es nula procedemos a validar la cobertura
                                    if(data!.calculaFactibilidad?.factibilidad != "0"){//si factibilidad es 0 no hay cobertura
                                        SwiftEventBus.post(
                                            "CoberturaResumen-agregarUbicacion-ConCovertura",
                                            sender: Sitio(ciudad: data!.calculaFactibilidad?.cuidad ?? "",cluster: data!.calculaFactibilidad?.nombre_cluster ?? "",sitio: /*data.calculaFactibilidad?.cuidad ?? */"",zona: data!.calculaFactibilidad?.zona ?? "",distrito: data!.calculaFactibilidad?.distrito ?? "",medioDeAcceso: /*data.calculaFactibilidad?.cuidad ??*/ "")
                                        )
                                        SwiftEventBus.post("CoberturaResumen-agregarUbicacion-ConCovertura-pestaña",sender: nil)
                                        self!.listaCarga[self!.cargaCount]!.cobertura = true
                                        self!.listaCarga[self!.cargaCount]!.categoryService = (data!.calculaFactibilidad?.categoryService)!
                                        self!.listaCarga[self!.cargaCount]!.cluster = (data!.calculaFactibilidad?.nombre_cluster)!
                                        self!.listaCarga[self!.cargaCount]!.distrito = (data!.calculaFactibilidad?.distrito)!
                                        self!.listaCarga[self!.cargaCount]!.factibilidad = (data!.calculaFactibilidad?.factibilidad)!
                                        self!.listaCarga[self!.cargaCount]!.plaza = (data!.calculaFactibilidad?.cuidad)!
                                        self!.listaCarga[self!.cargaCount]!.region = (data!.calculaFactibilidad?.region)!
                                        self!.listaCarga[self!.cargaCount]!.regionId = (data!.calculaFactibilidad?.idRegion)!
                                        if(data!.calculaFactibilidad?.factibilidad == "5"){
                                            self!.listaCarga[self!.cargaCount]!.tipoCobertura = "Microondas"
                                        }else if(data!.calculaFactibilidad?.factibilidad == "0"){
                                            self!.listaCarga[self!.cargaCount]!.tipoCobertura = "No factible"
                                        }else{
                                            self!.listaCarga[self!.cargaCount]!.tipoCobertura = "Fibra"
                                        }
                                        self!.listaCarga[self!.cargaCount]!.zona = (data!.calculaFactibilidad?.zona)!
                                        Logger.d("Se agrego con cobertura")
                                        self?.dataList.append((self!.listaCarga[self!.cargaCount]!))
                                        SwiftEventBus.post("CoberturaResumen-agregarUbicacion-general",sender: self?.dataList)
                                        self!.cargaCount = self!.cargaCount + 1
                                        group.leave()
                                        next(nil)
                                    }else{
                                        SwiftEventBus.post(
                                            "CoberturaResumen-agregarUbicacion-SinCovertura",
                                            sender: Sitio(ciudad: data!.calculaFactibilidad?.cuidad ?? "",cluster: data!.calculaFactibilidad?.nombre_cluster ?? "",sitio: /*data.calculaFactibilidad?.cuidad ?? */"",zona: data!.calculaFactibilidad?.zona ?? "",distrito: data!.calculaFactibilidad?.distrito ?? "",medioDeAcceso: /*data.calculaFactibilidad?.cuidad ??*/ "")
                                        )
                                        SwiftEventBus.post("CoberturaResumen-agregarUbicacion-SinCovertura-pestaña", sender: nil)
                                        self!.listaCarga[self!.cargaCount]!.cobertura = false
                                        Logger.d("Se agrego sin cobertura")
                                        self?.dataList.append((self!.listaCarga[self!.cargaCount]!))
                                        SwiftEventBus.post("CoberturaResumen-agregarUbicacion-general",sender: self?.dataList)
                                        self!.cargaCount = self!.cargaCount + 1
                                        group.leave()
                                        next(nil)
                                    }
                                }else{
                                    self!.view?.conneccionError(message: "Ocurrio un error al consultar el servicio, intenta mas tarde")
                                    Logger.e("Ocurrio un error al consultar el servicio, intenta mas tarde")
                                    self!.cargaCount = self!.cargaCount + 1
                                    group.leave()
                                    next(nil)
                                }
                            }else{
                                Logger.e("Ocurrio un error al consultar el servicio, intenta mas tarde")
                                self!.cargaCount = self!.cargaCount + 1
                                group.leave()
                                next(nil)
                            }
                        }
                    }else{
                        self.view?.conneccionError(message: "Sin conneccion a internet, intenta mas tarde")
                        Logger.e("Sin conneccion a internet, intenta mas tarde")
                        self.cargaCount = self.cargaCount + 1
                        group.leave()
                        next(nil)
                    }
                }
            }
            
            queue.run {
                Logger.i("finished")
            }
            
        }
        
        group.notify(queue: .main, execute: {
            self.cargaCount = 0
            self.listaCarga = [ConfirmaUbicacionModel?]()
            self.dataList = [ConfirmaUbicacionModel]()
            self.view?.finishLoading()
        })
        
    }
    
    func initializaModel()-> ConfirmaUbicacionModel{
        return ConfirmaUbicacionModel(nombreSitio: "",codigoPostal: "",latitud: 0,longitud: 0,calle: "",numExt: "",numInt: "",colonia: "",estado: "",ciudad: "",municipio: "",entreCalle: "",yCalle: "",observaciones:"",cobertura: false,categoryService: "",cluster: "",distrito: "",factibilidad: "",plaza: "",region: "",regionId: "",tipoCobertura: "",zona: "")
    }
}
