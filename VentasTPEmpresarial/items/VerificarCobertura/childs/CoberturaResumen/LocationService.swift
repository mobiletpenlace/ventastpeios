//
//  LocationPresenter.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 29/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import RealmSwift
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

class LocationService {
    var serverManager: ServerDataManager?
    public var request : Alamofire.Request?
    
    private let baseURLString = "https://maps.googleapis.com/maps/api/place/autocomplete/json"
    private let baseDetailtsURLString = "https://maps.googleapis.com/maps/api/place/details/json"
    private let GOOGLE_MAP_KEY = "AIzaSyB6gsXrR55tEp7jFu19_TawlQ-cmV8sIkQ"
    init() {
    }
    
    public func getPredictionAddress(keyword: NSString,_ callBack:@escaping (GeometryResponse?) -> Void){
        var Geo : GeometryResponse? = GeometryResponse()
        if ConnectionUtils.isConnectedToNetwork(){
            let escapedString = keyword.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
            let urlString = "\(baseURLString)?key=\(GOOGLE_MAP_KEY)&language=es&input=\(String(describing: escapedString!))"
            request = Alamofire.request(urlString, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default,headers: nil).responseObject {(response: DataResponse<DirectionResponse>) in
                switch response.result {
                case .success:
                    let objectReponse : DirectionResponse = response.result.value!
                    if(!objectReponse.predictionData.isEmpty){
                        self.getLatLon(placeId: objectReponse.predictionData[0].place_id!){ data in
                            Geo = data
                            callBack(Geo)
                        }
                    }else{
                        callBack(nil)
                    }
                case .failure(_):
                    callBack(nil)
                }
            }
            
        } else {
            callBack(nil)
        }
    }
    
    public func getLatLon(placeId : String,_ callBack:@escaping (GeometryResponse?) -> Void){
        if ConnectionUtils.isConnectedToNetwork(){
            let urlString = "\(baseDetailtsURLString)?placeid=\(placeId)&key=\(GOOGLE_MAP_KEY)"
            request = Alamofire.request(urlString, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default,headers: nil).responseObject {
                (response: DataResponse<GeometryResponse>) in
                switch response.result {
                case .success:
                    let objectReponse: GeometryResponse = response.result.value!
                    if(objectReponse.result?.geometry?.location?.lat != nil){
                        callBack(objectReponse)
                    }else{
                        Logger.e("location lat es nil")
                        callBack(nil)
                    }
                case .failure(_):
                    Logger.e("Falló el request de location")
                    callBack(nil)
                }
            }
        } else {
            Logger.e("No hay internet.")
            callBack(nil)
        }
        
    }
    
}
