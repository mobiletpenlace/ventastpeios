//
//  CoberturaResumenVC.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 09/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus
import CSV

class CoberturaResumenVC: BaseItemVC, UIDocumentPickerDelegate {
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var segmentControl: CustomUISegmentedControl!
    @IBOutlet weak var image: UIImageView!
    fileprivate let presenter = CoberturaResumenPresenter(
        model: CoberturaResumenServie(),
        locationService: LocationService()
    )
    var dataList = [CoberturaResumenModel]()
    var count : Int = 0
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSubView()
        configEventBus()
        presenter.attachView(view: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //segmentControl.changeUnderlinePosition(0)
    }
    
    func addSubView() -> Void {
        var viewControllers2 = [BaseItemVC]()
        viewControllers2.append(ConCoberturaVC())
        viewControllers2.append(SinCoberturaVC())
        super.addViewXIBList(view: viewContent, viewControllers: viewControllers2)
    }
    
    @IBAction func selectedView(_ sender: CustomUISegmentedControl) {
        sender.changeUnderlinePosition(sender.currentIndex)
        super.cambiarVista(index: sender.selectedSegmentIndex)
    }
    
    @IBAction func cleanAction(_ sender: Any) {
        let alert = UIAlertController(
            title: "Borrar",
            message: "¿Seguro que quieres eliminar todos los sitios?",
            preferredStyle: .alert
        )
        let deleteAction = UIAlertAction(title: "Si", style: .default){ _ in
            SwiftEventBus.post("CoberturaResumen-limpiar",sender: nil)
        }
        let cancelAction = UIAlertAction(title: "No", style: .default, handler: nil)
        alert.addAction(cancelAction)
        alert.addAction(deleteAction)
        present(alert, animated: true)
    }
    
    @IBAction func cargaMasivaAction(_ sender: Any) {
        count = 0
        dataList = [CoberturaResumenModel]()
        let importMenu = UIDocumentPickerViewController(documentTypes: ["public.text", "public.source-code"], in: .import)
        importMenu.delegate = self
        self.present(importMenu, animated: true, completion: nil)
    }
    
    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        self.present(documentPicker, animated: true, completion: nil)

    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else { return }
            let texto = String(bytes: data,encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
            let csv = try! CSVReader(string: texto)
                    while let row = csv.next() {
                        if(self.count > 0){
                            if(row[1] != "" && row[2] != "" && row[4] != "" && row[6] != "" ){
                                self.dataList.append(CoberturaResumenModel(
                                    Nombre: row[0],
                                    Calle: row[1],
                                    NumExt: row[2],
                                    NumInt: row[3],
                                    Colonia: row[4],
                                    Cp: row[5],
                                    Municipio: row[6],
                                    Estado: row[7],
                                    Ciudad: row[8],
                                    Latitud: row[9],
                                    Longitud: row[10]))
                            }else if row[9] != "" && row[10] != "" {
                                self.dataList.append(CoberturaResumenModel(
                                    Nombre: row[0],
                                    Calle: row[1],
                                    NumExt: row[2],
                                    NumInt: row[3],
                                    Colonia: row[4],
                                    Cp: row[5],
                                    Municipio: row[6],
                                    Estado: row[7],
                                    Ciudad: row[8],
                                    Latitud: row[9],
                                    Longitud: row[10]))
                            }
                        }
                        self.count += 1
                    }
            self.presenter.onClickCarga(self.dataList)
        }
        task.resume()
    }

    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
//        dismiss(animated: true, completion: nil)
    }
    
    func configEventBus(){
        SwiftEventBus.onMainThread(self, name: "CoberturaResumen-agregarUbicacion-ConCovertura-pestaña") { [weak self] result in
            self?.changeView(pos: 0)
        }
        SwiftEventBus.onMainThread(self, name: "CoberturaResumen-agregarUbicacion-SinCovertura-pestaña") { [weak self] result in
            self?.changeView(pos: 1)
        }
    }
    
    func changeView(pos : Int){
        segmentControl.selectedSegmentIndex = pos
        segmentControl.sendActions(for: UIControlEvents.valueChanged)
    }
    
    override func viewDidShow() {
        super.viewDidShow()
        let header = HeaderData(
            imagen: #imageLiteral(resourceName: "logo_Sitio"),
            titulo: "Sitios",
            titulo2: "Validar Cobertura",
            titulo3: nil,
            subtitulo: "Resumen de Cobertura",
            closure: nil
        )
        super.updateTitle(headerData: header)
    }
    
}

extension CoberturaResumenVC : CoberturaResumenDelegate{
    
    func conneccionError(message: String) {
        self.showAlert(titulo: "Error", mensaje: message)
    }
    
    func mapa(ubicacion: ConfirmaUbicacionModel) {
        let storyboard = UIStoryboard(name: "PopMapViewController", bundle: Bundle(for: PopMapViewController.self))
        let v1 : PopMapViewController = storyboard.instantiateViewController(withIdentifier: "PopMapViewController") as! PopMapViewController
        v1.providesPresentationContextTransitionStyle = true
        v1.definesPresentationContext = true
        v1.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        v1.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        v1.lat = ubicacion.latitud
        v1.lon = ubicacion.longitud
        self.present(v1, animated: true, completion: nil)
    }
    
    func titleCobertura(numero: String) {
        segmentControl.setTitle("Con Cobertura (\(numero))", forSegmentAt: 0)
    }
    
    func titleSinCobertura(numero: String) {
        segmentControl.setTitle("Sin Cobertura (\(numero))", forSegmentAt: 1)
    }
    
    func updateTitleHeader() {
        
    }
    
    func startLoading() {
        super.showLoading(view: view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
    
}
    

    


