//
//  PopMapViewController.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 07/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation

class PopMapViewController: UIViewController , CLLocationManagerDelegate, GMSMapViewDelegate{
    
    @IBOutlet weak var mapa: GMSMapView!

    var lat: Double!
    var lon: Double!
    var location: CLLocationCoordinate2D!
    var mGoogleMapView: GMSMapView!
    var marker: GMSMarker? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup(){
        location = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        marker = GMSMarker(position: location)
        marker?.title = ""
        marker?.map = self.mapa
        let icono = scaleImage(
            image: UIImage(named: "ic_google_pin")!,
            scaledToSize: CGSize(width: 30.0, height: 40.0)
        )
        marker?.icon = icono
        let camera = GMSCameraPosition.camera(
            withLatitude: location.latitude,
            longitude: location.longitude,
            zoom: 17.0
        )
        mapa.animate(to: camera)
    }
    
    @IBAction func salirButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func scaleImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
}

