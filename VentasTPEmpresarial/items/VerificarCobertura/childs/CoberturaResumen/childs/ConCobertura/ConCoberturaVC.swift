//
//  CoberturaResumenVC.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 09/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus
class ConCoberturaVC: BaseItemVC {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var AgregarImage: UIImageView!
    let customCell = "ConCoberturaCell"
    fileprivate let presenter = ConCoberturaPresenter(model : SinCoverturaServie())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let imgTint = AgregarImage.image?.withRenderingMode(.alwaysTemplate)
        AgregarImage.image = imgTint
        AgregarImage.tintColor = UIColor.white
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
        presenter.attachView(view: self)
    }
    
    override func viewDidShow(){
        presenter.viewDidShow()
    }
    
    private func setUpView() {
        iniciarCollectionView()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        guard let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else {return}
        flowLayout.invalidateLayout()
    }
    
    func iniciarCollectionView(){
        let nib = UINib.init(nibName: customCell, bundle: nil)
        self.collectionView.register(nib, forCellWithReuseIdentifier: customCell)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }

    @IBAction func onClickSiguiente(_ sender: UIButton) {
        presenter.onClickSiguiente()
    }
    
    @IBAction func onClickAgregar(_ sender: UIButton) {
        presenter.onClickAgregar()
    }
    
}

extension ConCoberturaVC: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.getSize()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: customCell, for: indexPath) as! ConCoberturaCell
        let data = presenter.getItemIndex(index: indexPath.row)
        cell.labelCiudad.text = data.ciudad
        cell.labelCitio.text = data.sitio
        cell.labelPlaza.text = data.plaza
        cell.labelZona.text = data.zona
        cell.labelCluster.text = data.cluster
        cell.labelDistrito.text = data.distrito
        cell.labelMedioDeAcceso.text = data.ubicacion.tipoCobertura
        cell.ubicacion = data.ubicacion
        cell.posLoc = data.posicionLocal
        cell.posGen = data.posicionGeneral
        if(data.DataValid){
            cell.labelDatosDeUbicacion.textColor = Colores.Verde
            cell.labelDatosDeUbicacion.text = "Correcto"
        }else{
            cell.labelDatosDeUbicacion.textColor = Colores.Rojo
            cell.labelDatosDeUbicacion.text = "Datos incompletos"
        }
        return cell
    }
}

extension ConCoberturaVC: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = collectionView.frame.size.width - 20
        //let h = collectionView.frame.size.height / 2 - 10
        return CGSize(width: w, height: 180)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension ConCoberturaVC:  ConCoberturaDelegate{
    func desbloquea() {
        
    }
    
    func bloquea() {
        
    }
    
    func soloCotiza() {
        padreController?.padreController?.cambiarVista(DetalleSitioVC.self) // detalle sitio
    }
    
    func editar() {
        padreController?.padreController?.cambiarVista(ConfirmaUbicacionVC.self) // confirma ubicacion
    }
    
    func Agregar() {
        padreController?.padreController?.cambiarVista(CoberturaMapaVC.self) // cobertura mapa
    }
    
    func actualizarDatos() {
        collectionView.reloadData()
        let size = presenter.getSize()
        let indexPath = IndexPath(row: (size - 1), section: 0)
        if(size > 0){
            collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredVertically)
        }
        
    }
    func cambiarNombre(){
        
    }
    
    func cambiarSiguienteVista() {
        padreController?.padreController?.cambiarVista(DetalleSitioVC.self) // detalle sitio
    }
    
    func updateTitleHeader() {
    }
    
    func startLoading() {
        super.showLoading(view: view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
}
