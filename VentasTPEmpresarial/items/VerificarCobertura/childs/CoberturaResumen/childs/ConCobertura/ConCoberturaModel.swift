//
//  CoberturaResumenModel.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 15/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

struct ConCoberturaModel {
    let ciudad: String
    let sitio: String
    let plaza: String
    let zona: String
    let cluster: String
    let distrito: String
    let cobertura: Bool
    let medioAcceso: String
    var ubicacion : ConfirmaUbicacionModel
    var posicionLocal : Int
    var posicionGeneral : Int
    var DataValid: Bool
}
