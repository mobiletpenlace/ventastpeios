//
//  CoberturaResumenPresenter.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 15/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import Foundation
import SwiftEventBus

protocol ConCoberturaDelegate: BaseDelegate {
    func actualizarDatos()
    func Agregar()
    func editar()
    func cambiarSiguienteVista()
    func soloCotiza()
    func desbloquea()
    func bloquea()
}

class ConCoberturaPresenter {
    fileprivate let model: SinCoverturaServie?
    weak fileprivate var view: ConCoberturaDelegate?
    fileprivate var dataList = [ConCoberturaModel]()
    fileprivate var dataDirecciones = [ConfirmaUbicacionModel]()
    var count : Int = 0
    var posLoc : Int!
    var posGen : Int!
    
    init(model: SinCoverturaServie){
        self.model = model
    }
    
    func attachView(view: ConCoberturaDelegate){
        self.view = view
        SwiftEventBus.onMainThread(self, name: "CoberturaResumen-Bloquea-Botones") { [weak self] result in
            self?.view?.bloquea()
        }
        SwiftEventBus.onMainThread(self, name: "CoberturaResumen-Desbloquea-Botones") { [weak self] result in
            self?.view?.desbloquea()
        }
        
        SwiftEventBus.onMainThread(self, name: "CoberturaResumen-agregarUbicacion-ConCovertura") { [weak self] result in
            if let p = result!.object as? Sitio{
                self?.agregarUbicacion(p: p)
            }else{
                Logger.w("No se ha podido convertir el dato en Eventbus")
            }
        }
        SwiftEventBus.onMainThread(self, name: "CoberturaResumen-agregarUbicacion-general") { [weak self] result in
            self?.dataDirecciones = result!.object as! [ConfirmaUbicacionModel]
            if(self?.dataDirecciones.last?.cobertura)!{
                self?.dataList[(self?.count)!].ubicacion = (self?.dataDirecciones.last)!
                self?.dataList[(self?.count)!].posicionGeneral = ((self?.dataDirecciones.count)! - 1)
                if( self?.dataList[(self?.count)!].ubicacion.codigoPostal == "" || self?.dataList[(self?.count)!].ubicacion.calle == "" || self?.dataList[(self?.count)!].ubicacion.colonia == "" || self?.dataList[(self?.count)!].ubicacion.estado == "" || self?.dataList[(self?.count)!].ubicacion.ciudad == "" || self?.dataList[(self?.count)!].ubicacion.municipio == ""){
                    self?.dataList[(self?.count)!].DataValid = false
                }else{
                    self?.dataList[(self?.count)!].DataValid = true
                }
                self?.count = (self?.count)! + 1
                self?.view?.actualizarDatos()
            }else{
            }
        }
        SwiftEventBus.onMainThread(self, name: "CoberturaResumen-validarCobertura-child") { [weak self] result in
            self?.view?.Agregar()
        }
        SwiftEventBus.onMainThread(self, name: "CoberturaResumen-Restart") { [weak self] result in
            self?.dataList = [ConCoberturaModel]()
            self?.dataDirecciones = [ConfirmaUbicacionModel]()
            self?.actualizar()
        }
        SwiftEventBus.onMainThread(self, name: "ConfirmaUbicacion-editar(redirigir)") { [weak self] result in
            self?.view?.editar()
        }
        SwiftEventBus.onMainThread(self, name: "Cobertura-Con-Editar-Ubicacion") { [weak self] result in
            self?.dataDirecciones[(self?.posGen)!] = result?.object as! ConfirmaUbicacionModel
            self?.dataList[(self?.posLoc)!].ubicacion = (self?.dataDirecciones[(self?.posGen)!])!
            self?.actualizar()
        }
        SwiftEventBus.onMainThread(self, name: "Cobertura-Editar-posLoc") { [weak self] result in
            if let posLoc = result?.object as? Int{
                self?.posLoc = posLoc
            }else{
                Logger.w("No se ha podido convertir el dato en Eventbus")
            }
        }
        SwiftEventBus.onMainThread(self, name: "Cobertura-Editar-posGen") { [weak self] result in
            if let posGen = result?.object as? Int{
                self?.posGen = posGen
            }else{
                Logger.w("No se ha podido convertir el dato en Eventbus")
            }
        }
        SwiftEventBus.onMainThread(self, name: "ConfirmaUbicacion-eliminar-posLoc") { [weak self] result in
            if let posLoc = result?.object as? Int{
                self?.posLoc = posLoc
            }else{
                Logger.w("No se ha podido convertir el dato en Eventbus")
            }
        }
        SwiftEventBus.onMainThread(self, name: "ConfirmaUbicacion-eliminar-posGen") { [weak self] result in
            if let posGen = result?.object as? Int{
                self?.posGen = posGen
            }else{
                Logger.w("No se ha podido convertir el dato en Eventbus")
            }
        }
        SwiftEventBus.onMainThread(self, name: "ConfirmaUbicacion-eliminar-ConCovertura") { [weak self] result in
            self?.eliminaSitio()
        }
        SwiftEventBus.onMainThread(self, name: "ConfirmaUbicacion-eliminar-actualizar") { [weak self] result in
            self?.dataDirecciones = result?.object as! [ConfirmaUbicacionModel]
            self?.actualizaSitio()
        }
        SwiftEventBus.onMainThread(self, name: "CoberturaResumen-limpiar") { [weak self] result in
            self?.count = 0
            self?.dataList = [ConCoberturaModel]()
            self?.dataDirecciones = [ConfirmaUbicacionModel]()
            self?.view?.actualizarDatos()
        }
    }
    
    func agregarUbicacion(p : Sitio){
        dataList.append(
            ConCoberturaModel(
                ciudad: p.ciudad,
                sitio: p.sitio,
                plaza: p.ciudad,
                zona: p.zona,
                cluster: p.cluster,
                distrito: p.distrito,
                cobertura: true,
                medioAcceso: p.medioDeAcceso,
                ubicacion: ConfirmaUbicacionModel(
                    nombreSitio: "",
                    codigoPostal: "",
                    latitud: 0,
                    longitud: 0,
                    calle: "",
                    numExt: "",
                    numInt: "",
                    colonia: "",
                    estado: "",
                    ciudad: "",
                    municipio: "",
                    entreCalle: "",
                    yCalle: "",
                    observaciones:"",
                    cobertura: false,
                    categoryService: "",
                    cluster: "",
                    distrito: "",
                    factibilidad: "",
                    plaza: "",
                    region: "",
                    regionId: "",
                    tipoCobertura: "",
                    zona: ""),
                posicionLocal: (dataList.count ),
                posicionGeneral: 0,
                DataValid: false
            )
        )
        self.actualizar()
    }
    
    func actualizar(){
        if(dataList.count > 0){
            for i in 0...(dataList.count - 1) {
                if( dataList[i].ubicacion.nombreSitio == "" || dataList[i].ubicacion.codigoPostal == "" || dataList[i].ubicacion.calle == "" || dataList[i].ubicacion.colonia == "" || dataList[i].ubicacion.estado == "" || dataList[i].ubicacion.ciudad == "" || dataList[i].ubicacion.municipio == ""){
                    dataList[i].DataValid = false
                }else{
                    dataList[i].DataValid = true
                }
            }
        }
        SwiftEventBus.post("ConfirmaUbicacion-Actualizar-Numeracion-Cobertura", sender: String("\(dataList.count)"))
        view?.actualizarDatos()
    }
    
    func eliminaSitio(){
        var newDataList = [ConCoberturaModel]()
        var newDataDirecciones = [ConfirmaUbicacionModel]()
        let count2 : Int = (dataDirecciones.count - 1)
        var aux : Int = 0
        var aux2 : Int = 0
        var aux3 : Int = 0
        
        for i in 0...count2{
            if(dataDirecciones[i].cobertura){
                if(aux != posLoc){
                    newDataList.append(dataList[aux])
                    newDataList[aux2].posicionLocal = aux2
                    newDataList[aux2].posicionGeneral = aux3
                    aux2 = aux2 + 1
                    newDataDirecciones.append(dataDirecciones[i])
                    aux3 = aux3 + 1
                    aux = aux + 1
                }else{
                    aux = aux + 1
                }
            }else{
                newDataDirecciones.append(dataDirecciones[i])
                aux3 = aux3 + 1
            }
            
        }
        dataDirecciones =  newDataDirecciones
        dataList = newDataList
        self.count = self.count - 1
        SwiftEventBus.post("ConfirmaUbicacion-eliminar-actualizar", sender: dataDirecciones)
        actualizar()
    }
    
    func actualizaSitio(){
        let count2 : Int = (dataDirecciones.count - 1 )
        var aux = 0
        if(count2 < 0){
            return
        }
        
        for i in 0...count2{
            if(dataDirecciones[i].cobertura){
                dataList[aux].posicionGeneral = i
                aux =  aux + 1
            }else{
            }
            
        }
    }
    
    func viewDidAppear(){
        
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
    }
    
    func detachView() {
        view = nil
    }
    
    func getItemIndex(index: Int) -> ConCoberturaModel {
        return dataList[index]
    }
    
    func getSize() -> Int{
        return dataList.count
    }
    
    func onClickSiguiente(){
        model?.GuardarSitios(dataDirecciones)
        SwiftEventBus.post("Cotizacion_DetalleSitio_DetalleSitios(actualizar)", sender: nil)
        SwiftEventBus.post("CoberturaResumen-Restart", sender: nil)
        //SwiftEventBus.post("menuTabBar-cambiarItem", sender: 4)
        self.count = 0
        self.dataList = [ConCoberturaModel]()
        self.dataDirecciones = [ConfirmaUbicacionModel]()
        self.actualizar()
        view?.cambiarSiguienteVista()
        //        print(dataDirecciones)
    }
    
    func onClickAgregar(){
        view?.Agregar()
    }
    
    func soloCotiza(){
        view!.soloCotiza()
    }
}
