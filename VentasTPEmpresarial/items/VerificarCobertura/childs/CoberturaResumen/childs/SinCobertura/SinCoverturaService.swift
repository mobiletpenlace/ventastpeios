//
//  SinCoverturaService.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 22/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import RealmSwift

class SinCoverturaServie: BaseDBManagerDelegate {
    var serverManager: ServerDataManager?
    var realm: Realm!
    var dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()
        serverManager = ServerDataManager()
        realm = getRealm()
    }
    
    func GuardarSitios(_ sitios: [ConfirmaUbicacionModel]){
        for sitio in sitios{
            dbManager.addSitioCobertura(sitio.nombreSitio, sitio.cobertura, sitio.codigoPostal, sitio.latitud, sitio.longitud, sitio.calle, sitio.numExt, sitio.numInt, sitio.colonia, sitio.estado, sitio.ciudad, sitio.colonia, sitio.entreCalle, sitio.yCalle, sitio.observaciones, sitio.categoryService, sitio.cluster, sitio.distrito, sitio.factibilidad, sitio.plaza, sitio.region, sitio.regionId, sitio.tipoCobertura, sitio.zona)
        }
    }
    
}
