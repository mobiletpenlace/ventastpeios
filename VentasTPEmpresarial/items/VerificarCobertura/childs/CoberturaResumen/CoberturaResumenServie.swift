//
//  CoberturaResumenServie.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 26/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import RealmSwift

class CoberturaResumenServie: BaseDBManagerDelegate {
    var serverManager: ServerDataManager?
    var realm: Realm!
    var dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()
        serverManager = ServerDataManager()
        realm = getRealm()
    }
    
    func validaCP(latitude: String, Longitude: String, TipoCliente: String,_ callBack:@escaping ((Bool, MiddleCoordenadasResponse?)) -> Void){
        let model = MiddleCoordenadasRequest(lat: latitude, long: Longitude, tCliente: TipoCliente)
        var res: MiddleCoordenadasResponse? = nil
        serverManager?.post(model: model, url: Constantes.Url.Middleware.Prod.consultaCobertura, tipo: .middle){ responseData in
            guard let data = responseData.data else{
                callBack((false, nil))
                return
            }
            do{
                let decoder = JSONDecoder()
                res = try decoder.decode(MiddleCoordenadasResponse.self, from: data)
            }catch let err{
                Logger.e("error: \(err)")
            }
            callBack((true,res))
        }
    }
    
}
