//
//  CoberturaResumenModel.swift
//  VentasTPEmpresarial
//
//  Created by Juan Reynaldo Escobar Miron on 26/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

struct CoberturaResumenModel {
    var Nombre: String
    var Calle: String
    var NumExt: String
    var NumInt: String
    var Colonia: String
    var Cp: String
    var Municipio: String
    var Estado: String
    var Ciudad : String
    var Latitud : String
    var Longitud : String
}
