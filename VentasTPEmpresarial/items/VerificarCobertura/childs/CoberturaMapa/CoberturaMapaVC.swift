//
//  MapsCoberturaViewController.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 09/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import SwiftEventBus

class CoberturaMapaVC: BaseItemVC {
    @IBOutlet weak var mapView: GMSMapView!
    var marker: GMSMarker? = nil
    var locationManager = CLLocationManager()
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    fileprivate let presenter = CoberturaMapaPresenter(service : ConvertirLeadService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow(){
        searchController?.isActive = true
        presenter.viewDidShow()
    }
    
    private func setUpView() {
        iniciarGoogleMaps()
        iniciarGooglePlaces()
        iniciarLocationManager()
    }
    
    @IBAction func onClickSiguiente(_ sender: UIButton) {
        presenter.onClickSiguiente()
    }
}

extension CoberturaMapaVC: CLLocationManagerDelegate{
    
    func iniciarLocationManager(){
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        Logger.e("error al onbtener la ubicacion \(error)")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        presenter.didUpdateLocations(location: locations.last!)
    }
}

extension CoberturaMapaVC: GMSMapViewDelegate{
    
    func iniciarGoogleMaps() {
        self.mapView.delegate = self
        self.mapView.isMyLocationEnabled = true
        self.mapView.settings.myLocationButton = true
        self.mapView.settings.compassButton = true
        self.mapView.settings.zoomGestures = true
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        self.mapView.isMyLocationEnabled = true
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        self.mapView.isMyLocationEnabled = true
        if (gesture){
            mapView.selectedMarker = nil
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        presenter.didTapAt(coordinate: coordinate)
    }
}

extension CoberturaMapaVC: GMSAutocompleteResultsViewControllerDelegate {
    
    func iniciarGooglePlaces(){
        resultsViewController = GMSAutocompleteResultsViewController()
        
        let filter = GMSAutocompleteFilter()
        filter.country = "mx"
        
        resultsViewController?.autocompleteFilter = filter
        resultsViewController?.delegate = self
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        definesPresentationContext = true
        
        searchController?.obscuresBackgroundDuringPresentation = false
        searchController?.searchBar.placeholder = "Ingresa la dirección"
        searchController?.searchBar.setImage(UIImage(named: "ic_google_pin"), for: UISearchBarIcon.search, state: .normal)
        resultsViewController?.tableCellBackgroundColor = UIColor(white: 0.0, alpha: 0.4)
        resultsViewController?.tableCellSeparatorColor = .black
        resultsViewController?.primaryTextColor = .white
        resultsViewController?.primaryTextHighlightColor = .white
        resultsViewController?.secondaryTextColor = .lightGray
        searchController?.searchBar.searchBarStyle = .minimal
        searchController?.searchBar.barTintColor = .black
        
        let w = self.mapView.frame.size.width
        let y = self.mapView.frame.origin.y
        let subView = UIView(frame: CGRect(x: 0, y: y, width: w, height: 50.0))
        subView.backgroundColor = UIColor(white: 1, alpha: 0.8)
        subView.addSubview((searchController?.searchBar)!)
        self.view.addSubview(subView)
        searchController?.searchBar.sizeToFit()
        searchController?.hidesNavigationBarDuringPresentation = false
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didAutocompleteWith place: GMSPlace) {
        searchController?.isActive = false
        if (marker != nil){
            marker!.map = nil
        }
        marker = GMSMarker(position: place.coordinate)
        marker?.title = ""
        marker?.map = self.mapView
        let icono = presenter.scaleImage(
            image: UIImage(named: "ic_google_pin")!,
            scaledToSize: CGSize(width: 30.0, height: 40.0)
        )
        marker?.icon = icono
        let camera = GMSCameraPosition.camera(
            withLatitude: place.coordinate.latitude + 0.003,
            longitude: place.coordinate.longitude - 0.005718469,
            zoom: 17.0
        )
        mapView.animate(to: camera)
        presenter.didTapAt(coordinate: place.coordinate)
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didFailAutocompleteWithError error: Error){
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func didRequestAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

extension CoberturaMapaVC: CoberturaMapaDelegate{
 
    func alert() {
        super.showBannerAdvertencia(title: "Selecciona una ubicacion para continuar")
    }
    
    
    func actualizarDatos() {
        
    }
    
    func cambiarSiguienteVista() {
        searchController?.isActive = false
        padreController?.cambiarVista(ConfirmaUbicacionVC.self)
    }
    
    func updateTitleHeader() {
        let header = HeaderData(
            imagen: #imageLiteral(resourceName: "logo_Sitio"),
            titulo: "Sitios",
            titulo2: "Validar Cobertura",
            titulo3: nil,
            subtitulo: "Ubicación",
            closure: { [weak self] in
                self?.searchController?.isActive = false
                self?.padreController?.cambiarVista(CoberturaResumenVC.self)
            }
        )
        super.updateTitle(headerData: header)
    }
    
    func startLoading() {
        super.showLoading(view: view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
    func moverCamaraAPosicion(location: CLLocation){
        let coordenada = location.coordinate
        let camera = GMSCameraPosition.camera(
            withLatitude: coordenada.latitude,
            longitude: coordenada.longitude,
            zoom: 17.0
        )
        mapView.animate(to: camera)
        locationManager.stopUpdatingLocation()
    }
    
    func agregarMarcador(coordinate: CLLocationCoordinate2D, titulo: String, icono: UIImage){
        if (marker != nil){
            marker!.map = nil
        }
        Logger.d(coordinate.latitude)
        Logger.d(coordinate.longitude)
        marker = GMSMarker(position: coordinate)
        marker?.title = titulo
        marker?.map = self.mapView
        marker?.icon = icono
    }
}
