//
//  CoberturaMapaPresenter.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 01/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import Foundation
import SwiftEventBus
import GoogleMaps

protocol CoberturaMapaDelegate: BaseDelegate {
    func actualizarDatos()
    func cambiarSiguienteVista()
    func moverCamaraAPosicion(location: CLLocation)
    func agregarMarcador(coordinate: CLLocationCoordinate2D, titulo: String, icono: UIImage)
    func alert()
}

class CoberturaMapaPresenter{
    //fileprivate let model: CoberturaMapaService
    weak fileprivate var view: CoberturaMapaDelegate?
    //fileprivate var data: CoberturaMapaModel?
    let geocoder = GMSGeocoder()
    fileprivate let service: ConvertirLeadService
    var isMarked = false
    var delegacion = ""
    
    init(service: ConvertirLeadService){
        self.service = service
    }
    
    func attachView(view: CoberturaMapaDelegate){
        self.view = view
        
    }
    
    func viewDidAppear(){
        
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
    }
    
    func detachView() {
        view = nil
    }
    
    //    func getData() -> CoberturaMapaModel? {
    //        return data
    //    }
    
    func didUpdateLocations(location: CLLocation){
        view?.moverCamaraAPosicion(location: location)
        Logger.d(location.coordinate.latitude)
    }
    
    func onClickSiguiente(){
        if(isMarked){
            view?.cambiarSiguienteVista()
        }else{
            view?.alert()
        }
        
    }
    
    func didTapAt(coordinate: CLLocationCoordinate2D){
        let icono = scaleImage(
            image: UIImage(named: "ic_google_pin")!,
            scaledToSize: CGSize(width: 30.0, height: 40.0)
        )   
        view?.agregarMarcador(
            coordinate: coordinate,
            titulo: "ubicacion de instalacion",
            icono: icono)
        view?.startLoading()
        coordenadaADireccion(coordinate: coordinate)
    }
    
    func coordenadaADireccion(coordinate: CLLocationCoordinate2D){
        isMarked = true
        view?.updateTextoLoading(texto: "Buscando datos de las coordenadas.")
        geocoder.reverseGeocodeCoordinate(coordinate) { [unowned self] response, error in
            if let address = response?.firstResult() {
                let str = address.thoroughfare ?? ""
                let numero = self.getStreetNumber(street: str)
                let calle = str.replacingOccurrences(of: numero, with: "", options: .literal, range: nil)
                if(address.postalCode != nil){
                    self.view?.updateTextoLoading(texto: "Buscando datos del codigo postal.")
                    self.service.consultaCp(cp: address.postalCode ?? ""){ [weak self] data in
                        self?.view?.finishLoading()
                        if(data != nil){
                            if (data!.arrColonias == nil){
                                self?.view?.showBannerError(title: "No se pudo obtener las colonias de ese lugar.")
                            }else{
                                self!.delegacion = data!.arrColonias![0].delegacionMunicipio ?? ""
                                SwiftEventBus.post(
                                    Constantes.Eventbus.Id.verificarCoberturaCambioPlan,
                                    sender: Ubicacion(
                                        nombreSitio: "",
                                        numero: numero,
                                        latitud: coordinate.latitude,
                                        longitud: coordinate.longitude,
                                        calle: calle,
                                        colonia: address.subLocality ?? "",
                                        estado: address.administrativeArea ?? "",
                                        ciudad: address.locality ?? "",
                                        codigoPostal: address.postalCode ?? "",
                                        delegacionMunicipio: self!.delegacion
                                    )
                                )
                            }
                        }else{
                            self?.view?.showBannerError(title: "Error en el servicio consulta CP, intenta mas tarde.")
                        }
                    }
                }else{
                    self.view?.finishLoading()
                    Logger.d("codigo postal en nill")
                }
            }
        }
    }
    
    func scaleImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    func getStreetNumber(street : String) -> String{
        let matched = self.matches(for: "(MZ)?[0-9]+(\\s)*(LT)?[0-9]+", in: street)
        return matched.first ?? ""
    }
    
    func matches(for regex: String, in text: String) -> [String] {
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let results = regex.matches(in: text,
                                        range: NSRange(text.startIndex..., in: text))
            return results.map {
                String(text[Range($0.range, in: text)!])
            }
        } catch let error {
            Logger.e("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
}
