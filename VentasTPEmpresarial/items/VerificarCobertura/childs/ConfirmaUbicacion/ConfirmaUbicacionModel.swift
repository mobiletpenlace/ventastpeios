//
//  ConfirmaUbicacionModel.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 01/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

struct ConfirmaUbicacionModel {
    var nombreSitio: String
    var codigoPostal: String//
    var latitud: Double//
    var longitud: Double//
    var calle: String//
    var numExt: String//
    var numInt: String//
    var colonia: String//
    var estado: String//
    var ciudad: String//
    var municipio: String//
    var entreCalle: String//
    var yCalle: String//
    var observaciones: String//
    var cobertura: Bool//
    var categoryService: String//
    var cluster: String//
    var distrito: String//
    var factibilidad: String//
    var plaza: String//
    var region: String//
    var regionId: String//
    var tipoCobertura: String//
    var zona : String//

}
