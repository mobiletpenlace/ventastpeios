//
//  ConfirmaUbicacionService.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 15/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import RealmSwift

class ConfirmaUbicacionService: BaseDBManagerDelegate {
    var serverManager: ServerDataManager?
    var realm: Realm!
    var dbManager: DBManager
    
    init() {
        dbManager = DBManager.getInstance()
        serverManager = ServerDataManager()
        realm = getRealm()
    }
    
    func validaCP(latitude: String, Longitude: String, TipoCliente: String,_ callBack:@escaping (MiddleCoordenadasResponse?) -> Void){
        let model = MiddleCoordenadasRequest(
            lat: latitude,
            long: Longitude,
            tCliente: TipoCliente
        )
        serverManager?.post(model: model, url: Constantes.Url.Middleware.Prod.consultaCobertura, tipo: .middle){ responseData in
            guard let data = responseData.data else{
                Logger.e("error en el servicio valida CP")
                callBack(nil)
                return
            }
            do{
                let decoder = JSONDecoder()
                let responseOBJ = try decoder.decode(MiddleCoordenadasResponse.self, from: data)
                callBack(responseOBJ)
            }catch let err{
                Logger.e("error: \(err)")
                callBack(nil)
            }
        }
    }
    
    func agregarValidacionASitio(_ sitio: ConfirmaUbicacionModel, _ id: Int, _ cobertura: Bool) {
         dbManager.addSitioCobertura(
            id,
            sitio.nombreSitio,
            cobertura,
            sitio.codigoPostal,
            sitio.latitud,
            sitio.longitud,
            sitio.calle,
            sitio.numExt,
            sitio.numInt,
            sitio.colonia,
            sitio.estado,
            sitio.ciudad,
            sitio.municipio,
            sitio.entreCalle,
            sitio.yCalle,
            sitio.observaciones,
            sitio.categoryService,
            sitio.cluster,
            sitio.distrito,
            sitio.factibilidad,
            sitio.plaza,
            sitio.region,
            sitio.regionId,
            sitio.tipoCobertura,
            sitio.zona
        )
    }
}
