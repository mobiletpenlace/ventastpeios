//
//  ConfirmaUbicacionPresenter.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 15/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import SwiftEventBus

protocol ConfirmaUbicacionDelegate: BaseDelegate {
    func actualizarDatos()
    func cambiarSiguienteVista()
    func mapa()
    func resumen()
    func returnToSites()
    func limpiarPantalla()
}

class ConfirmaUbicacionPresenter{
    fileprivate let service: ConfirmaUbicacionService
    weak fileprivate var view: ConfirmaUbicacionDelegate?
    fileprivate var model: ConfirmaUbicacionModel?
    fileprivate var dataList = [ConfirmaUbicacionModel]()
    var isedit = false, isNormal = true
    var id : Int!, posLoc : Int!, posGen : Int!
    
    
    init(_ service: ConfirmaUbicacionService){
        self.service = service
    }
    
    func attachView(view: ConfirmaUbicacionDelegate){
        self.view = view
        SwiftEventBus.onMainThread(self, name: Constantes.Eventbus.Id.verificarCoberturaCambioPlan) { [weak self] result in
            if let model = result!.object as? Ubicacion{
                self?.cambiarUbicacion(model: model)
            }
        }
        SwiftEventBus.onMainThread(self, name: "CoberturaResumen-validarCobertura") {
            [weak self] result in
            if let id = result?.object as? Int{
                self?.id = id
                self?.isNormal = false
            }
        }
        SwiftEventBus.onMainThread(self, name: "CoberturaResumen-validarCobertura-child") { [weak self] result in
            self?.view?.mapa()
        }
        SwiftEventBus.onMainThread(self, name: "CoberturaResumen-Restart") { [weak self] result in
            self?.dataList = [ConfirmaUbicacionModel]()
        }
        SwiftEventBus.onMainThread(self, name: "ConfirmaUbicacion-editar") { [weak self] result in
            if let data = result?.object as? ConfirmaUbicacionModel{
                self?.model = data
                self?.isedit = true
                self?.view?.actualizarDatos()
            }
        }
        SwiftEventBus.onMainThread(self, name: "ConfirmaUbicacion-editar-posLoc") { [weak self] result in
            if let posLoc = result?.object as? Int{
                self?.posLoc = posLoc
            }
        }
        SwiftEventBus.onMainThread(self, name: "ConfirmaUbicacion-editar-posGen") { [weak self] result in
            if let posGen = result?.object as? Int{
                self?.posGen = posGen
            }
        }
        SwiftEventBus.onMainThread(self, name: "ConfirmaUbicacion-eliminar-actualizar") { [weak self] result in
            if let dataList = result?.object as? [ConfirmaUbicacionModel]{
                self?.dataList = dataList
            }
        }
        SwiftEventBus.onMainThread(self, name: "CoberturaResumen-limpiar") { [weak self] result in
            self?.dataList = [ConfirmaUbicacionModel]()
        }
    }
    
    func cambiarUbicacion(model : Ubicacion){
        self.model = ConfirmaUbicacionModel(
            nombreSitio: model.nombreSitio,
            codigoPostal: model.codigoPostal ,
            latitud: model.latitud ,
            longitud: model.longitud ,
            calle: model.calle ,
            numExt: model.numero ,
            numInt: "",
            colonia: model.colonia ,
            estado: model.estado ,
            ciudad: model.ciudad,
            municipio: model.delegacionMunicipio,
            entreCalle: "",
            yCalle: "",
            observaciones:"",
            cobertura: false,
            categoryService: "",
            cluster: "",
            distrito: "",
            factibilidad: "",
            plaza: "",
            region: "",
            regionId: "",
            tipoCobertura: "",
            zona: ""
        )
        view?.actualizarDatos()// mostrar la información actualizada
    }
    
    func viewDidAppear(){
        
    }
    
    func viewDidShow(){
        view?.updateTitleHeader()
    }
    
    func detachView() {
        view = nil
    }
    
    func getData() -> ConfirmaUbicacionModel? {
        return model
    }
    
    func onClickSiguiente(_ modelo : ConfirmaUbicacionModel){
        guard model != nil else {
            Logger.w("model es nil y no se puede avanzar en confirmar ubicación.")
            return
        }
        model?.nombreSitio = modelo.nombreSitio
        model?.codigoPostal = modelo.codigoPostal
        model?.calle = modelo.calle
        model?.numExt = modelo.numExt
        model?.numInt = modelo.numInt
        model?.colonia = modelo.colonia
        model?.estado = modelo.estado
        model?.ciudad = modelo.ciudad
        model?.municipio = modelo.municipio
        model?.entreCalle = modelo.entreCalle
        model?.yCalle = modelo.yCalle
        model?.observaciones = modelo.observaciones
        if (isedit){
            if(model?.cobertura)!{
                SwiftEventBus.post("Cobertura-Editar-posLoc",sender: posLoc)
                SwiftEventBus.post("Cobertura-Editar-posGen",sender: posGen)
                SwiftEventBus.post("Cobertura-Con-Editar-Ubicacion",sender: model)
                SwiftEventBus.post("CoberturaResumen-agregarUbicacion-ConCovertura-pestaña", sender: nil)
            }else{
                SwiftEventBus.post("Cobertura-Editar-posLoc",sender: posLoc)
                SwiftEventBus.post("Cobertura-Editar-posGen",sender: posGen)
                SwiftEventBus.post("Cobertura-Sin-Editar-Ubicacion",sender: model)
                SwiftEventBus.post("CoberturaResumen-agregarUbicacion-SinCovertura-pestaña",sender: nil)
            }
            isedit = false
            self.view?.limpiarPantalla()
            self.view?.cambiarSiguienteVista()
        }else{
            view?.startLoading()
            if ConnectionUtils.isConnectedToNetwork(){
                service.validaCP(latitude: String("\(model!.latitud)"), Longitude: String("\(model!.longitud)"), TipoCliente: "RESIDENCIAL"){ [weak self] data in
                    
                    guard let data = data else {
                        self!.view?.finishLoading()
                        self!.view?.showBannerError(title: "Ocurrio un error al consultar el servicio, intenta mas tarde")
                        return
                    }
                    
                    if(data.calculaFactibilidad != nil){
                        if(self?.isNormal)!{
                            if(data.calculaFactibilidad?.factibilidad != "0"){
                                SwiftEventBus.post(
                                    "CoberturaResumen-agregarUbicacion-ConCovertura",
                                    sender: Sitio(
                                        ciudad: data.calculaFactibilidad?.cuidad ?? "",
                                        cluster:data.calculaFactibilidad?.nombre_cluster ?? "",
                                        sitio: /*data.calculaFactibilidad?.cuidad ?? */"",
                                        zona: data.calculaFactibilidad?.zona ?? "",
                                        distrito: data.calculaFactibilidad?.distrito ?? "",
                                        medioDeAcceso: /*data.calculaFactibilidad?.cuidad ??*/ "")
                                )
                                SwiftEventBus.post("CoberturaResumen-agregarUbicacion-ConCovertura-pestaña",sender: nil)
                                self?.model?.cobertura = true
                                self?.model?.categoryService = (data.calculaFactibilidad?.categoryService) ?? ""
                                self?.model?.cluster = (data.calculaFactibilidad?.nombre_cluster) ?? ""
                                self?.model?.distrito = (data.calculaFactibilidad?.distrito) ?? ""
                                self?.model?.factibilidad = (data.calculaFactibilidad?.factibilidad) ?? ""
                                self?.model?.plaza = (data.calculaFactibilidad?.cuidad) ?? ""
                                self?.model?.region = (data.calculaFactibilidad?.region) ?? ""
                                self?.model?.regionId = (data.calculaFactibilidad?.idRegion) ?? ""
                                if(data.calculaFactibilidad?.factibilidad == "5"){
                                    self?.model?.tipoCobertura = "Microondas"
                                }else if(data.calculaFactibilidad?.factibilidad == "0"){
                                    self?.model?.tipoCobertura = "No factible"
                                }else{
                                    self?.model?.tipoCobertura = "Fibra"
                                }
                                self?.model?.zona = (data.calculaFactibilidad?.zona) ?? ""
                            }else{
                                SwiftEventBus.post(
                                    "CoberturaResumen-agregarUbicacion-SinCovertura",
                                    sender: Sitio(
                                        ciudad: data.calculaFactibilidad?.cuidad ?? "",
                                        cluster: data.calculaFactibilidad?.nombre_cluster ?? "",
                                        sitio: /*data.calculaFactibilidad?.cuidad ?? */"",
                                        zona: data.calculaFactibilidad?.zona ?? "",
                                        distrito: data.calculaFactibilidad?.distrito ?? "",
                                        medioDeAcceso: /*data.calculaFactibilidad?.cuidad ??*/ "")
                                )
                                SwiftEventBus.post("CoberturaResumen-agregarUbicacion-SinCovertura-pestaña", sender: nil)
                                self?.model?.cobertura = false
                            }
                            self?.dataList.append((self?.model)!)
                            SwiftEventBus.post("CoberturaResumen-agregarUbicacion-general",sender: self?.dataList)
                            self?.view?.finishLoading()
                            self?.view?.limpiarPantalla()
                            self?.view?.cambiarSiguienteVista()
                        }else{
                            self?.model?.categoryService = (data.calculaFactibilidad?.categoryService) ?? ""
                            self?.model?.cluster = (data.calculaFactibilidad?.nombre_cluster) ?? ""
                            self?.model?.distrito = (data.calculaFactibilidad?.distrito) ?? ""
                            self?.model?.factibilidad = (data.calculaFactibilidad?.factibilidad) ?? ""
                            self?.model?.plaza = (data.calculaFactibilidad?.cuidad) ?? ""
                            self?.model?.region = (data.calculaFactibilidad?.region) ?? ""
                            self?.model?.regionId = (data.calculaFactibilidad?.idRegion) ?? ""
                            if(data.calculaFactibilidad?.factibilidad == "1"){
                                self?.service.agregarValidacionASitio((self?.model)!, (self?.id)!, true)
                            }else{
                                self?.service.agregarValidacionASitio((self?.model)!, (self?.id)!, false)
                            }
                            self?.view?.finishLoading()
                            SwiftEventBus.post("Cotizacion_DetalleSitio_DetalleSitios(actualizar)", sender: nil)
                            self?.isNormal = true
                            self?.view?.resumen()
                            //SwiftEventBus.post("menuTabBar-cambiarItem", sender: 4)
                            self?.view?.limpiarPantalla()
                            self?.view?.returnToSites()
                        }
                    }else{
                        self!.view?.finishLoading()
                        self!.view?.showBannerError(title: "Ocurrio un error al consultar el servicio, intenta mas tarde")
                    }
                }
            }else{
                self.view?.finishLoading()
                self.view?.showBannerError(title: "Sin conneccion a internet, intenta mas tarde")
                
            }
            
        }
    }
    
}
