//
//  ConfirmaUbicacionVC.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 09/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import UIKit

class ConfirmaUbicacionVC: BaseItemVC {
    @IBOutlet weak var labelNombreSitio: UITextField!
    @IBOutlet weak var labelCodigoPostal: UITextField!
    @IBOutlet weak var labelCalle: UITextField!
    @IBOutlet weak var labelNumeroExterior: UITextField!
    @IBOutlet weak var labelNumeroInterior: UITextField!
    @IBOutlet weak var labelColonia: UITextField!
    @IBOutlet weak var labelEstado: UITextField!
    @IBOutlet weak var labelCiudad: UITextField!
    @IBOutlet weak var labelDelegacionMunicipio: UITextField!
    @IBOutlet weak var labelEntreCalle: UITextField!
    @IBOutlet weak var labelYCalle: UITextField!
    @IBOutlet weak var textViewObservaciones: UITextView!
    fileprivate let presenter = ConfirmaUbicacionPresenter(ConfirmaUbicacionService())
    var modelo: ConfirmaUbicacionModel!
    public var mFormValidator : FormValidator!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(view: self)
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidShow(){
        presenter.viewDidShow()
    }
    
    private func setUpView() {
        textViewObservaciones.layer.borderWidth = 1
        textViewObservaciones.layer.borderColor = UIColor.lightGray.cgColor
        labelNumeroExterior.delegate = self
        labelNumeroInterior.delegate = self
        labelDelegacionMunicipio.delegate = self
        labelCodigoPostal.delegate = self
    }
    
    
    @IBAction func onClickSiguiente(_ sender: UIButton) {
        self.view.endEditing(true)
        modelo = ConfirmaUbicacionModel(
            nombreSitio: labelNombreSitio.text!,
            codigoPostal: labelCodigoPostal.text!,
            latitud: 0,
            longitud: 0,
            calle: labelCalle.text!,
            numExt: labelNumeroExterior.text!,
            numInt: labelNumeroInterior.text!,
            colonia: labelColonia.text!,
            estado: labelEstado.text!,
            ciudad: labelCiudad.text!,
            municipio: labelDelegacionMunicipio.text!,
            entreCalle: labelEntreCalle.text!,
            yCalle: labelYCalle.text!,
            observaciones: textViewObservaciones.text,
            cobertura: false,
            categoryService: "",
            cluster: "",
            distrito: "",
            factibilidad: "",
            plaza: "",
            region: "",
            regionId: "",
            tipoCobertura: "",
            zona: "")
        presenter.onClickSiguiente(modelo)
    }
    
}

extension ConfirmaUbicacionVC: ConfirmaUbicacionDelegate{
    
    func limpiarPantalla() {
        self.labelCodigoPostal.text = ""
        self.labelCalle.text = ""
        self.labelNumeroExterior.text = ""
        self.labelNumeroInterior.text = ""
        self.labelColonia.text = ""
        self.labelEstado.text =  ""
        self.labelCiudad.text =  ""
        self.labelDelegacionMunicipio.text =  ""
        self.labelEntreCalle.text =  ""
        self.labelYCalle.text = ""
        self.textViewObservaciones.text =  ""
        self.labelNombreSitio.text = ""
    }
    
    func returnToSites() {
        padreController?.cambiarVista(DetalleSitioVC.self)
    }
    
    func mapa() {
        padreController?.cambiarVista(CoberturaMapaVC.self)
    }
    
    func resumen() {
        padreController?.cambiarVista(CoberturaResumenVC.self)
    }
    
    
    func actualizarDatos() {
        let data = presenter.getData()
        self.labelNombreSitio.text = data?.nombreSitio ?? ""
        isEmpty(self.labelNombreSitio)
        self.labelCodigoPostal.text = data?.codigoPostal ?? ""
        isEmpty(self.labelCodigoPostal)
        self.labelCalle.text = data?.calle ?? ""
        isEmpty(self.labelCalle)
        let nExt = data?.numExt ?? ""
        if(nExt.count > 8){
            self.labelNumeroExterior.text = ""
        }else{
            self.labelNumeroExterior.text = data?.numExt ?? ""
        }
        self.labelNumeroInterior.text = data?.numInt ?? ""
        self.labelColonia.text = data?.colonia ?? ""
        isEmpty(self.labelColonia)
        self.labelEstado.text = data?.estado ?? ""
        isEmpty(self.labelEstado)
        self.labelCiudad.text = data?.ciudad ?? ""
        isEmpty(self.labelCiudad)
        self.labelDelegacionMunicipio.text = data?.municipio ?? ""
        isEmpty(self.labelDelegacionMunicipio)
        self.labelEntreCalle.text = data?.entreCalle ?? ""
        self.labelYCalle.text = data?.yCalle ?? ""
        self.textViewObservaciones.text = data?.observaciones ?? ""
    }
    
    func isEmpty(_ label : UITextField){
        if(label.text != ""){
            label.isEnabled = false
        }else{
            label.isEnabled = true
        }
    }
    
    func updateTitleHeader() {
        let header = HeaderData(
            imagen: #imageLiteral(resourceName: "logo_Sitio"),
            titulo: "Sitios",
            titulo2: "Validar Cobertura",
            titulo3: nil,
            subtitulo: "Confirmación de Ubicación",
            closure:{ [weak self] in
                self?.padreController?.cambiarVista(CoberturaMapaVC.self)
            }
        )
        super.updateTitle(headerData: header)
    }
    
    func startLoading() {
        super.showLoading(view: view)
    }
    
    func finishLoading() {
        super.hideLoading()
    }
    
    func cambiarSiguienteVista(){
        padreController?.cambiarVista(CoberturaResumenVC.self)
    }
    
}


extension ConfirmaUbicacionVC: UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField {
            
        case labelDelegacionMunicipio:
            let ACCEPTABLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            
            return (string == filtered)
            
        case labelCodigoPostal:
            let letras = CharacterSet.decimalDigits
            let limiteDeCaracteres = 5
            
            //revisamos si el caracter ingresado es válido
            if string.rangeOfCharacter(from: letras.inverted) != nil {
                return false
            }
            //revisamos si el caracter no rebasa el límite de caracteres
            if ((textField.text?.count)! + (string.count - range.length)) > limiteDeCaracteres {
                return false
            }else{
                return true
            }
        case labelNumeroExterior:
            let limiteDeCaracteres = 8
            
            //revisamos si el caracter no rebasa el límite de caracteres
            if ((textField.text?.count)! + (string.count - range.length)) > limiteDeCaracteres {
                return false
            }else{
                return true
            }
            
        case labelNumeroInterior:
            let limiteDeCaracteres = 8
            
            //revisamos si el caracter no rebasa el límite de caracteres
            if ((textField.text?.count)! + (string.count - range.length)) > limiteDeCaracteres {
                return false
            }else{
                return true
            }
            
            
        default:
            var letras = CharacterSet.letters
            letras.insert(charactersIn: " ")
            if string.rangeOfCharacter(from: letras.inverted) != nil {
                return false
            }
            return true
        }
    }
}
