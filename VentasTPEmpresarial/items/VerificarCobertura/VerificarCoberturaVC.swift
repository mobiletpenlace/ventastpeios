//
//  VerifyCoverageViewController.swift
//  VentasTPEmpresarial
//
//  Created by antonio lavin on 01/06/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class VerificarCoberturaVC: BaseMenuItemVC, TimeMeasureDelegate{
    @IBOutlet weak var viewContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tiempoInicio = iniciarTiempo()
        
        super.subscribe(nameItem: Constantes.Eventbus.Id.menuItemVerificaCobertura)
        super.headerData = HeaderData(
            imagen: #imageLiteral(resourceName: "logo_Sitio"),
            titulo: "Sitios",
            titulo2: "validar cobertura",
            titulo3: nil,
            subtitulo: nil,
            closure: { [weak self] in
                self?.padreController?.cambiarVista(index: 0)
            }
        )
        addItems()
        medirTiempo(tipoInicio: tiempoInicio, nombre: "Verificar Cobertura")
    }
    
    func addItems(){
        var controllers = [BaseItemVC]()
        controllers.append(CoberturaResumenVC())
        controllers.append(CoberturaMapaVC())
        controllers.append(ConfirmaUbicacionVC())
        super.addViewXIBList(view: viewContainer, viewControllers: controllers)
    }
    
}
