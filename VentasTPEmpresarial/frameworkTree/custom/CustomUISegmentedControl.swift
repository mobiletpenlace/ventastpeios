//
//  CustomUISegmentedControl.swift
//  HelloWorldProyect
//
//  Created by julian dorantes fuertes on 07/06/18.
//  Copyright © 2018 total. All rights reserved.
//

import UIKit

class CustomUISegmentedControl: UISegmentedControl {
    var currentIndex: Int = 0

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }

    private func configure() {
        let font: UIFont = UIFont(name: "Montserrat", size: 16)!
        let font2: UIFont = UIFont(name: "Montserrat-SemiBold", size: 16)!
        self.backgroundColor = .clear
        self.tintColor = .clear
        self.setTitleTextAttributes([
            NSAttributedStringKey.font: font,
            NSAttributedStringKey.foregroundColor: UIColor.darkGray
            ], for: .normal)
        self.setTitleTextAttributes([
            NSAttributedStringKey.font: font2,
            NSAttributedStringKey.foregroundColor: UIColor.black
            ], for: .selected)
        addLine()
    }

    func addLine() {
        let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
        let underlineHeight: CGFloat = 2.0
        let underlineXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth))
        let underLineYPosition = self.bounds.size.height - 1.0
        let underlineFrame = CGRect(x: underlineXPosition, y: underLineYPosition, width: underlineWidth, height: underlineHeight)
        let underline = UIView(frame: underlineFrame)
        underline.backgroundColor = UIColor.red
        underline.tag = 1
        self.addSubview(underline)
    }

    func changeUnderlinePosition(_ newIndex: Int) {
        guard let underline = self.viewWithTag(1) else { return }
        let underlineFinalXPosition = (self.bounds.width / CGFloat(self.numberOfSegments)) * CGFloat(selectedSegmentIndex)
        underline.frame.size.width = self.bounds.size.width / CGFloat(self.numberOfSegments)
        UIView.animate(withDuration: 0.2, animations: {
            underline.frame.origin.x = underlineFinalXPosition
        })
    }
}
