//
//  TimeMeasureDelegate.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 20/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

protocol TimeMeasureDelegate {
    func iniciarTiempo() -> Date
    func medirTiempo(tipoInicio: Date, nombre: String)
}

extension TimeMeasureDelegate {

    func iniciarTiempo() -> Date {
        return Date()
    }

    func medirTiempo(tipoInicio: Date, nombre: String) {
        let diferencia = Date().timeIntervalSince(tipoInicio)
        Logger.v("log: --> \(nombre): El tiempo toma \(diferencia) segundos")
    }

}
