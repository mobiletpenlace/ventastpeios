//
//  BaseFormatNumber.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 25/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
protocol BaseFormatNumber {
    func numeroAFormatoPrecio(numero: Float) -> String
}

extension BaseFormatNumber {
    func numeroAFormatoPrecio(numero: Float) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        numberFormatter.minimumFractionDigits = 2
        numberFormatter.maximumFractionDigits = 2
        let formattedNumber = numberFormatter.string(from: NSNumber(value: numero))
        return formattedNumber ?? "0.0"
    }
}
