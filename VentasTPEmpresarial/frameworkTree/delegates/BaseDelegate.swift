//
//  BaseTreeDelegate.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 22/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import NotificationBannerSwift

protocol BaseDelegate: NSObjectProtocol {
    func updateTitleHeader()
    func startLoading()
    func finishLoading()
    func updateTextoLoading(texto: String)
    
    func showBanner(title: String, style: BannerStyle)
    func showBanner(title: String, subtitle: String, style: BannerStyle)
    func showBanner(title: String, subtitle: String, style: BannerStyle, leftView: UIImageView)
    func showBanner(title: String, subtitle: String, style: BannerStyle, leftView: UIImageView, rightView: UIImageView)
    
    func showBannerOK(title: String)
    func showBannerError(title: String)
    func showBannerAdvertencia(title: String)
}
