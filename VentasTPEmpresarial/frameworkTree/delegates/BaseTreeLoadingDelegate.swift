//
//  BaseTreeLoading.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 10/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

protocol BaseTreeLoadingDelegate {
    func showLoading(onView: UIView) -> UIView
    func hideLoading(spinner: UIView)
}

extension BaseTreeLoadingDelegate {

    func showLoading(onView: UIView) -> UIView {
        let spinnerView = UIView()
        spinnerView.frame = CGRect(x: 0, y: 0, width: 200, height: 100)
        spinnerView.center = onView.center
        spinnerView.backgroundColor = UIColor.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.5)
        spinnerView.layer.cornerRadius = 5

        let ai = UIActivityIndicatorView.init(activityIndicatorStyle: .whiteLarge)
        ai.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        ai.startAnimating()
        ai.center = CGPoint(x: spinnerView.frame.size.width / 2, y: spinnerView.frame.size.height / 2)
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        return spinnerView
    }

    func hideLoading(spinner: UIView) {
        DispatchQueue.main.async {
            spinner.removeFromSuperview()
        }
    }
}
