//
//  ImageWriteReadDelegate.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 28/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
protocol BaseImageWriteReadDelegate {
    func guardarImagen(img: UIImage, nombre: String)
    func leerImagen(nombre: String) -> UIImage?
}

extension BaseImageWriteReadDelegate {

    func guardarImagen(img: UIImage, nombre: String) {
        if let data = UIImageJPEGRepresentation(img, 0.6) {
            let filename = getDocumentsDirectory().appendingPathComponent(nombre)
            try! data.write(to: filename)
            Logger.d("guardada la imagen: \(nombre)")
        }
    }

    func leerImagen(nombre: String) -> UIImage? {
        let fileManager = FileManager.default
        let filename = getDocumentsDirectory().appendingPathComponent(nombre)
        if fileManager.fileExists(atPath: filename.path) {
            let image = UIImage(contentsOfFile: filename.path)
            Logger.d("leida la imagen: \(nombre)")
            return image
        } else {
            Logger.e("error en imagen")
            return UIImage()
        }
    }

    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }

}
