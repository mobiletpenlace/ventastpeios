//
//  BaseAlertView.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 28/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

protocol BaseAlertViewDelegate {
    func alert(_ title: String, _ message: String, _ vc: UIViewController, _ titulobutton1: String, _ closureBtn1: ((UIAlertAction) -> Void)?)
    func alert(_ title: String, _ message: String, _ vc: UIViewController, _ titulobutton1: String, _ closureBtn1: ((UIAlertAction) -> Void)?, _ titulobutton2: String, _ closureBtn2: ((UIAlertAction) -> Void)?)

    func alert(_ title: String, _ message: String, _ vc: UIViewController, _ titulobutton1: String, _ closureBtn1: ((UIAlertAction) -> Void)?, _ titulobutton2: String, _ closureBtn2: ((UIAlertAction) -> Void)?, _ titulobutton3: String, _ closureBtn3: ((UIAlertAction) -> Void)?)
}

extension BaseAlertViewDelegate {

    func alert(_ title: String, _ message: String, _ vc: UIViewController, _ titulobutton1: String, _ closureBtn1: ((UIAlertAction) -> Void)?) {
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: UIAlertControllerStyle.alert
        )
        alert.addAction(UIAlertAction(title: titulobutton1, style: UIAlertActionStyle.default, handler: closureBtn1))
        vc.present(alert, animated: true, completion: nil)
    }

    func alert(_ title: String, _ message: String, _ vc: UIViewController, _ titulobutton1: String, _ closureBtn1: ((UIAlertAction) -> Void)?, _ titulobutton2: String, _ closureBtn2: ((UIAlertAction) -> Void)?) {
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: UIAlertControllerStyle.alert
        )
        alert.addAction(UIAlertAction(title: titulobutton1, style: UIAlertActionStyle.default, handler: closureBtn1))
        alert.addAction(UIAlertAction(title: titulobutton2, style: UIAlertActionStyle.default, handler: closureBtn2))
        vc.present(alert, animated: true, completion: nil)
    }

    func alert(_ title: String, _ message: String, _ vc: UIViewController, _ titulobutton1: String, _ closureBtn1: ((UIAlertAction) -> Void)?, _ titulobutton2: String, _ closureBtn2: ((UIAlertAction) -> Void)?, _ titulobutton3: String, _ closureBtn3: ((UIAlertAction) -> Void)?) {
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: UIAlertControllerStyle.alert
        )
        alert.addAction(UIAlertAction(title: titulobutton1, style: UIAlertActionStyle.default, handler: closureBtn1))
        alert.addAction(UIAlertAction(title: titulobutton2, style: UIAlertActionStyle.default, handler: closureBtn2))
        alert.addAction(UIAlertAction(title: titulobutton3, style: UIAlertActionStyle.default, handler: closureBtn3))
        vc.present(alert, animated: true, completion: nil)
    }
}
