//
//  BaseDBManager.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 06/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//
import RealmSwift

protocol BaseDBManagerDelegate {
    var serverManager: ServerDataManager? { get }
    var dbManager: DBManager { get set }
    func getRealm() -> Realm
    func getDBManager() -> DBManager
    func getData<REQ, RES>(model: REQ, response: RES.Type, url: String, _ tipo: TipoRequest, _ callBack: @escaping (RES?) -> Void) where REQ: Codable, RES: Codable
    func getIdEmpleado() -> String
}

extension BaseDBManagerDelegate {

    func getRealm() -> Realm {
        let dbManager = DBManager.getInstance()
        dbManager.reloadRealm()
        return dbManager.getRealm()
    }

    func getDBManager() -> DBManager {
        return DBManager.getInstance()
    }
    
    func getData<REQ, RES>(model: REQ, response: RES.Type, url: String, _ tipo: TipoRequest, _ callBack: @escaping (RES?) -> Void) where REQ: Codable, RES: Codable {
        serverManager?.getData(model: model, response: response, url: url, tipo, callBack)
    }
    
    func getIdEmpleado() -> String {
        return dbManager.getConfig().idEmpleado
    }
}
