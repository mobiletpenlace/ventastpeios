//
//  BaseTreeDialogs.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 21/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import UIKit

protocol BaseTreeDialogs {
    func showAlert(titulo: String, mensaje: String, vc: UIViewController)
    func showAlertYesNo(titulo: String, mensaje: String, vc: UIViewController, _ closure: ((UIAlertAction) -> Void)?)
    func showAlertYesNo(titulo: String, mensaje: String, vc: UIViewController, closureY: ((UIAlertAction) -> Void)?, closureN: ((UIAlertAction) -> Void)?)
    func showDialog(vc: UIViewController, customAlert: BaseDialogCustom)
}

extension BaseTreeDialogs {

    func showDialog(vc: UIViewController, customAlert: BaseDialogCustom) {
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        vc.present(customAlert, animated: true, completion: nil)
    }

    func showAlert(titulo: String, mensaje: String, vc: UIViewController) {
        let ac = UIAlertController(title: titulo, message: mensaje, preferredStyle: .alert)
        ac.addAction(
            UIAlertAction(title: "OK", style: .default)
        )
        vc.present(ac, animated: true)
    }

    func showAlertYesNo(titulo: String, mensaje: String, vc: UIViewController, _ closure: ((UIAlertAction) -> Void)?) {
        let alert = UIAlertController(
            title: titulo,
            message: mensaje,
            preferredStyle: UIAlertControllerStyle.alert
        )
        alert.addAction(UIAlertAction(title: "Si", style: UIAlertActionStyle.default, handler: closure))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }

    func showAlertYesNo(titulo: String, mensaje: String, vc: UIViewController, closureY: ((UIAlertAction) -> Void)?, closureN: ((UIAlertAction) -> Void)?) {
        let alert = UIAlertController(
            title: titulo,
            message: mensaje,
            preferredStyle: UIAlertControllerStyle.alert
        )
        alert.addAction(UIAlertAction(title: "Si", style: UIAlertActionStyle.default, handler: closureY))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: closureN))
        vc.present(alert, animated: true, completion: nil)
    }
}
