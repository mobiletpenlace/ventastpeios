//
//  ServerDataManager.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 16/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation
import Alamofire

struct ResponseData {
    let data: Data?
    let status: ServerStatus
}

enum ServerStatus {
    case ok, fail(String), tokenExpired
}

enum TipoRequest {
    case sf, middle, form
}

class ServerDataManager {
    let jsonEncoder = JSONEncoder()
    let jsonDecoder = JSONDecoder()
    var manager: Alamofire.SessionManager = {
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            "https://189.203.181.233": .disableEvaluation,
            "189.203.181.233": .disableEvaluation,

            "https://cs3.salesforce.com": .disableEvaluation,
            "cs3.salesforce.com": .disableEvaluation

        ]
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        let manager = Alamofire.SessionManager(
            configuration: URLSessionConfiguration.default,
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )
        return manager
    }()

    func objToJSON<A: Codable>(model: A) -> String {
        let jsonData = try? jsonEncoder.encode(model)
        let jsonString = String(data: jsonData!, encoding: .utf8)
        return jsonString ?? ""
    }

    func getHeadersSalesFo() -> [String: String] {
        let headers = [
            "Accept": "application/json; charset=UTF-8",
            "Content-Type": "application/json; charset=UTF-8",
            "Authorization": "Bearer " + Variables.TOKEN
        ]
        return headers
    }

    func getHeadersMiddle() -> [String: String] {
        let headers = [
            "Accept": "application/json; charset=UTF-8",
            "Content-Type": "application/json; charset=UTF-8",
            "Authorization": "Basic ZmZtYXBwOjRnZW5kNG1pM250bw== "
        ]
        return headers
    }

    func getHeadersForm() -> [String: String] {
        let headers = [
            "Accept": "application/json; charset=UTF-8",
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
            "Authorization": "Bearer 00DQ000000EeC6l!AQgAQE2n9BVsr_26jbsRuv2wMm_7Rqfxo.MRlcocahx2r3WtOITNK0VZgeiEC6wqv73EP7_C2Lhf3hwnUNm5tlXps0.pLkqm"
        ]
        return headers
    }
}

extension ServerDataManager: BaseBannerDelegate {

    private func createRequest(url path: String, headers: [String: String]) -> URLRequest {
        var request = URLRequest(url: URL(string: path)!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.timeoutInterval = 120 // 10 secs
        for header in headers {
            request.setValue(header.value, forHTTPHeaderField: header.key)
        }
        return request
    }

    private func requestManager(_ request: URLRequest, closure: @escaping (ResponseData) -> Void) {
        if ConnectionUtils.isConnectedToNetwork() {
            manager.request(request).responseJSON { response in
                var responseData: ResponseData?
                switch response.result {
                case .success:
                    responseData = ResponseData(data: response.data, status: ServerStatus.ok)
                case .failure(let error):
                    let statusCode = response.response?.statusCode ?? -1
                    if Constantes.Config.showHTTPLogs {
                        Logger.d("Error code en failure method: \(statusCode)")
                    }
                    if statusCode == 401 {
                        if Constantes.Config.showHTTPLogs {
                            Logger.e("error: El token ha expirado")
                        }
                        responseData = ResponseData(data: nil, status: ServerStatus.tokenExpired)
                    } else {
                        let strError = error.localizedDescription
                        if Constantes.Config.showHTTPLogs {
                            Logger.e("error: \(strError)")
                        }
                        responseData = ResponseData(data: nil, status: ServerStatus.fail(strError))
                    }
                }
                if let result = response.result.value {
                    let statusCode = response.response!.statusCode
                    if Constantes.Config.showHTTPLogs {
                        Logger.d("response statusCode:  \(String(describing: statusCode))")
                        if let JSON = result as? NSDictionary {
                            Logger.d("response: \(JSON)")
                        }
                    }
                }
                if let responseData = responseData {
                    closure(responseData)
                }
            }
        } else {
            let strError = "No hay conexion a internet."
            Logger.e(strError)
            showBannerError(title: "No hay conexion a internet.")
            closure(ResponseData(data: nil, status: ServerStatus.fail(strError)))
        }
    }

    func post(model: String, url path: String, tipo: TipoRequest, closure: @escaping (ResponseData) -> Void) {
//        Logger.println("request model: \(model)")
        let data = model.data(using: .utf8)
        post(url: path, tipo: tipo, data: data, closure: closure)
    }

    func post<A: Codable>(model: A, url path: String, tipo: TipoRequest, closure: @escaping (ResponseData) -> Void) {
        if Constantes.Config.showHTTPLogs {
            Logger.d("request json: \(objToJSON(model: model))")
        }
        do {
            let data = try jsonEncoder.encode(model)
            post(url: path, tipo: tipo, data: data, closure: closure)
        } catch {
            let strError = "Error al crear el request en json."
            Logger.e(strError)
            closure(ResponseData(data: nil, status: ServerStatus.fail(strError)))
        }
    }

    func post(url path: String, tipo: TipoRequest, data: Data? = nil, closure: @escaping (ResponseData) -> Void) {
        var headers = [String: String]()
        switch tipo {
        case .sf:
            headers = getHeadersSalesFo()
        case .middle:
            headers = getHeadersMiddle()
        case .form:
            headers = getHeadersForm()
        }
        var request = createRequest(url: path, headers: headers)
        if let data = data {
            request.httpBody = data
        }
        requestManager(request, closure: closure)
    }

    func get(url path: String, parameters: Parameters?, closure: @escaping (ResponseData) -> Void) {
        if ConnectionUtils.isConnectedToNetwork() {
            manager.request(path, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { response in
                switch response.result {
                case .success:
                    closure(ResponseData(data: response.data, status: ServerStatus.ok))
                case .failure(let error):
                    let strError = error.localizedDescription
                    Logger.e(strError)
                    closure(ResponseData(data: nil, status: ServerStatus.fail(strError)))
                }
            }
        } else {
            let strError = "No hay conexion a internet."
            Logger.e(strError)
            showBannerError(title: "No hay conexion a internet.")
            closure(ResponseData(data: nil, status: ServerStatus.fail(strError)))
        }
    }

    
    func getData<REQ, RES>(model: REQ, response: RES.Type, url: String, _ tipo: TipoRequest, _ callBack:@escaping (RES?) -> Void) where REQ: Codable, RES: Codable {
        post(model: model, url: url, tipo: tipo) { responseData in
            guard let data = responseData.data else {
                Logger.e("error en el servicio ")
                callBack(nil)
                return
            }
            do {
                let decoder = JSONDecoder()
                let responseOBJ = try decoder.decode(response, from: data)
                callBack(responseOBJ)
            } catch let err {
                Logger.e("error: --> \(err)")
                callBack(nil)
            }
        }
    }
}
