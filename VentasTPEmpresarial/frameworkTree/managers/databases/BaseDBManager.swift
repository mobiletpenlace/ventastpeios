//
//  BaseDbManager.swift
//  VentasTPEmpresarial
//
//  Created by eduardo mancilla on 2/22/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import RealmSwift

class BaseDBManager {
    private var realm: Realm
    static var instancia: BaseDBManager?

    private init() {
        let config = Realm.Configuration(
            schemaVersion: 1,
            migrationBlock: { _, oldSchemaVersion in
                if oldSchemaVersion < 1 {
                    // Apply any necessary migration logic here.
                }
            }, deleteRealmIfMigrationNeeded: true
        )
        Realm.Configuration.defaultConfiguration = config
        realm = try! Realm()
    }

    func getRealm() -> Realm {
        return realm
    }

    func reloadRealm() {
        realm = try! Realm()
    }

    public static func getInstance() -> BaseDBManager {
        if instancia == nil {
            instancia = BaseDBManager()
        }
        return instancia!
    }

    func incrementID<T: Object>(tipo: T.Type) -> Int {
        reloadRealm()
        return (realm.objects(tipo).max(ofProperty: "id") as Int? ?? 0) + 1
    }

    func find<T: Object>(_ tipo: T.Type, _ id: String) -> T? {
        return realm.object(ofType: tipo, forPrimaryKey: id)
    }

    func find<T: Object>(_ tipo: T.Type, _ id: Int) -> T? {
        return realm.object(ofType: tipo, forPrimaryKey: id)
    }

    func find<T: Object>(_ tipo: T.Type, _ field: String, _ value: Any) -> T? {
        return realm.objects(tipo).filter("\(field) == '\(value)'").first
    }

    func all<T: Object>(_ tipo: T.Type) -> Results<T> {
        return realm.objects(tipo)
    }

    func all<T: Object>(_ tipo: T.Type, _ ascending: Bool) -> Results<T> {
        return realm.objects(tipo).sorted(byKeyPath: "id", ascending: ascending)
    }

    func all<T: Object>(_ tipo: T.Type, _ byKeyPath: String, _ ascending: Bool) -> Results<T> {
        return realm.objects(tipo).sorted(byKeyPath: byKeyPath, ascending: ascending)
    }

    func add<T: Object>(_ obj: T) {
        reloadRealm()
        try! realm.write {
            realm.add(obj, update: true)
        }
    }

    func add<T: Object>(_ obj: T, update: Bool) {
        reloadRealm()
        try! realm.write {
            realm.add(obj, update: update)
        }
    }

    func append<T: Object, O: List<T>>(_ obj: O, _ append: T) {
        reloadRealm()
        try! realm.write {
            obj.append(append)
        }
    }

    func eliminarAllData() {
        reloadRealm()
        try! realm.write {
            realm.deleteAll()
        }
    }

}
