//
//  BaseMenuItemVC.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 07/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class BaseMenuVC: BaseTreeVC, BaseTreeDelegate {
    var headerView: HeaderViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        super.delegate = self
        SwiftEventBus.onMainThread(self, name: Constantes.Eventbus.Id.menu) { [weak self] _ in
            self?.actualizarHeaderTitle()
        }
    }

    func addHeaderView(container: UIView) {
        headerView = HeaderViewController()
        addViewXIB(vc: headerView!, container: container)
    }

    /*protocol*/
    func onChangeItem(pos: Int) {
        actualizarHeaderTitle()
        if let controller = viewControllersItems[pos] as? BaseMenuItemVC {
            controller.viewDidShow()
        }
    }

    func actualizarHeaderTitle() {
        let pos = getPosChildSelected()
        guard CodeRep.verifyRangeArray(array: viewControllersItems, index: pos) == true else {
            Logger.w("BaseMenuVC: no se va a actualizar los header: index out \(pos)")
            return
        }
        if let controller = viewControllersItems[pos] as? BaseMenuItemVC {
            SwiftEventBus.post(Constantes.Eventbus.Id.header, sender: controller.headerData)//cambiar el encabezado
        }
    }

    /*protocol*/
    func onAddItem(vc: BaseTreeVC) { }
}
