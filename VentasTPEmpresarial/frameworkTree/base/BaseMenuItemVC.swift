//
//  BaseMenuItemVC.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 07/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import SwiftEventBus

class BaseMenuItemVC: BaseTreeVC, BaseTreeDelegate {
    private var posicionActual: Int = 0
    private var header: HeaderData?
    private var nameItem: String = ""
    var headerData: HeaderData? {
        get { return header }
        set {
            header = newValue
            SwiftEventBus.post(Constantes.Eventbus.Id.menu, sender: nil)//llamar a actualizar el menu
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        super.delegate = self
        headerData = HeaderData(
            imagen: UIImage(),
            titulo: nil,
            titulo2: nil,
            titulo3: nil,
            subtitulo: nil,
            closure: nil
        )
    }

    func subscribe(nameItem: String) {
        self.nameItem = nameItem
        SwiftEventBus.onMainThread(self, name: nameItem) { [weak self] result in
            self?.headerData = result!.object as? HeaderData
        }
    }

    func viewDidShow() {
        if let vc = getViewChildSelected() as? BaseItemVC {
            vc.viewDidShow()
        }
    }

    /*protocol*/
    func onChangeItem(pos: Int) {
        posicionActual = pos
        viewDidShow()
    }

    func onAddItem(vc: BaseTreeVC) {
        if let item = vc as? BaseItemVC {
            item.setSubscribeTitle(nameItem: self.nameItem)
        }
    }
}
