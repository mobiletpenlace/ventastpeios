//
//  BaseViewController.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 02/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftEventBus

class BaseItemVC: BaseTreeVC, BaseTreeDelegate, BaseTreeEfectsDelegate, BaseBannerDelegate {
    var posicionActual: Int = 0
    var show: Bool = false
    var nameEventBusId: String = ""
    var spinner: UIView?
    var cortina: UIView?
    var textoLoading = "Cargando..."
    var textfieldLoading: UILabel?

    override func viewDidLoad() {
        if Constantes.Config.showInfoLifeCycle{
            Logger.i("info")
        }
        super.viewDidLoad()
        super.delegate = self
    }

    /*se asigna el id del menuItem a llamar, cuando se deba actualizar el titulo.
    Esta funcion es llamada automaticamente por MenuItem*/
    func setSubscribeTitle(nameItem: String) {
        self.nameEventBusId = nameItem
        for vc in super.viewControllersItems {
            if let item = vc as? BaseItemVC {
                item.setSubscribeTitle(nameItem: nameItem)//actualizar los views hijos
            }
        }
    }

    /*Actualiza el menu del item*/
    func updateTitle(headerData: HeaderData) {
        if Constantes.Config.showInfoLifeCycle{
            Logger.i("info")
        }
        SwiftEventBus.post(nameEventBusId, sender: headerData)
    }

    /*protocol*/
    func onChangeItem(pos: Int) {
        posicionActual = pos
        viewDidShow()
    }

    func onShow() {
        show = true
    }

    func onFinish() {

    }

    /*cuando se deba ejecutar la actualizacion del titulo o cualquier accion
     cuando se ha mostrado la vista actual*/
    func viewDidShow() {
        if Constantes.Config.showInfoLifeCycle{
            Logger.i("info")
        }
        for vc in viewControllersSubViews {
            if let vc = vc as? BaseItemVC {
                vc.viewDidShow()
            }
        }
        if let vc = getViewChildSelected() as? BaseItemVC {
            vc.viewDidShow()
        }
    }

    /*protocol*/
    func onAddItem(vc: BaseTreeVC) {

    }

}

extension BaseItemVC {

    func showAlert(titulo: String, mensaje: String) {
        let ac = UIAlertController(title: titulo, message: mensaje, preferredStyle: .alert)
        ac.addAction(
            UIAlertAction(title: "OK", style: .default)
        )
        present(ac, animated: true)
    }

    func showDialog(customAlert: BaseDialogCustom) {
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(customAlert, animated: true, completion: nil)
    }

    func showDialog(customAlert: BaseDialogCustom, delegate: CustomAlertViewDelegate) {
        customAlert.delegate = delegate
        showDialog(customAlert: customAlert)
    }

    private func addView(parentView: UIView, vc: UIViewController) {
        parentView.addSubview(vc.view)
        parentView.frame = vc.view.bounds
        vc.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }

    private func displayCortina(onView: UIView) -> UIView {
        let spinnerView = UIView()
        let w = onView.frame.size.width
        let h = onView.frame.size.height
        spinnerView.frame = CGRect(x: 0, y: 0, width: w, height: h)

        spinnerView.center = onView.center
        spinnerView.backgroundColor = Colores.transparente

        DispatchQueue.main.async {
            onView.addSubview(spinnerView)
        }

        return spinnerView
    }

    private func displaySpinner(onView: UIView) -> UIView {
        let spinnerView = UIView()
        spinnerView.frame = CGRect(x: 0, y: 0, width: 200, height: 100)
        addBorder(view: spinnerView)
        addShadowView(view: spinnerView)
        spinnerView.center = onView.center
        spinnerView.backgroundColor = Colores.blanco
        spinnerView.layer.cornerRadius = 5

        let ai = UIActivityIndicatorView.init(activityIndicatorStyle: .whiteLarge)
        ai.color = Colores.azulOscuro
        ai.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        ai.startAnimating()
        ai.center = CGPoint(x: spinnerView.frame.size.width / 2, y: spinnerView.frame.size.height / 2 - 10)

//        let ai = NVActivityIndicatorView(
//            frame: CGRect(x: 0, y: 0, width: 60,  height: 60),
//            type: NVActivityIndicatorType.ballClipRotateMultiple,
//            color: Colores.azulOscuro,
//            padding: 10
//        )
//        ai.startAnimating()
//        ai.center = CGPoint(x: spinnerView.frame.size.width / 2,
//                            y: spinnerView.frame.size.height / 2 - 10);

        let image = UIImage(named: "icon_enlace")
        let imageView = UIImageView(image: image!)
        imageView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        imageView.contentMode = .scaleAspectFit

        textfieldLoading = UILabel(frame: CGRect(x: 0, y: 50, width: 200, height: 50))
        textfieldLoading!.text = textoLoading
        textfieldLoading!.font = Constantes.Fonts.Text.semiBold
        textfieldLoading!.textAlignment = .center

        textfieldLoading!.backgroundColor = Colores.transparente
        textfieldLoading!.textColor = Colores.azulOscuro
        textfieldLoading!.numberOfLines = 3

        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            spinnerView.addSubview(imageView)
            spinnerView.addSubview(self.textfieldLoading!)
            onView.addSubview(spinnerView)
        }

        return spinnerView
    }

    private func removeSpinner(spinner: UIView) {
        DispatchQueue.main.async {
            spinner.removeFromSuperview()
        }
    }

    private func removeCortina(cortina: UIView) {
        DispatchQueue.main.async {
            cortina.removeFromSuperview()
        }
    }

    func showLoading(view: UIView) {
        if spinner == nil {
            cortina = displayCortina(onView: view)
            spinner = displaySpinner(onView: view)
        }
    }

    func hideLoading() {
        if spinner != nil {
            removeSpinner(spinner: spinner!)
            removeCortina(cortina: cortina!)
            spinner = nil
        }
    }

    func updateTextoLoading() {
        if let textfieldLoading = textfieldLoading {
            textfieldLoading.text = "Cargando..."
        }
    }

    func updateTextoLoading(texto: String) {
        if let textfieldLoading = textfieldLoading {
            textfieldLoading.text = texto
        }
    }
}
