//
//  BaseTreeVC.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 06/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class BaseTreeVC: UIViewController, BaseTreeAnimations, BaseClassInfoDelegate {
    var viewControllersItems = [BaseTreeVC]()
    var viewControllersSubViews = [BaseTreeVC]()
    var padreController: BaseTreeVC?
    var delegate: BaseTreeDelegate?
    var posicionIndex: Int = -1
    var data = [String: Any]()
    var keyIndex = [String: Int]()

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func removeViewControllerChild(_ viewController: BaseTreeVC) {
        viewController.willMove(toParentViewController: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParentViewController()
        viewController.padreController = nil
    }

    private func addView(view: UIView, vc: BaseTreeVC) {
        addChildViewController(vc)
        view.addSubview(vc.view)
        vc.view.frame = view.bounds
        vc.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        vc.didMove(toParentViewController: self)
        vc.padreController = self
        if delegate != nil {
            delegate?.onAddItem(vc: vc)
        }
    }

    private func addViewControllerChild(view: UIView, vc: BaseTreeVC) {
        addView(view: view, vc: vc)
        viewControllersSubViews.append(vc)//agregando la lista de subviews child
    }

    private func addViewControllerItem(view: UIView, vc: BaseTreeVC) {
        addView(view: view, vc: vc)
        viewControllersItems.append(vc)//agregando la lista de subviews child
        let key = getClassName(vc)
        let pos = viewControllersItems.count - 1
        keyIndex[key] = pos
    }

    private func loadStoryBoard(nombre: String) -> BaseTreeVC {
        let storyBoard: UIStoryboard = UIStoryboard(name: nombre, bundle: nil)
        let vc = storyBoard.instantiateInitialViewController() as! BaseTreeVC
        return vc
    }

    private func loadStoryBoard(nombre: String, identificador: String) -> BaseTreeVC {
        let storyBoard: UIStoryboard = UIStoryboard(name: nombre, bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: identificador) as! BaseTreeVC
        return vc
    }

    func addViewStoryboard(view: UIView, name: String, identificador: String) {
        let storyBoard = loadStoryBoard(nombre: name, identificador: identificador)
        addViewControllerChild(view: view, vc: storyBoard)
    }

    func addViewStoryBoardList(view: UIView, viewControllers: [[String: String]]) {
        for vc in viewControllers {
            guard let nombre = vc["nombre"] else {
                Logger.e("no se pasado el parametro nombre en el diccionario")
                return
            }
            var storyBoard: BaseTreeVC
            if let identificador = vc["identificador"] {
                storyBoard = loadStoryBoard(nombre: nombre, identificador: identificador)
            } else {
                storyBoard = loadStoryBoard(nombre: nombre)
            }
            addViewControllerItem(view: view, vc: storyBoard)
        }
        cambiarVistaChild(index: 0)
    }

    func addViewXIBList(view: UIView, viewControllers: [BaseTreeVC]) {
        for vc in viewControllers {
            addViewControllerItem(view: view, vc: vc)
        }
        cambiarVistaChild(index: 0)
    }

    func addViewXIB(vc: BaseTreeVC, container: UIView) {
        addViewControllerChild(view: container, vc: vc)
    }

    private func getPosForKey<T>(_ obj: T.Type) -> Int? {
        let key = getClassName(obj)
        return keyIndex[key]
    }

    func cambiarVista<T>(_ tipo: T.Type) {
        guard let indice = getPosForKey(tipo) else {
            Logger.e("no habia clave para cambiar vista del tipo:  \(tipo). ")
            return
        }
        cambiarVista(index: indice)
    }

    func cambiarVista(index: Int) {
//        guard posicionIndex != index else {  return }
        cambiarVistaChild(index: index)
        if delegate != nil {
            delegate?.onChangeItem(pos: index)
        }
    }

    func cambiarVistaChild(index: Int) {
//        if posicionIndex != index {
        guard CodeRep.verifyRangeArray(array: viewControllersItems, index: index) == true else {
            Logger.w("BaseTreeVC: se ha excedido el limite en el rango de los viewcontrollers")
            return
        }
        if let vc = getViewChildSelected() {
            vc.pauseAllChilds()
        }
        for v in viewControllersItems {
            v.view.isHidden = true
            fadeOut(view: v.view, 0.0, delay: 0.0)
        }
        let vista = viewControllersItems[index].view!
        vista.isHidden = false//mostar el actual
        view.bringSubview(toFront: vista)
        fadeIn(view: vista, 0.3, delay: 0.0)
        posicionIndex = index
//        }
    }

    func cambiarVistaInternamente(index: Int) {
        if posicionIndex != index {
            guard CodeRep.verifyRangeArray(array: viewControllersItems, index: index) == true else {
                Logger.w("BaseTreeVC: se ha excedido el limite en el rango de los viewcontrollers")
                return
            }
            posicionIndex = index
        }
    }

    func iniciarStoryBoard(name: String, identificador: String) -> UIViewController {
        let storyBoard: UIStoryboard = UIStoryboard(name: name, bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: identificador)
        return vc
    }

    func showStoryBoardView(vc: UIViewController, completion: (() -> Void)?) {
        self.present(vc, animated: true, completion: completion)
    }

    func getViewChildSelected() -> BaseTreeVC? {
        var r: BaseTreeVC?
        if posicionIndex > -1 {
            r = viewControllersItems[posicionIndex]
        }
        return r
    }

    func getPosChildSelected() -> Int {
        return posicionIndex
    }

    //-------------init pause childs-------------
    func pauseAllChilds() {
        for vc in viewControllersSubViews {
            vc.pauseAllChilds()
        }
        if let vc = getViewChildSelected() {
            vc.pauseAllChilds()
        }
        willHideView()
    }

    func willHideView() {

    }

    //-------------end pause childs-------------
}
