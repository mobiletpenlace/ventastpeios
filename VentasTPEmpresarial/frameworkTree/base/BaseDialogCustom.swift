//
//  BaseDialogCustom.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 27/07/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

protocol CustomAlertViewDelegate: class {
    func clickOKButtonDialog(value: Any)
    func clickCancelButtonDialog()
    func viewDidShow()
}

class BaseDialogCustom: UIViewController, BaseTreeEfectsDelegate, BaseBannerDelegate {
    var delegate: CustomAlertViewDelegate?
    var alertView: UIView!
    var viewControllersSubViews = [BaseTreeVC]()
    var cerrarConclickFuera: Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    func setView(view: UIView) {
        alertView = view
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupView()
        animateView()
        if let delegate = delegate {
            delegate.viewDidShow()
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        view.layoutIfNeeded()
    }

    func setupView() {
        alertView.layer.cornerRadius = 15
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        if cerrarConclickFuera {
            addbuttonClose()
        }
        self.view.addSubview(alertView)
    }

    func addbuttonClose() {
        let btn: UIButton = UIButton(frame: CGRect(x: 100, y: 400, width: 100, height: 50))
        btn.backgroundColor = UIColor.clear
        btn.setTitle("", for: .normal)
        btn.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        btn.tag = 1
        self.view.addSubview(btn)
        btn.snp.makeConstraints { (make) -> Void in
            make.size.equalTo(self.view)
        }
    }

    @objc func buttonAction(sender: UIButton!) {
        let btnsendtag: UIButton = sender
        if btnsendtag.tag == 1 {
            cancel()
        }
    }

    func animateView() {
        alertView.alpha = 0
        self.alertView.frame.origin.y = self.alertView.frame.origin.y + 50
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.alertView.alpha = 1.0
            self.alertView.frame.origin.y = self.alertView.frame.origin.y - 50
        })
    }

    func cancel() {
        delegate?.clickCancelButtonDialog()
        self.dismiss(animated: true, completion: nil)
    }

    func result(value: Any) {
        delegate?.clickOKButtonDialog(value: value)
        self.dismiss(animated: true, completion: nil)
    }

    func addViewXIB(vc: BaseTreeVC, container: UIView) {
        addViewControllerChild(view: container, vc: vc)
    }

    private func addView(view: UIView, vc: BaseTreeVC) {
        addChildViewController(vc)
        view.addSubview(vc.view)
        vc.view.frame = view.bounds
        vc.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        vc.didMove(toParentViewController: self)
    }

    private func addViewControllerChild(view: UIView, vc: BaseTreeVC) {
        addView(view: view, vc: vc)
        viewControllersSubViews.append(vc)//agregando la lista de subviews child
    }

    func showAlert(titulo: String, mensaje: String) {
        let ac = UIAlertController(title: titulo, message: mensaje, preferredStyle: .alert)
        ac.addAction(
            UIAlertAction(title: "OK", style: .default)
        )
        present(ac, animated: true)
    }

    func showAlert(titulo: String, mensaje: String, closure: @escaping (() -> Void)) {
        let ac = UIAlertController(title: titulo, message: mensaje, preferredStyle: .alert)
        ac.addAction(
            UIAlertAction(title: "OK", style: .default) { _ in
                closure()
            }
        )
        present(ac, animated: true)
    }

    func showDialog(customAlert: BaseDialogCustom) {
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(customAlert, animated: true, completion: nil)
    }

    func showDialog(customAlert: BaseDialogCustom, delegate: CustomAlertViewDelegate) {
        customAlert.delegate = delegate
        showDialog(customAlert: customAlert)
    }
}
