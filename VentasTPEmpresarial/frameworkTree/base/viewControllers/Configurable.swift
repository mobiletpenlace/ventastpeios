//
//  Configurable.swift
//  VentasTPEmpresarial
//
//  Created by eduardo mancilla on 3/7/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation

protocol Configurable {
    associatedtype DataType: Any
    func config(withItem item: DataType)
}
