//
//  LabelsPresentable.swift
//  VentasTPEmpresarial
//
//  Created by eduardo mancilla on 3/7/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation

protocol LabelsPresentable {
    var texts: [String] { get }
}
