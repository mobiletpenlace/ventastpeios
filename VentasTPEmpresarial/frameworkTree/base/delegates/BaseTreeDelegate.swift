//
//  BaseTreeDelegate.swift
//  VentasTPEmpresarial
//
//  Created by eduardo mancilla on 2/14/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation

protocol BaseTreeDelegate {
    func onChangeItem(pos: Int)
    func onAddItem(vc: BaseTreeVC)
}
