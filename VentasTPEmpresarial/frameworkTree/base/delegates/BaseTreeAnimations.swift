//
//  BaseTreeAnimations.swift
//  VentasTPEmpresarial
//
//  Created by eduardo mancilla on 2/20/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation

protocol BaseTreeAnimations {
    func fadeIn(view: UIView, _ duration: TimeInterval, delay: TimeInterval, completion: ((Bool) -> Void)?)
    func fadeOut(view: UIView, _ duration: TimeInterval, delay: TimeInterval, completion: ((Bool) -> Void)?)
}

extension BaseTreeAnimations {

    func fadeIn(view: UIView, _ duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: ((Bool) -> Void)? = nil) {
        UIView.animate(
            withDuration: duration,
            delay: delay,
            options: UIViewAnimationOptions.curveEaseIn,
            animations: { view.alpha = 1.0 },
            completion: completion
        )
    }

    func fadeOut(view: UIView, _ duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: ((Bool) -> Void)? = nil) {
        UIView.animate(
            withDuration: duration,
            delay: delay,
            options: UIViewAnimationOptions.curveEaseIn,
            animations: { view.alpha = 0.0 },
            completion: completion
        )
    }

}
