//
//  BaseClassInfoDelegate.swift
//  VentasTPEmpresarial
//
//  Created by eduardo mancilla on 2/19/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation

protocol BaseClassInfoDelegate {
    func getClassName<T>(_ obj: T) -> String
    func getClassName<T>(_ obj: T.Type) -> String
}

extension BaseClassInfoDelegate {

    func getClassName<T>(_ obj: T) -> String {
        return String(describing: type(of: obj))
    }

    func getClassName<T>(_ obj: T.Type) -> String {
        return String(describing: T.self)
    }
}
