//
//  BaseBannerDelegate.swift
//  VentasTPEmpresarial
//
//  Created by eduardo mancilla on 2/14/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation
import NotificationBannerSwift

protocol BaseBannerDelegate {
    func showBanner(title: String, style: BannerStyle)
    func showBanner(title: String, subtitle: String, style: BannerStyle)
    func showBanner(title: String, subtitle: String, style: BannerStyle, leftView: UIImageView)
    func showBanner(title: String, subtitle: String, style: BannerStyle, leftView: UIImageView, rightView: UIImageView)
    func showBannerOK(title: String)
    func showBannerError(title: String)
    func showBannerAdvertencia(title: String)
}

extension BaseBannerDelegate {

    func showBanner(title: String, style: BannerStyle) {
        let banner = NotificationBanner(
            title: title,
            style: style
        )
        show(banner: banner)
    }

    func showBanner(title: String, subtitle: String, style: BannerStyle) {
        let banner = NotificationBanner(
            title: title,
            subtitle: subtitle,
            style: style
        )
        show(banner: banner)
    }

    func showBannerOK(title: String) {
        let leftView = UIImageView(image: #imageLiteral(resourceName: "icon_check_on"))
        let banner = NotificationBanner(
            title: title,
            leftView: leftView,
            style: .success
        )
        show(banner: banner)
    }

    func showBannerError(title: String) {
        let leftView = UIImageView(image: #imageLiteral(resourceName: "icon_fail"))
        let banner = NotificationBanner(
            title: title,
            leftView: leftView,
            style: .danger
        )
        show(banner: banner)
    }

    func showBannerAdvertencia(title: String) {
        let leftView = UIImageView(image: #imageLiteral(resourceName: "logo_tipsVentas"))
        let banner = NotificationBanner(
            title: title,
            leftView: leftView,
            style: .warning
        )
        show(banner: banner)
    }

    func showBanner(title: String, subtitle: String, style: BannerStyle, leftView: UIImageView) {
        let banner = NotificationBanner(
            title: title,
            subtitle: subtitle,
            leftView: leftView,
            style: style
        )
        show(banner: banner)
    }

    func showBanner(title: String, subtitle: String, style: BannerStyle, leftView: UIImageView, rightView: UIImageView) {
        let banner = NotificationBanner(
            title: title,
            subtitle: subtitle,
            leftView: leftView,
            rightView: rightView,
            style: style
        )
        show(banner: banner)
    }

    private func show(banner: NotificationBanner) {
        banner.duration = 2
        banner.show()
    }
}
