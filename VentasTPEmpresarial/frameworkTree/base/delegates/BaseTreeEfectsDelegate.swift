//
//  BaseTreeEfectsDelegate.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 08/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

protocol BaseTreeEfectsDelegate {
    func addShadowView(view: UIView)
    func addBorder(view: UIView)
    func addRoundCorner(view: UIView, value: Float)
    func addRoundCornersInBottom(cornerRadius: Double, view: UIView)
}

extension BaseTreeEfectsDelegate {

    func addShadowView(view: UIView) {
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOffset = CGSize(width: 6.0, height: 6.0)
        view.layer.masksToBounds = false
        view.layer.shadowRadius = 5.0
        view.layer.shadowOpacity = 0.2
    }

    func addBorder(view: UIView) {
        view.layer.borderColor = UIColor.black.cgColor
        view.layer.borderWidth = CGFloat(0.5)
    }

    func addRoundCorner(view: UIView, value: Float) {
        view.layer.cornerRadius = CGFloat(value)
        view.clipsToBounds = true
    }

    func addRoundCornersInBottom(cornerRadius: Double, view: UIView) {
        let path = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: [.bottomLeft, .bottomRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = view.bounds
        maskLayer.path = path.cgPath
        view.layer.mask = maskLayer
    }

}
