//
//  CascadeDelete.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 03/09/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

/* Example:
final class ContactEntity: Object {
    @objc dynamic var id: String = ""
    let phoneNumbers = List<PhoneNumberEntity>() // this needs to be deleted every time `ContactEntity` is deleted
    let profiles = List<SocialProfileEntity>()   // this needs to be deleted every time `ContactEntity` is deleted
}

guard let database = try? Realm() else { return }
let contacts = database.objects(ContactEntity.self)
do {
    try database.write {
        database.delete(contacts, cascading: true)
    }
} catch {
    // handle write error here
}
// to delete ContactEntity
guard let contact = Array(database.objects(ContactEntity.self)).first else { return }
do {
    try database.write {
        database.delete(contact, cascading: true)
    }
} catch {
    // handle write error here
}
 */

import RealmSwift
import Realm

protocol CascadeDeleting: class {
    func delete<S: Sequence>(_ objects: S, cascading: Bool) where S.Iterator.Element: Object
    func delete<Entity: Object>(_ entity: Entity, cascading: Bool)
}

extension Realm: CascadeDeleting {
    func delete<S: Sequence>(_ objects: S, cascading: Bool) where S.Iterator.Element: Object {
        for obj in objects {
            delete(obj, cascading: cascading)
        }
    }
    
    func delete<Entity: Object>(_ entity: Entity, cascading: Bool) {
        if cascading {
            cascadeDelete(entity)
        } else {
            delete(entity)
        }
    }
}

private extension Realm {
    private func cascadeDelete(_ entity: RLMObjectBase) {
        guard let entity = entity as? Object else { return }
        var toBeDeleted = Set<RLMObjectBase>()
        toBeDeleted.insert(entity)
        while !toBeDeleted.isEmpty {
            guard let element = toBeDeleted.removeFirst() as? Object,
                !element.isInvalidated else { continue }
            resolve(element: element, toBeDeleted: &toBeDeleted)
        }
    }
    
    private func resolve(element: Object, toBeDeleted: inout Set<RLMObjectBase>) {
        element.objectSchema.properties.forEach {
            guard let value = element.value(forKey: $0.name) else { return }
            if let entity = value as? RLMObjectBase {
                toBeDeleted.insert(entity)
            } else if let list = value as? RealmSwift.ListBase {
                for index in 0..<list._rlmArray.count {
                    toBeDeleted.insert(list._rlmArray.object(at: index) as! RLMObjectBase)
                }
            }
        }
        delete(element)
    }
}
