//
//  Log.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 04/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

/// Enum which maps an appropiate symbol which added as prefix for each log message
///
/// - error: Log type error
/// - info: Log type info
/// - debug: Log type debug
/// - verbose: Log type verbose
/// - warning: Log type warning
/// - severe: Log type severe
enum LogEvent: String {
//    case e = "[‼️]" // error
//    case i = "[ℹ️]" // info
//    case d = "[💬]" // debug
//    case v = "[🔬]" // verbose
//    case w = "[⚠️]" // warning
//    case s = "[🔥]" // severe

    case e = "[❌❌❌]" // error
    case i = "[✅✅✅]" // info
    case d = "[💬💬💬]" // debug
    case v = "[📘📘📘]" // verbose
    case w = "[⚠️⚠️⚠️]" // warning
    case s = "[🔥🔥🔥]" // severe
}

/// Wrapping Swift.print() within DEBUG flag
///
/// - Note: *print()* might cause [security vulnerabilities](https://codifiedsecurity.com/mobile-app-security-testing-checklist-ios/)
///
/// - Parameter object: The object which is to be logged
///
func print(_ object: Any) {
    // Only allowing in DEBUG mode
    #if DEBUG
        Swift.print(object)
    #endif
}

class Logger {

//    static var dateFormat = "yyyy-MM-dd hh:mm:ssSSS"
    static var dateFormat = "hh:mm:ss"
    static var dateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = dateFormat
        formatter.locale = Locale.current
        formatter.timeZone = TimeZone.current
        return formatter
    }

    private static var isLoggingEnabled: Bool {
        #if DEBUG
            return true
        #else
            return false
        #endif
    }

    class func e(_ object: Any, filename: String = #file, line: Int = #line, column: Int = #column, funcName: String = #function) {
        if isLoggingEnabled {
            print("\(LogEvent.e.rawValue)|\(sourceFileName(filePath: filename))|\(funcName):\(line):\(column) at \(Date().toString()) |||| \(object)")
        }
    }

    class func i (_ object: Any, filename: String = #file, line: Int = #line, column: Int = #column, funcName: String = #function) {
        if isLoggingEnabled {
            print("\(LogEvent.i.rawValue)|\(sourceFileName(filePath: filename))|\(funcName):\(line):\(column) at \(Date().toString()) |||| \(object)")
        }
    }

    class func d(_ object: Any, filename: String = #file, line: Int = #line, column: Int = #column, funcName: String = #function) {
        if isLoggingEnabled {
            print("\(LogEvent.d.rawValue)|\(sourceFileName(filePath: filename))|\(funcName):\(line):\(column) at \(Date().toString()) |||| \(object)")
        }
    }

    class func v(_ object: Any, filename: String = #file, line: Int = #line, column: Int = #column, funcName: String = #function) {
        if isLoggingEnabled {
            print("\(LogEvent.v.rawValue)|\(sourceFileName(filePath: filename))|\(funcName):\(line):\(column) at \(Date().toString()) |||| \(object)")
        }
    }

    class func w(_ object: Any, filename: String = #file, line: Int = #line, column: Int = #column, funcName: String = #function) {
        if isLoggingEnabled {
            print("\(LogEvent.w.rawValue)|\(sourceFileName(filePath: filename))|\(funcName):\(line):\(column) at \(Date().toString()) |||| \(object)")
        }
    }

    class func s(_ object: Any, filename: String = #file, line: Int = #line, column: Int = #column, funcName: String = #function) {
        if isLoggingEnabled {
            print("\(LogEvent.s.rawValue)|\(sourceFileName(filePath: filename))|\(funcName):\(line):\(column) at \(Date().toString()) |||| \(object)")
        }
    }

    /// Extract the file name from the file path
    ///
    /// - Parameter filePath: Full file path in bundle
    /// - Returns: File Name with extension
    private class func sourceFileName(filePath: String) -> String {
        let components = filePath.components(separatedBy: "/")
        return components.isEmpty ? "" : components.last!
    }

    @available(*, deprecated, message: "replace for Logger.d()")
    class func println(_ mensaje: String) {
        print(" log: --> " + mensaje)
    }
}

internal extension Date {
    func toString() -> String {
        return Logger.dateFormatter.string(from: self as Date)
    }
}
