//
//  CodeRep.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 07/08/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

class CodeRep {
    static func verifyRangeArray(array: [Any], index: Int) -> Bool {
        return (array.count > 0) && (index < array.count) && (index >= 0)
    }
}
