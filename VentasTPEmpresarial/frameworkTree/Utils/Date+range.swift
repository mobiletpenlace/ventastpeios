//
//  Date+range.swift
//  VentasTPEmpresarial
//
//  Created by julian dorantes fuertes on 15/11/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

extension Date {
    func isBetween(_ date1: Date, and date2: Date) -> Bool {
        return (min(date1, date2) ... max(date1, date2)).contains(self)
    }
}
