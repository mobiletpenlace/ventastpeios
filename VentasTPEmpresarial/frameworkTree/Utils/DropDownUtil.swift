//
//  DropDownUtil.swift
//  VentasTPEmpresarial
//
//  Created by eduardo mancilla on 2/26/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//
import UIKit
import DropDown
import EasyTipView

class DropDownUtil: NSObject {
    private var lista: [UITextField: DropDown] = [:]
    private var listaToolTips: [UITextField: EasyTipView] = [:]
    private var listaToolTipsIsShow: [UITextField: Bool] = [:]
    private var view: UIView?
    private let tiempoToolTip = 2000
    let borderColorBien = UIColor.lightGray
    let borderColorMal = UIColor.red
    let mensajeError = "Seleccione una opción."

    override init() {
        DropDown.appearance().textColor = Colores.negro
        DropDown.appearance().textFont = Constantes.Fonts.Text.semiBold
        DropDown.appearance().backgroundColor = Colores.blanco
        DropDown.appearance().selectionBackgroundColor = Colores.selectDlgMiPerfilCelda
        DropDown.appearance().cornerRadius3 = 0
    }

    private func existe(_ textfield: UITextField) -> Bool {
        return lista[textfield] != nil
    }

    func add(textfield: UITextField, data array: [String], view: UIView? = nil, selectionAction closure: SelectionClosure?) {
        guard !existe(textfield) else {
            Logger.w("Ya existe un dropdown para ese textfield.")
            return
        }

        self.view = view

        let dropDown = DropDown()
        dropDown.anchorView = textfield // UIView or UIBarButtonItem
        dropDown.dataSource = array
        dropDown.direction = .bottom

        lista[textfield] = dropDown
        listaToolTips[textfield] = EasyTipView(text: mensajeError)
        listaToolTipsIsShow[textfield] = false

        dropDown.bottomOffset = CGPoint(x: 0, y: (dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.selectionAction = { (index: Int, item: String) in
            textfield.text = item
            if let closure = closure {
                closure(index, item)
            }
        }
    }

    func seleccionarDropDown(pos: Int, _ textField: UITextField) {
        if let dropDown = lista[textField] {
            dropDown.clearSelection()
            dropDown.selectRow(at: pos)
            textField.text = dropDown.selectedItem
            _ = validate()
        }
    }

    func updateDropdown(pos: Int, array: [String], textfield: UITextField) {
        if let dropDown = lista[textfield] {
            dropDown.dataSource = array
            dropDown.clearSelection()

            dropDown.selectRow(at: pos)
            guard pos >= 0 && pos < array.count else {
                textfield.text = ""
                Logger.d("El indice es mayor que los datos.")
                return
            }
            textfield.text = dropDown.selectedItem
             _ = validate()
        }
    }

    func updateDropdown(value: String, array: [String], textfield: UITextField) {
        if let dropDown = lista[textfield] {
            dropDown.dataSource = array
            dropDown.clearSelection()
            var pos = -1
            for (i, v) in array.enumerated() {
                if v == value { pos = i }
            }
            guard pos != -1 else {
                Logger.e("La posición en el dropdown para el valor buscado es -1, por lo tanto no se encontró")
                return
            }
            dropDown.selectRow(at: pos)
            guard pos >= 0 && pos < array.count else {
                textfield.text = ""
                Logger.d("El indice es mayor que los datos.")
                return
            }
            textfield.text = dropDown.selectedItem
            _ = validate()
        }
    }

    func clear() {
        for (textfield, dropDown) in lista {
            textfield.text = ""
            dropDown.clearSelection()
            bien(textfield)
        }
    }

    func validate() -> Bool {
        var res = true
        for (textfield, _) in lista {
            let texto = textfield.text ?? ""
            Logger.d("el texto del dropbox es: \(texto)")

            if texto.trim().count == 0 {
                mal(textfield)
                res = false
            } else {
                bien(textfield)
            }
        }
        return res
    }

    private func bien(_ textField: UITextField) {
        textField.layer.borderColor = borderColorBien.cgColor
        textField.layer.borderWidth = 0.25
//        textField.addLineBotom(color: borderColorBien, width: 0.25)

        guard let tipView = listaToolTips[textField] else {
            Logger.w("No se ha encontrado el tooltip con esa clave.")
            return
        }
        tipView.dismiss()
    }

    private func mal(_ textField: UITextField) {
        textField.layer.borderColor = borderColorMal.cgColor
        textField.layer.borderWidth = 2
//        textField.addLineBotom(color: borderColorMal, width: 2)
        guard let tipView = listaToolTips[textField] else {
            Logger.w("No se ha encontrado el tooltip con esa clave.")
            return
        }
        guard let tipViewIsShow = listaToolTipsIsShow[textField] else {
            Logger.w("No se ha encontrado el tooltip bool con esa clave.")
            return
        }

        if tipViewIsShow == false {
            listaToolTipsIsShow[textField] = true
            tipView.show(forView: textField)
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(tiempoToolTip)) {
                tipView.dismiss {
                    self.listaToolTipsIsShow[textField] = false
                }
            }
        }
    }
}

extension DropDownUtil: UITextFieldDelegate {

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if let dropDown = lista[textField] {
            if let view = view {
                view.endEditing(true)
            }
            dropDown.show()
            return false
        }
        return true
    }

}
