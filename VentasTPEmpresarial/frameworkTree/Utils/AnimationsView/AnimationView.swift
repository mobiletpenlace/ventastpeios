//
//  AnimationView.swift
//  VentasTPEmpresarial
//
//  Created by eduardo mancilla on 3/5/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation

class AnimationView {
    
    class func shakeAnimation(_ view: UIView, numberOfShakes shakes: Float, revert: Bool) {
        //        let animation: CABasicAnimation = CABasicAnimation(keyPath: "shadowColor")
        //        animation.fromValue = baseColor
        //        animation.toValue = UIColor.red.cgColor
        //        animation.duration = 0.4
        //        if revert { animation.autoreverses = true } else { animation.autoreverses = false }
        //        view.layer.add(animation, forKey: "")
        
        let shake: CABasicAnimation = CABasicAnimation(keyPath: "position")
        shake.duration = 0.07
        shake.repeatCount = shakes
        if revert { shake.autoreverses = true  } else { shake.autoreverses = false }
        shake.fromValue = NSValue(cgPoint: CGPoint(x: view.center.x - 10, y: view.center.y))
        shake.toValue = NSValue(cgPoint: CGPoint(x: view.center.x + 10, y: view.center.y))
        view.layer.add(shake, forKey: "position")
    }
}
