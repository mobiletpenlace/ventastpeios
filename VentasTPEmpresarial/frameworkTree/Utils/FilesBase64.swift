//
//  FilesBase64.swift
//  VentasTPEmpresarial
//
//  Created by eduardo mancilla on 12/10/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import Foundation

class FilesBase64 {

    static func pdf(_ urlPDF: String) -> String {
        guard let url = URL(string: urlPDF) else { return "" }
        return pdf(url)
    }

    static func pdf(_ url: URL) -> String {
        let fileData = try! Data.init(contentsOf: url)
        return pdf(fileData)
    }

    static func pdf(_ fileData: Data) -> String {
        let base64String: String = fileData.base64EncodedString(options: NSData.Base64EncodingOptions.init(rawValue: 0))
        return base64String
    }
}
