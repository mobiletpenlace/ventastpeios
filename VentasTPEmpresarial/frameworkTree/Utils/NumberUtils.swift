//
//  NumberUtils.swift
//  VentasTPEmpresarial
//
//  Created by eduardo mancilla on 1/9/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation

class NumberUtils {

    static func roundedPlaces(number: Float, toPlaces places: Int) -> Float {
        let divisor = pow(10.0, Double(places))
        return Float((Double(number) * divisor).rounded() / divisor)
    }

}
