//
//  ButtonUtils.swift
//  VentasTPEmpresarial
//
//  Created by eduardo mancilla on 2/26/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation
class ButtonUtils {

    class func setIcon(_ button: UIButton, icon: String) {
        button.setImage(UIImage(named: icon), for: .normal)
    }

    class func setIcon(_ button: UIButton, icon: UIImage) {
        button.setImage(icon, for: .normal)
    }

    class func setTitle(_ button: UIButton, title: String) {
        button.setTitle(title, for: .normal)
    }

    class func setTitle(_ button: UIButton, _ valeTrue: String, _ valueFalse: String, res: Bool) {
        setTitle(
            button,
            title: res ? valeTrue : valueFalse
        )
    }
}
