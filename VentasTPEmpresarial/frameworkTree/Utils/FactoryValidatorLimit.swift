//
//  FactoryValidatorLimit.swift
//  VentasTPEmpresarial
//
//  Created by eduardo mancilla on 2/27/19.
//  Copyright © 2019 TotalPlay. All rights reserved.
//

import Foundation

class ValidatorLimitManager: NSObject {
    var listaValidator = [FieldValidator]()

    func addValidator(_ field: FieldValidator) {
        listaValidator.append(field)
    }
}

extension ValidatorLimitManager: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard string.count > 0 else { return true }
        return validateTextfield(textField, range, string)
    }

    func validateTextfield(_ textFieldP: UITextField, _ range: NSRange, _ string: String) -> Bool {
        let allowedChars = "12345"
        for validator in listaValidator {
            switch validator {
            case .textField(let textField, let regexType):
                if textFieldP == textField {
                    var maximo: Int?
                    switch regexType {
                    case .max(let value):
                        maximo = value
                    case .min(let value):
                        maximo = nil
                    case .exact(let value):
                        maximo = value
                    case .between(let valueMin, let valueMax):
                        maximo = valueMax
                    case .validate(let value):
                        maximo = nil
                    }
                    return restrict(textField, range, string, allowedChars, maximo)
                }
            case .textView(let textView, let regexType):
                Logger.d("se ha agregado un textview")
            }
        }
    }

    func restrict(_ textField: UITextField, _ range: NSRange, _ string: String, _ allowedChars: String? = nil, _ maxLength: Int? = nil) -> Bool {
        let currentText = textField.text ?? ""
        let prospectiveText = (currentText as NSString).replacingCharacters(in: range, with: string)
        let containOnly = (allowedChars != nil) ? containsOnly(prospectiveText, allowedChars!) : true
        let limit = (maxLength != nil) ? isLimit(prospectiveText, max: maxLength!) : true
        return containOnly && limit
    }

    private func containsOnly(_ string: String, _ matchCharacters: String) -> Bool {
        let disallowedCharacterSet = NSCharacterSet(charactersIn: matchCharacters).inverted
        return string.rangeOfCharacter(from: disallowedCharacterSet) == nil
    }

    private func isLimit(_ string: String, max: Int) -> Bool {
        return string.count <= max
    }
}
